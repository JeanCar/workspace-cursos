package com.nh.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
public class ReplyingKafkaConsumer {

	@KafkaListener(topics = "${spring.kafka.topic.request-topic}")
	@SendTo
	public Model listen(Model request) throws InterruptedException {
		int sum = request.getFirstNumber() + request.getSecondNumber();
		request.setAdditionalProperties("sum", sum);
		return request;
	}

	@KafkaListener(topics = "${spring.kafka.topic.request-topic2}")
	@SendTo
	public Model listenMultiply(Model request) throws InterruptedException {
		int mult = request.getFirstNumber() * request.getSecondNumber();
		request.setAdditionalProperties("mult", mult);
		return request;
	}
}
