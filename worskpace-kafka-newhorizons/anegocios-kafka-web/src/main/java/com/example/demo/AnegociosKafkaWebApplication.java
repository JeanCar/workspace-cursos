package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnegociosKafkaWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnegociosKafkaWebApplication.class, args);
	}

}
