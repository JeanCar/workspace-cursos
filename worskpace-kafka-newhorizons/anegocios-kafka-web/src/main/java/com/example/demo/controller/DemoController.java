package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Calculator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class DemoController {

	private static final Logger logger = LoggerFactory.getLogger(DemoController.class);

	@GetMapping("/index")
	public String calculate(Model model) {
		model.addAttribute("calculator", new Calculator());
		model.addAttribute("resultado", "");
		return "index";
	}

	@RequestMapping(value = "/processForm", method = RequestMethod.POST)
	public ModelAndView processForm(@Validated Calculator calculator, @RequestParam("radioName") String seleccionar) {
		ModelAndView model = new ModelAndView();
		String url = null;
		String key = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			ObjectMapper mapper = new ObjectMapper();
			HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(calculator), headers);
			if ("Sumar".equalsIgnoreCase(seleccionar)) {
				url = "http://localhost:8083/sum";
				key = "sum";
			}
			if ("Multiplicar".equalsIgnoreCase(seleccionar)) {
				url = "http://localhost:8083/mult";
				key = "mult";
			}
			ResponseEntity<Calculator> response = restTemplate.postForEntity(url, entity, Calculator.class);
			System.out.println(response.getBody().toString());
			model.addObject("resultado", response.getBody().getAdditionalProperties().get(key));
			model.setViewName("index");
		} catch (RestClientException e) {
			logger.debug(e.getMessage());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return model;
	}
}
