package com.nh.everis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MicroKafkaBrokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroKafkaBrokerApplication.class, args);
	}

}
