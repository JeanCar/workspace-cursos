package org.ejemplos;

import io.reactivex.Observable;

public class LauncherDisposable4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<Integer> source = Observable.create(observableEmitter -> {
			try {
				for (int i = 0; i < 10000; i++) {
					while (!observableEmitter.isDisposed()) {
						observableEmitter.onNext(i);
					}
					if (observableEmitter.isDisposed())
						return;
				}
				observableEmitter.onComplete();
			} catch (Throwable e) {
				observableEmitter.onError(e);
			}
		});
		source.subscribe(l -> System.out.println("Se muestra: " + l));
	}

}
