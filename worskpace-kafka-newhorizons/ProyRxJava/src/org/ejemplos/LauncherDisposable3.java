package org.ejemplos;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class LauncherDisposable3 {
	private static final CompositeDisposable disposables = new CompositeDisposable();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);
		// subscribe and capture disposables
		Disposable disposable1 = seconds.subscribe(i -> System.out.println("Observer 1: " + i));
		Disposable disposable2 = seconds.subscribe(i -> System.out.println("Observer 2: " + i));

		// put both disposables into CompositeDisposable
		disposables.addAll(disposable1, disposable2);
		// sleep 5 seconds
		sleep(5000);
		// dispose all disposables
		disposables.dispose();
		// sleep 5 seconds to prove
		// there are no more emissions
		sleep(5000);
	}

	public static void sleep(int valor) {
		try {
			Thread.sleep(valor);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
