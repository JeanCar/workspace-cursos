package org.ejemplos;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

public class LauncherObservable5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> items =
				 Arrays. asList("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
				 Observable<String> source = Observable. fromIterable(items);
				 source. map(String:: length) . filter(i -> i >= 5)
				 . subscribe(s -> System. out. println("RECEIVED: " + s));
				 }
	}


