package org.ejemplos;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class Launcher3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<Long> secondIntervals = Observable.interval(1, TimeUnit.SECONDS);
		secondIntervals.subscribe(s -> System.out.println(s));
		/*
		 * Hold main thread for 5 seconds so Observable above has chance to fire
		 */
		sleep(5000);
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
