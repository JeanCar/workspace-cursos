package org.ejemplos;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class LauncherObserver1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
		Observer<Integer> myObserver = new Observer<Integer>() {
			@Override
			public void onSubscribe(Disposable d) {
				// do nothing with Disposable, disregard for now
			}

			@Override
			public void onNext(Integer value) {
				System.out.println("RECEIVED: " + value);
			}

			@Override
			public void onError(Throwable e) {
				e.printStackTrace();
			}

			@Override
			public void onComplete() {
				System.out.println("Done!");
			}
		};
		source.map(String::length).filter(i -> i >= 5).subscribe(myObserver);
	}
}
