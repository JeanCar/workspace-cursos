package org.ejemplos;

import io.reactivex.Observable;

import com.github.davidmoten.rx.jdbc.ConnectionProviderFromUrl;
import com.github.davidmoten.rx.jdbc.Database;
import java.sql. Connection;

public class LauncherObsCold3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Connection conn =
				 new ConnectionProviderFromUrl("jdbc:sqlite:/home/thomas/rexon_metals.db"). get();
				 Database db = Database. from(conn);
				 Observable<String> customerNames =  db. select("SELECT NAME FROM CUSTOMER")
				 . getAs(String.class);
				 customerNames. subscribe(s -> System. out. println(s));
	}

}
