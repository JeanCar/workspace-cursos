package org.ejemplos;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.ResourceObserver;

public class LauncherDisposable2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<Long> source = 	Observable.interval(1, TimeUnit.SECONDS);
		ResourceObserver<Long> myObserver = new ResourceObserver<Long>() {
			@Override
			public void onNext(Long value) {
				System.out.println(value);
			}

			@Override
			public void onError(Throwable e) {
				e.printStackTrace();
			}

			@Override
			public void onComplete() {
				System. out. println("Completado!");
			}
		};
		//capture Disposable
		 Disposable disposable = source. subscribeWith(myObserver);
	}
}
