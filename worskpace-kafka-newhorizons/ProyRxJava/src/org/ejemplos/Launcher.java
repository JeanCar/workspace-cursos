package org.ejemplos;

import io.reactivex.Observable;

public class Launcher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<String> myStrings =
				 Observable. just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
				 myStrings. subscribe(s -> System. out. println(s));
	}

}
