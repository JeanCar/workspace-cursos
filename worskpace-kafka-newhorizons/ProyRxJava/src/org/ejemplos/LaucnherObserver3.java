package org.ejemplos;

import io.reactivex.Observable;

public class LaucnherObserver3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Observable<String> source =
				 Observable. just("Alpha", "Beta", "Gamma", "Delta",  "Epsilon");
				 source. map(String:: length) . filter(i -> i >= 5)
				 . subscribe(i -> System. out. println("RECEIVED: " + i),
						 Throwable:: printStackTrace);
	}

}
