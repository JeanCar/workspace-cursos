package org.ejemplos;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class LauncherDisposable1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);
		Disposable disposable = seconds.subscribe(l -> System.out.println("Recibido: " + l));
		// sleep 5 seconds
		sleep(5000);

		// dispose and stop emissions
		disposable.dispose();
		// sleep 5 seconds to prove
		// there are no more emissions
		sleep(5000);

	}

	public static void sleep(int valor) {
		try {
			Thread.sleep(valor);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
