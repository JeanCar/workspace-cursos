package org.ejemplos;

import io.reactivex.Observable;

public class LauncherObsHot5 {
	private static int inicio = 1;
	 private static int cuenta = 5;
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		// ejemplo de empty()
		Observable<String> empty = Observable.empty();
		empty.subscribe(System.out::println, Throwable::printStackTrace, () -> System.out.println("Realizado!"));

		// ejemplo de never()
		Observable<String> obs1 = Observable.never();
		obs1.subscribe(System.out::println, Throwable::printStackTrace, () -> System.out.println("Completado!"));
		sleep(5000);
		
		// ejemplo de error()
		Observable. error(new Exception("Fallo en el programa!"))
		. subscribe(i -> System. out. println("RECEIVED: " + i),
				 Throwable:: printStackTrace,
				 () -> System. out. println("Hecho!"));
		
		usoDefer();
		usoCallable();
	}

	private static void usoCallable() {
		// TODO Auto-generated method stub
		
	Observable. just(1 / 0)
		 . subscribe(i -> System. out. println("RECIBIDO: " + i), e -> System. out. println("Error Generado: " + e) );
		
		Observable. fromCallable(() -> 1 / 0)
		 . subscribe(i -> System. out. println("Received: " + i), e -> System. out. println("Se produjo error: " + e) );
	}

	private static void usoDefer() {
		// TODO Auto-generated method stub
		
		 //Observable<Integer> source = Observable. range(inicio, cuenta);
		 Observable<Integer> source = Observable. defer(() -> Observable. range(inicio, cuenta));
		source. subscribe(i -> System. out. println("Observador 1: " + i));
		 //modify count
		 cuenta = 10;
		 source. subscribe(i -> System. out. println("Observador 2: " + i));
	}

	public static void sleep(int intervalo) {
		try {
			Thread.sleep(intervalo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
