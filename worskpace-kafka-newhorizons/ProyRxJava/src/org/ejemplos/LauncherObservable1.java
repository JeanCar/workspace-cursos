package org.ejemplos;

import io.reactivex.Observable;

public class LauncherObservable1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<String> source = Observable. create(emitter -> {
			 emitter. onNext("Alpha");
			 emitter. onNext("Beta");
			 emitter. onNext("Gamma");
			 emitter. onNext("Delta");
			 emitter. onNext("Epsilon");
			 emitter. onComplete();
			 });
			 source. subscribe(s -> System. out. println("RECEIVED: " + s));
	}

}
