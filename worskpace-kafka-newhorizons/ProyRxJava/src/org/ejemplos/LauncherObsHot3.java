package org.ejemplos;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class LauncherObsHot3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Observable<Long> seconds = Observable. interval(1,TimeUnit. SECONDS);
		//Observer 1
		 seconds. subscribe(l -> System. out. println("Observer 1: " + l));
		 //sleep 7 seconds
		 sleep(7000);
		 //Observer 2
		 seconds. subscribe(l -> System. out. println("Observer 2: " + l));
		 //sleep 5 seconds
		 sleep(5000);
		 }
	

	private static void sleep(int intervalo) {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(intervalo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
