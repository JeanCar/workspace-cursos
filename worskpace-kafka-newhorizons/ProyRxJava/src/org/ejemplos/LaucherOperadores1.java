package org.ejemplos;

import java.text.SimpleDateFormat;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public class LaucherOperadores1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      //usoSingle();
     // usoMaybe();
      //usoCompletable();
      usoGroupBy();
	}

	private static void usoGroupBy() {
		// TODO Auto-generated method stub
		
		 Observable<String> mySource1 = Observable.just("a", "bb", "cc", "d", "eee");
	        mySource1.groupBy(s -> s.length())
	                .flatMapSingle(x -> x.toList())
	                .subscribe(System.out::println);

		
		
		Observable<String> mySource = Observable.just("a", "bb", "cc", "d", "eee");
        mySource.groupBy(s -> s.length())
				
				  .flatMapSingle(group -> group.reduce("", (x,y) -> "".equals(x) ? y : x + ", "
				  + y) .map(e -> group.getKey() + ": " + e))
				 
                .subscribe(System.out::println);
	}

	private static void usoCompletable() {
		// TODO Auto-generated method stub
		Completable. fromRunnable(() -> runProcess())
		 . subscribe(() -> System. out. println("Completado!"));
		 }
		 public static void runProcess() {
		   SimpleDateFormat hoy = new SimpleDateFormat("dd/MM/yyyy");
		   System.out.println("Hoy es "+hoy.format(new java.util.Date()));
		 }
	

	private static void usoMaybe() {
		// TODO Auto-generated method stub
		// has emission
		 Maybe<Integer> presentSource = Maybe. just(100);
		 presentSource. subscribe(s -> System. out. println(" (Maybe) Proceso 1 recibido: " + s), Throwable:: printStackTrace,
		 () -> System. out. println("Maybe Proceso 1 hecho!") );
		 //no emission
		 Maybe<Integer> emptySource = Maybe. empty();
		 emptySource. subscribe(s -> System. out. println(" (Maybe) Proceso 2 recibido: " + s), Throwable:: printStackTrace,
		 () -> System. out. println("Maybe Proceso 2 hecho!") );
	}

	private static void usoSingle() {
		// TODO Auto-generated method stub
		Single. just("Curso NH"). map(String:: length)
		. subscribe(System. out:: println,
				 Throwable::printStackTrace);
	}

}
