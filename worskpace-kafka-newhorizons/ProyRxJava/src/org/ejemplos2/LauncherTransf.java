package org.ejemplos2;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LauncherTransf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//usoBuffer1();
		//usoBuffer2();
		//usoWindow1();
		//usoWindow2();
		//usoThrottle();
		usoSwitching();
	}

	private static void usoSwitching() {
		// TODO Auto-generated method stub
		Observable<String> items = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota");
				 //delay each String to emulate an intense calculation
				 Observable<String> processStrings = items.concatMap(s ->  Observable.just(s)
				 .delay(randomSleepTime(),TimeUnit.MILLISECONDS));
				 //run processStrings every 5 seconds, and kill each previous instance to start next
				 Observable.interval(5, TimeUnit.SECONDS)
				 .switchMap(i -> processStrings.doOnDispose(() -> System.out.println("Disposing! Starting next"))
				 ).subscribe(System.out::println);
				 //keep application alive for 20 seconds
				 sleep(20000);
				 }
	
	public static int randomSleepTime() {
		 //returns random sleep time between 0 to 2000 milliseconds
		 return ThreadLocalRandom.current().nextInt(2000);
		 }

	private static void usoThrottle() {
		// TODO Auto-generated method stub
		Observable<String> source1 = Observable.interval(100,TimeUnit.MILLISECONDS)
				 .map(i -> (i + 1) * 100) // map to elapsed time
				 .map(i -> "SOURCE 1: " + i)
				 .take(10);
				 Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS)
						 .map(i -> (i + 1) * 300) // map to elapsed time
						 .map(i -> "SOURCE 2: " + i)
						 .take(3);
				Observable<String> source3 = Observable.interval(2000,	TimeUnit.MILLISECONDS)
						 .map(i -> (i + 1) * 2000) // map to elapsed time
						 .map(i -> "SOURCE 3: " + i)
						 .take(2);
				//Observable.concat(source1, source2, source3).subscribe(System.out::println);
				Observable.concat(source1, source2, source3) .throttleFirst(1, TimeUnit.SECONDS)
				         .subscribe(System.out::println);			
						 sleep(6000);
						 
						 // para throttleLast agregar
							/*
							 * Observable.concat(source1, source2, source3) .throttleLast(1,
							 * TimeUnit.SECONDS) .subscribe(System.out::println);
							 */
						 
						 // para throttleFirst agregar
							/*
							 * Observable.concat(source1, source2, source3) .throttleFirst(1,
							 * TimeUnit.SECONDS) .subscribe(System.out::println);
							 */
	}

	private static void usoWindow2() {
		// TODO Auto-generated method stub
		Observable.interval(300, TimeUnit.MILLISECONDS).map(i -> (i + 1) * 300) // map to elapsed time
		 .window(1, TimeUnit.SECONDS)
		 .flatMapSingle(obs -> obs.reduce("", (total,next) -> total + (total.equals("") ? "" : "|") + next))
		 .subscribe(System.out::println);
		 sleep(6000);
	}

	private static void usoWindow1() {
		// TODO Auto-generated method stub
		Observable.range(1,50)
		 .window(8)
		 .flatMapSingle(obs -> obs.reduce("", (total, next) -> total + (total.equals("") ? "" : "|") + next))
				  .subscribe(System.out::println);
	}

	private static void usoBuffer2() {
		// TODO Auto-generated method stub
		Observable.interval(300, TimeUnit.MILLISECONDS).map(i -> (i + 1) * 300) // map to elapsed time
				.buffer(1, TimeUnit.SECONDS).subscribe(System.out::println);
		sleep(5000);
	}

	private static void usoBuffer1() {
		// TODO Auto-generated method stub
		Observable<Integer> observable = Observable.just(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

		observable.subscribeOn(Schedulers.io()).delay(2, TimeUnit.SECONDS, Schedulers.io()).buffer(3)
				.subscribe(new Observer<List<Integer>>() {
					@Override
					public void onSubscribe(Disposable d) {
						System.out.println("Suscrito");
					}

					@Override
					public void onNext(List<Integer> integers) {
						System.out.println("onNext: ");
						for (Integer value : integers) {
							System.out.println(value);
						}
					}

					@Override
					public void onError(Throwable e) {
						System.out.println("Error");
					}

					@Override
					public void onComplete() {
						System.out.println("Hecho! ");
					}
				});
		sleep(3000);
	}

	public static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
