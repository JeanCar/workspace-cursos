package org.ejemplos2;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class LauncherReplay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS).replay().autoConnect();
		// Observer 1
		seconds.subscribe(l -> System.out.println("Observer 1: " + l));
		sleep(3000);
		// Observer 2
		seconds.subscribe(l -> System.out.println("Observer 2: " + l));
		sleep(3000);
	}

	public static void sleep(long valor) {
		try {
			Thread.sleep(valor);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
