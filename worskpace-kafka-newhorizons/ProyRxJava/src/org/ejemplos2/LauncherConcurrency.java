package org.ejemplos2;

import java.time.LocalTime;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LauncherConcurrency {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// usoConc1();
		// usoConc2();
		// usoConc3();
		// usoParal1();
		//usoParal2();
		usoParal3();
	}

	private static void usoParal3() {
		// TODO Auto-generated method stub
		Disposable d = Observable.interval(1, TimeUnit.SECONDS)
				.doOnDispose(() -> System.out.println("Disposing en hilo " + Thread.currentThread().getName()))
				.unsubscribeOn(Schedulers.io()).subscribe(i -> System.out.println("Recibido " + i));
		sleep(3000);
		d.dispose();
		sleep(3000);
	}

	private static void usoParal2() {
		// TODO Auto-generated method stub
		Disposable d = Observable.interval(1, TimeUnit.SECONDS)
				.doOnDispose(() -> System.out.println("Disposing en hilo " + Thread.currentThread().getName()))
				.subscribe(i -> System.out.println("Recibido " + i));
		sleep(3000);
		d.dispose();
		sleep(3000);
	}

	private static void usoParal1() {
		// TODO Auto-generated method stub
		int coreCount = Runtime.getRuntime().availableProcessors();
		AtomicInteger assigner = new AtomicInteger(0);
		Observable.range(1, 10).groupBy(i -> assigner.incrementAndGet() % coreCount)
				.flatMap(grp -> grp.observeOn(Schedulers.io()).map(i2 -> intenseCalculation(i2)))
				.subscribe(i -> System.out.println(
						"Received " + i + " " + LocalTime.now() + " en hilo " + Thread.currentThread().getName()));
		sleep(20000);
	}

	private static void usoConc3() {
		// TODO Auto-generated method stub
		Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon").subscribeOn(Schedulers.computation())
				.map(LauncherConcurrency::intenseCalculation)
				.blockingSubscribe(System.out::println, Throwable::printStackTrace, () -> System.out.println("Hecho!"));
	}

	private static void usoConc2() {
		// TODO Auto-generated method stub
		Observable<String> fuente1 = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
				.subscribeOn(Schedulers.computation()).map(s -> intenseCalculation((s)));
		Observable<Integer> fuente2 = Observable.range(1, 6).subscribeOn(Schedulers.computation())
				.map(s -> intenseCalculation((s)));
		Observable.zip(fuente1, fuente2, (s, i) -> s + "-" + i).subscribe(System.out::println);
		sleep(20000);
	}

	private static void usoConc1() {
		// TODO Auto-generated method stub
		Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon").subscribeOn(Schedulers.computation())
				.map(s -> intenseCalculation(s)).subscribe(System.out::println);

		Observable.range(1, 6).subscribeOn(Schedulers.computation()).map(s -> intenseCalculation(s))
				.subscribe(System.out::println);
		sleep(20000);
	}

	public static <T> T intenseCalculation(T value) {
		sleep(ThreadLocalRandom.current().nextInt(3000));
		return value;
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
