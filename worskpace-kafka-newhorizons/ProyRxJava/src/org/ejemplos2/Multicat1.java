package org.ejemplos2;

import java.util.concurrent.ThreadLocalRandom;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

public class Multicat1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sinMulti();
		conMulti();
		conMultiMap();
		noMulti();
		conAuto();
	}

	private static void conAuto() {
		// TODO Auto-generated method stub
		Observable<Integer> threeRandoms = Observable. range(1, 3)
				 . map(i -> randomInt())
				 . publish()
				 . autoConnect(2);
				 //Observer 1 - print each random integer
				 threeRandoms. subscribe(i -> System. out. println("Observer auto 1: " + i));
				 //Observer 2 - sum the random integers, then print
				 threeRandoms. reduce(0, (total, next) -> total + next)
				 . subscribe(i -> System. out. println("Observer auto 2: " + i));
				 
				//Observer 3 - receives nothing
				 threeRandoms. subscribe(i -> System.out.println("Observer auto 3: " + i));
				 }
	

	private static void noMulti() {
		// TODO Auto-generated method stub
		ConnectableObservable<Integer> threeRandoms = Observable. range(1, 3).map(i -> randomInt()). publish();
				 //Observer 1 - print each random integer
				 threeRandoms. subscribe(i -> System. out. println("Observer 1: " + i));
				 //Observer 2 - sum the random integers, then print
				 threeRandoms. reduce(0, (total, next) -> total + next).subscribe(i -> System. out. println("Observer 2: " + i));
				 threeRandoms. connect();
	}

	private static void conMultiMap() {
		// TODO Auto-generated method stub
		ConnectableObservable<Integer> threeRandoms = Observable. range(1, 3). map(i -> randomInt()). publish();
				 threeRandoms. subscribe(i -> System. out. println("Observer con map 1: " + i));
				 threeRandoms. subscribe(i -> System. out. println("Observer con map 2: " + i));
				 threeRandoms. connect();
	}

	private static void conMulti() {
		// TODO Auto-generated method stub
		ConnectableObservable<Integer> threeInts = Observable. range(1, 3). publish();
				 Observable<Integer> threeRandoms = threeInts. map(i -> randomInt());
				 threeRandoms. subscribe(i -> System. out. println("Observer 3: " + i));
				 threeRandoms. subscribe(i -> System. out. println("Observer 4: " + i));
				 threeInts. connect();
	}

	private static void sinMulti() {
		// TODO Auto-generated method stub
		Observable<Integer> threeRandoms = Observable.range(1, 3).map(i -> randomInt());
		threeRandoms.subscribe(i -> System.out.println("Observer 1: " + i));
		threeRandoms.subscribe(i -> System.out.println("Observer 2:" + i));
	}

	public static int randomInt() {
		return ThreadLocalRandom.current().nextInt(100000);
	}

}
