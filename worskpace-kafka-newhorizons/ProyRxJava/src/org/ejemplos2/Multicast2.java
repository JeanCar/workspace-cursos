package org.ejemplos2;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.subjects.AsyncSubject;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import io.reactivex.subjects.UnicastSubject;

public class Multicast2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// usoCache();
		// usoPublish();
		// noSubject();
		// usoBehavior();
		// usoReplay();
		// usoAsync();
		usoUniCast();
	}

	private static void usoUniCast() {
		// TODO Auto-generated method stub
//		ejemplo 1
		Subject<String> subject = UnicastSubject.create();
		Observable.interval(300, TimeUnit.MILLISECONDS).map(l -> ((l + 1) * 300) + " milisegundos").subscribe(subject);
		sleep(2000);
		subject.subscribe(s -> System.out.println("Observer Unicast1: " + s));
		sleep(2000);
		
	// ejemplo2	

		Subject<String> subject5 = UnicastSubject.create();
		Observable.interval(300, TimeUnit.MILLISECONDS).map(l -> ((l + 1) * 300) + " milisegundos").subscribe(subject5);
		sleep(2000);
		// multicast to support multiple Observers
		Observable<String> multicast = subject5.publish().autoConnect();
		// bring in first Observer
		multicast.subscribe(s -> System.out.println("Observer Unic2: " + s));
		sleep(2000);
		// bring in second Observer
		multicast.subscribe(s -> System.out.println("Observer Unic3: " + s));
		sleep(1000);

	}

	private static void usoAsync() {
		// TODO Auto-generated method stub
		Subject<String> subject4 = AsyncSubject.create();
		subject4.subscribe(s -> System.out.println("Observer Async1: " + s), Throwable::printStackTrace,
				() -> System.out.println("Observer Async1 ejecutado!"));
		subject4.onNext("Alpha");
		subject4.onNext("Beta");
		subject4.onNext("Gamma");
		subject4.onComplete();
		subject4.subscribe(s -> System.out.println("Observer Async2: " + s), Throwable::printStackTrace,
				() -> System.out.println("Observer Async2 ejecutado!"));
	}

	private static void usoReplay() {
		// TODO Auto-generated method stub
		Subject<String> subject = ReplaySubject.create();
		subject.subscribe(s -> System.out.println("Observer 1: " + s));
		subject.onNext("Alpha");
		subject.onNext("Beta");
		subject.onNext("Gamma");
		subject.onComplete();
		subject.subscribe(s -> System.out.println("Observer 2: " + s));
	}

	private static void usoBehavior() {
		// TODO Auto-generated method stub
		Subject<String> subject3 = BehaviorSubject.create();
		subject3.subscribe(s -> System.out.println("Observer Behavior1: " + s));
		subject3.onNext("Alpha");
		subject3.onNext("Beta");
		subject3.onNext("Gamma");
		subject3.subscribe(s -> System.out.println("Observer Behavior2: " + s));
	}

	private static void noSubject() {
		// TODO Auto-generated method stub
		Subject<String> subject2 = PublishSubject.create();
		subject2.onNext("Alpha");
		subject2.onNext("Beta");
		subject2.onNext("Gamma");
		subject2.onComplete();
		subject2.map(String::length).subscribe(System.out::println); // los datos se perdieron

	}

	private static void usoPublish() {
		// TODO Auto-generated method stub
		Subject<String> subject = PublishSubject.create();
		subject.map(String::length).subscribe(System.out::println);
		subject.onNext("Alpha");
		subject.onNext("Beta");
		subject.onNext("Gamma");
		subject.onComplete();

		Observable<String> source1 = Observable.interval(1, TimeUnit.SECONDS).map(l -> (l + 1) + " segundos");
		Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS)
				.map(l -> ((l + 1) * 300) + " milisegundos");
		Subject<String> subject1 = PublishSubject.create();
		subject1.subscribe(System.out::println);
		source1.subscribe(subject1);
		source2.subscribe(subject1);
		sleep(3000);
	}

	private static void usoCache() {
		// TODO Auto-generated method stub
		Observable<Integer> cachedRollingTotals = Observable.just(6, 2, 5, 7, 1, 4, 9, 8, 3)
				.scan(0, (total, next) -> total + next)
				// . cache();
				.cacheWithInitialCapacity(9);
		cachedRollingTotals.subscribe(System.out::println);

	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
