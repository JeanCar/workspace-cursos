package org.ejemplos2;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class LauncherBasico1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		usoTake1();
		usoTake2();
		/*
		 * usoSkip(); usoTakeWhile(); usoDistinct(); usoUntilChanged(); usoElement();
		 * usoStartWidth(); usoDelay(); usoScan(); usoCount(); usoReduce();
		 * usoContains(); usoToMap();
		 */
	}

	private static void usoToMap() {
		// TODO Auto-generated method stub
		Observable. just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
				 . toMap(s -> s. charAt(0))
				 . subscribe(s -> System. out. println("Tuplas Generadas: " + s));
		
		Observable. just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
				 . toMap(s -> s. charAt(0), String:: length)
				 . subscribe(s -> System. out. println("Tuplas: " + s));
		
	 }
	

	private static void usoContains() {
		// TODO Auto-generated method stub
		Observable. range(1, 10000)
		 . contains(9563)
		 . subscribe(s -> System. out. println("Contiene: " + s));
	}

	private static void usoReduce() {
		// TODO Auto-generated method stub
		Observable. just(5, 3, 7, 10, 2, 14)
		 . reduce((total, next) -> total + next)
		 . subscribe(s -> System. out. println("Acumulado: " + s));
		
		Observable. just(5, 3, 7, 10, 2, 14)
		 . reduce("", (total, next) -> total + (total. equals("") ? "" : ",") + next)
		 . subscribe(s -> System. out. println("Recibidos de Reduce: " + s));
		 
	}

	private static void usoCount() {
		// TODO Auto-generated method stub
		Observable. just("Alpha", "Beta", "Gamma", "Delta","Epsilon")
				 . count()
				 . subscribe(s -> System. out. println("Count: " + s));
			
	}

	private static void usoScan() {
		// TODO Auto-generated method stub
		Observable. just(5, 3, 7, 10, 2, 14)
		 . scan((accumulator, next) -> accumulator + next)
		 . subscribe(s -> System. out. println("Received Scan1: " + s));
		
		Observable. just("Alpha", "Beta", "Gamma", "Delta","Epsilon")
				 . scan(0, (total, next) -> total + 1)
				 . subscribe(s -> System. out. println("Received Scan2: " + s));
				 }
	

	private static void usoDelay() {
		// TODO Auto-generated method stub
		Observable. just("Alpha", "Beta", "Gamma" , "Delta","Epsilon")
				 . delay(3, TimeUnit. SECONDS)
				 . subscribe(s -> System. out. println("Received Delay: " + s));
				 sleep(5000);
	}

	private static void usoStartWidth() {
		// TODO Auto-generated method stub
		Observable<String> menu1 = 	 Observable. just("Coffee", "Tea", "Espresso", "Latte");
				//print menu
				 menu1. startWith("MENU CAFETERIA")
				 . subscribe(System. out:: println);
				 
				 Observable<String> menu2 =
						 Observable. just("Coffee", "Tea", "Espresso", "Latte");
						 //print menu
						 menu2. startWithArray("MENU CAFETERIA", "----------------")
						 . subscribe(System. out:: println);
	}

	private static void usoElement() {
		// TODO Auto-generated method stub
		Observable. just("Alpha", "Beta", "Zeta", "Eta", "Gamma", "Delta")
				 . elementAt(3)
				 . subscribe(i -> System. out. println("RECEIVED8: " + i));
	}

	private static void usoUntilChanged() {
		// TODO Auto-generated method stub
		Observable. just(1, 1, 1, 2, 2, 3, 3, 2, 1, 1)
		 . distinctUntilChanged()
		 . subscribe(i -> System. out. println("RECEIVED7: " + i));
	}

	private static void usoDistinct() {
		// TODO Auto-generated method stub
		Observable. just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
				 . distinct(String:: length)
				 . subscribe(i -> System. out. println("RECEIVED6: " + i));
	}

	private static void usoTakeWhile() {
		// TODO Auto-generated method stub
		Observable. range(1, 100)
		 . takeWhile(i -> i < 5)
		 . subscribe(i -> System. out. println("RECEIVED4: " + i));
		
		Observable. range(1, 100)
		 . skipWhile(i -> i <= 95)
		 . subscribe(i -> System. out. println("RECEIVED5: " + i));
		 
	}

	private static void usoSkip() {
		// TODO Auto-generated method stub
		Observable. range(1, 100)
		 . skip(90)
		 . subscribe(i -> System. out. println("RECEIVED3: " + i));
	}

	private static void usoTake2() {
		// TODO Auto-generated method stub
		Observable.interval(300, TimeUnit.MILLISECONDS).take(2, TimeUnit.SECONDS)
				.subscribe(i -> System.out.println("RECEIVED2: " + i));
		sleep(5000);
	}

	private static void usoTake1() {
		// TODO Auto-generated method stub
		Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon").take(3)
				.subscribe(s -> System.out.println("RECEIVED1: " + s));
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
