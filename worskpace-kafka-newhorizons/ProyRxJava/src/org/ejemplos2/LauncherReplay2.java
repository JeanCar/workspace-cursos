package org.ejemplos2;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class LauncherReplay2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		codigo1();
		codigo2();
		codigo3();
	}

	private static void codigo3() {
		// TODO Auto-generated method stub
		Observable<Long> seconds = Observable.interval(300, TimeUnit.MILLISECONDS).map(l -> (l + 1) * 300) // map to
																											// elapsed
																											// milliseconds
				.replay(1, TimeUnit.SECONDS).autoConnect();
		// Observer 1
		seconds.subscribe(l -> System.out.println("Observer interval1: " + l));
		sleep(2000);
		seconds.subscribe(l -> System.out.println("Observer interval2: " + l));
		sleep(1000);
	}

	private static void codigo2() {
		// TODO Auto-generated method stub
		Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon").replay(1).refCount();
		// Observer 1
		source.subscribe(l -> System.out.println("Observer refcount3: " + l));
		// Observer 2
		source.subscribe(l -> System.out.println("Observer refcount4: " + l));
	}

	private static void codigo1() {
		// TODO Auto-generated method stub
		Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon").replay(1)
				.autoConnect();
		// Observer 1
		source.subscribe(l -> System.out.println("Observer autoconnect 1: " + l));
		// Observer 2
		source.subscribe(l -> System.out.println("Observer autoconnect 2: " + l));
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
