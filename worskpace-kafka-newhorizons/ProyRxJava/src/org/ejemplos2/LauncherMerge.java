package org.ejemplos2;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class LauncherMerge {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		merge1();
		merge2();
		merge3();
	}

	private static void merge3() {
		// TODO Auto-generated method stub
		// emit every second
		Observable<String> source1 = Observable.interval(1, TimeUnit.SECONDS).map(l -> l + 1) // emit elapsed seconds
				.map(l -> "Source1: " + l + " seconds");
		// emit every 300 milliseconds
		Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS).map(l -> (l + 1) * 300) // emit
																												// elapsed
																												// milliseconds
				.map(l -> "Source2: " + l + " milliseconds");
		// merge and subscribe
		Observable.merge(source1, source2).subscribe(System.out::println);
		// keep alive for 3 seconds
		sleep(3000);
	}

	private static void merge2() {
		// TODO Auto-generated method stub
		Observable<String> source1 = Observable.just("Alpha", "Beta");
		Observable<String> source2 = Observable.just("Gamma", "Delta");
		Observable<String> source3 = Observable.just("Epsilon", "Zeta");
		Observable<String> source4 = Observable.just("Eta", "Theta");
		Observable<String> source5 = Observable.just("Iota", "Kappa");
		Observable.mergeArray(source1, source2, source3, source4, source5)
				.subscribe(i -> System.out.println("REUNIDOS: " + i));
	}

	private static void merge1() {
		// TODO Auto-generated method stub
		Observable<String> source1 = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
		Observable<String> source2 = Observable.just("Zeta", "Eta", "Theta");
		Observable.merge(source1, source2).subscribe(i -> System.out.println("RECEIVED merge: " + i));

		source1.mergeWith(source2).subscribe(i -> System.out.println("RECEIVED mergeWidth: " + i));
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
