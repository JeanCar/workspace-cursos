package org.ejemplos2;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class LauncherBackPreesure {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//usoBack1();
		usoBack2();
	}

	private static void usoBack2() {
		// TODO Auto-generated method stub
		Flowable.range(1, 1000).doOnNext(s -> System.out.println("Source pushed " + s)).observeOn(Schedulers.io())
				.map(i -> intenseCalculation(i)).subscribe(new Subscriber<Integer>() {
					Subscription subscription;
					AtomicInteger count = new AtomicInteger(0);

					@Override
					public void onSubscribe(Subscription subscription) {
						this.subscription = subscription;
						System.out.println("Requesting 40 items!");
						subscription.request(40);
					}

					@Override
					public void onNext(Integer s) {
						sleep(450);
						System.out.println("Subscriber received " + s);
						if (count.incrementAndGet() % 20 == 0 && count.get() >= 40)
							System.out.println("Requesting 20 more!");
						subscription.request(20);
					}

					@Override
					public void onError(Throwable e) {
						e.printStackTrace();
					}

					@Override
					public void onComplete() {
						System.out.println("Done!");
					}
				});
		sleep(20000);

	}

	public static <T> T intenseCalculation(T value) {
		// sleep up to 200 milliseconds
		sleep(ThreadLocalRandom.current().nextInt(200));
		return value;
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void usoBack1() {
		// TODO Auto-generated method stub
		Flowable.range(1, 99999999).map(MyItem::new).observeOn(Schedulers.io()).subscribe(myItem -> {
			sleep(350);
			System.out.println("Received MyItem " + myItem.id);
		});
		sleep(Long.MAX_VALUE); // valor: 9,223,372,036,854,775,807
	}

	static final class MyItem {
		final int id;

		MyItem(int id) {
			this.id = id;
			System.out.println("Construyendo MyItem " + id);
		}
	}
}
