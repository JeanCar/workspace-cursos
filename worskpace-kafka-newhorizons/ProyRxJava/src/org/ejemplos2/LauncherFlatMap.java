package org.ejemplos2;

import io.reactivex.Observable;
import java.util.concurrent.TimeUnit;

public class LauncherFlatMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		usoFlat1();
		usoFlat2();
		usoFlat3();
	}

	private static void usoFlat3() {
		// TODO Auto-generated method stub
		Observable<Integer> intervalArguments = Observable.just(2, 3, 10, 7);
		
		  intervalArguments .flatMap(i -> Observable.interval(i, TimeUnit.SECONDS)
		  .map(i2 -> i + "s interval: " + ((i + 1) * i) +  " segundos transcurridos")).subscribe(System.out::println);
		sleep(12000);
	}

	private static void usoFlat2() {
		// TODO Auto-generated method stub
		Observable<String> source = Observable.just("521934/2342/FOXTROT", "21962/12112/78886/TANGO",
				"283242/4542/WHISKEY/2348562");
		source.flatMap(s -> Observable.fromArray(s.split("/"))).filter(s -> s.matches("[0-9]+")) // use regex to filter
																									// integers
				.map(Integer::valueOf).subscribe(System.out::println);
	}

	private static void usoFlat1() {
		// TODO Auto-generated method stub
		Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
		source.flatMap(s -> Observable.fromArray(s.split(""))).subscribe(System.out::println);

	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
