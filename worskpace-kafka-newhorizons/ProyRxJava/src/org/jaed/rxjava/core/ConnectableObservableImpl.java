package org.jaed.rxjava.core;


import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

public class ConnectableObservableImpl {

    public static void main(String[] args) throws InterruptedException {

        ConnectableObservable<Long> connectable
          = Observable.interval(200, TimeUnit.MILLISECONDS).publish();
        connectable.subscribe(System.out::println);

        System.out.println("Connect");
        connectable.connect();

        Thread.sleep(500);
        System.out.println("Sleep");
    }
}
