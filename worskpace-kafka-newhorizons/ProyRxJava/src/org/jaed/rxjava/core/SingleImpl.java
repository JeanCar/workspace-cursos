package org.jaed.rxjava.core;

import io.reactivex.Observable;
import io.reactivex.Single;

public class SingleImpl {

    public static void main(String[] args) {

        Single<String> single = Single.just("Hola")
          .doOnSuccess(System.out::print)
          .doOnError(e -> {
              throw new RuntimeException(e.getMessage());
          });
        
             
        single.subscribe();
    }
}
