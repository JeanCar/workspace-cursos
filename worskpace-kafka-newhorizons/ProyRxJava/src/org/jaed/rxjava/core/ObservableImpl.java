package org.jaed.rxjava.core;

import io.reactivex.Observable;
import io.reactivex.observables.BlockingObservable;

import java.util.Arrays;
import java.util.List;

public class ObservableImpl {

    private static Integer[] numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    private static String[] letters = {"a", "b", "c", "d", "e", "f", "g", "h", "i"};
    private static String[] titles = {"title"};
    private static List<String> titleList = Arrays.asList(titles);

    static Observable<String> getTitle() {
        return Observable.fromIterable(titleList);
    }

    public static void main(String[] args) {

        System.out.println("-------Just-----------");
        Observable<String> observable = Observable.just("Hello");
        observable.subscribe(
          System.out::println, //onNext
          Throwable::printStackTrace, //onError
          () -> System.out.println("onCompleted")  //onCompleted
        );

        Observable<String> blockingObservable = observable.blockingIterable(bufferSize);

        System.out.println();
        System.out.println("-------Map-----------");
        Observable.fromArray(letters)
          .map(String::toUpperCase)
          .subscribe(System.out::print);

        System.out.println();
        System.out.println("-------FlatMap-----------");
        Observable.just("book1", "book2")
          .flatMap(s -> getTitle())
          .subscribe(System.out::print);

        System.out.println();
        System.out.println("--------Scan----------");
        Observable.fromArray(letters)
          .scan(new StringBuilder(), StringBuilder::append)
          .subscribe(System.out::println);

        System.out.println();
        System.out.println("------GroubBy------------");
        Observable.fromArray(numbers)
          .groupBy(i -> 0 == (i % 2) ? "EVEN" : "ODD")
          .subscribe((group) -> group.subscribe((number) -> {
              System.out.println(group.getKey() + " : " + number);
          }));

        System.out.println();
        System.out.println("-------Filter-----------");
        Observable.fromArray(numbers)
          .filter(i -> (i % 2 == 1))
          .subscribe(System.out::println);

        System.out.println("------DefaultIfEmpty------------");
        Observable.empty()
          .defaultIfEmpty("Observable is empty")
          .subscribe(System.out::println);

        System.out.println("------DefaultIfEmpty-2-----------");
        Observable.fromArray(letters)
          .defaultIfEmpty("Observable is empty")
          .first()
          .subscribe(System.out::println);

        System.out.println("-------TakeWhile-----------");
        Observable.fromArray(numbers)
          .takeWhile(i -> i < 5)
          .subscribe(System.out::println);
    }
}