package com.nh.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnegociosReactivaWebfluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnegociosReactivaWebfluxApplication.class, args);
	}

}
