package com.nh.webflux.business.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.nh.webflux.business.PlaylistEntry;
import reactor.core.publisher.Flux;



@Repository
public class PlaylistEntryRepository {

    private static final String QUERY_FIND_ALL_PLAYLIST_ENTRIES =
            "SELECT p.PlaylistId as 'playlistID', " +
            "       p.Name as 'playlistName', " +
            "       t.Name as 'trackName', " +
            "       ar.Name as 'artistName', " +
            "       a.title as 'albumTitle' " +
            "FROM playlist p, PlaylistTrack pt, track t, Album a, Artist ar " +
            "WHERE p.PlaylistId = pt.PlaylistId AND " +
            "      pt.TrackId = t.TrackId AND " +
            "      t.AlbumId = a.AlbumId AND " +
            "      a.ArtistId = ar.ArtistId LIMIT 10";


    private JdbcTemplate jdbcTemplate;


    public PlaylistEntryRepository() {
        super();
    }


    @Autowired
    public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<PlaylistEntry> findAllPlaylistEntries() {

        return this.jdbcTemplate.query(
                QUERY_FIND_ALL_PLAYLIST_ENTRIES,
                (resultSet, i) -> {
                    return new PlaylistEntry(
                            Integer.valueOf(resultSet.getInt("playlistID")),
                            resultSet.getString("playlistName"),
                            resultSet.getString("trackName"),
                            resultSet.getString("artistName"),
                            resultSet.getString("albumTitle"));
                });

    }


    public Flux<PlaylistEntry> findLargeCollectionPlaylistEntries() {

    	List<PlaylistEntry> listado = this.jdbcTemplate.query(
                QUERY_FIND_ALL_PLAYLIST_ENTRIES,
                (resultSet, i) -> {
                	return new PlaylistEntry(
                            Integer.valueOf(resultSet.getInt("playlistID")),
                            resultSet.getString("playlistName"),
                            resultSet.getString("trackName"),
                            resultSet.getString("artistName"),
                            resultSet.getString("albumTitle"));
                });
        return Flux.fromIterable(listado);

    }

}
