package com.nh.kafka.controller;

import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.nh.kafka.model.Model;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;

@RestController
public class MultController {

	@Autowired
	private ReplyingKafkaTemplate<String, Model, Model> kafkaTemplate;

	@Value("${spring.kafka.topic.request-topic2}")
	String requestTopic;

	@Value("${spring.kafka.topic.requestreplay-topic2}")
	String requestReplayTopic;

	@PostMapping(value = "/mult", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Model mult(@RequestBody Model request) throws InterruptedException, ExecutionException {

		ProducerRecord<String, Model> record = new ProducerRecord<String, Model>(requestTopic, request);
		record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, requestReplayTopic.getBytes()));
		RequestReplyFuture<String, Model, Model> sendAndReceive = kafkaTemplate.sendAndReceive(record);
		SendResult<String, Model> sendResult = sendAndReceive.getSendFuture().get();
		sendResult.getProducerRecord().headers()
				.forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));
		ConsumerRecord<String, Model> consumerRecord = sendAndReceive.get();
		return consumerRecord.value();

	}

}
