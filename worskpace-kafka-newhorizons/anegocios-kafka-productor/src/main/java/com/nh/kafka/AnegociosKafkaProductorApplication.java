package com.nh.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class AnegociosKafkaProductorApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnegociosKafkaProductorApplication.class, args);
	}

}
