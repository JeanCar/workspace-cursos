package com.nh.kafka.producer;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.nh.kafka.model.Model;

import org.springframework.kafka.support.serializer.JsonDeserializer;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SenderConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;
	@Value("${spring.kafka.topic.requestreplay-topic}")
	private String requestReplayTopic;
	@Value("${spring.kafka.topic.requestreplay-topic2}")
	private String requestReplayTopic2;	
	@Value("${spring.kafka.consumer.group-id}")
	private String consumerGroup;
	
	@Bean
	public Map<String, Object> producerConfigs() {
	    Map<String, Object> props = new HashMap<>();
	    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
	    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
	    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
	    return props;
	}
	 
	@Bean
	public ProducerFactory<String, Model> producerFactory() {
	    return new DefaultKafkaProducerFactory<>(producerConfigs());
	}
	 
	@Bean
	public KafkaTemplate<String, Model> kafkaTemplate() {
	    return new KafkaTemplate<>(producerFactory());
	}

	@Bean
	public Map<String, Object> consumerConfigs() {
	    Map<String, Object> props = new HashMap<>();
	    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
	    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
	    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
	    props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroup);
	    return props;
	}
	@Bean
	public ReplyingKafkaTemplate<String, Model, Model> replyKafkaTemplate(ProducerFactory<String,Model> pf,
			    KafkaMessageListenerContainer<String, Model> container) {
	    return new ReplyingKafkaTemplate<>(pf, container);
	}
	
	@Bean
	public KafkaMessageListenerContainer<String, Model> replyContainer(ConsumerFactory<String, Model> cf) {
		ContainerProperties containerProperties = new ContainerProperties(requestReplayTopic,requestReplayTopic2);
		return new KafkaMessageListenerContainer<>(cf,containerProperties);
	}
	 
	@Bean
	public ConsumerFactory<String, Model> consumerFactory() {
		JsonDeserializer<Model> deserializer = new JsonDeserializer(Model.class);
		deserializer.setRemoveTypeHeaders(false);
		deserializer.addTrustedPackages("*");
		deserializer.setUseTypeMapperForKey(true);
	    return new DefaultKafkaConsumerFactory<>(consumerConfigs(),
	    		 new StringDeserializer(),deserializer);
	}
	

    @Bean
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, Model>> 
                                             kafkaListenerContainerFactory(){
        ConcurrentKafkaListenerContainerFactory<String, Model> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setReplyTemplate(kafkaTemplate());
        return factory;
    }

	@Bean
	public Sender sender() {
	    return new Sender();
	}
	 
}
