package com.example.demo.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.domain.Word;
import com.example.demo.service.SentenceService;

@RestController
public class SentenceController {
	
	@Autowired
	private SentenceService sentenceService;
	
	@RequestMapping("/sentence")
	public @ResponseBody String getSentence() {
	    return sentenceService.buildSentence();
	}
	
	/*
	
	@Autowired
	//private DiscoveryClient client;
	private LoadBalancerClient client;
	
	@RequestMapping("/sentence")
	public @ResponseBody String getSentence() {
	    return 
	      getWord("spring-cloud-ms-subject").getText() + " "
	      + getWord("spring-cloud-ms-verb").getText() + " "
	      + getWord("spring-cloud-ms-article").getText() + " "
	      + getWord("spring-cloud-ms-adjective").getText() + " "
	      + getWord("spring-cloud-ms-noun").getText() + "."
	      ;
	}

	public Word getWord(String service) {
		ServiceInstance serviceInstance = client.choose(service);
		//if (list != null && list.size() > 0) {
			URI uri = serviceInstance.getUri();
			if (uri != null) {
				return (new RestTemplate()).getForObject(uri, Word.class);
			}
		//}
		return null;
	}
	
	*/
}
