package com.example.demo.domain;

public class Word {
	private String text;
	
	public Word() {
	}
	
	public Word(String text) {
		this.text = text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}
