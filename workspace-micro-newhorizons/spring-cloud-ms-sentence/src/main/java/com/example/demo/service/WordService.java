package com.example.demo.service;

import com.example.demo.domain.Word;

public interface WordService {
	public Word getSubject();
	public Word getVerb();
	public Word getAdjective();
	public Word getNoun();
	public Word getArticle();	
}

