package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class Word {

  private String text;

  public void setText(String text) {
      this.text = text;
  }
  
  public String getText() {
      return text;
  }

}
