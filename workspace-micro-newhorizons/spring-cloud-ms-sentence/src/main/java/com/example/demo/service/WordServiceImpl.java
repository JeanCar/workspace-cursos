package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AdjectiveClient;
import com.example.demo.dao.ArticleClient;
import com.example.demo.dao.NounClient;
import com.example.demo.dao.SubjectClient;
import com.example.demo.dao.VerbClient;
import com.example.demo.domain.Word;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class WordServiceImpl implements WordService{
	
	//@Autowired
	//private LoadBalancerClient client;
	
	@Autowired
	private SubjectClient subjectClient;
	
	@Autowired
	private VerbClient verbClient;
	
	@Autowired
	private ArticleClient articleClient;
	
	@Autowired
	private AdjectiveClient adjectiveClient;
	
	@Autowired
	private NounClient nounClient;
	
	
	@Override
	public Word getSubject() {
		return subjectClient.getWord();
	}

	@Override
	public Word getVerb() {
		return verbClient.getWord() ;
	}

	@Override
	@HystrixCommand(fallbackMethod = "getFallbackAdjective")
	public Word getAdjective() {
		return adjectiveClient.getWord() ;
	}
	
	public Word getFallbackAdjective() {
		return new Word("-") ;
	}
	
	

	@Override
	@HystrixCommand(fallbackMethod = "getFallbackNoun")
	public Word getNoun() {
		return nounClient.getWord() ;
	}
	
	public Word getFallbackNoun() {
		return new Word("-") ;
	}

	@Override
	public Word getArticle() {
		return articleClient.getWord() ;
	}
/*	
	public Word getWord(String service) {
		ServiceInstance serviceInstance = client.choose(service);
		//if (list != null && list.size() > 0) {
			URI uri = serviceInstance.getUri();
			if (uri != null) {
				return (new RestTemplate()).getForObject(uri, Word.class);
			}
		//}
		return null;
	}
*/
}
