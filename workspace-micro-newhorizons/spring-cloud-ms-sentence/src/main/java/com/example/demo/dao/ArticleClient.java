package com.example.demo.dao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.domain.Word;

@FeignClient("spring-cloud-ms-article")
public interface ArticleClient {
	
	@RequestMapping(value="/",method = RequestMethod.GET)
	public Word getWord();
}

