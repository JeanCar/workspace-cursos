package com.example.demo;

public class Word {
	private String text;
	
	public Word(String text) {
		super();
		this.text = text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}

