package com.example.demo;

import static com.example.demo.utils.CseCourierConfigurations.CSE_LOGIN;
import static com.example.demo.utils.CseCourierConfigurations.CSE_PASSWORD;
import static com.example.demo.utils.LabelCseConstants.BASE_64;

import com.example.demo.exception.CSEApplicationException;
import com.example.demo.model.request.CseRequest;
import com.example.demo.model.response.CourierLabelDataHolder;
import com.example.demo.model.response.LabelResponse;
import com.example.demo.service.PruebaService;
import com.example.demo.utils.LabelCseUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@ComponentScan("com.example.demo")
public class prueba {

  private static AnnotationConfigApplicationContext applicationContext;

  private static String labelTemplate =
      "<g id=\"layer1\" transform=\"matrix(0.99328118,0,0,0.78757559,-0.38185567,0.13568747)\"><image y='0.63875651' x='1.825043' id='image3755' preserveAspectRatio='none' height='100%' width='100%' xlink:href='data:image/jpeg;base64,%BASE64_LABEL%'/></g>";

  private static int nro_pedido = 2;

  public static void main(String[] args) {
    applicationContext = new AnnotationConfigApplicationContext(
        prueba.class);

    PruebaService ejecutar = applicationContext.getBean(PruebaService.class);
    Map<String, String> auth = new HashMap<String, String>();
    auth.put(CSE_LOGIN.name(), "INDITEX");
    auth.put(CSE_PASSWORD.name(), "!LYMfmX7Ahz82");
    try {
      for (int i = 0; i < nro_pedido; i++) {
        CseRequest req = getRequest();
        LabelResponse res = ejecutar.generarLabelCse(req, auth);
        List<CourierLabelDataHolder> resFinal = new ArrayList<>();
        if (res != null) {
          log.info("***************************INICIO PEDIDO****************************");
          res.getListCourier().forEach(etiqueta -> {
            log.info("COURIERBOXREFERENCE: " + etiqueta.getCourierReferences());
            log.info("BASE64: " + etiqueta.getCourierBase64());
            CourierLabelDataHolder dataHolder = new CourierLabelDataHolder();
            replacePlaceHoldersMapper(labelTemplate, etiqueta.getIdCourier(), etiqueta.getItxReferences(),
                etiqueta.getCourierReferences(), etiqueta.getCourierBase64(), dataHolder);
            resFinal.add(dataHolder);
            log.info("-------------------------");
          });
          log.info("***************************FIN PEDIDO****************************");
        }
      }
    } catch (CSEApplicationException e) {
      // TODO Auto-generated catch block
      log.error(e.getMessage());;
    }
  }

  public static CseRequest getRequest() {
    CseRequest request = new CseRequest();
    request.setOrderNumber(generateOrderNumber());
    request.setItxReference(Arrays.asList("215113726766547", "215113726766548"));
    return request;
  }

  private static void replacePlaceHoldersMapper(String labelSvg, Integer order, String itxRef, String courierRef, String base64,
      CourierLabelDataHolder courierLabelDataHolder) {
    Map<String, Object> replacements = new HashMap<>();
    replacements.put(BASE_64, base64);
    courierLabelDataHolder.setOrder(order);
    courierLabelDataHolder.setItxReference(itxRef);
    courierLabelDataHolder.setItxReference(courierRef);
    courierLabelDataHolder.setLabelCode(LabelCseUtil.replacePlaceHolders(labelSvg, replacements));

    log.info("ETIQUETA:" + courierLabelDataHolder.getLabelCode());

  }

  public static String generateOrderNumber() {
    String timeStamp = String.valueOf(System.currentTimeMillis());
    int length = timeStamp.length();
    timeStamp = timeStamp.substring(length - 8);
    log.info("numero de orden: {}", timeStamp);
    return timeStamp;
  }
}
