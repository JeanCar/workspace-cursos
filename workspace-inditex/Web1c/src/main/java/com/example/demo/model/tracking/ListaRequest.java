package com.example.demo.model.tracking;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ListaRequest {

	private String key;
}
