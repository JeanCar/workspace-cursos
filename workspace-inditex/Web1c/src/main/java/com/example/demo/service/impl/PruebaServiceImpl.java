package com.example.demo.service.impl;

import com.example.demo.exception.CSEApplicationException;
import com.example.demo.model.request.CseRequest;
import com.example.demo.model.response.LabelResponse;
import com.example.demo.service.PruebaService;
import com.example.demo.thirdparty.consumo.CreateGMHResponse;
import com.example.demo.thirdparty.consumo.GetFormsForDocumentsResponse;
import com.example.demo.thirdparty.consumo.SaveWaybillOfficeResponse;
import com.example.demo.utils.LabelCseUtil;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
public class PruebaServiceImpl implements PruebaService {

  @Autowired
  LabelCseBuilder labelCseBuilder;

  @Autowired
  PruebaSender labelCseSender;

  @Autowired
  LabelCseProcessor labelCseProcessor;

  private String nroDocumento = "";

  @Override
  public LabelResponse generarLabelCse(CseRequest request, Map<String, String> authentication) throws CSEApplicationException {
    log.debug("[START returnLabelCse]");
    // Label response
    LabelResponse labelResponse = new LabelResponse();

    // Set itx-apiKey in header HTTP
    // this.methodCseService.setHeaderItxApiKey();

    // Get saveWaybillOfficeResponse
    SaveWaybillOfficeResponse saveWaybillOfficeResponse = this.labelCseSender
        .returnSaveWayBillOffice(this.labelCseBuilder.buildSaveRequest(request, authentication));

    // Check instanceof SaveWayBillOfficeError
    if (saveWaybillOfficeResponse.getReturn().getItems() != null) {
      this.nroDocumento = saveWaybillOfficeResponse.getReturn().getItems().get(0).getValue();
    }

    // createGMHResponse
    CreateGMHResponse createGMHResponse = this.labelCseSender
        .returnCreateGMH(this.labelCseBuilder.buildCreateGMHRequest(request, this.nroDocumento, authentication));

    // Get getFormsForDocumentsResponse
    GetFormsForDocumentsResponse getFormsForDocumentsResponse = this.labelCseSender.returnGetFormsForDocuments(
        this.labelCseBuilder.buildGetFormForDocumentRequest(this.nroDocumento, authentication));

    // getFormsForDocumentsResponse.getReturn() always exist, is not null
    if (!CollectionUtils.isEmpty(getFormsForDocumentsResponse.getReturn().getLists())) {
      // Get index 0
      List<byte[]> bdData = getFormsForDocumentsResponse.getReturn().getLists().get(0).getBDatas();
      // Generate Images
      labelResponse = this.labelCseProcessor
          .proccessResponse(LabelCseUtil.generateToBase64(bdData, LabelCseUtil.findCourierReference(createGMHResponse),
              LabelCseUtil.findItxReference(createGMHResponse)));
    }
    // return LabelResponse
    log.debug("[STOP returnLabelCse]");
    return labelResponse;
  }

}
