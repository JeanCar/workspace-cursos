package com.example.demo.model.response;

import java.util.List;
import lombok.Data;
import lombok.ToString;


@Data
@ToString(includeFieldNames = true)
public class LabelResponse {

  private List<LabelCourierResponse> listCourier;
}
