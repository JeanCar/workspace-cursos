package com.example.demo.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.example.demo.thirdparty.consumo.GetFormsForDocuments;
import com.example.demo.thirdparty.consumo.GetFormsForDocumentsResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WSGetFormsForDocumentsTemplate extends WebServiceGatewaySupport {

	public GetFormsForDocumentsResponse callWebService(GetFormsForDocuments request) {
		log.info("Consultando servicio....");
		Object rpta = this.getWebServiceTemplate().marshalSendAndReceive(request);
		log.info("respuesta obtenida....");
		return (GetFormsForDocumentsResponse) rpta;
	}
}
