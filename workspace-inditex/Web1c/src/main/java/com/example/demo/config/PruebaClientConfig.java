package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class PruebaClientConfig {

//	@Autowired
//	ApplicationProperties properties;

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.example.demo.thirdparty.consumo");

		return marshaller;
	}

	@Bean
	public WSSaveWayBillOfficeTemplate soapConnector1(Jaxb2Marshaller marshaller) {
		WSSaveWayBillOfficeTemplate client = new WSSaveWayBillOfficeTemplate();
		client.setDefaultUri("http://web.cse.ru/cargo_api_i/ws/Web1C.1cws");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public WSCreateGmhTemplate soapConnector2(Jaxb2Marshaller marshaller) {
		WSCreateGmhTemplate client = new WSCreateGmhTemplate();
		client.setDefaultUri("http://web.cse.ru/cargo_api_i/ws/Web1C.1cws");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public WSGetFormsForDocumentsTemplate soapConnector3(Jaxb2Marshaller marshaller) {
		WSGetFormsForDocumentsTemplate client = new WSGetFormsForDocumentsTemplate();
		client.setDefaultUri("http://web.cse.ru/cargo_api_i/ws/Web1C.1cws");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}
