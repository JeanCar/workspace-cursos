package com.example.demo.service.impl;

import static com.example.demo.utils.CseCourierConfigurations.CSE_LOGIN;
import static com.example.demo.utils.CseCourierConfigurations.CSE_PASSWORD;
import static com.example.demo.utils.LabelCseConstants.CASE_LABEL_ITX;
import static com.example.demo.utils.LabelCseConstants.CLIENT_CODE;
import static com.example.demo.utils.LabelCseConstants.DOCUMENTS;
import static com.example.demo.utils.LabelCseConstants.DOCUMENT_TYPE;
import static com.example.demo.utils.LabelCseConstants.FLOAT;
import static com.example.demo.utils.LabelCseConstants.FORMAT;
import static com.example.demo.utils.LabelCseConstants.GMH;
import static com.example.demo.utils.LabelCseConstants.HEIGHT;
import static com.example.demo.utils.LabelCseConstants.LENGTH;
import static com.example.demo.utils.LabelCseConstants.NAME;
import static com.example.demo.utils.LabelCseConstants.NUMBER;
import static com.example.demo.utils.LabelCseConstants.PARAMETERS;
import static com.example.demo.utils.LabelCseConstants.PDF;
import static com.example.demo.utils.LabelCseConstants.PRINT;
import static com.example.demo.utils.LabelCseConstants.STRING;
import static com.example.demo.utils.LabelCseConstants.STRING_ZERO;
import static com.example.demo.utils.LabelCseConstants.TYPE;
import static com.example.demo.utils.LabelCseConstants.WAYBILL;
import static com.example.demo.utils.LabelCseConstants.WEIGHT;
import static com.example.demo.utils.LabelCseConstants.WIDTH;

import com.example.demo.model.request.CseRequest;
import com.example.demo.model.request.LabelRequestDto;
import com.example.demo.thirdparty.consumo.Cargo;
import com.example.demo.thirdparty.consumo.CreateGMH;
import com.example.demo.thirdparty.consumo.DestinationAddress;
import com.example.demo.thirdparty.consumo.DestinationInformation;
import com.example.demo.thirdparty.consumo.Element;
import com.example.demo.thirdparty.consumo.GetFormsForDocuments;
import com.example.demo.thirdparty.consumo.ObjectFactory;
import com.example.demo.thirdparty.consumo.Order;
import com.example.demo.thirdparty.consumo.SaveWaybillOffice;
import com.example.demo.utils.LabelCseUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LabelCseBuilder {

  public SaveWaybillOffice buildSaveRequest(CseRequest labelRequestDto, Map<String, String> authentication) {
    LabelCseBuilder.log.debug("[START buildSaveRequest]");
    ObjectFactory fact = new ObjectFactory();

    // Set node DestinationAddress
    DestinationAddress addressRecipients = fact.createDestinationAddress();
    addressRecipients.setGeography("postcode-119192");
    addressRecipients.setInfo("Российская Федерация 119192 Москва Акадика Орина 1 кпус 28, квара 164");
    addressRecipients.setFreeForm(Boolean.TRUE);

    // Set node Cargo
    Cargo cargo = new Cargo();
    cargo.setCargoPackageQty(1);
    cargo.setWeight(0.1f);
    // if (labelRequestDto.getPaymentMethod() != null && labelRequestDto.getPaymentMethod() != 1) {
    cargo.setCOD(Float.parseFloat("70"));
    // } else {
    // cargo.setCOD(Float.parseFloat(STRING_ZERO));
    // }

    // Set node DestinationInformation
    DestinationInformation destinationRecipients = new DestinationInformation();
    destinationRecipients.setAddress(addressRecipients);
    destinationRecipients.setCargo(cargo);
    destinationRecipients.setPhone("79150000428");
    destinationRecipients.setUrgency("18c4f207-458b-11dc-9497-0015170f8c09");
    destinationRecipients.setClient("Яа Шуич");
    destinationRecipients.setOfficial("Яа Шуич");

    // Set node addressSender
    DestinationAddress addressSender = fact.createDestinationAddress();
    addressSender.setGeography("cd2e9c9b-442b-11dc-9497-0015170f8c09");
    addressSender.setFreeForm(Boolean.TRUE);
    addressSender.setInfo("Россия, 141230 Московская обл., г. Пушкино мкр. Клязьма, ул. Костомаровская, д. 5/1");

    // Set node DestinationInformation
    DestinationInformation destinationSender = new DestinationInformation();
    destinationSender.setAddress(addressSender);
    destinationSender.setClient("Clinique");
    destinationSender.setOfficial("Clinique");
    destinationSender.setPhone("8(495)258-41-17");

    // Set node data
    Order data = new Order();
    data.setDeliveryDate(LabelCseUtil.convertGregorianCalendarToString("2020-03-19T00:00:00"));
    data.setRecipients(Arrays.asList(destinationRecipients));
    data.setSender(destinationSender);
    data.setTypeOfCargo("4aab1fc6-fc2b-473a-8728-58bcd4ff79ba");
    data.setTypeOfPayer("0");
    data.setWayOfPayment("1");
    data.setDeliveryOfCargo("0");

    SaveWaybillOffice saveWaybillOffice = fact.createSaveWaybillOffice();

    // Inform authentication from courierConfiguration
    if (authentication != null) {
      // Config Login and Password
      authentication.entrySet().forEach(entry -> {
        if (CSE_LOGIN.name().equalsIgnoreCase(entry.getKey())) {
          saveWaybillOffice.setLogin(entry.getValue());
        }
        if (CSE_PASSWORD.name().equalsIgnoreCase(entry.getKey())) {
          saveWaybillOffice.setPassword(entry.getValue());
        }
      });
    }

    saveWaybillOffice.setClientNumber(labelRequestDto.getOrderNumber());
    saveWaybillOffice.setOrderData(data);
    LabelCseBuilder.log.debug("[STOP buildSaveRequest]");
    return saveWaybillOffice;
  }

  public CreateGMH buildCreateGMHRequest(CseRequest labelRequestDto, String nroDocumento,
      Map<String, String> authentication) {
    LabelCseBuilder.log.debug("[START buildCreateGMHRequest]");
    ObjectFactory fact = new ObjectFactory();
    CreateGMH createGMH = fact.createCreateGMH();

    // Inform authentication from courierConfiguration
    if (authentication != null) {
      authentication.entrySet().forEach(entry -> {
        if (CSE_LOGIN.name().equalsIgnoreCase(entry.getKey())) {
          createGMH.setLogin(entry.getValue());
        }
        if (CSE_PASSWORD.name().equalsIgnoreCase(entry.getKey())) {
          createGMH.setPassword(entry.getValue());
        }
      });
    }

    Element data = new Element();
    data.setKey(GMH);

    final List<Element> listData = labelRequestDto.getItxReference().stream().map(itx -> {
      Element element = new Element();
      element.setKey(itx);

      element.setFields(createFields(itx));
      return element;
    }).collect(Collectors.toList());

    data.setLists(listData);
    createGMH.setData(data);

    // Set node parameters
    Element parameters = new Element();
    parameters.setKey(PARAMETERS);

    final List<Element> listParameters = new ArrayList<>();

    // Set node documentType
    Element documentType = new Element();
    documentType.setKey(DOCUMENT_TYPE);
    documentType.setValue(WAYBILL);
    documentType.setValueType(STRING);

    // Set node number
    Element number = new Element();
    number.setKey(NUMBER);
    number.setValue(nroDocumento);
    number.setValueType(STRING);

    listParameters.add(documentType);
    listParameters.add(number);

    parameters.setLists(listParameters);
    createGMH.setParameters(parameters);
    LabelCseBuilder.log.debug("[STOP buildCreateGMHRequest]");
    return createGMH;
  }

  private static List<Element> createFields(final String itx) {
    final List<Element> fields = new ArrayList<>();

    // Set node height
    Element height = new Element();
    height.setKey(HEIGHT);
    height.setValue(STRING_ZERO);
    height.setValueType(FLOAT);

    // Set node length
    Element length = new Element();
    length.setKey(LENGTH);
    length.setValue(STRING_ZERO);
    length.setValueType(FLOAT);

    // Set node width
    Element width = new Element();
    width.setKey(WIDTH);
    width.setValue(STRING_ZERO);
    width.setValueType(FLOAT);

    // Set node weight
    Element weight = new Element();
    weight.setKey(WEIGHT);
    weight.setValue(STRING_ZERO);
    weight.setValueType(FLOAT);

    // Set node clientCode
    Element clientCode = new Element();
    clientCode.setKey(CLIENT_CODE);
    clientCode.setValue(itx);
    clientCode.setValueType(FLOAT);

    // Set node field
    fields.add(height);
    fields.add(length);
    fields.add(width);
    fields.add(weight);
    fields.add(clientCode);

    // Return fields
    return fields;
  }

  public GetFormsForDocuments buildGetFormForDocumentRequest(String nroDocumento,
      Map<String, String> authentication) {
    LabelCseBuilder.log.debug("[START buildGetFormForDocumentRequest]");
    ObjectFactory fact = new ObjectFactory();
    GetFormsForDocuments getFormsForDocuments = fact.createGetFormsForDocuments();

    // Inform authentication from courierConfiguration
    if (authentication != null) {
      authentication.entrySet().forEach(entry -> {
        // CSE_LOGIN
        if (CSE_LOGIN.name().equalsIgnoreCase(entry.getKey())) {
          getFormsForDocuments.setLogin(entry.getValue());
        }
        // CSE_PASSWORD
        if (CSE_PASSWORD.name().equalsIgnoreCase(entry.getKey())) {
          getFormsForDocuments.setPassword(entry.getValue());
        }
      });
    }

    // Set node listDocument
    Element documents = new Element();
    documents.setKey(DOCUMENTS);

    // Set node listDocument
    final List<Element> listDocument = new ArrayList<>();
    Element document = new Element();
    document.setKey(nroDocumento);
    listDocument.add(document);
    documents.setLists(listDocument);

    // generate documents
    getFormsForDocuments.setDocuments(documents);

    // Set node parameters
    Element parameters = new Element();
    parameters.setKey(PARAMETERS);
    final List<Element> listParameter = new ArrayList<>();

    // Set node documentType
    Element documentType = new Element();
    documentType.setKey(DOCUMENT_TYPE);
    documentType.setValue(WAYBILL);
    documentType.setValueType(STRING);

    // Set node name
    Element name = new Element();
    name.setKey(NAME);
    // CSE label for ITX
    name.setValue(CASE_LABEL_ITX);
    name.setValueType(STRING);

    // Set node type
    Element type = new Element();
    // Type
    type.setKey(TYPE);
    // print
    type.setValue(PRINT);
    // String
    type.setValueType(STRING);

    // Set node name
    Element format = new Element();
    format.setKey(FORMAT);
    format.setValue(PDF);
    format.setValueType(STRING);

    // Set node listParameter
    listParameter.add(documentType);
    listParameter.add(name);
    listParameter.add(type);
    listParameter.add(format);
    parameters.setLists(listParameter);

    getFormsForDocuments.setParameters(parameters);
    LabelCseBuilder.log.debug("[STOP buildGetFormForDocumentRequest]");
    return getFormsForDocuments;
  }

}
