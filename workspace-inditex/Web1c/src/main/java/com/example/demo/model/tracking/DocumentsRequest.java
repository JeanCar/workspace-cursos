package com.example.demo.model.tracking;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties
@JsonInclude(Include.NON_NULL)
public class DocumentsRequest {

	private String key;
	
	private List<PropertiesRequest> properties;
	
	private List<ListaRequest> lists;
	
}
