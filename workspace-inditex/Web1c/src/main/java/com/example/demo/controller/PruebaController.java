//package com.example.demo.controller;
//
//import static com.example.demo.utils.CseCourierConfigurations.CSE_LOGIN;
//import static com.example.demo.utils.CseCourierConfigurations.CSE_PASSWORD;
//
//import com.example.demo.exception.CSEApplicationException;
//import com.example.demo.model.request.LabelRequestDto;
//import com.example.demo.model.response.LabelResponse;
//import com.example.demo.service.PruebaService;
//import java.util.HashMap;
//import java.util.Map;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("web/label")
//public class PruebaController {
//
//  @Autowired
//  PruebaService service;
//
//  @PostMapping(value = "/cse")
//  LabelResponse consultarEtiquetaCse(@RequestBody LabelRequestDto request) throws CSEApplicationException {
//    Map<String, String> authentication = new HashMap<String, String>();
//    authentication.put(CSE_LOGIN.name(), "INDITEX");
//    authentication.put(CSE_PASSWORD.name(), "!LYMfmX7Ahz82");
//
//    return service.generarLabelCse(request, authentication);
//  }
//}
