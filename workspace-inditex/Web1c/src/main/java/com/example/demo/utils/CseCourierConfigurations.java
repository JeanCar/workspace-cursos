package com.example.demo.utils;

public enum CseCourierConfigurations {

	/** The cse login. */
	CSE_LOGIN,

	/** The cse password. */
	CSE_PASSWORD;
}
