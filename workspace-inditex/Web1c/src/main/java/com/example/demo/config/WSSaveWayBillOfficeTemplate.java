package com.example.demo.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.example.demo.thirdparty.consumo.SaveWaybillOffice;
import com.example.demo.thirdparty.consumo.SaveWaybillOfficeResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WSSaveWayBillOfficeTemplate extends WebServiceGatewaySupport {

	public SaveWaybillOfficeResponse callWebService(SaveWaybillOffice request) {
		log.info("Consultando servicio....");
		Object rpta = this.getWebServiceTemplate().marshalSendAndReceive(request);
		log.info("respuesta obtenida....");
		return (SaveWaybillOfficeResponse) rpta;
	}
}
