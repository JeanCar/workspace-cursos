package com.example.demo.utils;

import static lombok.AccessLevel.PRIVATE;

import lombok.NoArgsConstructor;

@NoArgsConstructor(access = PRIVATE)
public class LabelCseConstants {

  /** The Constant PATTERN. */
  public static final String PATTERN = "\\%(.+?)\\%";

  /** The Constant ERROR. */
  public static final String ERROR = "ERROR";

  /** The Constant GUID. */
  public static final String GUID = "GUID";

  /** The Constant ERROR GUID. */
  public static final String ERROR_GUID = "00000000-0000-0000-0000-000000000000";

  /** The Constant URGENCY. */
  public static final String URGENCY = "18c4f207-458b-11dc-9497-0015170f8c09";

  /** The Constant TYPE_CARGO. */
  public static final String TYPE_CARGO = "4aab1fc6-fc2b-473a-8728-58bcd4ff79ba";

  /** The Constant GMH. */
  public static final String GMH = "GMH";

  /** The Constant HEIGHT. */
  public static final String HEIGHT = "Height";

  /** The Constant LENGTH. */
  public static final String LENGTH = "Length";

  /** The Constant WIDTH. */
  public static final String WIDTH = "Width";

  /** The Constant WEIGHT. */
  public static final String WEIGHT = "Weight";

  /** The Constant CERO. */
  public static final String STRING_ZERO = "0";

  /** The Constant UNO. */
  public static final String STRING_ONE = "1";

  /** The Constant FLOAT. */
  public static final String FLOAT = "float";

  /** The Constant STRING. */
  public static final String STRING = "string";

  /** The Constant CLIENT_CODE. */
  public static final String CLIENT_CODE = "ClientCode";

  /** The Constant PARAMETERS. */
  public static final String PARAMETERS = "parameters";

  /** The Constant NAME. */
  public static final String NAME = "name";

  /** The Constant TYPE. */
  public static final String TYPE = "type";

  /** The Constant FORMAT. */
  public static final String FORMAT = "format";

  /** The Constant DOCUMENTS. */
  public static final String DOCUMENTS = "documents";

  /** The Constant DOCUMENT_TYPE. */
  public static final String DOCUMENT_TYPE = "documenttype";

  /** The Constant WAYBILL. */
  public static final String WAYBILL = "waybill";

  /** The Constant CASE_LABEL_ITX. */
  public static final String CASE_LABEL_ITX = "CSE label for ITX";

  /** The Constant PRINT. */
  public static final String PRINT = "print";

  /** The Constant PDF. */
  public static final String PDF = "pdf";

  /** The Constant NUMBER. */
  public static final String NUMBER = "Number";

  /** The Constant CLIENT_NUMBER. */
  public static final String CLIENT_NUMBER = "ClientNumber";

  /** The Constant GMH_NUMBER. */
  public static final String GMH_NUMBER = "GMH_Number";

  /** The Constant GMH_CLIENT_NUMBER. */
  public static final String GMH_CLIENT_NUMBER = "GMH_ClientNumber";

  /** The Constant BASE_64. */
  public static final String BASE_64 = "%BASE64_LABEL%";

  /** The Constant APPLICATION_PDF. */
  public static final String APPLICATION_PDF = "application/pdf";

  /** The Constant NUMBER_CODE_ERROR_DUPLICATED. */
  public static final String NUMBER_CODE_ERROR_DUPLICATED = "04032";

  /** The Constant METHOD_SAVE_WAY_BILL_OFFICE. */
  public static final String METHOD_SAVE_WAY_BILL_OFFICE = "SaveWayBillOffice";

  /** The Constant METHOD_CREATE_GMH. */
  public static final String METHOD_CREATE_GMH = "CreateGMH";

  /** The Constant METHOD_GET_FORMS_FOR_DOCUMENTS. */
  public static final String METHOD_GET_FORMS_FOR_DOCUMENTS = "GetFormsForDocuments";

  /** The Constant header KONG WS. */
  public static final String HEADER_JANUS = "itx-apiKey";

  /** The Constant SPACE. */
  public static final String SPACE = " ";
}
