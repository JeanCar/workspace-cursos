package com.example.demo.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.example.demo.thirdparty.consumo.CreateGMH;
import com.example.demo.thirdparty.consumo.CreateGMHResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WSCreateGmhTemplate extends WebServiceGatewaySupport {

	public CreateGMHResponse callWebService(CreateGMH request) {
		log.info("Consultando servicio....");
		Object rpta = this.getWebServiceTemplate().marshalSendAndReceive(request);
		log.info("respuesta obtenida....");
		return (CreateGMHResponse) rpta;
	}
}
