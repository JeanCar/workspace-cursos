package com.example.demo.model.request;

import lombok.Data;
import lombok.ToString;

/**
 * Element used for storage info received from webservice.
 */
@Data
@ToString(includeFieldNames = true)
public class LabelRecipientDto {

  /** The client. */
  private String client;

  /** The official. */
  private String official;

  /** The geography. */
  private String geography;

  /** The free form. */
  private boolean freeForm;

  /** The phone. */
  private String phone;

  /** The address. */
  private String address;

  /** The urgency. */
  private String urgency;

  /** The cod. */
  private String cod;

  /** The cargo package qty. */
  private Integer cargoPackageQty;

  /** The cargo weight. */
  private Float cargoWeight;

  /** The cargo packages weight. */
  private String cargoPackagesWeight;
}
