package com.example.demo.service.impl;

import static com.example.demo.utils.LabelCseConstants.ERROR;
import static com.example.demo.utils.LabelCseConstants.ERROR_GUID;
import static com.example.demo.utils.LabelCseConstants.GMH;
import static com.example.demo.utils.LabelCseConstants.GUID;
import static com.example.demo.utils.LabelCseConstants.METHOD_CREATE_GMH;
import static com.example.demo.utils.LabelCseConstants.METHOD_GET_FORMS_FOR_DOCUMENTS;
import static com.example.demo.utils.LabelCseConstants.METHOD_SAVE_WAY_BILL_OFFICE;
import static com.example.demo.utils.LabelCseConstants.NUMBER_CODE_ERROR_DUPLICATED;

import com.example.demo.config.WSCreateGmhTemplate;
import com.example.demo.config.WSGetFormsForDocumentsTemplate;
import com.example.demo.config.WSSaveWayBillOfficeTemplate;
import com.example.demo.exception.CSEApplicationException;
import com.example.demo.thirdparty.consumo.CreateGMH;
import com.example.demo.thirdparty.consumo.CreateGMHResponse;
import com.example.demo.thirdparty.consumo.Element;
import com.example.demo.thirdparty.consumo.GetFormsForDocuments;
import com.example.demo.thirdparty.consumo.GetFormsForDocumentsResponse;
import com.example.demo.thirdparty.consumo.ResultString;
import com.example.demo.thirdparty.consumo.SaveWaybillOffice;
import com.example.demo.thirdparty.consumo.SaveWaybillOfficeResponse;
import com.example.demo.utils.CseErrorCodes;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
@Slf4j
public class PruebaSender {

  @Autowired
  WSSaveWayBillOfficeTemplate template1;

  @Autowired
  WSCreateGmhTemplate template2;

  @Autowired
  WSGetFormsForDocumentsTemplate template3;

  public SaveWaybillOfficeResponse returnSaveWayBillOffice(SaveWaybillOffice request) throws CSEApplicationException {
    SaveWaybillOfficeResponse saveWaybillOfficeResponse = null;
    log.debug("[START returnSaveWayBillOffice]");
    try {
      jaxbObjectRequestToXML(request);
      // Call WS
      saveWaybillOfficeResponse = this.template1.callWebService(request);
    } catch (RuntimeException ex) {
      log.error("Error doing request returnSaveWayBillOffice: {}", request.toString(), ex);
      throw new CSEApplicationException(CseErrorCodes.CALL_WS.getErrorCode(), ex.getMessage(),
          this.getClass());
    }

    List<ResultString> resultItems = new ArrayList<>();
    if (saveWaybillOfficeResponse.getReturn() != null && saveWaybillOfficeResponse.getReturn().getItems() != null) {
      resultItems = saveWaybillOfficeResponse.getReturn().getItems();
    }

    // Check error authentication and error diferent code 04032
    if (CollectionUtils.isEmpty(resultItems)
        || (!NUMBER_CODE_ERROR_DUPLICATED.equals(resultItems.get(0).getErrorInfo())
            && saveWaybillOfficeResponse.getReturn().isError())) {
      log.error("Empty results doing request returnSaveWayBillOffice: {} ", request.toString());
      throw new CSEApplicationException(CseErrorCodes.CALL_WS.getErrorCode(), METHOD_SAVE_WAY_BILL_OFFICE,
          this.getClass());
    }
    jaxbObjectResponseToXML(saveWaybillOfficeResponse);
    log.debug("[STOP returnSaveWayBillOffice]");
    return saveWaybillOfficeResponse;
  }

  public CreateGMHResponse returnCreateGMH(CreateGMH request) throws CSEApplicationException {
    CreateGMHResponse createGMHResponse = null;
    log.debug("[START returnCreateGMH]");
    try {
      jaxbObjectRequestToXML(request);
      // Call WS
      createGMHResponse = this.template2.callWebService(request);
    } catch (RuntimeException ex) {
      log.error("Error doing request createGMH: {}", request.toString(), ex);
      throw new CSEApplicationException(CseErrorCodes.CALL_WS.getErrorCode(), ex.getMessage(),
          this.getClass());
    }
    // Manage errorAuthentication
    Optional<Element> errorAuthentication =
        createGMHResponse.getReturn().getProperties()
            .stream().filter(
                f -> Boolean.TRUE.equals(f.getValue())
                    && ERROR.equalsIgnoreCase(f.getKey()))
            .findFirst();

    // Manage errorValidationData
    Optional<Element> errorValidationData =
        createGMHResponse.getReturn().getLists().stream()
            .filter(p -> GMH.equalsIgnoreCase(p.getKey())
                && p.getProperties().stream()
                    .anyMatch(f -> ERROR.equalsIgnoreCase(f.getKey())
                        && Boolean.TRUE.equals(f.getValue())))
            .findFirst();

    // Check errorAuthentication and errorValidationData
    if (errorAuthentication.isPresent() || errorValidationData.isPresent()) {
      log.error("Authentication or ValidationData error doing request createGMH: {} ", request.toString());
      throw new CSEApplicationException(CseErrorCodes.CALL_WS.getErrorCode(), METHOD_CREATE_GMH, this.getClass());
    }
    jaxbObjectResponseToXML(createGMHResponse);
    log.debug("[STOP returnCreateGMH]");
    return createGMHResponse;
  }

  public GetFormsForDocumentsResponse returnGetFormsForDocuments(GetFormsForDocuments request) throws CSEApplicationException {
    GetFormsForDocumentsResponse getFormsForDocumentsResponse = null;
    log.debug("[START returnGetFormsForDocuments]");
    try {
      jaxbObjectRequestToXML(request);
      // Call WS
      getFormsForDocumentsResponse = this.template3.callWebService(request);
    } catch (RuntimeException ex) {
      log.error("Error doing request getFormsForDocuments: {}", request.toString(), ex);
      throw new CSEApplicationException(CseErrorCodes.CALL_WS.getErrorCode(), ex.getMessage(),
          this.getClass());
    }
    // Manage error
    Optional<Element> errorValidationData =
        getFormsForDocumentsResponse.getReturn()
            .getLists().stream().filter(p -> p.getProperties().stream()
                .anyMatch(x -> ERROR.equalsIgnoreCase(x.getKey())
                    && Boolean.TRUE.equals(x.getValue())))
            .findFirst();
    // Blank page error
    Optional<Element> errorValidationBlankPage =
        getFormsForDocumentsResponse.getReturn()
            .getLists().stream().filter(p -> p.getProperties().stream()
                .anyMatch(x -> GUID.equalsIgnoreCase(x.getKey())
                    && ERROR_GUID.equals(x.getValue())))
            .findFirst();

    // Check error validation data and blank page
    if (errorValidationData.isPresent() || errorValidationBlankPage.isPresent()) {
      log.error(" ValidationData error doing request getFormsForDocuments: {} ", request.toString());
      throw new CSEApplicationException(CseErrorCodes.CALL_WS.getErrorCode(), METHOD_GET_FORMS_FOR_DOCUMENTS,
          this.getClass());
    }
    jaxbObjectResponseToXML(getFormsForDocumentsResponse);
    log.debug("[STOP returnGetFormsForDocuments]");
    return getFormsForDocumentsResponse;
  }

  public static void jaxbObjectRequestToXML(Object object) {
    JAXBContext jaxbContext = null;
    Marshaller jaxbMarshaller = null;

    try {
      StringWriter sw = new StringWriter();
      if (object instanceof SaveWaybillOffice) {
        jaxbContext = JAXBContext.newInstance(SaveWaybillOffice.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        SaveWaybillOffice res = (SaveWaybillOffice) object;
        jaxbMarshaller.marshal(res, sw);
      } else if (object instanceof CreateGMH) {
        jaxbContext = JAXBContext.newInstance(CreateGMH.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        CreateGMH res = (CreateGMH) object;
        jaxbMarshaller.marshal(res, sw);

      } else if (object instanceof GetFormsForDocuments) {
        jaxbContext = JAXBContext.newInstance(GetFormsForDocuments.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        GetFormsForDocuments req = (GetFormsForDocuments) object;
        jaxbMarshaller.marshal(req, sw);
      }
      log.info(sw.toString());

    } catch (JAXBException ex) {
      log.error("Error de procesar XML: ", ex);
    }
  }

  public static void jaxbObjectResponseToXML(Object object) {
    JAXBContext jaxbContext = null;
    Marshaller jaxbMarshaller = null;

    try {
      StringWriter sw = new StringWriter();
      if (object instanceof SaveWaybillOfficeResponse) {
        jaxbContext = JAXBContext.newInstance(SaveWaybillOfficeResponse.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        SaveWaybillOfficeResponse res = (SaveWaybillOfficeResponse) object;
        jaxbMarshaller.marshal(res, sw);
      } else if (object instanceof CreateGMHResponse) {
        jaxbContext = JAXBContext.newInstance(CreateGMHResponse.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        CreateGMHResponse res = (CreateGMHResponse) object;
        jaxbMarshaller.marshal(res, sw);

      } else if (object instanceof GetFormsForDocumentsResponse) {
        jaxbContext = JAXBContext.newInstance(GetFormsForDocumentsResponse.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        GetFormsForDocumentsResponse res = (GetFormsForDocumentsResponse) object;
        jaxbMarshaller.marshal(res, sw);
      }
      log.info(sw.toString());

    } catch (JAXBException ex) {
      log.error("Error de procesar XML: ", ex);
    }
  }
}
