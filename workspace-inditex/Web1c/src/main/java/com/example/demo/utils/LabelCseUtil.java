package com.example.demo.utils;

import static com.example.demo.utils.LabelCseConstants.APPLICATION_PDF;
import static com.example.demo.utils.LabelCseConstants.GMH_CLIENT_NUMBER;
import static com.example.demo.utils.LabelCseConstants.GMH_NUMBER;
import static com.example.demo.utils.LabelCseConstants.PATTERN;
import static lombok.AccessLevel.PRIVATE;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataSource;
import javax.imageio.ImageIO;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.example.demo.exception.CSEApplicationException;
import com.example.demo.model.response.LabelCourierResponse;
import com.example.demo.thirdparty.consumo.CreateGMHResponse;
import com.example.demo.thirdparty.consumo.Element;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = PRIVATE)
public class LabelCseUtil {

  /**
   * Generate to base64. Using for images
   *
   * @param base64Label base64 label
   * @param courierRef courier ref
   * @param itxRef itx ref
   * @return the list
   */
  public static List<LabelCourierResponse> generateToBase64(List<byte[]> base64Label, List<String> courierRef, List<String> itxRef)
      throws CSEApplicationException {
    List<LabelCourierResponse> listCourier = new ArrayList<>();
    if (base64Label == null || base64Label.isEmpty()) {
      throw new CSEApplicationException(CseErrorCodes.CONVERT_BASE64.getErrorCode(),
              "No information is received in base64 for conversion", LabelCseUtil.class);
    }
    // Get PDFDocument
    DataSource inputStream = new ByteArrayDataSource(base64Label.get(0), APPLICATION_PDF);
    try (PDDocument document = PDDocument.load(inputStream.getInputStream())) {

      // Create PDFRenderer
      PDFRenderer renderer = new PDFRenderer(document);
      // Create labelCourierResponse
      LabelCourierResponse labelCourierResponse;
      // Create ByteArrayOutputStream
      ByteArrayOutputStream baos;
      // Create imageInByte
      byte[] imageInByte;
      int order = 0;
      int diffPages = document.getPages().getCount() - itxRef.size();
      if (diffPages < 0) {
        throw new CSEApplicationException(CseErrorCodes.NUMBER_PDF_PAGES.getErrorCode(),
            "The number of pages in the pdf is less than the labels received", LabelCseUtil.class);
      }
      // Iterate itxRef PDF may contain blank pages at the end
      for (int page = diffPages; page < document.getPages().getCount(); page++) {
        // Create image
        BufferedImage image = renderer.renderImageWithDPI(page, 200, ImageType.BINARY);
        // Rotate img 90 degrees
        image = rotateClockwise90(image);
        baos = new ByteArrayOutputStream();

        // Create image
        ImageIO.write(image, "PNG", baos);
        baos.flush();
        imageInByte = baos.toByteArray();
        baos.close();

        // LabelCourierResponse
        labelCourierResponse = new LabelCourierResponse();
        // This field will be used for the setOrder and must always start at 1
        labelCourierResponse.setIdCourier(order + 1);
        labelCourierResponse.setItxReferences(itxRef.get(order));
        labelCourierResponse.setCourierReferences(courierRef.get(order));
        labelCourierResponse.setCourierBase64(Base64.getEncoder().encodeToString(imageInByte));
        // add labelCourierResponse
        listCourier.add(labelCourierResponse);
        order++;
      }
    } catch (IOException ex) {
      LabelCseUtil.log.error("Label Conversion Error: ", ex);
      throw new CSEApplicationException(CseErrorCodes.CONVERT_BASE64.getErrorCode(), "Label Conversion Error",
              LabelCseUtil.class);
    }
    return listCourier;
  }

  /**
   * Convert gregorian calendar to string.
   *
   * @param date fecha
   * @return the XML gregorian calendar
   */
  public static XMLGregorianCalendar convertGregorianCalendarToString(String date) {
    XMLGregorianCalendar returnDate = null;
    try {
      returnDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
    } catch (DatatypeConfigurationException ex) {
      LabelCseUtil.log.error("Conversion Error XmlGregorianCalendar: ", ex);
    }
    return returnDate;
  }

  public static String replacePlaceHolders(String labelSvg, Map<String, Object> replacements) {
    Pattern pattern = Pattern.compile(PATTERN);
    Matcher matcher = pattern.matcher(labelSvg);
    StringBuffer buffer = new StringBuffer();

    // search placeholder
    while (matcher.find()) {
      Object replacement = replacements.get(String.join("", "%", matcher.group(1), "%"));
      if (replacement != null) {
        matcher.appendReplacement(buffer,"");
        buffer.append(replacement);
      }
    }
    matcher.appendTail(buffer);
    // Return SVG with params
    return buffer.toString();
  }

  public static List<String> findCourierReference(CreateGMHResponse createGMHResponse) {

    List<String> courierRef = new ArrayList<>();
    // Iterate list courierReference
    createGMHResponse.getReturn().getLists().stream().map(Element::getProperties).forEach(k -> {
      for (Element element : k) {
        if (GMH_NUMBER.equalsIgnoreCase(element.getKey())) {
          courierRef.add(element.getValue().toString());
        }
      }
    });
    // list courierRef
    return courierRef;
  }

  public static List<String> findItxReference(CreateGMHResponse createGMHResponse) {
    List<String> itxRef = new ArrayList<>();

    // Iterate list ixtReference
    createGMHResponse.getReturn().getLists().stream().map(Element::getProperties).forEach(k -> {
      for (Element element : k) {
        if (GMH_CLIENT_NUMBER.equalsIgnoreCase(element.getKey())) {
          itxRef.add(element.getValue().toString());
        }
      }
    });
    // List itxRef
    return itxRef;
  }

//  public static List<TrackingNumberDataHolder> getCourierReference(String value) {
//    final List<TrackingNumberDataHolder> listTrackingNumberDataHolder = new ArrayList<>();
//    TrackingNumberDataHolder trackingNumberDataHolder = new TrackingNumberDataHolder();
//    trackingNumberDataHolder.setKey(RangeTypeEnum.MAIN_RANGE);
//    trackingNumberDataHolder.setValue(value);
//    listTrackingNumberDataHolder.add(trackingNumberDataHolder);
//    return listTrackingNumberDataHolder;
//  }

  public static BufferedImage rotateClockwise90(BufferedImage image) {
    int width = image.getWidth();
    int height = image.getHeight();

    BufferedImage dest = new BufferedImage(height, width, image.getType());

    Graphics2D graphics2D = dest.createGraphics();
    graphics2D.translate((height - width) / 2, (height - width) / 2);
    graphics2D.rotate(Math.PI / 2, height / (double) 2, width / (double) 2);
    graphics2D.drawRenderedImage(image, null);

    return dest;
  }

}
