package com.example.demo.model.request;

import java.util.List;
import lombok.Data;
import lombok.ToString;

/**
 * Element used for storage info received from webservice.
 */
@Data
@ToString(includeFieldNames = true)
public class LabelRequestDto {

  /** The client number. */
  private String clientNumber;

  /** The delivery date. */
  private String deliveryDate;

  /** The itx references. */
  private List<String> itxReferences;

  /** The recipients. */
  private LabelRecipientDto recipients;

  /** The sender. */
  private LabelSenderDto sender;

  /** The type of cargo. */
  private String typeOfCargo;

  /** The type of payer. */
  private String typeOfPayer;

  /** The way of payment. */
  private String wayOfPayment;

  /** The payment method. */
  private Integer paymentMethod;

  /** The delivery of cargo. */
  private String deliveryOfCargo;

}
