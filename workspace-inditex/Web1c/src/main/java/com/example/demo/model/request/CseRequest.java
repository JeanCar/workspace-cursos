package com.example.demo.model.request;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CseRequest {

  private String orderNumber;
  private List<String> itxReference;
}
