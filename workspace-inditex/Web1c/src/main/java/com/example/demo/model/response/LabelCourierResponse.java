package com.example.demo.model.response;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(includeFieldNames = true)
public class LabelCourierResponse {

	/** The id courier. */
	private int idCourier;

	/** The itx references. */
	private String itxReferences;

	/** The courier references. */
	private String courierReferences;

	/** The courier base64. */
	private String courierBase64;
}
