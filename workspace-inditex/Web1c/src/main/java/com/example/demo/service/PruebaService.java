package com.example.demo.service;

import com.example.demo.exception.CSEApplicationException;
import com.example.demo.model.request.CseRequest;
import com.example.demo.model.response.LabelResponse;
import java.util.Map;

public interface PruebaService {

  LabelResponse generarLabelCse(CseRequest request, Map<String, String> authentication) throws CSEApplicationException;
}
