package com.example.demo.model.tracking;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties
@JsonInclude(Include.NON_NULL)
public class TrackRequest {
	
	private String login;
	private String password;
	private DocumentsRequest documents;
	
	private ParametersRequest parameters;
	
}
