package com.example.demo.service.impl;

import com.example.demo.model.response.LabelCourierResponse;
import com.example.demo.model.response.LabelResponse;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class LabelCseProcessor {

  public LabelResponse proccessResponse(List<LabelCourierResponse> listCourier) {
    LabelResponse labelResponse = new LabelResponse();
    labelResponse.setListCourier(listCourier);
    return labelResponse;
  }
}
