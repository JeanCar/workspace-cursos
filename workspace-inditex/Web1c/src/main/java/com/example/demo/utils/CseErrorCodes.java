package com.example.demo.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CseErrorCodes {

  /** The courier configuration. */
  COURIER_CONFIGURATION("FETCH-CC"),

  /** The sequences. */
  SEQUENCES("FETCH-SEQ"),

  /** The courier route data. */
  COURIER_ROUTE_DATA("FETCH-ROUTES"),

  /** The call ws. */
  CALL_WS("WS"),

  /** The replace place holders. */
  REPLACE_PLACE_HOLDERS("REPLACE-PH"),

  /** The fill label courier data. */
  FILL_LABEL_COURIER_DATA("FILL-LCD"),

  /** Error code when converting an image to base64. */
  CONVERT_BASE64("CONVERT_BASE64"),

  /** Error code when set itx-apiKey. */
  ITX_API_KEY_HEADER("ITX_API_KEY_HEADER"),

  /** The number of pages in the pdf is less than the labels received. */
  NUMBER_PDF_PAGES("NUMBER_PDF_PAGES");
  /** The error code. */
  private String errorCode;
}
