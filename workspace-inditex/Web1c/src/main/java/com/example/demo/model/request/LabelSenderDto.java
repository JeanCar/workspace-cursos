package com.example.demo.model.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(includeFieldNames = true)
public class LabelSenderDto {

  /** The client. */
  private String client;

  /** The official. */
  private String official;

  /** The geography. */
  private String geography;

  /** The free form. */
  private boolean freeForm;

  /** The phone. */
  private String phone;

  /** The address. */
  private String address;
}
