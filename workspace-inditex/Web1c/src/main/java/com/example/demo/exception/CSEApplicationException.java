package com.example.demo.exception;

public class CSEApplicationException extends Exception {
	private static final long serialVersionUID = 1L;

	public CSEApplicationException(String errorCode, String description, Class<?> classException) {
		super(errorCode);
	}
}
