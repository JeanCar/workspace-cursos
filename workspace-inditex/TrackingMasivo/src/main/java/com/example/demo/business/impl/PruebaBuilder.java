package com.example.demo.business.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import com.example.demo.model.PruebaRequest;
import com.example.demo.thirdparty.consumo.request.Autentificacion;
import com.example.demo.thirdparty.consumo.request.Dato;
import com.example.demo.thirdparty.consumo.request.RequestSwagger;

@Component
public class PruebaBuilder {

	public RequestSwagger builderRequest(PruebaRequest request) {
		RequestSwagger r = new RequestSwagger();
		List<Dato> datos = request.getDatos().stream().map(m -> {
			Dato d = new Dato();
			d.setCodigoSeguimiento(m.getCodigoSeguimiento());
			d.setCodigoAlternativo(m.getCodigoAlternativo());
			d.setFechaDesde(m.getFechaDesde());
			d.setFechaHasta(m.getFechaHasta());

			return d;

		}).collect(Collectors.toList());
		r.setDatos(datos);

		Autentificacion autentificacion = new Autentificacion();
		autentificacion.setShipper(request.getAutentificacion().getShipper());
		autentificacion.setPassword(request.getAutentificacion().getPassword());
		r.setAutentificacion(autentificacion);

		return r;

	}

}
