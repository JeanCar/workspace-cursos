package com.example.demo.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.common.io.Resources;
import com.google.gson.Gson;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor(access= AccessLevel.PRIVATE)
@Log4j
public class PruebaUtil {

	
	public static String fileResourceToString(String resourcePath) {
		String cadena = "";
		// PruebaRequest venta2 = new PruebaRequest();
		try {
			cadena = new String(Files.readAllBytes(Paths.get(Resources.getResource(resourcePath).toURI())),
					StandardCharsets.UTF_8);
			System.out.println("JSON: " + cadena);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cadena;
	}

}
