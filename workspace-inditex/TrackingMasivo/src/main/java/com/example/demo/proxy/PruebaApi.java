package com.example.demo.proxy;

import java.util.List;

import com.example.demo.thirdparty.consumo.request.RequestSwagger;
import com.example.demo.thirdparty.consumo.response.ResponseSwagger;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PruebaApi {

	// @Headers({"Content-Type:application/json;charset=UTF-8"})}
	@FormUrlEncoded
	@POST("/urbano3/wbs/tracking/consultaTrackingMasivo/")
	Single<List<ResponseSwagger>> getConsumo(@Field("json") String json);
	
	@FormUrlEncoded
	@POST("/urbano3/wbs/tracking/consultaTrackingMasivo/")
	Single<List<ResponseSwagger>> getConsumo2(@Field("json") String json);
	
	@Headers({"Content-Type:application/json;charset=UTF-8"})
	@POST("/urbano3/wbs/tracking/consultaTrackingMasivo/")
	Single<List<ResponseSwagger>> getConsumo3(@Body RequestSwagger body);
}
