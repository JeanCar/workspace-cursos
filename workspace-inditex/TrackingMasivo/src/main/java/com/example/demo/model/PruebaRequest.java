package com.example.demo.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PruebaRequest {

	private List<DatosRequest> datos;
	private AutentificacionRequest autentificacion;
	
}


