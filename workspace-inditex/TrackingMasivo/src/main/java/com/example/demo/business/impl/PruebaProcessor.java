package com.example.demo.business.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.model.DomicilioResponse;
import com.example.demo.model.PruebaResponse;
import com.example.demo.thirdparty.consumo.response.Domicilio;
import com.example.demo.thirdparty.consumo.response.ResponseSwagger;

import io.reactivex.Single;

@Component
public class PruebaProcessor {

	public Single<PruebaResponse> processResponse(List<ResponseSwagger> res) {
		PruebaResponse r = new PruebaResponse();
		ResponseSwagger elemento = res.get(0);

		r.setCodGestion(elemento.getCodGestion());
		r.setCodError(elemento.getCodError());
		r.setDescError(elemento.getDescError());
		r.setEstado(elemento.getEstado());
		r.setShipper(elemento.getShipper());
		r.setFecha(elemento.getFecha());
		r.setNombreCliente(elemento.getNombreCliente());

		Domicilio domicilio = res.get(0).getDomicilio();
		DomicilioResponse dom = new DomicilioResponse();
		dom.setDireccion(domicilio.getDireccion());
		dom.setLocalidad(domicilio.getLocalidad());
		dom.setProvincia(domicilio.getProvincia());
		r.setDomicilio(dom);

		return Single.just(r);
	}

}
