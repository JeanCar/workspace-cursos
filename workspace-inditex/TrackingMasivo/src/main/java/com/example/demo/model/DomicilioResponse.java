package com.example.demo.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NonNull
public class DomicilioResponse {

	private String direccion;
	private String localidad;
	private String provincia;
}
