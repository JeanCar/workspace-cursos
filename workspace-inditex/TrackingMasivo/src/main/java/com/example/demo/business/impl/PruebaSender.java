package com.example.demo.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.config.RestClientConfiguration;
import com.example.demo.thirdparty.consumo.request.RequestSwagger;
import com.example.demo.thirdparty.consumo.response.ResponseSwagger;

import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PruebaSender {

	@Autowired
	RestClientConfiguration api;

	public Single<List<ResponseSwagger>> senderRequest(String json) {
		return api.getProxy().getConsumo(json).doOnSuccess(item -> log.info("Successful"));
	}
		
	public Single<List<ResponseSwagger>> senderRequest2(String json) {
		return api.getProxy().getConsumo2(json).doOnSuccess(item -> log.info("Successful"));
	}
	
	
	public Single<List<ResponseSwagger>> senderRequest3(RequestSwagger body) {
		return api.getProxy().getConsumo3(body).doOnSuccess(item -> log.info("Successful"));
	}

}
