package com.example.demo.model;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NonNull
public class PruebaResponse {
  
	private String codGestion;
    private Integer codError;
    private String descError;
    private String shipper;
    private String nombreCliente;
    private String fecha;
    private String estado;
    private DomicilioResponse domicilio;

}
