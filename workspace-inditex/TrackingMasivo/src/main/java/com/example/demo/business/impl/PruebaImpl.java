package com.example.demo.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.business.Prueba;
import com.example.demo.model.PruebaRequest;
import com.example.demo.model.PruebaResponse;
import com.example.demo.util.PruebaUtil;

import io.reactivex.Single;

@Service
public class PruebaImpl implements Prueba {

	@Autowired
	private PruebaProcessor processor;

	@Autowired
	private PruebaBuilder builder;

	@Autowired
	private PruebaSender sender;

	private static final String archivo = "JSON/REQUEST.JSON";

	// LEYENDO DESDE UN ARCHIVO
	@Override
	public Single<PruebaResponse> obtenerDatos() {
		// TODO Auto-generated method stub
		return sender.senderRequest(PruebaUtil.fileResourceToString(archivo))
				.flatMap(obj1 -> processor.processResponse(obj1)).doOnSuccess(s -> System.out.println("exitoso"));

	}

	// ENVIANDO DESDE LA API REST CREADA TXT
	@Override
	public Single<PruebaResponse> obtenerDatos2(String param) {
		// TODO Auto-generated method stub
		return sender.senderRequest2(param).flatMap(obj1 -> processor.processResponse(obj1))
				.doOnSuccess(s -> System.out.println("exitoso"));
	}

	
	@Override
	public Single<PruebaResponse> obtenerDato3(PruebaRequest body) {
		// TODO Auto-generated method stub
		return sender.senderRequest3(builder.builderRequest(body)).flatMap(obj1 -> processor.processResponse(obj1))
				.doOnSuccess(s -> System.out.println("exitoso"));
	}
}
