package com.example.demo.business;


import com.example.demo.model.PruebaRequest;
import com.example.demo.model.PruebaResponse;

import io.reactivex.Single;

public interface Prueba {

	Single<PruebaResponse> obtenerDatos();
	
	Single<PruebaResponse> obtenerDatos2(String param);

	Single<PruebaResponse> obtenerDato3(PruebaRequest body);

}
