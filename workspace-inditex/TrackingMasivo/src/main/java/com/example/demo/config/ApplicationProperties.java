package com.example.demo.config;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

@Configuration
@Getter
@Setter
@PropertySource("classpath:application.yml")
public class ApplicationProperties {


	  @NotNull
	  @Value("${application.component}")
	  private String component;

	  @NotNull
	  @Value("${application.api.path}")
	  private String baseUrl;

	  @NotNull
	  @Value("${application.api.connect-timeout}")
	  private Long connectTimeout;

	  @NotNull
	  @Value("${application.api.read-timeout}")
	  private Long readTimout;

	  @NotNull
	  @Value("${application.api.write-timeout}")
	  private Long writeTimout;
}
