package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.business.Prueba;
import com.example.demo.model.PruebaRequest;
import com.example.demo.model.PruebaResponse;

import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/tracking")
@Slf4j
class PruebaController {

	@Autowired
	Prueba service;

	//DESDE UN ARCHIVO JSON
	@PostMapping(value = "/masivo")
	public Single<PruebaResponse> obtenerDatos() {
		return service.obtenerDatos();
	}

	//DESDE UN TEXTO PLANO
	@PostMapping(value = "/masivo2",consumes="text/plain")
	public Single<PruebaResponse> obtenerDatos2(@RequestBody String param) {
		log.info("JSON: " + param);
		return service.obtenerDatos2(param);
	}
	
	
	//DESDE UN BODY JSON
	@PostMapping(value = "/masivo3")
	public Single<PruebaResponse> obtenerDatos3(@RequestBody PruebaRequest body) {
		log.info("JSON: " + body);
		return service.obtenerDato3(body);
	}
}
