package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.tracking.TrackRequest;
import com.example.demo.model.tracking.TrackResponse;
import com.example.demo.service.PruebaService;
import com.example.demo.thirdparty.cosumo.proxy.impl.WebServiceTracking;

import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PruebaServiceImpl implements PruebaService{

	@Autowired
	PruebaBuilder builder;
	
	@Autowired
	PruebaSender sender;
	
	@Autowired
	PruebaProccesor proccesor;
	
	@Autowired
	WebServiceTracking trackingService;
	
	@Override
	public Observable<TrackResponse> consultarTracking(TrackRequest request) {
		// TODO Auto-generated method stub
		//log.info("RESPUESTA: " +  trackingService.consultarTracking(request));
		return sender.obtenerDataTracking(builder.builderRequest(request)).map(proccesor::processTracking)
				.doOnError(error-> log.info(error.getMessage()));
	}

}
