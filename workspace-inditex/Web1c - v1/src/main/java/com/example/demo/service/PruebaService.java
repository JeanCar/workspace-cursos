package com.example.demo.service;

import com.example.demo.model.tracking.TrackRequest;
import com.example.demo.model.tracking.TrackResponse;

import io.reactivex.Observable;

public interface PruebaService {

	Observable<TrackResponse> consultarTracking(TrackRequest request);

}
