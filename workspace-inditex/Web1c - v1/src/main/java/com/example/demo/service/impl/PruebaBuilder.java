package com.example.demo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Component;

import com.example.demo.model.tracking.TrackRequest;
import com.example.demo.thirdparty.consumo.Element;
import com.example.demo.thirdparty.consumo.Tracking;

@Component
public class PruebaBuilder {

	public Tracking builderRequest(TrackRequest request) {
		Tracking tracking = new Tracking();

		tracking.setLogin(request.getLogin());
		tracking.setPassword(request.getPassword());

		Element documents = new Element();
		documents.setKey(request.getDocuments().getKey());

		List<Element> properties = request.getDocuments().getProperties().stream().map(m -> {
			Element element = new Element();
			element.setKey(m.getKey());
			
			JAXBElement<Object> valor1 = 
				    new JAXBElement<Object>(new QName("http://www.cargo3.ru"), Object.class, m.getValue());
			
			JAXBElement<String> valor2 = 
				    new JAXBElement<String>(new QName("http://www.cargo3.ru"), String.class, m.getValuetype());
			
			if(valor1.isNil()) {
				element.setValue(valor1);
				
			}
			if(valor2.isNil()) {
				element.setValueType(valor2);
			}
			
			return element;

		}).collect(Collectors.toList());

		documents.setProperties(properties);

		List<Element> lists = request.getDocuments().getLists().stream().map(n->{
			Element element = new Element();
			element.setKey(n.getKey());
			return element;			
		}).collect(Collectors.toList());
		
		documents.setLists(lists);
		tracking.setDocuments(documents);
		
		Element parameter = new Element();
		parameter.setKey(request.getParameters().getKey());
		
		tracking.setParameters(parameter);

		return tracking;
	}
}
