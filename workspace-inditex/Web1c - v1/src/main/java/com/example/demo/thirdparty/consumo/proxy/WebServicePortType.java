
package com.example.demo.thirdparty.consumo.proxy;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.example.demo.thirdparty.consumo.Element;
import com.example.demo.thirdparty.consumo.ObjectFactory;
import com.example.demo.thirdparty.consumo.Order;
import com.example.demo.thirdparty.consumo.ResultArray;
import com.example.demo.thirdparty.consumo.ResultArrayPrint;
import com.example.demo.thirdparty.consumo.ResultString;
import com.example.demo.thirdparty.consumo.TrackingResponse;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10
 * Generated source version: 2.1
 * 
 */
@WebService(name = "WebServicePortType", targetNamespace = "http://www.cargo3.ru")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface WebServicePortType {


    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "Calc", action = "http://www.cargo3.ru#WebService:Calc")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "Calc", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.Calc")
    @ResponseWrapper(localName = "CalcResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CalcResponse")
    public Element calc(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "Calculator", action = "http://www.cargo3.ru#WebService:Calculator")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "Calculator", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.Calculator")
    @ResponseWrapper(localName = "CalculatorResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CalculatorResponse")
    public Element calculator(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "DeleteDocuments", action = "http://www.cargo3.ru#WebService:DeleteDocuments")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "DeleteDocuments", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.DeleteDocuments")
    @ResponseWrapper(localName = "DeleteDocumentsResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.DeleteDocumentsResponse")
    public Element deleteDocuments(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "GetDocuments", action = "http://www.cargo3.ru#WebService:GetDocuments")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "GetDocuments", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetDocuments")
    @ResponseWrapper(localName = "GetDocumentsResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetDocumentsResponse")
    public Element getDocuments(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param documents
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "GetFormsForDocuments", action = "http://www.cargo3.ru#WebService:GetFormsForDocuments")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "GetFormsForDocuments", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetFormsForDocuments")
    @ResponseWrapper(localName = "GetFormsForDocumentsResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetFormsForDocumentsResponse")
    public Element getFormsForDocuments(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "documents", targetNamespace = "http://www.cargo3.ru")
        Element documents,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "GetListOfDocuments", action = "http://www.cargo3.ru#WebService:GetListOfDocuments")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "GetListOfDocuments", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetListOfDocuments")
    @ResponseWrapper(localName = "GetListOfDocumentsResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetListOfDocumentsResponse")
    public Element getListOfDocuments(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "GetReferenceData", action = "http://www.cargo3.ru#WebService:GetReferenceData")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "GetReferenceData", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetReferenceData")
    @ResponseWrapper(localName = "GetReferenceDataResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetReferenceDataResponse")
    public Element getReferenceData(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "GetUserInfo", action = "http://www.cargo3.ru#WebService:GetUserInfo")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "GetUserInfo", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetUserInfo")
    @ResponseWrapper(localName = "GetUserInfoResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.GetUserInfoResponse")
    public Element getUserInfo(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "PutReferenceData", action = "http://www.cargo3.ru#WebService:PutReferenceData")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "PutReferenceData", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.PutReferenceData")
    @ResponseWrapper(localName = "PutReferenceDataResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.PutReferenceDataResponse")
    public String putReferenceData(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "PutUserInfo", action = "http://www.cargo3.ru#WebService:PutUserInfo")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "PutUserInfo", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.PutUserInfo")
    @ResponseWrapper(localName = "PutUserInfoResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.PutUserInfoResponse")
    public Element putUserInfo(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param name
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "Report", action = "http://www.cargo3.ru#WebService:Report")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "Report", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.Report")
    @ResponseWrapper(localName = "ReportResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.ReportResponse")
    public Element report(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "name", targetNamespace = "http://www.cargo3.ru")
        String name,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "SaveDocuments", action = "http://www.cargo3.ru#WebService:SaveDocuments")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "SaveDocuments", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveDocuments")
    @ResponseWrapper(localName = "SaveDocumentsResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveDocumentsResponse")
    public Element saveDocuments(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param documents
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "Tracking", action = "http://www.cargo3.ru#WebService:Tracking")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "Tracking", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.Tracking")
    @ResponseWrapper(localName = "TrackingResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.TrackingResponse")
    public TrackingResponse tracking(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "documents", targetNamespace = "http://www.cargo3.ru")
        Element documents,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
       Element  parameters);

    /**
     * 
     * @param password
     * @param data
     * @param log
     * @param login
     * @param parameters
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "SaveDocumentsCello", action = "http://www.cargo3.ru#WebService:SaveDocumentsCello")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "SaveDocumentsCello", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveDocumentsCello")
    @ResponseWrapper(localName = "SaveDocumentsCelloResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveDocumentsCelloResponse")
    public boolean saveDocumentsCello(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters,
        @WebParam(name = "log", targetNamespace = "http://www.cargo3.ru")
        String log);

    /**
     * 
     * @param password
     * @param data
     * @param log
     * @param login
     * @param parameters
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "SaveAckansCello", action = "http://www.cargo3.ru#WebService:SaveAckansCello")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "SaveAckansCello", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveAckansCello")
    @ResponseWrapper(localName = "SaveAckansCelloResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveAckansCelloResponse")
    public boolean saveAckansCello(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters,
        @WebParam(name = "log", targetNamespace = "http://www.cargo3.ru")
        String log);

    /**
     * 
     * @param password
     * @param data
     * @param log
     * @param login
     * @param parameters
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "SaveGenresCello", action = "http://www.cargo3.ru#WebService:SaveGenresCello")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "SaveGenresCello", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveGenresCello")
    @ResponseWrapper(localName = "SaveGenresCelloResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveGenresCelloResponse")
    public boolean saveGenresCello(
        @WebParam(name = "login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters,
        @WebParam(name = "log", targetNamespace = "http://www.cargo3.ru")
        String log);

    /**
     * 
     * @param number
     * @param password
     * @param orderData
     * @param language
     * @param company
     * @param clientNumber
     * @param office
     * @param login
     * @return
     *     returns com.example.demo.thirdparty.consumo.ResultArray
     */
    @WebMethod(operationName = "SaveWaybillOffice", action = "http://www.cargo3.ru#WebService:SaveWaybillOffice")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "SaveWaybillOffice", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveWaybillOffice")
    @ResponseWrapper(localName = "SaveWaybillOfficeResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.SaveWaybillOfficeResponse")
    public ResultArray saveWaybillOffice(
        @WebParam(name = "Language", targetNamespace = "http://www.cargo3.ru")
        String language,
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "Company", targetNamespace = "http://www.cargo3.ru")
        String company,
        @WebParam(name = "Number", targetNamespace = "http://www.cargo3.ru")
        String number,
        @WebParam(name = "ClientNumber", targetNamespace = "http://www.cargo3.ru")
        String clientNumber,
        @WebParam(name = "OrderData", targetNamespace = "http://www.cargo3.ru")
        Order orderData,
        @WebParam(name = "Office", targetNamespace = "http://www.cargo3.ru")
        String office);

    /**
     * 
     * @param number
     * @param password
     * @param length
     * @param width
     * @param weight
     * @param login
     * @param height
     * @return
     *     returns com.example.demo.thirdparty.consumo.ResultString
     */
    @WebMethod(operationName = "Measurement", action = "http://www.cargo3.ru#WebService:Measurement")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "Measurement", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.Measurement")
    @ResponseWrapper(localName = "MeasurementResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.MeasurementResponse")
    public ResultString measurement(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "Number", targetNamespace = "http://www.cargo3.ru")
        String number,
        @WebParam(name = "Length", targetNamespace = "http://www.cargo3.ru")
        Float length,
        @WebParam(name = "Height", targetNamespace = "http://www.cargo3.ru")
        Float height,
        @WebParam(name = "Width", targetNamespace = "http://www.cargo3.ru")
        Float width,
        @WebParam(name = "Weight", targetNamespace = "http://www.cargo3.ru")
        Float weight);

    /**
     * 
     * @param number
     * @param password
     * @param parametrs
     * @param login
     * @param type
     * @param printFormName
     * @return
     *     returns com.example.demo.thirdparty.consumo.ResultArrayPrint
     */
    @WebMethod(operationName = "PrintDocument", action = "http://www.cargo3.ru#WebService:PrintDocument")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "PrintDocument", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.PrintDocument")
    @ResponseWrapper(localName = "PrintDocumentResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.PrintDocumentResponse")
    public ResultArrayPrint printDocument(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "Type", targetNamespace = "http://www.cargo3.ru")
        String type,
        @WebParam(name = "PrintFormName", targetNamespace = "http://www.cargo3.ru")
        String printFormName,
        @WebParam(name = "Number", targetNamespace = "http://www.cargo3.ru")
        String number,
        @WebParam(name = "Parametrs", targetNamespace = "http://www.cargo3.ru")
        Element parametrs);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "CheckClientNumber", action = "http://www.cargo3.ru#WebService:CheckClientNumber")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "CheckClientNumber", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CheckClientNumber")
    @ResponseWrapper(localName = "CheckClientNumberResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CheckClientNumberResponse")
    public Element checkClientNumber(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "CheckPlannedDeliveryDate", action = "http://www.cargo3.ru#WebService:CheckPlannedDeliveryDate")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "CheckPlannedDeliveryDate", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CheckPlannedDeliveryDate")
    @ResponseWrapper(localName = "CheckPlannedDeliveryDateResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CheckPlannedDeliveryDateResponse")
    public Element checkPlannedDeliveryDate(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "UpdateClientProducts", action = "http://www.cargo3.ru#WebService:UpdateClientProducts")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "UpdateClientProducts", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.UpdateClientProducts")
    @ResponseWrapper(localName = "UpdateClientProductsResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.UpdateClientProductsResponse")
    public Element updateClientProducts(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "CreateGMH", action = "http://www.cargo3.ru#WebService:CreateGMH")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "CreateGMH", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CreateGMH")
    @ResponseWrapper(localName = "CreateGMHResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.CreateGMHResponse")
    public Element createGMH(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "DeleteGMH", action = "http://www.cargo3.ru#WebService:DeleteGMH")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "DeleteGMH", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.DeleteGMH")
    @ResponseWrapper(localName = "DeleteGMHResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.DeleteGMHResponse")
    public Element deleteGMH(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

    /**
     * 
     * @param password
     * @param data
     * @param login
     * @param parameters
     * @return
     *     returns com.example.demo.thirdparty.consumo.Element
     */
    @WebMethod(operationName = "UpdateDocument", action = "http://www.cargo3.ru#WebService:UpdateDocument")
    @WebResult(targetNamespace = "http://www.cargo3.ru")
    @RequestWrapper(localName = "UpdateDocument", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.UpdateDocument")
    @ResponseWrapper(localName = "UpdateDocumentResponse", targetNamespace = "http://www.cargo3.ru", className = "com.example.demo.thirdparty.consumo.UpdateDocumentResponse")
    public Element updateDocument(
        @WebParam(name = "Login", targetNamespace = "http://www.cargo3.ru")
        String login,
        @WebParam(name = "Password", targetNamespace = "http://www.cargo3.ru")
        String password,
        @WebParam(name = "data", targetNamespace = "http://www.cargo3.ru")
        Element data,
        @WebParam(name = "parameters", targetNamespace = "http://www.cargo3.ru")
        Element parameters);

}
