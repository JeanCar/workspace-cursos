package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.config.WebServiceTemplate;
import com.example.demo.thirdparty.consumo.Element;
import com.example.demo.thirdparty.consumo.Tracking;
import com.example.demo.thirdparty.consumo.TrackingResponse;

import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PruebaSender {

	@Autowired
	WebServiceTemplate template;

	public Observable<TrackingResponse> obtenerDataTracking(Tracking request) {
	
		TrackingResponse soapResponse = template.callWebService(request);
		
		log.info("SOAP****");
		log.info("Key: " + soapResponse.getReturn().getKey());
		
		soapResponse.getReturn().getLists().forEach(m->{
			
			log.info("Key:" +m.getKey());
			log.info("Value:" + m.getValue().getValue().toString());
			log.info("ValueType:" + m.getValueType().getValue().toString());
			log.info("***properties****");
			m.getProperties().forEach(n->{
				log.info(" Key: " + n.getKey());
				log.info(" value " + n.getValue().getValue().toString());
				log.info(" valueType " + n.getValueType().getValue().toString());
				log.info("**Lista***");
				n.getLists().forEach(z->{
					log.info("  Key: " + z.getKey());
					log.info("  value " + z.getValue().getValue().toString());
					log.info("  valueType " + z.getValueType().getValue().toString());
					
				});
			});
		});
		
			String key = "";
//			Optional<Object> codigo = soapResponse.getReturn().getProperties().stream().map(m -> m.getLists())
//					.map(p -> p.get(0).getValue().getValue()).findFirst();
			Optional<List<Element>> listaError = soapResponse.getReturn().getProperties().stream()
					.map(m -> m.getLists()).findFirst();
			if (listaError.isPresent()) {
				key = listaError.get().get(0).getKey();
				log.info("Response key Error: " + key);
			}
		 return Observable.fromCallable(() -> soapResponse);

	}
}
