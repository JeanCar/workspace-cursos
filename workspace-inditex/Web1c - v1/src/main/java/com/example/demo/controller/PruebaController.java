package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.tracking.TrackRequest;
import com.example.demo.model.tracking.TrackResponse;
import com.example.demo.service.PruebaService;

import io.reactivex.Observable;

@RestController
@RequestMapping("web")
public class PruebaController {

	@Autowired
	PruebaService service;
	
	
	@PostMapping(value = "/tracking")
	Observable<TrackResponse> consultarTracking(@RequestBody TrackRequest request){
		return service.consultarTracking(request);
		
	}
}
