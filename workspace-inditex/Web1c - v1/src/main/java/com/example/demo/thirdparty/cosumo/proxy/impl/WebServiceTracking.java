package com.example.demo.thirdparty.cosumo.proxy.impl;

import com.example.demo.model.tracking.TrackRequest;
import com.example.demo.thirdparty.consumo.TrackingResponse;

public interface WebServiceTracking {

	public TrackingResponse consultarTracking(TrackRequest request);
	
}
