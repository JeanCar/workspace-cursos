package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class PruebaClientConfig  {

	@Autowired
	ApplicationProperties properties;
	
	   @Bean
	    public Jaxb2Marshaller marshaller() {
	        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	        // this is the package name specified in the <generatePackage> specified in
	        // pom.xml
	       // marshaller.setContextPath("com.example.demo.thirparty.consumo");
	        marshaller.setPackagesToScan("com.example.demo.thirdparty.consumo");

	        return marshaller;
	    }
	 
	   
	   @Bean
	    public WebServiceTemplate soapConnector(Jaxb2Marshaller marshaller) {
		   WebServiceTemplate client = new WebServiceTemplate();
	        client.setDefaultUri(properties.getBaseUrl());
	        client.setMarshaller(marshaller);
	        client.setUnmarshaller(marshaller);
	        return client;
	    }
}
