package com.example.demo.service.impl;

import org.springframework.stereotype.Component;

import com.example.demo.model.tracking.TrackResponse;
import com.example.demo.thirdparty.consumo.TrackingResponse;

@Component
public class PruebaProccesor {

	public TrackResponse processTracking(TrackingResponse response) {
		TrackResponse res = new TrackResponse();

		res.setKey(response.getReturn().getKey());

		return res;
	}
}
