package com.example.demo.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.example.demo.thirdparty.consumo.Tracking;
import com.example.demo.thirdparty.consumo.TrackingResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WebServiceTemplate extends WebServiceGatewaySupport {

	public TrackingResponse callWebService(Tracking request) {
		log.info("Consultando servicio....");
		Object rpta = this.getWebServiceTemplate().marshalSendAndReceive(request);
		log.info("respuesta obtenida....");
		return (TrackingResponse) rpta;
	}
}
