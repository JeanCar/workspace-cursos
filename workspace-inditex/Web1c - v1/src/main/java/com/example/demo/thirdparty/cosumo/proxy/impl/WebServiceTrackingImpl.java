package com.example.demo.thirdparty.cosumo.proxy.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.demo.config.ApplicationProperties;
import com.example.demo.model.tracking.TrackRequest;
import com.example.demo.thirdparty.consumo.Element;
import com.example.demo.thirdparty.consumo.TrackingResponse;
import com.example.demo.thirdparty.consumo.proxy.WebServicePortType;

@Service
public class WebServiceTrackingImpl implements WebServiceTracking {

	@Autowired
	ApplicationProperties properties;

//	@Autowired
//	@Qualifier("WebServicePortType")
//	WebServicePortType port;

	@Override
	public TrackingResponse consultarTracking(TrackRequest request) {
		TrackingResponse trackingResponse = new TrackingResponse();
		String login = request.getLogin();
		String password = request.getPassword();
		Element documents = new Element();
		documents.setKey(request.getDocuments().getKey());
		List<Element> properties = request.getDocuments().getProperties().stream().map(m -> {
			Element element = new Element();
			element.setKey(m.getKey());
			JAXBElement<Object> valor1 = new JAXBElement<Object>(new QName("http://www.cargo3.ru"), Object.class,
					m.getValue());

			JAXBElement<String> valor2 = new JAXBElement<String>(new QName("http://www.cargo3.ru"), String.class,
					m.getValuetype());
			element.setValue(valor1);
			element.setValueType(valor2);
			return element;

		}).collect(Collectors.toList());

		documents.setProperties(properties);

		Element parameters = new Element();
		parameters.setKey(request.getParameters().getKey());
		//trackingResponse = port.tracking(login, password, documents, parameters);
		return trackingResponse;
	}

}
