package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Web1cApplication {

	public static void main(String[] args) {
		SpringApplication.run(Web1cApplication.class, args);
	}

}
