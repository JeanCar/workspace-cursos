package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.EventoRepository;
import com.example.demo.model.Eventos;

@RestController
public class PruebaController {

	@Autowired
	EventoRepository repository;
	
	
	@GetMapping("evento/listar")
	public List<Eventos> listar(){
		return repository.findAll();
		
	}
	
	@GetMapping("evento/listar/{filtro}")
	public List<Eventos> listaPorTitulo(@PathVariable("filtro") String titulo){
		return repository.findByTitulo(titulo);
		
	}
	
}
