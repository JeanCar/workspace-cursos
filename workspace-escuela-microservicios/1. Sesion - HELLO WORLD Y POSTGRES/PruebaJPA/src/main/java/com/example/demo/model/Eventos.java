package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "t_evento")
public class Eventos {

	  private Long id;
	  private String titulo;
	  private Date fecha;

	  
	  public Eventos() {
	  }

	  public Eventos(String titulo, Date fecha) {
	      this.titulo = titulo;
	      this.fecha = fecha;
	  }

	  @Id
	  @Column(name = "evento_id")	
	  public Long getId() {
	      return id;
	  }

	  private void setId(Long id) {
	      this.id = id;
	  }

	  @Column(name = "evento_fecha")
	  public Date getFecha() {
	      return fecha;
	  }

	  public void setFecha(Date fecha) {
	      this.fecha = fecha;
	  }
	  
	  public String getTitulo() {
	      return titulo;
	  }

	  public void setTitulo(String titulo) {
	      this.titulo = titulo;
	  }


}
