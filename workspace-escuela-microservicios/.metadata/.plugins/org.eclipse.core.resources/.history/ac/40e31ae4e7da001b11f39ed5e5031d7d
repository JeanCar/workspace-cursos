package com.example.store.shooping.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.store.shooping.bean.Invoice;
import com.example.store.shooping.bean.InvoiceItem;
import com.example.store.shooping.model.Customer;
import com.example.store.shooping.model.Product;
import com.example.store.shooping.repository.InvoiceItemsRepository;
import com.example.store.shooping.repository.InvoiceRepository;
import com.example.store.shooping.serivce.client.CustomerClient;
import com.example.store.shooping.serivce.client.ProductClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
//@RequiredArgsConstructor
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private InvoiceItemsRepository invoiceItemsRepository;

	@Autowired
	private CustomerClient customerClient;

	@Autowired
	private ProductClient productClient;

	@Override
	public List<Invoice> findInvoiceAll() {
		return invoiceRepository.findAll();
	}

	@Override
	public Invoice createInvoice(Invoice invoice) {
		Invoice invoiceDB = invoiceRepository.findByNumberInvoice(invoice.getNumberInvoice());
		if (invoiceDB != null) {
			return invoiceDB;
		}
		invoice.setState("CREATED");
		invoiceDB = invoiceRepository.save(invoice);
		invoiceDB.getItems().forEach(items -> {
			productClient.updateStockProduct(items.getId(), items.getQuantity() * -1);
		});
		return invoice;
	}

	@Override
	public Invoice updateInvoice(Invoice invoice) {
		Invoice invoiceDB = getInvoice(invoice.getId());
		if (invoiceDB == null) {
			return null;
		}
		invoiceDB.setCustomerId(invoice.getCustomerId());
		invoiceDB.setDescription(invoice.getDescription());
		invoiceDB.setNumberInvoice(invoice.getNumberInvoice());
		invoiceDB.getItems().clear();
		invoiceDB.setItems(invoice.getItems());
		return invoiceRepository.save(invoiceDB);
	}

	@Override
	public Invoice deleteInvoice(Invoice invoice) {
		Invoice invoiceDB = getInvoice(invoice.getId());
		if (invoiceDB == null) {
			return null;
		}
		invoiceDB.setState("DELETED");
		return invoiceRepository.save(invoiceDB);
	}

	@Override
	public Invoice getInvoice(Long id) {
		Optional<Invoice> invoiceOptional = invoiceRepository.findById(id);
		if (invoiceOptional.isPresent()) {
			Invoice invoice = invoiceOptional.get();
			ResponseEntity<Customer> customerEntity = customerClient.getCustomer(invoice.getCustomerId());
			Customer customer = Optional.ofNullable(customerEntity.getBody()).orElse(new Customer());
			invoice.setCustomer(customer);
			List<InvoiceItem> invoiceItems = invoice.getItems().stream().map(items -> {
				ResponseEntity<Product> productEntity = productClient.getProduct(items.getId());
				Product product = Optional.ofNullable(productEntity.getBody()).orElse(new Product());
				items.setProduct(product);
				return items;
			}).collect(Collectors.toList());
			invoice.setItems(invoiceItems);
		}
		return invoiceRepository.findById(id).orElse(null);
	}
}
