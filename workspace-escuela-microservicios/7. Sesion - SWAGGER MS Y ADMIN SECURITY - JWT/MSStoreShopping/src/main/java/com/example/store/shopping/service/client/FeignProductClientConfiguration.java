package com.example.store.shopping.service.client;

import org.springframework.context.annotation.Bean;

import feign.auth.BasicAuthRequestInterceptor;


public class FeignProductClientConfiguration {

	@Bean
	public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
		return new BasicAuthRequestInterceptor("Jean", "1234");
	}
}
