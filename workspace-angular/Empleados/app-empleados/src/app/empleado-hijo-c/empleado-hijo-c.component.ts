import { Component, Input, OnInit } from '@angular/core';
import { Empleado } from '../empleado.model';

@Component({
  selector: 'app-empleado-hijo-c',
  templateUrl: './empleado-hijo-c.component.html',
  styleUrls: ['./empleado-hijo-c.component.css']
})
export class EmpleadoHijoCComponent implements OnInit {

  @Input() empleadoDeLista:Empleado;
  @Input() indice:number;

  constructor() { }

  ngOnInit(): void {
  }


  empleados: Empleado[] = [
    new Empleado('Jean', 'Vasquez', 'Programador', 500),
    new Empleado('Luis', 'Chavez', 'Consultor', 120),
    new Empleado('Carlos', 'Rodriguez', 'Director', 100),
  ];

  empleadoAgregado(empleado:Empleado){
    this.empleados.push(empleado);
  }

  arrayCaracteristicas =[''];

  agregarCaracteristicas(nuevaCaracteristicias: string){
    this.arrayCaracteristicas.push(nuevaCaracteristicias);

  }

}
