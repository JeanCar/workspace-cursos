import { Component } from '@angular/core';
import { Empleado } from './empleado.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Listado de Empleados';
  empleados: Empleado[] = [
    new Empleado('Jean', 'Vasquez', 'Programador', 500),
    new Empleado('Luis', 'Chavez', 'Consultor', 120),
    new Empleado('Carlos', 'Rodriguez', 'Director', 100),
  ];

  cuadroNombre:string="";
  cuadroApellido:string="";
  cuadroCargo:string="";
  cuadroSalario:number=0;

  addEmployee(){
    let miEmployee = new Empleado(this.cuadroNombre,this.cuadroApellido,
      this.cuadroCargo,this.cuadroSalario);
    this.empleados.push(miEmployee);
  }

}
