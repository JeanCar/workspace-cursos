import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tittle = 'Registros de Usuarios';
  message = "";
  success = false;
  nombre:string="";
  apellido:string="";
  cargo:string="";
  entries:any[];
  constructor(){
    this.entries=[
    { tittle:"titulo 01"},
    { tittle:"titulo 02"},
    { tittle:"titulo 03"}
  ];
  }

  registrarUsuario(){
    this.success = true;
    this.message ="usuario registrado con éxito";
  }
}
