package com.everis.services.cross.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.everis.services.cross.api.request.IdSearchPathParam;
import com.everis.services.cross.api.response.Response;
import com.everis.services.cross.service.ApiCrossService;
import com.everis.services.cross.util.EverisUtil;
import io.reactivex.Single;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/cross/v1")
@Slf4j
public class ApiCrossController {

  @Autowired
  private ApiCrossService corporateLoansService;

  /**
   * sendToHostBean.
   */
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      value = "Consulta genérica de colocaciones por cliente (Créditos Consist)."
          + " Permite realizar consultas por código de la línea de crédito. Los "
          + "datos de salida incluyen los principales campos asociados al desembolso.",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET",
      notes = "classpath:swagger/notes/apicross.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecutó satisfactoriamente.",
          response = Response.class)})
  @GetMapping(value = "/obtenerDatos/{id}")
  public Single<Response> obtenerDatos(
      @PathVariable("id") IdSearchPathParam pathParams) {

    Single<Response> response = null;

    try {
      log.info("PathVariable: "+ pathParams.getCorporateLoanId());
      response =
          corporateLoansService.obtenerRespuestaCorporateLoans(pathParams.getCorporateLoanId());
//      log.info("Response: " + EverisUtil.toJson(response));
    } catch (Exception e) {
      log.error("Error: " + e);
    }

    return response;
  }
}
