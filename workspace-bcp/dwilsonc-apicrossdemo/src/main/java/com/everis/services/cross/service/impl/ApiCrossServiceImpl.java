package com.everis.services.cross.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.everis.services.cross.api.response.DebtType;
import com.everis.services.cross.api.response.Product;
import com.everis.services.cross.api.response.Response;
import com.everis.services.cross.api.response.TioAux;
import com.everis.services.cross.service.ApiCrossService;
import io.reactivex.Single;

@Service
public class ApiCrossServiceImpl implements ApiCrossService {

  @Override
  public Single<Response> obtenerRespuestaCorporateLoans(String corporateLoanId) {
    Map<String,Single<Response>> responseMapIni = constructResponseObject(corporateLoanId);
    return responseMapIni.entrySet().stream()
        .filter(e -> e.getKey().equals(corporateLoanId))
        .map(Map.Entry::getValue)
        .findFirst()
        .orElse(null);
  }
  
  private Map<String,Single<Response>> constructResponseObject(String corporateLoanId) {
    Map<String,Single<Response>> responseMap = new HashMap<String,Single<Response>>();
    responseMap.put("D47500049014", getResponse1());
    responseMap.put("D47500049012", getResponse2());
    responseMap.put("D47500049013", getResponse3());
    return responseMap;
  }
  
  private Single<Response> getResponse1() {
    Response response = new Response();
    response.setCorporateLoanId("D47500049014");
    response.setDebtType(new DebtType());
    response.getDebtType().setCode("CTG14");
    response.setProductDetail(new Product());
    response.getProductDetail().setTioaux(new TioAux());
    response.getProductDetail().getTioaux().setCode("FWDCOM14");
    return Single.fromCallable(() -> response);
  }
  
  private Single<Response> getResponse2() {
    Response response = new Response();
    response.setCorporateLoanId("D47500049012");
    response.setDebtType(new DebtType());
    response.getDebtType().setCode("CTG12");
    response.setProductDetail(new Product());
    response.getProductDetail().setTioaux(new TioAux());
    response.getProductDetail().getTioaux().setCode("FWDCOM12");
    return Single.fromCallable(() -> response);
  }
  
  private Single<Response> getResponse3() {
    Response response = new Response();
    response.setCorporateLoanId("D47500049013");
    response.setDebtType(new DebtType());
    response.getDebtType().setCode("CTG13");
    response.setProductDetail(new Product());
    response.getProductDetail().setTioaux(new TioAux());
    response.getProductDetail().getTioaux().setCode("FWDCOM13");
    return Single.fromCallable(() -> response);
  }
  
}
