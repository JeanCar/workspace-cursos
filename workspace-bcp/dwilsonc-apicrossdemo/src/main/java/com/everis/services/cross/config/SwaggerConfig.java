package com.everis.services.cross.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any()).build();
  }
  /*
   * Después de definir el bean Docket , su método select () devuelve una instancia de
   * ApiSelectorBuilder , que proporciona una forma de controlar los puntos finales expuestos por
   * Swagger.
   * 
   * Los predicados para la selección de RequestHandler se pueden configurar con la ayuda de
   * RequestHandlerSelectors y PathSelectors . El uso de any () para ambos hará que la documentación
   * de toda su API esté disponible a través de Swagger.
   * 
   * 
   * 
   */
}
