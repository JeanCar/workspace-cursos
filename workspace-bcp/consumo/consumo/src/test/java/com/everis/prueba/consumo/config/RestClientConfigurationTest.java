package com.everis.prueba.consumo.config;

import static org.mockito.Mockito.when;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class RestClientConfigurationTest {

  @Mock
  private ApplicationProperties properties;

  @InjectMocks
  private RestClientConfiguration test;

  @Before
  public void setUp() throws Exception {
   
  }

  @Test
  public void getProxyTest() { 
    when(properties.getBaseUrl()).thenReturn("http://www.mocky.io");
    Assert.assertNotNull(test.getProxy1());
  }
}
