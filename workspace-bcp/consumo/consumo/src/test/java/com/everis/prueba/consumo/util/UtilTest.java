package com.everis.prueba.consumo.util;

import com.everis.prueba.consumo.mock.first.thirdparty.ApiResponse;
import com.everis.prueba.consumo.mock.second.thirdparty.ApiResponse2;
import com.everis.prueba.consumo.model.api.Response;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class UtilTest {

  ClassLoader classLoader = getClass().getClassLoader();
  Gson gson = new Gson();

  public ApiResponse getLoanJsonResponse01(String json) throws Exception {
    File file = new File(classLoader.getResource(json).getFile());
    String content = new String(Files.readAllBytes(file.toPath()));
    return gson.fromJson(content, ApiResponse.class);

  }
  
  public Response getLoanJsonResponse02(String json) throws Exception {
    File file = new File(classLoader.getResource(json).getFile());
    String content = new String(Files.readAllBytes(file.toPath()));
    return gson.fromJson(content, Response.class);

  }
  
  public List<ApiResponse2> getLoanJsonResponse03(String json) throws Exception {
    File file = new File(classLoader.getResource(json).getFile());
    String content = new String(Files.readAllBytes(file.toPath()));
    return Arrays.asList(gson.fromJson(content, ApiResponse2[].class));

  }
//  
//  public List<EvaluationElement> getEvaluationElement(String jsonLoanRes) throws Exception {
//    String response =
//        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
//    ObjectMapper mapper = new ObjectMapper();
//    return Arrays.asList(mapper.readValue(response, EvaluationElement[].class));
//  }
  
  
  
  public static Map<String, String> getFileToMap(String requestHeader) {
    Map<String, String> headers = new HashMap<>();
    headers = Arrays.stream(requestHeader.split("\\r?\\n"))
        .map(s -> s.replaceFirst(":", " : ").replaceAll("\\s+", " ").split(" : "))
        .collect(Collectors.toMap(s -> s[0], s -> s[1]));
    return headers;
  }

  public static String fileResourceToString(String resourcePath) throws Exception {
    return new String(Files.readAllBytes(Paths.get(Resources.getResource(resourcePath).toURI())),
        StandardCharsets.UTF_8);
  }
}
