package com.everis.prueba.consumo.business.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.everis.prueba.consumo.mock.first.thirdparty.ApiResponse;
import com.everis.prueba.consumo.mock.second.thirdparty.ApiResponse2;
import com.everis.prueba.consumo.model.api.Response;
import com.everis.prueba.consumo.util.UtilTest;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

@RunWith(MockitoJUnitRunner.class)
public class PruebaServiceImplTest {

  @Mock
  PruebaServiceSender sender;

  @Mock
  PruebaServiceProcessor processor;

  @InjectMocks
  PruebaServiceImpl test;

  private UtilTest util;


  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void obtenerDataTest() throws Exception {
    util = new UtilTest();
    String jsonResponse1 = "json/response.json";
    ApiResponse res1 = util.getLoanJsonResponse01(jsonResponse1);

    String jsonResponse2 = "json/response2.json";
    Response res2 = util.getLoanJsonResponse02(jsonResponse2);
    when(sender.callApi()).thenReturn(Single.just(res1));

    when(processor.convertResponse(any())).thenReturn(res2);
    
    Assert.assertNotNull(test.obtenerData());
    TestObserver<Response> testSingleResult = test.obtenerData().test();
    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();

  }

  @Test
  public void obtenerData2Test() throws Exception {
    util = new UtilTest();
    String jsonResponse1 = "json/response3.json";
    List<ApiResponse2> res1 = util.getLoanJsonResponse03(jsonResponse1);
    when(sender.callApi2()).thenReturn(Single.just(res1));

    Assert.assertNotNull(test.obtenerData2("D47500049012"));
    TestObserver<Response> testSingleResult = test.obtenerData2("D47500049012").test();
    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();

  }
}
