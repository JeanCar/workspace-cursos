package com.everis.prueba.consumo.business.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import com.everis.prueba.consumo.business.impl.PruebaServiceProcessor;
import com.everis.prueba.consumo.config.ApplicationProperties;
import com.everis.prueba.consumo.util.ExceptionUtil;

public class ExceptionUtilTest extends Exception {

  private static final long serialVersionUID = 1L;

  @Mock
  ApplicationProperties properties;

  @InjectMocks
  PruebaServiceProcessor test;

  @Before
  public void setUp() throws Exception {}

  @Test
  public void createObjectTest() {
    Assert.assertNotNull(ExceptionUtil.onSingleError(new ExceptionUtilTest()));
  }

  public ExceptionUtilTest() {
    super("Error de data");
  }
}
