package com.everis.prueba.consumo.bdd.steps;

import org.springframework.boot.web.server.LocalServerPort;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractBaseSteps {

  @LocalServerPort
  public int port;

  public String uri;
  public String header;

  public String getBasePath() {
    return "http://localhost:" + 9003;
  }

}
