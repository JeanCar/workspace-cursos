#Author: wvasquec@everis.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
@prueba
Feature: Test BDD, Pruebas de Comportamiento
    
    Background:
    Given Se realiza un GET a la URI "/api/everis/prueba/consumo/v1"
    And Se le envia el header bdd/header/headers.txt
    
    @Steps01
    Scenario Outline: Steps01 Realiza consulta de datos
    When Se usa el RECURSO <recurso> para obtener la consulta
    Then El RESPONSE muestra el estado <code>, retornando con campos validos

      Examples:
      | recurso     |code |
      |/obtenerDatos|200  | 
   
    @Steps02
 	Scenario Outline: Steps02 Validar la taxonomía de la URI 
    When Se usa el RECURSO <recurso> para evaluar la sintaxis
    Then El RESPONSE03 muestra el estado <code>, indicando que no se encontro el recurso

      Examples:
      | recurso 	|   code |
      |/obtieneDato	|   404  |
      |/obtenerDatas|   404  |
      |/obtener-data|   404  |   