package com.everis.prueba.consumo.model.api.header;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import groovy.transform.ToString;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 * <br/>
 * <b>Class</b>: RequestHeaders<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */


@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestHeaders {

  @Pattern(regexp = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-"
      + "[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$")
  @ApiParam(value = "Este campo es un valor estandar ya existente y sera usado como identificador.",
      name = "Request-ID", required = true, example = "Request-ID")
  @Size(min = 36)
  @NotNull
  @NotEmpty
  public String getRequestId() {
    return null;
  }

  @ApiParam(value = "Fecha de la peticion", required = true, name = "request-date",
      example = "request-date")
  @NotNull
  @NotEmpty
  @Size(max = 30)
  public String getRequestDate() {
    return null;
  }

  @ApiParam(value = "ID del componente o API que realiza la peticion.", name = "caller-name",
      required = true, example = "caller-name")
  @NotNull
  @NotEmpty
  @Size(min = 5, max = 100)
  public String getCallerName() {
    return null;
  }

  @ApiParam(
      value = "Codigo de la aplicacion  que invoca el servicio. Se debe usar el codigo de"
          + " 2 caracteres que tienen asignada las aplicaciones, puede ser el canal.",
      name = "app-code", required = true, example = "app-code")
  @NotNull
  @NotEmpty
  @Size(min = 2, max = 2)
  public String getApplicationCode() {
    return null;

  }
}
