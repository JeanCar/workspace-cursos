
package com.everis.prueba.consumo.mock.first.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "chargeMeanType",
    "chargeProduct",
    "accountingAccountTransfer"
})
public class ChargeMeanInformation {

    @JsonProperty("chargeMeanType")
    private ChargeMeanType chargeMeanType;
    @JsonProperty("chargeProduct")
    private ChargeProduct chargeProduct;
    @JsonProperty("accountingAccountTransfer")
    private AccountingAccountTransfer accountingAccountTransfer;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("chargeMeanType")
    public ChargeMeanType getChargeMeanType() {
        return chargeMeanType;
    }

    @JsonProperty("chargeMeanType")
    public void setChargeMeanType(ChargeMeanType chargeMeanType) {
        this.chargeMeanType = chargeMeanType;
    }

    @JsonProperty("chargeProduct")
    public ChargeProduct getChargeProduct() {
        return chargeProduct;
    }

    @JsonProperty("chargeProduct")
    public void setChargeProduct(ChargeProduct chargeProduct) {
        this.chargeProduct = chargeProduct;
    }

    @JsonProperty("accountingAccountTransfer")
    public AccountingAccountTransfer getAccountingAccountTransfer() {
        return accountingAccountTransfer;
    }

    @JsonProperty("accountingAccountTransfer")
    public void setAccountingAccountTransfer(AccountingAccountTransfer accountingAccountTransfer) {
        this.accountingAccountTransfer = accountingAccountTransfer;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
