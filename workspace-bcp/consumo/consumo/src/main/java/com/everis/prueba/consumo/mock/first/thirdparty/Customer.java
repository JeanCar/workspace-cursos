
package com.everis.prueba.consumo.mock.first.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bankingType",
    "bankingSegment"
})
public class Customer {

    @JsonProperty("bankingType")
    private BankingType bankingType;
    @JsonProperty("bankingSegment")
    private BankingSegment bankingSegment;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("bankingType")
    public BankingType getBankingType() {
        return bankingType;
    }

    @JsonProperty("bankingType")
    public void setBankingType(BankingType bankingType) {
        this.bankingType = bankingType;
    }

    @JsonProperty("bankingSegment")
    public BankingSegment getBankingSegment() {
        return bankingSegment;
    }

    @JsonProperty("bankingSegment")
    public void setBankingSegment(BankingSegment bankingSegment) {
        this.bankingSegment = bankingSegment;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
