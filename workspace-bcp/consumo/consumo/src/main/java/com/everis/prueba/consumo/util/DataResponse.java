package com.everis.prueba.consumo.util;

import com.everis.prueba.consumo.model.api.Product;
import com.everis.prueba.consumo.model.api.Response;
import io.reactivex.Single;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;


/**
 * <br/>
 * <b>Class</b>: DataResponse<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Component
public class DataResponse {

  /**
   * This method constructResponseObject.
   * 
   * @return {@Link Single}
   */
  public Map<String, Single<Response>> constructResponseObject() {
    Map<String, Single<Response>> responseMap = new HashMap<String, Single<Response>>();
    responseMap.put("D47500049012", getResponse1());
    responseMap.put("D47500049013", getResponse2());
    responseMap.put("D47500049014", getResponse3());
    return responseMap;
  }

  /**
   * This method getResponse1.
   *
   * @return {@Link Single}
   */
  private Single<Response> getResponse1() {
    Response response = new Response();
    Product p = new Product();
    p.setCurrencyCode("D47500049012");
    p.setTioAux("CTG12");
    response.setProduct(p);
    response.setInfoProperties("prueba1");
    return Single.fromCallable(() -> response);
  }

  /**
   * This method getResponse2.
   *
   * @return {@Link Single}
   */
  private Single<Response> getResponse2() {
    Response response = new Response();
    Product p = new Product();
    p.setCurrencyCode("D47500049013");
    p.setTioAux("CTG13");
    response.setProduct(p);
    response.setInfoProperties("prueba2");
    return Single.fromCallable(() -> response);
  }

  /**
   * This method getResponse3.
   *
   * @return {@Link Single}
   */
  private Single<Response> getResponse3() {
    Response response = new Response();
    Product p = new Product();
    p.setCurrencyCode("D47500049014");
    p.setTioAux("CTG14");
    response.setProduct(p);
    response.setInfoProperties("prueba3");
    return Single.fromCallable(() -> response);
  }
}
