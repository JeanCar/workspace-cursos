
package com.everis.prueba.consumo.mock.first.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "currency",
    "family",
    "product"
})
public class ProductDetail__ {

    @JsonProperty("currency")
    private Currency__ currency;
    @JsonProperty("family")
    private Family_ family;
    @JsonProperty("product")
    private Product_ product;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("currency")
    public Currency__ getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(Currency__ currency) {
        this.currency = currency;
    }

    @JsonProperty("family")
    public Family_ getFamily() {
        return family;
    }

    @JsonProperty("family")
    public void setFamily(Family_ family) {
        this.family = family;
    }

    @JsonProperty("product")
    public Product_ getProduct() {
        return product;
    }

    @JsonProperty("product")
    public void setProduct(Product_ product) {
        this.product = product;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
