
package com.everis.prueba.consumo.mock.second.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "aux",
    "infoProperties"
})
public class ApiResponse2 {

    @JsonProperty("code")
    private String code;
    @JsonProperty("aux")
    private String aux;
    @JsonProperty("infoProperties")
    private String infoProperties;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("aux")
    public String getAux() {
        return aux;
    }

    @JsonProperty("aux")
    public void setAux(String aux) {
        this.aux = aux;
    }

    @JsonProperty("infoProperties")
    public String getInfoProperties() {
        return infoProperties;
    }

    @JsonProperty("infoProperties")
    public void setInfoProperties(String infoProperties) {
        this.infoProperties = infoProperties;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
