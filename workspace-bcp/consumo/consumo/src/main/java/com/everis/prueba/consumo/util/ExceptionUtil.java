package com.everis.prueba.consumo.util;

import com.everis.prueba.consumo.model.api.Response;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * <br/>
 * <b>Class</b>: ExceptionUtil<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionUtil {

  /**
   * Method to onSingleError.
   * 
   * @param ex {@link Throwable}
   * @return {@link SingleSource}
   */
  public static SingleSource<Response> onSingleError(Throwable ex) {
    Response r = new Response();
    r.setInfoProperties("error: " + ex.getLocalizedMessage());
    return Single.just(r);

  }

}
