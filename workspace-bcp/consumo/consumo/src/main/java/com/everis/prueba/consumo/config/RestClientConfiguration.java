package com.everis.prueba.consumo.config;

import com.everis.prueba.consumo.proxy.PruebaApi;
import com.everis.prueba.consumo.proxy.PruebaApi2;
import io.reactivex.Single;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Spring configuration for external Rest endpoints using <b>Retrofit</b>.<br/>
 * <b>Class</b>: RestClientConfiguration<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Configuration
@Lazy
public class RestClientConfiguration {

  private static Retrofit retrofit = null;

  @Autowired
  private ApplicationProperties properties;

  /**
   * callApi getClient.
   *
   * @param baseUrl {@link String}
   * @return {@link Single}
   */
  
  //Cuando JavaConfig encuentre dicho método, ejecutará ese método y registrará el valor 
  //de retorno como un bean dentro de a BeanFactory
  @Bean
  public static Retrofit getClient(String baseUrl) {
    if (retrofit == null) {
      OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
      retrofit =
          new Retrofit.Builder().baseUrl(baseUrl)
              .addConverterFactory(GsonConverterFactory.create())
             .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
              .client(httpClient.build())
              .build();
    }
    return retrofit;

  }

  public PruebaApi getProxy1() {
    return RestClientConfiguration.getClient(properties.getBaseUrl()).create(PruebaApi.class);
  }
  
  public PruebaApi2 getProxy2() {
    return RestClientConfiguration.getClient(properties.getBaseUrl()).create(PruebaApi2.class);
  }
}
