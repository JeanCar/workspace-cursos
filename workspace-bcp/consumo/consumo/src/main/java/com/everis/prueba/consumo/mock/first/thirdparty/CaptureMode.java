
package com.everis.prueba.consumo.mock.first.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entryMode",
    "pinEntry"
})
public class CaptureMode {

    @JsonProperty("entryMode")
    private String entryMode;
    @JsonProperty("pinEntry")
    private Boolean pinEntry;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("entryMode")
    public String getEntryMode() {
        return entryMode;
    }

    @JsonProperty("entryMode")
    public void setEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    @JsonProperty("pinEntry")
    public Boolean getPinEntry() {
        return pinEntry;
    }

    @JsonProperty("pinEntry")
    public void setPinEntry(Boolean pinEntry) {
        this.pinEntry = pinEntry;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
