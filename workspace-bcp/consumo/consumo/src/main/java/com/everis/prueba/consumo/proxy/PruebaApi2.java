package com.everis.prueba.consumo.proxy;

import com.everis.prueba.consumo.mock.second.thirdparty.ApiResponse2;
import java.util.List;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;


/**
 * <b>Class</b>: PruebaApi<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

public interface PruebaApi2 {

  /**
   * callApi getEvaluationsUsingGet3.
   *
   * @return {@link Single}
   */
  @Headers({"Content-Type:application/stream+json;charset=UTF-8"})
  @GET("/v2/5de13595320000fc248092af")
  Single<List<ApiResponse2>> getListaUsingGet2();

}
