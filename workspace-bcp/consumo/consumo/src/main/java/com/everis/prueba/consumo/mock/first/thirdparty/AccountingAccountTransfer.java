
package com.everis.prueba.consumo.mock.first.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "accountingAccount",
    "registerDate",
    "affectationType",
    "sourceApplication",
    "transactionType",
    "referenceTransaction",
    "branchOffice",
    "agency",
    "costCenter",
    "intermediaryAccount",
    "intermediaryAgency",
    "intermediaryBranchOffice",
    "customerCase",
    "referenceDescription"
})
public class AccountingAccountTransfer {

    @JsonProperty("accountingAccount")
    private AccountingAccount accountingAccount;
    @JsonProperty("registerDate")
    private String registerDate;
    @JsonProperty("affectationType")
    private AffectationType affectationType;
    @JsonProperty("sourceApplication")
    private SourceApplication sourceApplication;
    @JsonProperty("transactionType")
    private TransactionType transactionType;
    @JsonProperty("referenceTransaction")
    private String referenceTransaction;
    @JsonProperty("branchOffice")
    private BranchOffice branchOffice;
    @JsonProperty("agency")
    private Agency agency;
    @JsonProperty("costCenter")
    private CostCenter costCenter;
    @JsonProperty("intermediaryAccount")
    private IntermediaryAccount intermediaryAccount;
    @JsonProperty("intermediaryAgency")
    private IntermediaryAgency intermediaryAgency;
    @JsonProperty("intermediaryBranchOffice")
    private IntermediaryBranchOffice intermediaryBranchOffice;
    @JsonProperty("customerCase")
    private CustomerCase customerCase;
    @JsonProperty("referenceDescription")
    private ReferenceDescription referenceDescription;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("accountingAccount")
    public AccountingAccount getAccountingAccount() {
        return accountingAccount;
    }

    @JsonProperty("accountingAccount")
    public void setAccountingAccount(AccountingAccount accountingAccount) {
        this.accountingAccount = accountingAccount;
    }

    @JsonProperty("registerDate")
    public String getRegisterDate() {
        return registerDate;
    }

    @JsonProperty("registerDate")
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    @JsonProperty("affectationType")
    public AffectationType getAffectationType() {
        return affectationType;
    }

    @JsonProperty("affectationType")
    public void setAffectationType(AffectationType affectationType) {
        this.affectationType = affectationType;
    }

    @JsonProperty("sourceApplication")
    public SourceApplication getSourceApplication() {
        return sourceApplication;
    }

    @JsonProperty("sourceApplication")
    public void setSourceApplication(SourceApplication sourceApplication) {
        this.sourceApplication = sourceApplication;
    }

    @JsonProperty("transactionType")
    public TransactionType getTransactionType() {
        return transactionType;
    }

    @JsonProperty("transactionType")
    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    @JsonProperty("referenceTransaction")
    public String getReferenceTransaction() {
        return referenceTransaction;
    }

    @JsonProperty("referenceTransaction")
    public void setReferenceTransaction(String referenceTransaction) {
        this.referenceTransaction = referenceTransaction;
    }

    @JsonProperty("branchOffice")
    public BranchOffice getBranchOffice() {
        return branchOffice;
    }

    @JsonProperty("branchOffice")
    public void setBranchOffice(BranchOffice branchOffice) {
        this.branchOffice = branchOffice;
    }

    @JsonProperty("agency")
    public Agency getAgency() {
        return agency;
    }

    @JsonProperty("agency")
    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    @JsonProperty("costCenter")
    public CostCenter getCostCenter() {
        return costCenter;
    }

    @JsonProperty("costCenter")
    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    @JsonProperty("intermediaryAccount")
    public IntermediaryAccount getIntermediaryAccount() {
        return intermediaryAccount;
    }

    @JsonProperty("intermediaryAccount")
    public void setIntermediaryAccount(IntermediaryAccount intermediaryAccount) {
        this.intermediaryAccount = intermediaryAccount;
    }

    @JsonProperty("intermediaryAgency")
    public IntermediaryAgency getIntermediaryAgency() {
        return intermediaryAgency;
    }

    @JsonProperty("intermediaryAgency")
    public void setIntermediaryAgency(IntermediaryAgency intermediaryAgency) {
        this.intermediaryAgency = intermediaryAgency;
    }

    @JsonProperty("intermediaryBranchOffice")
    public IntermediaryBranchOffice getIntermediaryBranchOffice() {
        return intermediaryBranchOffice;
    }

    @JsonProperty("intermediaryBranchOffice")
    public void setIntermediaryBranchOffice(IntermediaryBranchOffice intermediaryBranchOffice) {
        this.intermediaryBranchOffice = intermediaryBranchOffice;
    }

    @JsonProperty("customerCase")
    public CustomerCase getCustomerCase() {
        return customerCase;
    }

    @JsonProperty("customerCase")
    public void setCustomerCase(CustomerCase customerCase) {
        this.customerCase = customerCase;
    }

    @JsonProperty("referenceDescription")
    public ReferenceDescription getReferenceDescription() {
        return referenceDescription;
    }

    @JsonProperty("referenceDescription")
    public void setReferenceDescription(ReferenceDescription referenceDescription) {
        this.referenceDescription = referenceDescription;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
