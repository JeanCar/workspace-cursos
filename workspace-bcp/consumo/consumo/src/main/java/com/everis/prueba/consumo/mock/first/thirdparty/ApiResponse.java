
package com.everis.prueba.consumo.mock.first.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "currency",
    "amount",
    "chargeMeanInformation",
    "transferLimitControl",
    "depositMeanInformation",
    "exchangeRateInformation"
})
public class ApiResponse {

    @JsonProperty("currency")
    private Currency currency;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("chargeMeanInformation")
    private ChargeMeanInformation chargeMeanInformation;
    @JsonProperty("transferLimitControl")
    private TransferLimitControl transferLimitControl;
    @JsonProperty("depositMeanInformation")
    private DepositMeanInformation depositMeanInformation;
    @JsonProperty("exchangeRateInformation")
    private ExchangeRateInformation exchangeRateInformation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("currency")
    public Currency getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("chargeMeanInformation")
    public ChargeMeanInformation getChargeMeanInformation() {
        return chargeMeanInformation;
    }

    @JsonProperty("chargeMeanInformation")
    public void setChargeMeanInformation(ChargeMeanInformation chargeMeanInformation) {
        this.chargeMeanInformation = chargeMeanInformation;
    }

    @JsonProperty("transferLimitControl")
    public TransferLimitControl getTransferLimitControl() {
        return transferLimitControl;
    }

    @JsonProperty("transferLimitControl")
    public void setTransferLimitControl(TransferLimitControl transferLimitControl) {
        this.transferLimitControl = transferLimitControl;
    }

    @JsonProperty("depositMeanInformation")
    public DepositMeanInformation getDepositMeanInformation() {
        return depositMeanInformation;
    }

    @JsonProperty("depositMeanInformation")
    public void setDepositMeanInformation(DepositMeanInformation depositMeanInformation) {
        this.depositMeanInformation = depositMeanInformation;
    }

    @JsonProperty("exchangeRateInformation")
    public ExchangeRateInformation getExchangeRateInformation() {
        return exchangeRateInformation;
    }

    @JsonProperty("exchangeRateInformation")
    public void setExchangeRateInformation(ExchangeRateInformation exchangeRateInformation) {
        this.exchangeRateInformation = exchangeRateInformation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
