package com.everis.services.ux.api.response;

import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(value = Include.NON_NULL)
@Getter
@Setter
@ApiModel(
    description = "Contiene datos de consulta de credito.")
public class ResponseUx {

  @ApiModelProperty(name = "id", value = "Código del crédito Consist.",
      required = true)
  @Valid
  private String id;
  
  @ApiModelProperty(name = "productDetailTioauxCode",
      value = "\"Código de producto: TIO (Codigo Tipo Operación) "
          + "+ AUX (Codigo Auxiliar Operación)\r\n" + "Valores Posibles\r\n"
          + "AFBVEH   =  Banco - Vehiculo\r\n" + "AFCINM    " + "=  "
          + "Credileasing - inmueble\r\n" + "CCRSBY   " + "=  Stan by emitidos\"",
      required = true)
  @Valid
  private String productDetailTioauxCode;
  
  @ApiModelProperty(name = "debtTypecode",
      value = "\"Indicador de tipo de deuda\r\n" + "Valores para Tipo de deuda:\r\n"
          + "“DIR” Directa\r\n" + "“IND” Indirecta\r\n" + "“CTG” Contingente\"\r\n" + "",
      required = true)
  @Valid
  private String debtTypeCode;
}
