package com.everis.services.ux.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.everis.services.ux.api.request.IdSearchPathParam;
import com.everis.services.ux.api.response.ResponseUx;
import com.everis.services.ux.service.ApiUxService;
import io.reactivex.Single;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/ux/v1")
@Slf4j
public class ApiuxController {

  @Autowired
  private ApiUxService corporateLoansService;

  /**
   * sendToHostBean.
   */
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      value = "Consulta genérica de colocaciones por cliente (Créditos Consist)."
          + " Permite realizar consultas por código de la línea de crédito. Los "
          + "datos de salida incluyen los principales campos asociados al desembolso.",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET",
      notes = "classpath:swagger/notes/apiux.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecutó satisfactoriamente.",
          response = ResponseUx.class),
      @ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Bad Request"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  @GetMapping(value = "/obtenerDatos/{id}")
  public Single<ResponseUx> sendToHostBean(@PathVariable("id") IdSearchPathParam pathParams) {

    Single<ResponseUx> response = null;

    try {
      log.info("PathVariable: " + pathParams.getId());
      response = corporateLoansService.obtenerRespuestaCorporateLoans(pathParams.getId());
//      log.info("Response: " + EverisUtil.toJson(response));
    } catch (Exception e) {
      log.error("Error: " + e);
    }

    return response;
  }
}
