package com.everis.services.ux.service;

import com.everis.services.ux.api.response.ResponseUx;
import io.reactivex.Single;

public interface ApiUxService {

  public Single<ResponseUx> obtenerRespuestaCorporateLoans(String id); 
}
