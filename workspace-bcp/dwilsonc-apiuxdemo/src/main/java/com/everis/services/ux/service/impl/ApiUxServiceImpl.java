package com.everis.services.ux.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.everis.services.ux.api.response.ResponseUx;
import com.everis.services.ux.config.ApplicationProperties;
import com.everis.services.ux.service.ApiUxProcessor;
import com.everis.services.ux.service.ApiUxSender;
import com.everis.services.ux.service.ApiUxService;
import io.reactivex.Single;

@Service
public class ApiUxServiceImpl implements ApiUxService {

  @Autowired
  private ApplicationProperties properties;

  @Autowired
  private ApiUxSender sender;
  
  @Autowired
  private ApiUxProcessor processor;
  
  @Override
  public Single<ResponseUx> obtenerRespuestaCorporateLoans(String id) {
    
    return sender
        .obtenerRespuestaDeApiCross(properties.getBaseUrl(), id)
        .flatMap(processor::conversionResponseHaciaUX);
  }
}
