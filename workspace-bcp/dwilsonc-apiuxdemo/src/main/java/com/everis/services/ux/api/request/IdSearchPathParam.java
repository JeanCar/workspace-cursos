package com.everis.services.ux.api.request;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "Clase contenedora de los parametros que el API acepta.")
@ToString
public class IdSearchPathParam {

  @Size(min = 12, max = 12, message = "El campo debe tener 12 caracteres.")
  @ApiParam(required = true, value = "Código de linea de credito.", example = "D47500049076" + "",
      hidden = true, name = "id")
  @Valid
  private String id;
}
