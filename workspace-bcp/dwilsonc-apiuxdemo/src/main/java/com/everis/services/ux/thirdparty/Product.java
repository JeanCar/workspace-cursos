package com.everis.services.ux.thirdparty;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Contiene los productos.
 */
@ApiModel(description = "Contiene los productos.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-30T14:44:30.438-05:00")
public class Product {
  @SerializedName("tioaux")
  private TioAux tioaux = null;

  public Product tioaux(TioAux tioaux) {
    this.tioaux = tioaux;
    return this;
  }

   /**
   * Get tioaux
   * @return tioaux
  **/
  @ApiModelProperty(value = "")
  public TioAux getTioaux() {
    return tioaux;
  }

  public void setTioaux(TioAux tioaux) {
    this.tioaux = tioaux;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return Objects.equals(this.tioaux, product.tioaux);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tioaux);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Product {\n");
    
    sb.append("    tioaux: ").append(toIndentedString(tioaux)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

