package com.everis.services.ux.thirdparty;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Contiene datos de Loan
 */
@ApiModel(description = "Contiene datos de Loan")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-30T14:44:30.438-05:00")
public class Response {
  @SerializedName("corporateLoanId")
  private String corporateLoanId = null;

  @SerializedName("debtType")
  private DebtType debtType = null;

  @SerializedName("productDetail")
  private Product productDetail = null;

  public Response corporateLoanId(String corporateLoanId) {
    this.corporateLoanId = corporateLoanId;
    return this;
  }

   /**
   * C�digo del cr�dito Consist.
   * @return corporateLoanId
  **/
  @ApiModelProperty(example = "D47500049076", value = "C�digo del cr�dito Consist.")
  public String getCorporateLoanId() {
    return corporateLoanId;
  }

  public void setCorporateLoanId(String corporateLoanId) {
    this.corporateLoanId = corporateLoanId;
  }

  public Response debtType(DebtType debtType) {
    this.debtType = debtType;
    return this;
  }

   /**
   * Get debtType
   * @return debtType
  **/
  @ApiModelProperty(value = "")
  public DebtType getDebtType() {
    return debtType;
  }

  public void setDebtType(DebtType debtType) {
    this.debtType = debtType;
  }

  public Response productDetail(Product productDetail) {
    this.productDetail = productDetail;
    return this;
  }

   /**
   * Get productDetail
   * @return productDetail
  **/
  @ApiModelProperty(value = "")
  public Product getProductDetail() {
    return productDetail;
  }

  public void setProductDetail(Product productDetail) {
    this.productDetail = productDetail;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Response response = (Response) o;
    return Objects.equals(this.corporateLoanId, response.corporateLoanId) &&
        Objects.equals(this.debtType, response.debtType) &&
        Objects.equals(this.productDetail, response.productDetail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(corporateLoanId, debtType, productDetail);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Response {\n");
    
    sb.append("    corporateLoanId: ").append(toIndentedString(corporateLoanId)).append("\n");
    sb.append("    debtType: ").append(toIndentedString(debtType)).append("\n");
    sb.append("    productDetail: ").append(toIndentedString(productDetail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

