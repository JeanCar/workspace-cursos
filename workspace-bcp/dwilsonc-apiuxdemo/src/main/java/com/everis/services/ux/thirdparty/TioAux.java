package com.everis.services.ux.thirdparty;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Contiene el TioAux codigo y descripcion.
 */
@ApiModel(description = "Contiene el TioAux codigo y descripcion.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-30T14:44:30.438-05:00")
public class TioAux {
  @SerializedName("code")
  private String code = null;

  public TioAux code(String code) {
    this.code = code;
    return this;
  }

   /**
   * C�digo de producto.
   * @return code
  **/
  @ApiModelProperty(example = "PRLPRE", value = "C�digo de producto.")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TioAux tioAux = (TioAux) o;
    return Objects.equals(this.code, tioAux.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TioAux {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

