# Implementation Notes

### Acerca de la funcionalidad expuesta
Api de prueba que retorna la consulta de datos de transacciones

**Path variable** = {corporateLoanId}

<br/>

### URI de acceso a la API
| Método | URI |
|--------|-------------|
|GET| /api/examen/prueba/v1/obtenerDatos|



### Query Parameters válidos en esta versión de la API
| Query Param |Tipo de Dato | Descripción | Valores Permitidos |
|-------------|-------------|-------------|--------------------|
|corporateLoanId	 | String    |Código de linea de credito| | 

<br/>

### Usos válidos de Query Parameters
| Resultado Esperado | Path Param | Query Parameters requeridos | Query Parameters opcionales | Query Param Extrafield |
|---|---|---|---|---|
| Código de linea de credito | corporateLoanId | - | - | - |


<br/>



### Código de errores del Servicio en esta versión de la API
| Código | Descripción |
|--------|-------------|
| TL0000 | El servicio se ejecutó de manera satisfactoria                           |
| TL0001 | No se encontró resultados para los criterios de búsqueda                |
| TL0003 | Los datos proporcionados no son válidos                                  |
| TL0004 | Ocurrió un error en el servicio Externo consumido                        |
| TL0005 | Ocurrió un error en la comunicación con el BackEnd                       |
| TL0006 | Los datos proporcionados no cumplen con los criterios para ser procesado |
| TL0007 | Ocurrió un error en el Backend                                           |
| TL0009 | Problemas de Timeout en el Servicio Backend |
| TL9999 | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico   |

<br/>
