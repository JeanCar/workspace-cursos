package com.example.prueba.everis.config;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * Spring configuration for external Config-Server<b>Retrofit</b>.<br/>
 * <b>Class</b>: ApplicationProperties<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Configuration
@Getter
@Setter
@ToString
@PropertySource("classpath:application.yml")
public class ApplicationProperties {

  @NotNull
  @Value("${application.component}")
  private String component;

  @NotNull
  @Value("${application.api.path}")
  private String baseUrl;

  @NotNull
  @Value("${application.api.connect-timeout}")
  private String connectTimeout;

  @NotNull
  @Value("${application.api.read-timeout}")
  private String readTimout;

  @NotNull
  @Value("${application.api.write-timeout}")
  private String writeTimout;

  @NotNull
  @Value("${application.test.info}")
  private String fileProperty;

}
