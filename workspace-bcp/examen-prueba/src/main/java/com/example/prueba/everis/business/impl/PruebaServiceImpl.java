package com.example.prueba.everis.business.impl;

import com.example.prueba.everis.business.PruebaService;
import com.example.prueba.everis.model.api.Response;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <br/>
 * Class service that contains the necessary methods to process the data and business logic that
 * will consume the REST class LoanEvaluationController<br/>
 * <b>Class</b>: PruebaServiceImpl<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Service
@Slf4j
public class PruebaServiceImpl implements PruebaService {

  @Autowired
  private PruebaServiceSender sender;

  @Autowired
  private PruebaServiceProcessor processor;

  /**
   * This method obtenerData.
   *
   * @return {@Link Single}
   */

  @HystrixCommand(fallbackMethod = "getDefault")
  @Override
  public Single<Response> obtenerData() {
    return sender.callApi()
        // .lift(CircuitBreakerOperator.of(circuitBreaker))
        .map(processor::convertResponsePostToUx)
        // .subscribeOn(Schedulers.computation())
        // .onErrorResumeNext(ExceptionUtils::onLoanExceptionOrderPostError)
        .doOnSuccess(succ -> log.info("Successful"))
        .doOnError(err -> log.error("Error al consultar loanEvaluation", err));
  }


  public Single<Response> getDefault() {

    return Single.just(new Response());

  }
}
