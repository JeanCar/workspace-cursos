
package com.example.prueba.everis.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "classification",
    "dealType",
    "customer",
    "targetRate",
    "referenceDescription"
})
public class ExchangeRateInformation {

    @JsonProperty("classification")
    private Classification classification;
    @JsonProperty("dealType")
    private DealType dealType;
    @JsonProperty("customer")
    private Customer_ customer;
    @JsonProperty("targetRate")
    private TargetRate targetRate;
    @JsonProperty("referenceDescription")
    private ReferenceDescription__ referenceDescription;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("classification")
    public Classification getClassification() {
        return classification;
    }

    @JsonProperty("classification")
    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    @JsonProperty("dealType")
    public DealType getDealType() {
        return dealType;
    }

    @JsonProperty("dealType")
    public void setDealType(DealType dealType) {
        this.dealType = dealType;
    }

    @JsonProperty("customer")
    public Customer_ getCustomer() {
        return customer;
    }

    @JsonProperty("customer")
    public void setCustomer(Customer_ customer) {
        this.customer = customer;
    }

    @JsonProperty("targetRate")
    public TargetRate getTargetRate() {
        return targetRate;
    }

    @JsonProperty("targetRate")
    public void setTargetRate(TargetRate targetRate) {
        this.targetRate = targetRate;
    }

    @JsonProperty("referenceDescription")
    public ReferenceDescription__ getReferenceDescription() {
        return referenceDescription;
    }

    @JsonProperty("referenceDescription")
    public void setReferenceDescription(ReferenceDescription__ referenceDescription) {
        this.referenceDescription = referenceDescription;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
