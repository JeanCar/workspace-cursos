
package com.example.prueba.everis.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "depositMeanType",
    "depositProduct",
    "customerAccountTransfer"
})
public class DepositMeanInformation {

    @JsonProperty("depositMeanType")
    private DepositMeanType depositMeanType;
    @JsonProperty("depositProduct")
    private DepositProduct depositProduct;
    @JsonProperty("customerAccountTransfer")
    private CustomerAccountTransfer customerAccountTransfer;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("depositMeanType")
    public DepositMeanType getDepositMeanType() {
        return depositMeanType;
    }

    @JsonProperty("depositMeanType")
    public void setDepositMeanType(DepositMeanType depositMeanType) {
        this.depositMeanType = depositMeanType;
    }

    @JsonProperty("depositProduct")
    public DepositProduct getDepositProduct() {
        return depositProduct;
    }

    @JsonProperty("depositProduct")
    public void setDepositProduct(DepositProduct depositProduct) {
        this.depositProduct = depositProduct;
    }

    @JsonProperty("customerAccountTransfer")
    public CustomerAccountTransfer getCustomerAccountTransfer() {
        return customerAccountTransfer;
    }

    @JsonProperty("customerAccountTransfer")
    public void setCustomerAccountTransfer(CustomerAccountTransfer customerAccountTransfer) {
        this.customerAccountTransfer = customerAccountTransfer;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
