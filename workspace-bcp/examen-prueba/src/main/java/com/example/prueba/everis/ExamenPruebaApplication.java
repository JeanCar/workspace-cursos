package com.example.prueba.everis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.ComponentScan;

/**
 * <br/>
 * Class service that contains the necessary methods to process the data and business logic that
 * will consume the REST class LoanEvaluationController<br/>
 * <b>Class</b>: ExamenPruebaApplication<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.prueba"})
@EnableEurekaServer
@EnableHystrixDashboard
@EnableCircuitBreaker
public class ExamenPruebaApplication {

  public static void main(String[] args) {
    SpringApplication.run(ExamenPruebaApplication.class, args);
  }

}
