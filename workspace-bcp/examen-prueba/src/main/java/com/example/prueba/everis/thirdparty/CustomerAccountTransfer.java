
package com.example.prueba.everis.thirdparty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "registerDate",
    "utc",
    "referenceDescriptions"
})
public class CustomerAccountTransfer {

    @JsonProperty("registerDate")
    private String registerDate;
    @JsonProperty("utc")
    private String utc;
    @JsonProperty("referenceDescriptions")
    private List<ReferenceDescription_> referenceDescriptions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("registerDate")
    public String getRegisterDate() {
        return registerDate;
    }

    @JsonProperty("registerDate")
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    @JsonProperty("utc")
    public String getUtc() {
        return utc;
    }

    @JsonProperty("utc")
    public void setUtc(String utc) {
        this.utc = utc;
    }

    @JsonProperty("referenceDescriptions")
    public List<ReferenceDescription_> getReferenceDescriptions() {
        return referenceDescriptions;
    }

    @JsonProperty("referenceDescriptions")
    public void setReferenceDescriptions(List<ReferenceDescription_> referenceDescriptions) {
        this.referenceDescriptions = referenceDescriptions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
