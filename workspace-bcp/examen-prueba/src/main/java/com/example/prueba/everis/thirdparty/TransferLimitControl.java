
package com.example.prueba.everis.thirdparty;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cardId",
    "captureMode",
    "productDetail"
})
public class TransferLimitControl {

    @JsonProperty("cardId")
    private String cardId;
    @JsonProperty("captureMode")
    private CaptureMode captureMode;
    @JsonProperty("productDetail")
    private ProductDetail_ productDetail;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cardId")
    public String getCardId() {
        return cardId;
    }

    @JsonProperty("cardId")
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @JsonProperty("captureMode")
    public CaptureMode getCaptureMode() {
        return captureMode;
    }

    @JsonProperty("captureMode")
    public void setCaptureMode(CaptureMode captureMode) {
        this.captureMode = captureMode;
    }

    @JsonProperty("productDetail")
    public ProductDetail_ getProductDetail() {
        return productDetail;
    }

    @JsonProperty("productDetail")
    public void setProductDetail(ProductDetail_ productDetail) {
        this.productDetail = productDetail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
