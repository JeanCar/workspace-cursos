package com.example.prueba.everis.business.impl;


import com.example.prueba.everis.config.ApplicationProperties;

import com.example.prueba.everis.model.api.Product;
import com.example.prueba.everis.model.api.Response;
import com.example.prueba.everis.thirdparty.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**
 * <br/>
 * <b>Class</b>: PruebaServiceProcessor<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Component
public class PruebaServiceProcessor {

  @Autowired
  ApplicationProperties properties;

  /**
   * This method is used to parse the cross service response.
   *
   * @param postResponse {@Link ApiResponse}
   * @return {@Link Response}
   */
  public Response convertResponsePostToUx(ApiResponse postResponse) {

    Response r = new Response();
    Product p = new Product();
    p.setCurrencyCode(postResponse.getChargeMeanInformation().getChargeProduct().getProductDetail()
        .getCurrency().getCode());
    p.setTioAux(postResponse.getChargeMeanInformation().getAccountingAccountTransfer()
        .getAccountingAccount().getTioaux());
    r.setProduct(p);
    r.setInfoProperties(properties.getFileProperty());
    return r;
  }
}
