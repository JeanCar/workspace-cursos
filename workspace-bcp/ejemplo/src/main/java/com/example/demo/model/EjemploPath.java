package com.example.demo.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EjemploPath  implements CharSequence{

  @NotNull(message = "No debe ser nulo.")
  @NotEmpty(message = "No debe ser vacio")
  private String id;
  
  @Override
  public int length() {
    // TODO Auto-generated method stub
    return this.id.length();
  }

  @Override
  public char charAt(int index) {
    // TODO Auto-generated method stub
    return this.id.charAt(index);
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    // TODO Auto-generated method stub
    return this.id.subSequence(start, end);
  }

}
