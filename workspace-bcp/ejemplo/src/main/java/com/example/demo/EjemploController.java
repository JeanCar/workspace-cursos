package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.EjemploPath;
import com.example.demo.model.Persona;

@RestController
@RequestMapping("ejemplo")
public class EjemploController {

  
  @GetMapping(value = "/obtenerDatos")
  public String obtener() {
    return "hello";
  }
  
  @GetMapping(value = "/obtenerDatos2")
  public String obtener2() {
    return "hello2";
  }
  
  @GetMapping(value = "/obtenerDatos3/{id}")
  public String obtener3(@PathVariable("id") EjemploPath id) {
    return "prueba path Variable " + id.getId();
  }
 
  @GetMapping(value = "/obtenerDatos4")
  public String obtener4(@RequestParam(name="id") String query) {
    return "prueba Request Param " + query;
  }
  
  @PostMapping(value = "/obtenerDatos5")
  public String obtener5(@RequestBody Persona per) {
    return "prueba Post Body " +per.toString();
  }
}
