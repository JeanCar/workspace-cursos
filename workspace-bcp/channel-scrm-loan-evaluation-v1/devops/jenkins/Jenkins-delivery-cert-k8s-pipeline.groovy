@Library('jenkins-sharedlib@atla-new-features')
import sharedlib.JenkinsfileUtil

def utils = new JenkinsfileUtil(steps, this)
/* Project settings */
def project = "ATLA"
/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients = ""
def deploymentEnviroment = "cert"
// ocp | aks
def hosted = "aks"
// for AKS Resource Group, for anything else: def aksRG = ""
def aksRG = "RGEU2APPATLAC01"
// for AKS Cluster Name, for anything else: def aksCluster = ""
def aksCluster = "AKSEU2APPATLAC01"
// Ingress information for AKS
def ingressFQDN = "azingresscert.credito.bcp.com.pe"
def ingressCertificateSecret = "azingresscert-cert"
// registry for AKS
def azureRegistry = "acrgeu2inctc01.azurecr.io"
try {
  node {
    stage('Preparation') {
      cleanWs()
      utils.executePostExecutionTasks()
      utils.notifyByMail('START', recipients)
      checkout scm
      utils.prepare()
      //Setup parameters
      env.project = "${project}"
    }
    // values for k8s annotations
    def gitrepo = "${utils.currentGitURL}"
    def repobranch = "${utils.branchName}"
    def gitcommit = "${utils.currentGitCommit}"

    stage('Pre-Release') {
      utils.startReleaseMaven()
    }

    stage('Save Results') {
      utils.saveResultMaven('jar')
    }

    stage('Delivery app  to ' + deploymentEnviroment) {
      // map nested to hosted variable
      def hosted_on_enviroment = [
                                  ocp: [
                                          client: 'oc',
                                          credentials: [
                                            azure_registry:  [ id: "acr-devops-${deploymentEnviroment}", type: "UsernamePasswordMultiBinding", varuser: "AZURE_REGISTRY_USERNAME", varpassword: "AZURE_REGISTRY_PASSWORD" ],
                                            registry: [ id: "ocp_registry_credentials_${deploymentEnviroment}", type: "UsernamePasswordMultiBinding", varuser: "EXTERNAL_REGISTRY_USERNAME", varpassword: "EXTERNAL_REGISTRY_PASSWORD" ],
                                            host: [ id: "host_server_${deploymentEnviroment}_key", type: "SSHUserPrivateKeyBinding", varuser: "OS_USER", varkey: "KEYFILE" ]
                                          ]
                                  ],
                                  aks: [
                                          client: 'kubectl',
                                          credentials: [
                                            registry: [ id: "acr-devops-${deploymentEnviroment}", type: "UsernamePasswordMultiBinding", varuser: "EXTERNAL_REGISTRY_USERNAME", varpassword: "EXTERNAL_REGISTRY_PASSWORD" ],
                                            akslogin: [ id: "${project}".toLowerCase()+"_az_sp_${deploymentEnviroment}", type: "StringBinding", varkey: "AKS_SP" ],
                                            akscertificate: [ id: "${project}".toLowerCase()+"_az_certificate_${deploymentEnviroment}", type: "FileBinding", varkey: "AKS_CERTIFICATE" ],
                                            tenant: [ id: "${project}".toLowerCase()+"_az_tenant_${deploymentEnviroment}", type: "StringBinding", varkey: "AZ_TENANT" ]
                                          ]
                                  ]
      ]

      def deliverybranchname = "develop"
      utils.downloadGitRepo(project, "delivery-k8s", "${deliverybranchname}", "atla-token-bitbucket-${deploymentEnviroment}")
      sh "cp -r tmp-repository/devops/{ansible,docker} devops"

      credentialBinding = utils.getCredentialMap(hosted_on_enviroment[hosted].credentials)

      // Docker values
      def DOCKER_IP = sh (script: "grep \$(hostname) /etc/hosts | awk '{print \$1}'", returnStdout: true).trim()
      def DOCKER_SUPPORT_REGISTRY="acrgeu2inctc01.azurecr.io"
      def DOCKER_SUPPORT_REGISTRY_URL="https://${DOCKER_SUPPORT_REGISTRY}"
      def DOCKER_SUPPORT_REGISTRY_CREDENTIAL="acr-devops-${deploymentEnviroment}"
      def DOCKER_SUPPORT_IMAGE="${DOCKER_SUPPORT_REGISTRY}/"+"${project}".toLowerCase()+"/atlas-docker-image-deployment-tools:1.0.3"
      def DOCKER_SERVER="tcp://${DOCKER_IP}:2376"

      def group_id = utils.getGroupId().replace(".", "/")
      def pom_ap_name = utils.getApplicationName()
      def ap_version = utils.getApplicationVersion()

      def ansible_cmd_common = [
        workspace: "${WORKSPACE}", hosted_on: hosted, deployment_enviroment: deploymentEnviroment,
        client_k8s: hosted_on_enviroment."${hosted}"."client", pom_ap_name: pom_ap_name, ap_version: ap_version,
        group_id: group_id, namespace: "${project}".toLowerCase(), project: "${project}".toLowerCase(),
        git_repo: gitrepo, repo_branch: repobranch, git_commit: "${gitcommit}", delivery_branch: "${deliverybranchname}",
        aks_ingress_certificate: "${ingressFQDN}", aks_ingress_certificate_secret: "${ingressCertificateSecret}",
      ]

      def ansible_cmd = "ansible-playbook ${WORKSPACE}/devops/ansible/site.yml -v -i ${WORKSPACE}/devops/ansible/hosts --limit docker-local-${deploymentEnviroment},*${hosted}* \
        -e @${WORKSPACE}/devops/deploy/${deploymentEnviroment}-vars.yaml -e @${WORKSPACE}/devops/ansible/roles/vars/${deploymentEnviroment}'-vars.yaml' "
      for (item in ansible_cmd_common) {
        ansible_cmd+='-e '+item.key+'='+item.value+' '
      }

      withCredentials(credentialBinding){
        docker.withRegistry("${DOCKER_SUPPORT_REGISTRY_URL}", "${DOCKER_SUPPORT_REGISTRY_CREDENTIAL}") {
          def docker_cmd="docker run --rm -v ${WORKSPACE}:${WORKSPACE}:rw -u 1002:1014 --network host -e DOCKER_HOST=${DOCKER_SERVER} -e ANSIBLE_HOST_KEY_CHECKING=False"
          def docker_image="${DOCKER_SUPPORT_IMAGE}"
          def ansible_cmd_extra = [:]
          if ( hosted == 'aks' ) {
            sh "rm -fr ${WORKSPACE}/tmp; mkdir -p ${WORKSPACE}/tmp; cp ${AKS_CERTIFICATE} ${WORKSPACE}/tmp/AKS_CERTIFICATE"
            ansible_cmd_extra << [user: "''", ansible_user: "''", ansible_ssh_pass: "''", aks_cluster: "${aksCluster}", aks_rg: "${aksRG}", aks_service_principal: "${AKS_SP}",
                                  aks_certificate: "${WORKSPACE}/tmp/AKS_CERTIFICATE", az_tenant: "${AZ_TENANT}"] } else {
            ansible_cmd_extra << [user: "${OS_USER}", ansible_user: "${OS_USER}", aks_cluster: "''", aks_rg: "''", az_username: "''", az_password: "''", az_tenant: "''"]
            }
          if ( hosted == 'ocp' ) {
            sh "rm -fr ${WORKSPACE}/tmp; mkdir -p ${WORKSPACE}/tmp; cp ${KEYFILE} ${WORKSPACE}/tmp/SSH_KEY_FILE"
            ansible_cmd_extra << [ansible_ssh_private_key_file: "${WORKSPACE}/tmp/SSH_KEY_FILE", ansible_ssh_pass: "''"]
            ansible_cmd_extra << [azure_registry_username: "${AZURE_REGISTRY_USERNAME}", azure_registry_password: "${AZURE_REGISTRY_PASSWORD}"]
            }
          ansible_cmd_extra << [azure_docker_registry: "${azureRegistry}", external_registry_username: "${EXTERNAL_REGISTRY_USERNAME}", external_registry_password: "${EXTERNAL_REGISTRY_PASSWORD}"]
          for (item in ansible_cmd_extra) {
            ansible_cmd+="-e "+item.key+'='+item.value+' '
          }
          sh "${docker_cmd} ${docker_image} ${ansible_cmd}"
        }
      }
    }
      stage('Post Execution') {
        utils.printLogstashInfo('SUCCESS',"${project}","${deploymentEnviroment}","${hosted}")
        utils.executePostExecutionTasks()
        utils.notifyByMail('SUCCESS', recipients)
      }
    }
  } catch (Exception e) {
  node {
    utils.printLogstashInfo('FAILED',"${project}","${deploymentEnviroment}","${hosted}")
    utils.executeOnErrorExecutionTasks()
    utils.notifyByMail('FAIL', recipients)
    throw e
  }
}
