package com.bcp.atlas.services.channel.config;


import com.bcp.atlas.core.http.client.interceptor.AtlasExceptionInterceptor;
import com.bcp.atlas.core.http.client.interceptor.AtlasHttpLoggingInterceptor;
import com.bcp.atlas.core.http.client.interceptor.AuditHeadersInterceptor;
import com.bcp.atlas.core.http.utils.AtlasHttpClient;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.EvaluateApi;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.EvaluationsApi;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.LoanEvaluationApi;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Spring configuration for external Rest endpoints using <b>Retrofit</b>.<br/>
 * <b>Class</b>: RestClientConfiguration<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Configuration
@Lazy
public class RestClientConfiguration {

  @Bean
  LoanEvaluationApi loanEvaluationApi(
      @Value("${application.atlas.api.loanevaluation.path}") String loanApiBaseUrl,
      @Value("${application.atlas.api.loanevaluation.connect-timeout}") long connectTimeout,
      @Value("${application.atlas.api.loanevaluation.read-timeout}") long readTimeout,
      @Value("${application.atlas.api.loanevaluation.write-timeout}") long writeTimeout,
      CrossCdaInterceptor requestInterceptor,
      @Qualifier("atlas.default") CircuitBreaker circuitBreaker,
      OkHttpClient.Builder builder) {
    builder.interceptors().clear();
    builder.addInterceptor(new AuditHeadersInterceptor());
    builder.addInterceptor(requestInterceptor);
    builder.addInterceptor(new AtlasHttpLoggingInterceptor());
    builder.addInterceptor(new AtlasExceptionInterceptor());
    return AtlasHttpClient.builder()
        .clientBuilder(builder).baseUrl(loanApiBaseUrl)
        .circuitBreaker(circuitBreaker).connectTimeout(connectTimeout).readTimeout(readTimeout)
        .writeTimeout(writeTimeout).buildProxy(LoanEvaluationApi.class);
  }

  @Bean
  EvaluateApi evaluteApi(@Value("${application.atlas.api.evaluate.path}") String loanApiBaseUrl,
      @Value("${application.atlas.api.evaluate.connect-timeout}") long connectTimeout,
      @Value("${application.atlas.api.evaluate.read-timeout}") long readTimeout,
      @Value("${application.atlas.api.evaluate.write-timeout}") long writeTimeout,
      CrossExceptionInterceptor requestInterceptor,
      @Qualifier("atlas.default") CircuitBreaker circuitBreaker, OkHttpClient.Builder builder) {
    builder.interceptors().clear();
    builder.addInterceptor(new AuditHeadersInterceptor());
    builder.addInterceptor(requestInterceptor);
    builder.addInterceptor(new AtlasHttpLoggingInterceptor());
    builder.addInterceptor(new AtlasExceptionInterceptor());
    return AtlasHttpClient.builder()
        .clientBuilder(builder).baseUrl(loanApiBaseUrl)
        .circuitBreaker(circuitBreaker).connectTimeout(connectTimeout).readTimeout(readTimeout)
        .writeTimeout(writeTimeout).converterFactory(GsonConverterFactory.create())
        .buildProxy(EvaluateApi.class);
  }
  

  @Bean
  EvaluationsApi evaluationApi(
      @Value("${application.atlas.api.evaluation.path}") String loanApiBaseUrl,
      @Value("${application.atlas.api.evaluation.connect-timeout}") long connectTimeout,
      @Value("${application.atlas.api.evaluation.read-timeout}") long readTimeout,
      @Value("${application.atlas.api.evaluation.write-timeout}") long writeTimeout,
      CrossArchivoNegativoInterceptor requestInterceptor,
      @Qualifier("atlas.default") CircuitBreaker circuitBreaker,
      OkHttpClient.Builder builder) {
    builder.interceptors().clear();
    builder.addInterceptor(new AuditHeadersInterceptor());
    builder.addInterceptor(requestInterceptor);
    builder.addInterceptor(new AtlasHttpLoggingInterceptor());
    builder.addInterceptor(new AtlasExceptionInterceptor());
    return AtlasHttpClient.builder().clientBuilder(builder).baseUrl(loanApiBaseUrl)
        .circuitBreaker(circuitBreaker).connectTimeout(connectTimeout).readTimeout(readTimeout)
        .writeTimeout(writeTimeout).buildProxy(EvaluationsApi.class);
  }
}
