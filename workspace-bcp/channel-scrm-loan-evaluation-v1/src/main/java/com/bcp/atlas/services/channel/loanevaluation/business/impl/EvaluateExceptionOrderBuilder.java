package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostEvaluationFieldRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostExceptionRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.ValidUtils;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * <br/>
 * <b>Class</b>: LoanExceptionOrderBuilder<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase..</li>
 *         </ul>
 * @version 1.0
 */

@Component
@AllArgsConstructor
public class EvaluateExceptionOrderBuilder {


  @Autowired
  private ValidUtils validUtils;


  /**
   * Method to create the body towards the cross.
   *
   * @param request {@link PostExceptionOrderUxRequest}
   * @return {@link PostRequest}
   */

  public PostRequest buildPostRequest(PostExceptionOrderUxRequest request) {

    validUtils.validate(request, PostExceptionOrderUxRequest.class, Constants.PAQUETE_API);

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(request.getRequiredFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(request.getEvaluation().getExceptionId());

    List<PostEvaluationFieldRequest> postEvaluationFieldRequest = request
        .getEvaluation()
        .getEvaluationFields()
        .stream()
        .map(list -> {
          PostEvaluationFieldRequest field = new PostEvaluationFieldRequest();
          field.setName(list.getName());
          field.setValue(list.getValue());
          return field;
        })
        .collect(Collectors.toList());
    
    postExceptionRequest.setEvaluationFields(postEvaluationFieldRequest);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

}
