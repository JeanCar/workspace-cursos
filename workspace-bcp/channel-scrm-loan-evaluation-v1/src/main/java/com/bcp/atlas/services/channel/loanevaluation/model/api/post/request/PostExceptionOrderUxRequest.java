package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiParam;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Class that represents a model of data presentation that will be entered by the api<br/>
 * <b>Class</b>: PostExceptionOrderUxRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */


@Getter
@Setter
@ToString
@JsonIgnoreProperties
public class PostExceptionOrderUxRequest {

  @ApiParam(name = "requiredFullEvaluation",
      value = "Indica si se evaluaran todas las reglas o no, en caso"
          + " sea true todas las reglas que cumplan con la condicion seran evaluadas, "
          + "en caso sea false, solo la primera que aplique.",
      example = "false", type = "Boolean", required = true)
  @NotNull
  private Boolean requiredFullEvaluation;

  @ApiParam(name = "evaluation", value = "", example = "", type = "Object", required = true)
  @NotNull
  @Valid
  private PostEvaluationRequest evaluation;

}
