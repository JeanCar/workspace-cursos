package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.bcp.atlas.services.channel.loanevaluation.validation.InjectionSafe;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br>
 * <b>Class</b>: PostOfferRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostOfferRequest {

  @ApiModelProperty(name = "saleTypeCode", value = "Tipo de la venta de campaña",
      example = "011 = CEN ACTIVO FIJO\r\n" + "001 = TARJETA DE CREDITO  \r\n"
          + "002 = CANJE DE TC\r\n" + "013 = DESCUENTOS DE LETRAS",
      required = false, position = 6)
  @Size(max = 3)
  private String saleTypeCode;

  @ApiModelProperty(name = "typeCode", value = "Tipo de oferta", example = "001 = FIDELIZACION",
      required = false, position = 7)
  @Size(max = 3)
  private String typeCode;

  @ApiModelProperty(name = "categoryCode", value = "Categoria de la oferta",
      example = "CATEGORIA_01 = Categoria_01_PdC\r\n" + "CATEGORIA_02 = Categoria_02_PdC\r\n"
          + "CATEGORIA_03 = Categoria_03_PdC",
      required = false, position = 8)
  @InjectionSafe
  @Size(max = 30)
  private String categoryCode;

}
