package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br>
 * <b>Class</b>: PostSalesInformationRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostSaleInformationRequest {

  @ApiModelProperty(name = "saleTypeCode", value = "Tipo de venta del préstamo",
      example = "1 = TC Normal\r\n" + "2 = TC Garantía Líquida\r\n" + "3 = TxT\r\n"
          + "4 = CE Normal\r\n" + "5 = CE Garantía Líquida\r\n" + "6 = CH Tradicional",
      required = true, position = 4)
  @NotNull
  @NotEmpty
  @Size(min = 1, max = 1)
  private String saleTypeCode;

  @ApiModelProperty(name = "offer", value = "", dataType = "object", example = "", position = 5)
  @Valid
  private PostOfferRequest offer;

}
