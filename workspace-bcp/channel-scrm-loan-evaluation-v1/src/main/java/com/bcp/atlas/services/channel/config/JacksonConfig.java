package com.bcp.atlas.services.channel.config;

import static com.fasterxml.jackson.core.JsonGenerator.Feature.IGNORE_UNKNOWN;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_COMMENTS;
import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;


/**
 * <b>Class</b>: JacksonConfig<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Co mpany</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li> // *
 *         </ul>
 * @version 1.0
 */

@Configuration
public class JacksonConfig {

  
  /**
   * Method customObjectMapper.
   *
   * @return {@link ObjectMapper}
   */
  @Primary
  @Bean
  public ObjectMapper customObjectMapper() {
    return new Jackson2ObjectMapperBuilder()
        .modules(new JavaTimeModule(), new Jdk8Module())
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .dateFormat(new StdDateFormat())
        .failOnUnknownProperties(true)
        .featuresToDisable(WRITE_DATES_AS_TIMESTAMPS)
        .featuresToEnable(ACCEPT_CASE_INSENSITIVE_PROPERTIES)
        .featuresToEnable(IGNORE_UNKNOWN)
        .featuresToEnable(ALLOW_COMMENTS)
        .createXmlMapper(false)
        .build();
  }
}
