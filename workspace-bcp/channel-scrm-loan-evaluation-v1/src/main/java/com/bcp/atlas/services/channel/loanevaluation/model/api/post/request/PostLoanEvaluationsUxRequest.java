package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;


import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Class that represents a model of data presentation that will be entered by the api<br/>
 * <b>Class</b>: PostLoanEvaluationsUxRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@ToString
@JsonIgnoreProperties
public class PostLoanEvaluationsUxRequest {


  @ApiModelProperty(name = "loanOrderId", value = "Id de la Solicitud", example = "00004567",
      required = true, position = 1)
  @NotNull
  @NotEmpty
  @Size(min = 8, max = 16)
  @Pattern(regexp = Constants.PATTERN_LOANEVALUATIONID)
  private String loanOrderId;

  @ApiModelProperty(name = "registerDate", value = "Fecha y Hora de la Solicitud",
      example = "2018-02-12T09:59:59.000", required = true, position = 2)
  @NotNull
  @NotEmpty
  @Pattern(regexp = Constants.PATTERN_REGISTERDATE)
  @Size(max = 28)
  private String registerDate;

  @ApiModelProperty(name = "saleInformation", value = "", dataType = "object", example = "",
      required = true, position = 3)
  @NotNull
  @Valid
  private PostSaleInformationRequest saleInformation;

  @ApiModelProperty(name = "persons",
      value = "Para el caso de SCRM tomar titular / conyuque (0, 1)", dataType = "List",
      example = "", required = true, position = 9)
  @NotNull
  @Valid
  private List<PostPersonsRequest> persons;

  @ApiModelProperty(name = "evaluations", value = "", dataType = "object", example = "",
      required = true, position = 30)
  @NotNull
  @Valid
  private PostEvaluationsRequest evaluations;
}
