package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PostPersonsResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostPersonsResponse {

  @ApiModelProperty(name = "evaluationScore", value = "Score de la solicitud de evaluación.",
      dataType = "Number", example = "351", position = 17)
  private BigDecimal evaluationScore;

  @ApiModelProperty(name = "hasSalaryAccount",
      value = "true: Es cliente PDH   false: No es cliente PDH.", dataType = "boolean",
      example = "true", position = 18)
  private boolean hasSalaryAccount;

  @ApiModelProperty(name = "relationTypeCode", value = "Codigo de Relacion de la Persona.",
      example = "", position = 19)
  private String relationTypeCode;

  @ApiModelProperty(name = "riskSegment", value = "Código de segmentacion Riesgo Per - Variable.",
      dataType = "String", example = "A|B|C|D|E", position = 20)
  private String riskSegment;

  @ApiModelProperty(name = "consolidatedDebtCalculated", value = "", dataType = "Object",
      example = "", position = 21)
  private PostConsolidatedDebtCalculatedResponse consolidatedDebtCalculated;

  @ApiModelProperty(name = "salaryIncome", value = "", dataType = "Object", example = "",
      position = 21)
  private PostSalaryIncomeResponse salaryIncome;

  @ApiModelProperty(name = "riskCalculatedIncome", value = "", dataType = "Object", example = "",
      position = 21)
  private PostRiskCalculatedIcomeResponse riskCalculatedIncome;

  @ApiModelProperty(name = "additionalIncome", value = "", dataType = "Object", example = "",
      position = 27)
  private PostAdditionalIncomeResponse additionalIncome;

  @ApiModelProperty(name = "calculatedIncome", value = "", dataType = "Object", example = "",
      position = 22)
  private PostCalculatedIcomeResponse calculatedIncome;


}
