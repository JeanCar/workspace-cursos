package com.bcp.atlas.services.channel.loanevaluation.util.constants;

import static lombok.AccessLevel.PRIVATE;

import lombok.NoArgsConstructor;

/**
 * Class that contains fixed variables for use in other classes<br/>
 * <b>Class</b>: Constants<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         // * <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@NoArgsConstructor(access = PRIVATE)
public final class Constants {

  public static final String CODE_ERROR_UNEXPECTED = "TL9999";
  public static final String MESSAGE_ERROR_UNEXPECTED =
      "Se produjo un error al consumir el servicio.";
  public static final String COMPONENT_API = "channel-scrm-loan-evaluation-v1";
  public static final String HEADER_OPN_NUMBER = "opn-number";
  public static final String REQUEST_ID = "550e8400-e29b-41d4-a716-406655440001";
  public static final String REQUEST_DATE = "2017-06-01T17:15:20.509-0400";
  public static final String USER_CODE = "APTLCER";
  public static final String APP_CODE = "TL";
  public static final String CALLER_NAME = "0S56523";
  public static final String SUPERVISOR_CODE = "0S9999";
  public static final String GUILD_CLIENT = "S";
  public static final String NAME_GUILD_CLIENT = "guild-client";
  public static final String OPN_NUMBER = "99399513";
  public static final String OPN_CODE_API_EXCEPTION = "464";
  public static final String OPN_CODE_ARCHIVO_NEGATIVO = "720";
  public static final String SERVER_APP = "cross";
  public static final String BRANCH_OFFICE_CODE = "191";
  public static final String FECHA_STRING = "2019-01-02";
  public static final String DATE_TIME_FORMATTER01 = "yyyy-MM-dd-'T'hh:mm:ss.SSSZ";
  public static final String DATE_TIME_FORMATTER02 = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String CIUDAD = "America/Lima";
  public static final String FORMATO_DATE = "yyyy-MM-dd";
  public static final String PAQUETE_API =
      "com.bcp.atlas.services.channel.loanevaluation.model.thirdparty";
  public static final String CERO_STRING = "0";
  public static final int CERO_INT = 0;
  public static final String UTF_8 = "UTF-8";
  public static final String APPLICATION_JSON = "application/json";
  public static final String PATTERN_LOANEVALUATIONID = "^[a-zA-Z0-9]+$";
  public static final String PATTERN_REGISTERDATE = "^[a-zA-Z0-9\\-\\.\\\\:]+$";
  public static final String PATTERN_TYPECODE = "^[a-zA-Z0-9]+$";
  public static final String PATTERN_BIRTHDATE =
      "^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
  public static final String PATTERN_CURRENCY_CODE = "^(PEN|EUR|USD)$";
  public static final String PATTERN_REQUEST_CURRENCYCODE = "^[a-zA-Z]+$";
  public static final String PATTERN_RELATION_TYPECODE = "^[A-Z]$";
  public static final String DECIMAL_MIN = "0.00";
  public static final String PATTERN_EXCEPTIONID = "^[0-9]+$";
  public static final String PATTERN_FIELD_NAME = "^[a-zA-Z0-9\\_]+$";
  public static final String PERSON_ID =
      "([a-zA-Z\\d\\-]{1,11}[ABCDEFLM]([\\d]{3})|[a-zA-Z\\d\\-]{1,11}[1234567](000))";
  public static final String COMPONENT_EXCEPTION_ORDER =
      "atlas-cross-services-servicing-customer-order-evaluation-exception-orders";
  public static final String RISKSEGMENT_CODE_E = "E";
  public static final String MOUNT_AN = "150.00";
  public static final String TITULAR = "T";
  public static final String CONYUGE = "C";
  public static final String LEVE = "LEVE";
  public static final String GRAVE = "GRAVE";
  public static final String SBS01 = "sbs01";
  public static final String AN01 = "an01";
  public static final String AN02 = "an02";
  public static final String AN03 = "an03";
  public static final String AN04 = "an04";
  public static final String AN05 = "an05";
  public static final String CDE01 = "cde01";
  public static final String IS01 = "is01";
  public static final String IS02 = "is02";
  public static final String IS03 = "is03";
  public static final String BP01 = "bp01";
  public static final String RH01 = "rh01";
  public static final String TS01 = "ts01";
  public static final String TS02 = "ts02";
  public static final String TA01 = "ta01";
  public static final String TA02 = "ta02";
  public static final String TA03 = "ta03";
  public static final String TA04 = "ta04";
  public static final String SEGMENT_A = "A";
  public static final String SEGMENT_B = "B";
  public static final String SEGMENT_C = "C";
  public static final String SEGMENT_D = "D";
  public static final String SEGMENT_E = "E";
  public static final String CODE_EXITO = "LE0000";
  public static final String MENSAJE_EXITO = "Satisfactorio";
  public static final String CODE_NO_EVALUADO = "LE0001";
  public static final String MENSAJE_NO_EVALUADO = "No se evalua el módulo.";
  public static final String CODE_ERROR_TECHNICAL = "TL0009";
  public static final String MENSAJE_ERROR_TECHNICAL =
      "Ocurrio un error intentando invocar servicios externos.";
  public static final String COMPONENT_CHANNEL = "AKS.Loan-Evaluations-v1";
  public static final String AP = "AP";
}
