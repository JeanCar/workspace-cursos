package com.bcp.atlas.services.channel.loanevaluation.business.impl;


import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationFieldsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostAdditionalFieldsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostEvaluationResultsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.GenericUtil;
import com.bcp.atlas.services.channel.util.ValidUtils;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * <br/>
 * <b>Class</b>: LoanExceptionOrderProccesor<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Component
@AllArgsConstructor
public class EvaluateExceptionOrderProccesor {

  @Autowired
  private ValidUtils validUtils;

  /**
   * This method is used to parse the cross service response.
   *
   * @param getResponse {@Link GetResponse}
   * @return {@Link LoanExceptionOrderUxResponseGet}
   */

  public GetExceptionOrderUxResponse convertResponseGetToUx(GetResponse getResponse) {
    validUtils.validate(getResponse, GetResponse.class, Constants.PAQUETE_API);
    GetExceptionOrderUxResponse loanExceptionOrderUxResponseGet = new GetExceptionOrderUxResponse();

    loanExceptionOrderUxResponseGet.setExceptionId(getResponse.getException().getExceptionId());
    loanExceptionOrderUxResponseGet.setDescription(getResponse.getException().getDescription());

    List<GetEvaluationFieldsResponse> evaluationFields =
        getResponse.getException().getEvaluationFields().stream().map(list -> {
          GetEvaluationFieldsResponse fields = new GetEvaluationFieldsResponse();
          fields.setName(list.getName());
          fields.setType(list.getType().getValue());
          return fields;
        }).collect(Collectors.toList());

    loanExceptionOrderUxResponseGet.setEvaluationFields(evaluationFields);

    return loanExceptionOrderUxResponseGet;
  }


  /**
   * This method is used to parse the cross service response.
   *
   * @param postResponse {@Link PostResponse}
   * @return {@Link LoanExceptionOrderUxResponsePost}
   */

  public PostExceptionOrderUxResponse convertResponsePostToUx(PostResponse postResponse) {
    validUtils.validate(postResponse, PostResponse.class, Constants.PAQUETE_API);

    PostExceptionOrderUxResponse loanExceptionOrderUxResponsePost =
        new PostExceptionOrderUxResponse();

    List<PostEvaluationResultsResponse> additionalFields =
        postResponse.getException().getEvaluationResults().stream().map(list -> {
          PostEvaluationResultsResponse field = new PostEvaluationResultsResponse();
          if (list.getStatus() != null) {
            field.setAutonomyLevel(GenericUtil.convertStrToInt(list.getStatus().getCode()));
          }
      
          field.setAdditionalFields(list.getAdditionalFields().stream().map(li -> {
            PostAdditionalFieldsResponse additional = new PostAdditionalFieldsResponse();
            additional.setName(li.getName());
            additional.setValue(li.getValue());
            return additional;
          }).collect(Collectors.toList()));

          return field;
        }).collect(Collectors.toList());


    loanExceptionOrderUxResponsePost.setEvaluationResults(additionalFields);
    return loanExceptionOrderUxResponsePost;
  }
}
