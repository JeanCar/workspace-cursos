package com.bcp.atlas.services.channel.loanevaluation.business.impl;


import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.loanevaluation.business.LoanEvaluationService;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostModules;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostResponseList;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetEvaluationQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.header.LoanEvaluationUxHeader;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostPersonsRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtil;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.operator.CircuitBreakerOperator;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <br/>
 * Class service that contains the necessary methods to process the data and business logic that
 * will consume the REST class LoanEvaluationController<br/>
 * <b>Class</b>: LoanEvaluationImpl<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Service
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class LoanEvaluationImpl implements LoanEvaluationService {

  @Autowired
  private LoanEvaluationBuilder loanEvaluationBuilder;

  @Autowired
  private LoanEvaluationProccesor loanEvaluationProccesor;

  @Autowired
  private LoanEvaluationSender loanEvaluationSender;

  @Autowired
  @Qualifier("atlas.default")
  private CircuitBreaker circuitBreaker;

  @Autowired
  private EvaluateExceptionOrderSender evaluateExceptionOrderSender;

  @Autowired
  private ApplicationProperties properties;

  @Autowired
  private LoanEvaluationUtil loanEvaluationUtil;


  @Override
  public Single<PostLoanEvaluationsUxResponse> loanEvaluations(LoanEvaluationUxHeader headers,
      PostLoanEvaluationsUxRequest request) {

    return loanEvaluationSender.callLoanEvaluation(loanEvaluationBuilder.buildRequest(request))
        .lift(CircuitBreakerOperator.of(circuitBreaker)).map(loanResponse -> {
          Wrapper wrapper = new Wrapper();
          log.info("1. Validar Modulos Declinados CDA: {}", validateCdaResponse(loanResponse));
          if (validateCdaResponse(loanResponse)) {
            wrapper.setCountModulesDeclined(true);
            wrapper.setCrediticiaDeclined(true);
          } else {
            wrapper.setPostResponseList(new PostResponseList());
          }
          wrapper.setLoanEvaluationResponse(modifyModules(loanResponse));
          return wrapper;
        }).flatMap(wrappercre -> {
          if (validateCrediticia(wrappercre)) {
            return callCrediticia(wrappercre, request);
          } else {
            return Single.just(wrappercre);
          }
        }).flatMap(wrappercom -> {
          if (validateComercialPricing(wrappercom)) {
            return callComerialPricing(wrappercom, request);
          } else {
            return Single.just(wrappercom);
          }
        }).flatMap(wrapperdoc -> {
          if (validateDocumental(wrapperdoc)) {
            return callDocumental(wrapperdoc, request);
          } else {
            return Single.just(wrapperdoc);
          }
        }).map(wrapperResponse -> loanEvaluationProccesor.convertResponseToUx(wrapperResponse))
        .doOnSuccess(succ -> log.info("Successful"))
        .doOnError(err -> log.error("Error al consultar loanEvaluation", err));
  }



  /**
   * This method validateCrediticia.
   *
   * @param wrappercre {@Link Wrapper}
   * @return {@Link boolean}
   */
  public boolean validateCrediticia(Wrapper wrappercre) {
    return !wrappercre.isCountModulesDeclined() && loanEvaluationUtil
        .moduleGeneralDecline(wrappercre.getLoanEvaluationResponse().getStatus().getCode());
  }

  /**
   * This method validateComercialPricing.
   *
   * @param wrappercom {@Link Wrapper}
   * @return {@Link boolean}
   */
  public boolean validateComercialPricing(Wrapper wrappercom) {
    return !wrappercom.isCountModulesDeclined() && validateModuleComercialPricing(wrappercom);

  }

  /**
   * This method validateModuleComercialPricing.
   *
   * @param wrappercom {@Link Wrapper}
   * @return {@Link boolean}
   */
  public boolean validateModuleComercialPricing(Wrapper wrappercom) {
    String status = wrappercom.getLoanEvaluationResponse().getStatus().getCode();
    return (!loanEvaluationUtil.moduleGeneralDecline(status) && !wrappercom.isCrediticiaDeclined())
        || (loanEvaluationUtil.moduleGeneralDecline(status) && !wrappercom.isCrediticiaDeclined());
  }

  /**
   * This method validateDocumental.
   *
   * @param wrapperdoc {@Link Wrapper}
   * @return {@Link boolean}
   */
  public boolean validateDocumental(Wrapper wrapperdoc) {
    return !wrapperdoc.isCountModulesDeclined() && !loanEvaluationUtil
        .moduleGeneralDecline(wrapperdoc.getLoanEvaluationResponse().getStatus().getCode());
  }

  /**
   * This method validateCdaResponse.
   *
   * @param loanEvaluationResponse {@Link LoanEvaluationResponse}
   * @return {@Link boolean}
   */
  public boolean validateCdaResponse(LoanEvaluationResponse loanEvaluationResponse) {
    long count = loanEvaluationResponse.getEvaluations().stream()
        .filter(p -> !p.getModule().getCode().equalsIgnoreCase(properties.getAnCode())
            && !p.getModule().getCode().equalsIgnoreCase(properties.getCemCode())
            && !p.getModule().getCode().equalsIgnoreCase(properties.getSbsCode())
            && (loanEvaluationUtil.modulesDecline(p.getStatus().getCode())))
        .count();
    return count > 1;
  }


  /**
   * This method modifyModules.
   *
   * @param loanEvaluationResponse {@Link LoanEvaluationResponse}
   * @return {@Link LoanEvaluationResponse}
   */
  public LoanEvaluationResponse modifyModules(LoanEvaluationResponse loanEvaluationResponse) {
    LoanEvaluationResponse response = new LoanEvaluationResponse();
    response.setLoanEvaluationId(loanEvaluationResponse.getLoanEvaluationId());
    response.setStatus(loanEvaluationResponse.getStatus());
    response.setConsolidatedFinancialInformation(
        loanEvaluationResponse.getConsolidatedFinancialInformation());
    response.setPersons(loanEvaluationResponse.getPersons());
    response.setLoanOffers(loanEvaluationResponse.getLoanOffers());
    response.setEvaluations(loanEvaluationUtil.initModules(loanEvaluationResponse));
    return response;

  }

  /**
   * This method callCrediticia.
   *
   * @param wrapper {@Link Wrapper}
   * @param request {@Link PostLoanEvaluationsUxRequest}
   * @return {@Link Single}
   */
  public Single<Wrapper> callCrediticia(Wrapper wrapper, PostLoanEvaluationsUxRequest request) {
    log.info("1.1. ¿Validación de Modulos Crediticios (AN,CEM,SBS) es correcto?: {}",
        loanEvaluationUtil.evaluateStatusModulesCrediticia(wrapper));
    if (loanEvaluationUtil.evaluateStatusModulesCrediticia(wrapper)) {

      log.info("1.1.1. ¿An es declinado?: {}",
          loanEvaluationUtil.evaluateAnStatusDeclined(wrapper).isPresent());
      if (loanEvaluationUtil.evaluateAnStatusDeclined(wrapper).isPresent()) {
        return returnAn(wrapper, request).flatMap(wp -> {
          if (wp.getPostResponseList() != null) {
            return returnCem(wp, request);
          }
          return returnCda(wrapper);
        });
      } else {
        return returnSbs(wrapper, request).flatMap(wp -> returnCem(wp, request));
      }
    }
    return returnCda(wrapper);
  }

  /**
   * This method returnCda.
   *
   * @param wrapper {@Link Wrapper}
   * @return {@Link Single}
   */
  public Single<Wrapper> returnCda(Wrapper wrapper) {
    Wrapper w = new Wrapper();
    w.setLoanEvaluationResponse(modifyModules(wrapper.getLoanEvaluationResponse()));
    w.setCrediticiaDeclined(true);
    return Single.just(w);
  }

  /**
   * This method returnAn.
   *
   * @param wrapper {@Link Wrapper}
   * @param request {@Link PostLoanEvaluationsUxRequest}
   * @return {@Link Single}
   */
  public Single<Wrapper> returnAn(Wrapper wrapper, PostLoanEvaluationsUxRequest request) {
    String personId = "";
    Optional<PostPersonsRequest> person = loanEvaluationUtil.capturePersonId(request);
    if (person.isPresent()) {
      PostPersonsRequest personRequest = person.get();
      personId = personRequest.getPersonId();
    }
    GetEvaluationQueryParam evaluationParam = new GetEvaluationQueryParam();
    evaluationParam.setPersonId(personId);
    evaluationParam.setIsActive(Boolean.TRUE);

    Single<List<EvaluationElement>> singleEvaluation = loanEvaluationSender
        .callEvaluation(evaluationParam).subscribeOn(Schedulers.computation()).onErrorResumeNext(
            (Throwable ex) -> Single.just(Collections.singletonList(new EvaluationElement())));

    return singleEvaluation.flatMap(evaluationElement -> {

      if (evaluationElement.isEmpty()) {
        evaluationElement = Collections.singletonList(new EvaluationElement());
      }

      if (evaluationElement.get(0).getStatus() != null) {
        List<GetEvaluationResponse> archivoNegativoList =
            loanEvaluationBuilder.evaluationResponse(evaluationElement);
        if (!loanEvaluationUtil.validateFilterAn(archivoNegativoList)) {

          Single<PostModules> anModule = evaluateExceptionOrderSender
              .callLoanEvaluationPost(loanEvaluationBuilder.builderArchivoNegativoRequest(
                  loanEvaluationBuilder.archivoNegatioWrapperBuilder(archivoNegativoList),
                  wrapper.getLoanEvaluationResponse(), request))
              .subscribeOn(Schedulers.computation())
              .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
              .map(res -> loanEvaluationUtil.mapReturn(res));

          return anModule.map(archivoNegativo -> {
            Wrapper wrapperWithCrediticiaData = new Wrapper();
            wrapperWithCrediticiaData.setCountModulesDeclined(wrapper.isCountModulesDeclined());
            wrapperWithCrediticiaData.setCrediticiaDeclined(wrapper.isCrediticiaDeclined());
            wrapperWithCrediticiaData
                .setLoanEvaluationResponse(wrapper.getLoanEvaluationResponse());
            wrapperWithCrediticiaData.setIncomeMark(request.getEvaluations().getIncomeMark());
            PostResponseList response = new PostResponseList();
            response.setArchivoNegativo(archivoNegativo);
            wrapperWithCrediticiaData.setPostResponseList(response);
            return wrapperWithCrediticiaData;
          });

        }
        return returnCda(wrapper);

      }
      return Single.just(wrapper);
    });

  }

  /**
   * This method returnSbs.
   *
   * @param wrapper {@Link Wrapper}
   * @param request {@Link PostLoanEvaluationsUxRequest}
   * @return {@Link Single}
   */
  public Single<Wrapper> returnSbs(Wrapper wrapper, PostLoanEvaluationsUxRequest request) {
    log.info("1.1.2. ¿Sbs es declinado?: {}",
        loanEvaluationUtil.evaluateSbsStatusDeclined(wrapper).isPresent());
    if (loanEvaluationUtil.evaluateSbsStatusDeclined(wrapper).isPresent()) {
      Single<PostModules> sbsModule = evaluateExceptionOrderSender
          .callLoanEvaluationPost(loanEvaluationBuilder
              .builderClasificacionSbsRequest(wrapper.getLoanEvaluationResponse()))
          .subscribeOn(Schedulers.computation())
          .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
          .map(res -> loanEvaluationUtil.mapReturn(res));

      return sbsModule.map(sbs -> {
        Wrapper wrapperWithCrediticiaData = new Wrapper();
        wrapperWithCrediticiaData.setCountModulesDeclined(wrapper.isCountModulesDeclined());
        wrapperWithCrediticiaData.setCrediticiaDeclined(wrapper.isCrediticiaDeclined());
        wrapperWithCrediticiaData.setLoanEvaluationResponse(wrapper.getLoanEvaluationResponse());
        wrapperWithCrediticiaData.setIncomeMark(request.getEvaluations().getIncomeMark());
        PostResponseList response = new PostResponseList();
        response.setSbs(sbs);
        wrapperWithCrediticiaData.setPostResponseList(response);
        return wrapperWithCrediticiaData;
      });

    }
    return Single.just(wrapper);
  }


  /**
   * This method returnCem.
   *
   * @param wrapper {@Link Wrapper}
   * @param request {@Link PostLoanEvaluationsUxRequest}
   * @return {@Link Single}
   */
  public Single<Wrapper> returnCem(Wrapper wrapper, PostLoanEvaluationsUxRequest request) {
    log.info("1.1.2. ¿CEM es declinado?: {}",
        loanEvaluationUtil.evaluateCemStatusDeclined(wrapper).isPresent());
    if (loanEvaluationUtil.evaluateCemStatusDeclined(wrapper).isPresent()) {
      Single<PostModules> cemModule = evaluateExceptionOrderSender
          .callLoanEvaluationPost(
              loanEvaluationBuilder.builderCdeRequest(wrapper.getLoanEvaluationResponse()))
          .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
          .subscribeOn(Schedulers.computation()).map(res -> loanEvaluationUtil.mapReturn(res));
      return cemModule.map(cde -> {
        Wrapper wrapperWithCrediticiaData = new Wrapper();
        wrapperWithCrediticiaData.setCountModulesDeclined(wrapper.isCountModulesDeclined());
        wrapperWithCrediticiaData.setCrediticiaDeclined(wrapper.isCrediticiaDeclined());
        wrapperWithCrediticiaData.setLoanEvaluationResponse(wrapper.getLoanEvaluationResponse());
        wrapperWithCrediticiaData.setIncomeMark(request.getEvaluations().getIncomeMark());
        PostResponseList response = new PostResponseList();
        response.setCem(cde);
        response.setArchivoNegativo(wrapper.getPostResponseList().getArchivoNegativo());
        response.setSbs(wrapper.getPostResponseList().getSbs());
        wrapperWithCrediticiaData.setPostResponseList(response);
        return wrapperWithCrediticiaData;
      });
    }
    return Single.just(wrapper);
  }

  /**
   * This method callComerialPricing.
   *
   * @param wrapper {@Link Wrapper}
   * @param request {@Link PostLoanEvaluationsUxRequest}
   * @return {@Link Single}
   */
  public Single<Wrapper> callComerialPricing(Wrapper wrapper,
      PostLoanEvaluationsUxRequest request) {

    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    Single<PostModules> tsModule = evaluateExceptionOrderSender
        .callLoanEvaluationPost(loanEvaluationBuilder.builderTasaRequest(loanEvaluationResponse))
        .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
        .subscribeOn(Schedulers.computation()).map(res -> loanEvaluationUtil.mapReturn(res));

    Single<PostModules> taModule = evaluateExceptionOrderSender
        .callLoanEvaluationPost(
            loanEvaluationBuilder.builderTasaAutorizadaRequest(loanEvaluationResponse, request))
        .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
        .subscribeOn(Schedulers.computation()).map(res -> loanEvaluationUtil.mapReturn(res));

    return Single.zip(tsModule, taModule, (PostModules tasa, PostModules tasaAutorizada) -> {
      Wrapper wrapperWithComerciallData = new Wrapper();
      wrapperWithComerciallData.setCountModulesDeclined(wrapper.isCountModulesDeclined());
      wrapperWithComerciallData.setCrediticiaDeclined(wrapper.isCrediticiaDeclined());
      wrapperWithComerciallData.setLoanEvaluationResponse(wrapper.getLoanEvaluationResponse());
      wrapperWithComerciallData.setIncomeMark(request.getEvaluations().getIncomeMark());
      PostResponseList response = new PostResponseList();
      response.setTasa(tasa);
      response.setTasaAutorizada(tasaAutorizada);
      response.setCem(wrapper.getPostResponseList().getCem());
      response.setArchivoNegativo(wrapper.getPostResponseList().getArchivoNegativo());
      response.setSbs(wrapper.getPostResponseList().getSbs());
      wrapperWithComerciallData.setPostResponseList(response);
      return wrapperWithComerciallData;
    });
  }

  /**
   * This method callDocumental.
   *
   * @param wrapper {@Link Wrapper}
   * @param request {@Link PostLoanEvaluationsUxRequest}
   * @return {@Link Single}
   */
  public Single<Wrapper> callDocumental(Wrapper wrapper, PostLoanEvaluationsUxRequest request) {

    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    Single<PostModules> bpModule = evaluateExceptionOrderSender
        .callLoanEvaluationPost(
            loanEvaluationBuilder.builderBoletasPagoRequest(loanEvaluationResponse))
        .subscribeOn(Schedulers.computation())
        .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
        .map(res -> loanEvaluationUtil.mapReturn(res));

    Single<PostModules> rhModule = evaluateExceptionOrderSender
        .callLoanEvaluationPost(
            loanEvaluationBuilder.builderReciboHonorariosRequest(loanEvaluationResponse))
        .subscribeOn(Schedulers.computation())
        .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
        .map(res -> loanEvaluationUtil.mapReturn(res));

    Single<PostModules> isModule = evaluateExceptionOrderSender
        .callLoanEvaluationPost(loanEvaluationBuilder
            .builderIngresosSustentadosRequest(loanEvaluationResponse, request))
        .subscribeOn(Schedulers.computation())
        .onErrorResumeNext((Throwable ex) -> loanEvaluationUtil.errorReturn(ex))
        .map(res -> loanEvaluationUtil.mapReturn(res));

    return Single.zip(bpModule, rhModule, isModule,
        (PostModules boletaPago, PostModules reciboHonorarios, PostModules ingresosSustentos) -> {
          Wrapper wrapperWithdocumentalData = new Wrapper();
          wrapperWithdocumentalData.setCountModulesDeclined(wrapper.isCountModulesDeclined());
          wrapperWithdocumentalData.setCrediticiaDeclined(wrapper.isCrediticiaDeclined());
          wrapperWithdocumentalData
              .setLoanEvaluationResponse(modifyModules(wrapper.getLoanEvaluationResponse()));
          PostResponseList response = new PostResponseList();
          response.setBoletaPago(boletaPago);
          response.setReciboHonorarios(reciboHonorarios);
          if (request.getEvaluations().getIncomeOutsideGuideline().compareTo(BigDecimal.ZERO) > 0) {
            response.setIngresoSustentos(ingresosSustentos);
          }
          response.setTasa(wrapper.getPostResponseList().getTasa());
          response.setTasaAutorizada(wrapper.getPostResponseList().getTasaAutorizada());
          wrapperWithdocumentalData.setPostResponseList(response);
          return wrapperWithdocumentalData;
        });
  }

}
