package com.bcp.atlas.services.channel.loanevaluation.model.api.get.response;

import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * <br>
 * <b>Class</b>: GetRecordsResponse<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRecordsResponse {

  @ApiModelProperty(name = "registerDate",
      value = "Fecha de Ingreso del motivo a Archivo Negativo", dataType = "LocalDate",
      example = "2013-02-28T00:00:00", position = 2)
  @JsonFormat(pattern = Constants.DATE_TIME_FORMATTER02)
  private LocalDate registerDate;

  @ApiModelProperty(name = "debtsAmount",
      value = "Monto informado por el que ingreso a Archivo negativo", dataType = "BigDecimal",
      example = "6215.2897", position = 3)
  private BigDecimal debtsAmount;

  @ApiModelProperty(name = "reasonCode",
      value = "Codigo de estado del registro + Codigo que identifica al recurso involucrado",
      example = "006002", position = 4)
  private String reasonCode;

}
