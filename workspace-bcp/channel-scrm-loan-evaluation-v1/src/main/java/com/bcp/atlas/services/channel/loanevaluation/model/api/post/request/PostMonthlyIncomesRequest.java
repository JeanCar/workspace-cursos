package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br>
 * <b>Class</b>: PostMonthlyIncomesRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostMonthlyIncomesRequest {

  @ApiModelProperty(name = "grossAmount", value = "Monto de Ingreso.", example = "1500.50",
      required = true, position = 22)
  @DecimalMin(Constants.DECIMAL_MIN)
  @Digits(integer = 14, fraction = 4)
  private BigDecimal grossAmount;

  @ApiModelProperty(name = "additionalGrossAmount", value = "Ingreso bruto adicional.",
      example = "1500.50", required = true, position = 23)
  private BigDecimal additionalGrossAmount;

  @ApiModelProperty(name = "currencyCode", value = "moneda", example = "EUR", required = true,
      position = 24)
  @Pattern(regexp = Constants.PATTERN_CURRENCY_CODE)
  @Size(min = 3, max = 3)
  private String currencyCode;

  @ApiModelProperty(name = "incomeTypeCode", value = "Codigo del Tipo de Ingreso.", example = "01",
      required = true, position = 25)
  @Size(min = 2, max = 2)
  private String incomeTypeCode;

  @ApiModelProperty(name = "incomeTaxCategoryCode", value = "Categoria de impuesto a la renta.",
      dataType = "Integer", example = "5", required = true, position = 26)
  @Digits(integer = 1, fraction = 0)
  private Integer incomeTaxCategoryCode;

  @ApiModelProperty(name = "employerId", value = "RUC de la Empresa empleadora.",
      example = "20395135288", required = true, position = 27)
  @Size(min = 11, max = 11)
  private String employerId;

  @ApiModelProperty(name = "employerPrimaryEconomicActivityCode",
      value = "Codigo de la Actividad Economica de la Empresa.", example = "0162", required = true,
      position = 28)
  @Size(min = 0, max = 5)
  private String employerPrimaryEconomicActivityCode;

  @ApiModelProperty(name = "relationshipStartDate", value = "Fecha de inicio del vinculo laboral",
      example = "2001-02-12T0959.000", required = true, position = 29)
  @Size(max = 28)
  @Pattern(regexp = Constants.PATTERN_REGISTERDATE)
  private String relationshipStartDate;

}
