package com.bcp.atlas.services.channel.util;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ReflectionUtils;

/**
 * Useful class to cast null fields to empty or zero value<br/>
 * <b>Class</b>: ValidUtils.java<br>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;<br>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br>
 *         <u>Developed by</u>: <br>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 *
 * @version 1.0
 */
@SuppressFBWarnings(value = "CRLF_INJECTION_LOGS", justification = "There is no injection here. ")
@Configuration
@Slf4j
public class ValidUtils {

  /**
   * Transform properties object from null to a default value.
   *
   * @param response {@link Object}
   * @param clase {@link Class}
   * @param modelPackage Package name of the thirdparty api model.
   */
  public <T> void validate(T response, Class<?> clase, String modelPackage) {
    Field[] allFields = clase.getDeclaredFields();
    Arrays.stream(allFields).forEach(field -> {
      ReflectionUtils.makeAccessible(field);
      Object item = null;
      try {
        item = field.get(response);
        boolean isPrimitive = !field.getType().getName().startsWith(modelPackage);
        if (!isPrimitive) { // clase
          if (item == null) {
            item = field.getType().newInstance();
          }
          validate(item, field.getType(), modelPackage);
          field.set(response, item);
        } else if (item == null) {
          // Se pone valor por defecto
          field.set(response, initValues().get(field.getType()));
        } else if (field.getType() == List.class) {
          ParameterizedType genericListType = (ParameterizedType) field.getGenericType();
          Class<?> genericClass = (Class<?>) genericListType.getActualTypeArguments()[0];
          for (Object value : (List<?>) item) {
            validate(genericClass.cast(value), genericClass, modelPackage);
          }
        }
      } catch (IllegalAccessException | InstantiationException e) {
        log.error(e.getMessage());
      }
    });
  }

  private HashMap<Class<?>, Object> initValues() {
    HashMap<Class<?>, Object> hm = new HashMap<>();
    hm.put(Integer.class, 0);
    hm.put(Long.class, 0L);
    hm.put(Double.class, 0.0);
    hm.put(BigDecimal.class, BigDecimal.ZERO);
    hm.put(List.class, Collections.emptyList());
    hm.put(String.class, StringUtils.EMPTY);
    hm.put(Boolean.class, Boolean.FALSE);
    return hm;
  }
}
