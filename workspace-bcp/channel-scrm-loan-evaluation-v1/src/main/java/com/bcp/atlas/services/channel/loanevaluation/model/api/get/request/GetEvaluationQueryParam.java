package com.bcp.atlas.services.channel.loanevaluation.model.api.get.request;

import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


/**
 * <br>
 * <b>Class</b>: GetEvaluationQueryParam<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@JsonIgnoreProperties
public class GetEvaluationQueryParam implements Serializable {

  private static final long serialVersionUID = -7921493455050718985L;
  @ApiModelProperty(name = "personId", value = "Identificador del número de documento del cliente.",
      example = "124563786000")
  @Size(min = 5, max = 15)
  @Pattern(regexp = Constants.PERSON_ID)
  private String personId;

  @ApiModelProperty(name = "isActive",
      value = "Flag para poder extrar "
          + "el información historica o activas del ingreso a archivo negativo.",
      example = "true", dataType = "boolean")
  private Boolean isActive;

}
