package com.bcp.atlas.services.channel.util;

import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.config.IncomesProperties;
import com.bcp.atlas.services.channel.config.IncomesProperties.Income;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.ModulesResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostModules;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostPersonsRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.CalculatedIncome;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluatedPerson;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationDetail;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationModule;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationStatusGeneral;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import io.reactivex.Single;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Useful class utils to LoanEvaluationImpl<br/>
 * <b>Class</b>: LoanEvaluationUtil<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Component
@AllArgsConstructor
@Slf4j
public class LoanEvaluationUtil {

  @Autowired
  private ApplicationProperties properties;

  @Autowired
  private IncomesProperties incomesProperties;

  @Autowired
  private InitModulesUtil initModuleUtil;

  /**
   * Method to init modules.
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @return {@link List}
   */
  public List<EvaluationDetail> initModules(LoanEvaluationResponse loanEvaluationResponse) {

    List<EvaluationDetail> evaluationList = new ArrayList<>();

    List<EvaluationDetail> otherModulesList = initModuleUtil.otherModules();
    loanEvaluationResponse.getEvaluations().forEach(list -> {
      EvaluationDetail detail = new EvaluationDetail();
      EvaluationModule module = new EvaluationModule();
      module.setCode(list.getModule().getCode());
      module.setDescription(list.getModule().getDescription());
      detail.setModule(module);
      EvaluationStatusGeneral status = new EvaluationStatusGeneral();
      status.setCode(list.getStatus().getCode());
      status.setDescription(list.getStatus().getDescription());
      detail.setStatus(status);
      detail.setEvaluationRules(list.getEvaluationRules());
      evaluationList.add(detail);
      otherModulesList
          .removeIf(f -> f.getModule().getCode().equalsIgnoreCase(list.getModule().getCode()));
    });
    evaluationList.addAll(otherModulesList);
    return evaluationList;
  }

  /**
   * Method evaluateStatusModulesCrediticia.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link long}
   */
  public boolean evaluateStatusModulesCrediticia(Wrapper wrapper) {
    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    return evaluateCountModulesCrediticia(wrapper)
        && loanEvaluationResponse.getEvaluations().stream()
            .filter(p -> (p.getModule().getCode().equalsIgnoreCase(properties.getAnCode())
                || p.getModule().getCode().equalsIgnoreCase(properties.getSbsCode()))
                && modulesDecline(p.getStatus().getCode()))
            .count() < 2;
  }


  /**
   * Method evaluateCountModulesCrediticia.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link long}
   */
  public boolean evaluateCountModulesCrediticia(Wrapper wrapper) {
    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    return loanEvaluationResponse.getEvaluations().stream()
        .filter(mod -> (mod.getModule().getCode().equalsIgnoreCase(properties.getAnCode())
            || mod.getModule().getCode().equalsIgnoreCase(properties.getSbsCode())
            || mod.getModule().getCode().equalsIgnoreCase(properties.getCemCode()))
            && modulesDecline(mod.getStatus().getCode()))
        .count() < 3;
  }

  /**
   * Method evaluateAnStatusDeclined.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link Optional}
   */
  public Optional<EvaluationDetail> evaluateAnStatusDeclined(Wrapper wrapper) {
    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    return loanEvaluationResponse.getEvaluations().stream()
        .filter(mod -> mod.getModule().getCode().equalsIgnoreCase(properties.getAnCode())
            && modulesDecline(mod.getStatus().getCode()))
        .findFirst();
  }

  
  /**
   * Method evaluateCemStatusDeclined.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link Optional}
   */
  public Optional<EvaluationDetail> evaluateCemStatusDeclined(Wrapper wrapper) {
    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    return loanEvaluationResponse.getEvaluations().stream()
        .filter(mod -> mod.getModule().getCode().equalsIgnoreCase(properties.getCemCode())
            && modulesDecline(mod.getStatus().getCode()))
        .findFirst();
  }
  
  /**
   * Method evaluateSbsStatusDeclined.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link Optional}
   */
  public Optional<EvaluationDetail> evaluateSbsStatusDeclined(Wrapper wrapper) {
    LoanEvaluationResponse loanEvaluationResponse = wrapper.getLoanEvaluationResponse();
    return loanEvaluationResponse.getEvaluations().stream()
        .filter(mod -> mod.getModule().getCode().equalsIgnoreCase(properties.getSbsCode())
            && modulesDecline(mod.getStatus().getCode()))
        .findFirst();
  }

  /**
   * Method evaluateAnStatusDeclined.
   * 
   * @param request {@link PostLoanEvaluationsUxRequest}
   * @return {@link Optional}
   */
  public Optional<PostPersonsRequest> capturePersonId(PostLoanEvaluationsUxRequest request) {
    return request.getPersons().stream()
        .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationTypeCode())).findFirst();
  }

  /**
   * Method evaluateAnStatusDeclined.
   * 
   * @param archivoNegativoList {@link List}
   * @return {@link boolean}
   */
  public boolean validateFilterAn(List<GetEvaluationResponse> archivoNegativoList) {
    log.info("> ¿MOTIVO GRAVE? {}", archivoNegativoList.stream().anyMatch(m -> m.getRecords()
        .stream().anyMatch(f -> Constants.GRAVE.equalsIgnoreCase(f.getReasonCode()))));
    log.info("> ¿MOTIVO MAYOR A 150.00?: {}",
        archivoNegativoList.stream().anyMatch(m -> m.getRecords().stream()
            .anyMatch(f -> f.getDebtsAmount().compareTo(new BigDecimal(Constants.MOUNT_AN)) > 0)));

    return archivoNegativoList.stream()
        .anyMatch(m -> m.getRecords().stream()
            .anyMatch(f -> Constants.GRAVE.equalsIgnoreCase(f.getReasonCode())
                || f.getDebtsAmount().compareTo(new BigDecimal(Constants.MOUNT_AN)) > 0));
  }

  /**
   * Method calculateCem.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link BigDecimal}
   */
  public BigDecimal calculateCem(Wrapper wrapper, String value) {

    BigDecimal factor = new BigDecimal(1).add(new BigDecimal(value));
    BigDecimal calDeuda = calculateDeudaCliente(wrapper);
    BigDecimal cde = calculateCde(wrapper);
    return cde.multiply(factor).subtract(calDeuda);

  }

  /**
   * Method calculatedDeudaCliente.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link BigDecimal}
   */
  public BigDecimal calculateDeudaCliente(Wrapper wrapper) {

    BigDecimal cde = calculateCde(wrapper);
    BigDecimal cem = Optional.ofNullable(wrapper.getLoanEvaluationResponse()
        .getConsolidatedFinancialInformation().getMonthlyIndebtednessCapacity().getAmount())
        .orElse(BigDecimal.ZERO);
    return cde.subtract(cem);
  }

  /**
   * Method calculateCde.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link BigDecimal}
   */
  public BigDecimal calculateCde(Wrapper wrapper) {
    BigDecimal amountMax = ingresoCalculadoBci(wrapper);
    BigDecimal factor = factorCde(wrapper);
    return amountMax.multiply(factor);
  }

  /**
   * Method factorCde.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link BigDecimal}
   */
  public BigDecimal factorCde(Wrapper wrapper) {
    BigDecimal calculado = BigDecimal.ZERO;
    String segment = captureSegmentTitular(wrapper.getLoanEvaluationResponse());
    BigDecimal amount = ingresoCalculadoBci(wrapper);
    Optional<Map<String, BigDecimal>> map = incomesProperties.getIncomes().parallelStream().filter(
        value -> (amount.compareTo(value.getMin()) >= 0) && (amount.compareTo(value.getMax()) < 0))
        .map(Income::getSegments).findFirst();
    if (map.isPresent()) {
      calculado = map.get().get(segment);
    }
    return calculado;
  }

  /**
   * Method ingresoCalculadoBci.
   * 
   * @param wrapper {@link Wrapper}
   * @return {@link BigDecimal}
   */
  public BigDecimal ingresoCalculadoBci(Wrapper wrapper) {

    BigDecimal amountMax = (wrapper.isIncomeMark())
        ? calculateMaxTitularMount(wrapper.getLoanEvaluationResponse())
            .add(calculateMaxConyugeMount(wrapper.getLoanEvaluationResponse()))
        : calculateMaxTitularMount(wrapper.getLoanEvaluationResponse());

    return Optional.ofNullable(amountMax).orElse(BigDecimal.ZERO);
  }


  /**
   * Method calculateMaxTitularMount.
   * 
   * @param response {@link LoanEvaluationResponse}
   * @return {@link BigDecimal}
   */
  public BigDecimal calculateMaxTitularMount(LoanEvaluationResponse response) {

    CalculatedIncome income = new CalculatedIncome();

    Optional<Optional<CalculatedIncome>> calculatedIncome = response.getPersons().stream()
        .filter(t -> Constants.TITULAR.equalsIgnoreCase(t.getRelationType().getCode()))
        .map(EvaluatedPerson::getFinancialInformation)
        .map(i -> i.getIncomes().stream()
            .filter(f -> !f.getRelatedSource().equalsIgnoreCase(properties.getResRelatedSourcPdh())
                && !f.getRelatedSource().equalsIgnoreCase(properties.getResRelatedSourcePrincipal())
                && !f.getRelatedSource()
                    .equalsIgnoreCase(properties.getResRelatedSourceAdicional()))
            .max(Comparator.comparing(CalculatedIncome::getIncomeAmount)))
        .findFirst();

    if (calculatedIncome.isPresent()) {
      Optional<CalculatedIncome> amount = calculatedIncome.get();
      if (amount.isPresent()) {
        income = amount.get();
      }
    }
    return Optional.ofNullable(income.getIncomeAmount()).orElse(BigDecimal.ZERO);
  }


  /**
   * Method calculateMaxTitularConyugeMount.
   * 
   * @param response {@link LoanEvaluationResponse}
   * @return {@link BigDecimal}
   */
  public BigDecimal calculateMaxConyugeMount(LoanEvaluationResponse response) {
    CalculatedIncome income = new CalculatedIncome();
    Optional<Optional<CalculatedIncome>> calculatedIncome = response.getPersons().stream()
        .filter(t -> Constants.CONYUGE.equalsIgnoreCase(t.getRelationType().getCode()))
        .map(EvaluatedPerson::getFinancialInformation)
        .map(i -> i.getIncomes().stream()
            .filter(f -> !f.getRelatedSource().equalsIgnoreCase(properties.getResRelatedSourcPdh())
                && !f.getRelatedSource().equalsIgnoreCase(properties.getResRelatedSourcePrincipal())
                && !f.getRelatedSource()
                    .equalsIgnoreCase(properties.getResRelatedSourceAdicional()))
            .max(Comparator.comparing(CalculatedIncome::getIncomeAmount)))
        .findFirst();
    if (calculatedIncome.isPresent()) {
      Optional<CalculatedIncome> amount = calculatedIncome.get();
      if (amount.isPresent()) {
        income = amount.get();
      }
    }
    return Optional.ofNullable(income.getIncomeAmount()).orElse(BigDecimal.ZERO);
  }


  /**
   * Method captureSegmentTitular.
   * 
   * @param response {@link LoanEvaluationResponse}
   * @return {@link String}
   */
  public String captureSegmentTitular(LoanEvaluationResponse response) {
    EvaluatedPerson person = new EvaluatedPerson();
    Optional<EvaluatedPerson> riskSegment = response.getPersons().stream()
        .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode())).findFirst();
    if (riskSegment.isPresent()) {
      person = riskSegment.get();
    }
    return captureRiskSegment(
        Optional.ofNullable(person.getRiskSegment().getCode()).orElse(Constants.SEGMENT_E));
  }

  /**
   * Method captureRiskSegment.
   * 
   * @param letter {@link String}
   * @return {@link String}
   */
  public String captureRiskSegment(String letter) {
    return (Constants.SEGMENT_A.equalsIgnoreCase(letter)
        || Constants.SEGMENT_B.equalsIgnoreCase(letter)
        || Constants.SEGMENT_C.equalsIgnoreCase(letter)
        || Constants.SEGMENT_D.equalsIgnoreCase(letter) ? letter : Constants.SEGMENT_E);
  }


  /**
   * Method modulesOtherDecline.
   * 
   * @param code {@link String}
   * @return {@link boolean}
   */
  public boolean modulesDecline(String code) {
    return (code.equalsIgnoreCase(properties.getResModuleDeclineCharacter())
        || code.equalsIgnoreCase(properties.getResModuleDeclineCode())
        || code.equalsIgnoreCase(properties.getResModuleRecommendDeclineCharacter())
        || code.equalsIgnoreCase(properties.getResModuleRecommendDeclineCode())
        || modulesOtherDecline(code));
  }

  /**
   * Method modulesOtherDecline.
   * 
   * @param code {@link String}
   * @return {@link boolean}
   */
  public boolean modulesOtherDecline(String code) {
    return (!code.equalsIgnoreCase(properties.getResModuleInvestigateCode())
        && !code.equalsIgnoreCase(properties.getResModuleApproveCode())
        && !code.equalsIgnoreCase(properties.getResModuleRecommendApproveCode())
        && !code.equalsIgnoreCase(properties.getResModuleNoDecisionCode()));
  }

  /**
   * Method moduleGeneralDecline.
   * 
   * @param code {@link String}
   * @return {@link boolean}
   */
  public boolean moduleGeneralDecline(String code) {
    return (code.equalsIgnoreCase(properties.getResGeneralDeclineCode())
        || code.equalsIgnoreCase(properties.getResGeneralDeclineCharacter()));
  }


  /**
   * This method mapReturn.
   *
   * @param res {@Link PostResponse}
   * @return {@Link PostModules}
   */
  public PostModules mapReturn(Object res) {
    PostModules obj = new PostModules();
    if (res instanceof ModulesResponse) {
      ModulesResponse mr = (ModulesResponse) res;
      obj.setCode(mr.getCodeError());
      obj.setDescription(mr.getMessageError());
    } else {
      PostResponse post = (PostResponse) res;
      obj.setCode(Constants.CODE_EXITO);
      obj.setDescription(Constants.MENSAJE_EXITO);
      obj.setModule(post);
    }
    return obj;
  }

  /**
   * This method mapReturn.
   *
   * @param ex {@Link Throwable}
   * @return {@Link Single}
   */
  public Single<ModulesResponse> errorReturn(Throwable ex) {
    ModulesResponse mr = new ModulesResponse();
    if (ex instanceof AtlasException) {
      AtlasException exp = ((AtlasException) ex);
      if (exp.getCode() != null) {
        if (!Constants.CODE_ERROR_UNEXPECTED.equalsIgnoreCase(exp.getCode())) {
          mr.setCodeError(exp.getCode());
          mr.setMessageError(exp.getDescription());
        } else {
          mr.setCodeError(Constants.CODE_ERROR_TECHNICAL);
          mr.setMessageError(Constants.MENSAJE_ERROR_TECHNICAL);
        }
      } else {
        mr.setCodeError(Constants.CODE_ERROR_TECHNICAL);
        mr.setMessageError(Constants.MENSAJE_ERROR_TECHNICAL);
      }
    }
    return Single.just(mr);
  }
}
