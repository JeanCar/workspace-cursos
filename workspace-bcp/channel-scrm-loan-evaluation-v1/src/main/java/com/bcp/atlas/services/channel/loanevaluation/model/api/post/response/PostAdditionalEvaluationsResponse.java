package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PostEvaluationInformationResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostAdditionalEvaluationsResponse {


  @ApiModelProperty(name = "statusCode",
      value = "LE0000 = Satisfactorio\r\n" + "LE0001 = No se evalua el módulo\r\n"
          + "TL0003 = Los datos proporcionados no son validos\r\n"
          + "TL0009 = Ocurrio un error intentando invocar servicios externos. "
          + "Por favor contactarse con el Soporte Técnico..",
      example = "LE0000|LE0001|TL0003|TL0009", position = 55)
  private String statusCode;

  @ApiModelProperty(name = "moduleCode", value = "SBS,AN,CEM,MISFP,MBP,MRH,MTS,MTSA.",
      example = "SBS", position = 55)
  private String moduleCode;

  @ApiModelProperty(name = "moduleDescription",
      value = "PosicionSBS,ArchivoNegativo,CapacidadPago,Ingresos sustentados fuera "
          + "de pauta,Boletas de pago,Recibos por honorarios,Tasa,Tasa Autorizada.",
      example = "SBS", position = 55)
  private String moduleDescription;

  @ApiModelProperty(name = "moduleClassification",
      value = "CREDITICIA|DOCUMENTAL|COMERCIAL|PRINCING.", example = "SBS", position = 55)
  private String moduleClassification;


  @ApiModelProperty(name = "moduleResults", value = "", dataType = "List", example = "",
      position = 56)
  private List<PostModuleResultsResponse> moduleResults;
}
