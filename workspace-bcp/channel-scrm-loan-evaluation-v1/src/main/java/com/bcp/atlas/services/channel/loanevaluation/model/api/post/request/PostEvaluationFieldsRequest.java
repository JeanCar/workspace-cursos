package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.loanevaluation.validation.InjectionSafe;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br>
 * <b>Class</b>: PostEvaluationFieldsRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostEvaluationFieldsRequest {

  @ApiModelProperty(name = "name", value = "Nombre de la variable de entrada.",
      example = "segmentacion_riesgo", required = true)
  @NotNull
  @NotEmpty
  @Pattern(regexp = Constants.PATTERN_FIELD_NAME)
  @Size(max = 50)
  private String name;

  @ApiModelProperty(name = "value", value = "Valor de la variable de entrada.", example = "A",
      required = true)
  @InjectionSafe
  @Size(max = 100)
  private String value;
}
