package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * <br>
 * <b>Class</b>: PostAddressRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostAddressRequest {

  @ApiModelProperty(name = "geolocationCode",
      value = "Código (Se compone de la concatenación de regionCode +"
          + " provinceCode + districtCode)",
      example = "020101", required = true, position = 17)
  @Size(min = 4, max = 6)
  private String geolocationCode;

  @ApiModelProperty(name = "regionCode",
      value = "Departamento al que pertenece la direccion de la persona", example = "02",
      required = true, position = 18)
  @Size(min = 2, max = 2)
  private String regionCode;

  @ApiModelProperty(name = "provinceCode",
      value = "Provincia al que pertenece la direccion de la persona", dataType = "List",
      example = "01", required = true, position = 19)
  @Size(min = 2, max = 2)
  private String provinceCode;

  @ApiModelProperty(name = "districtCode",
      value = "Distrito al que pertenece la direccion de la persona", example = "01",
      required = true, position = 20)
  @Size(min = 2, max = 2)
  private String districtCode;
}
