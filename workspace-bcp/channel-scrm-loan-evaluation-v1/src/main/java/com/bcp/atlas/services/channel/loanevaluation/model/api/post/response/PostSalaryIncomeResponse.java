package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PostIncomesResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostSalaryIncomeResponse {

  @ApiModelProperty(name = "incomeAmount", value = "Valor del Sueldo Promedio PDH.",
      dataType = "Number", example = "1500.00", position = 25)
  private BigDecimal amount;

  @ApiModelProperty(name = "currencyCode", value = "Moneda del Sueldo Promedio.",
      example = "PEN|USD", position = 26)
  private String currencyCode;

  @ApiModelProperty(name = "periodTypeCode",
      value = "Unidad de tiempo con la que el cliente recibe este ingreso.", example = "D|M|S|A",
      position = 30)
  private String periodTypeCode;

  @ApiModelProperty(name = "relatedSource",
      value = "Codigo de Fuente del Ingreso estimado| En caso el PDH sea BCP, colocar es PDH.",
      example = "PDH1/PDHBCP/MOTOR DE REGLAS/OTROS ADICIONALES", position = 27)
  private String relatedSource;

  @ApiModelProperty(name = "relatedSalaryAccountId", value = "Número de cuenta sueldo del cliente.",
      example = "1921122211211", position = 28)
  private String accountId;
}
