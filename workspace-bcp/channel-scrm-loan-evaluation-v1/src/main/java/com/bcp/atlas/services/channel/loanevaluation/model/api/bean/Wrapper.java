package com.bcp.atlas.services.channel.loanevaluation.model.api.bean;


import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <b>Class</b>: Wrapper<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@ToString
public class Wrapper {
  private boolean countModulesDeclined;
  private boolean isCrediticiaDeclined;
  private boolean incomeMark;
  private PostResponseList postResponseList;
  private LoanEvaluationResponse loanEvaluationResponse;
}
