package com.bcp.atlas.services.channel.util;


import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostAdditionalEvaluationsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostModuleResultsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationDetail;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationModule;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationStatusGeneral;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Useful class utils to LoanEvaluationImpl<br/>
 * <b>Class</b>: InitModulesUtil<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Component
public class InitModulesUtil {

  @Autowired
  private ApplicationProperties properties;

  /**
   * Method otherModules.
   * 
   * @return {@link List}
   */
  public List<EvaluationDetail> otherModules() {

    EvaluationDetail propas = new EvaluationDetail();
    EvaluationModule propasModule = new EvaluationModule();
    propasModule.setCode(properties.getPropasCode());
    propasModule.setDescription(properties.getPropasDescription());
    propas.setModule(propasModule);
    EvaluationStatusGeneral propasStatus = new EvaluationStatusGeneral();
    propasStatus.setCode(properties.getResModuleNoDecisionCode());
    propasStatus.setDescription(properties.getResModuleNoDecisionName());
    propas.setStatus(propasStatus);
    propas.setEvaluationRules(null);

    EvaluationDetail proact = new EvaluationDetail();
    EvaluationModule proactModule = new EvaluationModule();
    proactModule.setCode(properties.getProactCode());
    proactModule.setDescription(properties.getProactDescription());
    proact.setModule(proactModule);
    EvaluationStatusGeneral proactStatus = new EvaluationStatusGeneral();
    proactStatus.setCode(properties.getResModuleNoDecisionCode());
    proactStatus.setDescription(properties.getResModuleNoDecisionName());
    proact.setStatus(proactStatus);
    proact.setEvaluationRules(null);

    EvaluationDetail posbcp = new EvaluationDetail();
    EvaluationModule posbcpModule = new EvaluationModule();
    posbcpModule.setCode(properties.getPosbcpCode());
    posbcpModule.setDescription(properties.getPosbcpDescription());
    posbcp.setModule(posbcpModule);
    EvaluationStatusGeneral posbcpStatus = new EvaluationStatusGeneral();
    posbcpStatus.setCode(properties.getResModuleNoDecisionCode());
    posbcpStatus.setDescription(properties.getResModuleNoDecisionName());
    posbcp.setStatus(posbcpStatus);
    posbcp.setEvaluationRules(null);

    EvaluationDetail perfil = new EvaluationDetail();
    EvaluationModule perfilModule = new EvaluationModule();
    perfilModule.setCode(properties.getPerfilCode());
    perfilModule.setDescription(properties.getPerfilDescription());
    perfil.setModule(perfilModule);
    EvaluationStatusGeneral perfilStatus = new EvaluationStatusGeneral();
    perfilStatus.setCode(properties.getResModuleNoDecisionCode());
    perfilStatus.setDescription(properties.getResModuleNoDecisionName());
    perfil.setStatus(perfilStatus);
    perfil.setEvaluationRules(null);

    EvaluationDetail gral = new EvaluationDetail();
    EvaluationModule gralModule = new EvaluationModule();
    gralModule.setCode(properties.getGralCode());
    gralModule.setDescription(properties.getGralDescription());
    gral.setModule(gralModule);
    EvaluationStatusGeneral gralStatus = new EvaluationStatusGeneral();
    gralStatus.setCode(properties.getResModuleNoDecisionCode());
    gralStatus.setDescription(properties.getResModuleNoDecisionName());
    gral.setStatus(gralStatus);
    gral.setEvaluationRules(null);

    EvaluationDetail tran = new EvaluationDetail();
    EvaluationModule tranModule = new EvaluationModule();
    tranModule.setCode(properties.getTranCode());
    tranModule.setDescription(properties.getTranDescription());
    tran.setModule(tranModule);
    EvaluationStatusGeneral tranStatus = new EvaluationStatusGeneral();
    tranStatus.setCode(properties.getResModuleNoDecisionCode());
    tranStatus.setDescription(properties.getResModuleNoDecisionName());
    tran.setStatus(tranStatus);
    tran.setEvaluationRules(null);

    EvaluationDetail sunat = new EvaluationDetail();
    EvaluationModule sunatModule = new EvaluationModule();
    sunatModule.setCode(properties.getSunatCode());
    sunatModule.setDescription(properties.getSunatDescription());
    sunat.setModule(sunatModule);
    EvaluationStatusGeneral sunatStatus = new EvaluationStatusGeneral();
    sunatStatus.setCode(properties.getResModuleNoDecisionCode());
    sunatStatus.setDescription(properties.getResModuleNoDecisionName());
    sunat.setStatus(sunatStatus);
    sunat.setEvaluationRules(null);

    EvaluationDetail sbs = new EvaluationDetail();
    EvaluationModule sbsModule = new EvaluationModule();
    sbsModule.setCode(properties.getSbsCode());
    sbsModule.setDescription(properties.getSbsDescription());
    sbs.setModule(sbsModule);
    EvaluationStatusGeneral sbsStatus = new EvaluationStatusGeneral();
    sbsStatus.setCode(properties.getResModuleNoDecisionCode());
    sbsStatus.setDescription(properties.getResModuleNoDecisionName());
    sbs.setStatus(sbsStatus);
    sbs.setEvaluationRules(null);

    EvaluationDetail pricing = new EvaluationDetail();
    EvaluationModule pricingModule = new EvaluationModule();
    pricingModule.setCode(properties.getPricingCode());
    pricingModule.setDescription(properties.getPricingDescription());
    pricing.setModule(pricingModule);
    EvaluationStatusGeneral pricingStatus = new EvaluationStatusGeneral();
    pricingStatus.setCode(properties.getResModuleNoDecisionCode());
    pricingStatus.setDescription(properties.getResModuleNoDecisionName());
    pricing.setStatus(pricingStatus);
    pricing.setEvaluationRules(null);

    EvaluationDetail modbhs = new EvaluationDetail();
    EvaluationModule modbhsModule = new EvaluationModule();
    modbhsModule.setCode(properties.getModbhsCode());
    modbhsModule.setDescription(properties.getModbhsDescription());
    modbhs.setModule(modbhsModule);
    EvaluationStatusGeneral modbhsStatus = new EvaluationStatusGeneral();
    modbhsStatus.setCode(properties.getResModuleNoDecisionCode());
    modbhsStatus.setDescription(properties.getResModuleNoDecisionName());
    modbhs.setStatus(modbhsStatus);
    modbhs.setEvaluationRules(null);

    EvaluationDetail modapp = new EvaluationDetail();
    EvaluationModule modappModule = new EvaluationModule();
    modappModule.setCode(properties.getModappCode());
    modappModule.setDescription(properties.getModappDescription());
    modapp.setModule(modappModule);
    EvaluationStatusGeneral modappStatus = new EvaluationStatusGeneral();
    modappStatus.setCode(properties.getResModuleNoDecisionCode());
    modappStatus.setDescription(properties.getResModuleNoDecisionName());
    modapp.setStatus(modappStatus);
    modapp.setEvaluationRules(null);

    EvaluationDetail linea = new EvaluationDetail();
    EvaluationModule lineaModule = new EvaluationModule();
    lineaModule.setCode(properties.getLineaCode());
    lineaModule.setDescription(properties.getLineaDescription());
    linea.setModule(lineaModule);
    EvaluationStatusGeneral lineaStatus = new EvaluationStatusGeneral();
    lineaStatus.setCode(properties.getResModuleNoDecisionCode());
    lineaStatus.setDescription(properties.getResModuleNoDecisionName());
    linea.setStatus(lineaStatus);
    linea.setEvaluationRules(null);

    EvaluationDetail evalua = new EvaluationDetail();
    EvaluationModule evaluaModule = new EvaluationModule();
    evaluaModule.setCode(properties.getEvaluaCode());
    evaluaModule.setDescription(properties.getEvaluaDescription());
    evalua.setModule(evaluaModule);
    EvaluationStatusGeneral evaluaStatus = new EvaluationStatusGeneral();
    evaluaStatus.setCode(properties.getResModuleNoDecisionCode());
    evaluaStatus.setDescription(properties.getResModuleNoDecisionName());
    evalua.setStatus(evaluaStatus);
    evalua.setEvaluationRules(null);

    EvaluationDetail cem = new EvaluationDetail();
    EvaluationModule cemModule = new EvaluationModule();
    cemModule.setCode(properties.getCemCode());
    cemModule.setDescription(properties.getCemDescription());
    cem.setModule(cemModule);
    EvaluationStatusGeneral cemStatus = new EvaluationStatusGeneral();
    cemStatus.setCode(properties.getResModuleNoDecisionCode());
    cemStatus.setDescription(properties.getResModuleNoDecisionName());
    cem.setStatus(cemStatus);
    cem.setEvaluationRules(null);

    EvaluationDetail an = new EvaluationDetail();
    EvaluationModule anModule = new EvaluationModule();
    anModule.setCode(properties.getAnCode());
    anModule.setDescription(properties.getAnDescription());
    an.setModule(anModule);
    EvaluationStatusGeneral anStatus = new EvaluationStatusGeneral();
    anStatus.setCode(properties.getResModuleNoDecisionCode());
    anStatus.setDescription(properties.getResModuleNoDecisionName());
    an.setStatus(anStatus);
    an.setEvaluationRules(null);

    EvaluationDetail alter = new EvaluationDetail();
    EvaluationModule alterModule = new EvaluationModule();
    alterModule.setCode(properties.getAlterCode());
    alterModule.setDescription(properties.getAlterDescription());
    alter.setModule(alterModule);
    EvaluationStatusGeneral alterStatus = new EvaluationStatusGeneral();
    alterStatus.setCode(properties.getResModuleNoDecisionCode());
    alterStatus.setDescription(properties.getResModuleNoDecisionName());
    alter.setStatus(alterStatus);
    alter.setEvaluationRules(null);
    
    List<EvaluationDetail> otherModules = new ArrayList<>(16);
    otherModules.add(propas);
    otherModules.add(proact);
    otherModules.add(posbcp);
    otherModules.add(perfil);
    otherModules.add(gral);
    otherModules.add(tran);
    otherModules.add(sunat);
    otherModules.add(sbs);
    otherModules.add(pricing);
    otherModules.add(modbhs);
    otherModules.add(modapp);
    otherModules.add(linea);
    otherModules.add(evalua);
    otherModules.add(cem);
    otherModules.add(an);
    otherModules.add(alter);
    return otherModules;
  }

  /**
   * Method initAn.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initAn(String code) {
    PostAdditionalEvaluationsResponse an = new PostAdditionalEvaluationsResponse();
    an.setStatusCode(code);
    an.setModuleCode(properties.getArchivoNegativo());
    an.setModuleDescription(properties.getArchivoNegativoDescription());
    an.setModuleClassification(properties.getTipoCrediticia());
    List<PostModuleResultsResponse> resultsAn = new ArrayList<>();
    an.setModuleResults(resultsAn);
    return an;
  }

  /**
   * Method initTs.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initTs(String code) {
    PostAdditionalEvaluationsResponse ts = new PostAdditionalEvaluationsResponse();
    ts.setStatusCode(code);
    ts.setModuleCode(properties.getTasa());
    ts.setModuleDescription(properties.getTasaDescription());
    ts.setModuleClassification(properties.getTipoComercial());
    List<PostModuleResultsResponse> resultsTs = new ArrayList<>();
    ts.setModuleResults(resultsTs);
    return ts;
  }


  /**
   * Method initTa.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initTa(String code) {
    PostAdditionalEvaluationsResponse ta = new PostAdditionalEvaluationsResponse();
    ta.setStatusCode(code);
    ta.setModuleCode(properties.getTasaAutorizada());
    ta.setModuleDescription(properties.getTasaAutorizadaDescription());
    ta.setModuleClassification(properties.getTipoPricing());
    List<PostModuleResultsResponse> resultsTa = new ArrayList<>();
    ta.setModuleResults(resultsTa);
    return ta;
  }


  /**
   * Method initBp.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initBp(String code) {
    PostAdditionalEvaluationsResponse bp = new PostAdditionalEvaluationsResponse();
    bp.setStatusCode(code);
    bp.setModuleCode(properties.getBoletasPago());
    bp.setModuleDescription(properties.getBoletasPagoDescription());
    bp.setModuleClassification(properties.getTipoDocumental());
    List<PostModuleResultsResponse> resultsBp = new ArrayList<>();
    bp.setModuleResults(resultsBp);
    return bp;
  }

  /**
   * Method initIs.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initIs(String code) {
    PostAdditionalEvaluationsResponse is = new PostAdditionalEvaluationsResponse();
    is.setStatusCode(code);
    is.setModuleCode(properties.getIngresosSustentados());
    is.setModuleDescription(properties.getIngresosSustentadosDescription());
    is.setModuleClassification(properties.getTipoDocumental());
    List<PostModuleResultsResponse> resultsIs = new ArrayList<>();
    is.setModuleResults(resultsIs);
    return is;
  }


  /**
   * Method initRh.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initRh(String code) {
    PostAdditionalEvaluationsResponse rh = new PostAdditionalEvaluationsResponse();
    rh.setStatusCode(code);
    rh.setModuleCode(properties.getReciboHonorarios());
    rh.setModuleDescription(properties.getReciboHonorariosDescription());
    rh.setModuleClassification(properties.getTipoDocumental());
    List<PostModuleResultsResponse> resultsRh = new ArrayList<>();
    rh.setModuleResults(resultsRh);
    return rh;
  }


  /**
   * Method initCem.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initCem(String code) {
    PostAdditionalEvaluationsResponse cde = new PostAdditionalEvaluationsResponse();
    cde.setStatusCode(code);
    cde.setModuleCode(properties.getCapacidadPago());
    cde.setModuleDescription(properties.getCapacidadPagoDescription());
    cde.setModuleClassification(properties.getTipoCrediticia());
    List<PostModuleResultsResponse> resultsCde = new ArrayList<>();
    cde.setModuleResults(resultsCde);
    return cde;
  }


  /**
   * Method initSbs.
   * 
   * @return {@link PostAdditionalEvaluationsResponse}
   */
  public PostAdditionalEvaluationsResponse initSbs(String code) {
    PostAdditionalEvaluationsResponse sbs = new PostAdditionalEvaluationsResponse();
    sbs.setStatusCode(code);
    sbs.setModuleCode(properties.getClasificacionSbs());
    sbs.setModuleDescription(properties.getClasificacionSbsDescription());
    sbs.setModuleClassification(properties.getTipoCrediticia());
    List<PostModuleResultsResponse> resultsSbs = new ArrayList<>();
    sbs.setModuleResults(resultsSbs);
    return sbs;
  }

}
