package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PostAdditionalIncomeResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostAdditionalIncomeResponse {

  @ApiModelProperty(name = "amount", value = "valor de otros ingresos.", example = "1500.00",
      dataType = "Number", position = 28)
  private BigDecimal amount;

  @ApiModelProperty(name = "currencyCode", value = "Código de Moneda.", example = "PEN|USD",
      position = 29)
  private String currencyCode;

  @ApiModelProperty(name = "periodTypeCode",
      value = "Unidad de tiempo con la que el cliente recibe este ingreso.", example = "M",
      position = 30)
  private String periodTypeCode;

  @ApiModelProperty(name = "relatedSource", value = "Código de Fuente del Ingreso = ADICIONAL.",
      dataType = "String", example = "ADICIONAL", position = 31)
  private String relatedSource;

}
