package com.bcp.atlas.services.channel.loanevaluation.proxy.atlas;


import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import retrofit2.http.*;

public interface EvaluateApi {

  @GET("servicing/customer-order/v1/evaluation-exception-orders")
  io.reactivex.Maybe<GetResponse> getEvaluationExceptionOrdersUsingGET3(
      @retrofit2.http.Query("exceptionId") Integer exceptionId);

  @Headers({"Content-Type:application/json"})
  @POST("servicing/customer-order/v1/evaluation-exception-orders/evaluate")
  io.reactivex.Single<PostResponse> postEvaluateUsingPOST3
      (@retrofit2.http.Body PostRequest request);

}
