package com.bcp.atlas.services.channel.util;

import static lombok.AccessLevel.PRIVATE;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Class that allows to reuse operations for the business of the api.<br/>
 * <b>Class</b>: GenericUtil<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@NoArgsConstructor(access = PRIVATE)
@Slf4j
public final class GenericUtil {

  /**
   * Metodo para un numero a partitr de la fecha y hora actual en milisegundos.
   * 
   * @return value
   */
  public static String generateOperationNumber() {
    String timeStamp = String.valueOf(System.currentTimeMillis());
    int length = timeStamp.length();
    timeStamp = timeStamp.substring(length - 8);
    log.info("numero de operacion: {}", timeStamp);
    return timeStamp;
  }

  /**
   * Convertir String to Integer.
   * 
   * @param valor {@link String}
   * @return int
   */
  public static int convertStrToInt(String valor) {
    return (valor == null || valor.isEmpty()) ? 0 : Integer.parseInt(valor);
  }

}
