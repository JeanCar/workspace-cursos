package com.bcp.atlas.services.channel.loanevaluation.proxy.atlas;

import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import io.reactivex.Single;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface LoanEvaluationApi {
  
  @Headers({"Content-Type:application/json;charset=UTF-8"})
  @POST("customer-management/customer-credit-rating/v1/loan-evaluations")
  Single<LoanEvaluationResponse> creditEvaluationUsingPOST3(
      @retrofit2.http.Body LoanEvaluationRequest request);
}
