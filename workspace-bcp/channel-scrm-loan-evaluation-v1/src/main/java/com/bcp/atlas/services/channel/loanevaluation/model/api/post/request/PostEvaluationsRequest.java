package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br>
 * <b>Class</b>: PostEvaluationsRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostEvaluationsRequest {

  @ApiModelProperty(name = "debtClassificationBcp", value = "clasificacion deudora BCP.",
      example = "0|1|2|3|4", required = true, position = 31)
  @NotNull
  @NotEmpty
  @Size(min = 1, max = 1)
  private String debtClassificationBcp;

  @ApiModelProperty(name = "incomeOutsideGuideline",
      value = "ingreso sustentado fuera de pauta desde salesforce.", example = "8000.00",
      dataType = "Number", required = true, position = 32)
  @NotNull
  private BigDecimal incomeOutsideGuideline;

  @ApiModelProperty(name = "totalDigitizedIncome",
      value = "ingreso total digitado desde salesforce.", example = "3000.00", dataType = "Number",
      required = true, position = 33)
  @NotNull
  private BigDecimal totalDigitizedIncome;

  @ApiModelProperty(name = "customerSegment", value = "segmento del cliente desde salesforce.",
      example = "M1N|M1E|P1N|P1E|X1N|X1E|LEE|LEN|Q1N", required = true, position = 34)
  @NotNull
  @NotEmpty
  @Size(max = 3)
  private String customerSegment;

  @ApiModelProperty(name = "incomeMark",
      value = "Marca del sustento de ingreso, cuando el cliente solicita que para el"
          + " calculo del CEM se incluya los ingresos del conyuge. desde salesforce",
      example = "true|false", dataType = "Boolean", required = true, position = 35)
  @NotNull
  private Boolean incomeMark;

}
