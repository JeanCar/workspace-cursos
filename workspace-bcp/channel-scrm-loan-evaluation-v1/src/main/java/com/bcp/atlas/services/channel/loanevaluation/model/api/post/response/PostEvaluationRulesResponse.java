package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PostEvaluationRulesResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostEvaluationRulesResponse {


  @ApiModelProperty(name = "code", value = "Nombre de la regla", dataType = "String",
      example = "rs_ProAct_NoDecisor_PoliticasDenegacion_Bloqueos_C", position = 47)
  private String code;

  @ApiModelProperty(name = "description", value = "Descripion de la regla",
      example = "PoliticasDenegacion_Bloqueos", position = 48)
  private String description;

  @ApiModelProperty(name = "contextCode", value = "Referencia a entidad evaluada.", example = "S",
      position = 49)
  private String contextCode;

  @ApiModelProperty(name = "statusCode", value = "Codigo de Resultado de la decision de la regla",
      example = "D|I|N|A", position = 50)
  private String statusCode;

  @ApiModelProperty(name = "statusDescription",
      value = "Descripcion de Resultado de la decision de la regla.", example = "Decline",
      position = 51)
  private String statusDescription;

  @ApiModelProperty(name = "reasons", value = "", example = "", position = 52)
  private List<PostReasonsResponse> reasons;

}
