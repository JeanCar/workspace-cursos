package com.bcp.atlas.services.channel.config;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;



/**
 * Spring configuration for external Config-Server<b>Retrofit</b>.<br/>
 * <b>Class</b>: IncomesProperties<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Co mpany</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li> // *
 *         </ul>
 * @version 1.0
 */

@ConfigurationProperties(prefix = "application.cem")
@Getter
@Setter
@Component
@RefreshScope
@SuppressFBWarnings("USFW_UNSYNCHRONIZED_SINGLETON_FIELD_WRITES")
public class IncomesProperties {

  private List<Income> incomes;

  
  /**
   * <b>Class</b>: Income.<br/>
   */
  @Getter
  @Setter
  @ToString
  public static class Income {

    private BigDecimal min;
    private BigDecimal max;
    private Map<String, BigDecimal> segments;

  }

}
