package com.bcp.atlas.services.channel;

import com.bcp.atlas.core.starter.web.runner.StarterWebApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Main class for running Spring Boot framework.<br/>
 * <b>Class</b>: AtlasChannelLoanEvaluationApplication<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@ComponentScan(lazyInit = true)
public class AtlasChannelLoanEvaluationApplication extends StarterWebApplication {

  /**
   * Main method.
   */
  public static void main(String[] args) {
    new SpringApplication(AtlasChannelLoanEvaluationApplication.class).run(args);
  }
}