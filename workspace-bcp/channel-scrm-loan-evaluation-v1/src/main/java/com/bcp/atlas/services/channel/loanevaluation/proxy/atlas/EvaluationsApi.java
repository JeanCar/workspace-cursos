package com.bcp.atlas.services.channel.loanevaluation.proxy.atlas;


import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import io.reactivex.Single;
import java.util.List;
import retrofit2.http.*;


public interface EvaluationsApi {

  @Headers({"Content-Type:application/stream+json;charset=UTF-8"})
  @GET("sales/underwriting/v3/evaluations")
  Single<List<EvaluationElement>> getEvaluationsUsingGET3(
      @retrofit2.http.Query("personId") String personId,
      @retrofit2.http.Query("isActive") Boolean isActive);

}
