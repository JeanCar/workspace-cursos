package com.bcp.atlas.services.channel.config;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Spring configuration for external Config-Server<b>Retrofit</b>.<br/>
 * <b>Class</b>: ApplicationProperties<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@Lazy
@Configuration
public class ApplicationProperties {

  @NotNull
  @Value("${application.component}")
  private String apiComponent;

  @NotNull
  @Value("${application.loan.header.org-code}")
  private String orgCode;

  @NotNull
  @Value("${application.loan.header.agency-code}")
  private String agencyCode;

  @NotNull
  @Value("${application.loan.header.client-code}")
  private String clientCode;

  @NotNull
  @Value("${application.loan.header.opn-code}")
  private String opnCode;

  @NotNull
  @Value("${application.loan.header.guild-client}")
  private String guildClient;

  @NotNull
  @Value("${application.loan.header.app-name}")
  private String appName;

  @NotNull
  @Value("${application.loan.header.server-app}")
  private String serverApp;

  @NotNull
  @Value("${application.loan.header.branch-office-code}")
  private String branchOfficeCode;

  @NotNull
  @Value("${application.loan.header.server-name}")
  private String serverName;

  @NotNull
  @Value("${application.loan.header.server-terminal}")
  private String serverTerminal;

  @NotNull
  @Value("${application.loan.header.opn-type}")
  private String opnType;

  @NotNull
  @Value("${application.loan.header.server-ip}")
  private String serverIp;

  @NotNull
  @Value("${application.loan.request.amount}")
  private String reqLoanAmount;

  @NotNull
  @Value("${application.loan.request.currency-code}")
  private String reqLoanCurrencyCode;

  @NotNull
  @Value("${application.loan.request.currency-description}")
  private String reqLoanCurrencyDescription;

  @NotNull
  @Value("${application.loan.request.paymentPeriodValue}")
  private String reqLoanPaymenPeriodValue;

  @NotNull
  @Value("${application.loan.request.paymentPeriodUnit-code}")
  private String reqLoanPaymentPeriodUnitCode;

  @NotNull
  @Value("${application.loan.request.paymentPeriodUnit-description}")
  private String reqLoanPaymentPeriodUnitDescription;

  @NotNull
  @Value("${application.loan.request.relationTypeDescription}")
  private String reqPersonRelationTypeDescription;

  @NotNull
  @Value("${application.loan.response.relatedSource.pdh}")
  private String resRelatedSourcPdh;

  @NotNull
  @Value("${application.loan.response.relatedSource.principal}")
  private String resRelatedSourcePrincipal;

  @NotNull
  @Value("${application.loan.response.relatedSource.adicional}")
  private String resRelatedSourceAdicional;

  @NotNull
  @Value("${application.loan.response.general.decline.code}")
  private String resGeneralDeclineCode;

  @NotNull
  @Value("${application.loan.response.general.decline.character}")
  private String resGeneralDeclineCharacter;

  @NotNull
  @Value("${application.loan.response.general.decline.name}")
  private String resGeneralDeclineName;

  @NotNull
  @Value("${application.loan.response.modules.decline.code}")
  private String resModuleDeclineCode;

  @NotNull
  @Value("${application.loan.response.modules.decline.name}")
  private String resModuleDeclineName;

  @NotNull
  @Value("${application.loan.response.modules.decline.character}")
  private String resModuleDeclineCharacter;

  @NotNull
  @Value("${application.loan.response.modules.recommend-decline.code}")
  private String resModuleRecommendDeclineCode;

  @NotNull
  @Value("${application.loan.response.modules.recommend-decline.character}")
  private String resModuleRecommendDeclineCharacter;

  @NotNull
  @Value("${application.loan.response.modules.recommend-decline.name}")
  private String resModuleRecommendDeclineName;

  @NotNull
  @Value("${application.loan.response.modules.investigate.code}")
  private String resModuleInvestigateCode;

  @NotNull
  @Value("${application.loan.response.modules.investigate.character}")
  private String resModuleInvestigateCharacter;

  @NotNull
  @Value("${application.loan.response.modules.investigate.name}")
  private String resModuleInvestigateName;

  @NotNull
  @Value("${application.loan.response.modules.approve.code}")
  private String resModuleApproveCode;

  @NotNull
  @Value("${application.loan.response.modules.approve.character}")
  private String resModuleApproveCharacter;

  @NotNull
  @Value("${application.loan.response.modules.approve.name}")
  private String resModuleApproveName;

  @NotNull
  @Value("${application.loan.response.modules.recommend-approve.code}")
  private String resModuleRecommendApproveCode;

  @NotNull
  @Value("${application.loan.response.modules.recommend-approve.character}")
  private String resModuleRecommendApproveCharacter;

  @NotNull
  @Value("${application.loan.response.modules.recommend-approve.name}")
  private String resModuleRecommendApproveName;

  @NotNull
  @Value("${application.loan.response.modules.no-decision.code}")
  private String resModuleNoDecisionCode;

  @NotNull
  @Value("${application.loan.response.modules.no-decision.character}")
  private String resModuleNoDecisionCharacter;

  @NotNull
  @Value("${application.loan.response.modules.no-decision.name}")
  private String resModuleNoDecisionName;

  @NotNull
  @Value("${application.modules.fullevaluation}")
  private boolean modulesFullEvaluation;

  @NotNull
  @Value("${application.modules.sbs.name}")
  private String clasificacionSbs;

  @NotNull
  @Value("${application.modules.sbs.description}")
  private String clasificacionSbsDescription;

  @NotNull
  @Value("${application.modules.an.name}")
  private String archivoNegativo;

  @NotNull
  @Value("${application.modules.an.description}")
  private String archivoNegativoDescription;

  @NotNull
  @Value("${application.modules.cem.name}")
  private String capacidadPago;

  @NotNull
  @Value("${application.modules.cem.description}")
  private String capacidadPagoDescription;

  @NotNull
  @Value("${application.modules.is.name}")
  private String ingresosSustentados;

  @NotNull
  @Value("${application.modules.is.description}")
  private String ingresosSustentadosDescription;

  @NotNull
  @Value("${application.modules.bp.name}")
  private String boletasPago;

  @NotNull
  @Value("${application.modules.bp.description}")
  private String boletasPagoDescription;

  @NotNull
  @Value("${application.modules.rh.name}")
  private String reciboHonorarios;

  @NotNull
  @Value("${application.modules.rh.description}")
  private String reciboHonorariosDescription;

  @NotNull
  @Value("${application.modules.ts.name}")
  private String tasa;

  @NotNull
  @Value("${application.modules.ts.description}")
  private String tasaDescription;

  @NotNull
  @Value("${application.modules.ta.name}")
  private String tasaAutorizada;

  @NotNull
  @Value("${application.modules.ta.description}")
  private String tasaAutorizadaDescription;

  @NotNull
  @Value("${application.modules.sbs.exceptionId}")
  private int clasificacionSbsId;

  @NotNull
  @Value("${application.modules.an.exceptionId}")
  private int archivoNegativoId;

  @NotNull
  @Value("${application.modules.cem.exceptionId}")
  private int capacidadaEndeudamientoId;

  @NotNull
  @Value("${application.modules.is.exceptionId}")
  private int ingresosSustentadosId;

  @NotNull
  @Value("${application.modules.bp.exceptionId}")
  private int boletasPagoId;

  @NotNull
  @Value("${application.modules.rh.exceptionId}")
  private int reciboHonorariosId;

  @NotNull
  @Value("${application.modules.ts.exceptionId}")
  private int tasaId;

  @NotNull
  @Value("${application.modules.ta.exceptionId}")
  private int tasaAutorizadaId;

  @NotNull
  @Value("${application.modules.cda.others.propas.code}")
  private String propasCode;

  @NotNull
  @Value("${application.modules.cda.others.propas.description}")
  private String propasDescription;

  @NotNull
  @Value("${application.modules.cda.others.proact.code}")
  private String proactCode;

  @NotNull
  @Value("${application.modules.cda.others.proact.description}")
  private String proactDescription;

  @NotNull
  @Value("${application.modules.cda.others.posbcp.code}")
  private String posbcpCode;

  @NotNull
  @Value("${application.modules.cda.others.posbcp.description}")
  private String posbcpDescription;

  @NotNull
  @Value("${application.modules.cda.others.perfil.code}")
  private String perfilCode;

  @NotNull
  @Value("${application.modules.cda.others.perfil.description}")
  private String perfilDescription;

  @NotNull
  @Value("${application.modules.cda.others.gral.code}")
  private String gralCode;

  @NotNull
  @Value("${application.modules.cda.others.gral.description}")
  private String gralDescription;

  @NotNull
  @Value("${application.modules.cda.others.tran.code}")
  private String tranCode;

  @NotNull
  @Value("${application.modules.cda.others.tran.description}")
  private String tranDescription;

  @NotNull
  @Value("${application.modules.cda.others.sunat.code}")
  private String sunatCode;

  @NotNull
  @Value("${application.modules.cda.others.sunat.description}")
  private String sunatDescription;

  @NotNull
  @Value("${application.modules.cda.others.sbs.code}")
  private String sbsCode;

  @NotNull
  @Value("${application.modules.cda.others.sbs.description}")
  private String sbsDescription;

  @NotNull
  @Value("${application.modules.cda.others.pricing.code}")
  private String pricingCode;

  @NotNull
  @Value("${application.modules.cda.others.pricing.description}")
  private String pricingDescription;

  @NotNull
  @Value("${application.modules.cda.others.modbhs.code}")
  private String modbhsCode;

  @NotNull
  @Value("${application.modules.cda.others.modbhs.description}")
  private String modbhsDescription;

  @NotNull
  @Value("${application.modules.cda.others.modapp.code}")
  private String modappCode;

  @NotNull
  @Value("${application.modules.cda.others.modapp.description}")
  private String modappDescription;

  @NotNull
  @Value("${application.modules.cda.others.linea.code}")
  private String lineaCode;

  @NotNull
  @Value("${application.modules.cda.others.linea.description}")
  private String lineaDescription;

  @NotNull
  @Value("${application.modules.cda.others.evalua.code}")
  private String evaluaCode;

  @NotNull
  @Value("${application.modules.cda.others.evalua.description}")
  private String evaluaDescription;

  @NotNull
  @Value("${application.modules.cda.others.cem.code}")
  private String cemCode;

  @NotNull
  @Value("${application.modules.cda.others.cem.description}")
  private String cemDescription;

  @NotNull
  @Value("${application.modules.cda.others.an.code}")
  private String anCode;

  @NotNull
  @Value("${application.modules.cda.others.an.description}")
  private String anDescription;

  @NotNull
  @Value("${application.modules.cda.others.alter.code}")
  private String alterCode;

  @NotNull
  @Value("${application.modules.cda.others.alter.description}")
  private String alterDescription;

  @NotNull
  @Value("${application.cem.autonomy0.level}")
  private String autonomyLevelCero;

  @NotNull
  @Value("${application.cem.autonomy0.calculado.dos}")
  private String calculadoDos;

  @NotNull
  @Value("${application.cem.autonomy0.segment-ab.name}")
  private String autonomyCeroAbSegmentName;

  @NotNull
  @Value("${application.cem.autonomy0.segment-ab.cem}")
  private String autonomyCeroAbSegmentCem;

  @NotNull
  @Value("${application.cem.autonomy4.level}")
  private String autonomyLevelCuatro;

  @NotNull
  @Value("${application.cem.autonomy4.calculado.cinco}")
  private String calculadoCinco;

  @NotNull
  @Value("${application.cem.autonomy4.segment-ab.name}")
  private String autonomyCuatroAbSegmentName;

  @NotNull
  @Value("${application.cem.autonomy4.segment-ab.cem}")
  private String autonomyCuatroAbSegmentCem;

  @NotNull
  @Value("${application.cem.autonomy4.segment-c.name}")
  private String autonomyCuatroCeSegmentName;

  @NotNull
  @Value("${application.cem.autonomy4.segment-c.cem}")
  private String autonomyCuatroCeSegmentCem;

  @NotNull
  @Value("${application.cem.autonomy5.level}")
  private String autonomyLevelCinco;

  @NotNull
  @Value("${application.cem.autonomy5.segment-de.name}")
  private String autonomyCincoDeSegmentName;

  @NotNull
  @Value("${application.classification.name}")
  private String classificationCrediticia;
  @NotNull

  @Value("${application.classification.tipo.crediticia}")
  private String tipoCrediticia;

  @NotNull
  @Value("${application.classification.tipo.documental}")
  private String tipoDocumental;

  @NotNull
  @Value("${application.classification.tipo.comercial}")
  private String tipoComercial;

  @NotNull
  @Value("${application.classification.tipo.pricing}")
  private String tipoPricing;
  
  @NotNull
  @Value("${application.env}")
  private String env;
}
