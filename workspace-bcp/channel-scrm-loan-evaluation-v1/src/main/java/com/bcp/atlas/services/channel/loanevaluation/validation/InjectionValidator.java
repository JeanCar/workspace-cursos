package com.bcp.atlas.services.channel.loanevaluation.validation;

import static lombok.AccessLevel.PRIVATE;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.NoArgsConstructor;

/**
 * <br>
 * Utility class to validate string safety.<br>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br>
 *         <u>Changes</u>:<br>
 *         <ul>
 *         <li>Set 09, 2019 Creación de la clase.
 *         </ul>
 * @author Wilder Jean Carlos
 * @since 1
 */

@NoArgsConstructor(access = PRIVATE)
public final class InjectionValidator {

  private static final List<Pattern> COMPILED_PATTERNS;

  static {
    List<String> regexList = Arrays.asList("onload", "onmouseover", "onclick", "isnan", "parseint",
        "parsefloat", "unescape", "isfinite", "decodeuri", "decodeuricomponent", "stringify",
        "setTimeout", "<script>", "</script>", "(.) AND (.)=(.)", "src=", ";--%", "';(.*?);'",
        "javascript:", "vbscript:", "onload\\((.*?)\\(", "<script(.*?)>", "eval\\((.*?)\\)",
        "expression\\((.*?)\\)", "'(.*?)' LIKE '(.*?)'", "alert\\((.*?)\\)", "and (.*?)=(.*?)",
        "or (.*?)=(.*?)", "%s%n", "%n%s", "%n%n", "%s%s", "%25s", "%25n", "\"&(.*?)&\"", "\"\\|",
        "\\|(.*?)/", "\\|sleep", "&sleep(.*?)&", "timeout", "start-sleep");

    COMPILED_PATTERNS = regexList.stream().map(patternExpression -> {
      String regex = "(.*?)" + patternExpression + "(.*?)";
      return Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
    }).collect(Collectors.toList());
  }

  /**
   * Validate the safety of a given string.
   *
   * @param input Input string to validate.
   * @return true if the string is considered safe, otherwise returns false.
   */
  public static boolean isSafe(final String input) {
    if (input == null) {
      return true;
    }
    boolean errorExists =
        COMPILED_PATTERNS.stream().anyMatch(pattern -> pattern.matcher(input).matches());
    return !errorExists;
  }

}
