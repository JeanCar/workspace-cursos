package com.bcp.atlas.services.channel.loanevaluation.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * <br>
 * Annotation to validate possible injection attacks.<br>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br>
 * <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br>
 * <u>Changes</u>:<br>
 * <ul>
 * <li>Set 09, 2019 Creación de la clase.
 * </ul>
 * @author  Wilder Jean Carlos
 * @since 1
 */
@Documented
@Constraint(validatedBy = InjectionSafeValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectionSafe {

  /**
   * The message.
   *
   * @return The message.
   */
  String message() default "value not allowed.";

  /**
   * The groups.
   *
   * @return the groups.
   */
  Class<?>[] groups() default {};

  /**
   * The payload.
   *
   * @return the payload.
   */
  Class<? extends Payload>[] payload() default {};

}
