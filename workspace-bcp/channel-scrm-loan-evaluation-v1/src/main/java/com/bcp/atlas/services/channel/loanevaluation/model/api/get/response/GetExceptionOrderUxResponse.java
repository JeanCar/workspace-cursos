package com.bcp.atlas.services.channel.loanevaluation.model.api.get.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Class that represents a model of data presentation that will be entered by the api<br/>
 * <b>Class</b>: GetExceptionOrderUxResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetExceptionOrderUxResponse {

  @ApiModelProperty(name = "exceptionId", value = "Identificador de tabla de decision.",
      dataType = "Integer", example = "1", position = 1)
  private Integer exceptionId;

  @ApiModelProperty(name = "description", value = "Clasificacion SBS.", example = "I",
      position = 2)
  private String description;

  @ApiModelProperty(name = "evaluationFields",
      value = "Lista de variables de entrada pertenecientes a una tabla de decision.",
      dataType = "List", example = "", position = 3)
  private List<GetEvaluationFieldsResponse> evaluationFields;
}
