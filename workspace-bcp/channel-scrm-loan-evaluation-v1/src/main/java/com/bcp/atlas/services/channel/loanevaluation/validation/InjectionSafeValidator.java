package com.bcp.atlas.services.channel.loanevaluation.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * <br>
 * Validator related to {@link InjectionSafe}.<br>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br>
 *         <u>Changes</u>:<br>
 *         <ul>
 *         <li>Set 09, 2019 Creación de la clase.
 *         </ul>
 * @author Wilder Vassquez Chavez
 * @since 1
 */
public class InjectionSafeValidator implements ConstraintValidator<InjectionSafe, String> {

  @Override
  public void initialize(InjectionSafe constraintAnnotation) {}

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return InjectionValidator.isSafe(value);
  }
}
