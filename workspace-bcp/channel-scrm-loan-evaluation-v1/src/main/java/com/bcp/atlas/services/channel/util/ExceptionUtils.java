package com.bcp.atlas.services.channel.util;


import static com.bcp.atlas.core.constants.ErrorCategory.UNEXPECTED;
import static lombok.AccessLevel.PRIVATE;

import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.core.exception.AtlasExceptionBuilder;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.reactivex.MaybeSource;
import io.reactivex.SingleSource;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;


/**
 * Useful class to cast exceptions in the service<br/>
 * <b>Class</b>: ExceptionUtils<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@SuppressFBWarnings(value = "ITC_INHERITANCE_TYPE_CHECKING",
    justification = "Instance of validation is justifiable here")
@Component
@NoArgsConstructor(access = PRIVATE)
public class ExceptionUtils {

  /**
   * Return a AtlasExceptionBuilder depending of exception type.
   *
   * @param ex Throwable
   * @return AtlasExceptionBuilder
   */
  public static AtlasExceptionBuilder getAtlasExceptionBuilderFromHttpCall(Throwable ex) {

    if (ex instanceof AtlasException && ((AtlasException) ex).getErrorType() != null) {
      return ((AtlasException) ex).mutate();
    }

    return AtlasException
        .builder()
        .category(UNEXPECTED)
        .addDetail()
        .withComponent(Constants.COMPONENT_API)
        .withDescription(Constants.MESSAGE_ERROR_UNEXPECTED)
        .push();
  }


  /**
   * Method to onLoanEvaluationError.
   * 
   * @param ex {@link Throwable}
   * @return {@link SingleSource}
   */
  public static SingleSource<LoanEvaluationResponse> onLoanEvaluationError(
      Throwable ex) {
    return getAtlasExceptionBuilderFromHttpCall(ex).buildAsSingle();

  }

  public static SingleSource<PostExceptionOrderUxResponse> onLoanExceptionOrderPostError(
      Throwable ex) {
    return getAtlasExceptionBuilderFromHttpCall(ex).buildAsSingle();
  }

  /**
   * Method to onLoanExceptionOrderGetError.
   * 
   * @param ex {@link Throwable}
   * @return {@link SingleSource}
   */
  public static MaybeSource<GetExceptionOrderUxResponse> onLoanExceptionOrderGetError(
      Throwable ex) {
    return getAtlasExceptionBuilderFromHttpCall(ex).buildAsMaybe();
  }
  
  
}
