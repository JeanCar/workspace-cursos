package com.bcp.atlas.services.channel.loanevaluation.model.api.get.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiParam;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * <br>
 * <b>Class</b>: GetExceptionOrderQueryParam<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties
public class GetExceptionOrderQueryParam implements Serializable {

  private static final long serialVersionUID = -8887530613798376846L;
  @NotNull
  @Positive
  @ApiParam(name = "exceptionId",
      value = "Identificador de bloque de evaluación, solo para realizar la consulta"
          + " los parametros que se enviaran para la evaluación de las regla",
      example = "1", type = "string", required = true, hidden = true)
  private Integer exceptionId;

}
