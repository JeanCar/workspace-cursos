package com.bcp.atlas.services.channel.loanevaluation.model.api.bean;

import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <b>Class</b>: ModulesResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ModulesResponse extends PostResponse {

  private String codeError;
  private String messageError;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ModulesResponse modulesResponse = (ModulesResponse) o;
    return Objects.equals(this.codeError, modulesResponse.getCodeError())
        && Objects.equals(this.messageError, modulesResponse.getMessageError());
  }

  @Override
  public int hashCode() {
    return Objects.hash(codeError, messageError);
  }
}
