package com.bcp.atlas.services.channel.loanevaluation.business.impl;


import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.config.ModulesProperties;
import com.bcp.atlas.services.channel.config.ReasonsProperties;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostEvaluationFieldRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostExceptionRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.ArchivoNegativoWrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetRecordsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Address;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.CalculatedIncome;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Category;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.DemographicInformation;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.District;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Employer;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EmployerRelationship;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluatedPerson;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluatedPersonRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Geolocation;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.IncomeCurrency;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.IncomeTaxCategory;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.IncomeType;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Loan;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanOrder;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.MaritalStatus;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.MonthlyIncomeRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Offer;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.PaymentPeriod;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.PrimaryEconomicActivity;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Province;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Region;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.RelationType;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.SaleInformation;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.SaleType;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Type;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.Unit;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtil;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * <br/>
 * <b>Class</b>: LoanEvaluationBuilder<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase..</li>
 *         </ul>
 * @version 1.0
 */

@SuppressFBWarnings(value = "CRLF_INJECTION_LOGS", 
      justification = "view builder class information")
@Slf4j
@Component
@AllArgsConstructor
public class LoanEvaluationBuilder {

  @Autowired
  private ApplicationProperties properties;

  @Autowired
  private ReasonsProperties reasonsProperties;

  @Autowired
  private ModulesProperties modulesProperties;

  @Autowired
  private LoanEvaluationUtil loanEvaluationUtil;

  /**
   * Method to create the body towards the cross Loan Evaluations.
   *
   * @param request {@link PostLoanEvaluationsUxRequest}
   * @return {@link LoanEvaluationRequest}
   */
  public LoanEvaluationRequest buildRequest(PostLoanEvaluationsUxRequest request) {

    LoanEvaluationRequest loanRequest = new LoanEvaluationRequest();
    loanRequest.setLoanOrder(loanOrderBuild(request));
    loanRequest.setLoan(loanBuild());
    loanRequest.setSaleInformation(saleInformationBuild(request));
    loanRequest.setPersons(evaluatedPersonsBuild(request));

    return loanRequest;
  }


  private LoanOrder loanOrderBuild(PostLoanEvaluationsUxRequest request) {
    LoanOrder loanOrder = new LoanOrder();
    loanOrder.setLoanOrderId(request.getLoanOrderId());
    loanOrder.setRegisterDate(request.getRegisterDate());
    return loanOrder;
  }

  private Loan loanBuild() {
    Loan loan = new Loan();
    loan.setAmount(new BigDecimal(properties.getReqLoanAmount()));
    loan.setCurrency(incomeCurrencyBuilder());
    loan.setPaymentPeriod(paymentPeriodBuild());
    return loan;
  }

  private IncomeCurrency incomeCurrencyBuilder() {
    IncomeCurrency incomeCurrency = new IncomeCurrency();
    incomeCurrency.setCode(properties.getReqLoanCurrencyCode());
    incomeCurrency.setDescription(properties.getReqLoanCurrencyDescription());
    return incomeCurrency;
  }

  private PaymentPeriod paymentPeriodBuild() {
    PaymentPeriod paymentPeriod = new PaymentPeriod();
    paymentPeriod.setValue(properties.getReqLoanPaymenPeriodValue());
    paymentPeriod.setUnit(unitBuilder());
    return paymentPeriod;
  }

  private Unit unitBuilder() {
    Unit unit = new Unit();
    unit.setCode(properties.getReqLoanPaymentPeriodUnitCode());
    unit.setDescription(properties.getReqLoanPaymentPeriodUnitDescription());
    return unit;
  }

  private SaleInformation saleInformationBuild(PostLoanEvaluationsUxRequest request) {
    SaleInformation saleInformation = new SaleInformation();
    saleInformation.setSaleType(saleTypeBuild(request));
    saleInformation.setOffer(offerBuild(request));
    return saleInformation;
  }

  private Offer offerBuild(PostLoanEvaluationsUxRequest request) {
    Offer offer = new Offer();
    SaleType type = new SaleType();
    type.code(request.getSaleInformation().getOffer().getSaleTypeCode());
    offer.setSaleType(type);
    offer.setType(typeBuild(request));
    offer.setCategory(categoryBuild(request));
    return offer;
  }

  private SaleType saleTypeBuild(PostLoanEvaluationsUxRequest request) {
    SaleType saleType = new SaleType();
    saleType.setCode(request.getSaleInformation().getSaleTypeCode());
    return saleType;
  }


  private Type typeBuild(PostLoanEvaluationsUxRequest request) {
    Type type = new Type();
    type.setCode(request.getSaleInformation().getOffer().getTypeCode());
    return type;
  }

  private Category categoryBuild(PostLoanEvaluationsUxRequest request) {
    Category category = new Category();
    category.setCode(request.getSaleInformation().getOffer().getCategoryCode());
    return category;
  }

  private List<EvaluatedPersonRequest> evaluatedPersonsBuild(PostLoanEvaluationsUxRequest request) {
    return request.getPersons().stream().map(list -> {
      EvaluatedPersonRequest evaluatedPerson = new EvaluatedPersonRequest();
      evaluatedPerson.setPersonId(list.getPersonId());
      evaluatedPerson.setEstimatedIncomeRequired(list.getEstimatedIncomeRequired());
      RelationType relationType = new RelationType();
      relationType.setCode(list.getRelationTypeCode());
      relationType.setDescription(properties.getReqPersonRelationTypeDescription());
      evaluatedPerson.setRelationType(relationType);

      DemographicInformation demographicInformation = new DemographicInformation();
      demographicInformation.setBirthDate(list.getBirthDate());
      MaritalStatus maritalStatus = new MaritalStatus();
      maritalStatus.setCode(list.getMaritalStatusCode());
      demographicInformation.setMaritalStatus(maritalStatus);
      evaluatedPerson.setDemographicInformation(demographicInformation);

      if (list.getAddress() != null) {
        Geolocation geolocation = new Geolocation();
        geolocation.setCode(list.getAddress().getGeolocationCode());
        Region region = new Region();
        region.setCode(list.getAddress().getRegionCode());
        geolocation.setRegion(region);
        District district = new District();
        district.setCode(list.getAddress().getDistrictCode());
        geolocation.setDistrict(district);
        Province province = new Province();
        province.setCode(list.getAddress().getProvinceCode());
        geolocation.setProvince(province);
        Address address = new Address();
        address.setGeolocation(geolocation);
        evaluatedPerson.setAddress(address);
      }

      if (list.getMonthlyIncomes() != null) {
        List<MonthlyIncomeRequest> monthlyIncomes = list.getMonthlyIncomes().stream().map(li -> {
          MonthlyIncomeRequest monthlyIncome = new MonthlyIncomeRequest();
          monthlyIncome.setGrossAmount(String.valueOf(li.getGrossAmount()));
          monthlyIncome.setAdditionalGrossAmount(String
              .valueOf(Optional.ofNullable(li.getAdditionalGrossAmount()).orElse(BigDecimal.ZERO)));
          IncomeCurrency currency = new IncomeCurrency();
          currency.setCode(li.getCurrencyCode());
          monthlyIncome.setCurrency(currency);
          IncomeType incomeType = new IncomeType();
          incomeType.setCode(li.getIncomeTypeCode());
          monthlyIncome.setIncomeType(incomeType);
          IncomeTaxCategory incomeTaxCategory = new IncomeTaxCategory();
          incomeTaxCategory.setCode(String.valueOf(li.getIncomeTaxCategoryCode()));
          monthlyIncome.setIncomeTaxCategory(incomeTaxCategory);

          if (li.getEmployerId() != null || li.getRelationshipStartDate() != null) {
            EmployerRelationship employerRelationship = new EmployerRelationship();
            if (li.getEmployerId() != null) {
              Employer employer = new Employer();
              employer.setRuc(li.getEmployerId());
              if (li.getEmployerPrimaryEconomicActivityCode() != null) {
                PrimaryEconomicActivity primaryEconomicActivity = new PrimaryEconomicActivity();
                primaryEconomicActivity.setCode(li.getEmployerPrimaryEconomicActivityCode());
                employer.setPrimaryEconomicActivity(primaryEconomicActivity);
              }
              employerRelationship.setEmployer(employer);
            }

            if (li.getRelationshipStartDate() != null) {
              employerRelationship.setRelationshipStartDate(li.getRelationshipStartDate());
            }
            monthlyIncome.setEmployerRelationship(employerRelationship);
          }
          return monthlyIncome;
        }).collect(Collectors.toList());
        evaluatedPerson.setMonthlyIncomes(monthlyIncomes);
      }

      return evaluatedPerson;
    }).collect(Collectors.toList());


  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Clasificacion SBS
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @return {@link PostRequest}
   */
  public PostRequest builderClasificacionSbsRequest(LoanEvaluationResponse loanEvaluationResponse) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getClasificacionSbsId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getSbs().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest clasificacionSbs = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.SBS01.equals(name)) {
            clasificacionSbs.setName(entry.getValue());
            Optional<String> value = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(m -> Optional.ofNullable(m.getRiskSegment().getCode())
                    .orElse(Constants.SEGMENT_E))
                .findFirst();
            if (value.isPresent()) {
              clasificacionSbs.setValue(loanEvaluationUtil.captureRiskSegment(value.get()));
            }
          }

          return clasificacionSbs;
        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;

  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Archivo Negativo
   * 
   * @param archivoNegatioWrapper {@link ArchivoNegatioWrapper}
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @param request {@link PostLoanEvaluationsUxRequest}
   * @return {@link PostRequest}
   */
  public PostRequest builderArchivoNegativoRequest(ArchivoNegativoWrapper archivoNegatioWrapper,
      LoanEvaluationResponse loanEvaluationResponse, PostLoanEvaluationsUxRequest request) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getArchivoNegativoId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getAn().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest archivoNegativo = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.AN01.equals(name)) {
            archivoNegativo.setName(entry.getValue());
            Optional<String> value = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(m -> Optional.ofNullable(m.getRiskSegment().getCode())
                    .orElse(Constants.SEGMENT_E))
                .findFirst();
            if (value.isPresent()) {
              archivoNegativo.setValue(loanEvaluationUtil.captureRiskSegment(value.get()));
            }
          } else if (Constants.AN02.equals(name)) {
            archivoNegativo.setName(entry.getValue());
            archivoNegativo.setValue(Optional.ofNullable(archivoNegatioWrapper.getMonths())
                .orElse(Constants.CERO_STRING));
          } else if (Constants.AN03.equals(name)) {
            archivoNegativo.setName(entry.getValue());
            archivoNegativo.setValue(
                Optional.ofNullable(archivoNegatioWrapper.getReason()).orElse(StringUtils.EMPTY));
          } else if (Constants.AN04.equals(name)) {
            archivoNegativo.setName(entry.getValue());
            archivoNegativo
                .setValue(Optional.ofNullable(request.getEvaluations().getDebtClassificationBcp())
                    .orElse(Constants.CERO_STRING));
          } else if (Constants.AN05.equals(name)) {
            archivoNegativo.setName(entry.getValue());
            archivoNegativo.setValue(String.valueOf(
                Optional.ofNullable(archivoNegatioWrapper.getAmount()).orElse(BigDecimal.ZERO)));

          }

          return archivoNegativo;
        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }


  /**
   * Method to create the body towards the cross Evaluation Exception Order. Cde
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @return {@link PostRequest}
   */
  public PostRequest builderCdeRequest(LoanEvaluationResponse loanEvaluationResponse) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getCapacidadaEndeudamientoId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getCem().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest cde = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.CDE01.equals(name)) {
            cde.setName(entry.getValue());
            Optional<String> value = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(m -> Optional.ofNullable(m.getRiskSegment().getCode())
                    .orElse(Constants.SEGMENT_E))
                .findFirst();
            if (value.isPresent()) {
              cde.setValue(loanEvaluationUtil.captureRiskSegment(value.get()));
            }
          }
          return cde;
        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Ingresos Sustentados
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @param request {@link PostLoanEvaluationsUxRequest}
   * @return {@link PostRequest}
   */
  public PostRequest builderIngresosSustentadosRequest(
      LoanEvaluationResponse loanEvaluationResponse, PostLoanEvaluationsUxRequest request) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getIngresosSustentadosId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getIs().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest ingresoSustentado = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.IS01.equals(name)) {
            ingresoSustentado.setName(entry.getValue());
            Optional<String> value = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(m -> Optional.ofNullable(m.getRiskSegment().getCode())
                    .orElse(Constants.SEGMENT_E))
                .findFirst();
            if (value.isPresent()) {
              ingresoSustentado.setValue(loanEvaluationUtil.captureRiskSegment(value.get()));
            }

          } else if (Constants.IS02.equals(name)) {
            ingresoSustentado.setName(entry.getValue());
            ingresoSustentado.setValue(String
                .valueOf(Optional.ofNullable(request.getEvaluations().getIncomeOutsideGuideline())
                    .orElse(BigDecimal.ZERO)));
          } else if (Constants.IS03.equals(name)) {
            ingresoSustentado.setName(entry.getValue());
            ingresoSustentado.setValue(String
                .valueOf(Optional.ofNullable(request.getEvaluations().getTotalDigitizedIncome())
                    .orElse(BigDecimal.ZERO)));
          }
          return ingresoSustentado;

        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Boletas de Pago
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @return {@link PostRequest}
   */
  public PostRequest builderBoletasPagoRequest(LoanEvaluationResponse loanEvaluationResponse) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getBoletasPagoId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getBp().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest boletaPagos = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.BP01.equals(name)) {
            boletaPagos.setName(entry.getValue());
            Optional<String> value = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(m -> Optional.ofNullable(m.getRiskSegment().getCode())
                    .orElse(Constants.SEGMENT_E))
                .findFirst();
            if (value.isPresent()) {
              boletaPagos.setValue(loanEvaluationUtil.captureRiskSegment(value.get()));
            }
          }
          return boletaPagos;
        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Recibos por Honorarios
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @return {@link PostRequest}
   */
  public PostRequest builderReciboHonorariosRequest(LoanEvaluationResponse loanEvaluationResponse) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getReciboHonorariosId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getRh().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest reciboHonorarios = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.RH01.equals(name)) {
            reciboHonorarios.setName(entry.getValue());
            Optional<String> value = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(m -> Optional.ofNullable(m.getRiskSegment().getCode())
                    .orElse(Constants.SEGMENT_E))
                .findFirst();
            if (value.isPresent()) {
              reciboHonorarios.setValue(loanEvaluationUtil.captureRiskSegment(value.get()));
            }
          }
          return reciboHonorarios;
        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Tasa
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @return {@link PostRequest}
   */
  public PostRequest builderTasaRequest(LoanEvaluationResponse loanEvaluationResponse) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getTasaId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getTs().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest tasa = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.TS01.equals(name)) {
            tasa.setName(entry.getValue());
            loanEvaluationResponse.getPersons().stream()
                .filter(
                    list -> Constants.TITULAR.equalsIgnoreCase(list.getRelationType().getCode()))
                .map(income -> income.getFinancialInformation().getIncomes().stream().filter(
                    f -> !f.getRelatedSource().equalsIgnoreCase(properties.getResRelatedSourcPdh())
                        && !f.getRelatedSource()
                            .equalsIgnoreCase(properties.getResRelatedSourcePrincipal())
                        && !f.getRelatedSource()
                            .equalsIgnoreCase(properties.getResRelatedSourceAdicional()))
                    .max(Comparator.comparing(CalculatedIncome::getIncomeAmount))
                    .orElseGet(CalculatedIncome::new))
                .findAny().ifPresent(m -> tasa.setValue(String
                    .valueOf(Optional.ofNullable(m.getIncomeAmount()).orElse(BigDecimal.ZERO))));

          } else if (Constants.TS02.equals(name)) {
            tasa.setName(entry.getValue());
            boolean hasSalary = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(EvaluatedPerson::getHasSalaryAccount).findFirst().orElse(Boolean.FALSE);
            tasa.setValue(String.valueOf(hasSalary));
          }
          return tasa;
        }).collect(Collectors.toList());

    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order. Tasa Autorizada
   * 
   * @param loanEvaluationResponse {@link LoanEvaluationResponse}
   * @param request {@link PostLoanEvaluationsUxRequest}
   * @return {@link PostRequest}
   */
  public PostRequest builderTasaAutorizadaRequest(LoanEvaluationResponse loanEvaluationResponse,
      PostLoanEvaluationsUxRequest request) {

    PostRequest postRequest = new PostRequest();
    postRequest.setRequiredFullEvaluation(properties.isModulesFullEvaluation());
    PostExceptionRequest postExceptionRequest = new PostExceptionRequest();
    postExceptionRequest.setExceptionId(properties.getTasaAutorizadaId());

    List<PostEvaluationFieldRequest> evaluationFields =
        modulesProperties.getTa().getVariables().entrySet().stream().map(entry -> {
          PostEvaluationFieldRequest tasaAutorizada = new PostEvaluationFieldRequest();
          String name = entry.getKey();
          if (Constants.TA01.equals(name)) {
            tasaAutorizada.setName(entry.getValue());
            tasaAutorizada
                .setValue(Optional.ofNullable(request.getEvaluations().getCustomerSegment())
                    .orElse(StringUtils.EMPTY));
          } else if (Constants.TA02.equals(name)) {
            tasaAutorizada.setName(entry.getValue());
            loanEvaluationResponse.getPersons().stream()
                .filter(
                    list -> Constants.TITULAR.equalsIgnoreCase(list.getRelationType().getCode()))
                .map(income -> income.getFinancialInformation().getIncomes().stream().filter(
                    f -> !f.getRelatedSource().equalsIgnoreCase(properties.getResRelatedSourcPdh())
                        && !f.getRelatedSource()
                            .equalsIgnoreCase(properties.getResRelatedSourcePrincipal())
                        && !f.getRelatedSource()
                            .equalsIgnoreCase(properties.getResRelatedSourceAdicional()))
                    .max(Comparator.comparing(CalculatedIncome::getIncomeAmount))
                    .orElseGet(CalculatedIncome::new))
                .findAny().ifPresent(m -> tasaAutorizada.setValue(String
                    .valueOf(Optional.ofNullable(m.getIncomeAmount()).orElse(BigDecimal.ZERO))));

          } else if (Constants.TA03.equals(name)) {
            tasaAutorizada.setName(entry.getValue());
            loanEvaluationResponse.getPersons().stream()
                .filter(
                    list -> Constants.TITULAR.equalsIgnoreCase(list.getRelationType().getCode()))
                .findAny().ifPresent(obj -> tasaAutorizada.setValue(String.valueOf(
                    Optional.ofNullable(obj.getEvaluationScore()).orElse(BigDecimal.ZERO))));

          } else if (Constants.TA04.equals(name)) {
            tasaAutorizada.setName(entry.getValue());
            boolean hasSalary = loanEvaluationResponse.getPersons().stream()
                .filter(f -> Constants.TITULAR.equalsIgnoreCase(f.getRelationType().getCode()))
                .map(EvaluatedPerson::getHasSalaryAccount).findFirst().orElse(Boolean.FALSE);
            tasaAutorizada.setValue(String.valueOf(hasSalary));

          }

          return tasaAutorizada;
        }).collect(Collectors.toList());


    postExceptionRequest.setEvaluationFields(evaluationFields);
    postRequest.setException(postExceptionRequest);
    return postRequest;
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order.
   * 
   * @param objInformation {@link List}
   * @return {@link List}
   */
  public List<GetEvaluationResponse> evaluationResponse(List<EvaluationElement> objInformation) {
    return objInformation.stream().map(list -> {
      GetEvaluationResponse evaluation = new GetEvaluationResponse();
      evaluation.setStatusCode(list.getStatus().getCode());
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMATTER02);
      List<GetRecordsResponse> recordList = list.getRecords().stream().map(li -> {
        GetRecordsResponse record = new GetRecordsResponse();
        record.setRegisterDate(LocalDate.parse(li.getRegisterDate(), formatter));
        record.setDebtsAmount(li.getDebtsAmount());
        String codeReason = li.getStatus().getCode().concat(li.getInvolvedResource().getCode());
        Optional<String> value = reasonsProperties.getReasons().getValues().entrySet().stream()
            .filter(code -> codeReason.equals(code.getKey())).map(Entry::getValue).findFirst();
        if (value.isPresent()) {
          record.setReasonCode(value.get());
        }
        return record;
      }).collect(Collectors.toList());

      evaluation.setRecords(recordList);
      return evaluation;

    }).collect(Collectors.toList());
  }

  /**
   * Method to create the body towards the cross Evaluation Exception Order.
   * 
   * @param evaluationResponse {@link List}
   * @return {@link ArchivoNegativoWrapper}
   */
  public ArchivoNegativoWrapper archivoNegatioWrapperBuilder(
      List<GetEvaluationResponse> evaluationResponse) {
    ArchivoNegativoWrapper archivoNegativo = new ArchivoNegativoWrapper();
    evaluationResponse.stream()
        .map(m -> m.getRecords().stream()
            .sorted(Comparator.comparing(GetRecordsResponse::getRegisterDate).reversed())
            .collect(Collectors.toList()))
        .peek(obj -> log.info("Ordenado por fecha: {}", obj))
        .forEach(archivo -> {
          long months =
              ChronoUnit.MONTHS.between(archivo.get(0).getRegisterDate(), LocalDate.now());
          archivoNegativo.setMonths(String.valueOf(months));
          archivoNegativo.setAmount(archivo.get(0).getDebtsAmount());
          archivoNegativo.setReason(archivo.get(0).getReasonCode());
        });
    return archivoNegativo;

  }
}

