package com.bcp.atlas.services.channel.loanevaluation.business.impl;


import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.EvaluateApi;
import io.reactivex.Maybe;
import io.reactivex.Single;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <br/>
 * <b>Class</b>: LoanExceptionOrderSender<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 01, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Component
@Slf4j
@AllArgsConstructor
public class EvaluateExceptionOrderSender {

  @Autowired
  private EvaluateApi evaluateApi;

  /**
   * Check the data required for the exception request for an evaluation.
   *
   * @param exceptionId {@link Integer}
   * @return {@link Maybe}
   */
  public Maybe<GetResponse> callLoanEvaluationGet(Integer exceptionId) {
    return evaluateApi
        .getEvaluationExceptionOrdersUsingGET3(exceptionId)
        .doOnSuccess(item -> log.info("Successful"));
  }

  /**
   * Evaluation of the exception and or values ​​of an evaluation.
   *
   * @param request {@link PostRequest}
   * @return {@link Single}
   */
  public Single<PostResponse> callLoanEvaluationPost(PostRequest request) {
    return evaluateApi.postEvaluateUsingPOST3(request)
        .doOnSuccess(item -> log.info("Successful"));
  }
}
