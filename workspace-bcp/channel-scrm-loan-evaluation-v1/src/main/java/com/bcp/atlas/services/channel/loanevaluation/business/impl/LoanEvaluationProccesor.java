package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostEvaluationResultResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResultFieldResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostResponseList;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostAdditionalEvaluationsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostAdditionalIncomeResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostCalculatedIcomeResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostConsolidatedDebtCalculatedResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostConsolidatedFinancialInformationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostEvaluationRulesResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostEvaluationsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostFieldslResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostIndebtednessCapacityResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanOfferResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostModuleResultsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostPersonsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostReasonsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostRiskCalculatedIcomeResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostSalaryIncomeResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.CalculatedIncome;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.InstallmentsPeriod;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanOffers;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.InitModulesUtil;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtil;
import com.bcp.atlas.services.channel.util.ValidUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * <br/>
 * <b>Class</b>: LoanEvaluationProccesor<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Component
@AllArgsConstructor
public class LoanEvaluationProccesor {

  @Autowired
  private ValidUtils validUtils;

  @Autowired
  private ApplicationProperties properties;

  @Autowired
  private LoanEvaluationUtil loanEvaluationUtil;

  @Autowired
  private InitModulesUtil initModuleUtil;

  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @return {@Link PostLoanEvaluationsUxResponse}
   */
  public PostLoanEvaluationsUxResponse convertResponseToUx(Wrapper wrapperResponse) {

    validUtils.validate(wrapperResponse, Wrapper.class, Constants.PAQUETE_API);
    PostLoanEvaluationsUxResponse uxResponse = new PostLoanEvaluationsUxResponse();
    if (wrapperResponse.getLoanEvaluationResponse() != null) {
      convertToCda(uxResponse, wrapperResponse);
    }
    if (wrapperResponse.getPostResponseList() != null) {
      List<PostAdditionalEvaluationsResponse> evaluationInformation = new ArrayList<>();

      PostResponseList postResponseList = wrapperResponse.getPostResponseList();

      if (postResponseList.getTasa() != null) {
        String code = postResponseList.getTasa().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToTasaComercialPricing(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initTs(code));
        }
      } else {
        evaluationInformation.add(initModuleUtil.initTs(Constants.CODE_NO_EVALUADO));
      }
      if (postResponseList.getTasaAutorizada() != null) {
        String code = postResponseList.getTasaAutorizada().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToTasaAutorizadaComercialPricing(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initTa(code));
        }
      } else {
        evaluationInformation.add(initModuleUtil.initTa(Constants.CODE_NO_EVALUADO));
      }

      if (postResponseList.getBoletaPago() != null) {
        String code = postResponseList.getBoletaPago().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToDocumentalBp(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initBp(code));
        }
      } else {
        evaluationInformation.add(initModuleUtil.initBp(Constants.CODE_NO_EVALUADO));
      }

      if (postResponseList.getReciboHonorarios() != null) {
        String code = postResponseList.getReciboHonorarios().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToDocumentalRh(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initRh(code));
        }
      } else {
        evaluationInformation.add(initModuleUtil.initRh(Constants.CODE_NO_EVALUADO));
      }

      if (postResponseList.getIngresoSustentos() != null) {
        String code = postResponseList.getIngresoSustentos().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToDocumentalIs(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initIs(code));
        }
      } else {
        evaluationInformation.add(initModuleUtil.initIs(Constants.CODE_NO_EVALUADO));
      }

      if (postResponseList.getArchivoNegativo() != null) {
        String code = postResponseList.getArchivoNegativo().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToCrediticiaAn(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initAn(code));
        }

      } else {
        evaluationInformation.add(initModuleUtil.initAn(Constants.CODE_NO_EVALUADO));
      }

      if (postResponseList.getCem() != null) {
        String code = postResponseList.getCem().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          modifyCem(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initCem(code));
        }

      } else {
        evaluationInformation.add(initModuleUtil.initCem(Constants.CODE_NO_EVALUADO));
      }

      if (postResponseList.getSbs() != null) {
        String code = postResponseList.getSbs().getCode();
        if (Constants.CODE_EXITO.equals(code)) {
          convertToCrediticiaSbs(wrapperResponse, evaluationInformation, code);
        } else {
          evaluationInformation.add(initModuleUtil.initSbs(code));
        }
      } else {
        evaluationInformation.add(initModuleUtil.initSbs(Constants.CODE_NO_EVALUADO));
      }

      uxResponse.setAdditionalEvaluations(evaluationInformation);
    }

    return uxResponse;
  }

  /**
   * This method is used to parse the cross service response.
   *
   * @param uxResponse {@Link PostLoanEvaluationsUxResponse}
   * @param wrapperResponse {@Link Wrapper}
   */
  public void convertToCda(PostLoanEvaluationsUxResponse uxResponse, Wrapper wrapperResponse) {
    uxResponse
        .setLoanEvaluationId(wrapperResponse.getLoanEvaluationResponse().getLoanEvaluationId());
    uxResponse.setEvaluationStatusCode(
        wrapperResponse.getLoanEvaluationResponse().getStatus().getCode());
    uxResponse.setEvaluationStatusDescription(
        wrapperResponse.getLoanEvaluationResponse().getStatus().getDescription());

    if (wrapperResponse.getLoanEvaluationResponse().getConsolidatedFinancialInformation() != null) {

      PostConsolidatedFinancialInformationResponse information =
          new PostConsolidatedFinancialInformationResponse();
      information.setEvaluationScore(wrapperResponse.getLoanEvaluationResponse()
          .getConsolidatedFinancialInformation().getEvaluationScore());
      if (wrapperResponse.getLoanEvaluationResponse().getConsolidatedFinancialInformation()
          .getMonthlyIndebtednessCapacity() != null) {

        PostIndebtednessCapacityResponse capacity = new PostIndebtednessCapacityResponse();
        capacity.setAmount(wrapperResponse.getLoanEvaluationResponse()
            .getConsolidatedFinancialInformation().getMonthlyIndebtednessCapacity().getAmount());
        capacity.setCurrencyCode(
            wrapperResponse.getLoanEvaluationResponse().getConsolidatedFinancialInformation()
                .getMonthlyIndebtednessCapacity().getCurrency().getCode());
        information.setIndebtednessCapacity(capacity);
      }
      uxResponse.setConsolidatedFinancialInformation(information);
    }

    if (wrapperResponse.getLoanEvaluationResponse().getPersons() != null) {
      List<PostPersonsResponse> personResponse =
          wrapperResponse.getLoanEvaluationResponse().getPersons().stream().map(list -> {
            PostPersonsResponse person = new PostPersonsResponse();
            person.setEvaluationScore(list.getEvaluationScore());
            person.setHasSalaryAccount(list.getHasSalaryAccount());
            person.setRelationTypeCode(list.getRelationType().getCode());
            person.setRiskSegment(loanEvaluationUtil.captureRiskSegment(
                Optional.ofNullable(list.getRiskSegment().getCode()).orElse(Constants.SEGMENT_E)));

            PostConsolidatedDebtCalculatedResponse debt =
                new PostConsolidatedDebtCalculatedResponse();
            debt.setAmount(list.getFinancialInformation().getDebt().getAmount());
            debt.setCurrencyCode(list.getFinancialInformation().getDebt().getCurrency().getCode());
            debt.setRelatedSource(list.getFinancialInformation().getDebt().getRelatedSource());
            person.setConsolidatedDebtCalculated(debt);

            if (list.getFinancialInformation().getIncomes() != null) {

              Optional<CalculatedIncome> searchSalary = list.getFinancialInformation().getIncomes()
                  .stream().filter(f -> f.getRelatedSource()
                      .equalsIgnoreCase(properties.getResRelatedSourcPdh()))
                  .findFirst();

              if (searchSalary.isPresent()) {
                PostSalaryIncomeResponse salary = new PostSalaryIncomeResponse();
                CalculatedIncome calculatedIncome = searchSalary.get();
                salary.setAmount(calculatedIncome.getIncomeAmount());
                salary.setCurrencyCode(calculatedIncome.getCurrency().getCode());
                salary.setPeriodTypeCode(calculatedIncome.getPeriodType().getCode());
                salary.setRelatedSource(calculatedIncome.getRelatedSource());
                salary.setAccountId(
                    calculatedIncome.getRelatedSalaryAccount().getRelatedSalaryAccountId());
                person.setSalaryIncome(salary);

              } else {
                PostSalaryIncomeResponse salaryEmpty = new PostSalaryIncomeResponse();
                salaryEmpty.setAmount(BigDecimal.ZERO);
                salaryEmpty.setCurrencyCode(StringUtils.EMPTY);
                salaryEmpty.setPeriodTypeCode(StringUtils.EMPTY);
                salaryEmpty.setRelatedSource(StringUtils.EMPTY);
                salaryEmpty.setAccountId(StringUtils.EMPTY);
                person.setSalaryIncome(salaryEmpty);
              }

              Optional<CalculatedIncome> searchRisk = list.getFinancialInformation().getIncomes()
                  .stream().filter(f -> f.getRelatedSource()
                      .equalsIgnoreCase(properties.getResRelatedSourcePrincipal()))
                  .findFirst();

              if (searchRisk.isPresent()) {
                PostRiskCalculatedIcomeResponse risk = new PostRiskCalculatedIcomeResponse();
                CalculatedIncome calculatedIncome = searchRisk.get();
                risk.setAmount(calculatedIncome.getIncomeAmount());
                risk.setCurrencyCode(calculatedIncome.getCurrency().getCode());
                risk.setPeriodTypeCode(calculatedIncome.getPeriodType().getCode());
                risk.setRelatedSource(calculatedIncome.getRelatedSource());
                person.setRiskCalculatedIncome(risk);
              } else {
                PostRiskCalculatedIcomeResponse riskEmpty = new PostRiskCalculatedIcomeResponse();
                riskEmpty.setAmount(BigDecimal.ZERO);
                riskEmpty.setCurrencyCode(StringUtils.EMPTY);
                riskEmpty.setPeriodTypeCode(StringUtils.EMPTY);
                riskEmpty.setRelatedSource(StringUtils.EMPTY);
                person.setRiskCalculatedIncome(riskEmpty);
              }

              Optional<CalculatedIncome> searchAdditional = list.getFinancialInformation()
                  .getIncomes().stream().filter(f -> f.getRelatedSource()
                      .equalsIgnoreCase(properties.getResRelatedSourceAdicional()))
                  .findFirst();

              if (searchAdditional.isPresent()) {
                PostAdditionalIncomeResponse additional = new PostAdditionalIncomeResponse();
                CalculatedIncome calculatedIncome = searchAdditional.get();
                additional.setAmount(calculatedIncome.getIncomeAmount());
                additional.setCurrencyCode(calculatedIncome.getCurrency().getCode());
                additional.setPeriodTypeCode(calculatedIncome.getPeriodType().getCode());
                additional.setRelatedSource(calculatedIncome.getRelatedSource());
                person.setAdditionalIncome(additional);

              } else {
                PostAdditionalIncomeResponse additionalEmpty = new PostAdditionalIncomeResponse();
                additionalEmpty.setAmount(BigDecimal.ZERO);
                additionalEmpty.setCurrencyCode(StringUtils.EMPTY);
                additionalEmpty.setPeriodTypeCode(StringUtils.EMPTY);
                additionalEmpty.setRelatedSource(StringUtils.EMPTY);
                person.setAdditionalIncome(additionalEmpty);
              }

              Optional<CalculatedIncome> searchCalculated =
                  list.getFinancialInformation().getIncomes().stream()
                      .filter(f -> !f.getRelatedSource()
                          .equalsIgnoreCase(properties.getResRelatedSourcPdh())
                          && !f.getRelatedSource()
                              .equalsIgnoreCase(properties.getResRelatedSourcePrincipal())
                          && !f.getRelatedSource()
                              .equalsIgnoreCase(properties.getResRelatedSourceAdicional()))
                      .max(Comparator.comparing(CalculatedIncome::getIncomeAmount));

              if (searchCalculated.isPresent()) {
                PostCalculatedIcomeResponse calculated = new PostCalculatedIcomeResponse();
                CalculatedIncome calculatedIncome = searchCalculated.get();
                calculated.setAmount(calculatedIncome.getIncomeAmount());
                calculated.setCurrencyCode(calculatedIncome.getCurrency().getCode());
                calculated.setPeriodTypeCode(calculatedIncome.getPeriodType().getCode());
                calculated.setRelatedSource(calculatedIncome.getRelatedSource());
                person.setCalculatedIncome(calculated);
              } else {
                PostCalculatedIcomeResponse calculatedEmpty = new PostCalculatedIcomeResponse();
                calculatedEmpty.setAmount(BigDecimal.ZERO);
                calculatedEmpty.setCurrencyCode(StringUtils.EMPTY);
                calculatedEmpty.setPeriodTypeCode(StringUtils.EMPTY);
                calculatedEmpty.setRelatedSource(StringUtils.EMPTY);
                person.setCalculatedIncome(calculatedEmpty);
              }
            }
            return person;
          }).collect(Collectors.toList());
      uxResponse.setPersons(personResponse);
    }

    if (wrapperResponse.getLoanEvaluationResponse().getLoanOffers() != null) {

      if (wrapperResponse.getLoanEvaluationResponse().getLoanOffers().stream()
          .map(LoanOffers::getInstallmentsPeriod).findFirst().isPresent()) {

        Optional<InstallmentsPeriod> value = wrapperResponse.getLoanEvaluationResponse()
            .getLoanOffers().stream().map(LoanOffers::getInstallmentsPeriod)
            .max(Comparator.comparing(InstallmentsPeriod::getValue));

        if (value.isPresent()) {
          PostLoanOfferResponse loanOffer = new PostLoanOfferResponse();
          loanOffer.setInstallmentsQuantity(value.get().getValue());
          loanOffer.setInstallmentsFrequency(value.get().getUnit());
          uxResponse.setLoanOffer(loanOffer);
        }

      } else {
        PostLoanOfferResponse loanOffer = new PostLoanOfferResponse();
        loanOffer.setInstallmentsQuantity(Constants.CERO_INT);
        loanOffer.setInstallmentsFrequency(StringUtils.EMPTY);
        uxResponse.setLoanOffer(loanOffer);
      }
    } else {
      PostLoanOfferResponse loanOffer = new PostLoanOfferResponse();
      loanOffer.setInstallmentsQuantity(Constants.CERO_INT);
      loanOffer.setInstallmentsFrequency(StringUtils.EMPTY);
      uxResponse.setLoanOffer(loanOffer);
    }

    if (wrapperResponse.getLoanEvaluationResponse().getEvaluations() != null) {

      List<PostEvaluationsResponse> evaluationResponse =
          wrapperResponse.getLoanEvaluationResponse().getEvaluations().stream().map(list -> {
            PostEvaluationsResponse evaluation = new PostEvaluationsResponse();
            evaluation.setModuleCode(list.getModule().getCode());
            evaluation.setModuleDescription(list.getModule().getDescription());
            evaluation.setModuleClassification(properties.getClassificationCrediticia());
            evaluation.setStatusCode(list.getStatus().getCode());
            evaluation.setStatusDescription(list.getStatus().getDescription());

            List<PostEvaluationRulesResponse> evaluationRulesResponse =
                list.getEvaluationRules().stream().map(li -> {
                  PostEvaluationRulesResponse evaluationRules = new PostEvaluationRulesResponse();
                  evaluationRules.setCode(li.getCode());
                  evaluationRules.setDescription(li.getDescription());
                  evaluationRules.setContextCode(li.getContext().getCode());
                  evaluationRules.setStatusCode(li.getStatus().getCode());
                  evaluationRules.setStatusDescription(li.getStatus().getDescription());
                  List<PostReasonsResponse> reasonsResponse = li.getReasons().stream().map(l -> {
                    PostReasonsResponse reason = new PostReasonsResponse();
                    reason.setDescription(l.getDescription());
                    return reason;
                  }).collect(Collectors.toList());
                  evaluationRules.setReasons(reasonsResponse);
                  return evaluationRules;

                }).collect(Collectors.toList());

            evaluation.setEvaluationRules(evaluationRulesResponse);
            return evaluation;
          }).collect(Collectors.toList());

      uxResponse.setEvaluations(evaluationResponse);
    }

  }



  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToTasaComercialPricing(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {

    PostAdditionalEvaluationsResponse tasa = new PostAdditionalEvaluationsResponse();
    tasa.setStatusCode(code);
    tasa.setModuleCode(properties.getTasa());
    tasa.setModuleDescription(properties.getTasaDescription());
    tasa.setModuleClassification(properties.getTipoComercial());

    List<PostModuleResultsResponse> tasaList = wrapperResponse.getPostResponseList().getTasa()
        .getModule().getException().getEvaluationResults().stream().map(list -> {

          PostModuleResultsResponse result = new PostModuleResultsResponse();
          if (list.getStatus() != null) {
            result.setAutonomyLevel(
                Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
          }
          List<PostFieldslResponse> addTasaList = list.getAdditionalFields().stream().map(li -> {
            PostFieldslResponse addTasa = new PostFieldslResponse();
            addTasa.setName(li.getName());
            addTasa.setValue(li.getValue());
            return addTasa;
          }).collect(Collectors.toList());

          result.setFields(addTasaList);
          return result;
        }).collect(Collectors.toList());

    tasa.setModuleResults(tasaList);
    evaluationInformation.add(tasa);
  }

  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToTasaAutorizadaComercialPricing(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {

    PostAdditionalEvaluationsResponse tasaAutorizada = new PostAdditionalEvaluationsResponse();
    tasaAutorizada.setStatusCode(code);
    tasaAutorizada.setModuleCode(properties.getTasaAutorizada());
    tasaAutorizada.setModuleDescription(properties.getTasaAutorizadaDescription());
    tasaAutorizada.setModuleClassification(properties.getTipoPricing());

    List<PostModuleResultsResponse> tasaAurotizadaList =
        wrapperResponse.getPostResponseList().getTasaAutorizada().getModule().getException()
            .getEvaluationResults().stream().map(list -> {
              PostModuleResultsResponse result = new PostModuleResultsResponse();
              if (list.getStatus() != null) {
                result.setAutonomyLevel(
                    Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
              }
              List<PostFieldslResponse> addTasaAutorizadaList =
                  list.getAdditionalFields().stream().map(li -> {
                    PostFieldslResponse addTasaAutorizada = new PostFieldslResponse();
                    addTasaAutorizada.setName(li.getName());
                    addTasaAutorizada.setValue(li.getValue());
                    return addTasaAutorizada;
                  }).collect(Collectors.toList());

              result.setFields(addTasaAutorizadaList);
              return result;
            }).collect(Collectors.toList());

    tasaAutorizada.setModuleResults(tasaAurotizadaList);
    evaluationInformation.add(tasaAutorizada);
  }



  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToDocumentalBp(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {
    PostAdditionalEvaluationsResponse boletaPago = new PostAdditionalEvaluationsResponse();
    boletaPago.setStatusCode(code);
    boletaPago.setModuleCode(properties.getBoletasPago());
    boletaPago.setModuleDescription(properties.getBoletasPagoDescription());
    boletaPago.setModuleClassification(properties.getTipoDocumental());

    List<PostModuleResultsResponse> boletaPagoList = wrapperResponse.getPostResponseList()
        .getBoletaPago().getModule().getException().getEvaluationResults().stream().map(list -> {
          PostModuleResultsResponse result = new PostModuleResultsResponse();
          if (list.getStatus() != null) {
            result.setAutonomyLevel(
                Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
          }
          List<PostFieldslResponse> addBoletaPagoList =
              list.getAdditionalFields().stream().map(li -> {
                PostFieldslResponse addBoleta = new PostFieldslResponse();
                addBoleta.setName(li.getName());
                addBoleta.setValue(li.getValue());
                return addBoleta;
              }).collect(Collectors.toList());

          result.setFields(addBoletaPagoList);
          return result;
        }).collect(Collectors.toList());

    boletaPago.setModuleResults(boletaPagoList);
    evaluationInformation.add(boletaPago);
  }

  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToDocumentalRh(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {
    PostAdditionalEvaluationsResponse reciboHonorarios = new PostAdditionalEvaluationsResponse();
    reciboHonorarios.setStatusCode(code);
    reciboHonorarios.setModuleCode(properties.getReciboHonorarios());
    reciboHonorarios.setModuleDescription(properties.getReciboHonorariosDescription());
    reciboHonorarios.setModuleClassification(properties.getTipoDocumental());

    List<PostModuleResultsResponse> reciboHonorariosList =
        wrapperResponse.getPostResponseList().getReciboHonorarios().getModule().getException()
            .getEvaluationResults().stream().map(list -> {
              PostModuleResultsResponse result = new PostModuleResultsResponse();
              if (list.getStatus() != null) {
                result.setAutonomyLevel(
                    Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
              }

              List<PostFieldslResponse> addReciboHonorarioList =
                  list.getAdditionalFields().stream().map(li -> {
                    PostFieldslResponse addReciboHonorario = new PostFieldslResponse();
                    addReciboHonorario.setName(li.getName());
                    addReciboHonorario.setValue(li.getValue());
                    return addReciboHonorario;
                  }).collect(Collectors.toList());

              result.setFields(addReciboHonorarioList);
              return result;

            }).collect(Collectors.toList());

    reciboHonorarios.setModuleResults(reciboHonorariosList);
    evaluationInformation.add(reciboHonorarios);
  }

  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToDocumentalIs(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {

    PostAdditionalEvaluationsResponse ingresosSustentados = new PostAdditionalEvaluationsResponse();
    ingresosSustentados.setStatusCode(code);
    ingresosSustentados.setModuleCode(properties.getIngresosSustentados());
    ingresosSustentados.setModuleDescription(properties.getIngresosSustentadosDescription());
    ingresosSustentados.setModuleClassification(properties.getTipoDocumental());

    List<PostModuleResultsResponse> ingresosSustentadosList =
        wrapperResponse.getPostResponseList().getIngresoSustentos().getModule().getException()
            .getEvaluationResults().stream().map(list -> {
              PostModuleResultsResponse result = new PostModuleResultsResponse();
              if (list.getStatus() != null) {
                result.setAutonomyLevel(
                    Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
              }
              List<PostFieldslResponse> addIngresosList =
                  list.getAdditionalFields().stream().map(li -> {
                    PostFieldslResponse addIngresos = new PostFieldslResponse();
                    addIngresos.setName(li.getName());
                    addIngresos.setValue(li.getValue());
                    return addIngresos;
                  }).collect(Collectors.toList());

              result.setFields(addIngresosList);
              return result;

            }).collect(Collectors.toList());

    ingresosSustentados.setModuleResults(ingresosSustentadosList);
    evaluationInformation.add(ingresosSustentados);
  }


  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToCrediticiaAn(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {

    PostAdditionalEvaluationsResponse archivoNegativo = new PostAdditionalEvaluationsResponse();
    archivoNegativo.setStatusCode(code);
    archivoNegativo.setModuleCode(properties.getArchivoNegativo());
    archivoNegativo.setModuleDescription(properties.getArchivoNegativoDescription());
    archivoNegativo.setModuleClassification(properties.getTipoCrediticia());

    List<PostModuleResultsResponse> archivoNegativoList =
        wrapperResponse.getPostResponseList().getArchivoNegativo().getModule().getException()
            .getEvaluationResults().stream().map(list -> {
              PostModuleResultsResponse result = new PostModuleResultsResponse();
              if (list.getStatus() != null) {
                result.setAutonomyLevel(
                    Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
              }
              List<PostFieldslResponse> addArchivoList =
                  list.getAdditionalFields().stream().map(li -> {
                    PostFieldslResponse addArchivo = new PostFieldslResponse();
                    addArchivo.setName(li.getName());
                    addArchivo.setValue(li.getValue());
                    return addArchivo;
                  }).collect(Collectors.toList());

              result.setFields(addArchivoList);
              return result;

            }).collect(Collectors.toList());

    archivoNegativo.setModuleResults(archivoNegativoList);
    evaluationInformation.add(archivoNegativo);
  }

  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapperResponse {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void convertToCrediticiaSbs(Wrapper wrapperResponse,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {

    PostAdditionalEvaluationsResponse sbs = new PostAdditionalEvaluationsResponse();
    sbs.setStatusCode(code);
    sbs.setModuleCode(properties.getClasificacionSbs());
    sbs.setModuleDescription(properties.getSbsDescription());
    sbs.setModuleClassification(properties.getTipoCrediticia());

    List<PostModuleResultsResponse> sbsList = wrapperResponse.getPostResponseList().getSbs()
        .getModule().getException().getEvaluationResults().stream().map(list -> {
          PostModuleResultsResponse result = new PostModuleResultsResponse();
          if (list.getStatus() != null) {
            result.setAutonomyLevel(
                Optional.ofNullable(list.getStatus().getCode()).orElse(StringUtils.EMPTY));
          }

          List<PostFieldslResponse> addSbsList = list.getAdditionalFields().stream().map(li -> {
            PostFieldslResponse addSbs = new PostFieldslResponse();
            addSbs.setName(li.getName());
            addSbs.setValue(li.getValue());
            return addSbs;
          }).collect(Collectors.toList());
          result.setFields(addSbsList);
          return result;

        }).collect(Collectors.toList());

    sbs.setModuleResults(sbsList);
    evaluationInformation.add(sbs);
  }

  /**
   * This method is used to parse the cross service response.
   *
   * @param wrapper {@Link Wrapper}
   * @param evaluationInformation {@Link List}
   */
  public void modifyCem(Wrapper wrapper,
      List<PostAdditionalEvaluationsResponse> evaluationInformation, String code) {

    PostAdditionalEvaluationsResponse cde = new PostAdditionalEvaluationsResponse();
    cde.setStatusCode(code);
    cde.setModuleCode(properties.getCapacidadPago());
    cde.setModuleDescription(properties.getCapacidadPagoDescription());
    cde.setModuleClassification(properties.getTipoCrediticia());
    List<PostModuleResultsResponse> resultList = new ArrayList<>();

    String value = Optional
        .ofNullable(loanEvaluationUtil.captureSegmentTitular(wrapper.getLoanEvaluationResponse()))
        .orElse(Constants.SEGMENT_E);

    Optional<List<PostResultFieldResponse>> additionals =
        wrapper.getPostResponseList().getCem().getModule().getException().getEvaluationResults()
            .stream().map(PostEvaluationResultResponse::getAdditionalFields).findAny();

    if (Constants.SEGMENT_A.equalsIgnoreCase(value)
        || Constants.SEGMENT_B.equalsIgnoreCase(value)) {

      if (additionals.isPresent()) {
        List<PostResultFieldResponse> fields = additionals.get();

        Optional<PostResultFieldResponse> cero = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCeroAbSegmentName()))
            .findFirst();

        Optional<PostResultFieldResponse> cem01 = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCeroAbSegmentCem()))
            .findFirst();

        PostModuleResultsResponse res01 = new PostModuleResultsResponse();
        if (cero.isPresent()) {
          res01.setAutonomyLevel(cero.get().getValue());
        }
        PostFieldslResponse add01 = new PostFieldslResponse();
        add01.setName(properties.getCalculadoDos());
        if (cem01.isPresent()) {
          BigDecimal calculateAutonomyCero =
              loanEvaluationUtil.calculateCem(wrapper, cem01.get().getValue());
          add01.setValue(String.valueOf(calculateAutonomyCero));
        }

        List<PostFieldslResponse> additionals01 = new ArrayList<>();
        additionals01.add(add01);
        res01.setFields(additionals01);

        Optional<PostResultFieldResponse> cuatro = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCuatroAbSegmentName()))
            .findFirst();

        Optional<PostResultFieldResponse> cem02 = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCuatroAbSegmentCem()))
            .findFirst();

        PostModuleResultsResponse res02 = new PostModuleResultsResponse();
        if (cuatro.isPresent()) {
          res02.setAutonomyLevel(cuatro.get().getValue());
        }
        PostFieldslResponse add02 = new PostFieldslResponse();
        add02.setName(properties.getCalculadoCinco());
        if (cem02.isPresent()) {
          BigDecimal calculateAutonomyCuatro =
              loanEvaluationUtil.calculateCem(wrapper, cem02.get().getValue());
          add02.setValue(String.valueOf(calculateAutonomyCuatro));

        }
        List<PostFieldslResponse> additionals02 = new ArrayList<>();
        additionals02.add(add02);
        res02.setFields(additionals02);
        resultList.add(res01);
        resultList.add(res02);
        cde.setModuleResults(resultList);

      }

    } else if (Constants.SEGMENT_C.equalsIgnoreCase(value)) {
      if (additionals.isPresent()) {
        List<PostResultFieldResponse> fields = additionals.get();

        Optional<PostResultFieldResponse> cuatro = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCuatroCeSegmentName()))
            .findFirst();

        Optional<PostResultFieldResponse> cem01 = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCuatroCeSegmentCem()))
            .findFirst();

        PostModuleResultsResponse res03 = new PostModuleResultsResponse();
        if (cuatro.isPresent()) {
          res03.setAutonomyLevel(cuatro.get().getValue());
        }
        PostFieldslResponse add03 = new PostFieldslResponse();
        add03.setName(properties.getCalculadoCinco());
        if (cem01.isPresent()) {
          BigDecimal calculateAutonomyCuatro =
              loanEvaluationUtil.calculateCem(wrapper, cem01.get().getValue());
          add03.setValue(String.valueOf(calculateAutonomyCuatro));

        }

        List<PostFieldslResponse> additionals03 = new ArrayList<>();
        additionals03.add(add03);
        res03.setFields(additionals03);
        resultList.add(res03);
        cde.setModuleResults(resultList);
      }

    } else if (Constants.SEGMENT_D.equalsIgnoreCase(value)
        || Constants.SEGMENT_E.equalsIgnoreCase(value)) {
      if (additionals.isPresent()) {
        List<PostResultFieldResponse> fields = additionals.get();

        Optional<PostResultFieldResponse> autonomyCinco = fields.stream()
            .filter(f -> f.getName().equalsIgnoreCase(properties.getAutonomyCincoDeSegmentName()))
            .findFirst();
        PostModuleResultsResponse res04 = new PostModuleResultsResponse();
        if (autonomyCinco.isPresent()) {
          res04.setAutonomyLevel(autonomyCinco.get().getValue());
        }
        List<PostFieldslResponse> additionals04 = new ArrayList<>();
        res04.setFields(additionals04);
        resultList.add(res04);
        cde.setModuleResults(resultList);
      }
    } else {
      evaluationInformation.add(new PostAdditionalEvaluationsResponse());
    }
    evaluationInformation.add(cde);
  }
}
