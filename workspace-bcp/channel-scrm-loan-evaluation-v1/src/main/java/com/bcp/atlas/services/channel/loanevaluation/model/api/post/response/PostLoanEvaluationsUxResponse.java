package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Class that represents a model of data presentation that will be entered by the api<br/>
 * <b>Class</b>: PostLoanEvaluationsUxResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostLoanEvaluationsUxResponse {

  @ApiModelProperty(name = "loanEvaluationId",
      value = "Identificador único (Request-ID Header de la entrada - Identificador unico)",
      example = "550e8400-e29b-41d4-a716-100000001072_S9", position = 1)
  private String loanEvaluationId;

  @ApiModelProperty(name = "evaluationStatusCode",
      value = "Codigo del resultado de la evaluacion crediticia.", example = "I", position = 2)
  private String evaluationStatusCode;

  @ApiModelProperty(name = "evaluationStatusDescription",
      value = "Descripcion del resultado de la evaluacion crediticia.", example = "INVESTIGATE",
      position = 3)
  private String evaluationStatusDescription;

  @ApiModelProperty(name = "consolidatedFinancialInformation", value = "", dataType = "Object",
      example = "", position = 13)
  private PostConsolidatedFinancialInformationResponse consolidatedFinancialInformation;

  @ApiModelProperty(name = "persons", value = "", dataType = "List", example = "", position = 16)
  private List<PostPersonsResponse> persons;

  @ApiModelProperty(name = "loanOffer", value = "", dataType = "Object", example = "",
      position = 38)
  private PostLoanOfferResponse loanOffer;

  @ApiModelProperty(name = "evaluations", value = "", dataType = "List", example = "",
      position = 41)
  private List<PostEvaluationsResponse> evaluations;

  @ApiModelProperty(name = "additionalEvaluations", value = "", dataType = "List", example = "",
      position = 54)
  private List<PostAdditionalEvaluationsResponse> additionalEvaluations;
}
