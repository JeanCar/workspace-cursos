package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import com.bcp.atlas.services.channel.loanevaluation.business.EvaluateExceptionOrderService;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetExceptionOrderQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.header.LoanEvaluationUxHeader;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.util.ExceptionUtils;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.operator.CircuitBreakerOperator;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


/**
 * <br/>
 * Class service that contains the necessary methods to process the data and business logic that
 * will consume the REST class LoanEvaluationController<br/>
 * <b>Class</b>: LoanExceptionOrderImpl<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Service
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class EvaluateExceptionOrderImpl implements EvaluateExceptionOrderService {

  @Autowired
  private EvaluateExceptionOrderBuilder evaluateExceptionOrderBuilder;

  @Autowired
  private EvaluateExceptionOrderProccesor evaluateExceptionOrderProccesor;

  @Autowired
  private EvaluateExceptionOrderSender evaluateExceptionOrderSender;

  @Autowired
  @Qualifier("atlas.default")
  private CircuitBreaker circuitBreaker;

  @Override
  public Maybe<GetExceptionOrderUxResponse> loanEvaluationsEvaluateGet(
      LoanEvaluationUxHeader headers, GetExceptionOrderQueryParam request) {
    return evaluateExceptionOrderSender.callLoanEvaluationGet(request.getExceptionId())
        .lift(CircuitBreakerOperator.of(circuitBreaker))
        .map(evaluateExceptionOrderProccesor::convertResponseGetToUx)
        .subscribeOn(Schedulers.computation())
        .onErrorResumeNext(ExceptionUtils::onLoanExceptionOrderGetError)
        .doOnSuccess(succ -> log.info("Successful"))
        .doOnError(err -> log.error("Error al consultar loanEvaluation", err));
  }

  @Override
  public Single<PostExceptionOrderUxResponse> loanEvaluationsEvaluatePost(
      LoanEvaluationUxHeader headers, PostExceptionOrderUxRequest request) {
    return evaluateExceptionOrderSender
        .callLoanEvaluationPost(evaluateExceptionOrderBuilder.buildPostRequest(request))
        .lift(CircuitBreakerOperator.of(circuitBreaker))
        .map(evaluateExceptionOrderProccesor::convertResponsePostToUx)
        .subscribeOn(Schedulers.computation())
        .onErrorResumeNext(ExceptionUtils::onLoanExceptionOrderPostError)
        .doOnSuccess(succ -> log.info("Successful"))
        .doOnError(err -> log.error("Error al consultar loanEvaluation", err));
  }
}
