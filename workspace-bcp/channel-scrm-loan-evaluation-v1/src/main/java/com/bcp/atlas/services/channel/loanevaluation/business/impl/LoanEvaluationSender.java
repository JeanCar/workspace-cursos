package com.bcp.atlas.services.channel.loanevaluation.business.impl;


import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetEvaluationQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.EvaluationsApi;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.LoanEvaluationApi;
import com.bcp.atlas.services.channel.util.ExceptionUtils;
import io.reactivex.Single;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <br/>
 * <b>Class</b>: LoanEvaluationSender<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 01, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Component
@Slf4j
@AllArgsConstructor
public class LoanEvaluationSender {

  @Autowired
  private LoanEvaluationApi loanEvaluationApi;

  @Autowired
  private EvaluationsApi evaluationApi;

  /**
   * Credit evaluation Client method.
   *
   * @param request {@link LoanEvaluationRequest}
   * @return {@link Single}
   */

  public Single<LoanEvaluationResponse> callLoanEvaluation(LoanEvaluationRequest request) {
    return loanEvaluationApi.creditEvaluationUsingPOST3(request)
        .onErrorResumeNext(ExceptionUtils::onLoanEvaluationError)
        .doOnSuccess(item -> log.info("Successful"));
  }


  /**
   * Credit evaluation Client method.
   *
   * @param request {@link GetEvaluationQueryParam}
   * @return {@link Single}
   */

  public Single<List<EvaluationElement>> callEvaluation(GetEvaluationQueryParam request) {
    return evaluationApi.getEvaluationsUsingGET3(request.getPersonId(), request.getIsActive());
  }
}
