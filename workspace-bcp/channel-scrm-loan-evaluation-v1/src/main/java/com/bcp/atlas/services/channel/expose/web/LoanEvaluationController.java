package com.bcp.atlas.services.channel.expose.web;

import static com.bcp.atlas.core.constants.ErrorCategory.INVALID_REQUEST;
import static com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants.COMPONENT_CHANNEL;

import com.bcp.atlas.core.annotations.HttpHeadersMapping;
import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.core.exception.AtlasExceptionBuilder;
import com.bcp.atlas.core.starter.web.annotations.QueryParams;
import com.bcp.atlas.services.channel.loanevaluation.business.EvaluateExceptionOrderService;
import com.bcp.atlas.services.channel.loanevaluation.business.LoanEvaluationService;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetExceptionOrderQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.header.LoanEvaluationUxHeader;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main controller that exposes the service through HTTP / Rest for the resources and sub
 * resources<br/>
 * <b>Class</b>: LoanEvaluationController<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@SuppressFBWarnings(value = "SPRING_ENDPOINT",
    justification = "Methods in this class are just endpoints")
@RestController
@Api(tags = "Loan Evaluation v1")
@RequestMapping("/channel/scrm/loan-evaluation/v1")
public class LoanEvaluationController {

  @Autowired
  private LoanEvaluationService loanEvaluationService;

  @Autowired
  private EvaluateExceptionOrderService loanExceptionOrderService;

  /**
   * method perform evaluations risk.
   * 
   * @param headers object
   * @param request object
   * @param querys map
   * @return {@link Single}
   */
  @ApiOperation(
      value = "El servicio realiza la evaluacion de credito efectivo regular de "
          + "una persona natural",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class,
      httpMethod = "POST", notes = "classpath:swagger/clients/notes/loan-evaluation.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecuto sastifactoriamente.",
          response = PostLoanEvaluationsUxResponse.class),
      @ApiResponse(code = 404, message = "No se encontro el recurso solicitado.",
          response = AtlasException.class),
      @ApiResponse(code = 400, message = "El cliente envio datos incorrectos.",
          response = AtlasException.class),
      @ApiResponse(code = 500, message = "Error al obtener listado de creditos.",
          response = AtlasException.class),
      @ApiResponse(code = 503, message = "El servicio no se encuentra disponible.",
          response = AtlasException.class)})
  @PostMapping(value = "/loan-evaluations", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  @ResponseStatus(HttpStatus.OK)
  public Single<PostLoanEvaluationsUxResponse> loanEvaluations(
      @HttpHeadersMapping LoanEvaluationUxHeader headers,
      @RequestBody PostLoanEvaluationsUxRequest request,
      @RequestParam @ApiParam(hidden = true) Map<String, String> querys) {
    if (!querys.isEmpty()) {
      AtlasExceptionBuilder builder = AtlasException.builder();
      builder.description(INVALID_REQUEST.getDescription());
      builder.category(INVALID_REQUEST);
      builder.addDetail(true)
          .withCode(INVALID_REQUEST.getCode())
          .withDescription("Solicitud incorrecta.")
          .withComponent(COMPONENT_CHANNEL).push();
      return builder.buildAsSingle();
    }

    return loanEvaluationService.loanEvaluations(headers, request);
  }

  /**
   * method get variables for exceptions.
   * 
   * @param headers object
   * @param param object
   * @return {@link Maybe}
   */
  @ApiOperation(
      value = "Consulta los datos requeridos para la solicitud de excepcion de una evaluacion",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET",
      notes = "classpath:swagger/clients/notes/loan-exception-order.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecuto sastifactoriamente.",
          response = GetExceptionOrderUxResponse.class),
      @ApiResponse(code = 404, message = "No se encontro el recurso solicitado.",
          response = AtlasException.class),
      @ApiResponse(code = 400, message = "El cliente envio datos incorrectos.",
          response = AtlasException.class),
      @ApiResponse(code = 500, message = "Error al obtener listado de creditos.",
          response = AtlasException.class),
      @ApiResponse(code = 503, message = "El servicio no se encuentra disponible.",
          response = AtlasException.class)})
  @GetMapping(value = "/loan-evaluations/evaluate",
      produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  @ResponseStatus(HttpStatus.OK)
  public Maybe<GetExceptionOrderUxResponse> loanEvaluationsEvaluateGet(
      @HttpHeadersMapping LoanEvaluationUxHeader headers,
      @QueryParams GetExceptionOrderQueryParam param) {
    return loanExceptionOrderService.loanEvaluationsEvaluateGet(headers, param);
  }

  /**
   * method perform evaluation exception and autonomy.
   * 
   * @param headers Object
   * @param request Object
   * @param querys Map
   * @return {@link Single}
   */
  @ApiOperation(value = "Evalua la excepcion y/o valores de una evaluacion",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class,
      httpMethod = "POST", notes = "classpath:swagger/clients/notes/loan-exception-order.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecuto sastifactoriamente.",
          response = PostExceptionOrderUxResponse.class),
      @ApiResponse(code = 404, message = "No se encontro el recurso solicitado.",
          response = AtlasException.class),
      @ApiResponse(code = 400, message = "El cliente envio datos incorrectos.",
          response = AtlasException.class),
      @ApiResponse(code = 500, message = "Error al obtener listado de creditos.",
          response = AtlasException.class),
      @ApiResponse(code = 503, message = "El servicio no se encuentra disponible.",
          response = AtlasException.class)})
  @PostMapping(value = "/loan-evaluations/evaluate",
      produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  @ResponseStatus(HttpStatus.OK)
  public Single<PostExceptionOrderUxResponse> loanEvaluationsEvaluatePost(
      @HttpHeadersMapping LoanEvaluationUxHeader headers,
      @RequestBody PostExceptionOrderUxRequest request,
      @RequestParam @ApiParam(hidden = true) Map<String, String> querys) {
    if (!querys.isEmpty()) {
      AtlasExceptionBuilder builder = AtlasException.builder();
      builder.description(INVALID_REQUEST.getDescription());
      builder.category(INVALID_REQUEST);
      builder.addDetail(true)
          .withCode(INVALID_REQUEST.getCode())
          .withDescription("Solicitud incorrecta.")
          .withComponent(COMPONENT_CHANNEL).push();
      return builder.buildAsSingle();
    }
    
    return loanExceptionOrderService.loanEvaluationsEvaluatePost(headers, request);
  }

}
