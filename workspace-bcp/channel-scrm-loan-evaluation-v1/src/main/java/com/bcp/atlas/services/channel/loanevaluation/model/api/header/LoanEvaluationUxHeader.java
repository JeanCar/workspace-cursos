package com.bcp.atlas.services.channel.loanevaluation.model.api.header;

import com.bcp.atlas.core.model.SoftHttpHeadersRequest;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.loanevaluation.validation.InjectionSafe;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * Class that represents a presentation model of the header that will be sent by the api<br/>
 * LoanEvaluationController<br/>
 * <b>Class</b>: LoanEvaluationUxHeader<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */


@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanEvaluationUxHeader extends SoftHttpHeadersRequest {

  @Size(max = 1000)
  @InjectionSafe
  private String accept;

  @Size(max = 100)
  @InjectionSafe
  @JsonProperty("Content-Type")
  private String contentType;

  @Override
  @Pattern(regexp = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-"
      + "[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$")
  @ApiParam(value = "Este campo es un valor estandar ya existente y sera usado como identificador.",
      name = "Request-ID", required = true, example = Constants.REQUEST_ID)
  @Size(min = 36)
  @NotNull
  @NotEmpty
  public String getRequestId() {
    return super.getRequestId();
  }

  @Override
  @ApiParam(value = "Fecha de la peticion", required = true, name = "request-date",
      example = Constants.REQUEST_DATE)
  @NotNull
  @NotEmpty
  @Size(max = 30)
  public String getRequestDate() {
    return super.getRequestDate();
  }

  @Override
  @ApiParam(value = "ID del componente o API que realiza la peticion.", name = "caller-name",
      required = true, example = Constants.CALLER_NAME)
  @NotNull
  @NotEmpty
  @Size(min = 5, max = 100)
  public String getCallerName() {
    return super.getCallerName();
  }

  @Override
  @ApiParam(
      value = "Codigo de la aplicacion  que invoca el servicio. Se debe usar el codigo de"
          + " 2 caracteres que tienen asignada las aplicaciones, puede ser el canal.",
      name = "app-code", required = true, example = Constants.APP_CODE)
  @NotNull
  @NotEmpty
  @Size(min = 2, max = 2)
  public String getApplicationCode() {
    return super.getApplicationCode();
  }
  
}
