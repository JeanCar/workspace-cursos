package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PostEvaluationsResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostEvaluationsResponse {

  @ApiModelProperty(name = "moduleCode", value = "decisionName - Nombre del modulo ",
      example = "ARCHIVO-SBS", position = 41)
  private String moduleCode;

  @ApiModelProperty(name = "moduleDescription", value = "", example = "ARCHIVO-SBS", position = 42)
  private String moduleDescription;

  @ApiModelProperty(name = "moduleClassification", value = "", example = "CREDITICIA",
      position = 43)
  private String moduleClassification;

  @ApiModelProperty(name = "statusCode",
      value = "decisionResultCode - Codigo de Resultado de la decision" + " del modulo ",
      example = "D", position = 44)
  private String statusCode;

  @ApiModelProperty(name = "statusDescription",
      value = "decisionResult - Descripcion de Resultado de la decision " + "del modulo ",
      dataType = "String", example = "DECLINADO", position = 45)
  private String statusDescription;

  @ApiModelProperty(name = "evaluationRules", value = "", dataType = "List", example = "",
      position = 46)
  private List<PostEvaluationRulesResponse> evaluationRules;

}
