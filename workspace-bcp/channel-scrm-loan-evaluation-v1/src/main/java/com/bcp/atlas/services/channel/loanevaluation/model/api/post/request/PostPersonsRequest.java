package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br>
 * <b>Class</b>: PostEvaluatedPersonsRequest<br/>
 * <b>Copyright</b>: &copy; 2017 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 02, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class PostPersonsRequest {

  @ApiModelProperty(name = "personId",
      value = "Numero y Tipo de Documento de Identidad de la Persona", example = "000000021000",
      required = true, position = 10)
  @Size(min = 5, max = 15)
  @NotNull
  @NotEmpty
  @Pattern(regexp = Constants.PERSON_ID)
  private String personId;

  @ApiModelProperty(name = "estimatedIncomeRequired",
      value = "false: Usar ingreso digitado   true: Usar ingreso estimado", example = "true",
      required = true, position = 11)
  @NotNull
  private Boolean estimatedIncomeRequired;

  @ApiModelProperty(name = "relationTypeCode", value = "Codigo de Relacion de la Persona",
      example = "T", required = true, position = 13)
  @NotNull
  @NotEmpty
  @Pattern(regexp = Constants.PATTERN_RELATION_TYPECODE)
  @Size(min = 1, max = 1)
  private String relationTypeCode;

  @ApiModelProperty(name = "birthDate", value = "Fecha de Nacimiento de la Persona",
      example = "1986-09-19", required = false, position = 14)
  @NotNull
  @NotEmpty
  @Pattern(regexp = Constants.PATTERN_BIRTHDATE)
  @Size(max = 10)
  private String birthDate;

  @ApiModelProperty(name = "maritalStatusCode", value = "Codigo del Estado Civil de la Persona",
      example = "SOL", required = true, position = 15)
  @NotNull
  @NotEmpty
  @Size(min = 3, max = 3)
  private String maritalStatusCode;

  @ApiModelProperty(name = "address", value = "", dataType = "Object", example = "",
      required = true, position = 16)
  @Valid
  private PostAddressRequest address;

  @ApiModelProperty(name = "monthlyIncomes", value = "Datos los ingresos de la persona",
      dataType = "List", example = "", required = true, position = 21)
  @Valid
  private List<PostMonthlyIncomesRequest> monthlyIncomes;

}
