package com.bcp.atlas.services.channel.loanevaluation.business;

import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetExceptionOrderQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.header.LoanEvaluationUxHeader;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import io.reactivex.Maybe;
import io.reactivex.Single;


/**
 * <br/>
 * Clase Interfaz del Servicio para la logica de negocio que consumira la clase REST
 * LoanExceptionOrderService<br/>
 * <b>Class</b>: LoanExceptionOrderService<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
public interface EvaluateExceptionOrderService {


  Maybe<GetExceptionOrderUxResponse> loanEvaluationsEvaluateGet(
      LoanEvaluationUxHeader headers, GetExceptionOrderQueryParam request);

  Single<PostExceptionOrderUxResponse> loanEvaluationsEvaluatePost(
      LoanEvaluationUxHeader headers, PostExceptionOrderUxRequest request);
}
