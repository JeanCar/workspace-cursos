package com.bcp.atlas.services.channel.config;

import com.bcp.atlas.core.constants.HttpHeadersKey;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.GenericUtil;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;



/**
 * Interceptor for headers.<br/>
 * <b>Class</b>: RequestCrossInterceptor<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Component
@Slf4j
public class CrossArchivoNegativoInterceptor implements Interceptor {

  @Autowired
  private CustomExceptionHeaders header;

  @Autowired
  private ApplicationProperties properties;

  @Override
  public Response intercept(Chain chain) throws IOException {
    log.debug("Ingresando a CrossCdaInterceptor.");
    Request req = getNewRequest(chain.request());
    return chain.proceed(req);
  }

  private Request getNewRequest(Request originalRequest) {
    Builder newRequest = originalRequest.newBuilder().headers(getHeaders(originalRequest));
    return newRequest.build();
  }

  private Headers getHeaders(Request originalRequest) {
    okhttp3.Headers.Builder builder = originalRequest.headers().newBuilder();
    builder.add(HttpHeadersKey.OPERATION_CODE, Constants.OPN_CODE_ARCHIVO_NEGATIVO);
    builder.add(HttpHeadersKey.OPERATION_NUMBER, GenericUtil.generateOperationNumber());
    builder.add(HttpHeadersKey.USER_CODE,
        Constants.AP + originalRequest.header("app-code") + properties.getEnv());
    header.getHeader().forEach((key, value) -> {
      if (StringUtils.isEmpty(builder.get(key))) {
        builder.add(key, value);
      }

    });
    return builder.build();
  }


  /**
   * Variable store headers from cs.<br/>
   * <b>Class</b>: CustomHeaders<br/>
   * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
   * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
   *
   * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
   *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
   *         <u>Developed by</u>: <br/>
   *         <ul>
   *         <li>Wilder Jean Carlos</li>
   *         </ul>
   *         <u>Changes</u>:<br/>
   *         <ul>
   *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
   *         </ul>
   * @version 1.0
   */

  @Getter
  @Setter
  @RefreshScope
  @ConfigurationProperties(prefix = "application.loan")
  @Component
  @SuppressFBWarnings(value = "USFW_UNSYNCHRONIZED_SINGLETON_FIELD_WRITES",
      justification = "This class is used for configuration purposes")
  public static class CustomExceptionHeaders {
    private Map<String, String> header = new HashMap<>();

  }
}
