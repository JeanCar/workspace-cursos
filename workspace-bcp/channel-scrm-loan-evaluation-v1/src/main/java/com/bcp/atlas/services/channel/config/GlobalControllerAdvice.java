package com.bcp.atlas.services.channel.config;

import static com.bcp.atlas.core.constants.ErrorCategory.INVALID_REQUEST;
import static com.bcp.atlas.core.constants.ErrorCategory.UNEXPECTED;

import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.core.exception.impl.FieldValidationException;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ServerWebInputException;

/**
 * <b>Class</b>: GlobalControllerAdvice<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Co mpany</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li> // *
 *         </ul>
 * @version 1.0
 */

@ControllerAdvice
@Slf4j
public class GlobalControllerAdvice {

  
  /**
   * Transform an exception to hide some information.
   *
   * @param e The exception.
   * @return The transformed exception.
   */
  @SuppressFBWarnings(
      value = {
          "ITC_INHERITANCE_TYPE_CHECKING",
          "PRMC_POSSIBLY_REDUNDANT_METHOD_CALLS"
      },
      justification = "instanceof usage is acceptable in this method."
          + "There aren't redundant calls here.")
  @ExceptionHandler(Throwable.class)
  @ResponseBody
  public Single<AtlasException> handleException(Throwable e) {
    log.error("An exception was caught", e);
    if (e instanceof FieldValidationException
        || e instanceof ServerWebInputException
        || e instanceof JSONException) {
      return AtlasException.builder()
          .category(INVALID_REQUEST)
          .addDetail()
          .withDescription(INVALID_REQUEST.getDescription())
          .push()
          .buildAsSingle();
    } else if (e instanceof AtlasException) {
      return ((AtlasException) e).mutate().buildAsSingle();
    }
    return AtlasException.builder()
        .category(UNEXPECTED)
        .addDetail()
        .withDescription(UNEXPECTED.getDescription())
        .push()
        .buildAsSingle();
  }
}
