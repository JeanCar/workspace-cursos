# EvaluationApi

### Acerca de la funcionalidad expuesta
Los metodos expuestos tendra funcionalidades 1) Consulta los datos requeridos para la solicitud de excepción de una evaluación y 2) Evalua la excepción y/o valores de una evaluación. Internamente invoca al siguiente servicio **/servicing/customer-order/v1/evaluation-exception-orders**
<br/>

### URI de acceso a la API
| Método | URI                                                                      |
|--------|------------------------------------------------------------------------- |
|GET     | /channel/scrm/loan-evaluation/v1/loan-evaluations/evaluate?{exceptionId} |
|POST    | /channel/scrm/loan-evaluation/v1/loan-evaluations/evaluate               |

### Precondiciones para el consumo de esta versión de la API
No Aplica.

### Headers Requeridos
| Header              |Ejemplo                                        |
|-------------------- |---------------------------------------------  |
|Request-ID           |112e8400-e29b-41d4-a716-446666699943			  |
|request-date         |2019-06-01T17:15:20.509-0400					  |
|user-code            |APTLDES										  |
|app-code       	  |S9 											  |								 
|caller-name   		  |loan-evaluation-v1							  |
|Content-Type         |application/json								  |

<br/>

### Variantes válidas del Payload (Cuerpo del mesaje)
### Esta funcionalidad permite además manejar distintos tipos de payloads dependiendo de la operación a realizar. A continuación los siguientes casos:

### Caso 1. Consulta los datos requeridos para la solicitud de excepción de una evaluación.
```javascript  
 exceptionId=1
```
## Atributos:
### Lista de Valores usadas para el metodo GET en esta versión de la API
### Request
#### exceptionId | Identificador exception evaluation

### Response
#### exceptionid | Identificador de tabla de decision.
#### description | Descripción de tabla de decision.
#### evaluationFields.name | Nombre de variable de entrada.
#### evaluationFields.type | Tipo de variable de entrada.



### Caso 2. Evalua la excepción y/o valores de una evaluación
```javascript 
{
	"requiredFullEvaluation": true,
	"exception": {
		"exceptionId": 2,
		"evaluationFields": [
			{
				"name": "segmentacion_riesgo",
				"value": "E"
			}
		]
	}
}
```

## Atributos:
### Lista de Valores usadas en esta versión de la API

### Request
#### requiredFullEvaluation | Indica si se evaluaran todas las reglas o no
#### exception.exceptionId | Identificador de tabla de decision.
#### exception.evaluationFields.name | Nombre de la variable de entrada.
#### exception.evaluationFields.value | Valor de la variable de entrada.

### Response
#### exception.evaluationResults.statusCode | Codigo de nivel de autonomia.
#### exception.evaluationResults.additionalFields.name | Nombre de variable adicional.
#### exception.evaluationResults.additionalFields.value | Valor de variable adicional.


### HTTP response headers

 - **Content-Type**: application/json, application/json;charset=UTF-8
 - **Accept**: application/stream+json
 
### Códigos de error usados en esta versión de la API
| Codigo | Descripcion                                                              |
|--------|--------------------------------------------------------------------------|
| TL0003 | Los datos proporcionados no son válidos                                  |
| TL0005 | Ocurrió un error en la comunicación con el BackEnd                       |
| TL0007 | Ocurrio un error en el servicio externo                                  |
| TL9999 | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico   |
| TL0011 | No esta autorizado correctamente para ejecutar esta operacion.           |
