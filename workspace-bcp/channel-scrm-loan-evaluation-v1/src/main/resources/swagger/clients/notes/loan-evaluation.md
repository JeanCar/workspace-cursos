# LoanEvaluationApi

### Acerca de la funcionalidad expuesta
El servicio Realiza la evaluación de crédito efectivo regular de una persona natural. <br>
Internamente invoca al siguiente servicio **acs-customer-management-customer-credit-rating-v1** para obtener la información detallada de la persona y sus relacionados con la evaluación crediticia.

<br/>

### URI de acceso a la API
| Método | URI                                               |
|--------|-------------------------------------------------- |
|POST    | /channel/scrm/loan-evaluation/v1/loan-evaluations |


### Precondiciones para el consumo de esta versión de la API
No Aplica.

### Headers Requeridos
| Header              |Ejemplo                                        |
|-------------------- |---------------------------------------------  |
|Request-ID           |112e8400-e29b-41d4-a716-446666699943			  |
|request-date         |2019-06-01T17:15:20.509-0400					  |
|user-code            |APTLDES										  |
|app-code       	  |S9 											  |	
|supervisor-code      |0S9999    									  |		
|caller-name   		  |loan-evaluation-v1							  |
|Content-Type         |application/json								  |

<br/>

### Variantes válidas del Payload (Cuerpo del mesaje)
### Esta funcionalidad permite además manejar distintos tipos de payloads dependiendo de la operación a realizar. A continuación los siguientes casos:

### Caso 1. Evaluación crediticia de un cliente.
```javascript 
{
"loanOrderId": "00004567",
"registerDate": "2018-02-12T09:09:59.000",
"saleInformation": {
	"saleTypeCode": "1",
	"offer": {
		"saleTypeCode": "011",
		"typeCode": "001",
		"categoryCode": "CATEGORIA_01"
	}
},
"persons": [{
	"personId": "100195246000",
	"estimatedIncomeRequired": true,
	"relationTypeCode": "T",
	"birthDate": "1986-09-19",
	"maritalStatusCode": "SOL",
	"address":{
   "geolocationCode": "020101",
	"regionCode": "02",
	"provinceCode": "01",
	"districtCode": "01"
	
	},
	"monthlyIncomes": [{
		"grossAmount": 1500.5,
		"additionalGrossAmount": 1000.5,
		"currencyCode": "EUR",
		"incomeTypeCode": "01",
		"incomeTaxCaterogyCode": 5,
		"employerId": "20100075009",
		"employerPrimaryEconomicActivityCode": "0162",
		"relationshipStartDate": "2001-02-12T09:09:59.000"
	}]
}],
"evaluations": {
	"debtClassificationBcp": "0",
	"incomeOutsideGuideline": 10.00,
	"totalDigitizedIncome": 3000.00,
	"customerSegment": "93",
	"incomeMark" : false
}
}
```
## Atributos:
### Lista de Valores usadas en esta versión de la API

### Request
#### loanOrderId | Id de la Solicitud
#### registerDate | Fecha y Hora de la Solicitud 
#### saleInformation.typeCode| Tipo de venta del préstamo
#### saleInformation.offer.saleTypeCode | Tipo de  la venta de campaña
#### saleInformation.offer.typeCode | Tipo de oferta
#### saleInformation.offer.categoryCode | Categoria de la oferta
#### persons.personId | Numero y Tipo de Documento de Identidad de la Persona
#### persons.estimatedIncomeRequired | false: Usar ingreso digitado   true: Usar ingreso estimado
#### persons.relationTypeCode| Codigo de Relacion de la Persona
#### persons.birthDate | Fecha de Nacimiento
#### persons.maritalStatusCode | Código del Estado Civil de la Persona
#### persons.address.geolocationCode | Código Postal
#### persons.address.regionCode| Departamento al que pertenece la dirección de la persona
#### persons.address.provinceCode | Provincia al que pertenece la dirección de la persona
#### persons.address.districtCode | Distrito al que pertenece la dirección de la persona
#### persons.monthlyIncomes.grossAmount | Monto de ingresos de la persona
#### persons.monthlyIncomes.additionalGrossAmount | Ingreso bruto adicional
#### persons.monthlyIncomes.currencyCode | Código de moneda de ingreso mensual
#### persons.monthlyIncomes.incomeTypeCode | Codigo del Tipo de Ingreso
#### persons.monthlyIncomes.incomeTaxCaterogyCode | Categoría de impuesto a la renta
#### persons.monthlyIncomes.employerId | RUC de la Empresa empleadora
#### persons.monthlyIncomes.employerPrimaryEconomicActivityCode | Codigo de la actividad Económica de la empresa
#### persons.monthlyIncomes..relationshipStartDate |Fecha de inicio del vinculo laboral. Si no se cuenta la hora establecer el T00:00:00.000
#### evaluations.debtClassificationBcp | clasificación deudora BCP
#### evaluations.incomeOutsideGuideline | ingreso sustentado fuera de pauta desde salesforce
#### evaluations.totalDigitizedIncome | ingreso total digitado desde salesforce
#### evaluations.customerSegment | segmento del cliente desde salesforce
#### evaluations.incomeMark | Marca del sustento de ingreso, cuando el cliente solicita que para el calculo del CEM se incluya

 
### HTTP response headers

 - **Content-Type**: application/json, application/json;charset=UTF-8
 - **Accept**: application/stream+json
 
### Códigos de error usados en esta versión de la API
| Codigo | Descripcion                                                              |
|--------|--------------------------------------------------------------------------|
| TL0003 | Los datos proporcionados no son válidos                                  |
| TL0005 | Ocurrió un error en la comunicación con el BackEnd                       |
| TL0007 | Ocurrio un error en el servicio externo                                  |
| TL9999 | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico   |
| TL0011 | No esta autorizado correctamente para ejecutar esta operacion.           |
