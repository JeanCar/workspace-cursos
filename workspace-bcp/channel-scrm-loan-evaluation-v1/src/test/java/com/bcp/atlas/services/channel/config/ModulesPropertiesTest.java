package com.bcp.atlas.services.channel.config;

import com.bcp.atlas.services.channel.config.ModulesProperties.Modules;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ModulesPropertiesTest {

  @InjectMocks
  private ModulesProperties moduleProperties;

  @Mock
  private ApplicationProperties properties;

  @Test
  public void createObjectTest() {
    Modules modules = new Modules();
    modules.setVariables(TestMapsUtils.getRModulesMap());
    moduleProperties.setAn(modules);
    moduleProperties.setBp(modules);
    moduleProperties.setCem(modules);
    moduleProperties.setIs(modules);
    moduleProperties.setRh(modules);
    moduleProperties.setSbs(modules);
    moduleProperties.setTa(modules);
    moduleProperties.setTs(modules);

    Assert.assertNotNull(modules.getVariables());
    Assert.assertNotNull(modules.toString());
    Assert.assertNotNull(moduleProperties.getAn());
    Assert.assertNotNull(moduleProperties.getBp());
    Assert.assertNotNull(moduleProperties.getCem());
    Assert.assertNotNull(moduleProperties.getIs());
    Assert.assertNotNull(moduleProperties.getRh());
    Assert.assertNotNull(moduleProperties.getSbs());
    Assert.assertNotNull(moduleProperties.getTa());
    Assert.assertNotNull(moduleProperties.getTs());
    
  }
}
