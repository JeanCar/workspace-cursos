package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.util.ValidUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;

@RunWith(MockitoJUnitRunner.class)
public class EvaluateExceptionOrderProccesorTest {


  @InjectMocks
  private EvaluateExceptionOrderProccesor processor;

  @InjectMocks
  private TestUtilsEvaluation test;

  @Mock
  private ValidUtils validUtils;

  @Before
  public void setUp() throws Exception {
    processor = new EvaluateExceptionOrderProccesor(validUtils);
  }

  @Test
  public void responseProcessorGetTest() throws Exception {
    String jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_GET;
    GetResponse getResponse = test.getLoanExceptionOrderResponseGet(jsonLoanRes);
    processor.convertResponseGetToUx(getResponse);
    Assert.assertNotNull(processor);
  }

  @Test
  public void responseProcessorPostTest() throws Exception {
    String jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_POST;
    PostResponse postResponse = test.getLoanExceptionOrderResponsePost(jsonLoanRes);
    postResponse.getException().getEvaluationResults().forEach(list -> {
      Assert.assertNotNull(list.getStatus());

    });
    processor.convertResponsePostToUx(postResponse);
    Assert.assertNotNull(processor);
  }

  @Test
  public void responseProcessorNullPostTest() throws Exception {
    String jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_POST_NULL;
    PostResponse postResponse = test.getLoanExceptionOrderResponsePost(jsonLoanRes);
    postResponse.getException().getEvaluationResults().forEach(list -> {
      Assert.assertNull("The object you enter return null", list.getStatus());
    });

    processor.convertResponsePostToUx(postResponse);
    Assert.assertNotNull(processor);
  }

}
