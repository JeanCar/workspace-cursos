package com.bcp.atlas.services.channel.util;

import static org.mockito.Mockito.when;

import com.bcp.atlas.services.channel.config.ApplicationProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InitModuleUtilTest {

  @InjectMocks
  private InitModulesUtil initModulesUtilTest;


  @Mock
  private ApplicationProperties properties;
  
  @Before
  public void setUp() throws Exception {
    when(properties.getArchivoNegativo()).thenReturn("");
    when(properties.getArchivoNegativoDescription()).thenReturn("");
    when(properties.getTipoCrediticia()).thenReturn("");
    when(properties.getTasa()).thenReturn("");
    when(properties.getTasaDescription()).thenReturn("");
    when(properties.getTipoComercial()).thenReturn("");
    when(properties.getTasaAutorizada()).thenReturn("");
    when(properties.getTasaAutorizadaDescription()).thenReturn("");
    when(properties.getTipoPricing()).thenReturn("");
    when(properties.getBoletasPago()).thenReturn("");
    when(properties.getBoletasPagoDescription()).thenReturn("");
    when(properties.getTipoDocumental()).thenReturn("");
    when(properties.getIngresosSustentados()).thenReturn("");
    when(properties.getIngresosSustentadosDescription()).thenReturn("");
    when(properties.getReciboHonorarios()).thenReturn("");
    when(properties.getReciboHonorariosDescription()).thenReturn("");
    when(properties.getCapacidadPago()).thenReturn("");
    when(properties.getCapacidadPagoDescription()).thenReturn("");
    when(properties.getClasificacionSbs()).thenReturn("");
    when(properties.getClasificacionSbsDescription()).thenReturn("");
  }
  
  @Test
  public void otherModulesTest() throws Exception {
    Assert.assertNotNull(initModulesUtilTest.otherModules());
  }

  @Test
  public void initModulesAdditionalTest() throws Exception {
    Assert.assertNotNull(initModulesUtilTest.initAn("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initTs("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initTa("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initBp("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initIs("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initRh("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initCem("LE0000"));
    Assert.assertNotNull(initModulesUtilTest.initSbs("LE0000"));
  }

}
