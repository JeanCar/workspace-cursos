package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.ArchivoNegativoWrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostModules;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostPersonsRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.EvaluationDetail;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtil;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtilTest.EvaluationResponse;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class LoanEvaluationServiceImplTest {

  private TestUtilsEvaluation test;
  private LoanEvaluationImpl loanEvaluationImpl;
  private LoanEvaluationUtil loanEvaluationUtil;
  private EvaluateExceptionOrderSender exceptionSender;
  private LoanEvaluationProccesor processor;
  private LoanEvaluationSender sender;
  private LoanEvaluationBuilder builder;
  private ApplicationProperties properties;
  private CircuitBreaker circuitBreaker;

  @Before
  public void setUp() throws Exception {
    test = new TestUtilsEvaluation();
    circuitBreaker = CircuitBreaker.of("name", CircuitBreakerConfig.ofDefaults());

    builder = mock(LoanEvaluationBuilder.class);
    sender = mock(LoanEvaluationSender.class);
    processor = mock(LoanEvaluationProccesor.class);
    exceptionSender = mock(EvaluateExceptionOrderSender.class);
    loanEvaluationUtil = mock(LoanEvaluationUtil.class);
    properties = mock(ApplicationProperties.class);

    when(properties.getAnCode()).thenReturn("4");
    when(properties.getCemCode()).thenReturn("5");
    when(properties.getSbsCode()).thenReturn("3");
    when(builder.builderTasaRequest(any())).thenReturn(new PostRequest());
    loanEvaluationImpl = new LoanEvaluationImpl(builder, processor, sender, circuitBreaker,
        exceptionSender, properties, loanEvaluationUtil);
  }


  @Test
  public void validateCrediticiaTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationImpl.validateCrediticia(wrapper);
    Assert.assertNotNull(loanEvaluationImpl);
  }


  @Test
  public void loanEvaluationDeclineTest() throws Exception {

    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);

    when(builder.buildRequest(request)).thenReturn(new LoanEvaluationRequest());
    when(sender.callLoanEvaluation(any())).thenReturn(Single.just(loanEvaluationResponse));
    when(properties.getAnCode()).thenReturn("AN");
    when(properties.getCemCode()).thenReturn("CEM");
    when(properties.getSbsCode()).thenReturn("SBS");
    when(loanEvaluationUtil.modulesDecline("5")).thenReturn(true);

    TestObserver<PostLoanEvaluationsUxResponse> testSingleResult = loanEvaluationImpl
        .loanEvaluations(TestMapsUtils.getLoanEvaluationUxHeader(), request).test();
    testSingleResult.awaitTerminalEvent();
  }

  @Test
  public void validateComercialPricingTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationImpl.validateComercialPricing(wrapper);
    Assert.assertNotNull(loanEvaluationImpl);
  }

  @Test
  public void validateModuleComercialPricingTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationImpl.validateModuleComercialPricing(wrapper);
    Assert.assertNotNull(loanEvaluationImpl);
  }

  @Test
  public void validateDocumentalTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_DOCUMENTAL;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationImpl.validateDocumental(wrapper);
    Assert.assertNotNull(loanEvaluationImpl);
  }

  @Test
  public void validateCdaResponseTest() throws Exception {
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);
    loanEvaluationImpl.validateCdaResponse(loanEvaluationResponse);
    Assert.assertNotNull(loanEvaluationImpl);
  }

  @Test
  public void returnCdaTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationImpl.returnCda(wrapper);
    Assert.assertNotNull(loanEvaluationImpl);
  }

  @Test
  public void returnCemTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    String jsonCem = TestConstants.EXCEPTION_CEM;
    PostResponse cem = test.getLoanExceptionOrderResponsePost(jsonCem);
    Single<PostResponse> callCem = Single.just(cem);
    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);

    doReturn(Optional.of(new EvaluationDetail())).when(loanEvaluationUtil)
        .evaluateCemStatusDeclined(wrapper);
    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callCem);

    when(loanEvaluationUtil.mapReturn(any())).thenReturn(new PostModules());


    TestObserver<Wrapper> testSingleResult = loanEvaluationImpl.returnCem(wrapper, request).test();
    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();
  }

  @Test
  public void callComerialPricingTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);

    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);

    String jsonTasa = TestConstants.EXCEPTION_TASA;
    PostResponse tasa = test.getLoanExceptionOrderResponsePost(jsonTasa);
    Single<PostResponse> callTasa = Single.just(tasa);

    String jsonTasaAutorizada = TestConstants.EXCEPTION_TASA_AUTORIZADA;
    PostResponse tasaAutorizada = test.getLoanExceptionOrderResponsePost(jsonTasaAutorizada);
    Single<PostResponse> callTasaAutorizada = Single.just(tasaAutorizada);

    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callTasa);
    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callTasaAutorizada);
    when(loanEvaluationUtil.mapReturn(any())).thenReturn(new PostModules());

    TestObserver<Wrapper> testSingleResult =
        loanEvaluationImpl.callComerialPricing(wrapper, request).test();

    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();
  }


  @Test
  public void callDocumentalTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_DOCUMENTAL;
    Wrapper wrapper = test.getWrapper(jsonWrapper);

    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);

    String jsonBoleta = TestConstants.EXCEPTION_BOLETA_PAGO;
    PostResponse boleta = test.getLoanExceptionOrderResponsePost(jsonBoleta);
    Single<PostResponse> callBoleta = Single.just(boleta);

    String jsonIngresos = TestConstants.EXCEPTION_INGRESOS_SUSTENTADOS;
    PostResponse ingresos = test.getLoanExceptionOrderResponsePost(jsonIngresos);
    Single<PostResponse> callIngresos = Single.just(ingresos);

    String jsonRecibo = TestConstants.EXCEPTION_RECIBO_HONORARIOS;
    PostResponse recibo = test.getLoanExceptionOrderResponsePost(jsonRecibo);
    Single<PostResponse> callRecibo = Single.just(recibo);

    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callBoleta);
    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callIngresos);
    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callRecibo);
    when(loanEvaluationUtil.mapReturn(any())).thenReturn(new PostModules());

    TestObserver<Wrapper> testSingleResult =
        loanEvaluationImpl.callDocumental(wrapper, request).test();

    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();
  }

  @Test
  public void callCrediticiaAnTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);

    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);

    String jsonCem = TestConstants.EXCEPTION_CEM;
    PostResponse cem = test.getLoanExceptionOrderResponsePost(jsonCem);
    Single<PostResponse> callCem = Single.just(cem);

    String jsonArchivo = TestConstants.EXCEPTION_ARCHIVO_NEGATIVO;
    PostResponse archivo = test.getLoanExceptionOrderResponsePost(jsonArchivo);
    Single<PostResponse> callAn = Single.just(archivo);

    String jsonoUnder = TestConstants.CROSS_UNDERWRITING;
    List<EvaluationElement> element = test.getEvaluationElementList(jsonoUnder);

    String jsonUnderWriting = TestConstants.CROSS_GET_RESPONSE;
    List<EvaluationResponse> evaluationList = test.getEvaluationResponseBackup(jsonUnderWriting);

    String jsonLoanReq = TestConstants.EVALUATE_CROSS_REQUEST_POST;
    PostRequest postRequest = test.getLoanExceptionOrderRequestPost(jsonLoanReq);

    when(loanEvaluationUtil.evaluateStatusModulesCrediticia(wrapper)).thenReturn(true);
    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callCem);

    doReturn(Optional.of(true)).when(loanEvaluationUtil).evaluateAnStatusDeclined(wrapper);
    PostPersonsRequest personRequest = new PostPersonsRequest();
    personRequest.setPersonId("124014751000");
    when(loanEvaluationUtil.capturePersonId(request)).thenReturn(Optional.of(personRequest));

    when(sender.callEvaluation(any())).thenReturn(Single.just(element));

    List<GetEvaluationResponse> evaluationResponse =
        TestMapsUtils.parseArchivoNegativo(evaluationList);
    when(builder.evaluationResponse(element)).thenReturn(evaluationResponse);

    doReturn(false).when(loanEvaluationUtil).validateFilterAn(evaluationResponse);

    ArchivoNegativoWrapper archivoNegativoWrapper = new ArchivoNegativoWrapper();
    archivoNegativoWrapper.setAmount(BigDecimal.TEN);
    archivoNegativoWrapper.setMonths("24");
    archivoNegativoWrapper.setReason("LEVE");
    when(builder.archivoNegatioWrapperBuilder(evaluationResponse))
        .thenReturn(archivoNegativoWrapper);

    when(builder.builderArchivoNegativoRequest(archivoNegativoWrapper,
        wrapper.getLoanEvaluationResponse(), request)).thenReturn(postRequest);
    when(exceptionSender.callLoanEvaluationPost(postRequest)).thenReturn(callAn);
    when(loanEvaluationUtil.mapReturn(archivo)).thenReturn(new PostModules());

    TestObserver<Wrapper> testSingleResult =
        loanEvaluationImpl.callCrediticia(wrapper, request).test();

    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();
  }


  @Test
  public void callCrediticiaSbsTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);

    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);

    String jsonCem = TestConstants.EXCEPTION_CEM;
    PostResponse cem = test.getLoanExceptionOrderResponsePost(jsonCem);
    Single<PostResponse> callCem = Single.just(cem);

    String jsonSbs = TestConstants.EXCEPTION_SBS;
    PostResponse sbs = test.getLoanExceptionOrderResponsePost(jsonSbs);
    Single<PostResponse> callSbs = Single.just(sbs);

    String jsonLoanReq = TestConstants.EVALUATE_CROSS_REQUEST_POST;
    PostRequest postRequest = test.getLoanExceptionOrderRequestPost(jsonLoanReq);

    when(loanEvaluationUtil.evaluateStatusModulesCrediticia(wrapper)).thenReturn(true);

    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callCem);
    when(loanEvaluationUtil.mapReturn(any())).thenReturn(new PostModules());

    doReturn(Optional.empty()).when(loanEvaluationUtil).evaluateAnStatusDeclined(wrapper);

    doReturn(Optional.of(new EvaluationDetail())).when(loanEvaluationUtil)
        .evaluateSbsStatusDeclined(wrapper);

    when(builder.builderClasificacionSbsRequest(wrapper.getLoanEvaluationResponse()))
        .thenReturn(postRequest);
    when(exceptionSender.callLoanEvaluationPost(any())).thenReturn(callSbs);

    when(loanEvaluationUtil.mapReturn(any())).thenReturn(new PostModules());

    TestObserver<Wrapper> testSingleResult =
        loanEvaluationImpl.callCrediticia(wrapper, request).test();

    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();
  }

  @Test
  public void testNoConstructor() {
    loanEvaluationImpl = new LoanEvaluationImpl();

  }
}
