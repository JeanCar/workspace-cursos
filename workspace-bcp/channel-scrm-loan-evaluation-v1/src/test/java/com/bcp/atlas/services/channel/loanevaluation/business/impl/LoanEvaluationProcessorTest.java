package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.util.InitModulesUtil;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtil;
import com.bcp.atlas.services.channel.util.ValidUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;

@RunWith(MockitoJUnitRunner.class)
public class LoanEvaluationProcessorTest {

  @InjectMocks
  private LoanEvaluationProccesor processor;

  @InjectMocks
  private TestUtilsEvaluation test;

  @Mock
  private ValidUtils validUtils;

  @Mock
  private ApplicationProperties properties;

  @Mock
  private LoanEvaluationUtil loanEvaluationUtil;

  @Mock
  private InitModulesUtil initModulesUtil;


  @Before
  public void setUp() throws Exception {
    processor =
        new LoanEvaluationProccesor(validUtils, properties, loanEvaluationUtil, initModulesUtil);
  }


  @Test
  public void convertToCdaTest() throws Exception {
    String jsonWrapperRes = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapperRes);
    Assert.assertNotNull(wrapper.getPostResponseList().getTasa());
    Assert.assertNotNull(wrapper.getPostResponseList().getTasaAutorizada());
    Assert.assertNotNull(wrapper.getPostResponseList().getArchivoNegativo());
    Assert.assertNotNull(wrapper.getPostResponseList().getCem());
    processor.convertResponseToUx(wrapper);
    Assert.assertNotNull(processor);
  }

  @Test
  public void responseProcessorDocumentalTest() throws Exception {
    String jsonWrapperRes = TestConstants.WRAPPER_DOCUMENTAL;
    Wrapper wrapper = test.getWrapper(jsonWrapperRes);
    Assert.assertNotNull(wrapper.getPostResponseList().getTasa());
    Assert.assertNotNull(wrapper.getPostResponseList().getTasaAutorizada());
    Assert.assertNotNull(wrapper.getPostResponseList().getBoletaPago());
    Assert.assertNotNull(wrapper.getPostResponseList().getReciboHonorarios());
    Assert.assertNotNull(wrapper.getPostResponseList().getIngresoSustentos());
    processor.convertResponseToUx(wrapper);
    Assert.assertNotNull(processor);
  }

  @Test
  public void responseProcessorCrediticiaAnDeclinedTest() throws Exception {
    String jsonWrapperRes = TestConstants.WRAPPER_CREDITICIA_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapperRes);
    Assert.assertNotNull(wrapper.getLoanEvaluationResponse());
    processor.convertResponseToUx(wrapper);
    Assert.assertNotNull(processor);
  }

  @Test
  public void responseProcessorCrediticiaAnNoDeclinedTest() throws Exception {
    String jsonWrapperRes = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapperRes);
    Assert.assertNotNull(wrapper.getPostResponseList().getTasa());
    Assert.assertNotNull(wrapper.getPostResponseList().getTasaAutorizada());
    Assert.assertNotNull(wrapper.getPostResponseList().getSbs());
    Assert.assertNotNull(wrapper.getPostResponseList().getCem());
    processor.convertResponseToUx(wrapper);
    Assert.assertNotNull(processor);
  }

  @Test
  public void responseProcessorDeclinedTest() throws Exception {
    String jsonWrapperRes = TestConstants.WRAPPER_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapperRes);
    processor.convertResponseToUx(wrapper);
    Assert.assertNotNull(processor);
  }
}
