package com.bcp.atlas.services.channel.loanevaluation.model.api.bean;


import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ArchivoNegativoWrapperTest {


  private ArchivoNegativoWrapper archivoNegativoWrapper;

  @Before
  public void setUp() {
    archivoNegativoWrapper = new ArchivoNegativoWrapper();
  }

  @Test
  public void createQueryParamTest() {
    archivoNegativoWrapper.setAmount(BigDecimal.ZERO);
    archivoNegativoWrapper.setMonths("24");
    archivoNegativoWrapper.setReason("LEVE");
    Assert.assertNotNull(archivoNegativoWrapper.getAmount());
    Assert.assertNotNull(archivoNegativoWrapper.getMonths());
    Assert.assertNotNull(archivoNegativoWrapper.getReason());

  }
}
