package com.bcp.atlas.services.channel.loanevaluation.util;

import com.bcp.atlas.core.constants.HttpHeadersKey;
import com.bcp.atlas.services.channel.config.IncomesProperties.Income;
import com.bcp.atlas.services.channel.config.ModulesProperties.Modules;
import com.bcp.atlas.services.channel.config.ReasonsProperties.Reasons;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostModules;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.PostResponseList;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetRecordsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.header.LoanEvaluationUxHeader;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtilTest.EvaluationResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TestMapsUtils {

  public static LoanEvaluationUxHeader getLoanEvaluationUxHeader() {
    LoanEvaluationUxHeader headers = new LoanEvaluationUxHeader();
    headers.setRequestDate(Constants.REQUEST_DATE);
    headers.setUserCode(Constants.USER_CODE);
    headers.setApplicationCode(Constants.APP_CODE);
    headers.setCallerName(Constants.CALLER_NAME);
    headers.setRequestDate(Constants.REQUEST_DATE);
    return headers;
  }

  public static Map<String, String> getReasonsMap() {
    Map<String, String> reasonsMap = new HashMap<>();
    reasonsMap.put("001002", "GRAVE");
    reasonsMap.put("005005", "GRAVE");
    reasonsMap.put("006002", "LEVE");
    return reasonsMap;
  }

  public static Map<String, String> getRModulesMap() {
    Map<String, String> modulesMap = new HashMap<>();
    modulesMap.put("an01", "segmentacion_riesgo");
    modulesMap.put("an02", "antiguedad_archivo_negativo_meses");
    modulesMap.put("an03", "clasificacion_archivo_negativo");
    modulesMap.put("an04", "clasificacion_deudora_bcp");
    modulesMap.put("an04", "monto_reporte_archivo_negativo");
    return modulesMap;
  }

  public static Map<String, BigDecimal> getSegment() {
    Map<String, BigDecimal> segmentMap = new HashMap<>();
    segmentMap.put("an01", BigDecimal.TEN);
    segmentMap.put("an02", BigDecimal.ZERO);
    return segmentMap;
  }

  public static Reasons returnedReasons() {
    Reasons reasons = new Reasons();
    Map<String, String> reasonsMap = new HashMap<>();
    reasonsMap.put("001002", "GRAVE");
    reasonsMap.put("005005", "GRAVE");
    reasonsMap.put("006002", "LEVE");
    reasons.setValues(reasonsMap);
    return reasons;
  }

  public static Modules returnedModulesSbs() {
    Modules sbs = new Modules();
    Map<String, String> sbsMap = new HashMap<>();
    sbsMap.put("sbs01", "segmentacion_riesgo");
    sbs.setVariables(sbsMap);
    return sbs;
  }

  public static Modules returnedModulesAn() {
    Modules an = new Modules();
    Map<String, String> anMap = new HashMap<>();
    anMap.put("an01", "segmentacion_riesgo");
    anMap.put("an02", "segmentacion_riesgo");
    anMap.put("an03", "segmentacion_riesgo");
    anMap.put("an04", "segmentacion_riesgo");
    anMap.put("an05", "segmentacion_riesgo");
    an.setVariables(anMap);
    return an;
  }

  public static Modules returnedModulesCde() {
    Modules cde = new Modules();
    Map<String, String> cdeMap = new HashMap<>();
    cdeMap.put("cde01", "segmentacion_riesgo");
    cde.setVariables(cdeMap);
    return cde;
  }


  public static Modules returnedModulesIs() {
    Modules is = new Modules();
    Map<String, String> isMap = new HashMap<>();
    isMap.put("is01", "segmentacion_riesgo");
    isMap.put("is02", "ingreso_sustentado_fuera_pauta");
    isMap.put("is03", "ingreso_digitado_total");
    is.setVariables(isMap);
    return is;
  }


  public static Modules returnedModulesBp() {
    Modules bp = new Modules();
    Map<String, String> bpMap = new HashMap<>();
    bpMap.put("bp01", "segmentacion_riesgo");
    bp.setVariables(bpMap);
    return bp;
  }


  public static Modules returnedModulesRh() {
    Modules rh = new Modules();
    Map<String, String> rhMap = new HashMap<>();
    rhMap.put("rh01", "segmentacion_riesgo");
    rh.setVariables(rhMap);
    return rh;
  }

  public static Modules returnedModulesTs() {
    Modules ts = new Modules();
    Map<String, String> tsMap = new HashMap<>();
    tsMap.put("ts01", "segmentacion_riesgo");
    tsMap.put("ts01", "es_pago_de_haberes");
    ts.setVariables(tsMap);
    return ts;
  }

  public static Modules returnedModulesTa() {
    Modules ta = new Modules();
    Map<String, String> taMap = new HashMap<>();
    taMap.put("ta01", "es_pago_de_haberes");
    taMap.put("ta02", "score_cliente");
    taMap.put("ta03", "ingreso_calculado_bci");
    taMap.put("ta04", "segmento_cliente");
    ta.setVariables(taMap);
    return ta;
  }

  public static List<Income> getListaIncome() {
    List<Income> lista = new ArrayList<>();
    Income income = new Income();
    income.setMax(BigDecimal.TEN);
    income.setMin(BigDecimal.ZERO);
    return lista;
  }

  public static Map<String, String> getLoanHeaderMap() {
    Map<String, String> headerMap = new HashMap<>();
    headerMap.put(HttpHeadersKey.REQUEST_ID, Constants.REQUEST_ID);
    headerMap.put(HttpHeadersKey.USER_CODE, Constants.USER_CODE);
    headerMap.put(HttpHeadersKey.REQUEST_DATE, Constants.REQUEST_DATE);
    headerMap.put(HttpHeadersKey.APP_CODE, Constants.APP_CODE);
    headerMap.put(HttpHeadersKey.CALLER_NAME, Constants.CALLER_NAME);
    headerMap.put(HttpHeadersKey.SERVER_APP, Constants.SERVER_APP);
    headerMap.put(HttpHeadersKey.BRANCH_OFFICE_CODE, Constants.BRANCH_OFFICE_CODE);
    return headerMap;
  }

  public static PostResponseList mapPostResponseList() throws Exception {
    TestUtilsEvaluation evaluation = new TestUtilsEvaluation();
    String jsoAn = TestConstants.EXCEPTION_ARCHIVO_NEGATIVO;
    String jsonBp = TestConstants.EXCEPTION_BOLETA_PAGO;
    String jsonCem = TestConstants.EXCEPTION_CEM;
    String jsonIs = TestConstants.EXCEPTION_INGRESOS_SUSTENTADOS;
    String jsonRh = TestConstants.EXCEPTION_RECIBO_HONORARIOS;
    String jsonSbs = TestConstants.EXCEPTION_SBS;
    String jsonTs = TestConstants.EXCEPTION_TASA_AUTORIZADA;
    String jsonTa = TestConstants.EXCEPTION_TASA;
    PostResponse an = evaluation.getLoanExceptionOrderResponsePost(jsoAn);
    PostResponse bp = evaluation.getLoanExceptionOrderResponsePost(jsonBp);
    PostResponse cem = evaluation.getLoanExceptionOrderResponsePost(jsonCem);
    PostResponse is = evaluation.getLoanExceptionOrderResponsePost(jsonIs);
    PostResponse rh = evaluation.getLoanExceptionOrderResponsePost(jsonRh);
    PostResponse sbs = evaluation.getLoanExceptionOrderResponsePost(jsonSbs);
    PostResponse ts = evaluation.getLoanExceptionOrderResponsePost(jsonTs);
    PostResponse ta = evaluation.getLoanExceptionOrderResponsePost(jsonTa);
    PostModules moduloAn = new PostModules();
    moduloAn.setModule(an);
    PostModules moduloBp = new PostModules();
    moduloBp.setModule(bp);
    PostModules moduloCem = new PostModules();
    moduloCem.setModule(cem);
    PostModules moduloIs = new PostModules();
    moduloIs.setModule(is);
    PostModules moduloRh = new PostModules();
    moduloRh.setModule(rh);
    PostModules modulosbs = new PostModules();
    modulosbs.setModule(sbs);
    PostModules moduloTs = new PostModules();
    moduloTs.setModule(ts);
    PostModules moduloTa = new PostModules();
    moduloTa.setModule(ta);
    PostResponseList response = new PostResponseList();
    response.setArchivoNegativo(moduloAn);
    response.setBoletaPago(moduloBp);
    response.setCem(moduloCem);
    response.setIngresoSustentos(moduloIs);
    response.setReciboHonorarios(moduloRh);
    response.setSbs(modulosbs);
    response.setTasa(moduloTs);
    response.setTasaAutorizada(moduloTa);
    return response;
  }

  public static List<GetEvaluationResponse> parseArchivoNegativo(List<EvaluationResponse> evaluationList) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMATTER02);
    List<GetEvaluationResponse> archivoNegativoList = new ArrayList<>();
    evaluationList.forEach(list -> {
      GetEvaluationResponse eva = new GetEvaluationResponse();
      eva.setStatusCode(list.getStatusCode());
      List<GetRecordsResponse> recordList = new ArrayList<>();
      list.getRecords().forEach(li -> {
        GetRecordsResponse record = new GetRecordsResponse();
        record.setDebtsAmount(li.getDebtsAmount());
        record.setRegisterDate(LocalDate.parse(li.getRegisterDate(), formatter));
        record.setReasonCode(li.getReasonCode());
        recordList.add(record);
      });
      eva.setRecords(recordList);
      archivoNegativoList.add(eva);
    });
    return archivoNegativoList;
  }

}
