package com.bcp.atlas.services.channel.bdd.features;


import com.bcp.atlas.services.channel.AtlasChannelLoanEvaluationApplication;
import com.bcp.atlas.services.channel.bdd.AbstractBaseStep;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;


@ActiveProfiles("BDD")
@ContextConfiguration
@SpringBootTest(classes = AtlasChannelLoanEvaluationApplication.class,
    properties = "spring.main.webApplicationType = reactive",
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CommonSteps extends AbstractBaseStep {

  @Given("^Que se realiza un POST a la URI \"(.*)\"$")
  public void dadoSteps(String uri) throws Throwable {
    setUri(uri);
  }

  @And("^Se tiene autorizacion usando el APIKEY (.*)$")
  public void andSteps(String tokenFile) throws Throwable {
    setToken(TestUtilsEvaluation.fileResourceToString(tokenFile));

  }
}