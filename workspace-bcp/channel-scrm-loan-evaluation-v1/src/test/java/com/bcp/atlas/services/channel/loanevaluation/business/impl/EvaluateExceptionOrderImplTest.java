package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import static org.mockito.Mockito.when;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetExceptionOrderQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;


@RunWith(MockitoJUnitRunner.class)
public class EvaluateExceptionOrderImplTest {


  @InjectMocks
  private EvaluateExceptionOrderImpl loanExceptionOrderImpl;

  @InjectMocks
  TestUtilsEvaluation test;

  @Mock
  private EvaluateExceptionOrderProccesor processor;
  @Mock
  private EvaluateExceptionOrderSender sender;
  @Mock
  private EvaluateExceptionOrderBuilder builder;

  private CircuitBreaker circuitBreaker;

  private String jsonLoanReq;

  private String jsonLoanRes;


  @Before
  public void setUp() throws Exception {
    circuitBreaker = CircuitBreaker.of("123", CircuitBreakerConfig.ofDefaults());

    loanExceptionOrderImpl =
        new EvaluateExceptionOrderImpl(builder, processor, sender, circuitBreaker);
  }

  @Test
  public void evaluationExceptionOrderPostTest() throws Exception {

    jsonLoanReq = TestConstants.EVALUATE_UX_REQUEST_POST;
    PostExceptionOrderUxRequest loanExceptionOrderUxRequest =
        test.getloanExceptionOrderUxRequestPost(jsonLoanReq);

    jsonLoanReq = TestConstants.EVALUATE_CROSS_REQUEST_POST;
    PostRequest postRequest = test.getLoanExceptionOrderRequestPost(jsonLoanReq);

    jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_POST;
    PostResponse postResponse = test.getLoanExceptionOrderResponsePost(jsonLoanRes);

    jsonLoanRes = TestConstants.EVALUATE_UX_RESPONSE_POST;
    PostExceptionOrderUxResponse loanExceptionOrderUxResponsePost =
        test.getloanExceptionOrderUxResponsePost(jsonLoanRes);

    when(builder.buildPostRequest(loanExceptionOrderUxRequest)).thenReturn(postRequest);

    when(sender.callLoanEvaluationPost(postRequest)).thenReturn(Single.just(postResponse));

    when(processor.convertResponsePostToUx(postResponse))
        .thenReturn(loanExceptionOrderUxResponsePost);

    TestObserver<PostExceptionOrderUxResponse> testObserver =
        new TestObserver<PostExceptionOrderUxResponse>();

    loanExceptionOrderImpl
        .loanEvaluationsEvaluatePost(TestMapsUtils.getLoanEvaluationUxHeader(),
            loanExceptionOrderUxRequest)
        .subscribe(testObserver);
    testObserver.awaitTerminalEvent();
    testObserver.assertComplete();
  }

  @Test
  public void evaluationExceptionOrderGetTest() throws Exception {

    GetExceptionOrderQueryParam loanExceptionOrderQueryParam = new GetExceptionOrderQueryParam();
    loanExceptionOrderQueryParam.setExceptionId(TestConstants.EXCEPTION_ID);

    jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_GET;
    GetResponse getRespone = test.getLoanExceptionOrderResponseGet(jsonLoanRes);

    jsonLoanRes = TestConstants.EVALUATE_UX_RESPONSE_GET;
    GetExceptionOrderUxResponse loanExceptionOrderUxResponseGet =
        test.getloanExceptionOrderUxResponseGet(jsonLoanRes);


    when(sender.callLoanEvaluationGet(loanExceptionOrderQueryParam.getExceptionId()))
        .thenReturn(Maybe.just(getRespone));

    when(processor.convertResponseGetToUx(getRespone)).thenReturn(loanExceptionOrderUxResponseGet);

    loanExceptionOrderImpl =
        new EvaluateExceptionOrderImpl(builder, processor, sender, circuitBreaker);

    TestObserver<GetExceptionOrderUxResponse> testObserver =
        new TestObserver<GetExceptionOrderUxResponse>();

    loanExceptionOrderImpl.loanEvaluationsEvaluateGet(TestMapsUtils.getLoanEvaluationUxHeader(),
        loanExceptionOrderQueryParam).subscribe(testObserver);
    testObserver.awaitTerminalEvent();
    testObserver.assertComplete();
  }

  @Test
  public void testNoConstructor() {
    loanExceptionOrderImpl = new EvaluateExceptionOrderImpl();

  }
}
