package com.bcp.atlas.services.channel.config;


import com.bcp.atlas.services.channel.config.ReasonsProperties.Reasons;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ReasonsPropertiesTest {

  @InjectMocks
  private ReasonsProperties reasonProperties;

  @Mock
  private ApplicationProperties properties;

  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void createObjectTest() {
    Reasons reasons = new Reasons();
    reasons.setValues(TestMapsUtils.getReasonsMap());
    reasonProperties.setReasons(reasons);
    Assert.assertNotNull(reasons.getValues());
    Assert.assertNotNull(reasonProperties.getReasons());

  }
}
