package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import static org.mockito.Mockito.when;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetExceptionOrderQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.EvaluateApi;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import io.reactivex.Maybe;
import io.reactivex.Single;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;


@RunWith(MockitoJUnitRunner.class)
public class EvaluateExceptionOrderSenderTest {


  @InjectMocks
  private EvaluateExceptionOrderSender sender;

  @InjectMocks
  private TestUtilsEvaluation test;

  @Mock
  private EvaluateApi evaluateApi;

  @Before
  public void setUp() throws Exception {}


  @Test
  public void sendAndReceiveGet_01() throws Exception {

    String jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_GET;
    GetResponse getResponse = test.getLoanExceptionOrderResponseGet(jsonLoanRes);
    GetExceptionOrderQueryParam param = new GetExceptionOrderQueryParam();
    param.setExceptionId(TestConstants.EXCEPTION_ID);


    when(evaluateApi.getEvaluationExceptionOrdersUsingGET3(param.getExceptionId()))
        .thenReturn(Maybe.just(getResponse));

    sender.callLoanEvaluationGet(param.getExceptionId());
  }

  @Test
  public void sendAndReceivePost_02() throws Exception {

    String jsonLoan = TestConstants.EVALUATE_CROSS_REQUEST_POST;
    String jsonLoanRes = TestConstants.EVALUATE_CROSS_RESPONSE_POST;
    PostRequest postRequest = test.getLoanExceptionOrderRequestPost(jsonLoan);
    PostResponse postResponse = test.getLoanExceptionOrderResponsePost(jsonLoanRes);

    when(evaluateApi.postEvaluateUsingPOST3(postRequest)).thenReturn(Single.just(postResponse));

    sender.callLoanEvaluationPost(postRequest);
  }

}
