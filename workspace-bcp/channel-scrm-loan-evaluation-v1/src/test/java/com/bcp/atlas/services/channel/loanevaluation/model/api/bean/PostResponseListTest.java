package com.bcp.atlas.services.channel.loanevaluation.model.api.bean;


import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostResponseListTest {

  private PostResponseList postResponseList;

  @Before
  public void setUp() {
    postResponseList = new PostResponseList();
  }

  @Test
  public void createQueryParamTest() throws Exception {
    TestUtilsEvaluation evaluation = new TestUtilsEvaluation();
    String jsoAn = TestConstants.EXCEPTION_ARCHIVO_NEGATIVO;
    String jsonBp = TestConstants.EXCEPTION_BOLETA_PAGO;
    String jsonCem = TestConstants.EXCEPTION_CEM;
    String jsonIs = TestConstants.EXCEPTION_INGRESOS_SUSTENTADOS;
    String jsonRh = TestConstants.EXCEPTION_RECIBO_HONORARIOS;
    String jsonSbs = TestConstants.EXCEPTION_SBS;
    String jsonTs = TestConstants.EXCEPTION_TASA_AUTORIZADA;
    String jsonTa = TestConstants.EXCEPTION_TASA;
    PostResponse an = evaluation.getLoanExceptionOrderResponsePost(jsoAn);
    PostResponse bp = evaluation.getLoanExceptionOrderResponsePost(jsonBp);
    PostResponse cem = evaluation.getLoanExceptionOrderResponsePost(jsonCem);
    PostResponse is = evaluation.getLoanExceptionOrderResponsePost(jsonIs);
    PostResponse rh = evaluation.getLoanExceptionOrderResponsePost(jsonRh);
    PostResponse sbs = evaluation.getLoanExceptionOrderResponsePost(jsonSbs);
    PostResponse ts = evaluation.getLoanExceptionOrderResponsePost(jsonTs);
    PostResponse ta = evaluation.getLoanExceptionOrderResponsePost(jsonTa);

    PostModules moduloAn = new PostModules();
    moduloAn.setModule(an);
    PostModules moduloBp = new PostModules();
    moduloBp.setModule(bp);
    PostModules moduloCem = new PostModules();
    moduloCem.setModule(cem);
    PostModules moduloIs = new PostModules();
    moduloIs.setModule(is);
    PostModules moduloRh = new PostModules();
    moduloRh.setModule(rh);
    PostModules modulosbs = new PostModules();
    modulosbs.setModule(sbs);
    PostModules moduloTs = new PostModules();
    moduloTs.setModule(ts);
    PostModules moduloTa = new PostModules();
    moduloTa.setModule(ta);

    postResponseList.setArchivoNegativo(moduloAn);
    postResponseList.setBoletaPago(moduloBp);
    postResponseList.setCem(moduloCem);
    postResponseList.setIngresoSustentos(moduloIs);
    postResponseList.setReciboHonorarios(moduloRh);
    postResponseList.setSbs(modulosbs);
    postResponseList.setTasa(moduloTs);
    postResponseList.setTasaAutorizada(moduloTa);

    Assert.assertNotNull(postResponseList.getArchivoNegativo());
    Assert.assertNotNull(postResponseList.getBoletaPago());
    Assert.assertNotNull(postResponseList.getCem());
    Assert.assertNotNull(postResponseList.getIngresoSustentos());
    Assert.assertNotNull(postResponseList.getReciboHonorarios());
    Assert.assertNotNull(postResponseList.getSbs());
    Assert.assertNotNull(postResponseList.getTasa());
    Assert.assertNotNull(postResponseList.getTasaAutorizada());
    Assert.assertNotNull(postResponseList.toString());

  }
}
