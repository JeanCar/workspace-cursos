package com.bcp.atlas.services.channel.loanevaluation.model.api.get.response;

import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationFieldsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GetExceptionOrderUxResponseTest {

  private GetExceptionOrderUxResponse getExceptionOrderUxResponse;

  @Before
  public void setUp() {
    getExceptionOrderUxResponse = new GetExceptionOrderUxResponse();
  }


  @Test
  public void createResponseGetTest_01() {

    getExceptionOrderUxResponse.setExceptionId(1);
    getExceptionOrderUxResponse.setDescription("Clasificacion SBS");
    List<GetEvaluationFieldsResponse> evaluationFields = new ArrayList<>();
    GetEvaluationFieldsResponse evaluation = new GetEvaluationFieldsResponse();
    evaluation.setName("segmentacion_riesgo");
    evaluation.setType("STRING");
    evaluationFields.add(evaluation);
    getExceptionOrderUxResponse.setEvaluationFields(evaluationFields);
    Assert.assertNotNull(getExceptionOrderUxResponse.getExceptionId());
    Assert.assertNotNull(getExceptionOrderUxResponse.getDescription());
    Assert.assertNotNull(getExceptionOrderUxResponse.getEvaluationFields());
    evaluationFields.forEach(list -> {
      Assert.assertNotNull(evaluation.getName());
      Assert.assertNotNull(evaluation.getType());
    });
    Assert.assertNotNull(getExceptionOrderUxResponse);
    Assert.assertNotNull(getExceptionOrderUxResponse.toString());
  }
}
