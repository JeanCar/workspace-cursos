package com.bcp.atlas.services.channel.loanevaluation.util.constants;

import static lombok.AccessLevel.PRIVATE;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = PRIVATE)
public final class TestConstants {

  public static final Integer EXCEPTION_ID = 1;

  public static final String PAQUETE_API =
      "com.bcp.atlas.services.channel.loanevaluation.model.thirdparty";

  // REQUEST Y RESPONSE UX
  public static final String UX_REQUEST = "json/loan-evaluations/ux-request.json";
  public static final String UX_RESPONSE_CREDITICIA_AN_DECLINED =
      "json/loan-evaluations/ux-crediticia-an-declined.json";
  public static final String UX_RESPONSE_CREDITICIA_DECLINED =
      "json/loan-evaluations/ux-crediticia-declined.json"; 
  public static final String UX_RESPONSE_CREDITICIA_SBS =
      "json/loan-evaluations/ux-crediticia-sbs.json";
  public static final String UX_RESPONSE_DOCUMENTAL =
      "json/loan-evaluations/ux-documental.json";


  // MODULES
  public static final String EXCEPTION_TASA = 
      "json/loan-evaluations/modules/exception-tasa.json";
  public static final String EXCEPTION_TASA_AUTORIZADA =
      "json/loan-evaluations/modules/exception-tasa-autorizada.json";
  public static final String EXCEPTION_BOLETA_PAGO =
      "json/loan-evaluations/modules/exception-boleta-pago.json";
  public static final String EXCEPTION_INGRESOS_SUSTENTADOS =
      "json/loan-evaluations/modules/exception-ingresos-sustentados.json";
  public static final String EXCEPTION_RECIBO_HONORARIOS =
      "json/loan-evaluations/modules/exception-recibo-honorarios.json";
  public static final String EXCEPTION_CEM =
      "json/loan-evaluations/modules/exception-cem.json";
  public static final String EXCEPTION_SBS =
      "json/loan-evaluations/modules/exception-sbs.json";
  public static final String EXCEPTION_ARCHIVO_NEGATIVO =
      "json/loan-evaluations/modules/exception-archivo-negativo.json";
  
  // UNDERWRITING
  public static final String CROSS_UNDERWRITING =
      "json/loan-evaluations/underwriting/cross-underwriting.json";
  public static final String CROSS_UNDERWRITING_EMPTY =
      "json/loan-evaluations/underwriting/cross-underwriting-empty.json";
  public static final String CROSS_GET_RESPONSE =
      "json/loan-evaluations/underwriting/cross-getresponse.json";
  
  // WRAPPER
  public static final String WRAPPER_CREDITICIA_AN_DECLINED =
      "json/loan-evaluations/wrapper/wrapper-crediticia-an-declined.json";
  public static final String WRAPPER_CREDITICIA_DECLINED =
      "json/loan-evaluations/wrapper/wrapper-crediticia-declined.json";
  public static final String WRAPPER_CREDITICIA_SBS =
      "json/loan-evaluations/wrapper/wrapper-crediticia-sbs.json";
  public static final String WRAPPER_DOCUMENTAL =
      "json/loan-evaluations/wrapper/wrapper-documental.json";
  public static final String WRAPPER_DECLINED=
      "json/loan-evaluations/wrapper/wrapper-declined.json";
  public static final String WRAPPER_ARCHIVO_NEGATIVO=
      "json/loan-evaluations/wrapper/wrapper-archivo-negativo.json";
  
  // CDA
  public static final String CROSS_CDA_REQUEST =
      "json/loan-evaluations/cda/cross-cda-request.json";
  public static final String CROSS_CDA_RESPONSE_DECLINE = 
      "json/loan-evaluations/cda/cross-cda-response-decline.json";
  public static final String CROSS_CDA_RESPONSE_NO_DECLINE = 
      "json/loan-evaluations/cda/cross-cda-response-no-decline.json";
  public static final String CROSS_CDA_RESPONSE_NULL =
      "json/loan-evaluations/cda/cross-cda-response-null.json";


  // EVALUATE
  public static final String EVALUATE_CROSS_REQUEST_GET =
      "json/loan-evaluations/evaluate/evaluate-cross-request-get.json";
  public static final String EVALUATE_CROSS_REQUEST_POST =
      "json/loan-evaluations/evaluate/evaluate-cross-request-post.json";
  public static final String EVALUATE_CROSS_RESPONSE_GET_NULL =
      "json/loan-evaluations/evaluate/evaluate-cross-response-get-null.json";
  public static final String EVALUATE_CROSS_RESPONSE_GET =
      "json/loan-evaluations/evaluate/evaluate-cross-response-get.json";
  public static final String EVALUATE_CROSS_RESPONSE_POST_NULL =
      "json/loan-evaluations/evaluate/evaluate-cross-response-post-null.json";
  public static final String EVALUATE_CROSS_RESPONSE_POST =
      "json/loan-evaluations/evaluate/evaluate-cross-response-post.json";
  public static final String EVALUATE_UX_REQUEST_GET =
      "json/loan-evaluations/evaluate/evaluate-ux-request-get.json";
  public static final String EVALUATE_UX_REQUEST_POST =
      "json/loan-evaluations/evaluate/evaluate-ux-request-post.json";
  public static final String EVALUATE_UX_RESPONSE_GET =
      "json/loan-evaluations/evaluate/evaluate-ux-response-get.json";
  public static final String EVALUATE_UX_RESPONSE_POST =
      "json/loan-evaluations/evaluate/evaluate-ux-response-post.json";


}
