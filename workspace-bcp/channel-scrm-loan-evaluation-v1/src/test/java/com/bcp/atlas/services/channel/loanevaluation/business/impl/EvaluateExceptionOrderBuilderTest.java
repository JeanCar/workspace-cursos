package com.bcp.atlas.services.channel.loanevaluation.business.impl;


import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.util.ValidUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;

@RunWith(MockitoJUnitRunner.class)
public class EvaluateExceptionOrderBuilderTest {


  @InjectMocks
  private EvaluateExceptionOrderBuilder builder;

  @InjectMocks
  private TestUtilsEvaluation test;

  @Mock
  private ValidUtils validUtils;

  @Before
  public void setUp() throws Exception {}

  @Test
  public void builderPostTest() throws Exception {

    String jsonLoan = TestConstants.EVALUATE_UX_REQUEST_POST;
    PostExceptionOrderUxRequest loanEvaluationUxRequest =
        test.getloanExceptionOrderUxRequestPost(jsonLoan);
    builder = new EvaluateExceptionOrderBuilder(validUtils);
    builder.buildPostRequest(loanEvaluationUxRequest);

    Assert.assertNotNull(builder);
  }
}
