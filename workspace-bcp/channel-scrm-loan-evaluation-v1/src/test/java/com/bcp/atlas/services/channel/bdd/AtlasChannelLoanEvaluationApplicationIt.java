package com.bcp.atlas.services.channel.bdd;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Main Test class for running Spring Boot framework.<br/>
 * <b>Class</b>: AtlasChannelLoanEvaluationApplicationIT<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature/",
    plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report/cucumber.json"},
    glue = "com.bcp.atlas.services.channel.bdd.features", tags = "@loan-evaluations")
public class AtlasChannelLoanEvaluationApplicationIt {

}
