package com.bcp.atlas.services.channel.bdd;


import org.springframework.boot.web.server.LocalServerPort;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractBaseStep {
 
  @LocalServerPort
  private int port;
  private String uri;
  private String token;

  public String getBasePath() {
    return "http://localhost:" + port;
  }
}
