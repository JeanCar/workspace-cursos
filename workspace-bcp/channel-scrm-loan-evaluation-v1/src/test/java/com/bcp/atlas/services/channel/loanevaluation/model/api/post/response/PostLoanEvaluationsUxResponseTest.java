package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostEvaluationsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanOfferResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostModuleResultsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostReasonsResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class PostLoanEvaluationsUxResponseTest {
  private PostLoanEvaluationsUxResponse loanEvaluationUxResponse;

  private List<PostPersonsResponse> personList;
  private List<PostEvaluationsResponse> evaluationList;
  private List<PostEvaluationRulesResponse> ruleList;
  private List<PostReasonsResponse> reasonList;
  private List<PostAdditionalEvaluationsResponse> evalAddtitionalList;
  private List<PostModuleResultsResponse> resulList;

  @Before
  public void setUp() {
    loanEvaluationUxResponse = new PostLoanEvaluationsUxResponse();
    personList = new ArrayList<>();
    evaluationList = new ArrayList<>();
    ruleList = new ArrayList<>();
    reasonList = new ArrayList<>();
    evalAddtitionalList = new ArrayList<>();
    resulList = new ArrayList<>();

  }

  @Test
  public void createResponseTest_01() {

    loanEvaluationUxResponse.setLoanEvaluationId("I");
    loanEvaluationUxResponse.setEvaluationStatusCode("INVESTIGATE");
    loanEvaluationUxResponse.setEvaluationStatusDescription("INVESTIGATE");

    PostConsolidatedFinancialInformationResponse consolidated =
        new PostConsolidatedFinancialInformationResponse();
    consolidated.setEvaluationScore(351);
    PostIndebtednessCapacityResponse capacity = new PostIndebtednessCapacityResponse();
    capacity.setAmount(BigDecimal.ZERO);
    capacity.setCurrencyCode("PEN");
    consolidated.setIndebtednessCapacity(capacity);
    loanEvaluationUxResponse.setConsolidatedFinancialInformation(consolidated);

    PostPersonsResponse persons = new PostPersonsResponse();
    persons.setEvaluationScore(new BigDecimal("1000.00"));
    persons.setHasSalaryAccount(true);
    persons.setRelationTypeCode("001");
    persons.setRiskSegment("E");

    PostConsolidatedDebtCalculatedResponse debtCalculated =
        new PostConsolidatedDebtCalculatedResponse();
    debtCalculated.setAmount(new BigDecimal("1500.00"));
    debtCalculated.setCurrencyCode("PEN");
    debtCalculated.setRelatedSource("1");
    persons.setConsolidatedDebtCalculated(debtCalculated);

    PostSalaryIncomeResponse salary = new PostSalaryIncomeResponse();
    salary.setAmount(new BigDecimal("1200.00"));
    salary.setCurrencyCode("EUR");
    salary.setPeriodTypeCode("01");
    salary.setRelatedSource("00");
    salary.setAccountId("1");
    persons.setSalaryIncome(salary);

    PostRiskCalculatedIcomeResponse risk = new PostRiskCalculatedIcomeResponse();
    risk.setAmount(new BigDecimal("1200.00"));
    risk.setCurrencyCode("EUR");
    risk.setPeriodTypeCode("01");
    risk.setRelatedSource("00");
    persons.setRiskCalculatedIncome(risk);

    PostAdditionalIncomeResponse additional = new PostAdditionalIncomeResponse();
    additional.setAmount(new BigDecimal("1200.00"));
    additional.setCurrencyCode("EUR");
    additional.setPeriodTypeCode("01");
    additional.setRelatedSource("00");
    persons.setAdditionalIncome(additional);

    PostCalculatedIcomeResponse calculated = new PostCalculatedIcomeResponse();
    calculated.setAmount(new BigDecimal("1200.00"));
    calculated.setCurrencyCode("EUR");
    calculated.setPeriodTypeCode("01");
    calculated.setRelatedSource("00");
    persons.setCalculatedIncome(calculated);
    personList.add(persons);
    loanEvaluationUxResponse.setPersons(personList);

    PostLoanOfferResponse loanOffer = new PostLoanOfferResponse();
    loanOffer.setInstallmentsFrequency("M");
    loanOffer.setInstallmentsQuantity(100);
    loanEvaluationUxResponse.setLoanOffer(loanOffer);
    PostEvaluationsResponse evaluation = new PostEvaluationsResponse();
    evaluation.setModuleCode("ARCHIVO-SBS");
    evaluation.setModuleDescription("ARCHIVO-SBS");
    evaluation.setModuleClassification("CREDITICIA");
    evaluation.setStatusCode("D");
    evaluation.setStatusDescription("DECLINADO");

    PostEvaluationRulesResponse rules = new PostEvaluationRulesResponse();
    rules.setCode("1");
    rules.setDescription("reglas");
    rules.setContextCode("contexto");
    rules.setStatusCode("1");
    rules.setStatusDescription("status");
    PostReasonsResponse reason = new PostReasonsResponse();
    reason.setDescription("BLOQUEO TARJETA");
    reasonList.add(reason);
    rules.setReasons(reasonList);
    ruleList.add(rules);
    evaluation.setEvaluationRules(ruleList);
    evaluationList.add(evaluation);
    loanEvaluationUxResponse.setEvaluations(evaluationList);

    PostAdditionalEvaluationsResponse info = new PostAdditionalEvaluationsResponse();
    info.setStatusCode("LE0000");
    info.setModuleCode("MTS");
    info.setModuleDescription("Tasa");
    info.setModuleClassification("COMERCIAL");

    PostModuleResultsResponse result = new PostModuleResultsResponse();
    result.setAutonomyLevel("1");
    List<PostFieldslResponse> postAdditionalResponse = new ArrayList<>();
    PostFieldslResponse field = new PostFieldslResponse();
    field.setName("tasa_minima_porcentaje");
    field.setValue("12.5");
    postAdditionalResponse.add(field);
    result.setFields(postAdditionalResponse);
    resulList.add(result);
    info.setModuleResults(resulList);
    evalAddtitionalList.add(info);
    loanEvaluationUxResponse.setAdditionalEvaluations(evalAddtitionalList);

    Assert.assertNotNull(loanEvaluationUxResponse.getLoanEvaluationId());
    Assert.assertNotNull(loanEvaluationUxResponse.getEvaluationStatusCode());
    Assert.assertNotNull(loanEvaluationUxResponse.getEvaluationStatusDescription());

    Assert.assertNotNull(
        loanEvaluationUxResponse.getConsolidatedFinancialInformation().getEvaluationScore());
    Assert.assertNotNull(loanEvaluationUxResponse.getConsolidatedFinancialInformation()
        .getIndebtednessCapacity().getAmount());
    Assert.assertNotNull(loanEvaluationUxResponse.getConsolidatedFinancialInformation()
        .getIndebtednessCapacity().getCurrencyCode());

    Assert.assertNotNull(loanEvaluationUxResponse.getPersons());
    personList.forEach(lista -> {
      Assert.assertNotNull(lista.getEvaluationScore());
      Assert.assertNotNull(lista.isHasSalaryAccount());
      Assert.assertNotNull(lista.getRelationTypeCode());
      Assert.assertNotNull(lista.getRiskSegment());

      Assert.assertNotNull(lista.getConsolidatedDebtCalculated().getAmount());
      Assert.assertNotNull(lista.getConsolidatedDebtCalculated().getCurrencyCode());
      Assert.assertNotNull(lista.getConsolidatedDebtCalculated().getRelatedSource());

      Assert.assertNotNull(lista.getSalaryIncome().getAmount());
      Assert.assertNotNull(lista.getSalaryIncome().getCurrencyCode());
      Assert.assertNotNull(lista.getSalaryIncome().getPeriodTypeCode());
      Assert.assertNotNull(lista.getSalaryIncome().getRelatedSource());
      Assert.assertNotNull(lista.getSalaryIncome().getAccountId());


      Assert.assertNotNull(lista.getRiskCalculatedIncome().getAmount());
      Assert.assertNotNull(lista.getRiskCalculatedIncome().getCurrencyCode());
      Assert.assertNotNull(lista.getRiskCalculatedIncome().getPeriodTypeCode());
      Assert.assertNotNull(lista.getRiskCalculatedIncome().getRelatedSource());

      Assert.assertNotNull(lista.getAdditionalIncome().getAmount());
      Assert.assertNotNull(lista.getAdditionalIncome().getCurrencyCode());
      Assert.assertNotNull(lista.getAdditionalIncome().getPeriodTypeCode());
      Assert.assertNotNull(lista.getAdditionalIncome().getRelatedSource());

      Assert.assertNotNull(lista.getCalculatedIncome().getAmount());
      Assert.assertNotNull(lista.getCalculatedIncome().getCurrencyCode());
      Assert.assertNotNull(lista.getCalculatedIncome().getPeriodTypeCode());
      Assert.assertNotNull(lista.getCalculatedIncome().getRelatedSource());

    });

    Assert.assertNotNull(loanEvaluationUxResponse.getLoanOffer().getInstallmentsFrequency());
    Assert.assertNotNull(loanEvaluationUxResponse.getLoanOffer().getInstallmentsQuantity());

    Assert.assertNotNull(loanEvaluationUxResponse.getEvaluations());
    evaluationList.forEach(lista -> {
      Assert.assertNotNull(lista.getModuleCode());
      Assert.assertNotNull(lista.getModuleDescription());
      Assert.assertNotNull(lista.getModuleClassification());
      Assert.assertNotNull(lista.getStatusCode());
      Assert.assertNotNull(lista.getStatusDescription());
      Assert.assertNotNull(lista.getEvaluationRules());
      lista.getEvaluationRules().forEach(li -> {
        Assert.assertNotNull(li.getCode());
        Assert.assertNotNull(li.getDescription());
        Assert.assertNotNull(li.getContextCode());
        Assert.assertNotNull(li.getStatusCode());
        Assert.assertNotNull(li.getStatusDescription());
        Assert.assertNotNull(li.getReasons());
        li.getReasons().forEach(l -> {
          Assert.assertNotNull(l.getDescription());
        });

      });
    });

    Assert.assertNotNull(loanEvaluationUxResponse.getAdditionalEvaluations());

    evalAddtitionalList.forEach(lista -> {
      Assert.assertNotNull(lista.getStatusCode());
      Assert.assertNotNull(lista.getModuleClassification());
      Assert.assertNotNull(lista.getModuleCode());
      Assert.assertNotNull(lista.getModuleDescription());
      Assert.assertNotNull(lista.getModuleResults());

      lista.getModuleResults().forEach(li -> {
        Assert.assertNotNull(li.getAutonomyLevel());
        Assert.assertNotNull(li.getFields());

        li.getFields().forEach(l -> {
          Assert.assertNotNull(l.getName());
          Assert.assertNotNull(l.getValue());
        });
      });

    });
    Assert.assertNotNull(loanEvaluationUxResponse.toString());

  }
}
