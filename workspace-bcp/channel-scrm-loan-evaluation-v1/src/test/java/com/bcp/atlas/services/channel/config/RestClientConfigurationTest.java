package com.bcp.atlas.services.channel.config;

import static org.mockito.Mockito.when;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import okhttp3.OkHttpClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class RestClientConfigurationTest {

  @InjectMocks
  private RestClientConfiguration restClientConfiguration;

  private CircuitBreaker circuitBreaker;
  private String loanApiBaseUrl = "";
  private long connectTimeout;
  private long readTimeout;
  private long writeTimeout;
  OkHttpClient.Builder builder;

  @Before
  public void setUp() throws Exception {
    loanApiBaseUrl = "http://patlalnxd11:10999";
    circuitBreaker = CircuitBreaker.ofDefaults("atlas.default");
    connectTimeout = new Long("5000");
    readTimeout = new Long("4500");
    writeTimeout = new Long("3500");
  }

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private CrossCdaInterceptor interceptor1;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private CrossExceptionInterceptor interceptor2;


  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private CrossArchivoNegativoInterceptor interceptor3;

  @Test
  public void loanEvaluationApiTest() throws Exception, Throwable {
    OkHttpClient client = new OkHttpClient();
    when(interceptor1.intercept(Mockito.any()).code()).thenReturn(200);
    restClientConfiguration.loanEvaluationApi(loanApiBaseUrl, connectTimeout, readTimeout,
        writeTimeout, interceptor1, circuitBreaker, client.newBuilder());
    Assert.assertNotNull(restClientConfiguration);
  }

  @Test
  public void evaluteApiest() throws Exception, Throwable {
    OkHttpClient client = new OkHttpClient();
    when(interceptor2.intercept(Mockito.any()).code()).thenReturn(200);
    restClientConfiguration.evaluteApi(loanApiBaseUrl, connectTimeout, readTimeout, writeTimeout,
        interceptor2, circuitBreaker, client.newBuilder());
    Assert.assertNotNull(restClientConfiguration);
  }


  @Test
  public void evaluationApiTest() throws Exception, Throwable {

    OkHttpClient client = new OkHttpClient();
    when(interceptor3.intercept(Mockito.any()).code()).thenReturn(200);
    restClientConfiguration.evaluationApi(loanApiBaseUrl, connectTimeout, readTimeout, writeTimeout,
        interceptor3, circuitBreaker, client.newBuilder());
    Assert.assertNotNull(restClientConfiguration);
  }
}
