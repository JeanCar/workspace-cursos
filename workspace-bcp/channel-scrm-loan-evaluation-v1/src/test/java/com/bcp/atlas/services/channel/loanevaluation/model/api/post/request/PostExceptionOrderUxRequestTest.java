package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostExceptionOrderUxRequestTest {

  private PostExceptionOrderUxRequest postExceptionOrderUxRequest;

  @Before
  public void setUp() {
    postExceptionOrderUxRequest = new PostExceptionOrderUxRequest();
  }

  @Test
  public void createRequestTest() {

    postExceptionOrderUxRequest.setRequiredFullEvaluation(true);
    PostEvaluationRequest exception = new PostEvaluationRequest();
    exception.setExceptionId(1);
    List<PostEvaluationFieldsRequest> evaluationFieldsPost = new ArrayList<>();
    PostEvaluationFieldsRequest evaluation = new PostEvaluationFieldsRequest();
    evaluation.setName("segmentacion_riesgo");
    evaluation.setValue("E");
    evaluationFieldsPost.add(evaluation);
    exception.setEvaluationFields(evaluationFieldsPost);
    postExceptionOrderUxRequest.setEvaluation(exception);
    Assert.assertNotNull(postExceptionOrderUxRequest.getRequiredFullEvaluation());
    Assert.assertNotNull(postExceptionOrderUxRequest.getEvaluation().getEvaluationFields());
    evaluationFieldsPost.forEach(li -> {
      Assert.assertNotNull(li.getName());
      Assert.assertNotNull(li.getValue());
    });
    Assert.assertNotNull(postExceptionOrderUxRequest.toString());
  }
}
