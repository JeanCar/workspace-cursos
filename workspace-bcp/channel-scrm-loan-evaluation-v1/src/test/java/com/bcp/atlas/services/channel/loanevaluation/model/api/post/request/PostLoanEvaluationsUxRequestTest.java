package com.bcp.atlas.services.channel.loanevaluation.model.api.post.request;

import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostPersonsRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostMonthlyIncomesRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostLoanEvaluationsUxRequestTest {

  private PostLoanEvaluationsUxRequest postLoanEvaluationsUxRequest;

  @Before
  public void setUp() {
    postLoanEvaluationsUxRequest = new PostLoanEvaluationsUxRequest();
  }

  @Test
  public void createRequestTest() {
    postLoanEvaluationsUxRequest.setLoanOrderId("00004567");
    postLoanEvaluationsUxRequest.setRegisterDate("2018-02-12T09:09:59.000");
    PostSaleInformationRequest saleInformation = new PostSaleInformationRequest();
    saleInformation.setSaleTypeCode("1");
    PostOfferRequest offer = new PostOfferRequest();
    offer.setSaleTypeCode("1");
    offer.setTypeCode("001");
    offer.setCategoryCode("CATEGORY_01");
    saleInformation.setOffer(offer);
    postLoanEvaluationsUxRequest.setSaleInformation(saleInformation);
    List<PostPersonsRequest> personListUx = new ArrayList<>();
    PostPersonsRequest person = new PostPersonsRequest();
    person.setPersonId("101010101000");
    person.setEstimatedIncomeRequired(true);
    person.setRelationTypeCode("T");
    person.setBirthDate("1986-09-19");
    person.setMaritalStatusCode("SOL");
    PostAddressRequest address = new PostAddressRequest();
    address.setGeolocationCode("01210");
    address.setRegionCode("02");
    address.setProvinceCode("01");
    address.setDistrictCode("01");
    person.setAddress(address);
    List<PostMonthlyIncomesRequest> monthlyList = new ArrayList<>();
    PostMonthlyIncomesRequest monthly = new PostMonthlyIncomesRequest();
    monthly.setGrossAmount(new BigDecimal(15500.50));
    monthly.setAdditionalGrossAmount(new BigDecimal("1000.00"));
    monthly.setCurrencyCode("EUR");
    monthly.setIncomeTypeCode("01");
    monthly.setIncomeTaxCategoryCode(5);
    monthly.setEmployerId("20395135288");
    monthly.setEmployerPrimaryEconomicActivityCode("0162");
    monthly.setRelationshipStartDate("2001-02-12T09:09:59.000");
    person.setMonthlyIncomes(monthlyList);
    personListUx.add(person);
    postLoanEvaluationsUxRequest.setPersons(personListUx);

    PostEvaluationsRequest evaluation = new PostEvaluationsRequest();
    evaluation.setDebtClassificationBcp("0");
    evaluation.setIncomeOutsideGuideline(new BigDecimal("8000.00"));
    evaluation.setTotalDigitizedIncome(new BigDecimal("3000.00"));
    evaluation.setCustomerSegment("93");
    evaluation.setIncomeMark(true);
    postLoanEvaluationsUxRequest.setEvaluations(evaluation);

    Assert.assertNotNull(postLoanEvaluationsUxRequest.getLoanOrderId());
    Assert.assertNotNull(postLoanEvaluationsUxRequest.getRegisterDate());
    Assert.assertNotNull(postLoanEvaluationsUxRequest.getSaleInformation().getSaleTypeCode());
    Assert.assertNotNull(
        postLoanEvaluationsUxRequest.getSaleInformation().getOffer().getSaleTypeCode());
    Assert
        .assertNotNull(postLoanEvaluationsUxRequest.getSaleInformation().getOffer().getTypeCode());
    Assert.assertNotNull(
        postLoanEvaluationsUxRequest.getSaleInformation().getOffer().getCategoryCode());

    postLoanEvaluationsUxRequest.getPersons().forEach(list -> {
      Assert.assertNotNull(list.getPersonId());
      Assert.assertNotNull(list.getEstimatedIncomeRequired());
      Assert.assertNotNull(list.getRelationTypeCode());
      Assert.assertNotNull(list.getBirthDate());
      Assert.assertNotNull(list.getMaritalStatusCode());
      Assert.assertNotNull(list.getAddress().getGeolocationCode());
      Assert.assertNotNull(list.getAddress().getRegionCode());
      Assert.assertNotNull(list.getAddress().getProvinceCode());
      Assert.assertNotNull(list.getAddress().getDistrictCode());

      list.getMonthlyIncomes().forEach(li -> {
        Assert.assertNotNull(li.getGrossAmount());
        Assert.assertNotNull(li.getAdditionalGrossAmount());
        Assert.assertNotNull(li.getCurrencyCode());
        Assert.assertNotNull(li.getIncomeTaxCategoryCode());
        Assert.assertNotNull(li.getEmployerId());
        Assert.assertNotNull(li.getEmployerPrimaryEconomicActivityCode());
        Assert.assertNotNull(li.getRelationshipStartDate());
      });
    });

    Assert.assertNotNull(postLoanEvaluationsUxRequest.getEvaluations().getDebtClassificationBcp());
    Assert.assertNotNull(postLoanEvaluationsUxRequest.getEvaluations().getIncomeOutsideGuideline());
    Assert.assertNotNull(postLoanEvaluationsUxRequest.getEvaluations().getTotalDigitizedIncome());
    Assert.assertNotNull(postLoanEvaluationsUxRequest.getEvaluations().getCustomerSegment());
    Assert.assertNotNull(postLoanEvaluationsUxRequest.getEvaluations().getIncomeMark());


    Assert.assertNotNull(postLoanEvaluationsUxRequest.toString());
  }
}
