package com.bcp.atlas.services.channel.loanevaluation.model.api.get.response;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;

public class GetEvaluationResponseTest {
  private GetEvaluationResponse getEvaluationUxResponse;

  @Before
  public void setUp() {
    getEvaluationUxResponse = new GetEvaluationResponse();
  }

  @Test
  public void createResponseGetTest_01() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMATTER02);

    getEvaluationUxResponse.setStatusCode("001");
    List<GetRecordsResponse> recordsResponse = new ArrayList<>();
    GetRecordsResponse records01 = new GetRecordsResponse();
    records01.setRegisterDate(LocalDate.parse("2015-02-28T00:00:00",formatter));
    records01.setDebtsAmount(new BigDecimal("6215.2897"));
    records01.setReasonCode("GRAVE");
    GetRecordsResponse records02 = new GetRecordsResponse();
    records02.setRegisterDate(LocalDate.parse("2013-02-28T00:00:00",formatter));
    records02.setDebtsAmount(new BigDecimal("94596.6306"));
    records02.setReasonCode("LEVE");
    recordsResponse.add(records01);
    recordsResponse.add(records02);
    getEvaluationUxResponse.setRecords(recordsResponse);
    Assert.assertNotNull(getEvaluationUxResponse.getStatusCode());
    Assert.assertNotNull(getEvaluationUxResponse.getRecords());
    Assert.assertNotNull(getEvaluationUxResponse.toString());

  }
}
