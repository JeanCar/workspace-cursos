package com.bcp.atlas.services.channel.bdd.features;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import com.bcp.atlas.services.channel.bdd.AbstractBaseStep;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Qualifier;

@Slf4j
public class LoanEvaluationSteps01 {

  private String respuesta;
  private Integer statusCode;
  @Qualifier("AbstractBaseStep")
  AbstractBaseStep abs;

  public LoanEvaluationSteps01(AbstractBaseStep abs) {
    this.abs = abs;
  }

  @When("^Se usa el RECURSO01 (.*) enviandole un header (.*) y request (.*)$")
  public void cuandoSteps01(String resource, String headerFile, String requestFile)
      throws Exception {
    String requestHeader = TestUtilsEvaluation.fileResourceToString(headerFile);
    Map<String, String> head = TestUtilsEvaluation.getFileToMap(requestHeader);
    String requestBody = TestUtilsEvaluation.fileResourceToString(requestFile);
    Response res = null;
    String url = abs.getBasePath() + abs.getUri() + resource;
    log.info("URL : {}", url);
    res = given().contentType(Constants.APPLICATION_JSON).headers(head).auth()
        .oauth2(abs.getToken()).body(requestBody).when().post(url);

    statusCode = res.getStatusCode();
    respuesta = res.getBody().asString();
    log.info("STATUSCODE: {}", statusCode);
    log.info("RESPUESTA: {}", respuesta);
  }

  @Then("^El RESPONSE01 muestra el estado (.*), retornando con campos validos$")
  public void entoncesSteps01(int status) throws Throwable {
    Assert.assertThat(status, is(statusCode));
    Assert.assertNotNull(respuesta);
  }
}
