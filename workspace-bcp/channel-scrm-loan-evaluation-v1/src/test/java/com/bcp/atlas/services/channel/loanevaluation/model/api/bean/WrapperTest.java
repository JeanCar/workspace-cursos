package com.bcp.atlas.services.channel.loanevaluation.model.api.bean;

import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WrapperTest {

  @InjectMocks
  TestUtilsEvaluation test;


  private Wrapper wrapper;

  @Before
  public void setUp() {
    wrapper = new Wrapper();
  }

  @Test
  public void createQueryParamTest() throws Exception {
    wrapper.setCountModulesDeclined(true);
    wrapper.setCrediticiaDeclined(true);
    wrapper.setIncomeMark(true);
    String jsonRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse response = test.getLoanEvaluationResponse(jsonRes);

    wrapper.setLoanEvaluationResponse(response);
    wrapper.setPostResponseList(TestMapsUtils.mapPostResponseList());
    Assert.assertNotNull(wrapper.isCountModulesDeclined());
    Assert.assertNotNull(wrapper.isCrediticiaDeclined());
    Assert.assertNotNull(wrapper.isIncomeMark());
    Assert.assertNotNull(wrapper.getLoanEvaluationResponse());
    Assert.assertNotNull(wrapper.getPostResponseList());
    Assert.assertNotNull(wrapper.toString());
  }
}
