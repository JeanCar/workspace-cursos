package com.bcp.atlas.services.channel.util;

import com.bcp.atlas.core.constants.HttpHeadersKey;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;

public class TestUtils {

  public static String createUri(String uri, String path) throws IOException {
    return createUri(uri, path, "", null);
  }

  public static String createUri(String uri, String path, String fileQueryParam,
      ResourceLoader resourceLoader) throws IOException {
    if (path != "") {
      uri = uri + path;
    }
    if (fileQueryParam != "") {
      Properties prop = new Properties();
      prop.load(resourceLoader.getResource(fileQueryParam).getInputStream());
      boolean isFirst = true;
      StringBuilder sb = new StringBuilder();
      for (String key : prop.stringPropertyNames()) {
        if (!isFirst) {
          sb.append('&');
        }
        sb.append(key);
        sb.append("=");
        sb.append(prop.getProperty(key));
        isFirst = false;
      }
      uri = uri + "?" + sb.toString();
    }
    return uri;
  }

  public static HttpHeaders getLoanHeaderRequired() {
    HttpHeaders authHeader = new HttpHeaders();
    authHeader.add(HttpHeadersKey.REQUEST_ID, Constants.REQUEST_ID);
    authHeader.add(HttpHeadersKey.REQUEST_DATE, Constants.REQUEST_DATE);
    authHeader.add(HttpHeadersKey.USER_CODE, Constants.USER_CODE);
    authHeader.add(HttpHeadersKey.APP_CODE, Constants.APP_CODE);
    authHeader.add(HttpHeadersKey.CALLER_NAME, Constants.CALLER_NAME);
    return authHeader;
  }

  public static Headers createHeader(Properties prop) {
    List<Header> list = new ArrayList<>();

    for (String itemKey : prop.stringPropertyNames()) {
      list.add(new Header(itemKey, prop.getProperty(itemKey)));
    }
    return new Headers(list);
  }
}
