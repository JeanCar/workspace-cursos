package com.bcp.atlas.services.channel.util;

import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ExceptionUtilTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  public void exception_util_01() throws Exception {

    Assert.assertNotNull(ExceptionUtils.getAtlasExceptionBuilderFromHttpCall(
        AtlasException.builder().addDetail().withCode(Constants.CODE_ERROR_UNEXPECTED)
            .withDescription(Constants.MESSAGE_ERROR_UNEXPECTED).push().build()));
  }

  @Test
  public void exception_util_02() throws Exception {
    Assert.assertNotNull(ExceptionUtils.onLoanEvaluationError(
        AtlasException.builder().addDetail().withCode(Constants.CODE_ERROR_UNEXPECTED)
            .withDescription(Constants.MESSAGE_ERROR_UNEXPECTED).push().build()));
  }


  @Test
  public void exception_util_03() throws Exception {
    Assert.assertNotNull(ExceptionUtils.onLoanExceptionOrderGetError(
        AtlasException.builder().addDetail().withCode(Constants.CODE_ERROR_UNEXPECTED)
            .withDescription(Constants.MESSAGE_ERROR_UNEXPECTED).push().build()));
  }

  @Test
  public void exception_util_04() throws Exception {
    Assert.assertNotNull(ExceptionUtils.onLoanExceptionOrderPostError(
        AtlasException.builder().addDetail().withCode(Constants.CODE_ERROR_UNEXPECTED)
            .withDescription(Constants.MESSAGE_ERROR_UNEXPECTED).push().build()));
  }
}
