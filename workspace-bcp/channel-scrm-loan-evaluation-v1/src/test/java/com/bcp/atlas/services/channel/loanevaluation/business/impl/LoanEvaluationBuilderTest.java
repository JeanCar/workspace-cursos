package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import static org.mockito.Mockito.when;
import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.config.ModulesProperties;
import com.bcp.atlas.services.channel.config.ReasonsProperties;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.InvolvedResourceResponse;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.RecordsResponse;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.RecordsStatusResponse;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.StatusResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.ArchivoNegativoWrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetRecordsResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtil;
import com.bcp.atlas.services.channel.util.ValidUtils;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;


@RunWith(MockitoJUnitRunner.class)
public class LoanEvaluationBuilderTest {

  @InjectMocks
  private LoanEvaluationBuilder builder;

  @InjectMocks
  private TestUtilsEvaluation test;

  @Mock
  private ValidUtils validUtils;

  @Mock
  private ApplicationProperties properties;

  @Mock
  private ReasonsProperties reasonsProperties;

  @Mock
  private LoanEvaluationUtil loanEvaluationUtil;

  @Mock
  private ModulesProperties modulesProperties;


  @Before
  public void setUp() throws Exception {

    when(reasonsProperties.getReasons()).thenReturn(TestMapsUtils.returnedReasons());
    when(modulesProperties.getSbs()).thenReturn(TestMapsUtils.returnedModulesSbs());
    when(modulesProperties.getAn()).thenReturn(TestMapsUtils.returnedModulesAn());
    when(modulesProperties.getCem()).thenReturn(TestMapsUtils.returnedModulesCde());
    when(modulesProperties.getIs()).thenReturn(TestMapsUtils.returnedModulesIs());
    when(modulesProperties.getBp()).thenReturn(TestMapsUtils.returnedModulesBp());
    when(modulesProperties.getRh()).thenReturn(TestMapsUtils.returnedModulesRh());
    when(modulesProperties.getTs()).thenReturn(TestMapsUtils.returnedModulesTs());
    when(modulesProperties.getTa()).thenReturn(TestMapsUtils.returnedModulesTa());

    when(properties.getReqLoanAmount()).thenReturn("1500.5");
    when(properties.getReqLoanCurrencyCode()).thenReturn("PEN");
    when(properties.getReqLoanCurrencyDescription()).thenReturn("SOL");
    when(properties.getReqLoanPaymenPeriodValue()).thenReturn("12");
    when(properties.getReqLoanPaymentPeriodUnitCode()).thenReturn("M");
    when(properties.getReqLoanPaymentPeriodUnitDescription()).thenReturn("MONTH");
    when(properties.getReqPersonRelationTypeDescription()).thenReturn("TITULAR");

  }


  @Test
  public void builderTest() throws Exception {

    String jsonLoanRequest = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest loanEvaluationUxRequest =
        test.getloanEvaluationUxRequest(jsonLoanRequest);

    String jsonLoanResponse = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse loanEvaluationResponse =
        test.getLoanEvaluationResponse(jsonLoanResponse);

    String jsonArchivoNegativo = TestConstants.WRAPPER_ARCHIVO_NEGATIVO;
    ArchivoNegativoWrapper archivoNegativoWrapper =
        test.getArchivoNegativoWrapper(jsonArchivoNegativo);

    builder = new LoanEvaluationBuilder(properties, reasonsProperties, modulesProperties,
        loanEvaluationUtil);
    builder.buildRequest(loanEvaluationUxRequest);
    builder.builderClasificacionSbsRequest(loanEvaluationResponse);
    builder.builderArchivoNegativoRequest(archivoNegativoWrapper, loanEvaluationResponse,
        loanEvaluationUxRequest);
    builder.builderCdeRequest(loanEvaluationResponse);
    builder.builderIngresosSustentadosRequest(loanEvaluationResponse, loanEvaluationUxRequest);
    builder.builderBoletasPagoRequest(loanEvaluationResponse);
    builder.builderReciboHonorariosRequest(loanEvaluationResponse);
    builder.builderTasaRequest(loanEvaluationResponse);
    builder.builderTasaAutorizadaRequest(loanEvaluationResponse, loanEvaluationUxRequest);
    builder.builderClasificacionSbsRequest(loanEvaluationResponse);
    builder.builderCdeRequest(loanEvaluationResponse);
    Assert.assertNotNull(builder);
  }


  @Test
  public void evaluationResponseTest() throws Exception {
    builder = new LoanEvaluationBuilder(properties, reasonsProperties, modulesProperties,
        loanEvaluationUtil);

    List<EvaluationElement> evaluationElement = new ArrayList<>();
    EvaluationElement element = new EvaluationElement();
    StatusResponse status = new StatusResponse();
    status.setCode("1");
    element.setStatus(status);
    List<RecordsResponse> recordsResponse = new ArrayList<>();
    RecordsResponse record = new RecordsResponse();
    record.setRegisterDate("2013-02-28T00:00:00");
    record.setDebtsAmount(new BigDecimal("15000.00"));
    RecordsStatusResponse recordStatus = new RecordsStatusResponse();
    recordStatus.setCode("001");
    record.setStatus(recordStatus);
    InvolvedResourceResponse involved = new InvolvedResourceResponse();
    involved.setCode("002");
    record.setInvolvedResource(involved);
    recordsResponse.add(record);
    element.setRecords(recordsResponse);
    evaluationElement.add(element);
    Assert.assertNotNull(builder.evaluationResponse(evaluationElement));

  }

  @Test
  public void archivoNegatioWrapperBuilderTest() throws Exception {
    builder = new LoanEvaluationBuilder(properties, reasonsProperties, modulesProperties,
        loanEvaluationUtil);

    List<GetEvaluationResponse> evaluationResponse = new ArrayList<>();
    GetEvaluationResponse evaluation = new GetEvaluationResponse();
    evaluation.setStatusCode("1");
    List<GetRecordsResponse> recordsResponse = new ArrayList<>();
    GetRecordsResponse records = new GetRecordsResponse();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMATTER02);
    records.setRegisterDate(LocalDate.parse("2013-02-28T00:00:00", formatter));
    records.setReasonCode("LEVE");
    recordsResponse.add(records);
    evaluation.setRecords(recordsResponse);
    evaluationResponse.add(evaluation);
    Assert.assertNotNull(builder.archivoNegatioWrapperBuilder(evaluationResponse));

  }
}
