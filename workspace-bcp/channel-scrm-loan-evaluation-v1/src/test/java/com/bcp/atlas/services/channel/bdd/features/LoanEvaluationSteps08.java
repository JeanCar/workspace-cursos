package com.bcp.atlas.services.channel.bdd.features;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import com.bcp.atlas.services.channel.bdd.AbstractBaseStep;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Qualifier;

@Slf4j
public class LoanEvaluationSteps08 {

  private Integer statusCode;
  @Qualifier("AbstractBaseStep")
  AbstractBaseStep abs;

  public LoanEvaluationSteps08(AbstractBaseStep abs) {
    this.abs = abs;
  }

  @When("^Se usa el SUBRECURSO08 (.*) enviandole un header (.*)$")
  public void cuandoSteps08(String subresource, String headerFile)
      throws Exception {
    String requestHeader = TestUtilsEvaluation.fileResourceToString(headerFile);
    Map<String, String> head = TestUtilsEvaluation.getFileToMap(requestHeader);
    Response res = null;
    String url = abs.getBasePath() + abs.getUri() + subresource;
    log.info("URL : {}", url);
    res = given().contentType(Constants.APPLICATION_JSON).headers(head).auth()
        .oauth2(abs.getToken()).get(url);

    statusCode = res.getStatusCode();
    log.info("STATUSCODE: {}", statusCode);
  }

  @Then("^El RESPONSE08 muestra el estado (.*), indicando que no se encontro el recurso$")
  public void entoncesSteps08(int status) throws Throwable {
    Assert.assertThat(status, is(statusCode));
  
  }
}
