package com.bcp.atlas.services.channel.config;

import static org.mockito.Mockito.when;
import com.bcp.atlas.core.constants.HttpHeadersKey;
import com.bcp.atlas.services.channel.config.CrossCdaInterceptor.CustomHeaders;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.GenericUtil;
import java.io.IOException;
import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CrossCdaInterceptorTest {

  @InjectMocks
  private CrossCdaInterceptor interceptor;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private CustomHeaders headers;

  @Mock
  private ApplicationProperties properties;
  @Mock
  private GenericUtil utils;

  @Before
  public void init() {
    when(properties.getOrgCode()).thenReturn("1");
    when(properties.getAgencyCode()).thenReturn("001");
    when(properties.getClientCode()).thenReturn("TL");
    when(properties.getOpnCode()).thenReturn("13");
    when(properties.getGuildClient()).thenReturn("S");
    when(properties.getAppName()).thenReturn("RBM");
    when(properties.getServerApp()).thenReturn("DPCM");
    when(properties.getBranchOfficeCode()).thenReturn("300");
    when(properties.getServerName()).thenReturn("loan-evaluation-v1");
    when(properties.getServerTerminal()).thenReturn("LEA1");
    when(properties.getOpnType()).thenReturn("001");
    when(properties.getServerIp()).thenReturn("127.0.0.1");
    when(properties.getEnv()).thenReturn("DES");
  }

  @Test
  public void interceptTest() throws IOException {
    Chain chain = Mockito.mock(Chain.class, Answers.RETURNS_DEEP_STUBS);
    Request request = Mockito.mock(Request.class, Answers.RETURNS_DEEP_STUBS);
    when(headers.getHeader()).thenReturn(TestMapsUtils.getLoanHeaderMap());
    when(chain.request()).thenReturn(request);
    okhttp3.Headers.Builder headerBuilder = new okhttp3.Headers.Builder();
    headerBuilder.add(HttpHeadersKey.ORG_CODE, properties.getOrgCode());
    headerBuilder.add(HttpHeadersKey.AGENCY_CODE, properties.getAgencyCode());
    headerBuilder.add(HttpHeadersKey.CLIENT_CODE, properties.getClientCode());
    headerBuilder.add(HttpHeadersKey.OPERATION_CODE, properties.getOpnCode());
    headerBuilder.add(HttpHeadersKey.USER_CODE, "APS9" + properties.getEnv());
    headerBuilder.add(Constants.NAME_GUILD_CLIENT, properties.getGuildClient());
    headerBuilder.add(HttpHeadersKey.APP_NAME, properties.getAppName());
    headerBuilder.add(HttpHeadersKey.SERVER_APP, properties.getServerApp());
    headerBuilder.add(HttpHeadersKey.BRANCH_OFFICE_CODE, properties.getBranchOfficeCode());
    headerBuilder.add(HttpHeadersKey.SERVER_NAME, properties.getServerName());
    headerBuilder.add(HttpHeadersKey.SERVER_TERMINAL, properties.getServerTerminal());
    headerBuilder.add(HttpHeadersKey.OPERATION_TYPE, properties.getOpnType());
    headerBuilder.add(HttpHeadersKey.SERVER_IP, properties.getServerIp());
    headerBuilder.add(HttpHeadersKey.OPERATION_NUMBER, GenericUtil.generateOperationNumber());
    when(request.headers().newBuilder()).thenReturn(headerBuilder);
    interceptor.intercept(chain);
    Assert.assertNotNull(interceptor);
  }

  @Test
  public void headerTest() {
    CustomHeaders headers = new CustomHeaders();
    headers.setHeader(TestMapsUtils.getLoanHeaderMap());
    Assert.assertNotNull(headers.getHeader());
  }

}
