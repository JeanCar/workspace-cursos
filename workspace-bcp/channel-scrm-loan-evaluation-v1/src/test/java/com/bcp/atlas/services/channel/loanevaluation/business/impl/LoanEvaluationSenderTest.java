package com.bcp.atlas.services.channel.loanevaluation.business.impl;

import static org.mockito.Mockito.when;
import java.util.List;
import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetEvaluationQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.EvaluationsApi;
import com.bcp.atlas.services.channel.loanevaluation.proxy.atlas.LoanEvaluationApi;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;

@RunWith(MockitoJUnitRunner.class)
public class LoanEvaluationSenderTest {


  @InjectMocks
  private LoanEvaluationSender sender;

  @InjectMocks
  private TestUtilsEvaluation test;

  @Mock
  private LoanEvaluationApi loanEvaluationApi;

  @Mock
  private EvaluationsApi evaluationsApi;

  @Before
  public void setUp() throws Exception {}

  @Test
  public void sendAndReceive_01() throws Exception {

    String jsonLoan = TestConstants.CROSS_CDA_REQUEST;
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationRequest loanEvaluationRequest = test.getLoanEvaluationRequest(jsonLoan);
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);

    when(loanEvaluationApi.creditEvaluationUsingPOST3(loanEvaluationRequest))
        .thenReturn(Single.just(loanEvaluationResponse));
    
    TestObserver<LoanEvaluationResponse> testSingleResult =
        sender.callLoanEvaluation(loanEvaluationRequest).test();
    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();
    
  }

  @Test
  public void sendAndReceive_02() throws Exception {

    String personId = "100195246000";
    boolean active = Boolean.TRUE;
    String jsonLoanRes = TestConstants.CROSS_UNDERWRITING;
    List<EvaluationElement> evaluationElementResponse = test.getEvaluationElement(jsonLoanRes);
    GetEvaluationQueryParam evaluationParam = new GetEvaluationQueryParam();
    evaluationParam.setPersonId(personId);
    evaluationParam.setIsActive(active);
    when(evaluationsApi.getEvaluationsUsingGET3(evaluationParam.getPersonId(),
        evaluationParam.getIsActive())).thenReturn(Single.just(evaluationElementResponse));
    TestObserver<List<EvaluationElement>> testSingleResult =
        sender.callEvaluation(evaluationParam).test();

    testSingleResult.awaitTerminalEvent();
    testSingleResult.assertNoErrors();

  }
}
