package com.bcp.atlas.services.channel.loanevaluation.model.api.get.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetEvaluationQueryParamTest {

  private GetEvaluationQueryParam getEvaluationQueryParam;

  @Before
  public void setUp() {
    getEvaluationQueryParam = new GetEvaluationQueryParam();
  }

  @Test
  public void createQueryParamTest() {
    getEvaluationQueryParam.setPersonId("100195246000");
    getEvaluationQueryParam.setIsActive(true);
    Assert.assertNotNull(getEvaluationQueryParam.getPersonId());
    Assert.assertNotNull(getEvaluationQueryParam.getIsActive());
  }
}
