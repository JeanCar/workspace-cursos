package com.bcp.atlas.services.channel.bdd.features;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import com.bcp.atlas.services.channel.bdd.AbstractBaseStep;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Qualifier;


@Slf4j
public class LoanEvaluationSteps04 {

  private int statusCode;
  private List<String> elementos;
  @Qualifier("AbstractBaseStep")
  AbstractBaseStep abs;

  public LoanEvaluationSteps04(AbstractBaseStep abs) {
    this.abs = abs;
  }

  @When("^Se usa el RECURSO04 (.*) enviandole un header (.*) y request (.*)$")
  public void cuandoSteps04(String resource, String headerFile, String requestFile)
      throws Throwable {
    String requestHeader = TestUtilsEvaluation.fileResourceToString(headerFile);
    Map<String, String> head = TestUtilsEvaluation.getFileToMap(requestHeader);
    String requestBody = TestUtilsEvaluation.fileResourceToString(requestFile);
    Response res = null;
    String url = abs.getBasePath() + abs.getUri() + resource;
    log.info("URL: {}", url);
    res = given().contentType(Constants.APPLICATION_JSON).headers(head).auth()
        .oauth2(abs.getToken()).body(requestBody).post(url);

    statusCode = res.getStatusCode();
    JSONObject jsonObj = new JSONObject(res.getBody().asString());
    JSONArray array = jsonObj.getJSONArray("exceptionDetails");
    elementos = IntStream.range(0, array.length()).mapToObj(i -> array.getJSONObject(i))
        .map(e -> e.getString("description")).collect(Collectors.toList());

    log.info("RESPUESTA: {}", res.getBody().asString());
    log.info("STATUSCODE: {}", statusCode);
    log.info("ELEMENTO: {}", elementos);
  }

  @Then("^El RESPONSE04 muestra el estado (.*), de acuerdo a las validaciones mencionadas (.*)")
  public void entoncesSteps04(int codigo, String descripcion) throws Throwable {
    Assert.assertThat(statusCode, is(codigo));
    String requestDesc = TestUtilsEvaluation.fileResourceToString(descripcion);
    Map<String, String> desc = TestUtilsEvaluation.getFileToMap(requestDesc);
    log.info("map: " + desc);
    Assert.assertTrue(elementos.stream().anyMatch(e -> {
      return desc.values().stream().anyMatch(entry -> e.contains(entry));
    }));
  }
}
