
package com.bcp.atlas.services.channel.loanevaluation.util;


import com.bcp.atlas.services.channel.loanevaluation.evaluation.model.thirdparty.EvaluationElement;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.GetResponse;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostRequest;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.ArchivoNegativoWrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import com.bcp.atlas.services.channel.util.LoanEvaluationUtilTest.EvaluationResponse;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;



public class TestUtilsEvaluation {

  ClassLoader classLoader = getClass().getClassLoader();
  Gson gson = new Gson();

  public PostLoanEvaluationsUxRequest getloanEvaluationUxRequest(String jsonLoanReq)
      throws Exception {
    PostLoanEvaluationsUxRequest loanEvaluationUxRequest = new PostLoanEvaluationsUxRequest();
    String request =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanReq), Constants.UTF_8);
    loanEvaluationUxRequest = gson.fromJson(request, PostLoanEvaluationsUxRequest.class);
    return loanEvaluationUxRequest;
  }

  public PostExceptionOrderUxRequest getloanExceptionOrderUxRequestPost(String jsonLoanReq)
      throws Exception {
    PostExceptionOrderUxRequest loanExceptionOrderUxRequest = new PostExceptionOrderUxRequest();
    String request =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanReq), Constants.UTF_8);
    loanExceptionOrderUxRequest = gson.fromJson(request, PostExceptionOrderUxRequest.class);
    return loanExceptionOrderUxRequest;
  }

  public PostLoanEvaluationsUxResponse getloanEvaluationUxResponse(String jsonLoanRes)
      throws Exception {
    PostLoanEvaluationsUxResponse loanEvaluationUxResponse = new PostLoanEvaluationsUxResponse();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    loanEvaluationUxResponse = gson.fromJson(response, PostLoanEvaluationsUxResponse.class);
    return loanEvaluationUxResponse;
  }

  public GetExceptionOrderUxResponse getloanExceptionOrderUxResponseGet(String jsonLoanReq)
      throws Exception {
    GetExceptionOrderUxResponse loanExceptionOrderUxResponseGet = new GetExceptionOrderUxResponse();
    String request =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanReq), Constants.UTF_8);
    loanExceptionOrderUxResponseGet = gson.fromJson(request, GetExceptionOrderUxResponse.class);
    return loanExceptionOrderUxResponseGet;
  }

  public PostExceptionOrderUxResponse getloanExceptionOrderUxResponsePost(String jsonLoanRes)
      throws Exception {
    PostExceptionOrderUxResponse loanExceptionOrderUxResponsePost =
        new PostExceptionOrderUxResponse();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    loanExceptionOrderUxResponsePost = gson.fromJson(response, PostExceptionOrderUxResponse.class);
    return loanExceptionOrderUxResponsePost;
  }

  public LoanEvaluationRequest getLoanEvaluationRequest(String jsonLoanReq) throws Exception {
    LoanEvaluationRequest loanEvaluationRequest = new LoanEvaluationRequest();
    String request =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanReq), Constants.UTF_8);
    loanEvaluationRequest = gson.fromJson(request, LoanEvaluationRequest.class);
    return loanEvaluationRequest;
  }

  public List<EvaluationElement> getEvaluationElement(String jsonLoanRes) throws Exception {
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    ObjectMapper mapper = new ObjectMapper();
    return Arrays.asList(mapper.readValue(response, EvaluationElement[].class));
  }

  public List<GetEvaluationResponse> getEvaluationResponse(String jsonLoanRes) throws Exception {
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    ObjectMapper mapper = new ObjectMapper();
    return Arrays.asList(mapper.readValue(response, GetEvaluationResponse[].class));
  }

  public List<EvaluationResponse> getEvaluationResponseBackup(String jsonLoanRes) throws Exception {
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    ObjectMapper mapper = new ObjectMapper();
    return Arrays.asList(mapper.readValue(response, EvaluationResponse[].class));

  }


  public LoanEvaluationResponse getLoanEvaluationResponse(String jsonLoanRes) throws Exception {
    LoanEvaluationResponse loanEvaluationResponse = new LoanEvaluationResponse();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    loanEvaluationResponse = gson.fromJson(response, LoanEvaluationResponse.class);
    return loanEvaluationResponse;
  }

  public Wrapper getWrapper(String jsonLoanRes) throws Exception {
    Wrapper wrapper = new Wrapper();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    wrapper = gson.fromJson(response, Wrapper.class);
    return wrapper;
  }

  public ArchivoNegativoWrapper getArchivoNegativoWrapper(String jsonLoanRes) throws Exception {
    ArchivoNegativoWrapper wrapper = new ArchivoNegativoWrapper();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    wrapper = gson.fromJson(response, ArchivoNegativoWrapper.class);
    return wrapper;
  }

  public GetResponse getLoanExceptionOrderResponseGet(String jsonLoanRes) throws Exception {
    GetResponse getResponse = new GetResponse();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    getResponse = gson.fromJson(response, GetResponse.class);
    return getResponse;
  }

  public PostRequest getLoanExceptionOrderRequestPost(String jsonLoanReq) throws Exception {
    PostRequest postRequest = new PostRequest();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanReq), Constants.UTF_8);
    postRequest = gson.fromJson(response, PostRequest.class);
    return postRequest;
  }

  public PostResponse getLoanExceptionOrderResponsePost(String jsonLoanRes) throws Exception {
    PostResponse postResponse = new PostResponse();
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    postResponse = gson.fromJson(response, PostResponse.class);
    return postResponse;
  }

  public List<EvaluationElement> getEvaluationElementList(String jsonLoanRes) throws Exception {
    String response =
        IOUtils.toString(classLoader.getResourceAsStream(jsonLoanRes), Constants.UTF_8);
    ObjectMapper mapper = new ObjectMapper();
    return Arrays.asList(mapper.readValue(response, EvaluationElement[].class));

  }

  public static Map<String, String> getFileToMap(String requestHeader) {
    Map<String, String> headers = new HashMap<>();
    headers = Arrays.stream(requestHeader.split("\\r?\\n"))
        .map(s -> s.replaceFirst(":", " : ").replaceAll("\\s+", " ").split(" : "))
        .collect(Collectors.toMap(s -> s[0], s -> s[1]));
    return headers;
  }

  public static String fileResourceToString(String resourcePath) throws Exception {
    return new String(Files.readAllBytes(Paths.get(Resources.getResource(resourcePath).toURI())),
        StandardCharsets.UTF_8);
  }


}
