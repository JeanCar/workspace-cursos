package com.bcp.atlas.services.channel.util;

import java.text.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GenericUtilTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  public void utilTest_01() throws ParseException {
    Assert.assertNotNull(GenericUtil.generateOperationNumber());
    Assert.assertNotNull(GenericUtil.convertStrToInt("1"));
    Assert.assertNotNull(GenericUtil.convertStrToInt(null));
    Assert.assertNotNull(GenericUtil.convertStrToInt(""));
  }

}
