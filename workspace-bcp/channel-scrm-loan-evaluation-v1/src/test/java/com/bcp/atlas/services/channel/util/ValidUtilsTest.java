package com.bcp.atlas.services.channel.util;

import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidUtilsTest {

  @InjectMocks
  private ValidUtils validUtils;

  @InjectMocks
  TestUtilsEvaluation test;

  @Before
  public void setUp() {}

  @Test
  public void validateUtils_01() throws Exception {
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);
    validUtils.validate(loanEvaluationResponse, loanEvaluationResponse.getClass(),
        TestConstants.PAQUETE_API);
    Assert.assertNotNull(validUtils);
  }

  @Test
  public void validateUtils_02() throws Exception {
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_NULL;
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);
    validUtils.validate(loanEvaluationResponse, loanEvaluationResponse.getClass(),
        TestConstants.PAQUETE_API);
    Assert.assertNotNull(validUtils);
  }
}