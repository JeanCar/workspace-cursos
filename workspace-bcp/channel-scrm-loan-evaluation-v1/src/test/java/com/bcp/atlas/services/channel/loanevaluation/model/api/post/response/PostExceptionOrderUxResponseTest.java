package com.bcp.atlas.services.channel.loanevaluation.model.api.post.response;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostExceptionOrderUxResponseTest {

  private PostExceptionOrderUxResponse loanExceptionOrderUxResponsePost;

  @Before
  public void setUp() {
    loanExceptionOrderUxResponsePost = new PostExceptionOrderUxResponse();
  }


  @Test
  public void createResponsePostTest_01() {
    List<PostEvaluationResultsResponse> evaluationResults = new ArrayList<>();
    PostEvaluationResultsResponse evaluation = new PostEvaluationResultsResponse();
    evaluation.setAutonomyLevel(1);
    List<PostAdditionalFieldsResponse> additionalFields = new ArrayList<>();
    PostAdditionalFieldsResponse addition = new PostAdditionalFieldsResponse();
    addition.setName("segmentacion_riesgo");
    addition.setValue("E");
    additionalFields.add(addition);
    evaluation.setAdditionalFields(additionalFields);
    evaluationResults.add(evaluation);
    loanExceptionOrderUxResponsePost.setEvaluationResults(evaluationResults);
    Assert.assertNotNull(loanExceptionOrderUxResponsePost);
    Assert.assertNotNull(loanExceptionOrderUxResponsePost.toString());
  }
}
