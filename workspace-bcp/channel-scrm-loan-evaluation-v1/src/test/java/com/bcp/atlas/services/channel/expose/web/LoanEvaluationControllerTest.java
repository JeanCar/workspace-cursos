package com.bcp.atlas.services.channel.expose.web;

import static org.mockito.Mockito.when;
import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.core.utils.PropertyUtils;
import com.bcp.atlas.services.channel.loanevaluation.business.EvaluateExceptionOrderService;
import com.bcp.atlas.services.channel.loanevaluation.business.LoanEvaluationService;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.request.GetExceptionOrderQueryParam;
import com.bcp.atlas.services.channel.loanevaluation.model.api.get.response.GetExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostExceptionOrderUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostExceptionOrderUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.response.PostLoanEvaluationsUxResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.env.MockEnvironment;


@RunWith(MockitoJUnitRunner.class)
public class LoanEvaluationControllerTest {

  @InjectMocks
  private LoanEvaluationController loanEvaluationController;

  @Mock
  private LoanEvaluationService loanEvaluationService;

  @Mock
  private EvaluateExceptionOrderService loanExceptionOrderService;

  @InjectMocks
  private TestUtilsEvaluation test;

  private Map<String, String> querysEmpty;
  private Map<String, String> querys;

  @Before
  public void setUp() {
    PropertyUtils.setResolver(new MockEnvironment());
    querys = new HashMap<>();
    querysEmpty = new HashMap<>();
    querys.put("quey", "1=1");
  }

  @Test
  public void loanEvaluationTest() throws Exception {

    String jsonRq = TestConstants.UX_REQUEST;
    String jsonRes = TestConstants.UX_RESPONSE_CREDITICIA_AN_DECLINED;
    PostLoanEvaluationsUxRequest loanEvaluationUxRequest = test.getloanEvaluationUxRequest(jsonRq);
    PostLoanEvaluationsUxResponse loanEvaluationUxResponse =
        test.getloanEvaluationUxResponse(jsonRes);

    when(loanEvaluationService.loanEvaluations(TestMapsUtils.getLoanEvaluationUxHeader(),
        loanEvaluationUxRequest)).thenReturn(Single.just(loanEvaluationUxResponse));

    loanEvaluationController.loanEvaluations(TestMapsUtils.getLoanEvaluationUxHeader(),
        loanEvaluationUxRequest, querysEmpty);

    TestObserver<PostLoanEvaluationsUxResponse> observer =
        loanEvaluationController.loanEvaluations(TestMapsUtils.getLoanEvaluationUxHeader(),
            loanEvaluationUxRequest, querysEmpty).test();
    observer.awaitTerminalEvent();
    observer.assertComplete();

  }

  @Test
  public void loanEvaluationEvaluateGetTest() throws Exception {
    String jsonRes = TestConstants.EVALUATE_UX_RESPONSE_GET;
    GetExceptionOrderUxResponse loanExceptionOrderUxResponseGet =
        test.getloanExceptionOrderUxResponseGet(jsonRes);

    GetExceptionOrderQueryParam loanExceptionOrderQueryParam = new GetExceptionOrderQueryParam();
    loanExceptionOrderQueryParam.setExceptionId(TestConstants.EXCEPTION_ID);

    when(loanExceptionOrderService.loanEvaluationsEvaluateGet(
        TestMapsUtils.getLoanEvaluationUxHeader(), loanExceptionOrderQueryParam))
            .thenReturn(Maybe.just(loanExceptionOrderUxResponseGet));

    loanEvaluationController.loanEvaluationsEvaluateGet(
        TestMapsUtils.getLoanEvaluationUxHeader(), loanExceptionOrderQueryParam);

    TestObserver<GetExceptionOrderUxResponse> observer =
        loanEvaluationController.loanEvaluationsEvaluateGet(
            TestMapsUtils.getLoanEvaluationUxHeader(), loanExceptionOrderQueryParam).test();
    observer.awaitTerminalEvent();
    observer.assertComplete();

  }

  @Test
  public void loanEvaluationEvaluatePostTest() throws Exception {
    String jsonRq = TestConstants.EVALUATE_UX_REQUEST_POST;
    String jsonRes = TestConstants.EVALUATE_UX_RESPONSE_POST;

    PostExceptionOrderUxRequest loanExceptionOrderUxRequest =
        test.getloanExceptionOrderUxRequestPost(jsonRq);

    PostExceptionOrderUxResponse loanExceptionOrderUxResponsePost =
        test.getloanExceptionOrderUxResponsePost(jsonRes);

    when(loanExceptionOrderService.loanEvaluationsEvaluatePost(
        TestMapsUtils.getLoanEvaluationUxHeader(), loanExceptionOrderUxRequest))
            .thenReturn(Single.just(loanExceptionOrderUxResponsePost));

    loanEvaluationController.loanEvaluationsEvaluatePost(
        TestMapsUtils.getLoanEvaluationUxHeader(), loanExceptionOrderUxRequest, querysEmpty);
  }

  @Test
  public void testLoanEvaluationPostWithQuery() throws Exception {
    String jsonRq = TestConstants.UX_REQUEST;

    PostLoanEvaluationsUxRequest loanExceptionOrderUxRequest =
        test.getloanEvaluationUxRequest(jsonRq);

    TestObserver<PostLoanEvaluationsUxResponse> observer =
        loanEvaluationController.loanEvaluations(TestMapsUtils.getLoanEvaluationUxHeader(),
            loanExceptionOrderUxRequest, querys).test();
    observer.awaitTerminalEvent();
    observer.assertNotComplete();
    observer.assertError(AtlasException.class);
  }

  @Test
  public void testLoanEvaluationEvaluatePostWithQuery() throws Exception {
    String jsonRq = TestConstants.EVALUATE_UX_REQUEST_POST;

    PostExceptionOrderUxRequest loanExceptionOrderUxRequest =
        test.getloanExceptionOrderUxRequestPost(jsonRq);

    TestObserver<PostExceptionOrderUxResponse> observer = loanEvaluationController
        .loanEvaluationsEvaluatePost(TestMapsUtils.getLoanEvaluationUxHeader(),
            loanExceptionOrderUxRequest, querys)
        .test();
    observer.awaitTerminalEvent();
    observer.assertNotComplete();
    observer.assertError(AtlasException.class);
  }
}
