package com.bcp.atlas.services.channel.util;

import com.bcp.atlas.core.exception.AtlasException;
import com.bcp.atlas.services.channel.config.ApplicationProperties;
import com.bcp.atlas.services.channel.config.IncomesProperties;
import com.bcp.atlas.services.channel.loanevaluation.exception.model.thirdparty.PostResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.ModulesResponse;
import com.bcp.atlas.services.channel.loanevaluation.model.api.bean.Wrapper;
import com.bcp.atlas.services.channel.loanevaluation.model.api.post.request.PostLoanEvaluationsUxRequest;
import com.bcp.atlas.services.channel.loanevaluation.model.thirdparty.LoanEvaluationResponse;
import com.bcp.atlas.services.channel.loanevaluation.util.constants.TestConstants;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import com.bcp.atlas.services.channel.loanevaluation.util.TestUtilsEvaluation;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LoanEvaluationUtilTest {

  @InjectMocks
  private TestUtilsEvaluation test;

  @InjectMocks
  private LoanEvaluationUtil loanEvaluationUtil;

  @Mock
  private ApplicationProperties properties;

  @Mock
  private InitModulesUtil initModulesUtil;

  @Mock
  private IncomesProperties incomesProperties;

  @Before
  public void setUp() throws Exception {

    loanEvaluationUtil = new LoanEvaluationUtil(properties, incomesProperties, initModulesUtil);
  }

  @Test
  public void initModulesTest() throws Exception {
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);
    loanEvaluationUtil.initModules(loanEvaluationResponse);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void evaluateStatusModulesCrediticiaTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.evaluateStatusModulesCrediticia(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void evaluateCountModulesCrediticiaTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.evaluateCountModulesCrediticia(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void evaluateAnStatusDeclinedTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.evaluateAnStatusDeclined(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }


  @Test
  public void evaluateSbsStatusDeclinedTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.evaluateSbsStatusDeclined(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void evaluateCemStatusDeclinedTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_SBS;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.evaluateCemStatusDeclined(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }


  @Test
  public void capturePersonIdTest() throws Exception {
    String jsonReq = TestConstants.UX_REQUEST;
    PostLoanEvaluationsUxRequest request = test.getloanEvaluationUxRequest(jsonReq);
    loanEvaluationUtil.capturePersonId(request);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void validateFilterAnTest() throws Exception {
    String jsonUnderWriting = TestConstants.CROSS_GET_RESPONSE;
    List<EvaluationResponse> evaluationList = test.getEvaluationResponseBackup(jsonUnderWriting);
    loanEvaluationUtil.validateFilterAn(TestMapsUtils.parseArchivoNegativo(evaluationList));
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void calculateCemTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.calculateCem(wrapper, "0.02");
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void calculateDeudaClienteTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.calculateDeudaCliente(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void calculateCdeTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.calculateCde(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void factorCdeTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.factorCde(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void ingresoCalculadoBciTest() throws Exception {
    String jsonWrapper = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper wrapper = test.getWrapper(jsonWrapper);
    loanEvaluationUtil.ingresoCalculadoBci(wrapper);
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void calculateMaxTitularMountTest() throws Exception {
    String jsonRq = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper request = test.getWrapper(jsonRq);
    loanEvaluationUtil.calculateMaxTitularMount(request.getLoanEvaluationResponse());
  }

  @Test
  public void calculateMaxConyugeMountTest() throws Exception {
    String jsonLoanRes = TestConstants.CROSS_CDA_RESPONSE_DECLINE;
    LoanEvaluationResponse loanEvaluationResponse = test.getLoanEvaluationResponse(jsonLoanRes);
    loanEvaluationUtil.calculateMaxConyugeMount(loanEvaluationResponse);
    Assert.assertNotNull(loanEvaluationUtil);
  }


  @Test
  public void captureSegmentTitularTest() throws Exception {
    String jsonRq = TestConstants.WRAPPER_CREDITICIA_AN_DECLINED;
    Wrapper request = test.getWrapper(jsonRq);
    loanEvaluationUtil.captureSegmentTitular(request.getLoanEvaluationResponse());
    Assert.assertNotNull(loanEvaluationUtil);
  }

  @Test
  public void captureRiskSegmentTest() {
    Assert.assertNotNull(loanEvaluationUtil.captureRiskSegment("A"));
    Assert.assertNotNull(loanEvaluationUtil.captureRiskSegment("B"));
    Assert.assertNotNull(loanEvaluationUtil.captureRiskSegment("C"));
    Assert.assertNotNull(loanEvaluationUtil.captureRiskSegment("D"));
    Assert.assertNotNull(loanEvaluationUtil.captureRiskSegment("E"));
  }


  @Test
  public void modulesDeclineTest() {
    Assert.assertNotNull(loanEvaluationUtil.modulesDecline("3"));
    Assert.assertNotNull(loanEvaluationUtil.modulesDecline("5"));
    Assert.assertNotNull(loanEvaluationUtil.modulesDecline("4"));
    Assert.assertNotNull(loanEvaluationUtil.modulesDecline("6"));
  }


  @Test
  public void modulesOtherDeclineTest() {
    Assert.assertNotNull(loanEvaluationUtil.modulesOtherDecline("3"));
    Assert.assertNotNull(loanEvaluationUtil.modulesOtherDecline("1"));
    Assert.assertNotNull(loanEvaluationUtil.modulesOtherDecline("0"));
    Assert.assertNotNull(loanEvaluationUtil.modulesOtherDecline("2"));
    Assert.assertNotNull(loanEvaluationUtil.modulesOtherDecline("5"));
  }

  @Test
  public void moduleGeneralDeclineTest() {
    Assert.assertNotNull(loanEvaluationUtil.moduleGeneralDecline("5"));
    Assert.assertNotNull(loanEvaluationUtil.moduleGeneralDecline("D"));
    Assert.assertNotNull(loanEvaluationUtil.moduleGeneralDecline("2"));
  }

  @Test
  public void mapReturnTest01() throws Exception {
    String jsonArchivo = TestConstants.EXCEPTION_ARCHIVO_NEGATIVO;
    PostResponse archivo = test.getLoanExceptionOrderResponsePost(jsonArchivo);
    loanEvaluationUtil.mapReturn(archivo);
    Assert.assertNotNull(loanEvaluationUtil);
  }


  @Test
  public void mapReturnTest02() throws Exception {
    ModulesResponse res = new ModulesResponse();
    res.setCodeError("TL0003");
    res.setMessageError("Los datos propocionados incorrectos");
    loanEvaluationUtil.mapReturn(res);
    Assert.assertNotNull(loanEvaluationUtil);
  }


  @Test
  public void errorReturnTest() {
    Throwable ex = AtlasException.builder().addDetail().withCode("TL9999").withDescription("Error")
        .push().build();
    Assert.assertNotNull(loanEvaluationUtil.errorReturn(ex));

  }

  @Data
  @Getter
  @Setter
  public static class EvaluationResponse {
    private String statusCode;
    private List<RecordResponse> records;
  }

  @Data
  @Getter
  @Setter
  public static class RecordResponse {
    private String registerDate;
    private BigDecimal debtsAmount;
    private String reasonCode;

  }
}
