package com.bcp.atlas.services.channel.config;

import com.bcp.atlas.services.channel.config.IncomesProperties.Income;
import com.bcp.atlas.services.channel.loanevaluation.util.TestMapsUtils;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;



@RunWith(MockitoJUnitRunner.class)
public class IncomesPropertiesTest {

  @InjectMocks
  private IncomesProperties incomesProperties;

  @Mock
  private ApplicationProperties properties;

  @Test
  public void createObjectTest() {
    Income income = new Income();
    income.setMax(BigDecimal.TEN);
    income.setMin(BigDecimal.ZERO);
    
    income.setSegments(TestMapsUtils.getSegment());
    Assert.assertNotNull(income);
    incomesProperties.setIncomes(TestMapsUtils.getListaIncome());
    Assert.assertNotNull(income.getMax());
    Assert.assertNotNull(income.getMin());
    Assert.assertNotNull(income.getSegments());
    Assert.assertNotNull(income.toString());
    Assert.assertNotNull(incomesProperties.getIncomes());
    Assert.assertNotNull(incomesProperties.toString());

  }
}
