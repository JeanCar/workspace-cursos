package com.bcp.atlas.services.channel.loanevaluation.model.api.header;

import com.bcp.atlas.services.channel.loanevaluation.util.constants.Constants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LoanEvaluationUxHeaderTest {

  private LoanEvaluationUxHeader loanEvaluationUxHeader;

  @Before
  public void setUp() {
    loanEvaluationUxHeader = new LoanEvaluationUxHeader();
  }

  @Test
  public void createObjectTest() {
    loanEvaluationUxHeader.setRequestId(Constants.REQUEST_ID);
    loanEvaluationUxHeader.setRequestDate(Constants.REQUEST_DATE);
    loanEvaluationUxHeader.setUserCode(Constants.USER_CODE);
    loanEvaluationUxHeader.setCallerName(Constants.CALLER_NAME);
    loanEvaluationUxHeader.setApplicationCode(Constants.APP_CODE);
    Assert.assertNotNull(loanEvaluationUxHeader.getRequestDate());
    Assert.assertNotNull(loanEvaluationUxHeader.getRequestId());
    Assert.assertNotNull(loanEvaluationUxHeader.getUserCode());
    Assert.assertNotNull(loanEvaluationUxHeader.getCallerName());
    Assert.assertNotNull(loanEvaluationUxHeader.getApplicationCode());
    Assert.assertNotNull(loanEvaluationUxHeader.toString());
  }

}
