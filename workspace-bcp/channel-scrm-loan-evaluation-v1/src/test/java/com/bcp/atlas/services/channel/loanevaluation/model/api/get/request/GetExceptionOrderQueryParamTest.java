package com.bcp.atlas.services.channel.loanevaluation.model.api.get.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetExceptionOrderQueryParamTest {

  private GetExceptionOrderQueryParam loanExceptionOrderQueryParam;

  @Before
  public void setUp() {
    loanExceptionOrderQueryParam = new GetExceptionOrderQueryParam();
  }

  @Test
  public void createQueryParamTest() {
    loanExceptionOrderQueryParam.setExceptionId(1);
    Assert.assertNotNull(loanExceptionOrderQueryParam.getExceptionId());
    Assert.assertNotNull(loanExceptionOrderQueryParam.toString());
  }
}
