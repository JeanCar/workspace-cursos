#Author: wildervasquezc@bcp.com.pe
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
@loan-evaluations-evaluate
Feature: Test BDD, Loan Evaluation

    Background:
    Given Que se realiza un POST a la URI "/channel/scrm/loan-evaluation/v1"
    And Se tiene autorizacion usando el APIKEY bdd/loan-evaluations/token.txt

    @Steps05
    Scenario Outline: Consulta los datos requeridos para la solicitud de excepción de una evaluación
    When Se usa el SUBRECURSO05 /loan-evaluations/evaluate/ enviandole un header <headers> y request <body>
    Then El RESPONSE05 muestra el estado <code>, retornando con campos validos

    Examples:
    | headers                                    | body                                                     |code |
    |bdd/loan-evaluations/evaluate/headers-ok.txt|bdd/loan-evaluations/evaluate/evaluate-ux-request-get.json|200  | 
   
    @Steps06
    Scenario Outline: Evalua la excepción y/o valores de una evaluación por lo que internamente invoca al siguiente servicio
    When Se usa el SUBRECURSO06 /loan-evaluations/evaluate/ enviandole un header <headers> y request <body>
    Then El RESPONSE06 muestra el estado <code>, retornando con campos validos

    Examples:
    | headers                                    | body                                                      |code |
    |bdd/loan-evaluations/evaluate/headers-ok.txt|bdd/loan-evaluations/evaluate/evaluate-ux-request-post.json|200  | 
   

	@Steps07
 	Scenario Outline: Valida los valores requeridos del header enviado a evaluation-exception-order
    When Se usa el SUBRECURSO07 /loan-evaluations/evaluate/ enviandole un header <headers> y request <body>
    Then El RESPONSE07 muestra el estado <code>, indicando que los valores del header son validos
    
    Examples:
    | headers                                       | body                                                      |code|
    |bdd/loan-evaluations/evaluate/headers-error.txt|bdd/loan-evaluations/evaluate/evaluate-ux-request-post.json|400 |
      
    @Steps08
 	Scenario Outline: Valida la taxonomía de la URI GET
    When Se usa el SUBRECURSO08 <subrecurso> enviandole un header <header>
    Then El RESPONSE08 muestra el estado <code>, indicando que no se encontro el recurso

    Examples:
    | header                                     |       subrecurso                         |code  |
    |bdd/loan-evaluations/evaluate/headers-ok.txt|/loan-evaluations/evaluates?exception_id=1| 404  |
    |bdd/loan-evaluations/evaluate/headers-ok.txt|/loan-evaluations/evaluate?exception_id=1 | 400  |
    |bdd/loan-evaluations/evaluate/headers-ok.txt|/loan-evaluations/evaluate?exceptionId=1  | 200  |

	