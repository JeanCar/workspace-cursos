#Author: wildervasquezc@bcp.com.pe
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
@loan-evaluations
Feature: Test BDD, Loan Evaluation
    
    Background:
    Given Que se realiza un POST a la URI "/channel/scrm/loan-evaluation/v1"
    And Se tiene autorizacion usando el APIKEY bdd/loan-evaluations/token.txt
    
    @Steps01
    Scenario Outline: Steps01 Realiza la evaluación de crédito efectivo regular de una persona natural
    When Se usa el RECURSO01 /loan-evaluations/ enviandole un header <headers> y request <body>
    Then El RESPONSE01 muestra el estado <code>, retornando con campos validos

      Examples:
      | headers                           | body                                               |code |
      |bdd/loan-evaluations/headers-ok.txt|bdd/loan-evaluations/loan-evaluation-ux-request.json|200  | 
   
   
	@Steps02
 	Scenario Outline: Steps02 Validar los valores requeridos en el header
    When Se usa el RECURSO02 /loan-evaluations/ enviandole un header <headers> y request <body>
    Then El RESPONSE02 muestra el estado <code>, indicando que los valores del header son validos
    
      Examples:
      |     headers                          | body                                               |code|
      |bdd/loan-evaluations/headers-error.txt|bdd/loan-evaluations/loan-evaluation-ux-request.json|400 |

      
    @Steps03
 	Scenario Outline: Steps03 Validar la taxonomía de la URI 
    When Se usa el RECURSO03 <recurso> enviandole un header bdd/loan-evaluations/headers-ok.txt y request bdd/loan-evaluations/loan-evaluation-ux-request.json
    Then El RESPONSE03 muestra el estado <code>, indicando que no se encontro el recurso

    Examples:
      |     recurso    |   code |
      |/loan-evaluation|   404  |
      |/loanevaluations|   404  |
      |/loan evaluation|   404  |  

	
	@Steps04
    Scenario Outline: Steps04 Validar los campos del payload de acuerdo a la lista de restricciones 
    When Se usa el RECURSO04 /loan-evaluations/ enviandole un header bdd/loan-evaluations/headers-ok.txt y request <body>
    Then El RESPONSE04 muestra el estado <code>, de acuerdo a las validaciones mencionadas <validation>
        
    Examples:
    |body                                                      |code|      validation                     |
    |bdd/loan-evaluations/loan-evaluation-ux-request-error.json|400 |bdd/loan-evaluations/descriptions.txt|