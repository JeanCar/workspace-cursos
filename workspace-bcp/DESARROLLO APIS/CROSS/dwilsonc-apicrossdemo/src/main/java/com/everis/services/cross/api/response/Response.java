package com.everis.services.cross.api.response;

import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(value = Include.NON_NULL)
@Getter
@Setter
@ApiModel(
    description = "Contiene datos de Loan")
public class Response {

  @ApiModelProperty(name = "corporateLoanId", value = "Código del crédito Consist.",
      dataType = "String", example = "D47500049076")
  @Valid
  private String corporateLoanId;
  
  private DebtType debtType;
  
  private Product productDetail;
}
