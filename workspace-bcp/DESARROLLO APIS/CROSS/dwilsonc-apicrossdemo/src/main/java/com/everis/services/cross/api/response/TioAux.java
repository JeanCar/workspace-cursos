package com.everis.services.cross.api.response;

import java.io.Serializable;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
    description = "Contiene el TioAux codigo y descripcion.")
@JsonInclude(
    value = Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class TioAux implements Serializable {

  private static final long serialVersionUID = 893994203187873947L;

  @ApiModelProperty(
      name = "code",
      value = "Código de producto.",
      dataType = "String",
      example = "PRLPRE")
  @Valid
  private String code;

}
