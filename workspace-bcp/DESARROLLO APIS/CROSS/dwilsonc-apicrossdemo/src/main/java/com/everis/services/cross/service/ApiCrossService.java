package com.everis.services.cross.service;

import com.everis.services.cross.api.response.Response;
import io.reactivex.Single;

public interface ApiCrossService {

  public Single<Response> obtenerRespuestaCorporateLoans(String corporateLoanId); 
}
