node {
    def mvnHome
    
    stage('Preparacionv1') {
        try {
            git 'https://dwilsonc@bitbucket.org/dwilsonc/apicrossdemo.git'
            mvnHome = tool 'M2'
        } catch (e) {
            notifyStarted("Error al clonar proyecto api cross demo de Bitbucket!")
            throw e
        } 
    }
    
    stage('Prueba') {
        try {
            sh "'${mvnHome}/bin/mvn' test"
        } catch (e) {
            notifyStarted("Tests Failed in Jenkins!")
            throw e
        } 
    }
    stage('Construccion') {
        try {
            sh "'${mvnHome}/bin/mvn' clean package -DskipTests"
        }catch (e) {
            notifyStarted("Build Failed in Jenkins!")
            throw e
        } 
    }
    
    stage('Resultados') {
        try{
            archive 'target/*.jar'
        }catch (e) {
            notifyStarted("Packaging Failed in Jenkins!")
            throw e
        } 
    }

    stage('Despliegue') {
        try{
            sh '/var/lib/jenkins/workspace/ApiCross/runDeployment.sh'
        }catch (e) {
            notifyStarted("Deployment Failed in Jenkins!")
            throw e
        } 
    }
	
	stage('Enviando Correo!'){
        try{
         mail (to: 'dmwc007@gmail.com',
             subject: "Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) is waiting for input",
             body: "Please go to ${env.BUILD_URL}.");
        }catch (e) {
            notifyStarted("Envio fallido de correo!")
            throw e
        } 
   }
    
    notifyStarted("Tu c�digo ha sido testado y desplegado con �xito!!.")
}

def notifyStarted(String message) {
  slackSend (color: '#FFFF00', message: "${message}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
}