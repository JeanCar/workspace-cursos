package com.everis.services.ux.config;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ToString
public class ApplicationProperties {

  @NotNull
  @Value("${api-cross}")
  private String baseUrl;
}
