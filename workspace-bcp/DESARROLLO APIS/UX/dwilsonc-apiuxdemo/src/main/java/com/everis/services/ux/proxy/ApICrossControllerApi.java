package com.everis.services.ux.proxy;

import com.everis.services.ux.thirdparty.Response;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;


public interface ApICrossControllerApi {
  /**
   * Consulta gen�rica de colocaciones por cliente (Cr�ditos Consist). Permite realizar consultas por c�digo de la l�nea de cr�dito. Los datos de salida incluyen los principales campos asociados al desembolso.
   * classpath:swagger/notes/apicross.md
   * @param id id (required)
   * @return Call&lt;Response&gt;
   */
  @Headers({
    "Content-Type:application/json;charset&#x3D;UTF-8"
  })
  @GET("api/cross/v1/obtenerDat2os/{id}")
  Single<Response> obtenerDatosUsingGET(
    @retrofit2.http.Path("id") String id
  );

}
