package com.everis.services.ux.service;

import org.springframework.stereotype.Component;
import com.everis.services.ux.api.response.ResponseUx;
import com.everis.services.ux.thirdparty.Response;
import io.reactivex.Single;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ApiUxProcessor {

  public Single<ResponseUx> conversionResponseHaciaUX(Response responseCross) {
    ResponseUx responseUx = new ResponseUx();
    responseUx.setId(responseCross.getCorporateLoanId());
    responseUx.setDebtTypeCode(responseCross.getDebtType().getCode());
    responseUx.setProductDetailTioauxCode(responseCross.getProductDetail().getTioaux().getCode());
    return Single.fromCallable(() -> responseUx);
  }
}
