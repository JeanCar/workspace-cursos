package com.everis.services.ux.service;

import org.springframework.stereotype.Component;
import com.everis.services.ux.config.RestClientConfiguration;
import com.everis.services.ux.proxy.ApICrossControllerApi;
import com.everis.services.ux.thirdparty.Response;
import io.reactivex.Single;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ApiUxSender {

  public Single<Response> obtenerRespuestaDeApiCross(String baseUrl, String id) {

    ApICrossControllerApi apiGet =
        RestClientConfiguration.getClient(baseUrl).create(ApICrossControllerApi.class);

    Single<Response> response = apiGet.obtenerDatosUsingGET(id);

    return response;
  }
}
