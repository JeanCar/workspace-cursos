package com.everis.services.ux.util;

import com.everis.services.ux.api.response.ResponseUx;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EverisUtil {

  public static String toJson(ResponseUx response) {
    String json = "";

    try {
      ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
      json = ow.writeValueAsString(response);
    } catch (JsonProcessingException e) {
      log.error("Error al convertir Objecto a JSON>>");
    }

    return json;
  }
}
