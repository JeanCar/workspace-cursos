package com.everis.services.ux.thirdparty;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Contiene DebtType asociado.
 */
@ApiModel(description = "Contiene DebtType asociado.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-30T14:44:30.438-05:00")
public class DebtType {
  @SerializedName("code")
  private String code = null;

  public DebtType code(String code) {
    this.code = code;
    return this;
  }

   /**
   * Indicador de tipo de deuda.
   * @return code
  **/
  @ApiModelProperty(example = "DIR", value = "Indicador de tipo de deuda.")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DebtType debtType = (DebtType) o;
    return Objects.equals(this.code, debtType.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DebtType {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

