package com.example.demospringdata.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.demospringdata.dao.EventoRepository;
import com.example.demospringdata.model.Evento;

@RestController("base")
public class EventoController {

  @Autowired
  EventoRepository repository;

  @GetMapping("/eventos")
  public List<Evento> init() {
    return repository.findAll();
  }

  @GetMapping("/eventos/{filtro}")
  public List<Evento> init2(@PathVariable("filtro") String filtro) {
    return repository.findByTitulo(filtro);
  }

//  @GetMapping("/eventos_filtro")
//  public List<Evento> init3(@RequestParam(name = "id") Long id) {
//    return repository.findByIdContaining(id);
//  }
}
