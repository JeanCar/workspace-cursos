package com.example.demospringdata.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demospringdata.model.Evento;

public interface EventoRepository  extends JpaRepository<Evento, Long>{

  List<Evento> findByTitulo(String titulo);
  
  //List<Evento> findByIdContaining(Long id);
  //List<Evento> findByTituloContaining(String titulo);
}
