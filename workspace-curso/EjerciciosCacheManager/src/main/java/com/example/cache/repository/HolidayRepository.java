package com.example.cache.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.cache.bean.Holiday;


@Repository
public interface HolidayRepository extends JpaRepository<Holiday, Long> {

}