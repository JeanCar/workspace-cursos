package com.example.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciosCacheManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjerciciosCacheManagerApplication.class, args);
	}

}
