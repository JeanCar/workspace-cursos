package com.example.cache.config;

import javax.crypto.KeyGenerator;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class EmbeddedCacheConfig {
//	@Bean
//	public KeyGenerator carKeyGenerator() {
//		return new CarKeyGenerator();
//	}
}
