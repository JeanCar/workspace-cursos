package com.example.cache.controller;

import java.util.List;
import java.util.Map;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cache.bean.Holiday;
import com.example.cache.service.HolidayService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class HolidayController {

	private final CacheManager cacheManager;
	private final HolidayService holidayService;

	@GetMapping(value = "/holiday")
	public ResponseEntity<List<Holiday>> getHolidays() {
		Cache cache = this.cacheManager.getCache("holidays");
		printCache(cache);
		return new ResponseEntity<>(holidayService.findAll(), HttpStatus.OK);
	}

	@GetMapping(value = "/holiday/{id}")
	public ResponseEntity<Holiday> getHolidaysBydId(@PathVariable("id") Long id) {
		Cache cache = this.cacheManager.getCache("holidays");
		printCache(cache);
		return new ResponseEntity<>(holidayService.findById(id), HttpStatus.OK);
	}

	@PostMapping(value = "/holiday")
	public ResponseEntity<Holiday> saveHolidays(@RequestBody Holiday holiday) {
		return new ResponseEntity<>(holidayService.save(holiday), HttpStatus.OK);
	}

	@PutMapping(value = "/holiday")
	public ResponseEntity<Holiday> updateHolidays(@RequestBody Holiday holiday) {
		return new ResponseEntity<>(holidayService.update(holiday), HttpStatus.OK);
	}

	public void printCache(Cache cache) {
		System.out.println("*************************");
		System.out.println(cache.getName());
		System.out.println(cache.getNativeCache());
		Map<Object, String> map = (Map<Object, String>) cache.getNativeCache();
		System.out.println("1. " + map.get(1));
		System.out.println("2. " + map.get(2));
		System.out.println("3. " + map.get(3));
		System.out.println("*************************");

	}
}
