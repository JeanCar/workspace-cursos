package com.example.cache.service;

import java.util.List;

import com.example.cache.bean.Holiday;

public interface HolidayService {
	Holiday save(Holiday holiday);

	List<Holiday> findAll();

	Holiday findById(long id);

	void delete(Long id);

	Holiday update(Holiday holiday);
}
