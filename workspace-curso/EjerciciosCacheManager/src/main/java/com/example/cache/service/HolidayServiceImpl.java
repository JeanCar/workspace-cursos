package com.example.cache.service;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.cache.bean.Holiday;
import com.example.cache.repository.HolidayRepository;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Service
@RequiredArgsConstructor
public class HolidayServiceImpl implements HolidayService {

	private final HolidayRepository holidayRepository;

	@Override
	public Holiday save(Holiday holiday) {
		return holidayRepository.save(holiday);
	}

	@Override
	public List<Holiday> findAll() {
		// TODO Auto-generated method stub
		return holidayRepository.findAll();
	}

	@Override
	@CacheEvict(value = "holidays", key = "#id")
	public void delete(Long id) {
		holidayRepository.deleteById(id);
	}

	@Override
	@CachePut(value = "holidays", key = "#car.id")
	public Holiday update(Holiday holiday) {
		if (holidayRepository.existsById(holiday.getId())) {
			return holidayRepository.save(holiday);
		}
		throw new IllegalArgumentException("A car must have an id");
	}

	@Override
	@Cacheable(value ="holidays", key="#id")
	public Holiday findById(long id) {
		if (holidayRepository.existsById(id)) {
			return holidayRepository.findById(id).get();
		}
		throw new IllegalArgumentException("A car must have an id");
	}
}