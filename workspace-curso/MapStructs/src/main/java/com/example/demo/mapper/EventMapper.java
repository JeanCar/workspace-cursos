package com.example.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.example.demo.model.Evento;
import com.example.demo.model.EventoDTO;

@Mapper(componentModel = "spring")
public interface EventMapper {

	EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);
	
	@Mapping(source = "titulox",target = "titulo")
    @Mapping(source = "fechax",target = "fecha")
	EventoDTO EventotoEventDTO(Evento evento);

}
