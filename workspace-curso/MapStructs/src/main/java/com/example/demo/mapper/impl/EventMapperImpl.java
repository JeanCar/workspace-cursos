package com.example.demo.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dao.EventoRepository;
import com.example.demo.mapper.EventMapper;
import com.example.demo.model.Evento;
import com.example.demo.model.EventoDTO;

@Component
public class EventMapperImpl implements EventMapper {


	  
	
	@Override
	public EventoDTO EventotoEventDTO(Evento evento) {
		// TODO Auto-generated method stub
		
	    EventoDTO eventoDto = EventMapper.INSTANCE.EventotoEventDTO(evento);
	    eventoDto.setFecha(evento.getFechax());
	    eventoDto.setTitulo(evento.getTitulox());

		return eventoDto;
	}

}
