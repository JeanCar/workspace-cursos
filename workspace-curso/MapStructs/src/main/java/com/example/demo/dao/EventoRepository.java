package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Evento;

public interface EventoRepository  extends JpaRepository<Evento, Long>{

  List<Evento> findByTitulo(String titulo);
  
  List<Evento> findByTituloContaining(String titulo);
}
