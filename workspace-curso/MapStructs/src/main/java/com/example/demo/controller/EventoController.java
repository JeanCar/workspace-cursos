package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.EventoRepository;
import com.example.demo.model.Evento;

@RestController
public class EventoController {

  @Autowired
  EventoRepository repository;
  
  @GetMapping("/eventos")
  public List<Evento> init(){
    return repository.findAll();
  }
  
  @GetMapping("/eventos/{filtro}")
  public List<Evento> init2(@PathVariable("filtro") String filtro){
    return repository.findByTitulo(filtro);
  }
  
  /*@GetMapping("/eventos_filtro2/{filtro2}")
  public List<Evento> init3(@PathVariable("filtro2") String filtro2){
    return repository.findByTituloContaining(filtro2);
  }*/
}
