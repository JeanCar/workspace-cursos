package demo.example.demo.main;

import com.example.demo.service.ICalculadora;
import com.example.demo.service.impl.Calculadora;
import com.example.demo.service.impl.TestClass;

public class Principal {

	public static void main(String[] args) {
		ICalculadora calculadora = new Calculadora();
		Integer operando1 = 10;
		Integer operando2 = 22;
		Integer resultado = calculadora.suma(operando1, operando2);
		System.out.println(operando1 + "+" + operando2 + "=" + resultado);

		
		
		
		// Clase anonima
		ICalculadora calAnonimo = new ICalculadora() {

			@Override
			public int suma(int operando1, int operando2) {
				// TODO Auto-generated method stub
				return operando1 + operando2;
			}

		};
		
		ICalculadora cal2 = new ICalculadora() {
			
			@Override
			public int suma(int operando1, int operando2) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
		
		ICalculadora lambda = (op, op1) -> op + op1;
		
		lambda.suma(1, 2);
		
		
		
		
		System.out.println(calAnonimo.suma(15, 10));

		ICalculadora prueba = (p1, p2) -> p1 + p2;
		System.out.println(prueba.suma(15, 10));

		Calculadora c = new Calculadora();
		c.show();
		
		TestClass t = new TestClass();
		t.show();
		
		
	}

}
