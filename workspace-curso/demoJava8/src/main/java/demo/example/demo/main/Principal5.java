package demo.example.demo.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import com.example.demo.model.Employee;
import com.example.demo.service.MiFunc;

public class Principal5 {

	public static void main(String[] args) {
		
		
		 Function<Employee, String> funcEmpToString= (Employee e)-> {return e.getNombre();};
		  
		 MiFunc empleado = (nom,ed)-> new Employee(nom, ed); //new Employee("jean", 10);

		 MiFunc empleado2 = Employee::new;
		 empleado2.func("david", 15);
		 Employee ok1 = empleado.func("jean", 10);
		 
		 System.out.println(">" + ok1.getNombre());
		 

		 Employee ok = new Employee("jean", 10);		 
		 System.out.println(">" + ok.getNombre());
		 
		 
		 
		 List<Employee> employeeList= 
		     Arrays.asList(new Employee("Tom Jones", 45), 
		      new Employee("Harry Major", 25),
		      new Employee("Ethan Hardy", 65),
		      new Employee("Nancy Smith", 15),
		      new Employee("Deborah Sprightly", 29));
		 
		    List<String> empNameList=convertEmpListToNamesList(employeeList, funcEmpToString);
		    
		    empNameList.forEach(System.out::println);
	}
	
	public static List<String> convertEmpListToNamesList(List<Employee> employeeList, Function<Employee, String> funcEmpToString){
		   List<String> empNameList=new ArrayList<String>(); 
		   
		   for(Employee emp:employeeList){
		     empNameList.add(funcEmpToString.apply(emp));
		   }
		   return empNameList;
		  }
}
