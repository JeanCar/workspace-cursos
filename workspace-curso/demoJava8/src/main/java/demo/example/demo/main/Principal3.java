package demo.example.demo.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class Principal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//PREDICATE
		Predicate<Integer> positive = i -> i > 0;
//		if(positive.test(-5)) {
//			System.out.println("True");
//		}else {
//			System.out.println("False");
//
//		}
//		
		
		List<Integer> integerList = Arrays.asList(
		                      new Integer(1),new Integer(10),
		                      new Integer(200),new Integer(101), 
		                      new Integer(-10),new Integer(0));
		
		
		List<Integer> filteredList = filterList(integerList, positive);
		filteredList.forEach(s-> System.out.println(s));
		filteredList.forEach(System.out::println);
	
		
		Predicate<Integer> positive2 = i -> i <0;
		
		boolean ok = filteredList.stream().anyMatch(s->positive2.test(s));
		
		System.out.println(ok);
		
		System.out.println("**********************");
		
		
		Optional<Integer> ok2= filteredList.stream().findFirst();
		if(ok2.isPresent()) {
			System.out.println(ok2.get());
		}else {
			System.out.println("No encontro data");

		}

	}
	
	
	 public static List<Integer> filterList(List<Integer> listOfIntegers, Predicate<Integer> predicate){
		 //Lista = {1,10,200,101,-10,0}
		 List<Integer> filteredList = new ArrayList<Integer>();
		 
		 
		 for(Integer integer:listOfIntegers){
			 //1
		   if(predicate.test(integer)){
		    filteredList.add(integer);
		   }
		  }
		  return filteredList;
		 }

}
