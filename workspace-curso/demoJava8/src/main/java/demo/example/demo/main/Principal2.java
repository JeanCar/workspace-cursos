package demo.example.demo.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.example.demo.comparator.NombreComparator;
import com.example.demo.model.Persona;

public class Principal2 {
	
	
	public static void main(String[] args) {
		
//		List<Integer> numeros = new ArrayList<>();
//		numeros.add(2);
//		numeros.add(5);
//		numeros.add(3);
//		numeros.stream().forEach(p->System.out.println(p));
//		
		//COMPARATOR
		Persona p1 = new Persona("jean", 12);
		Persona p2 = new Persona("pedro", 42);
		Persona p3 = new Persona("carlos", 20);
		
		List<Persona> lista = Arrays.asList(p1,p2,p3);
		
		lista.sort(new NombreComparator());
	//	lista.stream().forEach(p->System.out.println(p.getNombre()));
		
		
		Comparator<Persona> poo =  (per1, per2)->per1.getNombre().compareTo(per2.getNombre());
	//	lista.sort(poo);
		Collections.sort(lista, poo);
	//	lista.forEach(p->System.out.println(p.getNombre()));
		
		
		//COMPARTING
		
		Comparator<Persona> empNameComparator = Comparator.comparing(Persona::getNombre).thenComparing(Persona::getEdad);
		  Collections.sort(lista, empNameComparator);
		//  lista.forEach(System.out::println);
		
		
		  //COMPARABLE
		  Collections.sort(lista);
		  lista.forEach(System.out::println);

		  
		  Set<String> setper = new  TreeSet<>();
		  setper.add("jean");
		  setper.add("pedro");
		  setper.add("carlos");
		  
		  
		  setper.forEach(System.out::println);

		  Set<String> setper2 = new  HashSet<>();
		  setper2.add("jean");
		  setper2.add("pedro");
		  setper2.add("pedro");
		  setper2.forEach(System.out::println);

		  
		  
	}
}
