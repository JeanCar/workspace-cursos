package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Persona implements Comparable<Persona>{

	private String nombre;
	private int edad;
	
	
	@Override
	public int compareTo(Persona obj) {
		// TODO Auto-generated method stub
	//	return this.nombre.compareTo(obj.getNombre());
		return this.edad - obj.getEdad();
	}


}
