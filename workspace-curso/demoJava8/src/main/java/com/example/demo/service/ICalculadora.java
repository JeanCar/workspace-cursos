package com.example.demo.service;

public interface ICalculadora {

	int suma(int operando1, int operando2);

	default void show() {
		System.out.println("Default TestInterface2");
	}
}
