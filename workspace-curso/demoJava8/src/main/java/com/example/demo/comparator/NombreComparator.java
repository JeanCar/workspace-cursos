package com.example.demo.comparator;

import java.util.Comparator;

import com.example.demo.model.Persona;

public class NombreComparator implements Comparator<Persona> {

	@Override
	public int compare(Persona p1, Persona p2) {
		// TODO Auto-generated method stub
		return  p1.getNombre().compareTo(p2.getNombre());
		//return p2.getEdad() - p1.getEdad();
	}

}
