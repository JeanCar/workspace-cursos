package com.patron.creacional.prototype.clone;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class Shape implements Cloneable {

	private int x;

	private int y;

	private String color;

	public Shape(Shape target) {
		if (target != null) {
			this.x = target.x;
			this.y = target.y;
			this.color = target.color;
		}
	}

	public abstract Shape clone();

}
