package com.patron.creacional.methodfactory.service;

//Deje que la subclase implemente el método de fábrica anterior y decida qué objeto crear. 
public class SMSNotification extends Notification {

	@Override
	public void notifyUser() {
        System.out.println("Sending an SMS notification");
	}

}
