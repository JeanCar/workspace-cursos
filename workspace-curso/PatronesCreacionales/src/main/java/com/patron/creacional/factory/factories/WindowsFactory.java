package com.patron.creacional.factory.factories;

import com.patron.creacional.factory.buttons.Button;
import com.patron.creacional.factory.buttons.WindowsButton;
import com.patron.creacional.factory.checkboxes.Checkbox;
import com.patron.creacional.factory.checkboxes.WindowsCheckbox;

//Las Fábricas Concretas implementan métodos de creación de la 
//fábrica abstracta. Cada fábrica concreta se corresponde con
//una variante específica de los productos y crea tan solo dichas 
//variantes de los productos
public class WindowsFactory implements GUIFactory {

	@Override
	public Button createButton() {
		// TODO Auto-generated method stub
        return new WindowsButton();
	}

	@Override
	public Checkbox createCheckbox() {
		// TODO Auto-generated method stub
        return new WindowsCheckbox();
	}

}
