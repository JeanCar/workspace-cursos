package com.patron.creacional.methodfactory.service;

//1. Defina un método de fábrica dentro de una interfaz. 
public abstract class Notification {
    
	public abstract void notifyUser();

}
