package com.patron.creacional.prototype.clone;

//2) Registro de prototipos : se utiliza como un servicio de registro para tener 
//acceso a todos los prototipos mediante parámetros de cadena simples.
public abstract class Color implements Cloneable {

	protected String colorName;

	public abstract void addColor();

// cuando la creación de objetos requiere mucho tiempo y una operación costosa, 
	// por lo que creamos objetos con el objeto existente en sí
	// Una de las mejores formas disponibles para crear un objeto a partir de
	// objetos existentes es el método clone()
	public Object clone() {
		Object clone = null;
		try {
			clone = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return clone;
	}
}
