package com.patron.creacional.builder.dto;

import lombok.Setter;
import lombok.ToString;

//El objeto, es decir, el menú, está vacío al principio. Una vez se hace la comanda, se añade contenido.
@Setter
@ToString
public class Menu {
	
	private String starter = "No entrante";
	
	private String maincourse = "No plato principal";
	
	private String dessert = "No postre";
	
	private String drink = "No bebida";
}
