package com.patron.creacional.factory.factories;

import com.patron.creacional.factory.buttons.Button;
import com.patron.creacional.factory.checkboxes.Checkbox;

//La interfaz Fábrica Abstracta declara un grupo de métodos para crear 
//cada uno de los productos abstractos.
public interface GUIFactory {
	//PRODUCTO ABSTRACTO - BUTTON
    Button createButton();
    //PRODUCTO ABSTRACTO - CHECKBOX
    Checkbox createCheckbox();
}
