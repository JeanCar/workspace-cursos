package com.patron.creacional.builder.menu;

import com.patron.creacional.builder.MenuBuilder;

//El constructor específico, en este caso el cocinero, construye (es decir, cocina) 
//los componentes individuales del plato de la comanda. Para ello, ignora (override) 
//los artículos del menú “abstract”:
public class MenuOfTheDayBuilder extends MenuBuilder {

	@Override
	public void buildStarter() {
		menu.setStarter("Sopa de calabaza");
	}

	@Override
	public void buildMainCourse() {
		menu.setMaincourse("Filete a la plancha con patatas");
	}

	@Override
	public void buildDessert() {
		menu.setDessert("Helado de vainilla");

	}

	@Override
	public void buildDrink() {
		menu.setDrink("Vino tinto");
	}

}
