package com.patron.creacional.singleton;

import lombok.Getter;

@Getter
public class Singleton {

	private static Singleton instance;
	public String value;

	// Este patrón se implementa haciendo privado el constructor de la clase
	// y creando (en la propia clase) un método que crea una instancia del objeto si
	// este no existe.
	private Singleton(String value) {
		// The following code emulates slow initialization.
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		this.value = value;
		System.out.println("Nombre Objeto: " + this.value);

	}

	// Vemos como al llamar al método 'getInstance()' pregunta si el atributo
	// 'Singleton' es null o no,
	// y si no lo es lo crea. En el caso de que ya haya sido creado no imprimirá un
	// mensaje por pantalla
	// diciendonos que ya existe un objeto de esa clase.
	public static Singleton getInstance(String value) {
		if (instance == null) {
			instance = new Singleton(value);
		} else {
			System.out.println("No se puede crear el objeto " + value + " porque ya existe");
		}

		return instance;
	}
}
