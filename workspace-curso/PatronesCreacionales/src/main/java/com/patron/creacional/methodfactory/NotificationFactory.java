package com.patron.creacional.methodfactory;

import com.patron.creacional.methodfactory.service.EmailNotification;
import com.patron.creacional.methodfactory.service.Notification;
import com.patron.creacional.methodfactory.service.PushNotification;
import com.patron.creacional.methodfactory.service.SMSNotification;

//Cree una clase de fábrica NotificationFactory.java para instanciar una clase concreta. 
public class NotificationFactory {
	// El patrón de diseño de fábrica dice que defina una interfaz
	// (una interfaz Java o una clase abstracta) y deje que las subclases decidan
	// qué objeto instanciar.

	// Dado que estos patrones de diseño hablan de la creación de instancias de un
	// objeto,
	// se incluye en la categoría de patrón de diseño creacional.
	public Notification createNotification(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		switch (channel) {
		case "SMS":
			return new SMSNotification();
		case "EMAIL":
			return new EmailNotification();
		case "PUSH":
			return new PushNotification();
		default:
			throw new IllegalArgumentException("Unknown channel " + channel);
		}
	}
}
