package com.patron.creacional.prototype.dto;

import com.patron.creacional.prototype.clone.Color;

//1) Prototipo : Este es el prototipo de un objeto real.
public class blueColor extends Color {
	
	public blueColor() {
		this.colorName = "blue";
	}

	@Override
	public void addColor() {
		System.out.println("Blue color added");
	}

}
