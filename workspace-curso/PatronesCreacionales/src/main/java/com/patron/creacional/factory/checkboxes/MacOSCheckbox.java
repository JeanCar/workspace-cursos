package com.patron.creacional.factory.checkboxes;

//Los Productos Concretos son implementaciones distintas de 
//productos abstractos agrupados por variantes.
//Cada producto abstracto (silla/sofá) debe implementarse en todas
//las variantes dadas (victoriano/moderno).
public class MacOSCheckbox implements Checkbox {

	@Override
	public void paint() {
		System.out.println("You have created MacOSCheckbox.");

	}

}
