package com.patron.creacional.prototype;

import java.util.HashMap;
import java.util.Map;

import com.patron.creacional.prototype.clone.Color;
import com.patron.creacional.prototype.dto.blackColor;
import com.patron.creacional.prototype.dto.blueColor;
//3) Cliente : el Cliente será responsable de utilizar el servicio de registro para acceder a las instancias prototipo.
public class ColorStore {

	private static Map<String, Color> colorMap = new HashMap<String, Color>();

	static {
		colorMap.put("blue", new blueColor());
		colorMap.put("black", new blackColor());
	}

	public static Color getColor(String colorName) {
		return (Color) colorMap.get(colorName).clone();
	}
}
