package com.patron.creacional.factory.checkboxes;

//Las Fábricas Concretas implementan métodos de creación de la fábrica abstracta.
//Cada fábrica concreta se corresponde con una variante específica de los productos
//y crea tan solo dichas variantes de los productos.
public interface Checkbox {
	void paint();
}
