package com.patron.creacional.prototype.dto;

import com.patron.creacional.prototype.clone.Shape;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Rectangle extends Shape {

	private int width;
	private int height;

	public Rectangle(Rectangle target) {
		super(target);
		if (target != null) {
			this.width = target.width;
			this.height = target.height;
		}
	}

	@Override
	public Shape clone() {
		return new Rectangle(this);
	}

}
