package com.patron.creacional.prototype.dto;

import com.patron.creacional.prototype.clone.Shape;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Circle extends Shape {

	private int radius;

	public Circle(Circle target) {
		super(target);
		if (target != null) {
			this.radius = target.radius;
		}
	}

	@Override
	public Shape clone() {
		return new Circle(this);
	}

}
