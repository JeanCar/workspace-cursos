package com.patron.creacional.builder;

import com.patron.creacional.builder.dto.Menu;

//El MattsRestaurant proporciona el “ambiente” necesario para que un plato pueda ser elaborado (o construido) para el cliente.
//Este ambiente es accesible para todos los clientes. El cliente solo se comunica
//con el MattsRestaurant para que la preparación como tal esté oculta:
public class MattsRestaurant {

	private MenuBuilder menuBuilder;

	public void setBuilder(MenuBuilder menuBuilder) {
		this.menuBuilder = menuBuilder;
	}

	// Luego, entra en escena el constructor. En nuestro ejemplo, sería el cocinero
	// jefe:
	public Menu buildMenu() {
		menuBuilder.buildStarter();
		menuBuilder.buildMainCourse();
		menuBuilder.buildDessert();
		menuBuilder.buildDrink();
		return menuBuilder.build();
	}
}
