package com.patron.creacional.builder;

import com.patron.creacional.builder.dto.Menu;

public abstract class MenuBuilder {

	public Menu menu = new Menu();

	public abstract void buildStarter();

	public abstract void buildMainCourse();

	public abstract void buildDessert();

	public abstract void buildDrink();

	Menu build() {
		return menu;
	}
}
