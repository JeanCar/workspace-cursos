package com.patron.creacional.factory.buttons;

//Los Productos Abstractos declaran interfaces para un grupo de
//productos diferentes pero relacionados que forman una familia
//de productos.
public interface Button {
	void paint();
}
