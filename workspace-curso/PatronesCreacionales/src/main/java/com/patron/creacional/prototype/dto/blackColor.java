package com.patron.creacional.prototype.dto;

import com.patron.creacional.prototype.clone.Color;

//1) Prototipo : Este es el prototipo de un objeto real.
public class blackColor extends Color {

	public blackColor() {
		this.colorName = "black";
	}

	@Override
	public void addColor() {
		System.out.println("Black color added");
	}
}