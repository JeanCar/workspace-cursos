package com.patron.creacional.factory;

import com.patron.creacional.factory.buttons.Button;
import com.patron.creacional.factory.checkboxes.Checkbox;
import com.patron.creacional.factory.factories.GUIFactory;

public class Application {
	private Button button;
	private Checkbox checkbox;

	public Application(GUIFactory factory) {
		button = factory.createButton();
		checkbox = factory.createCheckbox();
	}

	public void paint() {
		button.paint();
		checkbox.paint();
	}
}
