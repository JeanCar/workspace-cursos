package com.patron.creacional;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.patron.creacional.builder.MattsRestaurant;
import com.patron.creacional.builder.dto.Menu;
import com.patron.creacional.builder.menu.MenuOfTheDayBuilder;
import com.patron.creacional.builder.menu.SpecialMenuBuilder;
import com.patron.creacional.factory.app.Application;
import com.patron.creacional.factory.factories.GUIFactory;
import com.patron.creacional.factory.factories.MacOSFactory;
import com.patron.creacional.factory.factories.WindowsFactory;
import com.patron.creacional.methodfactory.NotificationFactory;
import com.patron.creacional.methodfactory.service.Notification;
import com.patron.creacional.prototype.ColorStore;
import com.patron.creacional.prototype.clone.Shape;
import com.patron.creacional.prototype.dto.Circle;
import com.patron.creacional.prototype.dto.Rectangle;
import com.patron.creacional.singleton.Singleton;

@SpringBootApplication
public class PatronesCreacionalesApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PatronesCreacionalesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// createAbstractFactory();
		// createBuilderPattern();
		// createMethodFactory();
		// createPrototype();
		//createPrototype2();
		//createSingleton();
	}

	void createAbstractFactory() {
		Application app; // CLIENT
		GUIFactory factory; // ABSTRACTFACTORY

		// El Cliente puede funcionar con cualquier variante fábrica/producto
		// concreta, siempre y cuando se comunique con sus objetos a través de
		// interfaces abstractas.
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.contains("mac")) {
			factory = new MacOSFactory();
			app = new Application(factory);// CREA
		} else {
			factory = new WindowsFactory();
			app = new Application(factory);
		}
		app.paint(); // METHOD
	}

	void createBuilderPattern() {
		// Por último, se resumen los componentes individuales del plato y se realiza la
		// entrega al cliente, es decir, “se imprime”.
		MattsRestaurant mattsRestaurant = new MattsRestaurant();
		mattsRestaurant.setBuilder(new MenuOfTheDayBuilder());
		Menu menu1 = mattsRestaurant.buildMenu();
		System.out.println(menu1.toString());

		mattsRestaurant.setBuilder(new SpecialMenuBuilder());
		Menu menu2 = mattsRestaurant.buildMenu();
		System.out.println(menu2.toString());

	}

	void createMethodFactory() {
		// Ahora usemos la clase de fábrica para crear y obtener un objeto de clase
		// concreta pasando alguna información.

		NotificationFactory notificationFactory = new NotificationFactory();
		Notification notification = notificationFactory.createNotification("SMS");
		notification.notifyUser();
	}

	void createPrototype() {
		ColorStore.getColor("blue").addColor();
		ColorStore.getColor("black").addColor();
		ColorStore.getColor("black").addColor();
		ColorStore.getColor("blue").addColor();
	}

	void createPrototype2() {
		List<Shape> shapes = new ArrayList<>();
		List<Shape> shapesCopy = new ArrayList<>();

		Circle circle = new Circle();
		circle.setX(10);
		circle.setY(20);
		circle.setRadius(15);
		circle.setColor("red");
		shapes.add(circle);

		Circle anotherCircle = (Circle) circle.clone();
		shapes.add(anotherCircle);

		System.out.println("************************************");

		System.out.println("circle: " + circle.toString());
		System.out.println("anotherCircle: " + anotherCircle.toString());

		Rectangle rectangle = new Rectangle();
		rectangle.setWidth(10);
		rectangle.setHeight(20);
		rectangle.setColor("blue");
		shapes.add(rectangle);
		System.out.println("------");

		Rectangle anotherRectangle = (Rectangle) rectangle.clone();
		shapes.add(anotherRectangle);
		System.out.println("rectangle: " + rectangle.toString());
		System.out.println("anotherRectangle: " + anotherRectangle.toString());
		System.out.println("************************************");

		cloneAndCompare(shapes, shapesCopy);

	}

	private void cloneAndCompare(List<Shape> shapes, List<Shape> shapesCopy) {
		System.out.println("************COMPARACION DE OBJETOS*****************");

		for (Shape shape : shapes) {
			shapesCopy.add(shape.clone());
		}
		for (int i = 0; i < shapes.size(); i++) {
			if (shapes.get(i) != shapesCopy.get(i)) {
				System.out.println(i + ": Shapes son objetos diferentes");
				if (shapes.get(i).equals(shapesCopy.get(i))) {
					System.out.println(i + ": Y son idénticos");
				} else {
					System.out.println(i + ": Pero no son idénticos (booo!)");
				}
			} else {
				System.out.println(i + ": Los objetos de forma son los mismos");
			}
		}
	}

	void createSingleton() {
		//Vamos a ver como efectivamente nos crea un solo objeto, ejecutando el siguiente código 
		//en el que llamamos dos veces el método 'getInstance()':
		Singleton singleton = Singleton.getInstance("Primer Instancia");
		Singleton anotherSingleton = Singleton.getInstance("Segunda Instancia");		
        System.out.println(singleton.getValue());
        System.out.println(anotherSingleton.getValue());   
	}
}