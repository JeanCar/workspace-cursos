package com.patron.creacional.builder.menu;

import com.patron.creacional.builder.MenuBuilder;

public class SpecialMenuBuilder extends MenuBuilder {

	@Override
	public void buildStarter() {
		menu.setStarter("Special Sopa de calabaza");
	}

	@Override
	public void buildMainCourse() {
		menu.setMaincourse("Special Filete a la plancha con patatas");
	}

	@Override
	public void buildDessert() {
		menu.setDessert("Special Helado de vainilla");

	}

	@Override
	public void buildDrink() {
		menu.setDrink("Special Vino tinto");
	}

}
