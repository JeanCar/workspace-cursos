package com.example.demo.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Datos {

	private String codigoSeguimiento;
	private String codigoAlternativo;
	private String fechaDesde;
	private String fechaHasta;
	
	
	
}
