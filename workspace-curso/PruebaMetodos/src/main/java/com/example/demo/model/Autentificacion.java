package com.example.demo.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Autentificacion {

	private String shipper;
	private String password;
	
}
