package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Persona;
import com.example.demo.model.Request;

@RestController
@RequestMapping("prueba")
public class PruebaController {

	
	@GetMapping(value = "/obtenerDatos/{filtro}")
	public String obtenerDatos(@PathVariable("filtro") String nombre) {
		return "Hola " + nombre;
	}
	
	@GetMapping(value ="/obtenerDatos2")
	public String obtenerDatos2(@RequestParam(name="nombre") String nombre) {
		return "prueba request param " +nombre;
	}
	
	
	@PostMapping(value="obtenerDatos3")
	public String obtenerDatos3(@RequestBody Persona persona ) {
		
		return "prueba post: " + persona.getNombre() + " " + persona.getApellidos();
	}
	
	
	@PostMapping(value="obtenerDatos4")
	public String obtenerDatos4(@RequestBody Request request ) {
		return "prueba post: " + request.getAutentificacion().getShipper();
	}
	
}
