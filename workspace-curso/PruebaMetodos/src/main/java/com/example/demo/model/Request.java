package com.example.demo.model;

import java.util.List;

import lombok.Data;

@Data
public class Request {

	private List<Datos> datos;
	
	private Autentificacion autentificacion;
	
}
