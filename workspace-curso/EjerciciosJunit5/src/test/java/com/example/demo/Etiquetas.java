package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.example.demo.bean.Persona;
import com.example.demo.service.CalculatorService;
import com.example.demo.service.impl.CalculatorServiceImpl;

public class Etiquetas {

	private CalculatorService calculator;
	private Persona person;

	@BeforeEach
	void init() {
		System.out.println("inside @BeforeEach");
		calculator = new CalculatorServiceImpl();
		person = new Persona("Jane", "Doe");
	}

	/**
	 * La @Tag anotación es útil cuando queremos crear un «paquete de prueba» con
	 * las pruebas seleccionadas. Las etiquetas se utilizan para filtrar qué pruebas
	 * se ejecutan:
	 */
	@Tag("calculator")
	@Test
	void test1() {
		assertNotNull(calculator, "La variable calculator debe ser inicializada");
	}

	@Tag("calculator")
	@Test
	void test2() {
		assertNotNull(calculator, "La variable calculator debe ser inicializada");
	}

	@Tag("persona")
	@Test
	void test3() {
		assertNotNull(person, "La variable person debe ser inicializada");
	}

}
