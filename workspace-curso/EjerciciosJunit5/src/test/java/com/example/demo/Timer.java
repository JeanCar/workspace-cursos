package com.example.demo;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CountDownLatch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Timer {

	@Test
	void timeoutNotExceeded() {
		// La siguiente afirmación tiene éxito.
		assertTimeout(ofMinutes(2), () -> {
			// Realiza una tarea que requiere menos de 2 minutos.
		});
	}

	@Test
	void timeoutNotExceededWithResult() {
		// La siguiente afirmación tiene éxito y devuelve el objeto suministrado.
		String actualResult = assertTimeout(ofMinutes(2), () -> {
			return "a result";
		});
		assertEquals("a result", actualResult);
	}

	@Test
	void timeoutNotExceededWithMethod() {
		// La siguiente aserción invoca una referencia a un método y devuelve un objeto.
		String actualGreeting = assertTimeout(ofMinutes(2), Timer::greeting);
		assertEquals("Hello, World!", actualGreeting);
	}

	private static String greeting() {
		return "Hello, World!";
	}

	@Test
	void timeoutExceeded() {
		// La siguiente aserción falla con un mensaje de error similar a: la ejecución
		// excedió el tiempo de espera de 10 ms por 91 ms
		assertTimeout(ofMillis(10), () -> {
			// Simular una tarea que dure más de 10 ms.
			Thread.sleep(100);
		});
	}

	@Test
	void timeoutExceededWithPreemptiveTermination() {
		//La siguiente aserción falla con un mensaje de error similar a:  ejecución agotada después de 10 ms
		assertTimeoutPreemptively(ofMillis(10), () -> {
			// Simulate task that takes more than 10 ms.
			new CountDownLatch(1).await();
		});
	}

	/**
	 * Prueba de operación de tiempo de espera: afirmar tiempo de espera preventivo
	 * Cuando queremos probar el tiempo de ejecución de un método que consume mucho
	 * tiempo y no queremos permitir que el método de prueba espere indefinidamente,
	 * podemos realizar una prueba de tiempo de espera en el método de prueba, y
	 * JUnit 5 introduce un método de afirmación para estoassertTimeout, Proporciona
	 * un amplio soporte para tiempos de espera.
	 * 
	 * Suponiendo que queremos que el código de prueba se ejecute en un segundo,
	 * podemos escribir el siguiente caso de prueba:
	 */
	@Test
	@DisplayName("Prueba del método de tiempo de espera")
	void test_should_complete_in_one_second() {
		Assertions.assertTimeoutPreemptively(Duration.of(1, ChronoUnit.SECONDS), () -> Thread.sleep(2000));
	}
}
