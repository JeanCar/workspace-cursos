package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.service.CalculatorService;
import com.example.demo.service.impl.CalculatorServiceImpl;

public class BeforeAfter {

	private CalculatorService calculator;

	/*
	 * @BeforeEach: Se llama a un método con esta anotación antes de cada método de
	 * prueba, muy útil cuando queremos que los métodos de prueba tengan algún
	 * código en común. Los métodos deben tener un voidtipo de retorno, no deben ser
	 * privatey no deben ser static.
	 * 
	 * @BeforeAll: Un método con esta anotación se llama solo una vez, antes de que
	 * se ejecute cualquiera de las pruebas, se usa principalmente en lugar
	 * de @BeforeEach cuando el código común es costoso, como establecer una
	 * conexión de base de datos. ¡El @BeforeAllmétodo debe ser el
	 * staticpredeterminado! Tampoco debe ser privatey debe tener un voidtipo de
	 * retorno.
	 * 
	 * @AfterAll: Un método con esta anotación se llama solo una vez, después de que
	 * se haya llamado a cada método de prueba. Normalmente se utiliza para cerrar
	 * conexiones establecidas por @BeforeAll. El método debe tener un voidtipo de
	 * retorno, no debe ser privatey debe ser static.
	 * 
	 * @AfterEach: Se llama a un método con esta anotación después de que cada
	 * método de prueba finaliza su ejecución. Los métodos deben tener un voidtipo
	 * de retorno, no deben ser privatey no deben ser static.
	 */
	@BeforeAll
	static void start() {
		System.out.println("inside @BeforeAll");
	}

	@BeforeEach
	void init() {
		System.out.println("inside @BeforeEach");
		calculator = new CalculatorServiceImpl();
	}

	@Test
	void groupedAssertionsTest() {
		assertAll(() -> assertEquals(2, calculator.add(1, 1), "La suma de su dos numeros positivos es incorrecto"),
				() -> assertEquals(5, calculator.minus(10, 5), "La resta de su dos numeros positivos es incorrecto"),
				() -> assertEquals(12, calculator.multiply(2, 6),
						"La multiplicación de su dos numeros positivos es incorrecto"),
				() -> assertEquals(5, calculator.divide(10, 2), "La dvisión de su dos numeros positivos es incorrecto"),
				() -> assertNotNull(calculator, "La variable calculator debe ser inicializada"));
	}

	@AfterEach
	void afterEach() {
		System.out.println("inside @AfterEach");
	}

	@AfterAll
	static void close() {
		System.out.println("inside @AfterAll");
	}
}
