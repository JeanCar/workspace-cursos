package com.example.demo;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class OrdenesMetodos {
	/**
	 * El siguiente ejemplo demuestra cómo garantizar que los métodos de prueba se
	 * ejecuten en el orden especificado a través de la @Orderanotación.
	 */
	@Test
	@Order(2)
	void nullValuesTest() {
		// perform assertions against null values
	}

	@Test
	@Order(1)
	void emptyValuesTest() {
		// perform assertions against empty values
	}

	@Test
	@Order(3)
	void validValuesTest() {
		// perform assertions against valid values
	}
}
