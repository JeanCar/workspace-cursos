package com.example.demo;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Timeout {

	/**
	 * La @Timeoutanotación permite declarar que una prueba, fábrica de prueba,
	 * plantilla de prueba o método de ciclo de vida debe fallar si su tiempo de
	 * ejecución excede una duración determinada. La unidad de tiempo para la
	 * duración por defecto es segundos pero es configurable.
	 * 
	 * El siguiente ejemplo muestra cómo @Timeout se aplica al ciclo de vida y los
	 * métodos de prueba.
	 */
	@BeforeEach
	@org.junit.jupiter.api.Timeout(5)
	void setUp() {
		// falla si el tiempo de ejecución supera los 5 segundos
	}

	@Test
	@org.junit.jupiter.api.Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
	void failsIfExecutionTimeExceeds100Milliseconds() {
		// falla si el tiempo de ejecución supera los 100 milisegundos
	}

}
