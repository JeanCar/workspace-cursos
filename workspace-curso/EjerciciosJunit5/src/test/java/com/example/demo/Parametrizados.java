package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class Parametrizados {

	/* @RepeatedTest */
	/**
	 * Prueba de repetibilidad: @RepeatedTest Se agregó soporte para configurar el
	 * número de ejecuciones de métodos de prueba en JUnit 5, permitiendo que los
	 * métodos de prueba se ejecuten repetidamente. Cuando desee ejecutar un método
	 * de prueba N veces, puede usar @RepeatedTest para marcarlo, como se muestra en
	 * el siguiente código:
	 */
	@DisplayName("Repetir prueba")
	@RepeatedTest(value = 3)
	public void repeatTest() {
		System.out.println("Ejecutar prueba");
	}

	/**
	 * Prueba básica de fuente de datos: @ValueSource
	 * 
	 * es la fuente de parámetros de datos más simple proporcionada por JUnit 5. Es
	 * compatible con los ocho tipos y cadenas básicas de Java, Class, y se asigna
	 * al atributo de tipo correspondiente en la anotación cuando se usa. Se pasa en
	 * una matriz. El código de muestra es el siguiente:
	 */
	@ParameterizedTest(name = "For example {0}")
	@ValueSource(ints = { 2, 4, 8 })
	void numberShouldBeEvenTest(int num) {
		Assertions.assertEquals(0, num % 2);
	}

	@ParameterizedTest
	@ValueSource(strings = { "Effective Java", "Code Complete", "Clean Code" })
	void evaluateInitialLettersTest(String title) {
		System.out.println(title);
		assertTrue(title.startsWith("C"), "La Palabra " + title + " no inicia con la letra 'C'");
	}

	/**
	 * Prueba de fuente de datos CSV: @CsvSource A través de @CsvSource, puede
	 * inyectar un conjunto de datos en el formato CSV especificado (valores
	 * separados por comas) y utilizar cada valor separado por comas para que
	 * coincida con los parámetros correspondientes a un método de prueba. El
	 * siguiente es un ejemplo de uso:
	 */
	@ParameterizedTest
	@CsvSource({ "1,One", "2,Two", "3,Three" })
	void dataFromCsvTest(long id, String name) {
		System.out.printf("id: %d, name: %s ", id, name);
	}

	/**
	 * Otra anotación simple que cambia el nombre mostrado del método de prueba.
	 */
	@ParameterizedTest
	@ValueSource(ints = { 6, 8, 2, 9 })
	void lessThanTenTest(int number) {
		assertTrue(number < 10, "el número " + number + " no es inferior a 10");
	}

	private static Stream<Arguments> data() {
		return Stream.of(Arguments.of(1, 1), Arguments.of(2, 1), Arguments.of(3, 2), Arguments.of(4, 3),
				Arguments.of(5, 5), Arguments.of(6, 8));
	}

	@ParameterizedTest
	@MethodSource("data")
	void fibonacciSequence(int input, int expected) {
		System.out.print(nthFibonacciTerm(input));
		assertEquals(expected, nthFibonacciTerm(input));
	}
	
	public static int nthFibonacciTerm(int n) {
	    if (n == 1 || n == 0) {
	        return n;
	    }
	    return nthFibonacciTerm(n-1) + nthFibonacciTerm(n-2);
	}
}