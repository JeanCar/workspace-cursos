package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.service.impl.CalculatorServiceImpl;

public class Suposiciones {
	/*
	 * ASUMME() Cuando una suposición no es cierta, la prueba no se ejecuta en
	 * absoluto. Las suposiciones se utilizan normalmente cuando no tiene sentido
	 * seguir ejecutando una prueba si no se cumplen determinadas condiciones, y la
	 * mayoría de las veces la propiedad que se está probando es algo externo, no
	 * directamente relacionado con lo que estamos probando
	 */
	/*
	 * algunos métodos de suposición sobrecargados:
	 * 
	 * assumeTrue(boolean assumption, optionalMsg) assumeFalse(boolean assumption,
	 * optionalMsg)
	 * 
	 * solo ejecutará la prueba si lo proporcionado assumptiones verdadero y falso,
	 * respectivamente. OptionalMsg mostrará solo si la suposición no es cierta.
	 * 
	 * 
	 * assumingThat(boolean assumption, Executable exec)– si assumptiones verdadero,
	 * exec se ejecutará; de lo contrario, este método no hace nada.
	 */
	CalculatorServiceImpl calculator;
	boolean bool;

	@BeforeEach
	void init() {
		System.out.println("inside @BeforeEach");
		bool = true;
		calculator = new CalculatorServiceImpl();
	}

	@Test
	void additionTest() {
		assumeTrue(bool, "Java ve que esta suposición no es cierta -> deja de ejecutar la prueba.");
		System.out.println("inside additionTest");
		assertAll(() -> assertEquals(2, calculator.add(1, 1), "No suma correctamente dos números positivos"),
				() -> assertEquals(0, calculator.add(-1, 1), "No suma correctamente un número negativo y uno positivo"),
				() -> assertNotNull(calculator, "La variable calculator debe ser inicializada"));
	}

	@Test
	void divisionTest() {
		assumeFalse(0 > 5, "Este mensaje no se mostrará, y la prueba continuará");
		assumingThat(!bool, () -> System.out.println("uD83DuDC4C"));
		System.out.println("inside divisionTest");
		assertThrows(ArithmeticException.class, () -> calculator.divide(2, 0));
	}

}
