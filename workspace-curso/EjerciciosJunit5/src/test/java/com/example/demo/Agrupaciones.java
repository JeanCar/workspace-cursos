package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.example.demo.service.CalculatorService;
import com.example.demo.service.impl.CalculatorServiceImpl;

@DisplayName("The calcultatorulator class: ")
public class Agrupaciones {

	CalculatorService calcultator;

	/**
	 * Esta anotación nos permite agrupar las pruebas donde tenga sentido hacerlo.
	 * Es posible que deseemos separar las pruebas que tratan de la suma de las
	 * pruebas que tratan de la división, la multiplicación, etc. y nos proporciona
	 * un camino fácil para @Disable ciertos grupos por completo. También nos
	 * permite probar y hacer oraciones completas en inglés como resultado de
	 * nuestra prueba, haciéndolo extremadamente legible.
	 */

	@BeforeEach
	void init() {
		calcultator = new CalculatorServiceImpl();
	}

	@Nested
	@DisplayName("al probar la suma")
	class Suma {
		@Test
		@DisplayName("con números positivos")
		void positive() {
			assertEquals(2, calcultator.add(1, 1), "La suma de su dos numeros positivos es incorrecto");
		}

		@Test
		@DisplayName("con números negativos")
		void negative() {
			assertEquals(-2, calcultator.add(-1, -1), "La suma de su dos numeros negativos es incorrecto");
		}
	}

	@Nested
	@DisplayName("al probar la resta")
	class Subtract {
		@Test
		@DisplayName("con números positivos")
		void positive() {
			assertEquals(5, calcultator.minus(10, 5), "La resta de su dos numeros positivos es incorrecto");
		}

		@Test
		@DisplayName("con números negativos")
		void negative() {
			assertEquals(-9, calcultator.minus(-10, -1), "el resultado debe ser la resta de los argumentos");
		}
	}

	@Nested
	@DisplayName("al probar la multiplicación")
	class Multiply {
		@Test
		@DisplayName("con números positivos")
		void positive() {
			assertEquals(100, calcultator.multiply(10, 10),
					"La multiplicación de su dos numeros positivos es incorrecto");
		}

		@Test
		@DisplayName("con números negativos")
		void negative() {
			assertEquals(100, calcultator.multiply(-10, -10),
					"La multiplicación de su dos numeros negativos es incorrecto");
		}
	}

	@Nested
	@DisplayName("al probar la división")
	class Division {
		@Test
		@DisplayName("con 0 como divisor")
		void throwsAtZero() {
			assertThrows(ArithmeticException.class, () -> calcultator.divide(2, 0),
					"the method should throw and ArithmeticException");
		}
	}
}
