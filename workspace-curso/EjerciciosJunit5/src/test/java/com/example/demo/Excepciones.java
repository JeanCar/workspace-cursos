package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.demo.service.impl.CalculatorServiceImpl;

/*https://howtodoinjava.com/junit5/expected-exception-example*/
public class Excepciones {

	private CalculatorServiceImpl calculator;

	@BeforeEach
	void init() {
		System.out.println("inside @BeforeEach");
		calculator = new CalculatorServiceImpl();
	}

	/*
	 * EXAMPLE:
	 * 
	 * @Test void expectedExceptionTest() { ApplicationException thrown =
	 * Assertions.assertThrows(ApplicationException.class, () -> { //Code under test
	 * }); Assertions.assertEquals("some message", exception.getMessage()); }
	 */

	/*
	 * 1.2. Salida de prueba
	 * 
	 * Si no se lanza ninguna excepción desde el executablebloque, lo assertThrows()
	 * hará FAIL.
	 * 
	 * 
	 * Si se lanza una excepción de un tipodiferente , assertThrows() will FAIL.
	 * 
	 * 
	 * Si el bloque de código arroja unaexcepción de una clase que es un subtipo de
	 * la expectedType excepción, entonces la assertThrows() voluntad PASS.
	 * 
	 */
	@Test
	@DisplayName("Excepción detectada por dividir entre 0")
	void arithmeticExceptionTest() {
		Exception exception = assertThrows(ArithmeticException.class, () -> calculator.divide(1, 0));
		assertEquals("/ by zero", exception.getMessage());
	}

	/**
	 * Para los métodos con excepciones en nuestro código, generalmente usamos
	 * try-catch para capturarlos y manejarlos. Para el código con excepciones
	 * generadas por las pruebas, JUnit 5 proporciona
	 * métodos.Assertions#assertThrows(Class<T>, Executable)Para las pruebas, el
	 * primer parámetro es el tipo de excepción, y el segundo es el parámetro de la
	 * interfaz funcional. Es similar a la interfaz Runnable. No requiere parámetros
	 * y no regresa. También admite el uso de expresiones Lambda. Para uso
	 * específico, consulte el siguiente código:
	 */
	@Test
	@DisplayName("Excepción detectada por parseo a entero")
	void illegalArgumentExceptionTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			Integer.valueOf(null);
		});
	}

	/*
	 * A continuación se muestra una prueba muy simple que se espera
	 * NumberFormatExceptionque se lance cuando se ejecute el bloque de código
	 * proporcionado.
	 */
	@Test
	void NumberFormatExceptionTest() {
		NumberFormatException thrown = Assertions.assertThrows(NumberFormatException.class, () -> {
			Integer.parseInt("One");
		}, "Se esperaba una NumberFormatException");

		Assertions.assertEquals("For input string: \"One\"", thrown.getMessage());
	}

	/*
	 * Por ejemplo, en el siguiente ejemplo "1"hay un número válido, por lo que no
	 * se generará ninguna excepción. Esta prueba fallará con el mensaje en la
	 * consola.
	 */
	@Test
	void NumberFormatExceptionSuccessTest() {
		NumberFormatException thrown = Assertions.assertThrows(NumberFormatException.class, () -> {
			Integer.parseInt("1");
		}, "Se esperaba un error de NumberFormatException");

		Assertions.assertEquals("Some expected message", thrown.getMessage());
	}

}
