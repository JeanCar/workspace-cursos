package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIf;
import org.junit.jupiter.api.condition.EnabledIf;

public class CondicionesMetodos {
	/**
	 * Un contenedor o prueba se puede habilitar o deshabilitar en función de la
	 * devolución booleana de un método a través de las anotaciones @EnabledIfy
	 * . @DisabledIf El método se proporciona a la anotación a través de su nombre.
	 * Si es necesario, el método de condición puede tomar un solo parámetro de tipo
	 * ExtensionContext.
	 */
	@Test
	@EnabledIf("customCondition")
	void enabled() {
		// ...
	}

	@Test
	@DisabledIf("customCondition")
	void disabled() {
		// ...
	}

	boolean customCondition() {
		return true;
	}
}
