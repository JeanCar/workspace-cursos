package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.example.demo.bean.Persona;
import com.example.demo.service.CalculatorService;
import com.example.demo.service.impl.CalculatorServiceImpl;

public class Afirmaciones {

	private final CalculatorService calculator = new CalculatorServiceImpl();

	private final Persona person = new Persona("Jane", "Doe");

	/* ASSERTEQUALS(), ASSERTTRUE() */
	/*
	 * JUnit 5 viene con muchos métodos de afirmación. Algunos de ellos son solo
	 * métodos de conveniencia que se pueden reemplazar fácilmente por un método
	 * assertEquals()o assertSame()
	 */
	@Test
	void additionTest() {
		assertNotNull(calculator, "La variable calculator debe ser inicializada");
		assertEquals(2, calculator.add(1, 1), "La suma de su dos numeros positivos es incorrecto");
		assertEquals(-2, calculator.add(-1, -1), "La suma de su dos numeros negativos es incorrecto");
		assertTrue(calculator.isLargerNumber(3, 1), () -> "El numero no es mayor");
		assertTrue(calculator.isSamllerNumber(1, 3), () -> "El numero no es menor");
	}

	@Test
	void minusTest() {
		assertNotNull(calculator, "La variable calculator debe ser inicializada");
		assertEquals(10, calculator.minus(11, 1), "La resta de su dos numeros positivos es incorrecto");
		assertEquals(0, calculator.minus(-1, -1), "La resta de su dos numeros negativos es incorrecto");
	}

	@Test
	void multiplyTest() {
		assertNotNull(calculator, "La variable calculator debe ser inicializada");
		assertEquals(10, calculator.multiply(10, 1), "La multiplicación de su dos numeros positivos es incorrecto");
		assertEquals(1, calculator.multiply(-1, -1), "La multiplicación de su dos numeros negativos es incorrecto");
	}

	@Test
	void divideTest() {
		assertNotNull(calculator, "La variable calculator debe ser inicializada");
		assertEquals(5, calculator.divide(10, 2), "La división de su dos numeros positivos es incorrecto");
		assertEquals(5, calculator.divide(-10, -2), "La división de su dos numeros negativos es incorrecto");
	}

	/* ASSERTALL() */
	/*
	 * assertAll() resuelve este problema ejecutando todas las afirmaciones y luego
	 * mostrándole el error incluso si fallaron varias afirmaciones. Es bueno
	 * entender que assertAll() básicamente verifica si alguno de los ejecutables
	 * arroja una excepción, ejecutándolos todos independientemente, y todos los que
	 * arrojan una excepción se agregan en el * MultipleFailuresErrorque arroja el
	 * método.
	 */
	@Test
	void groupedAssertions01Test() {
		assertAll(() -> assertEquals(2, calculator.add(1, 1), "La suma de su dos numeros positivos es incorrecto"),
				() -> assertEquals(5, calculator.minus(10, 5), "La resta de su dos numeros positivos es incorrecto"),
				() -> assertEquals(12, calculator.multiply(2, 6),
						"La multiplicación de su dos numeros positivos es incorrecto"),
				() -> assertEquals(5, calculator.divide(10, 2), "La dvisión de su dos numeros positivos es incorrecto"),
				() -> assertNotNull(calculator, "La variable calculator debe ser inicializada"));
	}

	@Test
	void groupedAssertions02Test() {
		// In a grouped assertion all assertions are executed, and all
		// failures will be reported together.
		assertAll("person", () -> assertEquals("Jane", person.getFirstName()),
				() -> assertEquals("Doe", person.getLastName()));
	}

	/* SUPPLIER ()-> */
	/*
	 * Es posible que haya notado que String optionalMsgse excluye de las
	 * declaraciones del método. JUnit 5 proporciona una pequeña optimización al
	 * optionalMsg
	 */
	@Test
	void optionalMessageTest() {
		// Realmente no desea que optionalMsg cargue algo así, independientemente de
		// si Java planea imprimirlo.
		/// assertEquals("", "", "The test failed because " + (Math.sqrt(50) +
		// Math.scalb(15,7) + Math.cosh(10) + Math.log1p(23)) + " is not a pretty
		// number");
		// La solución es usar un Supplier<String>. De esta manera podemos utilizar los
		// beneficios de la evaluación perezosa, si nunca ha oído hablar del concepto,
		// es básicamente Java diciendo «No calcularé nada que no necesite.
		assertEquals("12943.482041745538", "12943.4820417455382", () -> "The test failed because "
				+ (Math.sqrt(50) + Math.scalb(15, 7) + Math.cosh(10) + Math.log1p(23)) + " is not a pretty number");

	}

	/*
	 * Dentro de un bloque de código, si una aserción falla el código subsiguiente
	 * en el mismo bloque será omitido.
	 */
	@Test
	void dependentAssertions() {
		assertAll("properties", () -> {
			String firstName = person.getFirstName();
			assertNotNull(firstName);
			// Se ejecuta sólo si la afirmación anterior es válida.
			assertAll("first name", () -> assertTrue(firstName.startsWith("J")),
					() -> assertTrue(firstName.endsWith("e")));
		}, () -> {
			// Aserción agrupada, por lo que se procesa independientemente de los resultados
			// de las aserciones de nombre.
			String lastName = person.getLastName();
			assertNotNull(lastName);
			// Se ejecuta sólo si la afirmación anterior es válida.
			assertAll("last name", () -> assertTrue(lastName.startsWith("D")),
					() -> assertTrue(lastName.endsWith("e")));
		});
	}
	
	
	/**
	 * Nueva afirmación En el diseño de la API de aserción, JUnit 5 se ha mejorado
	 * significativamente y aprovecha al máximo las nuevas características de Java
	 * 8, especialmente las expresiones Lambda, y finalmente proporciona una nueva
	 * clase de aserción:org.junit.jupiter.api.Assertions. Muchos métodos de
	 * aserción aceptan parámetros de expresión Lambda. Una ventaja de usar
	 * expresiones Lambda en el mensaje de aserción es que es diferido. Si la
	 * construcción del mensaje es costosa, hacerlo puede ahorrar tiempo y recursos
	 * en cierta medida.
	 */

	@Test
	void groupArrayTest() {
		int[] numbers = { 0, 1, 2, 3, 4 };
		Assertions.assertAll("numbers", () -> Assertions.assertEquals(numbers[1], 1),
				() -> Assertions.assertEquals(numbers[3], 3), () -> Assertions.assertEquals(numbers[4], 4));

	}

}
