package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.service.CalculatorService;
import com.example.demo.service.DataService;

@Service
public class CalculatorServiceImpl implements CalculatorService {
	
	@Autowired
	private DataService dataService;
	 
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }
	@Override
	public int add(int a, int b) {
		return a + b;
	}

	@Override
	public int minus(int a, int b) {
		return a - b;
	}

	@Override
	public int multiply(int a, int b) {
		return a * b;
	}

	@Override
	public int divide(int a, int b) {
		return a / b;
	}

	@Override
	public boolean isLargerNumber(int a, int b) {
		return a > b;
	}

	@Override
	public boolean isSamllerNumber(int a, int b) {
		return a < b;
	}

	@Override
	public double calculateAverage() {
		int[] numbers = dataService.getListOfNumbers();
		double avg = 0;
		for (int i : numbers) {
			avg += i;
		}
		return (numbers.length > 0) ? avg / numbers.length : 0;
	}
}
