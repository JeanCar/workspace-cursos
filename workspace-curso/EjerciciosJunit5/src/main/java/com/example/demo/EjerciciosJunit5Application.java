package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciosJunit5Application {

	public static void main(String[] args) {
		SpringApplication.run(EjerciciosJunit5Application.class, args);
	}

}
