package com.example.demo.service;

public interface CalculatorService {
	double calculateAverage();

	public int add(int a, int b);

	public int minus(int a, int b);

	public int multiply(int a, int b);

	public int divide(int a, int b);

	public boolean isLargerNumber(int a, int b);

	public boolean isSamllerNumber(int a, int b);
}
