package com.example.event.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.example.event.builder.CustomSpringEvent;
import com.example.event.builder.CustomSpringEvent2;

@Component
public class CustomSpringEventPublisher3 {

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public void doStuffAndPublishAnEvent2(final String message) {
		System.out.println("Publishing custom event 2. ");
		CustomSpringEvent2 customSpringEvent = new CustomSpringEvent2(this, message);
		
		applicationEventPublisher.publishEvent(customSpringEvent);
		System.out.println("event was published 2. ");
	}

}
