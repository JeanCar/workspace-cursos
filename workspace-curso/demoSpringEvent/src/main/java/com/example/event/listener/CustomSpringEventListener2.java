package com.example.event.listener;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.example.event.builder.CustomSpringEvent;

@Component
public class CustomSpringEventListener2 {

	@EventListener
	public void processMessageEvent(CustomSpringEvent event) {
		System.out.println("Received spring custom event (forma 02)- " + event.getMessage());
	}

}
