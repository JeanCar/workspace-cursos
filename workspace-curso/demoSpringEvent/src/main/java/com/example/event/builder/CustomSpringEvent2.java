package com.example.event.builder;

import org.springframework.context.ApplicationEvent;

public class CustomSpringEvent2 extends ApplicationEvent{

	
	 /**
	 * 
	 */
	private static final long serialVersionUID = -4182291027044922388L;
	private String message;
	 
	    public CustomSpringEvent2(Object source, String message) {
	        super(source);
	        this.message = message;
	    }
	    public String getMessage() {
	        return message;
	    }
}
