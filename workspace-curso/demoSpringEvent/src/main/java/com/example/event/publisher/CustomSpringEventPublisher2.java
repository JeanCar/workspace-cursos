package com.example.event.publisher;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

import com.example.event.builder.CustomSpringEvent;

@Component
public class CustomSpringEventPublisher2 implements ApplicationEventPublisherAware {

	private ApplicationEventPublisher publisher;

	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	public void publish(final String message) {
		CustomSpringEvent ce = new CustomSpringEvent(this, message);
		publisher.publishEvent(ce);
	}
}
