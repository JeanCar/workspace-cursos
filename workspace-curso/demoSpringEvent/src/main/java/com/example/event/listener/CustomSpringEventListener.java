package com.example.event.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.example.event.builder.CustomSpringEvent;

@Component
public class CustomSpringEventListener implements ApplicationListener<CustomSpringEvent> {

	@Override
	public void onApplicationEvent(CustomSpringEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Received spring custom event (forma01) - " + event.getMessage());

	}

}
