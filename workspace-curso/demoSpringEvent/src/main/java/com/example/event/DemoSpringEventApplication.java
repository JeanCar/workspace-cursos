package com.example.event;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = PruebaEvent.class)
public class DemoSpringEventApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringEventApplication.class, args);
	}
	
	

}
