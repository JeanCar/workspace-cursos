package com.example.event.listener;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.example.event.builder.CustomSpringEvent2;

@Component
public class CustomSpringEventListener3 {

	@EventListener
	public void processMessageEvent(CustomSpringEvent2 event) {
		System.out.println("Received spring custom event (forma 02)- " + event.getMessage());
	}

}
