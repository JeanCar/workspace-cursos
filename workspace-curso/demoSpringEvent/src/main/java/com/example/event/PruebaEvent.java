package com.example.event;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.example.event.publisher.CustomSpringEventPublisher;
import com.example.event.publisher.CustomSpringEventPublisher2;
import com.example.event.publisher.CustomSpringEventPublisher3;

@Configuration
@ComponentScan("com.example.event")
public class PruebaEvent {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(
				PruebaEvent.class);
		
		
//		CustomSpringEventPublisher publishe1 = applicationContext.getBean(CustomSpringEventPublisher.class);
//		publishe1.doStuffAndPublishAnEvent("prueba evento");
		//applicationContext.close();
		

		
		CustomSpringEventPublisher2 publishe2 = applicationContext.getBean(CustomSpringEventPublisher2.class);
		publishe2.publish("prueba evento 2");
//		applicationContext.close();
		
//		CustomSpringEventPublisher3 publishe3 = applicationContext.getBean(CustomSpringEventPublisher3.class);
//		publishe3.doStuffAndPublishAnEvent2("prueba evento 3");
//		applicationContext.close();
	}

}
