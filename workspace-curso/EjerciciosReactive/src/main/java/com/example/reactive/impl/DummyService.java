package com.example.reactive.impl;

import java.util.List;

import com.example.reactive.model.DummyFoo;
import com.example.reactive.model.ObservableFoo;
import com.example.reactive.model.QueryParamInteger;
import com.example.reactive.model.QueryParamString;
import com.example.reactive.model.SingleFoo;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;



/**
 * <br/>
 * Clase Interfaz del Servicio para la logica de negocio que consumira la clase REST
 * DummyController<br/>
 * <b>Class</b>: DummyService<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>EJEMPLOS API</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Sep 4, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
public interface DummyService {

  Observable<DummyFoo> getFoos();

  Single<DummyFoo> getSingle1();

  Single<List<DummyFoo>> getSingle2();

  Single<SingleFoo> getSingle3();

  Single<DummyFoo> getSingle4();

  Single<DummyFoo> getSingle5(QueryParamString param);

  Single<List<SingleFoo>> getSingle6(QueryParamString param);

  Maybe<DummyFoo> getMaybe1(QueryParamInteger param);

  Observable<List<DummyFoo>> getObservable1();

  Observable<DummyFoo> getObservable2();

  Observable<DummyFoo> getObservable3();

  Observable<ObservableFoo> getObservable4();

  Observable<List<ObservableFoo>> getObservable5(QueryParamString param);

  Disposable getObservable6();

  Disposable getObservable7();

  Disposable getObservable8();

  Disposable getObservable9();

  Flowable<DummyFoo> getFlowable1();

  Flowable<DummyFoo> getFlowable2();

  Flowable<DummyFoo> getFlowable3();

  Flowable<DummyFoo> getFlowable4();

  Flowable<DummyFoo> getFlowable5(QueryParamInteger param) throws Exception;

  Completable getCompletable1();


}
