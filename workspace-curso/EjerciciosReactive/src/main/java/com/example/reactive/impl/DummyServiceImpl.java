package com.example.reactive.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import com.example.reactive.model.DummyFoo;
import com.example.reactive.model.ObservableFoo;
import com.example.reactive.model.QueryParamInteger;
import com.example.reactive.model.QueryParamString;
import com.example.reactive.model.SingleFoo;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DummyServiceImpl implements DummyService {

	public Observable<DummyFoo> getFoos() {
		return Observable.just(new DummyFoo("Una nueva esperanza"), new DummyFoo("El imperio contraataca"),
				new DummyFoo("El regreso del jedi")).doOnNext(movie -> {
					log.debug("person : {}", movie.getBar().replace("\n", "").replace("\r", ""));
				});

	}

	/* ********INICIO EJEMPLOS SINGLE******************* */
	// Emite un solo elemento o un evento de error. La versión reactiva de una
	// llamada al método.(PUEDE SER UN OBJETO O LISTA DE OBJETOS)
	// No implementa DoOnNext, pero si el OnSuccess y OnError
	// No acepta nulo o vacio
	@Override
	public Single<DummyFoo> getSingle1() {
		return Single.just(new DummyFoo("Single 01")).doOnSuccess(x -> log.info(String.format("Emitting: '%s'", x)))
				.doOnError(y -> log.info(String.format("Error: '%s'", y)));

	}

	@Override
	public Single<List<DummyFoo>> getSingle2() {
		// Cuando el elemento es una lista de objecto, Arrays.asList
		List<DummyFoo> lista = Arrays.asList(new DummyFoo("Single 02"), new DummyFoo("Single 03"));
		return Single.just(lista).subscribeOn(Schedulers.newThread())
				.doOnSuccess(x -> log.info(String.format("Emitting: '%s'", x)))
				.doOnError(y -> log.info(String.format("Error: '%s'", y)));
	}

	@Override
	public Single<SingleFoo> getSingle3() {
		// Zip combina las emisiones de múltiples Observables juntas a
		// través de una función específica y emite elementos únicos para
		// cada combinación en función de los resultados de esta función
		Single<DummyFoo> single01 = Single.just(new DummyFoo("Single 01"));
		Single<DummyFoo> single02 = Single.just(new DummyFoo("Single 02"));
		Single<DummyFoo> single03 = Single.just(new DummyFoo("Single 03"));
		return Single.zip(single01, single02, single03, (DummyFoo obj1, DummyFoo obj2, DummyFoo obj3) -> {
			SingleFoo obj = new SingleFoo();
			obj.setN1(obj1.getBar());
			obj.setN2(obj2.getBar());
			obj.setN3(obj2.getBar());
			return obj;
		});
	}

	@Override
	public Single<DummyFoo> getSingle4() {
		// Recuperar flujo: En ocasiones no se necesita propagar la excepción sino
		// continuar con la operación
		Single<DummyFoo> llamadadelservicio = Single.error(new IllegalArgumentException("Not supported"));
		// AtlasException.builder().systemCode("500").addDetail().withCode("500")
		// .withDescription("Error Single").push().buildAsSingle();
		return llamadadelservicio.onErrorResumeNext((Throwable ex) -> {
			DummyFoo mr = new DummyFoo();
			// Cuando se necesite obtener codigo y descripcion del error del api consumido
//			if (ex instanceof AtlasException) {
//				AtlasException exp = ((AtlasException) ex);
//				if (exp.getCode() != null) {
//					mr.setBar("Recuperando flujo del codigo de error:" + exp.getCode());
//				} else {
//					mr.setBar("No encontró host");
//				}
//			}
			return Single.defer(() -> Single.just(mr));
		});
	}

	@Override
	public Single<DummyFoo> getSingle5(QueryParamString param) {
		// No utilice los operadores blocking*(), (incluye blockingGet, blockingSingle,
		// blockingIterable y todas sus variaciones),
		// debido a que frena todo el trabajo del hilo actual y lo lleva al hilo del
		// request, el cual no queremos bloquear.
		// Para ello podemos reemplazar con un flatMap para obtener el elemento y
		// trabajar con ello.
		Single<List<DummyFoo>> lista = Single.just(Arrays.asList(new DummyFoo("Single 1"), new DummyFoo("Single 2")));
		// flatMap Elimina un nivel al elemento del Observable Ejemplo
		// Single<List<Object>>, retorna Single<Object>
		return lista.flatMap(m -> {
			m.stream().map(s -> s.getBar()).forEach(System.out::println);
			Optional<DummyFoo> first = m.stream().filter(p -> p.getBar().equals(param.getElemento())).findFirst();
			if (first.isPresent()) {
				return Single.just(new DummyFoo(first.get().getBar()));
			}
			return Single.just(new DummyFoo("No encontró elemento"));
		});
	}

	@Override
	public Single<List<SingleFoo>> getSingle6(QueryParamString param) {
		List<SingleFoo> lista = Arrays.asList(new SingleFoo("Single 01", "Single 02", "Single 03"));
		/*
		 * subscribeOn()El operador le dice a la fuente Observablequé subproceso emitir
		 * y empujar elementos hasta el final Observer (por lo tanto, afecta tanto a los
		 * operadores ascendentes como descendentes). El operador subscribeOn() tendrá
		 * el mismo efecto sin importar dónde lo coloques en la cadena de observables;
		 * sin embargo, no puedes utilizar múltiples operadores subscribeOn() en la
		 * misma cadena. Si incluyes más de un subscribeOn(), tu cadena sólo utilizará
		 * el subscribeOn() más cercano al observable fuente.
		 */

		if (param.getElemento().equalsIgnoreCase("io")) {
			// Comienza con la creación de un trabajador, que puede reutilizarse para otras
			// operaciones.
			// Por supuesto, si no se puede reutilizar (en el caso de trabajos de
			// procesamiento largos),
			// genera un nuevo subproceso (o trabajador) para la operación de manejo
			// Este beneficio también podría ser problemático porque no tiene límites y
			// puede tener un efecto drástico
			// en el rendimiento general si se genera una gran cantidad de hilos
			// (aunque los hilos no utilizados se eliminan después de 60 segundos de
			// inactividad).
			// La implementación está respaldada por un grupo de subprocesos Ejecutor que
			// crecerá según sea necesario
			return Single.just(lista).subscribeOn(Schedulers.io())
					.doOnSuccess(x -> log.info(
							String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
					.doOnError(y -> log.info(
							String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));
		} else if (param.getElemento().equalsIgnoreCase("newthread")) {
			// genera un nuevo hilo para cada observable activo.
			// Esto se puede utilizar para descargar la operación que consume tiempo del
			// hilo principal en otro hilo.
			// Crea y devuelve un programador que crea un nuevo subproceso para cada unidad
			// de trabajo.
			return Single.just(lista).subscribeOn(Schedulers.newThread())
					.doOnSuccess(x -> log.info(
							String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
					.doOnError(y -> log.info(
							String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));
		} else if (param.getElemento().equalsIgnoreCase("computation")) {
			// el número de subprocesos que se pueden usar está fijado al número de núcleos
			// presentes en el sistema.
			// Entonces, si tiene dos núcleos en su móvil, tendrá 2 hilos en el grupo.
			// Esto también significa que si estos dos hilos están ocupados, el proceso
			// tendrá que esperar a que estén disponibles.
			// Devuelve el número de procesadores disponibles para la máquina virtual Java.
			// Este valor puede cambiar durante una invocación particular de la máquina
			// virtual.
			log.info("CPU:" + Runtime.getRuntime().availableProcessors());
			return Single.just(lista).subscribeOn(Schedulers.computation())
					.doOnSuccess(x -> log.info(
							String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
					.doOnError(y -> log.info(
							String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));
		} else if (param.getElemento().equalsIgnoreCase("trampoline")) {
			// este programador ejecuta el código en el subproceso actual. Entonces,
			// si tiene un código ejecutándose en el hilo principal,
			// este planificador agregará el bloque de código en la cola del hilo principal.
			// Los programadores de trampolín son útiles cuando tenemos más de un observable
			// y queremos que se ejecuten en orden.
			return Single.just(lista).subscribeOn(Schedulers.trampoline())
					.doOnSuccess(x -> log.info(
							String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
					.doOnError(y -> log.info(
							String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));
		} else if (param.getElemento().equalsIgnoreCase("single")) {
			// este programador es bastante simple, ya que está respaldado por un solo hilo.
			// Entonces, no importa cuántos observables haya, solo se ejecutará en ese hilo.
			// Se puede considerar como un reemplazo de su hilo principal.
			// Esto se puede usar en escenarios en los que el número de observables podría
			// ser enorme para IO Schedulers
			return Single.just(lista).subscribeOn(Schedulers.single())
					.doOnSuccess(x -> log.info(
							String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
					.doOnError(y -> log.info(
							String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));

		} else if (param.getElemento().equalsIgnoreCase("executor")) {
			// se trata más de un IO Scheduler personalizado.
			// En este planificador, podemos crear un grupo personalizado de subprocesos
			// especificando el tamaño del grupo
			ExecutorService executor = Executors.newFixedThreadPool(10);
			return Single.just(lista).subscribeOn(Schedulers.from(executor))
					.doOnSuccess(x -> log.info(
							String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
					.doOnError(y -> log.info(
							String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));
		}
		return Single.just(lista)
				.doOnSuccess(x -> log.info(
						String.format("Emitting: '%s'", "Hilo1 [" + Thread.currentThread().getName() + "] : " + x)))
				.doOnError(y -> log
						.info(String.format("Error: '%s'", "Hilo [" + Thread.currentThread().getName() + "] : " + y)));

	}

	/* ********FIN EJEMPLOS SINGLE******************* */

	/* ********INICIO EJEMPLOS MAYBE******************* */

	@Override
	public Maybe<DummyFoo> getMaybe1(QueryParamInteger param) {
		// Tiene éxito con un elemento, o ningún elemento, o errores. La versión
		// reactiva de un Optional.
		if (param.getId() == 1) {
			return Maybe.empty();
		} else if (param.getId() == 2) {
			return Maybe.fromCallable(() -> null);
		}
		return Maybe.just(new DummyFoo("Error id")).doOnSuccess(x -> log.info(String.format("exito: '%s'", x)))
				.doOnError(y -> log.info(String.format("Error: '%s'", y)));
	}
	/* ********FIN EJEMPLOS MAYBE******************* */

	/* ********INICIO EJEMPLOS OBSERVABLE******************* */
	@Override
	public Observable<List<DummyFoo>> getObservable1() {
		// Emite 0 o n elementos y finaliza con un evento de éxito o error.
		List<DummyFoo> lista = Arrays.asList(new DummyFoo("Observable 1"), new DummyFoo("Observable 2")); // PUEDE
																											// EMITIR
																											// MAS
																											// ELEMENTOS
		List<DummyFoo> lista2 = Arrays.asList(new DummyFoo("Observable 3"), new DummyFoo("Observable 4")); // PUEDE
																											// EMITIR
																											// MAS
																											// ELEMENTOS
		return Observable.just(lista, lista2).doOnNext(m -> m.stream().forEach(z -> log.info(">" + z.getBar())));
	}

	@Override
	public Observable<DummyFoo> getObservable2() {
		// Merge: Unir dos a mas Observables
		Observable<DummyFoo> obs1 = Observable.just(new DummyFoo("Single 1"), new DummyFoo("Single 2"));
		Observable<DummyFoo> obs2 = Observable.just(new DummyFoo("Single 3"), new DummyFoo("Single 4"));
		return Observable.merge(obs1, obs2).doOnNext(movie -> {
			log.debug(">: {}", movie.getBar().replace("\n", "").replace("\r", ""));
		});
	}

	@Override
	public Observable<DummyFoo> getObservable3() {
		Single<DummyFoo> single1 = Single.just(new DummyFoo("Single 1"));
		Observable<DummyFoo> obs1 = Observable.just(new DummyFoo("Observable 1"), new DummyFoo("Observable 2"));

		/*
		 * De / a | Flowable | Observable | Maybe | Single | Completable | Flowable | |
		 * toObservable () |reduce(),
		 * elementAt(),singleElement()|scan(),elementAt(),first() |ignoreElements() |
		 * Observable | toFlowable() | toObservable () |reduce(),
		 * elementAt(),firstElement() |firstOrError(),last()... |ignoreElements() |
		 * Maybe | toFlowable() | toObservable () | | |toCompletable (x) fromMaybe |
		 * Single | toFlowable() | toObservable () | toMaybe() | |toCompletable (x)
		 * fromSingle | | Completable| toFlowable() | toObservable () | toMaybe()
		 * |toSingle(), toSingleDefault()| |
		 * https://www.vogella.com/tutorials/RxJava/article.html
		 */
		return single1.toObservable().mergeWith(obs1).doOnNext(movie -> {
			log.debug(">: {}", movie.getBar().replace("\n", "").replace("\r", ""));
		});
	}

	@Override
	public Observable<ObservableFoo> getObservable4() {
		Observable<List<DummyFoo>> lista = Observable.just(
				Arrays.asList(new DummyFoo("Observable 1"), new DummyFoo("Observable 2")),
				Arrays.asList(new DummyFoo("Observable 3"), new DummyFoo("Observable 4")));

		return lista.flatMap(m -> {
			ObservableFoo obs = new ObservableFoo();
			List<String> cadena = new ArrayList<>();
			m.stream().forEach(z -> cadena.add(z.getBar()));
			obs.setCadena(cadena);
			return Observable.just(obs).doOnNext(x -> {
				log.debug(">: {}", x.getCadena());
			});

		});
	}

	@Override
	public Observable<List<ObservableFoo>> getObservable5(QueryParamString param) {
		List<ObservableFoo> lista = Arrays.asList(
				new ObservableFoo(Arrays.asList("Observable x1", "Observable x2", "Observable x3", "Observable x4")));
		List<ObservableFoo> lista2 = Arrays.asList(
				new ObservableFoo(Arrays.asList("Observable y1", "Observable y2", "Observable y3", "Observable y4")));
//https://www.baeldung.com/rxjava-schedulers
		if (param.getElemento().equalsIgnoreCase("io")) {
			return Observable.just(lista).observeOn(Schedulers.io())
					.doOnNext(m -> m.stream()
							.forEach(z -> log.info("Hilo1 [" + Thread.currentThread().getName() + "] : " + z)))
					.mergeWith(Observable.just(lista2)).observeOn(Schedulers.newThread()).doOnNext(m -> m.stream()
							.forEach(z -> log.info("Hilo2 [" + Thread.currentThread().getName() + "] : " + z)));

		} else if (param.getElemento().equalsIgnoreCase("newthread")) {
			return Observable.just(lista).observeOn(Schedulers.newThread())
					.doOnNext(m -> m.stream()
							.forEach(z -> log.info("Hilo1 [" + Thread.currentThread().getName() + "] : " + z)))
					.mergeWith(Observable.just(lista2)).observeOn(Schedulers.io()).doOnNext(m -> m.stream()
							.forEach(z -> log.info("Hilo2 [" + Thread.currentThread().getName() + "] : " + z)));

		} else if (param.getElemento().equalsIgnoreCase("computation")) {
			log.info("CPU:" + Runtime.getRuntime().availableProcessors());
			return Observable.just(lista).subscribeOn(Schedulers.computation())
					.doOnNext(m -> m.stream().forEach(z -> log.info(">" + z.getCadena())));

		} else if (param.getElemento().equalsIgnoreCase("trampoline")) {
			return Observable.just(lista).subscribeOn(Schedulers.trampoline())
					.doOnNext(m -> m.stream().forEach(z -> log.info(">" + z.getCadena())));

		} else if (param.getElemento().equalsIgnoreCase("single")) {
			return Observable.just(lista).subscribeOn(Schedulers.single())
					.doOnNext(m -> m.stream().forEach(z -> log.info(">" + z.getCadena())));

		} else if (param.getElemento().equalsIgnoreCase("executor")) {
			ExecutorService executor = Executors.newFixedThreadPool(10);
			return Observable.just(lista).subscribeOn(Schedulers.from(executor))
					.doOnNext(m -> m.stream().forEach(z -> log.info(">" + z.getCadena())));

		}
		return Observable.just(lista);
	}

	@Override
	public Disposable getObservable6() {
		Observable<DummyFoo> obs1 = Observable.just(new DummyFoo("Single 1"), new DummyFoo("Single 2"),
				new DummyFoo("Single 3"), new DummyFoo("Single 4"), new DummyFoo("Single 5"), new DummyFoo("Single 6"),
				new DummyFoo("Single 7"), new DummyFoo("Single 8"), new DummyFoo("Single 9"),
				new DummyFoo("Single 10"));
		// Transforma los elementos emitidos por un Observable en Observables, después
		// aplana las emisiones de esos en un solo Observable.
		final TestScheduler scheduler = new TestScheduler();
		Disposable ok = obs1.flatMap(s -> {
			final int delay = new Random().nextInt(10);
			return Observable.just(s.getBar() + "x").delay(delay, TimeUnit.SECONDS, scheduler);
		})
				// .toList()
				// .collect(Collectors.toList())
				.doOnNext(m -> log.info(">" + m)).subscribe();

		scheduler.advanceTimeBy(1, TimeUnit.MINUTES);
		return ok;
	}

	@Override
	public Disposable getObservable7() {

		// cada vez que el Observable de origen emite un nuevo elemento, se dará de baja
		// y dejará de reflejar el Observable que se generó a partir del elemento
		// emitido previamente, y comenzará a reflejar solo el actual.
		Observable<DummyFoo> obs1 = Observable.just(new DummyFoo("Single 1"), new DummyFoo("Single 2"),
				new DummyFoo("Single 3"), new DummyFoo("Single 4"), new DummyFoo("Single 5"), new DummyFoo("Single 6"),
				new DummyFoo("Single 7"), new DummyFoo("Single 8"), new DummyFoo("Single 9"),
				new DummyFoo("Single 10"));

		final TestScheduler scheduler = new TestScheduler();
		Disposable ok = obs1.switchMap(s -> {
			final int delay = new Random().nextInt(10);
			return Observable.just(s.getBar() + "x").delay(delay, TimeUnit.SECONDS, scheduler);
		})
				// .toList()
				// .collect(Collectors.toList())
				.doOnNext(m -> log.info(">" + m)).subscribe();

		scheduler.advanceTimeBy(1, TimeUnit.MINUTES);
		return ok;
	}

	@Override
	public Disposable getObservable8() {
		// ConcatMap funciona casi igual que flatMap, pero conserva el orden de los
		// elementos.
		// Pero concatMap tiene un gran defecto: espera a que cada observable
		// termine todo el trabajo hasta que se procese el siguiente.
		Observable<DummyFoo> obs1 = Observable.just(new DummyFoo("Single 1"), new DummyFoo("Single 2"),
				new DummyFoo("Single 3"), new DummyFoo("Single 4"), new DummyFoo("Single 5"), new DummyFoo("Single 6"),
				new DummyFoo("Single 7"), new DummyFoo("Single 8"), new DummyFoo("Single 9"),
				new DummyFoo("Single 10"));

		final TestScheduler scheduler = new TestScheduler();
		Disposable ok = obs1.concatMap(s -> {
			final int delay = new Random().nextInt(10);
			return Observable.just(s.getBar() + "x").delay(delay, TimeUnit.SECONDS, scheduler);
		})
				// .toList()
				// .collect(Collectors.toList())
				.doOnNext(m -> log.info(">" + m)).subscribe();

		scheduler.advanceTimeBy(1, TimeUnit.MINUTES);
		return ok;
	}

	@Override
	public Disposable getObservable9() {
		Observable<String> myObservable = Observable.fromIterable(Arrays.asList("A", "B", "C", "D", "E"));
		// Observable<String> myObservable = Observable.fromArray(new String[]{"A", "B",
		// "C", "D", "E"});

		return myObservable.subscribe(System.out::println, Throwable::printStackTrace);
	}

	/* ********FIN EJEMPLOS OBSERVABLE******************* */

	/* ********INICIO EJEMPLOS FLOWABLE******************* */
	@Override
	public Flowable<DummyFoo> getFlowable1() {
		return Flowable.just(new DummyFoo("Flowable 1"), new DummyFoo("Flowable 2"), new DummyFoo("Flowable 3"))
				.onBackpressureBuffer().observeOn(Schedulers.computation());
	}

	@Override
	public Flowable<DummyFoo> getFlowable2() {
		Single<DummyFoo> single1 = Single.just(new DummyFoo("Single 1"));
		Single<DummyFoo> single2 = Single.just(new DummyFoo("Single 2"));
		Single<List<DummyFoo>> single3 = Single.just(Arrays.asList(new DummyFoo("Single 3")));
		return Single.merge(single1, single2, single3.flatMap(m -> Single.just(new DummyFoo(m.get(0).getBar()))));
	}

	@Override
	public Flowable<DummyFoo> getFlowable3() {
		Flowable<DummyFoo> reactiveMethodA = Flowable.just(new DummyFoo("groupA"));
		Flowable<DummyFoo> reactiveMethodB = Flowable.fromArray(new DummyFoo("groupB"));
		return reactiveMethodA.mergeWith(reactiveMethodB);
		// La ejecución anterior se realiza de manera serial
		// debido a que no poseen Schedulers asociados estos métodos reactivos que son
		// parte del merge().
	}

	@Override
	public Flowable<DummyFoo> getFlowable4() {
		Flowable<DummyFoo> groupA = Flowable.just(new DummyFoo("groupA")).subscribeOn(Schedulers.io()); // se requiere
																										// para realizar
																										// el
																										// paralelismo
																										// en el método
																										// merge().

		Flowable<DummyFoo> groupB = Flowable.just(new DummyFoo("groupB")).subscribeOn(Schedulers.io()); // se requiere
																										// para realizar
																										// el
																										// paralelismo
																										// en el método
																										// merge().

		Flowable<DummyFoo> groupC = Flowable.just(new DummyFoo("groupC")).subscribeOn(Schedulers.io()); // se requiere
																										// para realizar
																										// el
																										// paralelismo
																										// en el método
																										// merge().

		return Flowable.merge(groupA, groupB, groupC);
//      Al utilizar el operador subscribeOn() en cada método se realiza un salto hacia otro hilo en la operación que se realiza,
//      dando como resultado una ejecución paralela de los tres métodos reactivos dummyPersonsGroup*(). 
//      En caso de que alguno de los métodos no tenga el subscribeOn(),
//      entonces se ejecutaría en el hilo del request de Netty de nuestro controlador REST.
	}

	@Override
	public Flowable<DummyFoo> getFlowable5(QueryParamInteger param) throws Exception {
		Flowable<DummyFoo> country = Flowable.just(new DummyFoo("Peru"), new DummyFoo("Brasil"),
				new DummyFoo("Ecuador"), new DummyFoo("Argentina"), new DummyFoo("Chile"), new DummyFoo("Colombia"),
				new DummyFoo("Venezuela"), new DummyFoo("Uruguay"));

		if (param.getId() == 1) {
			return country.concatMap(m -> Flowable.just(new DummyFoo("X" + m.getBar()))).delay(1, TimeUnit.SECONDS)
					.doOnNext(System.out::println);

		} else if (param.getId() == 2) {
			return country.flatMap(m -> Flowable.just(new DummyFoo("X" + m.getBar()))).delay(2, TimeUnit.SECONDS)
					.doOnNext(System.out::println);
		} else if (param.getId() == 3) {
			return country.switchMap(m -> Flowable.just(new DummyFoo("X" + m.getBar())).delay(3, TimeUnit.SECONDS))
					.doOnNext(System.out::println);
		}

		return country;
	}

	/* ********FIN EJEMPLOS FLOWABLE******************* */

	/* ********INICIO EJEMPLOS COMPLETABLE******************* */

	@Override
	public Completable getCompletable1() {
		return Completable.fromAction(() -> {
			log.info("Completó evento");

		}).onErrorResumeNext((ex) -> Completable.error(new IllegalArgumentException("Not supported")));
		// AtlasException.builder().addDetail().withCode("400").withComponent("Ejemplo
		// Api")
		// .withDescription("Error Completable").push().buildAsCompletable());
	}

	/* ********FIN EJEMPLOS COMPLETABLE******************* */

}