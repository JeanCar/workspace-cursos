package com.example.reactive.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryParamInteger implements Serializable {

	private static final long serialVersionUID = -8887530613798376846L;
	@NotNull
	@Positive
	private Integer id;

}
