package com.example.reactive.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.reactive.impl.DummyService;
import com.example.reactive.model.DummyFoo;
import com.example.reactive.model.ObservableFoo;
import com.example.reactive.model.QueryParamInteger;
import com.example.reactive.model.QueryParamString;
import com.example.reactive.model.SingleFoo;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

@RestController
@RequestMapping("/scrm-channel/ejemplo-api/v1")
public class DummyController {

	DummyService dummyService;

	public DummyController(DummyService dummyService) {
		this.dummyService = dummyService;
	}

	@GetMapping(value = "/dummys", produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<DummyFoo> dummy() {
		return dummyService.getFoos();
	}

	@GetMapping(value = "/single1", produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<DummyFoo> single1() {
		return dummyService.getSingle1();
	}

	@GetMapping(value = "/single2", produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<List<DummyFoo>> single2() {
		return dummyService.getSingle2();
	}

	@GetMapping(value = "/single3", produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<SingleFoo> single3() {
		return dummyService.getSingle3();
	}

	@GetMapping(value = "/single4", produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<DummyFoo> single4() {
		return dummyService.getSingle4();
	}

	@GetMapping(value = "/single5", produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<DummyFoo> single5(@RequestParam QueryParamString param) {
		return dummyService.getSingle5(param);
	}

	@GetMapping(value = "/single6", produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<List<SingleFoo>> single6(@RequestParam QueryParamString param) {
		return dummyService.getSingle6(param);
	}

	@GetMapping(value = "/maybe1", produces = MediaType.APPLICATION_JSON_VALUE)
	public Maybe<DummyFoo> maybe1(@RequestParam QueryParamInteger param) {
		return dummyService.getMaybe1(param);
	}

	@GetMapping(value = "/observable1", produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<List<DummyFoo>> observable1() {
		return dummyService.getObservable1();
	}

	@GetMapping(value = "/observable2", produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<DummyFoo> observable2() {
		return dummyService.getObservable2();
	}

	@GetMapping(value = "/observable3", produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<DummyFoo> observable3() {
		return dummyService.getObservable3();
	}

	@GetMapping(value = "/observable4", produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<ObservableFoo> observable4() {
		return dummyService.getObservable4();
	}

	@GetMapping(value = "/observable5", produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<List<ObservableFoo>> observable5(@RequestParam QueryParamString param) {
		return dummyService.getObservable5(param);
	}

	@GetMapping(value = "/observable6", produces = MediaType.APPLICATION_JSON_VALUE)
	public Disposable observable6() {
		return dummyService.getObservable6();
	}

	@GetMapping(value = "/observable7", produces = MediaType.APPLICATION_JSON_VALUE)
	public Disposable observable7() {
		return dummyService.getObservable7();
	}

	@GetMapping(value = "/observable8", produces = MediaType.APPLICATION_JSON_VALUE)
	public Disposable observable8() {
		return dummyService.getObservable8();
	}

	@GetMapping(value = "/observable9", produces = MediaType.APPLICATION_JSON_VALUE)
	public Disposable observable9() {
		return dummyService.getObservable9();
	}

	@GetMapping(value = "/flowable1", produces = MediaType.APPLICATION_JSON_VALUE)
	public Flowable<DummyFoo> flowable1() {
		return dummyService.getFlowable1();
	}

	@GetMapping(value = "/flowable2", produces = MediaType.APPLICATION_JSON_VALUE)
	public Flowable<DummyFoo> flowable2() {
		return dummyService.getFlowable2();
	}

	@GetMapping(value = "/flowable3", produces = MediaType.APPLICATION_JSON_VALUE)
	public Flowable<DummyFoo> flowable3() {
		return dummyService.getFlowable3();
	}

	@GetMapping(value = "/flowable4", produces = MediaType.APPLICATION_JSON_VALUE)
	public Flowable<DummyFoo> flowable4() {
		return dummyService.getFlowable4();
	}

	@GetMapping(value = "/flowable5", produces = MediaType.APPLICATION_JSON_VALUE)
	public Flowable<DummyFoo> flowable5(@RequestParam QueryParamInteger param) throws Exception {
		return dummyService.getFlowable5(param);
	}

	@GetMapping(value = "/completable1", produces = MediaType.APPLICATION_JSON_VALUE)
	public Completable completable1() {
		return dummyService.getCompletable1();
	}
}
