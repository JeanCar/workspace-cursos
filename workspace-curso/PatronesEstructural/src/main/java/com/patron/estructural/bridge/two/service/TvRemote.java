package com.patron.estructural.bridge.two.service;

import com.patron.estructural.bridge.two.Tv;

public class TvRemote extends BaseRemote<Tv> {

	public TvRemote(Tv device) {
		super.device = device;
	}

	@Override
	public void price(double price) {
		super.device.setPrice(price);		
	}
}
