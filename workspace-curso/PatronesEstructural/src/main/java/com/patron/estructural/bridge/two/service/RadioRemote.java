package com.patron.estructural.bridge.two.service;

import com.patron.estructural.bridge.two.Radio;

public class RadioRemote extends BaseRemote<Radio> {

	public RadioRemote(Radio device) {
		super.device = device;
	}

	@Override
	public void price(double price) {
		super.device.setPrice(price);		
	}
}
