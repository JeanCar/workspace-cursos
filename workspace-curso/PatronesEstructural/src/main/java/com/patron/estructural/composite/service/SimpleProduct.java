package com.patron.estructural.composite.service;

//Lo primero que observamos es que las clases SimpleProduct extiende 
//de AbstractProduct, luego solo como ejemplo agregamos un atributo
//que solo un producto simple pueda tener, en este caso, agregamos
//la marca, aunque en este ejemplo no nos sirve de nada nos da una
//idea de que podemos personalizar los productos según el tipo.
public class SimpleProduct extends AbstractProduct {

	protected String brand;

	public SimpleProduct(String name, double price, String brand) {
		super(name, price);
		this.brand = brand;
	}
}
