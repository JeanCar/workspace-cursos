package com.patron.estructural.bridge.two.service;

import com.patron.estructural.bridge.two.Device;
import com.patron.estructural.bridge.two.Remote;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class BaseRemote<T> implements Remote<T> {

	protected Device device;

	protected T remote;

	public BaseRemote(T remote) {
		this.remote = remote;
	}

	@Override
	public void power() {
		System.out.println("Remote: power toggle");
		if (device.isEnabled()) {
			device.disable();
		} else {
			device.enable();
		}
	}

	@Override
	public void volumeDown(int volume) {
		System.out.println("Remote: volume down");
		device.setVolume(device.getVolume() - volume);
	}

	@Override
	public void volumeUp(int volume) {
		System.out.println("Remote: volume up");
		device.setVolume(device.getVolume() + volume);
	}

	@Override
	public void channelDown() {
		System.out.println("Remote: channel down");
		device.setChannel(device.getChannel() - 1);
	}

	@Override
	public void channelUp() {
		System.out.println("Remote: channel up");
		device.setChannel(device.getChannel() + 1);
	}
	
	public void mute() {
		System.out.println("Remote: mute");
		device.setVolume(0);
	}
	
	public abstract void price(double price);
}
