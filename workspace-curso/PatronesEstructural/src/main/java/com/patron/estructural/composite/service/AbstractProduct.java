package com.patron.estructural.composite.service;

import lombok.Data;

//Como podemos observar la clase AbstractProduct es una clase abstracta la cual 
//define las características mínimas de un producto, las cuales deben tener todos
//los productos sin importar que sean productos simples o paquetes.
@Data
public abstract class AbstractProduct {
	
	protected String name;
	
	protected double price;

	public AbstractProduct(String name, double price) {
		super();
		this.name = name;
		this.price = price;
	}
}
