package com.patron.estructural.bridge.one;

public interface Remote {
	
	void power();

	void volumeDown();

	void volumeUp();

	void channelDown();

	void channelUp();
}
