package com.patron.estructural.composite;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.patron.estructural.composite.service.AbstractProduct;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaleOrder {

	private long orderId;
	private String customer;
	private Calendar dateTime;
	private List<AbstractProduct> products = new ArrayList<>();

	public SaleOrder(long orderId, String customer) {
		super();
		this.orderId = orderId;
		this.customer = customer;
	}

	public double getPrice() {
		double price = 0d;
		for (AbstractProduct child : products) {
			price += child.getPrice();
		}
		return price;
	}

	public void addProduct(AbstractProduct product) {
		products.add(product);
	}

	public void removeProduct(AbstractProduct product) {
		products.remove(product);
	}

	public void printOrder() {
		NumberFormat formater = new DecimalFormat("###,##0.00");
		System.out.println("Nro Orden: " + orderId + "; Cliente: " + customer);
		for (AbstractProduct prod : products) {
			System.out.println(prod.getName() + " - " + formater.format(prod.getPrice()));

		}
		System.out.println("Total: " + formater.format(getPrice()));
		System.out.println("*******************************************");

	}
}
