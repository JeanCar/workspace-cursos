package com.patron.estructural.composite.service;

import java.util.ArrayList;
import java.util.List;

//Lo primero a observar en la clase CompositeProduct es que extiende 
//también la clase AbstractProduct lo cual nos garantiza que tanto el 
//SimpleProduct (Producto Simple) y el CompositeProduct(Paquete) puedan 
//ser tratados de igual manera y nos nos interesa saber en tiempo de 
//ejecución si es un paquete o un producto simple.
public class CompositeProduct extends AbstractProduct {

	private List<AbstractProduct> products = new ArrayList<>();

	public CompositeProduct(String name) {
		super(name, 0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPrice() {
		double price = 0d;
		for (AbstractProduct child : products) {
			price += child.getPrice();
		}
		return price;
	}

	@Override
	public void setPrice(double price) {
		throw new UnsupportedOperationException();
	}

	public void addProduct(AbstractProduct product) {
		this.products.add(product);
	}

	public boolean removeProduct(AbstractProduct product) {
		return this.products.remove(product);
	}

}
