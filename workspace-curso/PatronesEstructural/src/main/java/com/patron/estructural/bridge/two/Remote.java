package com.patron.estructural.bridge.two;

public interface Remote<T> {
	
	void power();

	void volumeDown(int volume);

	void volumeUp(int volume);

	void channelDown();

	void channelUp();
}
