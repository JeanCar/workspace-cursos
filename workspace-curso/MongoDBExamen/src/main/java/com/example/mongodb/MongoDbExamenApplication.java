package com.example.mongodb;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.example.mongodb.dto.LibroListItem;
import com.example.mongodb.repository.LibroRepository;

@SpringBootApplication
public class MongoDbExamenApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoDbExamenApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(LibroRepository libroRepository, MongoTemplate mongoTemplate) {
		return (args) -> {
//			Libros libro = new Libros();
//			libro.setTitulo("Titulo x");
//			Autor autor = new Autor();
//			autor.setNombre("autor x");
//			autor.setNacionalidad("chile");
//			autor.setCodigo("10245");
//			libro.setAutor(autor);
//			libro.setFecha_creada("2021-05-05");
//			libro.setEditorial("Editorial x");
//			libro.setTotalPaginas(100);
//			libro.setPrecio(654.0);
//			libro.setIdiomas(Arrays.asList("Español", "Ingles"));
//			Libros save = libroRepository.save(libro);
//			if (save != null) {
//				System.out.print("Libro Registrado\n");
//
//			}

			/*************************************************/

			// EJEMPLO DE AGREGACION
			var matchCriteria = new Criteria("autor.nacionalidad").is("Peru");
			MatchOperation match = Aggregation.match(matchCriteria);
			ProjectionOperation projection = Aggregation.project().and("titulo").as("libro").and("autor.nombre")
					.as("autor").and("fecha_creada").as("fecha").and("precio").as("precio").and("idiomas")
					.as("cantidadIdiomas");

			var aggregation = Aggregation.newAggregation(match, projection);
			var result = mongoTemplate.aggregate(aggregation, "Libros", LibroListItem.class);
			var libros = result.getMappedResults();
			Map<String, List<LibroListItem>> group = libros.stream()
					.collect(Collectors.groupingBy(LibroListItem::getAutor));
			group.entrySet().forEach(entry -> {
				System.out.println("Autor: " + entry.getKey());
				System.out.println("Cantidad: " +entry.getValue().size());
				System.out.println("********************************");
			});
			System.out.println("---------------------------------------------------------------------");

			/**********************************************/

			var matchCriteria2 = new Criteria("idiomas").size(2);
			MatchOperation match2 = Aggregation.match(matchCriteria2);
			ProjectionOperation projection2 = Aggregation.project().and("titulo").as("libro").and("autor.nombre")
					.as("autor").and("fecha_creada").as("fecha").and("precio").as("precio").and("idiomas")
					.as("cantidadIdiomas");
			var aggregation2 = Aggregation.newAggregation(match2, projection2);
			var result2 = mongoTemplate.aggregate(aggregation2, "Libros", LibroListItem.class);
			var libros2 = result2.getMappedResults();
			libros2.stream().forEach(object -> {
				System.out.println("Titulo: " + object.getLibro() + "; Autor: " + object.getAutor());
				System.out.println("Fecha: " + object.getFecha());
				System.out.println("Idiomas: " + object.getCantidadIdiomas());
				System.out.println("Precio:" + object.getPrecio());
				System.out.println("********************************");
			});
		};
	}

}
