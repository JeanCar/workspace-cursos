package com.example.mongodb.dto;

import java.util.List;

import lombok.Data;

@Data
public class LibroListItem {

	private String libro;

	private String autor;
	
	private String fecha;

	private double precio;

	private List<String> cantidadIdiomas;
	
}