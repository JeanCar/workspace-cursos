package com.example.mongodb.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Libros {

	@Id
	private String id;

	@Field
	private String titulo;

	@Field
	private Autor autor;

	@Field
	private String fecha_creada;

	@Field
	private String editorial;

	@Field
	private int totalPaginas;

	@Field
	private double precio;

	@Field
	private List<String> idiomas;
}
