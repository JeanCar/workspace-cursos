package com.everis.prueba.consumo.bdd.steps;

import static org.hamcrest.CoreMatchers.is;
import com.everis.prueba.consumo.util.UtilTest;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.rest.SerenityRest;
import org.junit.Assert;



@Slf4j
public class PruebaSteps01 {

  // String uri, header;
  private String respuesta;
  private Integer statusCode;
  //
  // @Qualifier("AbstractBaseStep")
  AbstractBaseSteps abs;

  public PruebaSteps01(AbstractBaseSteps abs) {
    this.abs = abs;
  }
  // @Before
  // public void setUp() {
  // // log.info("PUERTO::{}", abs.getPort());
  // SerenityRest.setDefaultPort(9002);
  //
  // }


  @When("^Se usa el RECURSO (.*) para obtener la consulta$")
  public void cuandoSteps01(String resource) throws Exception {
    log.info("header::{}", abs.getHeader());
    Map<String, String> head = UtilTest.getFileToMap(abs.getHeader());
    // Response res = null;
    // String url = getBasePath() + getUri() + resource;

    // String url = "localhost:" + port() + getUri() + resource;
    // log.info("URL : {}", url);
    // res = given().contentType(Constants.APPLICATION_JSON).headers(head).auth()
    // .oauth2(abs.getToken()).body(requestBody).when().post(url);

    // res = SerenityRest.given().contentType("application/json")
    // // .headers(head)
    // .get(uri+resource);
    log.info("URL:" + abs.getBasePath() + abs.getUri() + resource);
    Response res = SerenityRest.given().contentType("application/json").headers(head)
        // .and()
        // .params(params)
        .when().get(abs.getUri() + resource);
    //
    statusCode = res.getStatusCode();
    respuesta = res.getBody().asString();
    log.info("STATUSCODE: {}", statusCode);
    log.info("RESPUESTA: {}", respuesta);
  }

  @Then("^El RESPONSE muestra el estado (.*), retornando con campos validos$")
  public void entoncesSteps01(int status) throws Throwable {
    Assert.assertThat(status, is(statusCode));
    // Assert.assertNotNull(respuesta);
  }
}
