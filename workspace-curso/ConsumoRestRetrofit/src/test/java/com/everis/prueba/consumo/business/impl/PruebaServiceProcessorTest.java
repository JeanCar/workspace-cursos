package com.everis.prueba.consumo.business.impl;

import static org.mockito.Mockito.when;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.everis.prueba.consumo.config.ApplicationProperties;

import com.everis.prueba.consumo.util.UtilTest;
import com.example.demo.consumo.swagger.first.response.ContratoSwagger;


@RunWith(MockitoJUnitRunner.class)
public class PruebaServiceProcessorTest {

  @Mock
  ApplicationProperties properties;

  @InjectMocks
  PruebaServiceProcessor test;

  UtilTest util;

  @Before
  public void setUp() throws Exception {
	    util = new UtilTest();
  }

  @Test
  public void convertResponseTest() throws Exception {

    String jsonResponse1 = "json/response.json";
    ContratoSwagger res1 = util.getLoanJsonResponse01(jsonResponse1);
    when(properties.getFileProperty()).thenReturn("Prueba");

    Assert.assertNotNull(test.convertResponse(res1));
  }

}
