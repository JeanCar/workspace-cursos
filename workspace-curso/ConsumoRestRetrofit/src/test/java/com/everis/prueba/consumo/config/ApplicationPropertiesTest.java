package com.everis.prueba.consumo.config;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ApplicationPropertiesTest {

  private ApplicationProperties properties;

  @Before
  public void setUp() throws Exception {
    properties = new ApplicationProperties();
  }

  @Test
  public void valorestTest() {
  
    properties.setBaseUrl("");
    properties.setComponent("");
    properties.setConnectTimeout("");
    properties.setReadTimout("");
    properties.setWriteTimout("");
    properties.setFileProperty("");

    Assert.assertNotNull(properties.getBaseUrl());
    Assert.assertNotNull(properties.getComponent());
    Assert.assertNotNull(properties.getConnectTimeout());
    Assert.assertNotNull(properties.getReadTimout());
    Assert.assertNotNull(properties.getWriteTimout());
    Assert.assertNotNull(properties.getFileProperty());
    Assert.assertNotNull(properties.toString());
  }
}
