package com.everis.prueba.consumo.business.impl;

import static org.mockito.Mockito.when;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.everis.prueba.consumo.config.RestClientConfiguration;
import com.everis.prueba.consumo.proxy.PruebaApi;
import com.everis.prueba.consumo.proxy.PruebaApi2;
import com.everis.prueba.consumo.swagger.second.response.ApiResponse;
import com.everis.prueba.consumo.util.UtilTest;
import com.example.demo.consumo.swagger.first.response.ContratoSwagger;
import io.reactivex.Single;


@RunWith(MockitoJUnitRunner.class)
public class PruebaServiceSenderTest {


  @Mock
  PruebaApi api;

  @Mock
  RestClientConfiguration config;

  @InjectMocks
  PruebaServiceSender test;

  UtilTest util;

  @Before
  public void setUp() throws Exception {
    util = new UtilTest();
  }


  @Test
  public void callApiTest() throws Exception {
    String jsonResponse2 = "json/response2.json";
    ContratoSwagger res1 = util.getLoanJsonResponse01(jsonResponse2);

    when(config.getProxy1()).thenReturn(new PruebaApi() {
      @Override
      public Single<ContratoSwagger> getListaUsingGet1() {
        // TODO Auto-generated method stub
        return Single.just(res1);
      }
    });

    Assert.assertNotNull(test.callApi());
  }


  @Test
  public void createObjectTest() throws Exception {
    String jsonResponse2 = "json/response3.json";
    List<ApiResponse> res1 = util.getLoanJsonResponse03(jsonResponse2);

    when(config.getProxy2()).thenReturn(new PruebaApi2() {

      @Override
      public Single<List<ApiResponse>> getListaUsingGet2() {
        // TODO Auto-generated method stub
        return Single.just(res1);
      }
    });
    Assert.assertNotNull(test.callApi2());
  }
}
