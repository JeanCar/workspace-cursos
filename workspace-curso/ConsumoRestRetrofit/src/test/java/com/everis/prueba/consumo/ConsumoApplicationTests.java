package com.everis.prueba.consumo;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature/",
    plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report/cucumber.json"},
    glue = "com.everis.prueba.consumo.bdd.steps", tags = "@prueba")
public class ConsumoApplicationTests {

}
