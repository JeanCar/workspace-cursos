package com.everis.prueba.consumo.business.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.everis.prueba.consumo.model.api.Response;
import com.everis.prueba.consumo.swagger.second.response.ApiResponse;
import com.everis.prueba.consumo.util.UtilTest;
import com.example.demo.consumo.swagger.first.response.ContratoSwagger;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

@RunWith(MockitoJUnitRunner.class)
public class PruebaServiceImplTest {

	@Mock
	PruebaServiceSender sender;

	@Mock
	PruebaServiceProcessor processor;
	@Spy
	PruebaServiceImpl testmock;
	
	@InjectMocks
	PruebaServiceImpl test;

	private UtilTest util;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void obtenerDataTest() throws Exception {
		util = new UtilTest();
		String jsonResponse1 = "json/response.json";
		ContratoSwagger res1 = util.getLoanJsonResponse01(jsonResponse1);

		String jsonResponse2 = "json/response2.json";
		Response res2 = util.getLoanJsonResponse02(jsonResponse2);
		when(sender.callApi()).thenReturn(Single.just(res1));

		when(processor.convertResponse(any())).thenReturn(res2);

		Assert.assertNotNull(test.obtenerData());
		TestObserver<Response> testSingleResult = test.obtenerData().test();
		testSingleResult.awaitTerminalEvent();
		testSingleResult.assertNoErrors();

	}

	@Test
	public void obtenerData2Test() throws Exception {
		util = new UtilTest();
		String jsonResponse1 = "json/response3.json";
		List<ApiResponse> res1 = util.getLoanJsonResponse03(jsonResponse1);
		System.out.println(res1.toString());

		String jsonResponse2 = "json/response2.json";
		Response res2 = util.getLoanJsonResponse02(jsonResponse2);

		
		when(sender.callApi2()).thenReturn(Single.just(res1));

		Map<String, Single<Response>> map = new HashMap<String, Single<Response>>();
		map.put("D47500049012", Single.just(res2));

	//	doReturn(map).when().constructResponseObject2(any());
	//	when(test.constructResponseObject2(any())).thenReturn(map);
//		when(processor.convertResponse(any())).thenReturn(res2);

		Assert.assertNotNull(test.obtenerData2("D47500049012"));
//    TestObserver<Response> testSingleResult = test.obtenerData2("D47500049012").test();
//    testSingleResult.awaitTerminalEvent();
//    testSingleResult.assertNoErrors();

	}
}
