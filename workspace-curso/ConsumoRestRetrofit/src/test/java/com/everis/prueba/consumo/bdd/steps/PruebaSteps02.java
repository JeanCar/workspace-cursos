package com.everis.prueba.consumo.bdd.steps;

import static org.hamcrest.CoreMatchers.is;
import com.everis.prueba.consumo.util.UtilTest;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.rest.SerenityRest;
import org.junit.Assert;

@Slf4j
public class PruebaSteps02 {

  private Integer statusCode;

  // @Qualifier("AbstractBaseStep")
  AbstractBaseSteps abs;

  public PruebaSteps02(AbstractBaseSteps abs) {
    this.abs = abs;
  }

  @When("^Se usa el RECURSO (.*) para evaluar la sintaxis$")
  public void cuandoSteps02(String resource) throws Exception {

    Map<String, String> head = UtilTest.getFileToMap(abs.getHeader());
    log.info("URL:" + abs.getBasePath() + abs.getUri() + resource);
    Response res = SerenityRest.given().contentType("application/json").headers(head)
        // .and()
        // .params(params)
        .when().get(abs.getBasePath() + abs.getUri() + resource);
    statusCode = res.getStatusCode();
    // respuesta = res.getBody().asString();
    // log.info("STATUSCODE: {}", statusCode);
    // log.info("RESPUESTA: {}", respuesta);
  }

  @Then("^El RESPONSE muestra el estado (.*), indicando que no se encontro el recurso$")
  public void entoncesSteps02(int status) throws Throwable {
    Assert.assertThat(status, is(statusCode));
  }
}
