package com.everis.prueba.consumo.bdd.steps;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import com.everis.prueba.consumo.ConsumoApplication;
import com.everis.prueba.consumo.util.UtilTest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

//@ActiveProfiles("DEV")
//@ContextConfiguration
//@SpringBootTest(classes = ConsumoApplication.class,
//    properties = "spring.main.webApplicationType = reactive",
//    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CommonSteps {

  AbstractBaseSteps abs;

  public CommonSteps(AbstractBaseSteps abs) {
    this.abs = abs;

  }

  @Given("^Se realiza un GET a la URI \"(.*)\"$")
  public void dadoSteps(String uri) throws Throwable {
    abs.setUri(uri);
  }

  @And("^Se le envia el header (.*)$")
  public void andSteps(String headerFile) throws Throwable {
    abs.setHeader(UtilTest.fileResourceToString(headerFile));

  }
}
