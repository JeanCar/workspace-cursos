package com.everis.prueba.consumo.model.api.params;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <br/>
 * <b>Class</b>: IdSearchPathParam<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IdSearchPathParam implements CharSequence {

  @ApiParam(name = "paramId", example = "D47500049012", hidden = true, required = true,
      type = "String", value = "Identificador del Dato")
  @Size(min = 5, max = 15)
  @NotNull(message = "No debe ser nulo.")
  @NotEmpty(message = "No debe ser vacio")
  private String paramId;

  @Override
  public int length() {
    // TODO Auto-generated method stub
    return this.paramId.length();
  }

  @Override
  public char charAt(int index) {
    // TODO Auto-generated method stub
    return this.paramId.charAt(index);
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    // TODO Auto-generated method stub
    return this.paramId.subSequence(start, end);
  }
}
