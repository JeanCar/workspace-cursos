package com.everis.prueba.consumo.business.impl;


import com.everis.prueba.consumo.config.RestClientConfiguration;
import com.everis.prueba.consumo.swagger.second.response.ApiResponse;
import com.example.demo.consumo.swagger.first.response.ContratoSwagger;
import io.reactivex.Single;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <br/>
 * <b>Class</b>: PruebaServiceSender<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Banco del Crédito del Perú <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Abr 01, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */
@Component
@Slf4j
public class PruebaServiceSender {

  @Autowired
  RestClientConfiguration api;

  /**
   * callApi method.
   *
   * @return {@link Single}
   */
  public Single<ContratoSwagger> callApi() {
    return api.getProxy1().getListaUsingGet1().doOnSuccess(item -> log.info("Successful"));
  }

  /**
   * callApi method.
   *
   * @return {@link Single}
   */
  public Single<List<ApiResponse>> callApi2() {
    return api.getProxy2().getListaUsingGet2().doOnSuccess(item -> log.info("Successful"));
  }


}
