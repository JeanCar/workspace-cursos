package com.everis.prueba.consumo.business.impl;

import com.everis.prueba.consumo.business.PruebaService;
import com.everis.prueba.consumo.model.api.Product;
import com.everis.prueba.consumo.model.api.Response;
import com.everis.prueba.consumo.swagger.second.response.ApiResponse;
import com.everis.prueba.consumo.util.Constants;
import com.everis.prueba.consumo.util.DataResponse;
import com.everis.prueba.consumo.util.ExceptionUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.reactivex.Single;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <br/>
 * Class service that contains the necessary methods to process the data and business logic that
 * will consume the REST class LoanEvaluationController<br/>
 * <b>Class</b>: PruebaServiceImpl<br/>
 * Copyright: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * Company: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@Service
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class PruebaServiceImpl implements PruebaService {

  @Autowired
  private PruebaServiceSender sender;

  @Autowired
  private PruebaServiceProcessor processor;

  @Autowired
  DataResponse dataresponse;

  /**
   * This method obtenerData.
   *
   * @return {@Link Single}
   */

  @HystrixCommand(fallbackMethod = "getDefault")
  @Override
  public Single<Response> obtenerData() {
    return sender.callApi()
        // .lift(CircuitBreakerOperator.of(circuitBreaker))
        .map(processor::convertResponse)
        // .subscribeOn(Schedulers.computation())
        .onErrorResumeNext(ExceptionUtil::onSingleError)
        // .onErrorResumeNext(e -> {
        // return Single.error(e);
        // })
        .doOnSuccess(succ -> log.info("Successful"))
        .doOnError(err -> log.error("Error al consultar loanEvaluation", err));
  }

  public Single<Response> getDefault() {
    return Single.just(new Response());

  }

  /**
   * callApi method.
   *
   * @param paramId {@Link String}
   * @return {@link Single}
   */
  @Override
  public Single<Response> obtenerData2(String paramId) {
    // 1. Data en Duro
    // Map<String, Single<Response>> responseMapIni =
    // dataresponse.constructResponseObject();
    // return responseMapIni.entrySet().stream().filter(e ->
    // e.getKey().equals(paramId))
    // .map(Map.Entry::getValue).findFirst()
    // .orElse(Single.just(new Response(new Product(), "Data No encuentrada")));

    // 2. Data desde un Mock
    Map<String, Single<Response>> responseMapIni = constructResponseObject2(sender.callApi2());
    return responseMapIni.entrySet().stream().filter(e -> e.getKey().equals(paramId))
        .map(Map.Entry::getValue).findFirst()
        .orElse(Single.just(new Response(new Product(), Constants.ERROR)));

  }

  /**
   * callApi method.
   * 
   * @param res {@Link Single}
   * @return {@link Single}
   */
  public Map<String, Single<Response>> constructResponseObject2(Single<List<ApiResponse>> res) {
    Map<String, Single<Response>> responseMap = new HashMap<String, Single<Response>>();
    responseMap.put("D47500049012",
        res.flatMap(m -> Single.just(m.get(0)).map(x -> processor.converResponse(x))));
    responseMap.put("D47500049013",
        res.flatMap(m -> Single.just(m.get(1)).map(x -> processor.converResponse(x))));
    responseMap.put("D47500049014",
        res.flatMap(m -> Single.just(m.get(2)).map(x -> processor.converResponse(x))));
    return responseMap;
  }

}
