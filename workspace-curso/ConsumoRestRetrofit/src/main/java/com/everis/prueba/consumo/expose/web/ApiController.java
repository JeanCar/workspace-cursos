package com.everis.prueba.consumo.expose.web;

import com.everis.prueba.consumo.business.PruebaService;
import com.everis.prueba.consumo.model.api.Response;
import com.everis.prueba.consumo.model.api.params.IdSearchPathParam;
import io.reactivex.Single;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


/**
 * Main controller that exposes the service through HTTP / Rest for the resources and sub
 * resources<br/>
 * <b>Class</b>: ApiuxController<br/>
 * <b>Copyright</b>: &copy; 2019 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 *         <u>Service Provider</u>: Everis <br/>
 *         <u>Developed by</u>: <br/>
 *         <ul>
 *         <li>Wilder Jean Carlos</li>
 *         </ul>
 *         <u>Changes</u>:<br/>
 *         <ul>
 *         <li>Apr 1, 2019 Creaci&oacute;n de Clase.</li>
 *         </ul>
 * @version 1.0
 */

@RestController
@RequestMapping("/api/everis/prueba/consumo/v1")
@Slf4j
public class ApiController {

  @Autowired
  PruebaService service;


  /**
   * This method obtenerData.
   *
   * @return {@Link Single}
   */
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
	      value = "El servicio realiza la evaluacion de credito efectivo regular de "
	          + "una persona natural",
	      produces = MediaType.APPLICATION_JSON_VALUE,
	      consumes = MediaType.APPLICATION_JSON_VALUE, response = Object.class,
	      httpMethod = "POST", notes = "classpath:/swagger/cliente/notes/info.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecutó satisfactoriamente.",
          response = Response.class),
      @ApiResponse(code = 400, message = "Bad Request"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  @GetMapping(value = "/obtenerDatos")
  public Single<Response> obtenerDatos() {
    return service.obtenerData();
  }

  /**
   * This method obtenerData.
   *
   * @param pathParams {@link IdSearchPathParam}
   * @return {@Link Single}
   */
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(value = "Consulta genérica de colocaciones por cliente (Créditos Consist).",
      produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE,
      response = Object.class, httpMethod = "POST", notes = "classpath:swagger/cliente/notes/info.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecutó satisfactoriamente.",
          response = Response.class),
      @ApiResponse(code = 400, message = "Bad Request"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Failure")})
  @GetMapping(value = "/obtenerDatos2/{id}")
  public Single<Response> obtenerDatos2(@PathVariable("id") IdSearchPathParam pathParams) {
    Single<Response> response = null;
    try {
      log.info("PathVariable: " + pathParams.getParamId());
      response = service.obtenerData2(pathParams.getParamId());
    } catch (Exception e) {
      log.error("Error: " + e);
    }
    return response;
  }

}
