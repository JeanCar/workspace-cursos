package com.example.mongodb.entity;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
	@Id
	private String id;
	
	@Field
	private String district;

	@Field
	private String province;

	@Field
	private String department;

	@Field
	private String number;
}
