package com.example.mongodb.entity;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class College {

	@Id
	private String id;
	
	@Field
	private String name;

	@Field
	private Address address;
	
	@Field
	private Date create_date;
	
}
