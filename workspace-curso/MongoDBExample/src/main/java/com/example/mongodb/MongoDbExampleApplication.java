package com.example.mongodb;

import java.util.ArrayList;
import java.util.Date;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.example.mongodb.dto.CollegeListItem;
import com.example.mongodb.dto.PersonListItem;
import com.example.mongodb.entity.Address;
import com.example.mongodb.entity.College;
import com.example.mongodb.entity.Course;
import com.example.mongodb.entity.Person;
import com.example.mongodb.repository.CollegeRepository;
import com.example.mongodb.repository.CourserRepository;
import com.example.mongodb.repository.PersonRepository;

@SpringBootApplication
public class MongoDbExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoDbExampleApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(PersonRepository personRepository, 
			CourserRepository courseRepository, CollegeRepository collegeRepository,
			MongoTemplate mongoTemplate) {
		return (args) -> {
//			Person p = personRepository.save(new Person("Jean", "Carlos", "Vasquez", 20));
//			if (p != null) {
//				System.out.print("Persona Registrada");
//
//			}
			/*******************************************/
//			var query = new Query();
//			query.addCriteria(
//				new Criteria("age").gte(21).lt(100)
//				.and("firstName").is("Carlos")
//			)
//			.fields()
//				.include("firstName")
//				.include("lastName")
//				.include("age")
//				.exclude("id");
//			var people = mongoTemplate.find(query, Person.class);
//			if(people.isEmpty()) {
//				 System.out.println("No encontró people");
//			}else {
//				people.stream().forEach(person -> System.out.println(person));
//
//			}
			/*******************************************/
//			personRepository.save(new Person("Daniel", "Carbajal", null, 31));
//			System.out.println("Una persona ha sido registarda");
//			var course = new Course();
//			course.name = "Lenguaje De Programacion 2";
//			course.teachersName = "Claudia Zapata";
//			course.StudentIds = new ArrayList<>();
//			course.StudentIds.add("6111bd3bf43eda7051c9d75d");
//			course.StudentIds.add("6111c069ec1798610deb860e");
//			course.StudentIds.add("6111c1650f6adf5d271fd27b");
//			courseRepository.save(course);
			/*******************************************/
//			College college = new College();
//			college.setNombre("uni 1");
//			Address address = new Address();
//			address.setDepartment("depa 1");
//			address.setDistrict("dis 1");
//			address.setNumber("100");
//			address.setProvince("province 1");
//			college.setDirección(address);
//			college.setCreate_date(new Date());
//			College register = collegeRepository.save(college);
//			if (register != null) {
//				System.out.print("Persona Registrada");
//
//			}
			
			/*************************************************/
			
			//EJEMPLO DE AGREGACION
//			var matchCriteria = new Criteria("firstName").is("Carlos");
//			MatchOperation match = Aggregation.match(matchCriteria);			
//			ProjectionOperation projection = Aggregation.project()
//				.and("firstName").as("personName")
//				.and("_id").as("personId");
//			var aggregation = Aggregation.newAggregation(match,projection);
//			var result = mongoTemplate.aggregate(aggregation, "person", PersonListItem.class);
//			var peoples = result.getMappedResults();
//			peoples.stream().forEach(person -> {
//				System.out.println(person.personId);
//				System.out.println(person.personName);
//			});
			
			/**********************************************/
			
//			var matchCriteria2 = new Criteria("address.district").is("Rimac");
//			MatchOperation match2 = Aggregation.match(matchCriteria2);			
//			ProjectionOperation projection2 = Aggregation.project()
//				.and("name").as("nameCollege")
//				.and("address.district").as("districtCollege");
//			var aggregation2 = Aggregation.newAggregation(match2,projection2);
//			var result2 = mongoTemplate.aggregate(aggregation2, "college", CollegeListItem.class);
//			var college2 = result2.getMappedResults();
//			college2.stream().forEach(row -> {
//				System.out.println(row.getNameCollege() + " - " +row.getDistrictCollege() );
//			});
			/***********************************************/
			
			var sortByCount = Aggregation.sortByCount("address.district");
			ProjectionOperation projection = Aggregation.project()
				.and("_id").as("nameCollege")
				.and("count").as("countCollegexDistrict");
			var sortByCountAgregation = Aggregation.newAggregation(sortByCount,projection);
			var result3 = mongoTemplate.aggregate(sortByCountAgregation, "college", CollegeListItem.class);
			var people3 = result3.getMappedResults();
			people3.stream().forEach(row -> {
				System.out.println(row.getNameCollege() + " - " +row.getCountCollegexDistrict() );
			});
		};
	}

}
