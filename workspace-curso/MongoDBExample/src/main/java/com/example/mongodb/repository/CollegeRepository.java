package com.example.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.mongodb.entity.College;

@Repository
public interface CollegeRepository extends MongoRepository<College, String> {

}
