package com.example.mongodb.dto;

import lombok.Data;

@Data
public class CollegeListItem {

	private String nameCollege;
	
	private String districtCollege;
	
	private int countCollegexDistrict;
}
