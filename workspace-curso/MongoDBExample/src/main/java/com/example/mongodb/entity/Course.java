package com.example.mongodb.entity;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {
	@Id
	public String id;
	@Field
	public String name;
	@Field
	public String teachersName;
	@Field
	public ArrayList<String> StudentIds;
}
