package com.example.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.mongodb.entity.Course;

@Repository
public interface CourserRepository extends MongoRepository<Course, String> {

}
