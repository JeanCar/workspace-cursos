package com.example.cucumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciosCucumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjerciciosCucumberApplication.class, args);
	}

}
