package com.example.cucumber.controller;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cucumber.bean.Events;
import com.example.cucumber.bean.Greeting;
import com.example.cucumber.service.EventsService;

@RestController
@RequestMapping(value = "cucumber")
public class CucumberController {

	@Autowired
	EventsService eventsService;

	private final AtomicInteger count = new AtomicInteger();

	private static final String template = "Hello %s";

	@GetMapping("/saludo")
	public Greeting greetin(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(count.getAndIncrement(), String.format(template, name));
	}

	@GetMapping("evento/listar")
	public List<Events> listar() {
		return eventsService.findAll();

	}

	@GetMapping("evento/listar/{filtro}")
	public List<Events> listaPorTitulo(@PathVariable("filtro") String titulo) {
		return eventsService.findByTitulo(titulo);

	}

}
