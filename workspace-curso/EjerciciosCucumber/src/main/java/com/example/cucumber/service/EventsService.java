package com.example.cucumber.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.cucumber.bean.Events;

@Service
public class EventsService {

	public List<Events> findByTitulo(String titulo) {
		return fillData().stream().filter(t -> titulo.equalsIgnoreCase(t.getTitulo())).collect(Collectors.toList());
	}

	public List<Events> findAll() {
		return fillData();
	}
	
	private List<Events> fillData() {
		return List.of(
				new Events(1L, "Titulo 1", new Date()),
				new Events(2L, "Titulo 2", new Date()),
				new Events(3L, "Titulo 3", new Date()),
				new Events(4L, "Titulo 4", new Date()),
				new Events(5L, "Titulo 5", new Date()));

	}

}
