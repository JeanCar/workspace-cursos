package com.example.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature/",
    plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report/cucumber.json"},
    glue = "com.example.cucumber", tags = "@loan-evaluations")
class EjerciciosCucumberApplicationTests {

}
