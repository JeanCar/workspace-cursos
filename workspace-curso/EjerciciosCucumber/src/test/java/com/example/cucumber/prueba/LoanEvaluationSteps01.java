package com.example.cucumber.prueba;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;

import com.example.cucumber.EjerciciosCucumberApplication;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;

@Slf4j
//@ActiveProfiles("BDD")
@ContextConfiguration
@SpringBootTest(classes = EjerciciosCucumberApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class LoanEvaluationSteps01 {
	@Test
	void contextLoads() {
	}
	@LocalServerPort
	private int port;
	private String uri;
	private String token;

	public String getBasePath() {
		return "http://localhost:" + port;
	}

	private String respuesta ="";
	private Integer statusCode=0;

	@When("^Se usa el RECURSO01 (.*) enviandole un header (.*) y request (.*)$")
	public void cuandoSteps01(String resource, String headerFile, String requestFile) throws Exception {
//		String requestHeader = TestUtilsEvaluation.fileResourceToString(headerFile);
//		Map<String, String> head = TestUtilsEvaluation.getFileToMap(requestHeader);
//		String requestBody = TestUtilsEvaluation.fileResourceToString(requestFile);
//		Response res = null;
//		String url = abs.getBasePath() + abs.getUri() + resource;
//		log.info("URL : {}", url);
//		res = given().contentType(Constants.APPLICATION_JSON).headers(head).auth().oauth2(abs.getToken())
//				.body(requestBody).when().post(url);
//
//		statusCode = res.getStatusCode();
//		respuesta = res.getBody().asString();
		System.out.print("prueba");
		log.info("STATUSCODE: {}", statusCode);
		log.info("RESPUESTA: {}", respuesta);
	}

	@Then("^El RESPONSE01 muestra el estado (.*), retornando con campos validos$")
	public void entoncesSteps01(int status) throws Throwable {
//		Assert.assertThat(status, is(statusCode));
//		Assert.assertNotNull(respuesta);
	}
}
