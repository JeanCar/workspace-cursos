#Author: wilderjcvc@ext.zara.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
@loan-evaluations
Feature: Test BDD, Prueba Cucumer
    
    Background:
    Given Que se realiza un GET a la URI "/saludo"
    And Se tiene autorizacion usando el APIKEY com/example/feature/files/token.txt
    
    @Steps01
    Scenario Outline: Steps01 Realiza saludo personal con tu nombre
    When Se usa el RECURSO /saludo enviandole un header <headers> y params <body>
    Then El RESPONSE01 muestra el estado <code>, retornando con campos validos

      Examples:
      | headers                                | body   |code |
      |com/example/feature/files/headers-ok.txt|jean	|200  | 
   