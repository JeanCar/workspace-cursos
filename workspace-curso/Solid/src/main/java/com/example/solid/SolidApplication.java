package com.example.solid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.solid.abierto.cerrado.Audi1;
import com.example.solid.abierto.cerrado.Coche1;
import com.example.solid.abierto.cerrado.CocheAbstract1;
import com.example.solid.abierto.cerrado.Mercedes1;
import com.example.solid.abierto.cerrado.Renault1;
import com.example.solid.substitucion.liskov.Audi2;
import com.example.solid.substitucion.liskov.CocheAbstract2;
import com.example.solid.substitucion.liskov.Ford2;
import com.example.solid.substitucion.liskov.Mercedes2;
import com.example.solid.substitucion.liskov.Renault2;

@SpringBootApplication
public class SolidApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolidApplication.class, args);
	}

	//************* ABIERTO Y CERRADO*********************************
	void principioAbiertoCerradoError() {
		Coche1[] arrayCoches = { new Coche1("Renault"), new Coche1("Audi") };
		imprimirPrecioMedioCoche(arrayCoches);

		// Esto no cumpliría el principio abierto/cerrado, ya que si decidimos añadir un
		// nuevo coche de otra marca:
		// Coche[] arrayCoches = { new Coche("Renault"), new Coche("Audi"), new
		// Coche("Mercedes") };
	}

	public static void imprimirPrecioMedioCoche(Coche1[] arrayCoches) {
		for (Coche1 coche : arrayCoches) {
			if (coche.marca.equals("Renault"))
				System.out.println(18000);
			if (coche.marca.equals("Audi"))
				System.out.println(25000);
			// También tendríamos que modificar el método que hemos creado anteriormente:
			if (coche.marca.equals("Mercedes"))
				System.out.println(27000);
		}

		// Como podemos ver, para cada nuevo coche habría que añadir nueva lógica al
		// método precioMedioCoche().
		// Esto es un ejemplo sencillo, pero imagina que tu aplicación crece y crece
	}

	void principioAbiertoCerradoCorrecto() {
		// Cada coche extiende la clase abstracta Coche e implementa el método abstracto
		// precioMedioCoche().
		CocheAbstract1[] arrayCoches = { new Renault1(), new Audi1(), new Mercedes1() };
		// Así, cada coche tiene su propia implementación del método precioMedioCoche(),
		// por lo que el método
		// imprimirPrecioMedioCoche() itera el array de coches y solo llama al método
		// precioMedioCoche().

		//Ahora, si añadimos un nuevo coche, precioMedioCoche() no tendrá que ser modificado. 
		//Solo tendremos que añadir el nuevo coche al array, cumpliendo así el principio abierto/cerrado.
		imprimirPrecioMedioCoche2(arrayCoches);

	}

	public static void imprimirPrecioMedioCoche2(CocheAbstract1[] arrayCoches) {
		for (CocheAbstract1 coche : arrayCoches) {
			System.out.println(coche.precioMedioCoche());
		}
	}
	
	// ************LISKOV************************************
	
	void principioSubstitucionLiskov() {
		CocheAbstract2[] arrayCoches = {  
		        new Renault2(),
		        new Audi2(),
		        new Mercedes2(),
		        new Ford2()
		};
		imprimirNumAsientos(arrayCoches);
		imprimirNumAsientos2(arrayCoches);

	}
	public static void imprimirNumAsientos(CocheAbstract2[] arrayCoches){  
	    for (CocheAbstract2 coche : arrayCoches) {
	        if(coche instanceof Renault2)
	            System.out.println(numAsientosRenault(coche));
	        if(coche instanceof Audi2)
	            System.out.println(numAsientosRenault(coche));
	        if(coche instanceof Mercedes2)
	            System.out.println(numAsientosRenault(coche));
	        //Esto viola tanto el principio de substitución de Liskov como el de abierto/cerrado.
	        //El programa debe conocer cada tipo de Coche y llamar a su método numAsientos()
	        //asociado.
	        //Así, si añadimos un nuevo coche, el método debe modificarse para aceptarlo.
	        if(coche instanceof Ford2)
	            System.out.println(numAsientosRenault(coche));
	    }
	}
	
	private static char[] numAsientosRenault(CocheAbstract2 coche) {
		// TODO Auto-generated method stub
		return null;
	}

	
	//Como podemos ver, ahora el método imprimirNumAsientos() no necesita saber con qué tipo de 
	//coche va a realizar su lógica, simplemente llama al método numAsientos() del tipo Coche, 
	//ya que por contrato, una subclase de Coche debe implementar dicho método.
	public static void imprimirNumAsientos2(CocheAbstract2[] arrayCoches){  
        for (CocheAbstract2 coche : arrayCoches) {
            System.out.println(coche.numAsientos());
        }
    }

}
