package com.example.solid.inversion.dependencias;

public abstract class Conexion {
	abstract Dato getDatos();

	abstract void setDatos();
}
