package com.example.solid.abierto.cerrado;

public class Coche1 {
	public String marca;

	public Coche1(String marca) {
		this.marca = marca;
	}

	String getMarcaCoche() {
		return marca;
	}
}
