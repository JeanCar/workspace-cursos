package com.example.solid.inversion.dependencias;

public class AccesoADatos {
	private DatabaseService databaseService;

    public AccesoADatos(DatabaseService databaseService){
        this.databaseService = databaseService;
    }

    void getDatos(){
        databaseService.getDatos();
        //...
    }
}
