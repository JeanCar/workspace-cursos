package com.example.solid.responsabilidad.unica;

//Como podemos observar, la clase Coche permite tanto el acceso a las propiedades de la clase como a realizar 
//operaciones sobre la BBDD, por lo que la clase ya tiene más de una responsabilidad.
public class Coche {
	String marca;

	Coche(String marca) {
		this.marca = marca;
	}

	String getMarcaCoche() {
		return marca;
	}

	//ERROR
	//Supongamos que debemos realizar cambios en los métodos que realizan las operaciones a la BBDD.
	//En este caso, además de estos cambios, probablemente tendríamos que tocar los nombres o tipos
	//de las propiedades, métodos, etc, cosa que no parece muy eficiente porque solo estamos 
	//modificando cosas que tienen que ver con la BBDD
	void guardarCocheDB(Coche coche) {
	}
}
