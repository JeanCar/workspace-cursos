package com.example.solid.segregacion.interfaz;

//El problema es que el loro no nada, y el pingüino no vuela, por lo que tendríamos 
//que añadir una excepción o aviso si se intenta llamar a estos métodos. Además, si quisiéramos 
//añadir otro método a la interfaz IAve, tendríamos que recorrer cada una de las clases que la 
//implementa e ir añadiendo la implementación de dicho método en todas ellas. Esto viola el 
//principio de segregación de interfaz, 
//ya que estas clases (los clientes) no tienen por qué depender de métodos que no usan.

public interface IAve {
	
	//ERROR
	//void volar();

	//void comer();

	//Hasta aquí todo bien. Pero ahora imaginemos que queremos añadir a los pingüinos. 
	//Estos son aves, pero además tienen la habilidad de nadar. Podríamos hacer esto:
	//void nadar();
	
	
	//CORRECTO
	void comer();
}
