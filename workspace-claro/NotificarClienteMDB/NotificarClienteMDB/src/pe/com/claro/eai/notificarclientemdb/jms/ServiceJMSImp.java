package pe.com.claro.eai.notificarclientemdb.jms;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.BaseException;
import pe.com.claro.eai.notificarclientemdb.util.ConstantesMDB;

/**
 * @author vmiranda.
 * @clase: ServiceJMSImp.java
 * @descripcion descripcion de la clase.
 * @fecha_de_creacion: dd-mm-yyyy.
 * @fecha_de_ultima_actualizacion: dd-mm-yyyy.
 * @version 1.0
 */
@Component
public class ServiceJMSImp implements ServiceJMS {

	private final Logger logger = LoggerFactory.getLogger(ServiceJMSImp.class);

	@Autowired
	@Qualifier("jmsTemplateNotificarClienteError")
	private JmsTemplate jmsTemplateNotificarClienteError;

	@Override
	public void enviarObjectJMS(String cadenaTransaccion, final SendGwpMessageBeanRecepcion objDto)
			throws BaseException {
		final String transaccion = cadenaTransaccion + "[enviarObjectJMS]-";

		long tiempo = System.currentTimeMillis();

		try {
			this.jmsTemplateNotificarClienteError.send(new MessageCreator() {

				/**
				 * createMessage
				 * 
				 * @param objSesion
				 * @response Message
				 */
				public Message createMessage(Session objSesion) throws JMSException {

					String vCorrelationId = getCorrelationId();
					String vMessageId = vCorrelationId;

					ObjectMessage objMensaje = objSesion.createObjectMessage(objDto);
					objMensaje.setStringProperty(vCorrelationId, objMensaje.toString());
					objMensaje.setJMSCorrelationID(vCorrelationId); // [CorrelationID]
					objMensaje.setJMSMessageID(vMessageId); // [MessageID]

					logger.info(transaccion + "ENVIANDO MENSAJE [JMS], tipo [ObjectMessage]");
					return objMensaje;
				}
			});

			logger.info(transaccion + "Mensaje 'JMS' enviado. Tiempo de demora: [" + (System.currentTimeMillis() - tiempo)
					+ "] milisegundos.");
		} catch (NestedRuntimeException e) {
			this.logger.error(transaccion + "ERROR [NestedRuntimeException] - [enviando de 'JMS'] - ["
					+ e.getMessage() + "] ", e);
			throw new BaseException(e);
		} catch (Exception e) {
			this.logger.error(transaccion + "ERROR [Exception] - [enviando de 'JMS'] - [" + e.getMessage() + "] ",
					e);
			throw new BaseException(e);
		}

	}
	
	/**
	 * getCorrelationId
	 * 
	 * @return String
	 */
	private String getCorrelationId() {
		return (ConstantesMDB.cSIGLACORRELATIONID + new Date().getTime());
	}

}
