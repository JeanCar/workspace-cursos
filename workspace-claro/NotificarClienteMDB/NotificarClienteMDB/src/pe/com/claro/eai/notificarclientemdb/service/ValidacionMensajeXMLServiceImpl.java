package pe.com.claro.eai.notificarclientemdb.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.BaseException;
import pe.com.claro.eai.notificarclientemdb.util.ConstantesMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.eai.notificarclientemdb.util.UtilEAI;
import pe.com.java.jaxb.generator.abdcpproxy.bean.MensajeABDCP;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoCabeceraMensaje;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoCuerpoMensaje;

/**
 * @author mestrella,jramal,jhurtado
 * @clase: ValidacionServiceImpl.java
 * @descripcion descripcion de la clase.
 * @fecha_de_creacion: 25-03-2014.
 * @fecha_de_ultima_actualizacion: 13-02-2015.
 * @version 1.0
 */
@Service
public class ValidacionMensajeXMLServiceImpl implements ValidacionMensajeXMLService{

	private final Logger		logger	= LoggerFactory.getLogger( this.getClass().getName() );
	
	@Autowired
	PropertiesExternosMDB propertiesExterno;
	
	@Autowired
	UtilEAI objUtilEAI;
	
	/**
	 * obtiene los datos del mensaje
	 * @param cadenaTrazabilidadParam
	 * @param xml
	 */
	@Override
	public Map<String, Object> obtenerDatosMensaje(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request) throws BaseException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "obtenerDatosMensaje] ";
		
	//	this.logger.info( cadenaTrazabilidad + "-------- obtenerDatosMensaje [INICIO] --------" );
		
		Map<String, Object> mapRetorno = new HashMap<String, Object>();
		Map<String, Object> mapMensaje = validarMensaje(cadenaTrazabilidad, request.getXmlMsg());
		MensajeABDCP mensajeABDCPDocument = (MensajeABDCP) mapMensaje.get("xmlDocument");
		
		String msisdn = ConstantesMDB.cCADENAVACIA;
		
		TipoCuerpoMensaje cuerpoMensaje = mensajeABDCPDocument.getCuerpoMensaje();
		if (cuerpoMensaje.getAcreditacionPagoDeuda() != null) {
			msisdn = cuerpoMensaje.getAcreditacionPagoDeuda().getNumeracion();
			
		} else if (cuerpoMensaje.getAcreditacionPagoDeudaCedente() != null) {
			msisdn = cuerpoMensaje.getAcreditacionPagoDeudaCedente().getNumeracion();
			
		} else if (cuerpoMensaje.getAsignacionNumeroConsultaPrevia() != null) {
			msisdn = cuerpoMensaje.getAcreditacionPagoDeudaCedente().getNumeracion();
			
		}else if (cuerpoMensaje.getAsignacionNumeroSolicitud() != null) {
			msisdn = cuerpoMensaje.getAsignacionNumeroSolicitud().getNumeracion();
			
		}else if (cuerpoMensaje.getConsultaPreviaEnvioCedente()!= null) {
			msisdn = cuerpoMensaje.getConsultaPreviaEnvioCedente().getNumeracion();
			
		}else if (cuerpoMensaje.getConsultaPreviaObjecionConcesionarioCedente()!= null) {
			msisdn = cuerpoMensaje.getConsultaPreviaObjecionConcesionarioCedente().getNumeracion();
			
		}else if (cuerpoMensaje.getConsultaPreviaEnvioCedente()!= null) {
			msisdn = cuerpoMensaje.getConsultaPreviaEnvioCedente().getNumeracion();
			
		}else if (cuerpoMensaje.getConsultaPreviaObjecionConcesionarioCedente()!= null) {
			msisdn = cuerpoMensaje.getConsultaPreviaObjecionConcesionarioCedente().getNumeracion();
			
		}else if (cuerpoMensaje.getConsultaPreviaRechazadaABDCP()!= null) {
			msisdn = cuerpoMensaje.getConsultaPreviaRechazadaABDCP().getNumeracion();
			
		}else if (cuerpoMensaje.getEnvioSolicitudCedente()!= null) {
			msisdn = cuerpoMensaje.getEnvioSolicitudCedente().getNumeracion();
			
		}else if (cuerpoMensaje.getRechazadaABDCP()!= null) {
			msisdn = cuerpoMensaje.getRechazadaABDCP().getNumeracion();			
		}
		
	//	this.logger.info( cadenaTrazabilidad + "-------- obtenerDatosMensaje [FIN] --------" );
		
		mapRetorno.put( "MSISDN", msisdn );
		return mapRetorno;
		
	}
	
	/**
	 * validar mensaje
	 * @param cadenaTrazabilidadParam
	 * @param xml
	 */
	
	public Map<String, Object> validarMensaje( String cadenaTrazabilidadParam, String xml ) throws BaseException{
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "validarMensaje] ";

		MensajeABDCP objMensajeABDCP = null;
		TipoCabeceraMensaje cabeceraMensaje = null;
		TipoCuerpoMensaje cuerpoMensaje = null;
		String tipoMensaje = ConstantesMDB.cCADENAVACIA;
		Map<String, Object> mapRetorno = null;
		String mensajeErrorEnvioEmail = null;
		List<String> errores = new ArrayList<String>();
		String flujo="[Validar Mensaje]";

		try{

		//	this.logger.info( cadenaTrazabilidad + "-------- validarMensaje [INICIO] --------" );

			mapRetorno = new HashMap<String, Object>();

			try{

				//this.logger.info( cadenaTrazabilidad + "Ejecutando 'validarStringXmlWithXsd' ..." );
				boolean resultValidacionXml = objUtilEAI.validarStringXmlWithXsd( cadenaTrazabilidad, xml, ConstantesMDB.cRUTA_XSD );

				if( resultValidacionXml ){

					//this.logger.info( cadenaTrazabilidad + "Ejecutando 'convertirStringXmlToDocument' ..." );
					Document objDocument = (Document)objUtilEAI.convertirStringXmlToDocument( cadenaTrazabilidad, xml );

					//this.logger.info( cadenaTrazabilidad + "Ejecutando 'convertirDocumentToJaxb' ..." );
					objMensajeABDCP = objUtilEAI.convertirDocumentToJaxb( cadenaTrazabilidad, objDocument );
				}
				else{
					mensajeErrorEnvioEmail = "Error al crear XML desde datos enviados";
					errores.add(flujo+" - "+mensajeErrorEnvioEmail);
					this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail );
				}
			}
			catch( IOException e ){
				mensajeErrorEnvioEmail = "No se logro validar el campo xmlMsg. No se encuentra el archivo schemaABDCP.xsd";
				errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
				this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail + ", causa [" + e + "]" );
			}
			catch( SAXParseException e ){

				try{
					if( e.getMessage() != null ){
						String texto = "for type";
						int indice = e.getMessage().indexOf( texto );
						if( indice != -1 ){
							String campo = e.getMessage().substring( indice + texto.length() + 1, e.getMessage().length() - 1 ).trim();
							if( campo.startsWith( "'" ) && campo.endsWith( "'" ) ){
								mensajeErrorEnvioEmail = "El campo " + campo + " del xmlMsg no es valido con respecto al schemaABDCP.xsd";
								errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
							}
						}
						else{
							texto = "One of";
							indice = e.getMessage().indexOf( texto );
							if( indice != -1 ){
								String campo = e.getMessage().substring( indice + texto.length() + 1, e.getMessage().length() - 13 ).trim();
								if( campo.startsWith( "'{" ) && campo.endsWith( "}'" ) ){
									campo = campo.replace( "'{", "'" ).replace( "}'", "'" );
									mensajeErrorEnvioEmail = "El campo " + campo + " del xmlMsg no es valido con respecto al schemaABDCP.xsd";
									errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
								}
							}
						}
					}
				}
				catch( Exception ex ){
					mensajeErrorEnvioEmail = "El campo xmlMsg no es valido con respecto al schemaABDCP.xsd";
					errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
				}

				if( mensajeErrorEnvioEmail == null ){
					mensajeErrorEnvioEmail = "El campo xmlMsg no es valido con respecto al schemaABDCP.xsd";
					errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
				}

				this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail + ", causa [" + e + "]" );
			}
			catch( SAXException e ){
				mensajeErrorEnvioEmail = "El campo xmlMsg no se pudo validar con el esquema schemaABDCP.xsd";
				errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
				this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail + ", causa [" + e + "]" );
			}
			catch( Exception e ){
				mensajeErrorEnvioEmail = "Error al validar el campo xmlMsg";
				errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
				this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail + ", causa [" + e + "]" );
			}

			// Se valida parte de la estructura del campo xmlMsg
			if( objMensajeABDCP != null ){
				try{
					cabeceraMensaje = objMensajeABDCP.getCabeceraMensaje();
					cuerpoMensaje = objMensajeABDCP.getCuerpoMensaje();
					tipoMensaje = cuerpoMensaje.getIdMensaje();

					if( !propertiesExterno.cOPERADOR_ABDCP.equals( cabeceraMensaje.getRemitente() ) || !propertiesExterno.cOPERADOR_CLARO.equals( cabeceraMensaje.getDestinatario() ) ){
						objMensajeABDCP = null;
						mensajeErrorEnvioEmail = "El Remitente y/o Destinatario del mensaje no corresponde al ABDCP y Claro respectivamente.";
						errores.add(flujo+" - "+mensajeErrorEnvioEmail);
						this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail );
					}

					else if( ( ConstantesMDB.cTIPO_MENSAJE_ANS.equals( tipoMensaje ) && cuerpoMensaje.getAsignacionNumeroSolicitud() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_CPSPR.equals( tipoMensaje ) && cuerpoMensaje.getSolicitudProcedenteConsultaPreviaProcedente() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_ESC.equals( tipoMensaje ) && cuerpoMensaje.getEnvioSolicitudCedente() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_APDC.equals( tipoMensaje ) && cuerpoMensaje.getAcreditacionPagoDeudaCedente() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_RABDCP.equals( tipoMensaje ) && cuerpoMensaje.getRechazadaABDCP() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_FLEP.equals( tipoMensaje ) && cuerpoMensaje.getFueraLimiteEjecutarPortabilidad() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_PEP.equals( tipoMensaje ) && cuerpoMensaje.getProgramadaEjecutarPortabilidad() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_SPR.equals( tipoMensaje ) && cuerpoMensaje.getSolicitudProcedente() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_CNPF.equals( tipoMensaje ) && cuerpoMensaje.getCancelacionNoProgramacionFecha() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_AR.equals( tipoMensaje ) && cuerpoMensaje.getAceptacionRetorno() == null ) || ( ConstantesMDB.cTIPO_MENSAJE_DR.equals( tipoMensaje ) && cuerpoMensaje.getDenegacionRetorno() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_SR.equals( tipoMensaje ) && cuerpoMensaje.getSolicitudRetorno() == null ) || ( ConstantesMDB.cTIPO_MENSAJE_NI.equals( tipoMensaje ) && cuerpoMensaje.getNoIntegridad() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_NE.equals( tipoMensaje ) && cuerpoMensaje.getNotificacionError() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_ANCP.equals( tipoMensaje ) && cuerpoMensaje.getAsignacionNumeroConsultaPrevia() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_CPRABD.equals( tipoMensaje ) && cuerpoMensaje.getConsultaPreviaRechazadaABDCP() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_ECPC.equals( tipoMensaje ) && cuerpoMensaje.getConsultaPreviaEnvioCedente() == null )
							|| ( ConstantesMDB.cTIPO_MENSAJE_CPPR.equals( tipoMensaje ) && cuerpoMensaje.getConsultaPreviaProcedente() == null )

					){
						objMensajeABDCP = null;
						mensajeErrorEnvioEmail = "Se ha recibido un mensaje que no corresponde a los mensajes definidos y/o el atributo IdMensaje no corresponde con el contenido del Cuerpo del Mensaje.";
						errores.add(flujo+" - "+mensajeErrorEnvioEmail);
						this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail );
					}
				}
				catch( Exception e ){
					mensajeErrorEnvioEmail = "Error al validar estructura del mensaje.";
					errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
					this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail, e );
				}
			}
		}
		catch( Exception e ){
			mensajeErrorEnvioEmail = "Error al validar mensaje.";
			errores.add(flujo+" - "+mensajeErrorEnvioEmail + " Excepcion :"+e.getMessage());
			this.logger.error( cadenaTrazabilidad + mensajeErrorEnvioEmail + "ERROR: [Exception] - [" + e.getMessage() + "] ", e );
		}
		finally{
		//	this.logger.info( cadenaTrazabilidad + "-------- validarMensaje [FIN] --------" );
		}

		mapRetorno.put( "xmlDocument", objMensajeABDCP );
		mapRetorno.put( "mensajeErrorEnvioEmail", mensajeErrorEnvioEmail );
		mapRetorno.put( "listaErrores",errores);
		return mapRetorno;
	}

	
}