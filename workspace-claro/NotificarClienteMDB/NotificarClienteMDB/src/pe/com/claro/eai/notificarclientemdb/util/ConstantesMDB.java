package pe.com.claro.eai.notificarclientemdb.util;

import java.math.BigDecimal;
import java.util.ResourceBundle;

public class ConstantesMDB {

    private static ResourceBundle localProp = ResourceBundle.getBundle("application");

    public static final Integer CODIGO_ESTANDAR_EXITO_0 = Integer.parseInt(localProp.getString("codigo.estandar.exito.0"));
    public static final Integer CODIGO_ESTANDAR_EXITO_1 = Integer.parseInt(localProp.getString("codigo.estandar.exito.1"));
    public static final String CONSULTA_CLAVES_METODO = localProp.getString("ws.consulta.clave.metodo");
    public static final String CACHE_KEY_CREDENCIALES = localProp.getString("cache.key.credenciales");
    public static final String ERROR_CONEXION_NO_ENCONTRADA = localProp.getString("error.conexion.no.encontrada");

	
	public static final String STR_INDICADOR_ENTER= "|";
	public static final String CONSTANTE_VACIA = "";
	
	// ---------- [JMS] ----------//
		public static final String		cSIGLACORRELATIONID						= "CLARO";
		// ---------- [JMS] ----------//

		// //PARAMETROS [VARIOS]:
		public static final String		cCODIGO_INTERNACIONAL					= "51";
		public static final String		cESTADOOK								= "0";
		public static final String		cESTADONOK								= "1";
		public static final String		cESTADOOKMSJ							= "OK";
		public static final String		cESTADONOKMSJ							= "NOK";
		public static final String		cCADENAREPLACE01						= "XXX";
		public static final String		cCADENAREPLACE02						= "YYY";
		public static final String		cCADENAVACIA							= "";
		public static final String		cSEPARADOR_CADENA						= ",";
		public static final String		cSEPARADOR_GUION						= " - ";
		public static final String		cSEPARADOR_DOS_PUNTOS					= " : ";

		// Proceso Consulta Previa(todos son nuevos)
		public static final String		cTIPO_MENSAJE_ANCP						= "ANCP";
		public static final String		cTIPO_MENSAJE_CPRABD					= "CPRABD";
		public static final String		cTIPO_MENSAJE_ECPC						= "ECPC";
		public static final String		cTIPO_MENSAJE_CPPR						= "CPPR";

		// Proceso Solicitud de Portabilidad
		public static final String		cTIPO_MENSAJE_ANS						= "ANS";
		public static final String		cTIPO_MENSAJE_RABDCP					= "RABDCP";
		public static final String		cTIPO_MENSAJE_ESC						= "ESC";
		public static final String		cTIPO_MENSAJE_SPR						= "SPR";
		public static final String		cTIPO_MENSAJE_CNPF						= "CNPF";
		public static final String		cTIPO_MENSAJE_FLEP						= "FLEP";
		public static final String		cTIPO_MENSAJE_PEP						= "PEP";
		// Nuevos para portabilidad movil y fija.
		public static final String		cTIPO_MENSAJE_CPSPR						= "CPSPR";
		public static final String		cTIPO_MENSAJE_APDC						= "APDC";

		// Proceso solicitud de Retorno
		public static final String		cTIPO_MENSAJE_SR						= "SR";
		public static final String		cTIPO_MENSAJE_AR						= "AR";
		public static final String		cTIPO_MENSAJE_DR						= "DR";

		// Proceso de Denegación
		public static final String		cTIPO_MENSAJE_NI						= "NI";
		public static final String		cTIPO_MENSAJE_NE						= "NE";

		public static final String		cTIPO_NUMSECUENCIAL						= "00";

		public static final String		cNUEVA_LINEA							= "\n";

		public static final String		cRUTA_XSD								= "xsd/schemaABDCP.xsd";

		public static final String		cTIPO_MENSAJE							= "tipoMensaje";
		public static final String		cDESTINATARIO							= "destinatario";
		public static final String		cREMITENTE								= "remitente";
		public static final String		cFEC_CREA_MSJ							= "fechaCreacionMensaje";
		public static final String		cID_MENSAJE								= "identificadorMensaje";
		public static final String		cID_PROCESO								= "idProceos";
		public static final String		cOBSERVACIONES							= "observaciones";
		public static final String		cCAUSA_OBJECION							= "causaObjecion";
		public static final String		cFEC_VENCIMIENTO						= "fechaVencimiento";
		public static final String		cMONEDA									= "moneda";
		public static final String		cMONTO									= "monto";
		public static final String		cNUMERACION								= "numeracion";
		public static final String		cFEC_EJEC_PORTA							= "fechaEjecucionPortabilidad";
	    public static final String 		cFECHA_ACTIVACION						= "fechaActivacion";
	    public static final String		cFECHA_PAGO								= "fechaPago";
	    public static final String		cENTIDAD_PAGO							= "entidadPago";
	    public static final String		cDOCUMENTO_ADJUNTO						= "documentoAdjunto";
	    public static final String		cNUMERO_OPERACION_PAGO					= "numeroOperacionPago";

		public static final String		cSEPARADOR_FTP							= "/";
		public static final String		cCORCHETE_DERECHO						= "]";
		public static final BigDecimal	cACTIVO_BD								= new BigDecimal( 1 );

		public static final String		cCOD_ERROR_OK							= "0";

		// VALIDACION para portabilidad de numero

		// Mensajes
		public static final String		cFLUJO_EXITOSO_CODIGO					= "0";
		public static final String		cFLUJO_EXITOSO_MENSAJE					= "El flujo de validación fue exitoso.";

		public static final String		cSUSPENDIDO_CODIGO						= "REC01PRT01";
		public static final String		cSUSPENDIDO_MENSAJE						= "El abonado ha suspendido el servicio.";

		public static final String		cNUM_NO_PERTENECE_CONCESIONARIO_CODIGO	= "REC01PRT05";
		public static final String		cNUM_NO_PERTENECE_CONCESIONARIO_MENSAJE	= "El numero telefonico no pertenece al Concesionario cedente indicado.";

		public static final String		cNUM_NO_PERTENECE_TIPOSERVICIO_CODIGO	= "REC01PRT06";
		public static final String		cNUM_NO_PERTENECE_TIPOSERVICIO_MENSAJE	= "El numero telefonico no pertenece al Tipo de Servicio indicado.";

		public static final String		cNUM_NO_PERTENECE_IDLEGAL_CODIGO		= "REC01PRT07";
		public static final String		cNUM_NO_PERTENECE_IDLEGAL_MENSAJE		= "El numero telefonico no corresponde al documento de ID legal indicado, o la parte solicitante ya no es un abonado.";

		public static final String		cNUM_NO_PERTENECE_MODALIDAD_CODIGO		= "REC01PRT08";
		public static final String		cNUM_NO_PERTENECE_MODALIDAD_MENSAJE		= "El numero telefonico no corresponde a la modalidad indicada (Prepago/Postpago).";

		public static final String		cABONADO_TIENE_DEUDA_CODIGO				= "REC01PRT09";
		public static final String		cABONADO_TIENE_DEUDA_MENSAJE			= "El abonado tiene deuda exigible.";

		public static final String		cERROR_INTERNO_AL_VALIDAR				= "-1";

		public static final String		cVALIDACION_BSCS_OK						= "0";
		public static final String		cNUMERO_NO_EXISTEN_EN_BSCS				= "1";
		public static final String		cESTADO_SUSPENDIDO						= "s";
		public static final String		cMOTIVO_SUSP_COB						= "SUSP_COB";

		// clarify
		public static final String		cNUMERO_SI_CORRESPONDE_AL_DOCUMENTO		= "1";
		public static final String		cNUMERO_NO_CORRESPONDE_AL_DOCUMENTO		= "0";
		public static final String		cTIPO_DOCUMENTO_NO_VALIDO				= "-1";
		public static final String		cNO_ESTA_BLOQUEADO						= "false";
		public static final String		cCLARIFY_ESTADO_PREACTIVO				= "PREACTIVE";
		public static final String		cCLARIFY_ESTADO_RECICLADO				= "RECICLE";

		// BSCS
		public static final String		cNUMERO_NO_EXISTE_EN_BSCS				= "1";
		public static final String		cTIPODOC_NO_PERTENECE_AL_CLIENTE		= "2";
		public static final String		cTIPODOC_INVALIDO						= "3";

		// SGA
		public static final String		cVALIDACION_SGA_OK						= "0";
		public static final String		cNUMERO_NO_EXISTE_EN_SGA				= "1";
		public static final String		cNUMERO_NO_PERTENECE_AL_CLIENTE			= "2";
		public static final String		cNUMERO_ESTA_SUSPENDIDO					= "3";
		public static final String		cNUMERO_ESTA_SUSPENDIDO_POR_DEUDA		= "4";
		public static final String		cNUMERO_NO_RETORNO_DATOS				= "-1";

		public static final String		cCOMANDOENDOS							= "cmd /c \"del /f /q XXX\"";
		public static final String		cCOMANDOENLINUX							= "rm -r XXX\"";

		public static final String		cSOWINDOWS								= "WINDOWS";
		public static final String		cSOLINUX								= "LINUX";

		// OAC

		public static final String		cNO_SE_ENCONTRO_EN_OAC					= "1";
		public static final String		cCONSULTA_OAC_CORRECTA					= "0";
		public static final String		cERROR_GENERICO_OAC						= "-1";

		public static final String		cLINEA_SIN_DEUDA						= "0";

		// Validacion Programacion Portabilidad
		public static final String		cFLAG_RECEPCION							= "0";

		public static final String		cEJECUCION_CORRECTA						= "0";

		public static final String		cTIPO_CLIENTE_ESPECIAL					= "1";
		public static final String		cTIPO_CLIENTE_NO_ESPECIAL				= "2";

		public static final String		cPASSWORD_REGEX							= ".";
		public static final String		cPASSWORD_REPLACEMENT					= "*";

		public static final String		cINDICADOR_MOVIL						= "9";
		public static final String		cTIPO_SERVICIO_MOVIL					= "1";
		public static final String		cTIPO_SERVICIO_FIJO						= "2";

		// pvu response
		public static final String		cCONSULTA_REALIZADA_CORRECTAMENTE		= "1";
		public static final String		cPROGRAMACION_REALIZADO_CORRECTAMENTE	= "0";
		public static final int			cTIPO_PROGRAMACION_OK					= 0;

		public static final String		cFORMATO_FECHA_EVALUAR					= "dd-MM-yyyy";
		public static final String		cFORMATO_HORA_EVALUAR					= "HH:mm:ss";

		public static final String		cTIPO_MONEDA_SOLES_OAC					= "604";
		public static final String		cTIPO_MONEDA_DOLARES_OAC				= "840";

		public static final String		cTIPO_MONEDA_SOLES_ABDCP				= "01";
		public static final String		cTIPO_MONEDA_DOLARES_ABDCP				= "02";

		public static final int			cCANTIDAD_DIAS_SABADO_A_LUNES			= 2;

		public static final int			cCANTIDAD_DIAS_AUMENTAR_FIJO			= 1;
		public static final int			cCANTIDAD_DIAS_PROGRAMAR				= 1;
		public static final int			cCANTIDAD_DIAS_AUMENTAR					= 1;

		public static final int			cCOMPARACION_FECHA_ACTUAL_IGUAL			= 0;
		public static final int			cCOMPARACION_FECHA_ACTUAL_MENOR			= -1;
		public static final int			cCOMPARACION_FECHA_ACTUAL_MAYOR			= 1;

		public static final int			cCONSULTA_IN_OFFER						= 1;
		public static final int			cCONSULTA_IN_LOCKED						= 2;
		public static final int			cCONSULTA_IN_LAST_KNOWN_PERIOD			= 3;
		public static final int			cCONSULTA_IN_FIRST_CALL_DATE			= 4;
	        
	        public static final String		cINTERRUPTOR_IN_CONECTOR				= "0";
	    //map - errores procesarconsultaprevia

	    public static final String cERROR_REGISTRAR_MENSAJE_ANCP 										= "ErrorRegistrarMensajeANCP";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_ANCP								= "ErrorActualizarSolicitudMensajeANCP";
	    public static final String cERROR_REGISTRAR_MENSAJE_CPRABD										= "ErrorRegistrarMensajeCPRABD";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_CPRABD							= "ErrorActualizarSolicitudMensajeCPRABD";
	    public static final String cERROR_REGISTRAR_MENSAJE_ECPC										= "ErrorRegistrarMensajeECPC";
	    public static final String cERROR_REGISTRAR_SOLICITUD_MENSAJE_ECPC								= "ErrorRegistrarSolicitudMensajeECPC";
		public static final String cERROR_ENVIAR_MENSAJE_GWP_LISTENER_ENVIO_ECPC						= "ErrorEnviarMensajeGwpListenerEnvioECPC";
		public static final String cERROR_VALIDACION_NUMERO_PARA_PORTABILIDAD_ECPC					    = "ErrorValidacionNumeroParaPortabilidadECPC";
		public static final String cERROR_VALIDACION_NUMERO_PARA_PORTABILIDAD_ESC					    = "ErrorValidacionNumeroParaPortabilidadESC";
		public static final String cERROR_VALIDACION_MENSAJE										    = "ErrorValidacionMensaje";
		public static final String cERROR_HILO_FLUJO_VALIDACION_CP										= "ErrorHiloFlujoValidacion";
	    public static final String cERROR_OBTENER_ID_MENSAJE_CP											= "ErrorObtenerIDMensaje";
	    public static final String cERROR_REGISTRAR_MENSAJE_CPPR										= "ErrorRegistrarMensajeCPPR";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_CPPR								= "ErrorActualizarSolicitudMensajeCPPR";
	    public static final String cERROR_REGISTRAR_LOG_ESTADOS_PROCESAR_CONSULTA_PREVIA				= "ErrorRegistrarLogEstadosProcesarConsultaPrevia";
	    public static final String cERROR_ENVIAR_MENSAJE_A_LA_COLA											= "ErrorEnviarMensajeALaCola";
	    
	    //map - errores procesarsolicituddeportabilidad

	    public static final String cERROR_REGISTRAR_MENSAJE_ANS											= "ErrorRegistrarMensajeANS";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_ANS								= "ErrorActualizarSolicitudMensajeANS";
	    public static final String cERROR_REGISTRAR_MENSAJE_RABDCP										= "ErrorRegistrarMensajeRABDCP";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_RABDCP							= "ErrorActualizarSolicitudMensajeRABDCP";
	    public static final String cERROR_REGISTRAR_MENSAJE_ESC											= "ErrorRegistrarMensajeESC";
	    public static final String cERROR_REGISTRAR_SOLICITUD_MENSAJE_ESC								= "ErrorRegistrarSolicitudMensajeESC";
	    public static final String cERROR_ENVIAR_MENSAJE_GWP_LISTENER_ENVIO_ESC							= "ErrorEnviarMensajeGwpListenerEnvioESC";
	    public static final String cERROR_HILO_FLUJO_VALIDACION_SP										= "ErrorHiloFlujoValidacion";
	    public static final String cERROR_OBTENER_ID_MENSAJE_SP											= "ErrorObtenerIDMensaje";
	    public static final String cERROR_REGISTRAR_MENSAJE_SPR											= "ErrorRegistrarMensajeSPR";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_SPR								= "ErrorActualizarSolicitudMensajeSPR";
	    public static final String cERROR_ENVIAR_MENSAJE_GWP_LISTENER_ENVIO_SPR							= "ErrorEnviarMensajeGwpListenerEnvioSPR";
	    public static final String cERROR_VALIDAR_NUMERO_PARA_PROGRAMACION_PORTABILIDAD					= "ErrorValidarNumeroParaProgramacionPortabilidad";
	    public static final String cERROR_REGISTRAR_MENSAJE_CNPF										= "ErrorRegistrarMensajeCNPF";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_CNPF								= "ErrorActualizarSolicitudMensajeCNPF";
	    public static final String cERROR_REGISTRAR_MENSAJE_FLEP										= "ErrorRegistrarMensajeFLEP";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_FLEP								= "ErrorActualizarSolicitudMensajeFLEP";
	    public static final String cERROR_OBTENER_DATOS_DE_NUMERO_SEGUN_IDENTIFICADOR_PROCESO			= "ErrorObtenerDatosDeNumeroSegunIdentificadorProceso";
	    public static final String cERROR_OBTENER_TIPO_PLAN												= "ErrorObtenerTipoPlan";
	    public static final String cERROR_REGISTRAR_MENSAJE_PEP											= "ErrorRegistrarMensajePEP";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_PEP								= "ErrorActualizarSolicitudMensajePEP";
	    public static final String cERROR_ACTUALIZAR_TIPO_PLAN											= "ErrorActualizarTipoPlan";
	    public static final String cERROR_REGISTRAR_MENSAJE_CPSPR										= "ErrorRegistrarMensajeCPSPR";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_CPSPR							= "ErrorActualizarSolicitudMensajeCPSPR";
	    public static final String cERROR_ENVIAR_MENSAJE_GWP_LISTENER_ENVIO_CPSPR						= "ErrorEnviarMensajeGwpListenerEnvioCPSPR";
	    public static final String cERROR_REGISTRAR_MENSAJE_APDC										= "ErrorRegistrarMensajeAPDC";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_APDC								= "ErrorActualizarSolicitudMensajeAPDC";
	    public static final String cERROR_COPIAR_ARCHIVO_AL_FTP											= "ErrorCopiarArchivoAlFTP";
	    public static final String cERROR_REGISTRAR_LOG_ESTADOS_PROCESAR_SOLICITUD_DE_PORTABILIDAD		= "ErrorRegistrarLogEstadosProcesarSolicitudDePortabilidad";
	    public static final String cERROR_ENVIAR_MENSAJE_GWP_LISTENER_ENVIO_APD							= "ErrorEnviarMensajeGwpListenerEnvioAPD";
	    public static final String cERROR_ENVIO_APD_SEC													= "ErrorEnvioApdSec";	
	    public static final String cERROR_ENVIO_APD_IDS													= "ErrorEnvioApdIds";
	    public static final String cSEC																	= "XXX";
	    public static final String cMOTIVO																= "YYY";

	    //map - errores procesarsolicitudderetorno

	    public static final String cERROR_REGISTRAR_MENSAJE_AR											= "ErrorRegistrarMensajeAR";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_AR								= "ErrorActualizarSolicitudMensajeAR";
	    public static final String cERROR_REGISTRAR_MENSAJE_DR											= "ErrorRegistrarMensajeDR";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_DR								= "ErrorActualizarSolicitudMensajeDR";
	    public static final String cERROR_REGISTRAR_MENSAJE_SR											= "ErrorRegistrarMensajeSR";
	    public static final String cERROR_REGISTRAR_SOLICITUD_MENSAJE_SR								= "ErrorRegistrarSolicitudMensajeSR";
	    public static final String cERROR_REGISTRAR_LOG_ESTADOS_SOLICITUD_RETORNO						= "ErrorRegistrarLogEstadosSolicitudRetorno";

	    //map - errores procesardenegacion

	    public static final String cERROR_REGISTRAR_MENSAJE_NI											= "ErrorRegistrarMensajeNI";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_NI								= "ErrorActualizarSolicitudMensajeNI";
	    public static final String cERROR_REGISTRAR_MENSAJE_NE											= "ErrorRegistrarMensajeNE";
	    public static final String cERROR_ACTUALIZAR_SOLICITUD_MENSAJE_NE								= "ErrorActualizarSolicitudMensajeNE";
	    public static final String cERROR_REGISTRAR_LOG_ESTADOS_PROCESAR_DENEGACION						= "ErrorRegistrarLogEstadosProcesarDenegacion";
	    
	    //map - errores gwplistenerenvio

	    public static final String cERROR_MENSAJE_GWP_LISTENER_ENVIO									= "ErrorEnviarMensajeGwpListenerEnvio";
	    
	    //map - errores cola

	    public static final String cERROR_MENSAJE_A_LA_COLA												= "ErrorEnviarMensajeALaCola";	
	    
	    //enviar correo excepciones

	    public static final String cENVIO_CORREO_ACTIVADO							= "1";
	    public static final int cSIZE_FECHA_ACTIVACION								=8;
	    
	    public static final String cTIPO_SERVICIO1	= "1"; 
	    public static final String cTIPO_SERVICIO2	= "2";
	    public static final String cTIPO_PORTABILIDAD_1	= "01"; 
	    public static final String cTIPO_PORTABILIDAD_2	= "02";
	    public static final int cNUMERO_UNO	= 1; 
	    public static final int cNUMERO_OCHO	= 8;

}
