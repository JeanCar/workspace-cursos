package pe.com.claro.eai.notificarclientemdb.exception;

public class ReintentosException extends BaseException {

	private static final long serialVersionUID = 1L;

	public ReintentosException(String string) {
		super(string);
	}

	public ReintentosException(Exception exception) {
		super(exception);
	}

	public ReintentosException(String string, Exception exception) {
		super(string, exception);
	}

	public ReintentosException(String code, String message, Exception exception) {
		super(code, message, exception);
	}

	public ReintentosException(String code, String message) {
		super(code, message);
	}
}
