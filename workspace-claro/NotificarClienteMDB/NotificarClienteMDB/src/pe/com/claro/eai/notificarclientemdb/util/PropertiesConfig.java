package pe.com.claro.eai.notificarclientemdb.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfig {
	static private Properties properties = null;
	static private InputStream is = null;
	static private PropertiesConfig pp = null;

	static public PropertiesConfig getSingleton(String prop) {
		if(pp == null){
			try{
				pp = new PropertiesConfig();
				properties = new Properties();
				is = new FileInputStream(prop);
				properties.load(is);
				is.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return pp;
	}

	public String getValor(String variable){
				
		return properties.get(variable).toString();
	}
}