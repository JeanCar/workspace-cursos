package pe.com.claro.eai.notificarclientemdb.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.commons.lang.SystemUtils;
import org.apache.xmlbeans.XmlObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import pe.com.java.jaxb.generator.abdcpproxy.bean.MensajeABDCP;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoAcreditacionPagoDeuda;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoCabeceraMensaje;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoCuerpoMensaje;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoObjecionConcesionarioCedente;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoProgramacionPortabilidad;
import pe.com.java.jaxb.generator.abdcpproxy.bean.TipoSolicitudAceptadaCedente;

/**
 * @author cguerra.
 * @clase: UtilEAI.java
 * @descripcion descripcion de la clase.
 * @fecha_de_creacion: 17-03-2014.
 * @fecha_de_ultima_actualizacion: 17-03-2014.
 * @version 1.0
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
@Component
public class UtilEAI {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	private static HashMap<Class, JAXBContext> objMapaContexto = new HashMap<Class, JAXBContext>();

	@Autowired
	private PropertiesExternosMDB propertiesExterno;

	/**
	 * getJAXBContextFromClass
	 * 
	 * @param mensajeTransaccion
	 * @param objClase
	 * @return JAXBContext
	 */
	private JAXBContext obtenerContextoJaxBFromClass(String mensajeTransaccion, Class objClase) {

		JAXBContext objContexto = null;
		objContexto = objMapaContexto.get(objClase);

		if (objContexto == null) {
			try {
				this.logger.info(mensajeTransaccion + "INICIALIZANDO: [JaxContext...]");

				objContexto = JAXBContext.newInstance(objClase);
				objMapaContexto.put(objClase, objContexto);
			} catch (Exception e) {
				this.logger.error(mensajeTransaccion + "ERROR creando 'JAXBContext': ", e);
			}
		}

		return objContexto;
	}

	/**
	 * transformarXmlTextFromJaxB
	 * 
	 * @param mensajeTransaccion
	 * @param objJaxB
	 * @return String
	 */
	public String transformarXmlTextFromJaxB(String mensajeTransaccion, Object objJaxB) {

		String commandoRequestEnXml = null;
		JAXBContext objContexto = null;
		XmlObject objXML = null;

		try {
			objContexto = this.obtenerContextoJaxBFromClass(mensajeTransaccion, objJaxB.getClass());

			Marshaller objMarshaller = objContexto.createMarshaller();
			StringWriter objStringWritter = new StringWriter();

			objMarshaller.marshal(objJaxB, objStringWritter);

			objXML = XmlObject.Factory.parse(objStringWritter.toString());
			commandoRequestEnXml = objXML.toString();
		} catch (Exception e) {
			this.logger.error(mensajeTransaccion + "ERROR parseando object to 'XML': ", e);
		}

		return commandoRequestEnXml;
	}

	/**
	 * transfromarAnyObjectToXmlText
	 * 
	 * @param mensajeTransaccion
	 * @param objJaxB
	 * @return String
	 */
	public String transfromarAnyObjectToXmlText(String mensajeTransaccion, Object objJaxB) {

		String commandoRequestEnXml = null;
		JAXBContext objContexto = null;
		XmlObject objXML = null;

		try {
			objContexto = this.obtenerContextoJaxBFromClass(mensajeTransaccion, objJaxB.getClass());

			Marshaller objMarshaller = objContexto.createMarshaller();
			StringWriter objStringWritter = new StringWriter();

			objMarshaller.marshal(
					new JAXBElement(new QName("", objJaxB.getClass().getName()), objJaxB.getClass(), objJaxB),
					objStringWritter);
			objXML = XmlObject.Factory.parse(objStringWritter.toString());

			commandoRequestEnXml = objXML.toString();
		} catch (Exception e) {
			this.logger.error(mensajeTransaccion + "ERROR parseando object to 'XML': ", e);
		}

		return commandoRequestEnXml;
	}

	/**
	 * EndpointURL
	 * 
	 * @param objBinding
	 * @return String
	 */
	public String getEndpointURL(Object objBinding) {
		Map<String, Object> contextoRequest = ((javax.xml.ws.BindingProvider) objBinding).getRequestContext();
		return (String) contextoRequest.get(javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
	}

	/**
	 * setEndpointURL
	 * 
	 * @param URL
	 * @param objBinding
	 * @return String
	 */
	public String setEndpointURL(String URL, Object objBinding) {
		Map<String, Object> contextoRequest = ((javax.xml.ws.BindingProvider) objBinding).getRequestContext();
		return (String) contextoRequest.put(javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY, URL);
	}

	/**
	 * convertirSpringXmlToJaxb
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param cadenaXml
	 * @return MensajeABDCP
	 */
	public MensajeABDCP convertirSpringXmlToJaxb(String cadenaTranzabilidadParam, String cadenaXml) {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[convertirSpringXmlToJaxb] ";

		logger.info(cadenaTrazabilidad + "-------- convertirSpringXmlToJaxb [INICIO] --------");

		MensajeABDCP objMensajeABDCP = null;
		JAXBContext objJAXBContext = null;
		Unmarshaller objUnmarshaller = null;
		StringReader objStringReader = null;

		try {
			objJAXBContext = JAXBContext.newInstance(MensajeABDCP.class);
			objUnmarshaller = objJAXBContext.createUnmarshaller();

			objStringReader = new StringReader(cadenaXml);
			objMensajeABDCP = (MensajeABDCP) objUnmarshaller.unmarshal(objStringReader);
		} catch (Exception e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
		} finally {
			logger.info(cadenaTrazabilidad + "-------- convertirSpringXmlToJaxb [FIN] --------");
		}

		return objMensajeABDCP;
	}

	/**
	 * convertirStringXmlToDocument
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param cadenaXml
	 * @return Document
	 */
	public Document convertirStringXmlToDocument(String cadenaTranzabilidadParam, String cadenaXml) {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[convertirStringXmlToDocument] ";

		// logger.info( cadenaTrazabilidad + "--------
		// convertirStringXmlToDocument [INICIO] --------" );

		DocumentBuilderFactory objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder objDocumentBuilder = null;
		Document objDocument = null;
		InputSource objInputSource = null;
		StringReader objStringReader = null;

		try {
			objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();
			objStringReader = new StringReader(cadenaXml);
			objInputSource = new InputSource(objStringReader);
			objDocument = objDocumentBuilder.parse(objInputSource);
			objDocument.getDocumentElement().normalize();

			// logger.info( cadenaTrazabilidad + "Conversion de 'XML a Document'
			// => 'OK'..." );
		} catch (Exception e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception), Documento Invalido [" + cadenError + "]");

			objDocument = null;
		} finally {
			// logger.info( cadenaTrazabilidad + "--------
			// convertirStringXmlToDocument [FIN] --------" );
		}

		return objDocument;
	}

	/**
	 * convertirDocumentToStringXml
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param objDocument
	 * @return String
	 */
	public String convertirDocumentToStringXml(String cadenaTranzabilidadParam, Document objDocument) {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[convertirDocumentToStringXml] ";

		logger.info(cadenaTrazabilidad + "-------- convertirDocumentToStringXml [INICIO] --------");

		TransformerFactory objTransformerFactory = TransformerFactory.newInstance();
		Transformer objTransformer = null;
		StringWriter objStringWriter = null;
		DOMSource objDOMSource = null;
		StreamResult objStreamResult = null;

		String cadenaXml = null;

		try {
			objTransformer = objTransformerFactory.newTransformer();
			objStringWriter = new StringWriter();
			objDOMSource = new DOMSource(objDocument);
			objStreamResult = new StreamResult(objStringWriter);

			objTransformer.transform(objDOMSource, objStreamResult);
			cadenaXml = objStringWriter.getBuffer().toString();
		} catch (Exception e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception), Documento Invalido [" + cadenError + "]");

			cadenaXml = null;
		} finally {
			logger.info(cadenaTrazabilidad + "-------- convertirDocumentToStringXml [FIN] --------");
		}

		return cadenaXml;
	}

	/**
	 * convertirJaxbToSpringXml
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param objMensajeABDCP
	 * @return String
	 */
	public String convertirJaxbToSpringXml(String cadenaTranzabilidadParam, MensajeABDCP objMensajeABDCP) {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[convertirJaxbToSpringXml] ";

		logger.info(cadenaTrazabilidad + "-------- convertirJaxbToSpringXml [INICIO] --------");

		JAXBContext objJAXBContext = null;
		StringWriter objStringWriter = null;
		String cadenaXml = null;

		try {
			objJAXBContext = JAXBContext.newInstance(MensajeABDCP.class);

			Marshaller marshaller = objJAXBContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			objStringWriter = new StringWriter();
			marshaller.marshal(objMensajeABDCP, objStringWriter);

			cadenaXml = objStringWriter.toString();
		} catch (Exception e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
		} finally {
			logger.info(cadenaTrazabilidad + "-------- convertirJaxbToSpringXml [FIN] --------");
		}

		return cadenaXml;
	}

	/**
	 * convertirDocumentToJaxb
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param objDocument
	 * @return MensajeABDCP
	 */
	public MensajeABDCP convertirDocumentToJaxb(String cadenaTranzabilidadParam, Document objDocument) {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[convertirDocumentToJaxb] ";

		logger.info(cadenaTrazabilidad + "-------- convertirDocumentToJaxb [INICIO] --------");

		TransformerFactory objTransformerFactory = TransformerFactory.newInstance();
		Transformer objTransformer = null;
		StringWriter objStringWriter = null;
		DOMSource objDOMSource = null;
		StreamResult objStreamResult = null;

		String cadenaXml = null;

		MensajeABDCP objMensajeABDCP = null;
		JAXBContext objJAXBContext = null;
		Unmarshaller objUnmarshaller = null;
		StringReader objStringReader = null;

		try {
			objTransformer = objTransformerFactory.newTransformer();
			objStringWriter = new StringWriter();
			objDOMSource = new DOMSource(objDocument);
			objStreamResult = new StreamResult(objStringWriter);

			objTransformer.transform(objDOMSource, objStreamResult);
			cadenaXml = objStringWriter.getBuffer().toString();

			logger.info(cadenaTrazabilidad + "Conversion de 'Document a XML' => 'OK'...");
		} catch (Exception e) {
			logger.info(cadenaTrazabilidad + "ERROR, Documento Invalido: [" + e + "]");
			cadenaXml = null;
		}

		if (cadenaXml != null) {

			try {
				objJAXBContext = JAXBContext.newInstance(MensajeABDCP.class);
				objUnmarshaller = objJAXBContext.createUnmarshaller();

				objStringReader = new StringReader(cadenaXml);
				objMensajeABDCP = (MensajeABDCP) objUnmarshaller.unmarshal(objStringReader);

				logger.info(cadenaTrazabilidad + "Conversion de 'XML a MensajeABDCP' => 'OK'...");
			} catch (Exception e) {
				String cadenError = this.utilGetStackTrace(e);
				logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			} finally {
				logger.info(cadenaTrazabilidad + "-------- convertirDocumentToJaxb [FIN] --------");
			}
		}

		return objMensajeABDCP;
	}

	/**
	 * validarJaxbWithXsd
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param rutaXsd
	 * @param objMensajeABDCP
	 * @return boolean
	 */
	public boolean validarJaxbWithXsd(String cadenaTranzabilidadParam, String rutaXsd, MensajeABDCP objMensajeABDCP) {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[validarJaxbWithXsd] ";

		logger.info(cadenaTrazabilidad + "-------- validarJaxbWithXsd [INICIO] --------");

		JAXBContext objJAXBContext = null;
		JAXBSource objJAXBSource = null;
		SchemaFactory objSchemaFactory = null;
		String vRutaPath = null;
		File objFileRutaPath = null;
		Schema objSchema = null;
		Validator objValidator = null;

		boolean estado = false;

		try {
			objJAXBContext = JAXBContext.newInstance(MensajeABDCP.class);
			objJAXBSource = new JAXBSource(objJAXBContext, objMensajeABDCP);
			objSchemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			vRutaPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().toString();
			vRutaPath = vRutaPath.replace("file:/", "");
			objFileRutaPath = new File(vRutaPath + rutaXsd);
			objSchema = objSchemaFactory.newSchema(objFileRutaPath);
			objValidator = objSchema.newValidator();
			objValidator.validate(objJAXBSource);

			estado = true;
		} catch (IOException e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			logger.info(cadenaTrazabilidad + "ERROR (IOException) al obtener 'XSD': [" + cadenError + "]");

			estado = false;
		} catch (SAXParseException e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			logger.info(cadenaTrazabilidad + "ERROR (SAXParseException), Imposible convertir cadena a document 'XML': ["
					+ cadenError + "]");

			estado = false;
		} catch (Exception e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");

			estado = false;
		} finally {
			logger.info(cadenaTrazabilidad + "-------- validarJaxbWithXsd [FIN] --------");
		}

		return estado;
	}

	/**
	 * validarStringXmlWithXsd
	 * 
	 * @param cadenaTranzabilidadParam
	 * @param cadenaXml
	 * @param rutaXsd
	 * @return boolean
	 * @throws Exception
	 */
	public boolean validarStringXmlWithXsd(String cadenaTranzabilidadParam, String cadenaXml, String rutaXsd)
			throws Exception {
		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[validarStringXmlWithXsd] ";

		logger.info(cadenaTrazabilidad + "-------- validarStringXmlWithXsd [INICIO] --------");

		boolean estado = false;

		StringReader objStringReader = null;
		URL objURL = null;
		SchemaFactory objSchemaFactory = null;
		Schema objSchema = null;
		Validator objValidator = null;
		StreamSource objStreamSource = null;

		try {
			objStringReader = new StringReader(cadenaXml);
			objURL = this.getClass().getClassLoader().getResource(rutaXsd);
			objSchemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			objSchema = objSchemaFactory.newSchema(objURL);
			objValidator = objSchema.newValidator();
			objStreamSource = new StreamSource(objStringReader);

			objValidator.validate(objStreamSource);

			logger.info(cadenaTrazabilidad + "Validacion de 'XSD' => 'OK'...");

			estado = true;
		} catch (IOException e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			logger.info(cadenaTrazabilidad + "ERROR (IOException) al obtener 'XSD': [" + cadenError + "]");
			throw e;
		} catch (SAXParseException e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			logger.info(cadenaTrazabilidad + "ERROR (SAXParseException), Imposible convertir cadena a document 'XML': ["
					+ cadenError + "]");
			throw e;

		} catch (SAXException e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			logger.info(cadenaTrazabilidad + "ERROR (SAXException), Error al validar 'XML': [" + cadenError + "]");
			throw e;
		} catch (Exception e) {
			String cadenError = this.utilGetStackTrace(e);
			logger.info(cadenaTrazabilidad + "ERROR (Exception): [" + cadenError + "]");
			throw e;
		} finally {
			logger.info(cadenaTrazabilidad + "-------- validarStringXmlWithXsd [FIN] --------");
		}

		return estado;
	}

	/**
	 * utilGetStackTrace
	 * 
	 * @param objExcepcion
	 * @return String
	 */
	public String utilGetStackTrace(Exception objExcepcion) {

		StringWriter objPrintError = null;
		String cadenaError = null;

		try {
			objPrintError = new StringWriter();
			objExcepcion.printStackTrace(new PrintWriter(objPrintError));
			cadenaError = (objPrintError + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cadenaError;
	}

	/**
	 * getDate
	 * 
	 * @param fechaCadenaParam
	 * @param formato
	 * @return Timestamp
	 */
	public Timestamp getDate(String fechaCadenaParam, String formato) {

		SimpleDateFormat objSimpleDateFormat = null;
		java.util.Date objFecha = null;
		Timestamp objReturn = null;

		try {
			objSimpleDateFormat = new SimpleDateFormat(formato, new Locale("es", "pe"));
			objFecha = objSimpleDateFormat.parse(fechaCadenaParam);
			objReturn = new java.sql.Timestamp(objFecha.getTime());
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return objReturn;
	}

	/**
	 * getTimeStampToDateStr
	 * 
	 * @param fecha
	 * @param formato
	 * @return String
	 */
	public String getTimeStampToDateStr(Timestamp fecha, String formato) {

		SimpleDateFormat objSimpleDateFormat = null;
		String objFechaReturn = null;

		try {
			objSimpleDateFormat = new SimpleDateFormat(formato, new Locale("es", "pe"));
			objFechaReturn = objSimpleDateFormat.format(new java.util.Date(fecha.getTime()));
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return objFechaReturn;
	}

	/**
	 * getStringFromDate
	 * 
	 * @param fecha
	 * @param formato
	 * @return String
	 */
	public String getStringFromDate(Date fecha, String formato) {

		SimpleDateFormat objSimpleDateFormat = null;
		String objFechaReturn = null;

		try {
			objSimpleDateFormat = new SimpleDateFormat(formato, new Locale("es", "pe"));
			objFechaReturn = objSimpleDateFormat.format(fecha);
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return objFechaReturn;
	}

	/**
	 * getDateForProgrammingPortability
	 * 
	 * @param haveInstallationDate
	 * @param installationDate
	 * @param format
	 * @param days
	 * @return String
	 */
	public String getDateForProgrammingPortability(boolean haveInstallationDate, Date installationDate, String format,
			int days) {

		Date dateReturn = null;

		if (haveInstallationDate) {
			Calendar installationDay = Calendar.getInstance();
			installationDay.setTime(installationDate);
			installationDay.add(Calendar.DAY_OF_MONTH, days);
			installationDay.set(Calendar.HOUR_OF_DAY, Integer.parseInt(propertiesExterno.cPROGRAMACION_HORA));
			installationDay.set(Calendar.MINUTE, Integer.parseInt(propertiesExterno.cPROGRAMACION_MINUTO));
			installationDay.set(Calendar.SECOND, Integer.parseInt(propertiesExterno.cPROGRAMACION_SEGUNDO));
			dateReturn = installationDay.getTime();
		} else {
			// today plus the especificated days
			Calendar rightNow = Calendar.getInstance();
			rightNow.add(Calendar.DAY_OF_MONTH, days);
			rightNow.set(Calendar.HOUR_OF_DAY, Integer.parseInt(propertiesExterno.cPROGRAMACION_HORA));
			rightNow.set(Calendar.MINUTE, Integer.parseInt(propertiesExterno.cPROGRAMACION_MINUTO));
			rightNow.set(Calendar.SECOND, Integer.parseInt(propertiesExterno.cPROGRAMACION_SEGUNDO));
			dateReturn = rightNow.getTime();
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(format, new Locale("es", "pe"));
		return dateFormat.format(dateReturn);
	}

	/**
	 * getDate
	 * 
	 * @return Timestamp
	 */
	public Timestamp getDate() {

		Timestamp objReturn = null;

		try {
			objReturn = new java.sql.Timestamp(new java.sql.Date(Calendar.getInstance().getTime().getTime()).getTime());
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return objReturn;
	}

	/**
	 * getDate
	 * 
	 * @param formato
	 * @return String
	 */
	public String getDate(String formato) {

		String resultado = null;

		try {
			Timestamp fecha = new Timestamp(new Date().getTime());
			SimpleDateFormat sdf = new SimpleDateFormat(formato, new Locale("es", "pe"));
			resultado = sdf.format(fecha);
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return resultado;
	}

	/**
	 * getFechaYYYYMMDD�������� �@param�
	 * fechaCadenaParam�Debe�de�ser�en�formato: [yyyyMMdd] �@return�Date
	 */

	public Date getFechaYYYYMMDD(String fechaCadenaParam) {

		SimpleDateFormat objSimpleDateFormat = null;
		java.util.Date objFecha = null;
		try {
			objSimpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
			objFecha = objSimpleDateFormat.parse(fechaCadenaParam);
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return objFecha;
	}

	/**
	 * getIp
	 * 
	 * @return String
	 */
	public String getIp() {
		String ip = null;
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			this.logger.error(e.getMessage(), e);
		}
		return ip;
	}

	/**
	 * ejecutarComandoInConsola
	 * 
	 * @param comando
	 * @return String
	 */
	public String ejecutarComandoInConsola(String cadenaTranzabilidadParam, String comando) {

		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[ejecutarComandoInConsola] ";

		this.logger.info(cadenaTrazabilidad + "-------- ejecutarComandoInConsola [INICIO] --------");

		StringBuffer objStringBuffer = new StringBuffer();
		BufferedReader objBufferedReader = null;
		InputStreamReader objInputStreamReader = null;
		Process objProcess = null;
		String cadenaRetorno = null;

		try {
			objProcess = Runtime.getRuntime().exec(comando);
			objProcess.waitFor();

			objInputStreamReader = new InputStreamReader(objProcess.getInputStream());
			objBufferedReader = new BufferedReader(objInputStreamReader);

			String vLinea = "";

			while ((vLinea = objBufferedReader.readLine()) != null) {
				objStringBuffer.append(vLinea).append('\n');
			}

			cadenaRetorno = objStringBuffer.toString();
		} catch (Exception e) {
			this.logger.error(cadenaTrazabilidad + "ERROR: (Exception)", e);
		} finally {
			this.logger.info(cadenaTrazabilidad + "-------- ejecutarComandoInConsola [FIN] --------");
		}

		return cadenaRetorno;
	}

	/**
	 * verificarTipoOS
	 * 
	 * @param cadenaTranzabilidadParam
	 * @return String[]
	 */
	public String[] verificarTipoOS(String cadenaTranzabilidadParam) {

		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[verificarTipoOS] ";
		this.logger.info(cadenaTrazabilidad + "-------- verificarTipoOS [INICIO] --------");

		String[] vArregloRetornoOS = new String[2];

		String vNombreOS = "";
		String vVersionOS = "";

		String vNombreCompletoOS = "";
		String vTipoOS = "";

		try {
			if (SystemUtils.IS_OS_WINDOWS) {
				vNombreOS = "Windows";

				if (System.getProperty("sun.arch.data.model").equals("64")) {
					vVersionOS = "64";
				} else {
					vVersionOS = "32";
				}

				vTipoOS = ConstantesMDB.cSOWINDOWS;
			} else if (SystemUtils.IS_OS_LINUX) {
				vNombreOS = "Linux";

				if (System.getProperty("sun.arch.data.model").equals("64")) {
					vVersionOS = "64";
				} else {
					vVersionOS = "32";
				}

				vTipoOS = ConstantesMDB.cSOLINUX;
			}

			vNombreCompletoOS = (vNombreOS + "-" + vVersionOS);
		} catch (Exception e) {
			this.logger.info(cadenaTrazabilidad + "-------- verificarTipoOS [FIN] --------");
		} finally {
			vArregloRetornoOS[0] = vNombreCompletoOS;
			vArregloRetornoOS[1] = vTipoOS;
		}

		return vArregloRetornoOS;
	}

	/**
	 * Método para obtener el xmlMsg para diferentes tipos de mensajes.
	 * 
	 * @param cadenaTranzabilidadParam
	 *            cadena de trazabilidad, tipo String
	 * @param parametros
	 *            parametros, tipo String...: <blockquote> [0] id mensaje <br />
	 *            [1] remitente <br />
	 *            [2] destinatario <br />
	 *            [3] fecha de creacion del mensaje <br />
	 *            [4] id proceso <br />
	 *            [5] tipo de Mensaje <br />
	 *            [6] Para tipo de mensaje "CPAC" y "SAC" - observaciones <br />
	 *            [6] Para tipo de mensaje "CPOCC" y "OCC" - causa de objecion
	 *            <br />
	 *            [6] Para tipo de mensaje "SPR" y "CPSPR" - fecha de ejecucion
	 *            de portabilidad <br />
	 *            [6] Para tipo de mensaje "PP" - fecha de Programacion de
	 *            Portabilidad <br />
	 *            [7] Para tipo de mensaje "CPOCC" y "OCC" - numeracion (Si no
	 *            tiene deuda) <br />
	 *            [7] Para tipo de mensaje "CPOCC" y "OCC" - fecha de
	 *            vencimiento (Si tiene deuda)<br />
	 *            [8] Para tipo de mensaje "CPOCC" y "OCC" - moneda (Si tiene
	 *            deuda)<br />
	 *            [9] Para tipo de mensaje "CPOCC" y "OCC" - monto (Si tiene
	 *            deuda)<br />
	 *            [10] Para tipo de mensaje "CPOCC" y "OCC" - numeracion (Si
	 *            tiene deuda)<br />
	 *            </blockquote>
	 */
	public String obtenerMensajeABDCPXmlToString(String cadenaTranzabilidadParam, String... parametros) {

		Map<String, String> mapParametros = new HashMap<String, String>();

		mapParametros.put(ConstantesMDB.cID_MENSAJE, parametros[0]);
		mapParametros.put(ConstantesMDB.cREMITENTE, parametros[1]);
		mapParametros.put(ConstantesMDB.cDESTINATARIO, parametros[2]);
		mapParametros.put(ConstantesMDB.cFEC_CREA_MSJ, parametros[3]);
		mapParametros.put(ConstantesMDB.cID_PROCESO, parametros[4]);

		if (parametros[5].equals(propertiesExterno.cMENSAJE_CPAC)) {
			mapParametros.put(ConstantesMDB.cTIPO_MENSAJE, parametros[5]);
			mapParametros.put(ConstantesMDB.cOBSERVACIONES, parametros[6]);
			mapParametros.put(ConstantesMDB.cFECHA_ACTIVACION, parametros[7]);
		}

		else if (parametros[5].equals(propertiesExterno.cMENSAJE_CPOCC)) {
			mapParametros.put(ConstantesMDB.cTIPO_MENSAJE, parametros[5]);
			mapParametros.put(ConstantesMDB.cCAUSA_OBJECION, parametros[6]);
			if (mapParametros.get(ConstantesMDB.cCAUSA_OBJECION).equals(ConstantesMDB.cABONADO_TIENE_DEUDA_CODIGO)) {
				mapParametros.put(ConstantesMDB.cFEC_VENCIMIENTO, parametros[7]);
				mapParametros.put(ConstantesMDB.cMONEDA, parametros[8]);
				mapParametros.put(ConstantesMDB.cMONTO, parametros[9]);
				mapParametros.put(ConstantesMDB.cNUMERACION, parametros[10]);
			} else
				mapParametros.put(ConstantesMDB.cNUMERACION, parametros[7]);
		}

		else if (parametros[5].equals(propertiesExterno.cMENSAJE_SAC)) {
			mapParametros.put(ConstantesMDB.cTIPO_MENSAJE, parametros[5]);
			mapParametros.put(ConstantesMDB.cOBSERVACIONES, parametros[6]);
		}

		else if (parametros[5].equals(propertiesExterno.cMENSAJE_OCC)) {
			mapParametros.put(ConstantesMDB.cTIPO_MENSAJE, parametros[5]);
			mapParametros.put(ConstantesMDB.cCAUSA_OBJECION, parametros[6]);
			if (mapParametros.get(ConstantesMDB.cCAUSA_OBJECION).equals(ConstantesMDB.cABONADO_TIENE_DEUDA_CODIGO)) {
				mapParametros.put(ConstantesMDB.cFEC_VENCIMIENTO, parametros[7]);
				mapParametros.put(ConstantesMDB.cMONEDA, parametros[8]);
				mapParametros.put(ConstantesMDB.cMONTO, parametros[9]);
				mapParametros.put(ConstantesMDB.cNUMERACION, parametros[10]);
			} else
				mapParametros.put(ConstantesMDB.cNUMERACION, parametros[7]);
		}

		else if (parametros[5].equals(propertiesExterno.cMENSAJE_PP)) {
			mapParametros.put(ConstantesMDB.cTIPO_MENSAJE, parametros[5]);
			mapParametros.put(ConstantesMDB.cFEC_EJEC_PORTA, parametros[6]);
		} else if (parametros[5].equals(propertiesExterno.cMENSAJE_APD)) {
			mapParametros.put(ConstantesMDB.cTIPO_MENSAJE, parametros[5]);
			mapParametros.put(ConstantesMDB.cFECHA_PAGO, parametros[6]);
			mapParametros.put(ConstantesMDB.cENTIDAD_PAGO, parametros[7]);
			mapParametros.put(ConstantesMDB.cNUMERACION, parametros[8]);
			mapParametros.put(ConstantesMDB.cNUMERO_OPERACION_PAGO, parametros[9]);
			mapParametros.put(ConstantesMDB.cMONTO, parametros[10]);
			mapParametros.put(ConstantesMDB.cMONEDA, parametros[11]);
			mapParametros.put(ConstantesMDB.cDOCUMENTO_ADJUNTO, parametros[12]);
		}

		return this.obtenerMensajeABDCPXmlToString(cadenaTranzabilidadParam, mapParametros);
	}

	/**
	 * Método para obtener el xmlMsg para diferentes tipos de mensajes.
	 * 
	 * @param cadenaTranzabilidadParam
	 *            cadena de trazabilidad, tipo String
	 * @param parametros
	 *            parametros, tipo Map<String, String>
	 */
	public String obtenerMensajeABDCPXmlToString(String cadenaTranzabilidadParam, Map<String, String> parametros) {

		String cadenaTrazabilidad = cadenaTranzabilidadParam + "[obtenerMensajeABDCPXmlToString] ";

		logger.info(cadenaTrazabilidad + "-------- obtenerMensajeABDCPXmlToString [INICIO] --------");
		logger.info(cadenaTrazabilidad + "Parametros:");
		logger.info(cadenaTrazabilidad + "Destinatario: " + parametros.get(ConstantesMDB.cDESTINATARIO));
		logger.info(cadenaTrazabilidad + "Fecha Creacion Mensaje: " + parametros.get(ConstantesMDB.cFEC_CREA_MSJ));
		logger.info(cadenaTrazabilidad + "Id Mensaje: " + parametros.get(ConstantesMDB.cID_MENSAJE));
		logger.info(cadenaTrazabilidad + "Id Proceso: " + parametros.get(ConstantesMDB.cID_PROCESO));
		logger.info(cadenaTrazabilidad + "Remitente: " + parametros.get(ConstantesMDB.cREMITENTE));

		MensajeABDCP objMensajeABDCP = new MensajeABDCP();
		TipoCabeceraMensaje tipoCabeceraMensaje = new TipoCabeceraMensaje();
		tipoCabeceraMensaje.setDestinatario(parametros.get(ConstantesMDB.cDESTINATARIO));
		tipoCabeceraMensaje.setFechaCreacionMensaje(parametros.get(ConstantesMDB.cFEC_CREA_MSJ));
		tipoCabeceraMensaje.setIdentificadorMensaje(parametros.get(ConstantesMDB.cID_MENSAJE));
		tipoCabeceraMensaje.setIdentificadorProceso(parametros.get(ConstantesMDB.cID_PROCESO));
		tipoCabeceraMensaje.setRemitente(parametros.get(ConstantesMDB.cREMITENTE));
		objMensajeABDCP.setCabeceraMensaje(tipoCabeceraMensaje);

		TipoCuerpoMensaje tipoCuerpoMensaje = new TipoCuerpoMensaje();

		if (parametros.get(ConstantesMDB.cTIPO_MENSAJE) == propertiesExterno.cMENSAJE_CPAC) {

			logger.info(cadenaTrazabilidad + "Tipo Mensaje: " + parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			logger.info(cadenaTrazabilidad + "Observaciones: " + parametros.get(ConstantesMDB.cOBSERVACIONES));
			logger.info(cadenaTrazabilidad + "Fecha Activacion: " + parametros.get(ConstantesMDB.cFECHA_ACTIVACION));

			tipoCuerpoMensaje.setIdMensaje(parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			TipoSolicitudAceptadaCedente tipoSolicitudAceptadaCedente = new TipoSolicitudAceptadaCedente();
			tipoSolicitudAceptadaCedente.setObservaciones(parametros.get(ConstantesMDB.cOBSERVACIONES));
			tipoSolicitudAceptadaCedente.setFechaActivacion(parametros.get(ConstantesMDB.cFECHA_ACTIVACION));
			tipoCuerpoMensaje.setConsultaPreviaAceptadaCedente(tipoSolicitudAceptadaCedente);

		} else if (parametros.get(ConstantesMDB.cTIPO_MENSAJE) == propertiesExterno.cMENSAJE_CPOCC) {

			logger.info(cadenaTrazabilidad + "Tipo Mensaje: " + parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			logger.info(cadenaTrazabilidad + "Causa Objecion: " + parametros.get(ConstantesMDB.cCAUSA_OBJECION));
			logger.info(cadenaTrazabilidad + "Fec Vencimiento: " + parametros.get(ConstantesMDB.cFEC_VENCIMIENTO));
			logger.info(cadenaTrazabilidad + "Moneda: " + parametros.get(ConstantesMDB.cMONEDA));
			logger.info(cadenaTrazabilidad + "Monto: " + parametros.get(ConstantesMDB.cMONTO));
			logger.info(cadenaTrazabilidad + "Numeracion: " + parametros.get(ConstantesMDB.cNUMERACION));

			tipoCuerpoMensaje.setIdMensaje(parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			TipoObjecionConcesionarioCedente tipoObjecionConcesionarioCedente = new TipoObjecionConcesionarioCedente();
			tipoObjecionConcesionarioCedente.setCausaObjecion(parametros.get(ConstantesMDB.cCAUSA_OBJECION));
			tipoObjecionConcesionarioCedente.setFechaVencimiento(parametros.get(ConstantesMDB.cFEC_VENCIMIENTO));
			tipoObjecionConcesionarioCedente.setMoneda(parametros.get(ConstantesMDB.cMONEDA));
			tipoObjecionConcesionarioCedente.setMonto(parametros.get(ConstantesMDB.cMONTO));
			tipoObjecionConcesionarioCedente.setNumeracion(parametros.get(ConstantesMDB.cNUMERACION));
			tipoCuerpoMensaje.setConsultaPreviaObjecionConcesionarioCedente(tipoObjecionConcesionarioCedente);

		} else if (parametros.get(ConstantesMDB.cTIPO_MENSAJE) == propertiesExterno.cMENSAJE_SAC) {

			logger.info(cadenaTrazabilidad + "Tipo Mensaje: " + parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			logger.info(cadenaTrazabilidad + "Observaciones: " + parametros.get(ConstantesMDB.cOBSERVACIONES));

			tipoCuerpoMensaje.setIdMensaje(parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			TipoSolicitudAceptadaCedente tipoSolicitudAceptadaCedente = new TipoSolicitudAceptadaCedente();
			tipoSolicitudAceptadaCedente.setObservaciones(parametros.get(ConstantesMDB.cOBSERVACIONES));
			tipoCuerpoMensaje.setSolicitudAceptadaCedente(tipoSolicitudAceptadaCedente);

		} else if (parametros.get(ConstantesMDB.cTIPO_MENSAJE) == propertiesExterno.cMENSAJE_OCC) {

			logger.info(cadenaTrazabilidad + "Tipo Mensaje: " + parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			logger.info(cadenaTrazabilidad + "Causa Objecion: " + parametros.get(ConstantesMDB.cCAUSA_OBJECION));
			logger.info(cadenaTrazabilidad + "Fec Vencimiento: " + parametros.get(ConstantesMDB.cFEC_VENCIMIENTO));
			logger.info(cadenaTrazabilidad + "Moneda: " + parametros.get(ConstantesMDB.cMONEDA));
			logger.info(cadenaTrazabilidad + "Monto: " + parametros.get(ConstantesMDB.cMONTO));
			logger.info(cadenaTrazabilidad + "Numeracion: " + parametros.get(ConstantesMDB.cNUMERACION));

			tipoCuerpoMensaje.setIdMensaje(parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			TipoObjecionConcesionarioCedente tipoObjecionConcesionarioCedente = new TipoObjecionConcesionarioCedente();
			tipoObjecionConcesionarioCedente.setCausaObjecion(parametros.get(ConstantesMDB.cCAUSA_OBJECION));
			tipoObjecionConcesionarioCedente.setFechaVencimiento(parametros.get(ConstantesMDB.cFEC_VENCIMIENTO));
			tipoObjecionConcesionarioCedente.setMoneda(parametros.get(ConstantesMDB.cMONEDA));
			tipoObjecionConcesionarioCedente.setMonto(parametros.get(ConstantesMDB.cMONTO));
			tipoObjecionConcesionarioCedente.setNumeracion(parametros.get(ConstantesMDB.cNUMERACION));
			tipoCuerpoMensaje.setObjecionConcesionarioCedente(tipoObjecionConcesionarioCedente);

		} else if (parametros.get(ConstantesMDB.cTIPO_MENSAJE) == propertiesExterno.cMENSAJE_PP) {

			logger.info(cadenaTrazabilidad + "Tipo Mensaje: " + parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			logger.info(cadenaTrazabilidad + "Fecha Ejecucion Portabilidad: "
					+ parametros.get(ConstantesMDB.cFEC_EJEC_PORTA));

			tipoCuerpoMensaje.setIdMensaje(parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			TipoProgramacionPortabilidad tipoProgramacionPortabilidad = new TipoProgramacionPortabilidad();
			tipoProgramacionPortabilidad.setFechaEjecucionPortabilidad(parametros.get(ConstantesMDB.cFEC_EJEC_PORTA));
			tipoCuerpoMensaje.setProgramacionPortabilidad(tipoProgramacionPortabilidad);

		} else if (parametros.get(ConstantesMDB.cTIPO_MENSAJE) == propertiesExterno.cMENSAJE_APD) {

			logger.info(cadenaTrazabilidad + "Tipo Mensaje: " + parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			logger.info(cadenaTrazabilidad + "Fecha Pago: " + parametros.get(ConstantesMDB.cFECHA_PAGO));
			logger.info(cadenaTrazabilidad + "Entidad Pago: " + parametros.get(ConstantesMDB.cENTIDAD_PAGO));
			logger.info(cadenaTrazabilidad + "Numeracion: " + parametros.get(ConstantesMDB.cNUMERACION));
			logger.info(
					cadenaTrazabilidad + "Num Operacion Pago: " + parametros.get(ConstantesMDB.cNUMERO_OPERACION_PAGO));
			logger.info(cadenaTrazabilidad + "Monto: " + parametros.get(ConstantesMDB.cMONTO));
			logger.info(cadenaTrazabilidad + "Moneda: " + parametros.get(ConstantesMDB.cMONEDA));
			logger.info(cadenaTrazabilidad + "Documento Adjunto: " + parametros.get(ConstantesMDB.cDOCUMENTO_ADJUNTO));

			tipoCuerpoMensaje.setIdMensaje(parametros.get(ConstantesMDB.cTIPO_MENSAJE));
			TipoAcreditacionPagoDeuda tipoAcreditacionDeuda = new TipoAcreditacionPagoDeuda();
			tipoAcreditacionDeuda.setFechaPago(parametros.get(ConstantesMDB.cFECHA_PAGO));
			tipoAcreditacionDeuda.setEntidadPago(parametros.get(ConstantesMDB.cENTIDAD_PAGO));
			tipoAcreditacionDeuda.setNumeracion(parametros.get(ConstantesMDB.cNUMERACION));
			tipoAcreditacionDeuda.setNumeroOperacionPago(parametros.get(ConstantesMDB.cNUMERO_OPERACION_PAGO));
			tipoAcreditacionDeuda.setMonto(parametros.get(ConstantesMDB.cMONTO));
			tipoAcreditacionDeuda.setMoneda(parametros.get(ConstantesMDB.cMONEDA));
			tipoAcreditacionDeuda.setDocumentoAdjunto(parametros.get(ConstantesMDB.cDOCUMENTO_ADJUNTO));

			tipoCuerpoMensaje.setAcreditacionPagoDeuda(tipoAcreditacionDeuda);

		}

		objMensajeABDCP.setCuerpoMensaje(tipoCuerpoMensaje);

		String xmlMsg = this.convertirJaxbToSpringXml(cadenaTrazabilidad, objMensajeABDCP);

		logger.info(cadenaTrazabilidad + "-------- obtenerMensajeABDCPXmlToString [FIN] --------");

		return xmlMsg;
	}

	/**
	 * Método para comparar fechas.
	 * 
	 * @param hoy
	 *            fecha actual, tipo Date
	 * @param fecha
	 *            para comparar, tipo Date
	 * @return int 0 son iguales, -1 fecha actual es menor, 1 fecha actual es
	 *         mayor
	 */
	public int compararFechas(Date hoy, Date fecha) {
		if (compararFechasIguales(hoy, fecha)) {
			return 0;
		} else {
			if (hoy.before(fecha)) {
				return -1;
			} else {
				return 1;
			}
		}
	}

	/**
	 * Método para comparar fechas iguales.
	 * 
	 * @param hoy
	 *            fecha actual, tipo Date
	 * @param fecha
	 *            para comparar, tipo Date
	 * @return boolean true si son iguales, false si no son iguales
	 */
	public boolean compararFechasIguales(Date fechaHoy, Date fechaCalendario) {

		Calendar hoy = Calendar.getInstance();
		hoy.setTime(fechaHoy);
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fechaCalendario);

		int anioHoy = hoy.get(Calendar.YEAR);
		int mesHoy = hoy.get(Calendar.MONTH) + 1;
		int diaHoy = hoy.get(Calendar.DAY_OF_MONTH);

		int anioCalendario = calendario.get(Calendar.YEAR);
		int mesCalendario = calendario.get(Calendar.MONTH) + 1;
		int diaCalendario = calendario.get(Calendar.DAY_OF_MONTH);

		if (anioHoy == anioCalendario && mesHoy == mesCalendario && diaHoy == diaCalendario)
			return true;
		else
			return false;
	}

	/**
	 * Método para obtener el formato correcto de la fecha a enviar. * @param
	 * fecha para comparar, tipo String
	 * 
	 * @return formFecha, String
	 */
	public String formatoFecha(String fecha) {

		String fechaEnt = fecha;

		String formFecha = ConstantesMDB.cCADENAVACIA;
		for (int i = 0; i < fechaEnt.length(); i++) {

			if (formFecha.length() == ConstantesMDB.cSIZE_FECHA_ACTIVACION) {
				break;
			}
			if (!(fecha.charAt(i) == '-')) {
				formFecha = formFecha + fechaEnt.charAt(i);
			}

		}
		return formFecha;
	}

	public String restarFecha(int dias) {

		String formFecha = ConstantesMDB.cCADENAVACIA;
		SimpleDateFormat sdf = new SimpleDateFormat(this.propertiesExterno.cFORMATO_FECHA, new Locale("es", "pe"));
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(new Date().getTime());
		cal.add(Calendar.DATE, dias);
		formFecha = sdf.format(cal.getTime()).toString();

		return formFecha;
	}
}
