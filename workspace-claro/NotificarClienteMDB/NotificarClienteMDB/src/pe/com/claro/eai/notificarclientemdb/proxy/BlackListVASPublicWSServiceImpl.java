package pe.com.claro.eai.notificarclientemdb.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.notificarclientemdb.util.JAXBUtilitariosMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.eai.service.ws.baseschema.AuditRequest;
import pe.com.claro.eai.service.ws.baseschema.BlackListDetalle.DetalleBlackList;
import pe.com.claro.eai.service.ws.postventa.blacklistvaspublicws.BlackListVASPublicWSPortType;
import pe.com.claro.eai.service.ws.postventa.blacklistvaspublicws.types.ConsultarBlackListRequest;
import pe.com.claro.eai.service.ws.postventa.blacklistvaspublicws.types.ConsultarBlackListResponse;

/**
 * 
 * @author Cristhian Davila.
 * @clase: BlackListVASPublicWSServiceImpl.java
 * @descripcion Implementacion de Logica de Negocio.
 * @author_company: CLARO.
 * @fecha_de_creacion: 23-13-2018.
 * @fecha_de_ultima_actualizacion: 23-03-2018.
 * @version 1.0
 */
@Service
public class BlackListVASPublicWSServiceImpl implements BlackListVASPublicWSService {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier(value = "blackListVASPublicWSService")
	private BlackListVASPublicWSPortType service;

	@Autowired
	private PropertiesExternosMDB propertiesExternos;

	public void consultarBlackListServicio(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request,
			String msisdn) throws WSException, ReintentosException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "consultarBlackListServicio] ";
		log.info(cadenaTrazabilidad + "-------- validarClienteBlackList [INICIO] --------");
		log.info(cadenaTrazabilidad + "[INPUT]: msisdn: " + msisdn);
		ConsultarBlackListRequest consultarBlackListRequest = new ConsultarBlackListRequest();
		ConsultarBlackListResponse consultarBlackListResponse = new ConsultarBlackListResponse();
		long tiempoInicio = System.currentTimeMillis();
		int nroIntentosActividad = 0;

		while (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
			try {
				AuditRequest auditRequest = new AuditRequest();
				auditRequest.setAplicacion(request.getNombreSistemaOrigen());
				auditRequest.setIdTransaccion(request.getIdTransaccion());
				auditRequest.setIpAplicacion(request.getIpSistemaOrigen());
				auditRequest.setUsrAplicacion(request.getUsuarioAplicacion());

				msisdn = propertiesExternos.peruInternationalCode + msisdn;
				consultarBlackListRequest.setMsisdn(msisdn);

				consultarBlackListRequest.setAudit(auditRequest);

				consultarBlackListResponse = service.consultarBlackListServicio(consultarBlackListRequest);
				log.info(cadenaTrazabilidad + "[OUTPUT]: consultarBlackListResponse: ");
				log.info(JAXBUtilitariosMDB.anyObjectToXmlText(consultarBlackListResponse));

				if (consultarBlackListResponse.getAudit().getCodigoRespuesta()
						.equals(propertiesExternos.BLACKLISTVASPUBLIC_CODIGO_OK)) {
					if (consultarBlackListResponse.getDetalle() != null) {
						for (DetalleBlackList listDetBlackList : consultarBlackListResponse.getDetalle()
								.getDetalleBlackList()) {
							if (listDetBlackList.getEstado() != null && !listDetBlackList.getEstado()
									.equals(propertiesExternos.BLACKLISTVASPUBLIC_CODIGO_ESTADO_CLIENTE_NO_BLACKLIST)) {
								throw new WSException(null, "Cliente Pertenece a la BlackList.");
							}
						}
					}
				} else {
					String codigoRespuesta = consultarBlackListResponse.getAudit().getCodigoRespuesta();
					String msjRespuesta = consultarBlackListResponse.getAudit().getMensajeRespuesta();
					log.error(cadenaTrazabilidad + " Error: " + msjRespuesta);
					throw new WSException(codigoRespuesta, msjRespuesta);
				}
				break;

			} catch (WSException e) {
				throw new WSException(e.getCode(), e.getMessage());
			} catch (NestedRuntimeException e) {
				log.error(cadenaTrazabilidad + " Error TimeOut: " + e.getMessage());
			} catch (Exception e) {
				log.error(cadenaTrazabilidad + " Error: " + e.getMessage());
			} finally {
				nroIntentosActividad++;
				log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				log.info(cadenaTrazabilidad + "-------- validarClienteBlackList [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
				log.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExternos.NRO_REINTENTOS_WS) {
			log.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		log.info(cadenaTrazabilidad + "Cliente NO Pertenece a la BlackList.");
		log.info(cadenaTrazabilidad + "[OUTPUT]: consultarBlackListResponse: ");
		log.info(JAXBUtilitariosMDB.anyObjectToXmlText(consultarBlackListResponse));
		log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		log.info(cadenaTrazabilidad + "-------- validarClienteBlackList [FIN] --------");
	}
}
