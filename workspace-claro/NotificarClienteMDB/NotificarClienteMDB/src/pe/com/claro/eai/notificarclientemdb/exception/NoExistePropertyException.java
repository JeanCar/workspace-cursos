package pe.com.claro.eai.notificarclientemdb.exception;

public class NoExistePropertyException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoExistePropertyException(Throwable throwable) {
        super(throwable);
    }

    public NoExistePropertyException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public NoExistePropertyException(String string) {
        super(string);
    }

    public NoExistePropertyException() {
        super();
    }
}