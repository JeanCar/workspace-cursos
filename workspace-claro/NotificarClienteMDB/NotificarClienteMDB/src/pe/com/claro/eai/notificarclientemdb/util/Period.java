package pe.com.claro.eai.notificarclientemdb.util;

import java.util.Calendar;
import java.util.Date;

public class Period {
	private Calendar calMayor;
	private Calendar calMenor;
	private int anios;
	private int meses;
	private int dias;
	private int mesesAnios;

	public Period(Date fechaMayor, Date fechaMenor) {
		this.calMayor = Calendar.getInstance();
		this.calMayor.setTime(fechaMayor);
		this.calMenor = Calendar.getInstance();
		this.calMenor.setTime(fechaMenor);
		anios = meses = dias = mesesAnios = 0;
		diferenciaFechas();

	}

	public Period(Calendar calMayor, Calendar calMenor) {
		this.calMayor = calMayor;
		this.calMenor = calMenor;
		anios = meses = dias = mesesAnios = 0;
		diferenciaFechas();
	}

	private void diferenciaFechas() {
		if (validaFecha())
			return;

		int incrementa = 0;

		if (calMenor.get(Calendar.DAY_OF_MONTH) > calMayor.get(Calendar.DAY_OF_MONTH)) {
			incrementa = calMenor.getActualMaximum(Calendar.DAY_OF_MONTH);
		}

		if (incrementa != 0) {
			dias = (calMayor.get(Calendar.DAY_OF_MONTH) + incrementa) - calMenor.get(Calendar.DAY_OF_MONTH);
			incrementa = 1;
		} else {
			dias = calMayor.get(Calendar.DAY_OF_MONTH) - calMenor.get(Calendar.DAY_OF_MONTH);
		}

		if ((calMenor.get(Calendar.MONTH) + incrementa) > calMayor.get(Calendar.MONTH)) {
			meses = (calMayor.get(Calendar.MONTH) + 12) - (calMenor.get(Calendar.MONTH) + incrementa);
			incrementa = 1;
		} else {
			meses = (calMayor.get(Calendar.MONTH)) - (calMenor.get(Calendar.MONTH) + incrementa);
			incrementa = 0;
		}

		anios = calMayor.get(Calendar.YEAR) - (calMenor.get(Calendar.YEAR) + incrementa);

		mesesAnios = meses + (anios * 12);
	}

	private boolean validaFecha() {
		return calMenor.after(calMayor) || calMenor.equals(calMayor);
	}

	public int getDias() {
		return dias;
	}

	public int getMeses() {
		return meses;
	}

	public int getAnios() {
		return anios;
	}

	public int getMesesAnios() {
		return mesesAnios;
	}
}
