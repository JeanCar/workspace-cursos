package pe.com.claro.eai.notificarclientemdb.proxy;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.notificarclientemdb.util.JAXBUtilitariosMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.eai.ws.baseschema.AuditRequestType;
import pe.com.claro.eai.ws.postventa.gestionacuerdo.GestionAcuerdoWSPortType;
import pe.com.claro.eai.ws.postventa.gestionacuerdo.types.ObtenerReintegroEquipoRequest;
import pe.com.claro.eai.ws.postventa.gestionacuerdo.types.ObtenerReintegroEquipoResponse;

/**
 * 
 * @author Cristhian Davila.
 * @clase: GestionAcuerdoWSServiceImpl.java
 * @descripcion Implementacion de Logica de Negocio.
 * @author_company: CLARO.
 * @fecha_de_creacion: 23-13-2018.
 * @fecha_de_ultima_actualizacion: 23-03-2018.
 * @version 1.0
 */
@Service
public class GestionAcuerdoWSServiceImpl implements GestionAcuerdoWSService {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier(value = "esbGestionAcuerdoWSService")
	private GestionAcuerdoWSPortType service;

	@Autowired
	private PropertiesExternosMDB propertiesExternos;

	public ObtenerReintegroEquipoResponse obtenerReintegroEquipo(String cadenaTrazabilidadParam,
			SendGwpMessageBeanRecepcion request, String msisdn) throws WSException, ReintentosException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "esbGestionAcuerdoServicio] ";
		log.info(cadenaTrazabilidad + "-------- obtenerReintegroEquipoResponse [INICIO] --------");
		log.info(cadenaTrazabilidad + "[INPUT]: msisdn: " + msisdn);
		ObtenerReintegroEquipoRequest obtenerReintegroEquipoRequest = new ObtenerReintegroEquipoRequest();
		ObtenerReintegroEquipoResponse obtenerReintegroEquipoResponse = new ObtenerReintegroEquipoResponse();
		long tiempoInicio = System.currentTimeMillis();
		int nroIntentosActividad = 0;

		while (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
			try {
				AuditRequestType auditRequest = new AuditRequestType();
				auditRequest.setNombreAplicacion(request.getNombreSistemaOrigen());
				auditRequest.setIdTransaccion(request.getIdTransaccion());
				auditRequest.setIpAplicacion(request.getIpSistemaOrigen());
				auditRequest.setUsuarioAplicacion(request.getUsuarioAplicacion());

				obtenerReintegroEquipoRequest.setAuditRequest(auditRequest);
				obtenerReintegroEquipoRequest.setMsisdn(msisdn);
				SimpleDateFormat sdf = new SimpleDateFormat(propertiesExternos.ESBGESTIONACUERDO_FORMATO_FECHA_TRANSACCION);
				obtenerReintegroEquipoRequest.setFechaTransaccion(sdf.format(new Date()));
				
				obtenerReintegroEquipoResponse = service.obtenerReintegroEquipo(obtenerReintegroEquipoRequest);

				if (obtenerReintegroEquipoResponse.getAuditResponse().getCodigoRespuesta()
						.equals(propertiesExternos.ESBGESTIONACUERDO_CODIGO_OK)) {

					if ((obtenerReintegroEquipoResponse.getAcuerdoEstado() == null
							&& obtenerReintegroEquipoResponse.getDescripcionEstadoAcuerdo() == null)
							|| obtenerReintegroEquipoResponse.getAcuerdoFechaInicio() == null
							|| obtenerReintegroEquipoResponse.getAcuerdoFechaFin() == null) {
						log.error(cadenaTrazabilidad + " Error: No se encontr� acuerdo");
						throw new WSException(null, "No se encontr� acuerdo");
					} else {
						if (obtenerReintegroEquipoResponse.getAcuerdoEstado()
								.equals(propertiesExternos.ESBGESTIONACUERDO_ESTADO_ACUERDO)
								|| obtenerReintegroEquipoResponse.getDescripcionEstadoAcuerdo().equalsIgnoreCase(
										propertiesExternos.ESBGESTIONACUERDO_ESTADO_ACUERDO_DESCRIPCION)) {
							break;
						} else {
							log.error(cadenaTrazabilidad + " Error: Acuerdo no vigente");
							throw new WSException(null, "Acuerdo no vigente");
						}
					}
				} else {
					String codigoRespuesta = obtenerReintegroEquipoResponse.getAuditResponse().getCodigoRespuesta();
					String msjRespuesta = obtenerReintegroEquipoResponse.getAuditResponse().getMensajeRespuesta();
					log.error(cadenaTrazabilidad + " Error: " + msjRespuesta);
					throw new WSException(codigoRespuesta, msjRespuesta);
				}
			} catch (WSException e) {
				throw new WSException(e.getCode(), e.getMessage());
			} catch (NestedRuntimeException e) {
				log.error(cadenaTrazabilidad + " Error TimeOut: " + e.getMessage());
			} catch (Exception e) {
				log.error(cadenaTrazabilidad + " Error: " + e.getMessage());
			} finally {
				nroIntentosActividad++;
				log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				log.info(cadenaTrazabilidad + "-------- obtenerReintegroEquipoResponse [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
				log.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExternos.NRO_REINTENTOS_WS) {
			log.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		log.info(cadenaTrazabilidad + "[OUTPUT]: obtenerReintegroEquipoResponse: ");
		log.info(JAXBUtilitariosMDB.anyObjectToXmlText(obtenerReintegroEquipoResponse));

		log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		log.info(cadenaTrazabilidad + "-------- obtenerReintegroEquipoResponse [FIN] --------");
		
		return obtenerReintegroEquipoResponse;
	}
}
