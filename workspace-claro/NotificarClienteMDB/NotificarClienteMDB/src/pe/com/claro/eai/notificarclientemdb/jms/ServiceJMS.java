package pe.com.claro.eai.notificarclientemdb.jms;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.BaseException;

public interface ServiceJMS {

	public void enviarObjectJMS(String cadenaTransaccion, final SendGwpMessageBeanRecepcion objDto)
			throws BaseException;

}
