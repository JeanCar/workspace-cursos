package pe.com.claro.eai.notificarclientemdb.proxy;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;

public interface TipificacionInteraccionWSService {
	public void crearInteraccion(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request, String msisdn, String mensaje) throws WSException, ReintentosException;
}
