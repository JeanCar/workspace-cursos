package pe.com.claro.eai.notificarclientemdb.dao;

import java.sql.Types;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.com.claro.eai.notificarclientemdb.exception.DBException;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;

@Repository
public class PVUDAOImpl implements PVUDAO {

	private final Logger logger = LoggerFactory.getLogger(PVUDAOImpl.class);

	@Autowired
	private PropertiesExternosMDB propertiesExterno;

	private SimpleJdbcCall objJdbcCall;

	@Autowired
	@Qualifier(value = "pvuDS")
	private DataSource pvuDS;

	@Override
	public Double obtenerComportamientoPago(String cadenaTrazabilidadParam, int codigoContrato)
			throws DBException, ReintentosException {

		String cadenaTrazabilidad = cadenaTrazabilidadParam + "[obtenerComportamientoPago]-";

		int nroIntentosActividad = 0;
		long tiempoInicio = System.currentTimeMillis();

		logger.info(cadenaTrazabilidad + "-------- obtenerComportamientoPago [INICIO] --------");
		logger.info(cadenaTrazabilidad + "[INPUT]: codigoContrato: " + codigoContrato);
		JdbcTemplate objJdbcTemplate = null;

		String OWNER = propertiesExterno.DB_PVU_OWNER;
		String PAQUETE = propertiesExterno.DB_PVU_SISACT_PKG_ACUERDO_6;
		String PROCEDURE = propertiesExterno.DB_PVU_SP_CON_COMPORTAPAGO;

		Double comportamientoPago = null;

		while (nroIntentosActividad <= propertiesExterno.NRO_REINTENTOS_WS) {
			try {
				pvuDS.setLoginTimeout(this.propertiesExterno.DB_PVU_TIMEOUT);
				logger.info(cadenaTrazabilidad + "Obteniendo conexion a BD con JNDI: " + propertiesExterno.DB_PVU_JNDI);
				logger.info(cadenaTrazabilidad + "LLAMANDO 'SP': OWNER: [{}] PAQUETE: [{}] PROCEDURE:[{}]",
						new Object[] { OWNER, PAQUETE, PROCEDURE });

				SqlParameterSource objParametrosINPUT = new MapSqlParameterSource().addValue("PI_NUM_CONTRATO",
						codigoContrato, Types.NUMERIC);

				logger.info(cadenaTrazabilidad + "PARAMETROS [INPUT]: " + "PI_NUM_CONTRATO: [" + codigoContrato + "]");
				objJdbcCall = new SimpleJdbcCall(this.pvuDS);
				objJdbcTemplate = objJdbcCall.getJdbcTemplate();
				objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_PVU_TIMEOUT);

				// PARAMETROS [OUT]:
				Map<String, Object> objParametrosOUTPUT = objJdbcCall.withoutProcedureColumnMetaDataAccess()
						.withSchemaName(OWNER).withCatalogName(PAQUETE).withProcedureName(PROCEDURE)
						.declareParameters(new SqlParameter("PI_NUM_CONTRATO", Types.DOUBLE),
								new SqlOutParameter("PO_COMPORTAMIENTO", Types.DOUBLE),
								new SqlOutParameter("PO_CODIGO_RESPUESTA", Types.VARCHAR),
								new SqlOutParameter("PO_MENSAJE_RESPUESTA", Types.VARCHAR))
						.execute(objParametrosINPUT);

				String codigoRespuesta = String.valueOf(objParametrosOUTPUT.get("PO_CODIGO_RESPUESTA"));
				String mensajeRespuesta = String.valueOf(objParametrosOUTPUT.get("PO_MENSAJE_RESPUESTA"));

				if (codigoRespuesta.equals(propertiesExterno.DB_PVU_CODIGO_RESPUESTA_OK)) {
					comportamientoPago = (Double) objParametrosOUTPUT.get("PO_COMPORTAMIENTO");
				} else {
					throw new DBException(codigoRespuesta, mensajeRespuesta);
				}
				logger.info(cadenaTrazabilidad + "PARAMETROS [OUPUT]: " + "PO_COD: [" + codigoRespuesta + "], "
						+ "PO_MSG: [" + mensajeRespuesta + "], " + "PO_COMPORTAMIENTO: [" + comportamientoPago + "]");
				break;
			} catch (DBException e) {
				throw new DBException(e.getCode(), e.getMessage());
			} catch (Exception e) {
				String texto_error = e.getMessage() + "";
				if (e.getMessage().indexOf("SQLTimeoutException") > -1) {
					logger.error(
							cadenaTrazabilidad + "Tiempo de espera agotado para ejecucion de SP: " + e.getMessage());
				} else {
					logger.error(cadenaTrazabilidad + "Error general ejecutando SP: {}", texto_error);
				}

			} finally {
				nroIntentosActividad++;
				logger.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				logger.info(cadenaTrazabilidad + "-------- obtenerComportamientoPago [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExterno.NRO_REINTENTOS_WS) {
				logger.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExterno.NRO_REINTENTOS_WS) {
			logger.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		logger.info(cadenaTrazabilidad + "[OUTPUT]: comportamientoPago: " + (comportamientoPago == null ? "NULL"
				: comportamientoPago.toString()));

		logger.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		logger.info(cadenaTrazabilidad + "-------- obtenerComportamientoPago [FIN] --------");

		return comportamientoPago;
	}
}
