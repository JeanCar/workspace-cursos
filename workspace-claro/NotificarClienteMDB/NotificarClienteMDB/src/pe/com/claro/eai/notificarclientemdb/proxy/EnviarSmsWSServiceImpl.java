package pe.com.claro.eai.notificarclientemdb.proxy;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.notificarclientemdb.util.JAXBUtilitariosMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.eai.ws.baseschema.AuditRequestType;
import pe.com.claro.eai.ws.baseschema.RequestOpcionalType;
import pe.com.claro.eai.ws.baseschema.RequestOpcionalType.RequestOpcional;
import pe.com.claro.eai.ws.smsws.SmsWSPortype;
import pe.com.claro.eai.ws.smsws.types.EnviarMensajeRequest;
import pe.com.claro.eai.ws.smsws.types.EnviarMensajeResponse;

/**
 * 
 * @author Cristhian Davila.
 * @clase: EnviarSmsWSServiceImpl.java
 * @descripcion Implementacion de Logica de Negocio.
 * @author_company: CLARO.
 * @fecha_de_creacion: 23-13-2018.
 * @fecha_de_ultima_actualizacion: 23-03-2018.
 * @version 1.0
 */
@Service
public class EnviarSmsWSServiceImpl implements EnviarSmsWSService {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier(value = "sendSMSWSService")
	private SmsWSPortype service;

	@Autowired
	private PropertiesExternosMDB propertiesExternos;

	public void enviarSMS(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request, String msisdn,
			String mensaje) throws WSException, ReintentosException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "SmsServicio] ";
		log.info(cadenaTrazabilidad + "-------- enviarSMS [INICIO] --------");
		EnviarMensajeRequest enviarMensajeRequest = new EnviarMensajeRequest();
		EnviarMensajeResponse enviarMensajeResponse = new EnviarMensajeResponse();

		long tiempoInicio = System.currentTimeMillis();
		int nroIntentosActividad = 0;

		while (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
			try {

				String fechaHoraEnvioSMS = obtenerFechaHoraEnvioSMS();

				AuditRequestType auditRequest = new AuditRequestType();
				auditRequest.setNombreAplicacion(request.getNombreSistemaOrigen());
				auditRequest.setIdTransaccion(request.getIdTransaccion());
				auditRequest.setIpAplicacion(request.getIpSistemaOrigen());
				auditRequest.setUsuarioAplicacion(request.getUsuarioAplicacion());

				msisdn = propertiesExternos.peruInternationalCode + msisdn;

				enviarMensajeRequest.setAuditRequest(auditRequest);
				enviarMensajeRequest.setIdentificadorMAS(propertiesExternos.SMS_IDENTIFICADOR_MAS);
				enviarMensajeRequest.setMsisdn(msisdn);
				enviarMensajeRequest.setMensaje(mensaje);
				enviarMensajeRequest.setSender(propertiesExternos.SMS_SENDER);

				RequestOpcionalType requestOpcionalType = new RequestOpcionalType();

				RequestOpcional opcional1 = new RequestOpcional();
				opcional1.setCampo(propertiesExternos.SMS_CAMPO_SOXPN_FLAG_SAT_PUSH);
				opcional1.setValor(propertiesExternos.SMS_VALOR_SOXPN_FLAG_SAT_PUSH);
				requestOpcionalType.getRequestOpcional().add(opcional1);
				RequestOpcional opcional2 = new RequestOpcional();
				opcional2.setCampo(propertiesExternos.SMS_CAMPO_FECHA_HORA_ENVIO);
				opcional2.setValor(fechaHoraEnvioSMS);
				requestOpcionalType.getRequestOpcional().add(opcional2);

				enviarMensajeRequest.setListaRequestOpcional(requestOpcionalType);

				requestOpcionalType.getRequestOpcional();
				log.info(cadenaTrazabilidad + "[INPUT]: enviarMensajeRequest: ");
				log.info(JAXBUtilitariosMDB.anyObjectToXmlText(enviarMensajeRequest));
				enviarMensajeResponse = service.enviarMensaje(enviarMensajeRequest);

				if (enviarMensajeResponse != null && enviarMensajeResponse.getAuditResponse() != null
						&& enviarMensajeResponse.getAuditResponse().getCodigoRespuesta() != null
						&& !enviarMensajeResponse.getAuditResponse().getCodigoRespuesta()
								.equals(propertiesExternos.SMS_CODIGO_OK)) {
					String codigoRespuesta = enviarMensajeResponse.getAuditResponse().getCodigoRespuesta();
					String msjRespuesta = enviarMensajeResponse.getAuditResponse().getMensajeRespuesta();
					log.error(cadenaTrazabilidad + " Error: " + msjRespuesta);
					throw new WSException(codigoRespuesta, msjRespuesta);
				} else {
					break;
				}

			} catch (WSException e) {
				throw new WSException(e.getCode(), e.getMessage());
			} catch (NestedRuntimeException e) {
				log.error(cadenaTrazabilidad + " Error TimeOut: " + e.getMessage());

			} catch (Exception e) {
				log.error(cadenaTrazabilidad + " Error: " + e.getMessage());
			} finally {
				nroIntentosActividad++;
				log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				log.info(cadenaTrazabilidad + "-------- enviarSMS [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
				log.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");

			}
		}

		if (nroIntentosActividad > propertiesExternos.NRO_REINTENTOS_WS) {
			log.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		log.info(cadenaTrazabilidad + "Se envi� mensaje correctamente.");
		log.info(cadenaTrazabilidad + "[OUTPUT]: enviarMensajeResponse: ");
		log.info(JAXBUtilitariosMDB.anyObjectToXmlText(enviarMensajeResponse));
		log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		log.info(cadenaTrazabilidad + "-------- enviarSMS [FIN] --------");
	}

	private String obtenerFechaHoraEnvioSMS() throws WSException {
		String fechaHoraEnvio = null;
		try {
			Calendar calendarAhora = Calendar.getInstance();

			Calendar calendarMediaNoche = Calendar.getInstance();
			calendarMediaNoche.set(Calendar.HOUR_OF_DAY, 23);
			calendarMediaNoche.set(Calendar.MINUTE, 59);
			calendarMediaNoche.set(Calendar.SECOND, 59);

			Calendar calendarFranjaHorarioMin = Calendar.getInstance();
			String arrFranjaHorarioMin[] = propertiesExternos.SMS_FRANJA_HORARIA_MIN.split(":");
			calendarFranjaHorarioMin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(arrFranjaHorarioMin[0]));
			calendarFranjaHorarioMin.set(Calendar.MINUTE, Integer.parseInt(arrFranjaHorarioMin[1]));
			calendarFranjaHorarioMin.set(Calendar.SECOND, Integer.parseInt(arrFranjaHorarioMin[2]));

			Calendar calendarFranjaHorarioMax = Calendar.getInstance();
			String arrFranjaHorarioMax[] = propertiesExternos.SMS_FRANJA_HORARIA_MAX.split(":");
			calendarFranjaHorarioMax.set(Calendar.HOUR_OF_DAY, Integer.parseInt(arrFranjaHorarioMax[0]));
			calendarFranjaHorarioMax.set(Calendar.MINUTE, Integer.parseInt(arrFranjaHorarioMax[1]));
			calendarFranjaHorarioMax.set(Calendar.SECOND, Integer.parseInt(arrFranjaHorarioMax[2]));

			Date dateFranjaHorarioMin = calendarFranjaHorarioMin.getTime();
			Date dateFranjaHorarioMax = calendarFranjaHorarioMax.getTime();
			Date dateMediaNoche = calendarMediaNoche.getTime();
			Date ahora = calendarAhora.getTime();

			// dentro de la franja horaria <08:00am - 08:00pm]
			if ((dateFranjaHorarioMin.before(ahora) && dateFranjaHorarioMax.after(ahora))
					|| (dateFranjaHorarioMin.before(ahora) && dateFranjaHorarioMax.equals(ahora))) {
				calendarAhora.add(Calendar.MINUTE, 5);
				fechaHoraEnvio = new SimpleDateFormat("yyyy-MM-dd").format(calendarAhora.getTime());
				fechaHoraEnvio += "T" + new SimpleDateFormat("HH:mm:ss").format(calendarAhora.getTime());

			} else
			// dentro de la franja horaria <08:00pm - 12:00am]
			if ((dateFranjaHorarioMax.before(ahora) && dateMediaNoche.after(ahora))
					|| (dateFranjaHorarioMax.before(ahora) && dateMediaNoche.equals(ahora))) {
				calendarAhora.add(Calendar.DATE, 1);
				fechaHoraEnvio = new SimpleDateFormat("yyyy-MM-dd").format(calendarAhora.getTime());
				fechaHoraEnvio += "T" + "08:00:00";
			} else
			// dentro de la franja horaria <12:00am - 08:00am]
			if ((dateMediaNoche.before(ahora) && dateFranjaHorarioMin.after(ahora))
					|| (dateMediaNoche.before(ahora) && dateFranjaHorarioMin.equals(ahora))) {
				fechaHoraEnvio = new SimpleDateFormat("yyyy-MM-dd").format(calendarAhora.getTime());
				fechaHoraEnvio += "T" + "08:00:00";
			}

		} catch (Exception e) {
			throw new WSException("Error al obtener fecha env�o SMS, verificar formato de hora.");
		} 
		return fechaHoraEnvio;

	}
}
