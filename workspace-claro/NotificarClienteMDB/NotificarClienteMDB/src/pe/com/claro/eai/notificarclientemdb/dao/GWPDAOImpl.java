package pe.com.claro.eai.notificarclientemdb.dao;

import java.sql.Types;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.com.claro.eai.notificarclientemdb.exception.DBException;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.util.ConstantesMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;

@Repository
public class GWPDAOImpl implements GWPDAO {

	private final Logger logger = LoggerFactory.getLogger(GWPDAOImpl.class);

	@Autowired
	private PropertiesExternosMDB propertiesExterno;

	private SimpleJdbcCall objJdbcCall;

	@Autowired
	@Qualifier(value = "gwpDS")
	private DataSource gwpDS;

	@Override
	public String obtenerCuerpoMensaje(String cadenaTrazabilidadParam, int codigoContrato, int antiguedad,
			int permanenciaMeses, int comportamiento, int pendienteMeses, int pendienteDias)
			throws DBException, ReintentosException {

		String cadenaTrazabilidad = cadenaTrazabilidadParam + "[obtenerCuerpoMensaje]-";
		int nroIntentosActividad = 0;
		long tiempoInicio = System.currentTimeMillis();

		logger.info(cadenaTrazabilidad + "-------- obtenerCuerpoMensaje [INICIO] --------");
		JdbcTemplate objJdbcTemplate = null;

		String OWNER = propertiesExterno.DB_GWP_OWNER;
		String PAQUETE = propertiesExterno.DB_GWP_PKG_POME_MENSAJE;
		String PROCEDURE = propertiesExterno.DB_GWP_SP_PMESS_CUERPO_SMS;

		String mensaje = ConstantesMDB.cCADENAVACIA;

		while (nroIntentosActividad <= propertiesExterno.NRO_REINTENTOS_WS) {
			try {
				gwpDS.setLoginTimeout(this.propertiesExterno.DB_GWP_TIMEOUT);
				logger.info(cadenaTrazabilidad + "Obteniendo conexion a BD con JNDI: " + propertiesExterno.DB_GWP_JNDI);
				logger.info(cadenaTrazabilidad + "LLAMANDO 'SP': OWNER: [{}] PAQUETE: [{}] PROCEDURE:[{}]",
						new Object[] { OWNER, PAQUETE, PROCEDURE });

				SqlParameterSource objParametrosINPUT = new MapSqlParameterSource()
						.addValue("PI_CONTRATO", codigoContrato, Types.NUMERIC).addValue("PI_ANTIGUEDAD", antiguedad)
						.addValue("PI_PERMANENCIA_MESES", permanenciaMeses)
						.addValue("PI_COMPORTAMIENTO", comportamiento)
						.addValue("PI_PENDIENTE_MESES", pendienteMeses).addValue("PI_PENDIENTE_DIAS", pendienteDias)						;

				logger.info(cadenaTrazabilidad + "PARAMETROS [INPUT]: " + "PI_CONTRATO: [" + codigoContrato + "]"
						+ ", PI_ANTIGUEDAD: [" + antiguedad + "]" + ", PI_PERMANENCIA_MESES: [" + permanenciaMeses + "]"
						+ ", PI_COMPORTAMIENTO: [" + comportamiento + "]" + ", PI_PENDIENTE_MESES: [" + pendienteMeses
						+ "]" + ", PI_PENDIENTE_DIAS: [" + pendienteDias + "]");
				objJdbcCall = new SimpleJdbcCall(this.gwpDS);
				objJdbcTemplate = objJdbcCall.getJdbcTemplate();
				objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_PVU_TIMEOUT);

				Map<String, Object> objParametrosOUTPUT = objJdbcCall.withoutProcedureColumnMetaDataAccess()
						.withSchemaName(OWNER).withCatalogName(PAQUETE).withProcedureName(PROCEDURE)
						.declareParameters(new SqlParameter("PI_CONTRATO", Types.NUMERIC),
								new SqlParameter("PI_ANTIGUEDAD", Types.NUMERIC),
								new SqlParameter("PI_PERMANENCIA_MESES", Types.NUMERIC),
								new SqlParameter("PI_COMPORTAMIENTO", Types.NUMERIC),
								new SqlParameter("PI_PENDIENTE_MESES", Types.NUMERIC),
								new SqlParameter("PI_PENDIENTE_DIAS", Types.NUMERIC),
								new SqlOutParameter("PO_MENSAJE", Types.VARCHAR),
								new SqlOutParameter("PO_ERROR_CODE", Types.VARCHAR),
								new SqlOutParameter("PO_ERROR_MSG", Types.VARCHAR))
						.execute(objParametrosINPUT);

				String codigoRespuesta = String.valueOf(objParametrosOUTPUT.get("PO_ERROR_CODE"));
				String mensajeRespuesta = String.valueOf(objParametrosOUTPUT.get("PO_ERROR_MSG"));

				if (codigoRespuesta.equals(propertiesExterno.DB_GWP_CODIGO_RESPUESTA_OK)) {
					mensaje = String.valueOf(objParametrosOUTPUT.get("PO_MENSAJE"));
				} else {
					throw new DBException(codigoRespuesta, mensajeRespuesta);
				}
				logger.info(cadenaTrazabilidad + "PARAMETROS [OUPUT]: " + "PO_COD: [" + codigoRespuesta + "], "
						+ "PO_ERROR_MSG: [" + mensajeRespuesta + "], " + "PO_MENSAJE: [" + mensaje + "]");
				break;
			} catch (DBException e) {
				throw new DBException(e.getCode(), e.getMessage());
			} catch (Exception e) {
				String texto_error = e.getMessage() + "";
				if (e.getMessage().indexOf("SQLTimeoutException") > -1) {
					logger.error(
							cadenaTrazabilidad + "Tiempo de espera agotado para ejecucion de SP: " + e.getMessage());
				} else {
					logger.error(cadenaTrazabilidad + "Error general ejecutando SP: {}", texto_error);
				}

			} finally {
				nroIntentosActividad++;
				logger.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				logger.info(cadenaTrazabilidad + "-------- obtenerCuerpoMensaje [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExterno.NRO_REINTENTOS_WS) {
				logger.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExterno.NRO_REINTENTOS_WS) {
			logger.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		logger.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		logger.info(cadenaTrazabilidad + "-------- obtenerComportamientoPago [FIN] --------");

		return mensaje;
	}
}
