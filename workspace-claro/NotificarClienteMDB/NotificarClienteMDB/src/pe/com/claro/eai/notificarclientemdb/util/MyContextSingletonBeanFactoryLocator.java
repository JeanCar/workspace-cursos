package pe.com.claro.eai.notificarclientemdb.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.SingletonBeanFactoryLocator;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;

public class MyContextSingletonBeanFactoryLocator extends SingletonBeanFactoryLocator {


    @Override
    protected BeanFactory createDefinition(String resourceLocation,
                                           String factoryKey) {

        final ClassPathXmlApplicationContext cpxac =
            new ClassPathXmlApplicationContext(new String[] { resourceLocation },
                                               false);
        final ClassLoader classLoader = this.getClass().getClassLoader();
        cpxac.setClassLoader(classLoader);
        return cpxac;
    }

    private static final String BEANS_REFS_XML_NAME =
        "classpath*:beanRefContext.xml";

    private static Map<String, BeanFactoryLocator> instances = new HashMap<String, BeanFactoryLocator>();

    public static BeanFactoryLocator getInstance() throws BeansException {
        return getInstance(BEANS_REFS_XML_NAME);
    }

    
    public static BeanFactoryLocator getInstance(String selectorInput) throws BeansException {
        
    	String selector = selectorInput;
    	
        if (!ResourcePatternUtils.isUrl(selector)) {
            selector =
                    ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + selector;
        }

        synchronized (instances) {
            if (logger.isDebugEnabled()) {
                logger.debug("ContextSingletonBeanFactoryLocator.getInstance(): instances.hashCode=" +
                             instances.hashCode() + ", instances=" +
                             instances);
            }
            BeanFactoryLocator bfl =
                (BeanFactoryLocator)instances.get(selector);
            if (bfl == null) {
                bfl = new MyContextSingletonBeanFactoryLocator(selector);
                instances.put(selector, bfl);
            }
            return bfl;
        }
    }


    
    protected MyContextSingletonBeanFactoryLocator() {
        super(BEANS_REFS_XML_NAME);
    }

    
    protected MyContextSingletonBeanFactoryLocator(String resourceName) {
        super(resourceName);
    }

    
    protected void initializeDefinition(BeanFactory groupDef) throws BeansException {
        if (groupDef instanceof ConfigurableApplicationContext) {
            ((ConfigurableApplicationContext)groupDef).refresh();
        }
    }

    
    protected void destroyDefinition(BeanFactory groupDef,
                                     String resourceName) throws BeansException {
        if (groupDef instanceof ConfigurableApplicationContext) {
            
            if (logger.isDebugEnabled()) {
                logger.debug("ContextSingletonBeanFactoryLocator group with resourceName '" +
                             resourceName +
                             "' being released, as there are no more references");
            }
            ((ConfigurableApplicationContext)groupDef).close();
        }
    }

}