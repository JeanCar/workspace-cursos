package pe.com.claro.eai.notificarclientemdb.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExternosMDB {

	/*public static final PropertiesConfig rb = PropertiesConfig.getSingleton(System.getProperty("claro.properties") + 
			"NotificarClienteMDB.properties");
*/
	@Value("${peru.international.code}")
	public String peruInternationalCode;
			
	@Value( "${GMBR.programacion.hora}" )
	public String cPROGRAMACION_HORA;
	
	@Value( "${GMBR.programacion.minuto}" )
	public String cPROGRAMACION_MINUTO;
	
	@Value( "${GMBR.programacion.segundo}" )
	public String cPROGRAMACION_SEGUNDO;
	
	@Value( "${GMBR.mensaje.CPAC}" )
	public String cMENSAJE_CPAC;
	
	@Value( "${GMBR.mensaje.CPOCC}" )
	public String cMENSAJE_CPOCC;
	
	@Value( "${GMBR.mensaje.SAC}" )
	public String	cMENSAJE_SAC;
	
	@Value( "${GMBR.mensaje.OCC}" )
	public String cMENSAJE_OCC;
	
	@Value( "${GMBR.mensaje.PP}" )
	public String cMENSAJE_PP;
	
	@Value( "${GMBR.mensaje.APD}" )
	public String cMENSAJE_APD;
	
	@Value( "${GMBR.fecha.formato.tipofecha}" )
	public String cFORMATO_FECHA;	
	
	@Value( "${GMBR.operador.ABDCP}" )
	public String cOPERADOR_ABDCP;
	
	@Value( "${GMBR.operador.Claro}" )
	public String cOPERADOR_CLARO;

	//properties generales WS
	@Value( "${numero.reintentos.ws}" )
	public int NRO_REINTENTOS_WS;
	
	
	//properties JMS
	@Value( "${numero.reintentos.ws.reintentos.cola.error}" )
	public int NRO_REINTENTOS_COLA_ERROR;
	
	//Properties blackListVASPublic
	@Value( "${ws.blackListVASPublic.codigo.ok}" )
	public String BLACKLISTVASPUBLIC_CODIGO_OK;
	
	@Value("${ws.blackListVASPublic.codigo.estadoClienteNoBlackList}")
	public String BLACKLISTVASPUBLIC_CODIGO_ESTADO_CLIENTE_NO_BLACKLIST;
	
	//Properties SIACPostpagoConsultasWS
	@Value( "${ws.SIACPostpagoConsultas.codigoTipoCliente}" )
	public String CODIGO_TIPO_CLIENTE;
	
	//Properties esGestionAcuerdo
	@Value( "${ws.esbGestionAcuerdo.codigo.ok}" )
	public String ESBGESTIONACUERDO_CODIGO_OK;
	
	@Value( "${ws.esbGestionAcuerdo.estadoAcuerdo}" )
	public String ESBGESTIONACUERDO_ESTADO_ACUERDO;
	
	@Value( "${ws.esbGestionAcuerdo.estadoAcuerdoDescripcion}" )
	public String ESBGESTIONACUERDO_ESTADO_ACUERDO_DESCRIPCION;

	@Value( "${ws.esbGestionAcuerdo.formatoFechaFinAcuerdo}" )
	public String ESBGESTIONACUERDO_FORMATO_FECHA_FIN_ACUERDO;
	
	@Value( "${ws.esbGestionAcuerdo.formatoFechaTransaccion}" )
	public String ESBGESTIONACUERDO_FORMATO_FECHA_TRANSACCION;
	
	//Properties SMS
	@Value( "${ws.sms.codigo.ok}" )
	public String SMS_CODIGO_OK;
	
	@Value( "${ws.sms.identificador.mas}" )
	public String SMS_IDENTIFICADOR_MAS;
	
	@Value( "${ws.sms.sender}" )
	public String SMS_SENDER;
		
	@Value( "${ws.sms.franja.horario.min}" )
	public String SMS_FRANJA_HORARIA_MIN;
	
	@Value( "${ws.sms.franja.horario.max}" )
	public String SMS_FRANJA_HORARIA_MAX;
	
	@Value( "${ws.sms.campo.soxpn.flag.sat.push}" )
	public String SMS_CAMPO_SOXPN_FLAG_SAT_PUSH;
	
	@Value( "${ws.sms.valor.soxpn.flag.sat.push}" )
	public String SMS_VALOR_SOXPN_FLAG_SAT_PUSH;
	
	@Value( "${ws.sms.campo.fecha.hora.envio}" )
	public String SMS_CAMPO_FECHA_HORA_ENVIO;
	
	//Properties tipificacionInteraccion
	@Value( "${ws.tipificacion.codigo.ok}" )
	public String TIPIFICACION_CODIGO_OK;
		
	@Value( "${ws.tipificacion.clase}" )
	public String TIPIFICACION_CLASE;
	
	@Value( "${ws.tipificacion.codigo.empleado}" )
	public String TIPIFICACION_CODIGO_EMPLEADO;
	
	@Value( "${ws.tipificacion.codigo.sistema}" )
	public String TIPIFICACION_CODIGO_SISTEMA;
	
	@Value( "${ws.tipificacion.cuenta}" )
	public String TIPIFICACION_CUENTA;
	
	@Value( "${ws.tipificacion.flag.caso}" )
	public String TIPIFICACION_FLAG_CASO;
	
	@Value( "${ws.tipificacion.hecho.en.uno}" )
	public String TIPIFICACION_FLAG_HECHO_EN_UNO;
	
	@Value( "${ws.tipificacion.metodo.contacto}" )
	public String TIPIFICACION_METODO_CONTACTO;
	
	@Value( "${ws.tipificacion.obj.id}" )
	public String TIPIFICACION_OBJ_ID;
	
	@Value( "${ws.tipificacion.site.obj.id}" )
	public String TIPIFICACION_SITE_OBJ_ID;
	
	@Value( "${ws.tipificacion.subclase}" )
	public String TIPIFICACION_SUBCLASE;
	
	@Value( "${ws.tipificacion.text.resultado}" )
	public String TIPIFICACION_TEXT_RESULTADO;
	
	@Value( "${ws.tipificacion.tipo}" )
	public String TIPIFICACION_TIPO;
	
	@Value( "${ws.tipificacion.tipo.interaccion}" )
	public String TIPIFICACION_TIPO_INTERACCION;
	
	//properties PVUDS
	@Value( "${db.pvu.jndi}" )
	public String DB_PVU_JNDI;
	
	@Value( "${db.pvu.sisact.pkg.acuerdo.6}" )
	public String DB_PVU_SISACT_PKG_ACUERDO_6;
	
	@Value( "${db.pvu.sp.con.comportapago}" )
	public String DB_PVU_SP_CON_COMPORTAPAGO;
	
	@Value( "${db.pvu.owner}" )
	public String DB_PVU_OWNER;
	
	@Value( "${db.pvu.timeout}" )
	public int DB_PVU_TIMEOUT;
	
	@Value("${db.pvu.codigo.respuesta.ok}")
	public String DB_PVU_CODIGO_RESPUESTA_OK;
	
	//properties PVUDS
	@Value( "${db.gwp.jndi}" )
	public String DB_GWP_JNDI;
		
	@Value( "${db.gwp.pkg.pomes.mensaje}" )
	public String DB_GWP_PKG_POME_MENSAJE;
		
	@Value( "${db.gwp.sp.pmess.cuerpo.sms}" )
	public String DB_GWP_SP_PMESS_CUERPO_SMS;
		
	@Value( "${db.gwp.owner}" )
	public String DB_GWP_OWNER;
		
	@Value( "${db.gwp.timeout}" )
	public int DB_GWP_TIMEOUT;
		
	@Value("${db.gwp.codigo.respuesta.ok}")
	public String DB_GWP_CODIGO_RESPUESTA_OK;

}
