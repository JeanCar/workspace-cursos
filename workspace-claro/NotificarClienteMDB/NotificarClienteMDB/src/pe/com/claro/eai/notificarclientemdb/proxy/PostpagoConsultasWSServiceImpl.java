package pe.com.claro.eai.notificarclientemdb.proxy;

import javax.xml.ws.Holder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;

import com.claro.siacpostpago.datoscliente.Cliente;
import com.claro.siacpostpago.datoscontrato.Contrato;

import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.notificarclientemdb.util.ConstantesMDB;
import pe.com.claro.eai.notificarclientemdb.util.JAXBUtilitariosMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.siacpostpago.ws.DatosCliente;
import pe.com.claro.siacpostpago.ws.DatosClienteResponse;
import pe.com.claro.siacpostpago.ws.DatosContrato;
import pe.com.claro.siacpostpago.ws.DatosContratoResponse;
import pe.com.claro.siacpostpago.ws.SIACPostpagoConsultasWS;

/**
 * 
 * @author Cristhian DAvila.
 * @clase: ConsultaSaldoConsumoWSServiceImpl.java
 * @descripcion Implementacion de Logica de Negocio.
 * @author_company: CLARO.
 * @fecha_de_creacion: 23-03-2018.
 * @fecha_de_ultima_actualizacion: 23-03-2018
 * @version 1.0
 */

@Service
public class PostpagoConsultasWSServiceImpl implements PostpagoConsultasWSService {
	private final Logger log = Logger.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier(value = "SIACPostpagoConsultasWSService")
	private SIACPostpagoConsultasWS service;

	@Autowired
	private PropertiesExternosMDB propertiesExternos;

	@Override
	public DatosClienteResponse consultarDatosCliente(String cadenaTrazabilidadParam, String msisdn)
			throws WSException, ReintentosException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "consultarDatosCliente] ";
		log.info(cadenaTrazabilidad + "-------- obtenerDatosCliente [INICIO] --------");
		log.info(cadenaTrazabilidad + "[INPUT]: MSISDN: " + msisdn);

		long tiempoInicio = System.currentTimeMillis();
		int nroIntentosActividad = 0;

		Holder<DatosClienteResponse> hDatosClienteResponse = new Holder<DatosClienteResponse>();
		DatosClienteResponse datosClienteResponse = null;
		while (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
			try {
				DatosCliente datosClienteRequest = new DatosCliente();
				datosClienteRequest.setDnnum(msisdn);
				datosClienteRequest.setCustcode(ConstantesMDB.cCADENAVACIA);

				service.datosCliente(datosClienteRequest, hDatosClienteResponse);

				datosClienteResponse = hDatosClienteResponse.value;
				if (datosClienteResponse.getDatosClienteResponse().getCliente().isEmpty()) {
					throw new WSException(null, "No se encontraron datos del cliente");
				}

				if (!datosClienteResponse.getDatosClienteResponse().getCliente().get(0).getCodigoTipoCliente()
						.equals(propertiesExternos.CODIGO_TIPO_CLIENTE)) {
					throw new WSException(null, "El tipo cliente no es MASIVO");
				}

				@SuppressWarnings("unused")
				Cliente cliente = datosClienteResponse.getDatosClienteResponse().getCliente().get(0);

				break;

			} catch (WSException e) {
				throw new WSException(e.getCode(), e.getMessage());
			} catch (NestedRuntimeException e) {
				log.error(cadenaTrazabilidad + " Error TimeOut: " + e.getMessage());
			} catch (Exception e) {
				log.error(cadenaTrazabilidad + " Error: " + e.getMessage());
			} finally {
				nroIntentosActividad++;
				log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				log.info(cadenaTrazabilidad + "-------- obtenerDatosCliente [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
				log.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExternos.NRO_REINTENTOS_WS) {
			log.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		log.info(cadenaTrazabilidad + "[OUTPUT]: datosClienteResponse: ");
		log.info(JAXBUtilitariosMDB.anyObjectToXmlText(datosClienteResponse));
		log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		log.info(cadenaTrazabilidad + "-------- obtenerDatosCliente [FIN] --------");
		return datosClienteResponse;
	}

	@Override
	public DatosContratoResponse consultarDatosContrato(String cadenaTrazabilidadParam, int codigoContrato)
			throws WSException, ReintentosException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "consultarDatosContrato] ";
		log.info(cadenaTrazabilidad + "-------- obtenerFechaActivacion [INICIO] --------");
		log.info(cadenaTrazabilidad + "[INPUT]: codigoContrato: " + codigoContrato);
		long tiempoInicio = System.currentTimeMillis();
		int nroIntentosActividad = 0;

		Holder<DatosContratoResponse> hDatosClienteResponse = new Holder<DatosContratoResponse>();
		DatosContratoResponse datosContratoResponse = new DatosContratoResponse();
		while (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
			try {
				DatosContrato datosContratoRequest = new DatosContrato();
				datosContratoRequest.setPCoid(codigoContrato);
				service.datosContrato(datosContratoRequest, hDatosClienteResponse);
				datosContratoResponse = hDatosClienteResponse.value;

				if (!datosContratoResponse.getDatosContratoResponse().getContrato().isEmpty()) {
					Contrato contrato = datosContratoResponse.getDatosContratoResponse().getContrato().get(0);
					log.info(cadenaTrazabilidad + " Fecha Activacion (Antiguedad): " + contrato.getFechaAct());
				} else {
					throw new WSException(null, "No se encontraron datos del contrato");
				}

				break;

			} catch (WSException e) {
				throw new WSException(e.getCode(), e.getMessage());
			} catch (NestedRuntimeException e) {
				log.error(cadenaTrazabilidad + " Error TimeOut: " + e.getMessage());
			} catch (Exception e) {
				log.error(cadenaTrazabilidad + " Error: " + e.getMessage());
			} finally {
				nroIntentosActividad++;
				log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				log.info(cadenaTrazabilidad + "-------- obtenerFechaActivacion [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
				log.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExternos.NRO_REINTENTOS_WS) {
			log.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		log.info(cadenaTrazabilidad + "[OUTPUT]: datosContratoResponse: ");
		log.info(JAXBUtilitariosMDB.anyObjectToXmlText(datosContratoResponse));
		log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		log.info(cadenaTrazabilidad + "-------- obtenerFechaActivacion [FIN] --------");
		return datosContratoResponse;
	}

}
