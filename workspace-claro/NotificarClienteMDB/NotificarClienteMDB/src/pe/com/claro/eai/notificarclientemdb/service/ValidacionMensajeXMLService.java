package pe.com.claro.eai.notificarclientemdb.service;

import java.util.Map;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;

public interface ValidacionMensajeXMLService{
	
	public Map<String, Object> obtenerDatosMensaje(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request) throws Exception;
	
	
}
