package pe.com.claro.eai.notificarclientemdb.proxy;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.ws.postventa.gestionacuerdo.types.ObtenerReintegroEquipoResponse;

public interface GestionAcuerdoWSService {
	public ObtenerReintegroEquipoResponse obtenerReintegroEquipo(String cadenaTrazabilidadParam,
			SendGwpMessageBeanRecepcion request, String msisdn) throws WSException, ReintentosException;
}
