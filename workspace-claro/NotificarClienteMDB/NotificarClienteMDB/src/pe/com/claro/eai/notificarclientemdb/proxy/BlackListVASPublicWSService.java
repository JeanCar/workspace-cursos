package pe.com.claro.eai.notificarclientemdb.proxy;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;

public interface BlackListVASPublicWSService {
	public void consultarBlackListServicio(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request, String msisdn) throws WSException, ReintentosException;
}
