package pe.com.claro.eai.notificarclientemdb.exception;

public class JMSException extends BaseException {

	private static final long serialVersionUID = 1L;
	
	public JMSException(String string) {
	      super(string);
	 }

	 public JMSException(Exception exception) {
	      super(exception);
	 }

	 public JMSException(String string, Exception exception) {
	      super(string, exception);
	 }

	 public JMSException(String code, String message, Exception exception) {
	      super(code, message, exception);
	 }

	 public JMSException(String code, String message) {
	      super(code, message);
	 }
}
