package pe.com.claro.eai.notificarclientemdb.proxy;

import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.siacpostpago.ws.DatosClienteResponse;
import pe.com.claro.siacpostpago.ws.DatosContratoResponse;

/**
 * @author Daniel Cam.
 * @clase: ConsultaSaldoConsumoWSService.java
 * @descripcion interface logica de negocio.
 * @author_company: CLARO.
 * @fecha_de_creacion: 13-07-2017.
 * @fecha_de_ultima_actualizacion: 23-07-2017.
 * @version 1.0
 */
public interface PostpagoConsultasWSService {

	public DatosClienteResponse consultarDatosCliente(String cadenaTrazabilidadParam, String msisdn)
			throws WSException, ReintentosException;

	public DatosContratoResponse consultarDatosContrato(String cadenaTrazabilidadParam, int codigoContrato)
			throws WSException, ReintentosException;
}
