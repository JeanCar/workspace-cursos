package pe.com.claro.eai.notificarclientemdb.dao;

import pe.com.claro.eai.notificarclientemdb.exception.DBException;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;

public interface GWPDAO {

	public String obtenerCuerpoMensaje(String cadenaTrazabilidadParam, int codigoContrato, int antiguedad,
			int permanenciaMeses, int comportamiento, int pendienteMeses, int pendienteDias)
			throws DBException, ReintentosException;

}
