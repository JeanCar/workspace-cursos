package pe.com.claro.eai.notificarclientemdb.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.crm.clarify.InteraccionType;
import pe.com.claro.eai.crmservices.clarify.transaccioninteraccionesasync.TransaccionInteraccionesAsync;
import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.notificarclientemdb.util.JAXBUtilitariosMDB;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.eai.servicecommons.AuditType;
import pe.com.claro.eai.ws.baseschema.AuditRequestType;

/**
 * 
 * @author Cristhian Davila.
 * @clase: TipificacionInteraccionWSServiceImpl.java
 * @descripcion Implementacion de Logica de Negocio.
 * @author_company: CLARO.
 * @fecha_de_creacion: 23-13-2018.
 * @fecha_de_ultima_actualizacion: 23-03-2018.
 * @version 1.0
 */
@Service
public class TipificacionInteraccionWSServiceImpl implements TipificacionInteraccionWSService {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier(value = "tipificacionService")
	private TransaccionInteraccionesAsync service;

	@Autowired
	private PropertiesExternosMDB propertiesExternos;

	public void crearInteraccion(String cadenaTrazabilidadParam, SendGwpMessageBeanRecepcion request, String msisdn,
			String mensaje) throws WSException, ReintentosException {
		String cadenaTrazabilidad = cadenaTrazabilidadParam + "TransaccionInteraccionesAsync] ";
		log.info(cadenaTrazabilidad + "-------- crearInteraccion [INICIO] --------");
		AuditType auditType = new AuditType();

		long tiempoInicio = System.currentTimeMillis();
		int nroIntentosActividad = 0;

		while (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
			try {
				AuditRequestType auditRequest = new AuditRequestType();
				auditRequest.setNombreAplicacion(request.getNombreSistemaOrigen());
				auditRequest.setIdTransaccion(request.getIdTransaccion());
				auditRequest.setIpAplicacion(request.getIpSistemaOrigen());
				auditRequest.setUsuarioAplicacion(request.getUsuarioAplicacion());

				InteraccionType interaccionType = new InteraccionType();
				interaccionType.setClase(propertiesExternos.TIPIFICACION_CLASE);
				interaccionType.setCodigoEmpleado(propertiesExternos.TIPIFICACION_CODIGO_EMPLEADO);
				interaccionType.setCodigoSistema(propertiesExternos.TIPIFICACION_CODIGO_SISTEMA);
				interaccionType.setCuenta(propertiesExternos.TIPIFICACION_CUENTA);
				interaccionType.setFlagCaso(propertiesExternos.TIPIFICACION_FLAG_CASO);
				interaccionType.setHechoEnUno(propertiesExternos.TIPIFICACION_FLAG_HECHO_EN_UNO);
				interaccionType.setMetodoContacto(propertiesExternos.TIPIFICACION_METODO_CONTACTO);
				interaccionType.setNotas(mensaje);
				interaccionType.setObjId(propertiesExternos.TIPIFICACION_OBJ_ID);
				interaccionType.setSiteObjId(propertiesExternos.TIPIFICACION_SITE_OBJ_ID);
				interaccionType.setSubClase(propertiesExternos.TIPIFICACION_SUBCLASE);
				interaccionType.setTelefono(msisdn);
				interaccionType.setTextResultado(propertiesExternos.TIPIFICACION_TEXT_RESULTADO);
				interaccionType.setTipo(propertiesExternos.TIPIFICACION_TIPO);
				interaccionType.setTipoInteraccion(propertiesExternos.TIPIFICACION_TIPO_INTERACCION);

				log.info(cadenaTrazabilidad + "[INPUT]: interaccionType: ");
				log.info(JAXBUtilitariosMDB.anyObjectToXmlText(interaccionType));
				auditType = service.crearInteraccion(request.getIdTransaccion(), interaccionType, null, null, null);

				if (auditType != null && auditType.getErrorCode() != null
						&& !auditType.getErrorCode().equals(propertiesExternos.TIPIFICACION_CODIGO_OK)) {
					String codigoRespuesta = auditType.getErrorCode();
					String msjRespuesta = auditType.getErrorMsg();
					log.error(cadenaTrazabilidad + " Error: " + msjRespuesta);
					throw new WSException(codigoRespuesta, msjRespuesta);
				} else {
					break;
				}
			} catch (WSException e) {
				throw new WSException(e.getCode(), e.getMessage());
			} catch (NestedRuntimeException e) {
				log.error(cadenaTrazabilidad + " Error TimeOut: " + e.getMessage());

			} catch (Exception e) {
				log.error(cadenaTrazabilidad + " Error: " + e.getMessage());
			} finally {
				nroIntentosActividad++;
				log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
						+ " milisegundos ]");
				log.info(cadenaTrazabilidad + "-------- crearInteraccion [FIN] --------");
			}

			if (nroIntentosActividad <= propertiesExternos.NRO_REINTENTOS_WS) {
				log.info(cadenaTrazabilidad + " ----------- SE PROCEDE A REALIZAR EL REINTENTO : ["
						+ nroIntentosActividad + "]-----------");
			}
		}

		if (nroIntentosActividad > propertiesExternos.NRO_REINTENTOS_WS) {
			log.error(cadenaTrazabilidad + "Error: Se alcanz� l�mite de reintentos");
			throw new ReintentosException("N�mero de reintentos alcanzado");
		}

		log.info(cadenaTrazabilidad + "Se cre� interacci�n correctamente.");
		log.info(cadenaTrazabilidad + "[OUTPUT]: auditType: ");
		log.info(JAXBUtilitariosMDB.anyObjectToXmlText(auditType));
		log.info(cadenaTrazabilidad + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
				+ " milisegundos ]");
		log.info(cadenaTrazabilidad + "-------- crearInteraccion [FIN] --------");
	}
}
