package pe.com.claro.eai.notificarclientemdb.ejb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.ejb.MessageDriven;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.claro.siacpostpago.datoscliente.Cliente;

import pe.com.claro.eai.gwportarecepcion.pojo.SendGwpMessageBeanRecepcion;
import pe.com.claro.eai.notificarclientemdb.dao.GWPDAO;
import pe.com.claro.eai.notificarclientemdb.dao.PVUDAO;
import pe.com.claro.eai.notificarclientemdb.exception.BaseException;
import pe.com.claro.eai.notificarclientemdb.exception.DBException;
import pe.com.claro.eai.notificarclientemdb.exception.JMSException;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;
import pe.com.claro.eai.notificarclientemdb.exception.WSException;
import pe.com.claro.eai.notificarclientemdb.jms.ServiceJMS;
import pe.com.claro.eai.notificarclientemdb.proxy.BlackListVASPublicWSService;
import pe.com.claro.eai.notificarclientemdb.proxy.EnviarSmsWSService;
import pe.com.claro.eai.notificarclientemdb.proxy.GestionAcuerdoWSService;
import pe.com.claro.eai.notificarclientemdb.proxy.PostpagoConsultasWSService;
import pe.com.claro.eai.notificarclientemdb.proxy.TipificacionInteraccionWSService;
import pe.com.claro.eai.notificarclientemdb.service.ValidacionMensajeXMLService;
import pe.com.claro.eai.notificarclientemdb.util.ConstantesMDB;
import pe.com.claro.eai.notificarclientemdb.util.JAXBUtilitariosMDB;
import pe.com.claro.eai.notificarclientemdb.util.Period;
import pe.com.claro.eai.notificarclientemdb.util.PropertiesExternosMDB;
import pe.com.claro.eai.notificarclientemdb.util.SpringBeanInterceptors;
import pe.com.claro.eai.ws.postventa.gestionacuerdo.types.ObtenerReintegroEquipoResponse;
import pe.com.claro.siacpostpago.ws.DatosClienteResponse;
import pe.com.claro.siacpostpago.ws.DatosContratoResponse;

@MessageDriven
@TransactionManagement(TransactionManagementType.BEAN)
@Interceptors(SpringBeanInterceptors.class)
public class EjecutarNotificarClienteMDBBean implements MessageListener {

	public static final Logger log = LoggerFactory.getLogger(EjecutarNotificarClienteMDBBean.class);

	@Autowired
	ValidacionMensajeXMLService validacionMensajeXMLService;

	@Autowired
	BlackListVASPublicWSService blackListVASPublicWSService;

	@Autowired
	PostpagoConsultasWSService sIACPostpagoConsultasWSService;

	@Autowired
	GestionAcuerdoWSService gestionAcuerdoWSService;

	@Autowired
	EnviarSmsWSService enviarSmsWSService;

	@Autowired
	private PropertiesExternosMDB propertiesExterno;

	@Autowired
	private ServiceJMS serviceJMS;

	@Autowired
	private TipificacionInteraccionWSService tipificacionInteraccionWSService;

	@Autowired
	PVUDAO pvudao;

	@Autowired
	GWPDAO gwpdao;

	public EjecutarNotificarClienteMDBBean() {
	}

	@Override
	public void onMessage(Message message) {

		String msgMet = "[onMessage ";
		long tiempoInicio = System.currentTimeMillis();

		try {
			log.info("--------[INICIO] - METODO: [onMessage]--------");
			log.info(msgMet + "] Leer mensaje de la cola principal");

			if (message instanceof ObjectMessage) {

				log.info(msgMet + "] Tipo de mensaje: [ObjectMessage]");
				ObjectMessage obj = (ObjectMessage) message;
				SendGwpMessageBeanRecepcion request = (SendGwpMessageBeanRecepcion) obj.getObject();

				msgMet += "idTx=" + request.getIdTransaccion() + "] ";
				log.info(msgMet + "] OBJETO [INPUT]: " + JAXBUtilitariosMDB.anyObjectToXmlText(request));

				log.info(msgMet + "] Actividad 1: Obtener MSISDN del objeto mensaje");
				Map<String, Object> mapRetorno = validacionMensajeXMLService.obtenerDatosMensaje(msgMet, request);

				String msisdn = (String) mapRetorno.get("MSISDN");
				if (!msisdn.equals(ConstantesMDB.cCADENAVACIA)) {
					int codigoContrato;
					Double comportamientoPago;
					int antiguedad;
					int numMesesContrato;
					Period periodo;
					Calendar fechaActual = Calendar.getInstance();
					fechaActual.setTime(new Date());
					int pendienteMeses;
					int pendienteDias;
					Calendar fechaAcuerdoFin = Calendar.getInstance();
					int permanenciaMeses;

					String mensajeSms;

					log.info(msgMet + "] MSISDN: " + msisdn);

					log.info(msgMet + "] Actividad 2: Validar cliente en la BlackList.");
					blackListVASPublicWSService.consultarBlackListServicio(msgMet, request, msisdn);

					log.info(msgMet + "] Actividad 3: Obtener Tipo Cliente.");
					DatosClienteResponse datosCliente = sIACPostpagoConsultasWSService.consultarDatosCliente(msgMet,
							msisdn);
					codigoContrato = ((Cliente) datosCliente.getDatosClienteResponse().getCliente().get(0)).getCoId();

					log.info(msgMet + "] Actividad 4: Obtener Fecha Activacion (Antiguedad) y Meses de contrato.");

					DatosContratoResponse contratoResponse = sIACPostpagoConsultasWSService
							.consultarDatosContrato(msgMet, codigoContrato);

					Calendar fechaContrato = contratoResponse.getDatosContratoResponse().getContrato().get(0)
							.getFechaAct().toGregorianCalendar();

					periodo = new Period(fechaActual, fechaContrato);
					antiguedad = periodo.getMesesAnios();
					numMesesContrato = Integer.parseInt(contratoResponse.getDatosContratoResponse().getContrato().get(0)
							.getPlazoContrato().split(" ")[0]);

					log.info(msgMet + "] Actividad 5: Obtener Comportamiento de Pago.");
					comportamientoPago = pvudao.obtenerComportamientoPago(msgMet, codigoContrato);

					log.info(msgMet + "] Actividad 6: Obtener la fecha de inicio de contrato.");
					ObtenerReintegroEquipoResponse reintegroEquipo = gestionAcuerdoWSService
							.obtenerReintegroEquipo(msgMet, request, msisdn);

					SimpleDateFormat sdf = new SimpleDateFormat(
							propertiesExterno.ESBGESTIONACUERDO_FORMATO_FECHA_FIN_ACUERDO);
					fechaAcuerdoFin.setTime(sdf.parse(reintegroEquipo.getAcuerdoFechaFin()));
					periodo = new Period(fechaAcuerdoFin, fechaActual);

					pendienteMeses = periodo.getMesesAnios();
					pendienteDias = periodo.getDias();

					fechaAcuerdoFin.add(Calendar.MONTH, -numMesesContrato);
					periodo = new Period(fechaActual, fechaAcuerdoFin);
					permanenciaMeses = periodo.getMesesAnios();

					log.info(msgMet + "] Actividad 7: Obtener Cuerpo de Mensaje.");
					mensajeSms = gwpdao.obtenerCuerpoMensaje(msgMet, numMesesContrato, antiguedad, permanenciaMeses,
							comportamientoPago.intValue(), pendienteMeses, pendienteDias);

					log.info(msgMet + "] Actividad 8: Enviar Mensaje.");
					enviarSmsWSService.enviarSMS(msgMet, request, msisdn, mensajeSms);

					log.info(msgMet + "] Actividad 9: Tipificar transacci�n.");
					tipificacionInteraccionWSService.crearInteraccion(msgMet, request, msisdn, mensajeSms);

				} else {
					log.info(msgMet + "] No se pudo obtener MSISDN del objeto mensaje");
				}
			}
		} catch (DBException e) {
			log.error(msgMet + "Error en DB: [CODIGO]-> " + e.getCode());
			log.error(msgMet + "Error en DB: [MENSAJE]-> " + e.getMessage());
		} catch (WSException e) {
			log.error(msgMet + "Error en WS: [CODIGO]-> " + e.getCode());
			log.error(msgMet + "Error en WS: [MENSAJE]-> " + e.getMessage());
		} catch (JMSException e) {
			log.error(msgMet + "Error en JMS: " + e.getMessage());
		} catch (ReintentosException e) {
			log.info(msgMet + "-------- enviando a cola de error [INICIO] --------");

			SendGwpMessageBeanRecepcion mensaje = null;
			try {
				mensaje = (SendGwpMessageBeanRecepcion) ((ObjectMessage) message).getObject();
			} catch (javax.jms.JMSException ex) {
				log.error(msgMet + "Error en JMS: " + ex.getMessage());
			}
			Integer cantidadIntentosIN = propertiesExterno.NRO_REINTENTOS_COLA_ERROR;

			while (cantidadIntentosIN > 0) {
				try {
					log.info(msgMet + "Enviando Mensaje a la cola Error");
					serviceJMS.enviarObjectJMS(msgMet, mensaje);
					log.info(msgMet + "Se envio mensaje correctamente...");
					break;
				} catch (BaseException ex) {
					log.info(msgMet + "Lanzando... [JMSException] - Reintento " + cantidadIntentosIN);
					cantidadIntentosIN--;
				} finally {
					mensaje.setReintentos(mensaje.getReintentos() + 1);
				}
			}

			if (cantidadIntentosIN <= 0) {
				log.info(msgMet + "No se envi� mensaje a cola de Error");
			}
			log.info(msgMet + "-------- enviando a cola de error [FIN] --------");
		} catch (Exception e) {
			log.error(msgMet + "Error: " + e);

		} finally {
			log.info(msgMet + "Tiempo total de proceso(ms): " + (System.currentTimeMillis() - tiempoInicio)
					+ " milisegundos.");
			log.info(msgMet + "--------[FIN] - METODO: [onMessage]--------");
		}
	}

}
