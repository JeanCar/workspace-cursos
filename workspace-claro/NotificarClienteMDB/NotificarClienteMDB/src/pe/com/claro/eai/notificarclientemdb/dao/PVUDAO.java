package pe.com.claro.eai.notificarclientemdb.dao;

import pe.com.claro.eai.notificarclientemdb.exception.DBException;
import pe.com.claro.eai.notificarclientemdb.exception.ReintentosException;

public interface PVUDAO {

	public Double obtenerComportamientoPago(String cadenaTrazabilidadParam, int codigoContrato)
			throws DBException, ReintentosException;

}
