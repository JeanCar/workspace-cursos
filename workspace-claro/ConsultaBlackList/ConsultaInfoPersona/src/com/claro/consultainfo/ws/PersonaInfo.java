package com.claro.consultainfo.ws;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonaInfo {

	
	public static String validarPersona(String dni,String correo){
	    String prmdni="",prmcorreo="";
		String respuesta= "Error de validacion";
		 String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
		try {
			prmdni = (dni==null)?"":dni;
			prmcorreo = (correo==null)?"":correo;
			Pattern pattern = Pattern.compile(emailPattern);
			Matcher mather = pattern.matcher(prmcorreo);
			 
	        if(!prmdni.equalsIgnoreCase("") && !prmcorreo.equalsIgnoreCase("") && prmdni.length()==8 && mather.find() == true ) 	
	        	 respuesta = "valido";
	        else
	        	respuesta = "no valido";
	        
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	        return respuesta;
	}
	
	public static String obtenerPersona(String cadena){
	//"http://127.0.0.1:8090/EjemploPersona/ConsultaPersona"
		
		String ok="No encontro respuesta";
		StringBuilder resultado = new StringBuilder();
		try {

         URL url = new URL(cadena);
         HttpURLConnection conn = (HttpURLConnection) url.openConnection();
         conn.setRequestMethod("GET");
         conn.setRequestProperty("Accept", "application/json");
         if (conn.getResponseCode() != 200) {
             throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
         }
         InputStreamReader in = new InputStreamReader(conn.getInputStream());
         BufferedReader br = new BufferedReader(in);
         String output; 
         while ((output = br.readLine()) != null) {
        	 resultado.append(output+ "\n");
         }
         
         ok= resultado.toString();
         conn.disconnect();
        
     } catch (Exception e) {
         System.out.println("Exception in NetClientGet:- " + e);
         resultado.append("error de mensaje");
     }
	 return ok;
	 
	}
	
}
