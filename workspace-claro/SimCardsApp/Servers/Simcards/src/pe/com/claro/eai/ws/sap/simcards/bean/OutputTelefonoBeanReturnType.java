package pe.com.claro.eai.ws.sap.simcards.bean;


import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlReturnType;
import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.List;

import pe.com.claro.eai.ws.sap.simcards.types.ItWorkType;

public class OutputTelefonoBeanReturnType implements SqlReturnType{
    
    
    private static Logger LOGGER= Logger.getLogger(OutputTelefonoBeanReturnType.class);
    
   
	private RowMapper		rowMapper	= new RowMapper<ItWorkType>(){
		public ItWorkType mapRow( ResultSet rs, int numeroFila ) throws SQLException{
			ItWorkType objBean = new ItWorkType();
			objBean.setNroTelef( rs.getString( "P_NRO_TELEF" ) );
			return objBean;
		}
		
	};
    

            public Object getTypeValue( CallableStatement cs, int ix, int sqlType, String typeName ) throws SQLException{

                    ResultSet rs = null;
                    List lista = null;

                    try{
                            rs = (ResultSet)cs.getObject( ix );
                            lista = new ArrayList();

                            for( int i = 0; rs.next(); i++ ){
                                    lista.add( rowMapper.mapRow( rs, i ) );
                            }

                            return lista;
                    }
                    catch( SQLException e ){
                            this.LOGGER.info( "ERROR [SQLException]: " + e.getMessage() );

                            if( ( e.getMessage() != null ) && ( e.getMessage().startsWith( "Cursor is closed" ) ) ){
                                    this.LOGGER.info( "[Cursor is closed]: " );

                                    return new ArrayList();
                            }
                            else{
                                    throw e;
                            }
                    }
                    finally{
                            if( rs != null ){
                                    rs.close();
                            }
                    }
            }
}
