package pe.com.claro.eai.ws.sap.simcards.util;


import java.net.InetAddress;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class Utilitarios {
    
    private static final Logger logger= Logger.getLogger( Utilitarios.class );
      
      

  public static Timestamp ampliarFechaRec(Timestamp fechaRec, int ampliacion){
    Calendar cal = new GregorianCalendar();
    cal.setTimeInMillis(fechaRec.getTime());
    cal.add(Calendar.DATE, ampliacion);
    
    Timestamp fechaRecargaAmpliada = new Timestamp(cal.getTimeInMillis());
    return fechaRecargaAmpliada;
  }
    
    public static boolean isCurrentDateInPeriod(Date fromDate,Date toDate) throws Exception{
        
        try{
          Calendar currentDate= (new GregorianCalendar());
          currentDate.set( Calendar.HOUR_OF_DAY,0 );
          currentDate.set( Calendar.MINUTE,0 );
          currentDate.set( Calendar.SECOND,0 );
          currentDate.set( Calendar.MILLISECOND,0 );
          
          long fromDateMillis= fromDate.getTime();
          long toDateMillis= toDate.getTime();
          long sysdateMillis= currentDate.getTimeInMillis();
  
          if( sysdateMillis>=fromDateMillis && sysdateMillis<=toDateMillis ){
            return true;
          }
        } catch(Exception e){
            logger.error("Error en parseo de fechas:",e);
          throw new Exception("Error en parseo de fechas");
        }
        
        return false;
      }
    
    public static String obtainLocalIP(){
       String ip= null; 
        try {
          InetAddress thisIp =InetAddress.getLocalHost();
          ip= thisIp.getHostAddress();
        }
        catch(Exception e) {
            logger.error("Error obteniendo IP local:",e);
        }
        return ip;
    }
    
    public static String cambiarFormatoFecha(Timestamp fecha, String format){
        try{
          SimpleDateFormat sdf=new SimpleDateFormat(format);
          return sdf.format(fecha.getTime());    
        }catch(Exception e){
          return " ";
        }
    }    
}
