package pe.com.claro.eai.ws.sap.simcards;

import javax.annotation.PostConstruct;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import javax.jws.soap.SOAPBinding;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import pe.com.claro.eai.ws.sap.simcards.service.SimcardsService;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarSimcardPorReposicionRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarSimcardPorReposicionResponse;
import pe.com.claro.eai.ws.sap.simcards.types.AsociarNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.AsociarNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusTelefonoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusTelefonoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusVendidoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusVendidoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarStatusTelefonoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarStatusTelefonoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoDispRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoDispResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoTodosRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoTodosResponse;
import pe.com.claro.eai.ws.sap.simcards.types.DesasociarNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.DesasociarNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.EliminarNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.EliminarNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ListarLineasPrereservadasRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ListarLineasPrereservadasResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ListarMSISDNreservaRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ListarMSISDNreservaResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObjectFactory;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerDatosNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerDatosNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerHLRRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerHLRResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPostRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPostResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPreRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPreResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.RegistroIMSIPortabilidadRequest;
import pe.com.claro.eai.ws.sap.simcards.types.RegistroIMSIPortabilidadResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ReservaOnlineRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ReservaOnlineResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ReversionCambioNumeroRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ReversionCambioNumeroResponse;
import pe.com.claro.eai.ws.sap.simcards.types.RollbackPelVendidoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.RollbackPelVendidoResponse;

@XmlSeeAlso( { ObjectFactory.class })
@WebService(name = "SimcardsPortType", serviceName = "ebsSimcards", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards", portName = "ebsSimcards", wsdlLocation = "/WEB-INF/wsdl/ebsSimcards.wsdl")
public class SimcardsPortTypeImpl {

    @Autowired
    private SimcardsService simcardsService;

    public SimcardsPortTypeImpl() {
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/consultarTelefonoDisp", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/consultarTelefonoDispResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/consultarTelefonoDisp")
    @WebResult(name = "consultarTelefonoDispResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ConsultarTelefonoDispResponse consultarTelefonoDisp(@WebParam(name = "consultarTelefonoDispRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ConsultarTelefonoDispRequest parameters) {
        return simcardsService.consultarTelefonoDisp(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/consultarTelefonoTodos", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/consultarTelefonoTodosResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/consultarTelefonoTodos")
    @WebResult(name = "consultarTelefonoTodosResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ConsultarTelefonoTodosResponse consultarTelefonoTodos(@WebParam(name = "consultarTelefonoTodosRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ConsultarTelefonoTodosRequest parameters) {
        return simcardsService.consultarTelefonoTodos(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/consultarStatusTelefono", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/consultarStatusTelefonoResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/consultarStatusTelefono")
    @WebResult(name = "consultarStatusTelefonoResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ConsultarStatusTelefonoResponse consultarStatusTelefono(@WebParam(name = "consultarStatusTelefonoRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ConsultarStatusTelefonoRequest parameters) {
        return simcardsService.consultarStatusTelefono(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/cambiarStatusTelefono", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/cambiarStatusTelefonoResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/cambiarStatusTelefono")
    @WebResult(name = "cambiarStatusTelefonoResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public CambiarStatusTelefonoResponse cambiarStatusTelefono(@WebParam(name = "cambiarStatusTelefonoRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        CambiarStatusTelefonoRequest parameters) {
        return simcardsService.cambiarStatusTelefono(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/cambiarStatus", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/cambiarStatusResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/cambiarStatus")
    @WebResult(name = "cambiarStatusResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public CambiarStatusResponse cambiarStatus(@WebParam(name = "cambiarStatusRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        CambiarStatusRequest parameters) {
        return simcardsService.cambiarStatus(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/reversionCambioNumero", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/reversionCambioNumeroResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/reversionCambioNumero")
    @WebResult(name = "reversionCambioNumeroResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ReversionCambioNumeroResponse reversionCambioNumero(@WebParam(name = "reversionCambioNumeroRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ReversionCambioNumeroRequest parameters) {
        return simcardsService.reversionCambioNumero(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/registroIMSIPortabilidad", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/registroIMSIPortabilidadResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/registroIMSIPortabilidad")
    @WebResult(name = "registroIMSIPortabilidadResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public RegistroIMSIPortabilidadResponse registroIMSIPortabilidad(@WebParam(name = "registroIMSIPortabilidadRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        RegistroIMSIPortabilidadRequest parameters) {
        return simcardsService.registroIMSIPortabilidad(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/actualizarNroTelef", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/actualizarNroTelefResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/actualizarNroTelef")
    @WebResult(name = "actualizarNroTelefResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ActualizarNroTelefResponse actualizarNroTelef(@WebParam(name = "actualizarNroTelefRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ActualizarNroTelefRequest parameters) {
        return simcardsService.actualizarNroTelef(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/eliminarNroTelef", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/eliminarNroTelefResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/eliminarNroTelef")
    @WebResult(name = "eliminarNroTelefResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public EliminarNroTelefResponse eliminarNroTelef(@WebParam(name = "eliminarNroTelefRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        EliminarNroTelefRequest parameters) {
        return simcardsService.eliminarNroTelef(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/listarMSISDNreserva", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/listarMSISDNreservaResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/listarMSISDNreserva")
    @WebResult(name = "listarMSISDNreservaResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ListarMSISDNreservaResponse listarMSISDNreserva(@WebParam(name = "listarMSISDNreservaRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ListarMSISDNreservaRequest parameters) {
        return simcardsService.listarMSISDNreserva(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/rollbackPelVendido", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/rollbackPelVendidoResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/rollbackPelVendido")
    @WebResult(name = "rollbackPelVendidoResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public RollbackPelVendidoResponse rollbackPelVendido(@WebParam(name = "rollbackPelVendidoRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        RollbackPelVendidoRequest parameters) {
        return simcardsService.rollbackPelVendido(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/cambiarStatusVendido", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/cambiarStatusVendidoResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/cambiarStatusVendido")
    @WebResult(name = "cambiarStatusVendidoResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public CambiarStatusVendidoResponse cambiarStatusVendido(@WebParam(name = "cambiarStatusVendidoRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        CambiarStatusVendidoRequest parameters) {
        return simcardsService.cambiarStatusVendido(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/obtenerDatosNroTelef", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/obtenerDatosNroTelefResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/obtenerDatosNroTelef")
    @WebResult(name = "obtenerDatosNroTelefResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ObtenerDatosNroTelefResponse obtenerDatosNroTelef(@WebParam(name = "obtenerDatosNroTelefRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ObtenerDatosNroTelefRequest parameters) {
        return simcardsService.obtenerDatosNroTelef(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelefSerie", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/obtenerNroTelefSerieResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelefSerie")
    @WebResult(name = "obtenerNroTelefSerieResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ObtenerNroTelefSerieResponse obtenerNroTelefSerie(@WebParam(name = "obtenerNroTelefSerieRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ObtenerNroTelefSerieRequest parameters) {
        return simcardsService.obtenerNroTelefSerie(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelef", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/obtenerNroTelefResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelef")
    @WebResult(name = "obtenerNroTelefResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ObtenerNroTelefResponse obtenerNroTelef(@WebParam(name = "obtenerNroTelefRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ObtenerNroTelefRequest parameters) {
        return simcardsService.obtenerNroTelef(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelef", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/obtenerHLRResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelef")
    @WebResult(name = "obtenerHLRResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ObtenerHLRResponse obtenerHLR(@WebParam(name = "obtenerHLRRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ObtenerHLRRequest parameters) {
        return simcardsService.obtenerHLR(parameters);
    }
  @PostConstruct
    @WebMethod(exclude = true)
    public void init() {
       SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
  }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/actualizarSimcardPorReposicion", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/actualizarSimcardPorReposicionResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/actualizarSimcardPorReposicion")
    @WebResult(name = "actualizarSimcardPorReposicionResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ActualizarSimcardPorReposicionResponse actualizarSimcardPorReposicion(@WebParam(name = "actualizarSimcardPorReposicionRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ActualizarSimcardPorReposicionRequest parameters) {
        return simcardsService.actualizarSimcardPorReposicion(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/asociarNroTelefSerie", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/asociarNroTelefSerieResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/asociarNroTelefSerie")
    @WebResult(name = "asociarNroTelefSerieResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public AsociarNroTelefSerieResponse asociarNroTelefSerie(@WebParam(name = "asociarNroTelefSerieRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        AsociarNroTelefSerieRequest parameters) {
        return simcardsService.asociarNroTelefSerie(parameters);
    }  

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/desasociarNroTelefSerie", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/desasociarNroTelefSerieResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/desasociarNroTelefSerie")
    @WebResult(name = "desasociarNroTelefSerieResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public DesasociarNroTelefSerieResponse desasociarNroTelefSerie(@WebParam(name = "desasociarNroTelefSerieRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        DesasociarNroTelefSerieRequest parameters) {
        return simcardsService.desasociarNroTelefSerie(parameters);
    }
	
    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelefPost", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/obtenerNroTelefPostResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelefPost")
    @WebResult(name = "obtenerNroTelefPostResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ObtenerNroTelefPostResponse obtenerNroTelefPost(@WebParam(name = "obtenerNroTelefPostRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ObtenerNroTelefPostRequest parameters) {
        return simcardsService.obtenerNroTelefPost(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelefPre", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/obtenerNroTelefPreResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/obtenerNroTelefPre")
    @WebResult(name = "obtenerNroTelefPreResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ObtenerNroTelefPreResponse obtenerNroTelefPre(@WebParam(name = "obtenerNroTelefPreRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ObtenerNroTelefPreRequest parameters) {
        return simcardsService.obtenerNroTelefPre(parameters);
    }

   @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/listarLineasPrereservadas", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/listarLineasPrereservadasResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/listarLineasPrereservadas")
    @WebResult(name = "listarLineasPrereservadasResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ListarLineasPrereservadasResponse listarLineasPrereservadas(@WebParam(name = "listarLineasPrereservadasRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ListarLineasPrereservadasRequest parameters) {
        return simcardsService.listarLineasPrereservadas(parameters);
    }

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @Action(input = "http://claro.com.pe/eai/ws/sap/simcards/reservaOnline", output = "http://claro.com.pe/eai/ws/sap/simcards/SimcardsPortType/reservaOnlineResponse")
    @WebMethod(action = "http://claro.com.pe/eai/ws/sap/simcards/reservaOnline")
    @WebResult(name = "reservaOnlineResponse", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types", partName = "parameters")
    public ReservaOnlineResponse reservaOnline(@WebParam(name = "reservaOnlineRequest", partName = "parameters", targetNamespace = "http://claro.com.pe/eai/ws/sap/simcards/types")
        ReservaOnlineRequest parameters) {
        return simcardsService.reservaonline(parameters);
    }
}