
package pe.com.claro.eai.ws.sap.simcards.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTransaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="itReturn">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="itReturnRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itReturnType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="itWork">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="itWorkRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itWorkType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="itNroTel">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="itNroTelRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itNroTelType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idTransaccion",
    "codigoResultado",
    "mensajeResultado",
    "itReturn",
    "itWork",
    "itNroTel"
})
@XmlRootElement(name = "listarMSISDNreservaResponse")
public class ListarMSISDNreservaResponse {

    @XmlElement(required = true)
    protected String idTransaccion;
    @XmlElement(required = true)
    protected String codigoResultado;
    @XmlElement(required = true)
    protected String mensajeResultado;
    @XmlElement(required = true)
    protected ListarMSISDNreservaResponse.ItReturn itReturn;
    @XmlElement(required = true)
    protected ListarMSISDNreservaResponse.ItWork itWork;
    @XmlElement(required = true)
    protected ListarMSISDNreservaResponse.ItNroTel itNroTel;

    /**
     * Gets the value of the idTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * Sets the value of the idTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTransaccion(String value) {
        this.idTransaccion = value;
    }

    /**
     * Gets the value of the codigoResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResultado() {
        return codigoResultado;
    }

    /**
     * Sets the value of the codigoResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResultado(String value) {
        this.codigoResultado = value;
    }

    /**
     * Gets the value of the mensajeResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeResultado() {
        return mensajeResultado;
    }

    /**
     * Sets the value of the mensajeResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeResultado(String value) {
        this.mensajeResultado = value;
    }

    /**
     * Gets the value of the itReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ListarMSISDNreservaResponse.ItReturn }
     *     
     */
    public ListarMSISDNreservaResponse.ItReturn getItReturn() {
        return itReturn;
    }

    /**
     * Sets the value of the itReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListarMSISDNreservaResponse.ItReturn }
     *     
     */
    public void setItReturn(ListarMSISDNreservaResponse.ItReturn value) {
        this.itReturn = value;
    }

    /**
     * Gets the value of the itWork property.
     * 
     * @return
     *     possible object is
     *     {@link ListarMSISDNreservaResponse.ItWork }
     *     
     */
    public ListarMSISDNreservaResponse.ItWork getItWork() {
        return itWork;
    }

    /**
     * Sets the value of the itWork property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListarMSISDNreservaResponse.ItWork }
     *     
     */
    public void setItWork(ListarMSISDNreservaResponse.ItWork value) {
        this.itWork = value;
    }

    /**
     * Gets the value of the itNroTel property.
     * 
     * @return
     *     possible object is
     *     {@link ListarMSISDNreservaResponse.ItNroTel }
     *     
     */
    public ListarMSISDNreservaResponse.ItNroTel getItNroTel() {
        return itNroTel;
    }

    /**
     * Sets the value of the itNroTel property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListarMSISDNreservaResponse.ItNroTel }
     *     
     */
    public void setItNroTel(ListarMSISDNreservaResponse.ItNroTel value) {
        this.itNroTel = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="itNroTelRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itNroTelType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itNroTelRow"
    })
    public static class ItNroTel {

        protected List<ItNroTelType> itNroTelRow;

        /**
         * Gets the value of the itNroTelRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the itNroTelRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItNroTelRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItNroTelType }
         * 
         * 
         */
        public List<ItNroTelType> getItNroTelRow() {
            if (itNroTelRow == null) {
                itNroTelRow = new ArrayList<ItNroTelType>();
            }
            return this.itNroTelRow;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="itReturnRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itReturnType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itReturnRow"
    })
    public static class ItReturn {

        protected List<ItReturnType> itReturnRow;

        /**
         * Gets the value of the itReturnRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the itReturnRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItReturnRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItReturnType }
         * 
         * 
         */
        public List<ItReturnType> getItReturnRow() {
            if (itReturnRow == null) {
                itReturnRow = new ArrayList<ItReturnType>();
            }
            return this.itReturnRow;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="itWorkRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itWorkType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itWorkRow"
    })
    public static class ItWork {

        protected List<ItWorkType> itWorkRow;

        /**
         * Gets the value of the itWorkRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the itWorkRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItWorkRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItWorkType }
         * 
         * 
         */
        public List<ItWorkType> getItWorkRow() {
            if (itWorkRow == null) {
                itWorkRow = new ArrayList<ItWorkType>();
            }
            return this.itWorkRow;
        }

    }

}
