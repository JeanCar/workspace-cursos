package pe.com.claro.eai.ws.sap.simcards.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnType;
import org.springframework.jdbc.core.SqlTypeValue;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.AbstractSqlTypeValue;
import org.springframework.stereotype.Repository;
import pe.com.claro.eai.ws.sap.simcards.bean.OutputTelefonoBeanReturnType;
import pe.com.claro.eai.ws.sap.simcards.bean.DesasociarBean;
import pe.com.claro.eai.ws.sap.simcards.bean.ResponseBean;
import pe.com.claro.eai.ws.sap.simcards.exception.DBException;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarSimcardPorReposicionRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarSimcardPorReposicionResponse;
import pe.com.claro.eai.ws.sap.simcards.types.AsociarNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.AsociarNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusTelefonoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusTelefonoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusVendidoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusVendidoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarStatusTelefonoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarStatusTelefonoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoDispRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoDispResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoTodosRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoTodosResponse;
import pe.com.claro.eai.ws.sap.simcards.types.DesasociarNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.DesasociarNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.EliminarNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.EliminarNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.EsResumenType;
import pe.com.claro.eai.ws.sap.simcards.types.EtDetalleType;
import pe.com.claro.eai.ws.sap.simcards.types.GtOutTabType;
import pe.com.claro.eai.ws.sap.simcards.types.ItDataType;
import pe.com.claro.eai.ws.sap.simcards.types.ItMatSerType;
import pe.com.claro.eai.ws.sap.simcards.types.ItNroTelType;
import pe.com.claro.eai.ws.sap.simcards.types.ItNumTelefType;
import pe.com.claro.eai.ws.sap.simcards.types.ItReturnType;
import pe.com.claro.eai.ws.sap.simcards.types.ItTelSerType;
import pe.com.claro.eai.ws.sap.simcards.types.ItWorkType;
import pe.com.claro.eai.ws.sap.simcards.types.ListarMSISDNreservaRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ListarMSISDNreservaResponse;
import pe.com.claro.eai.ws.sap.simcards.types.NroSimcardsDataType;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerDatosNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerDatosNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerHLRRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerHLRResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPostRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPostResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPreRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPreResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.RegistroIMSIPortabilidadRequest;
import pe.com.claro.eai.ws.sap.simcards.types.RegistroIMSIPortabilidadResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ReversionCambioNumeroRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ReversionCambioNumeroResponse;
import pe.com.claro.eai.ws.sap.simcards.types.RollbackPelVendidoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.RollbackPelVendidoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ListarLineasPrereservadasRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ListarLineasPrereservadasResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ReservaOnlineRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ReservaOnlineResponse;
import pe.com.claro.eai.ws.sap.simcards.util.*;

@Repository
public class EAIDAOImpl implements EAIDAO {
  private static Logger logger= Logger.getLogger(EAIDAOImpl.class);
    @Autowired
    @Qualifier("eaiDS")
    private DataSource eaiDS;
    
    @Value("${db.eai.timeout.execution.db}")
    private Integer timeoutExecutionDb;
    
    
    public EAIDAOImpl() {
      super();
    }
    
//*****************************************************************************************************
// METODO : consultarTelefonoDisp
//          Consulta todos los telefonos con estado disponible y por codigo de HLR y Regi�n.
//    
//*****************************************************************************************************    
  public ConsultarTelefonoDispResponse consultarTelefonoDisp(ConsultarTelefonoDispRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[consultarTelefonoDisp]-";
    logger.info(traId + "======== Inicio del metodo consultarTelefonoDisp");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ConsultarTelefonoDispResponse response = new ConsultarTelefonoDispResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        objParametrosIN = new MapSqlParameterSource();
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN.addValue("P_CANTIDAD_TELEF", parametrosIn.getCantidadTelef(), OracleTypes.INTEGER);
        objParametrosIN.addValue("P_CLASIF_RED", parametrosIn.getClasifRed(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_CLIENTE", parametrosIn.getTipoCliente(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CODIGO_NACIONAL", parametrosIn.getCodigoNacional(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_NRO_TELEF", parametrosIn.getTipoNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CODIGO_HLR", parametrosIn.getCodigoHlr(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
 
        logger.info(traId + "Se procede a procesar conexion en EAI: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CONSULTAR_TELEFONO_DISP);

        SqlParameter[] parametros = new SqlParameter[9];
        parametros[0] = new SqlParameter("P_CANTIDAD_TELEF", oracle.jdbc.OracleTypes.INTEGER);
        parametros[1] = new SqlParameter("P_CLASIF_RED", oracle.jdbc.OracleTypes.VARCHAR);
        parametros[2] = new SqlParameter("P_TIPO_CLIENTE", oracle.jdbc.OracleTypes.VARCHAR);
        parametros[3] = new SqlParameter("P_CODIGO_NACIONAL", oracle.jdbc.OracleTypes.VARCHAR);
        parametros[4] = new SqlParameter("P_TIPO_NRO_TELEF", oracle.jdbc.OracleTypes.VARCHAR);
        parametros[5] = new SqlParameter("P_CODIGO_HLR", oracle.jdbc.OracleTypes.VARCHAR);
        parametros[6] = new SqlParameter("P_NRO_TELEF", oracle.jdbc.OracleTypes.VARCHAR);
        parametros[7] = new SqlOutParameter("IT_DATA", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItData(traId));
        parametros[8] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout( timeoutExecutionDb );
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CONSULTAR_TELEFONO_DISP);
        logger.info(traId + "P_CANTIDAD_TELEF : " + parametrosIn.getCantidadTelef());
        logger.info(traId + "P_CLASIF_RED     : " + parametrosIn.getClasifRed());
        logger.info(traId + "P_TIPO_CLIENTE   : " + parametrosIn.getTipoCliente());
        logger.info(traId + "P_CODIGO_NACIONAL: " + parametrosIn.getCodigoNacional());
        logger.info(traId + "P_TIPO_NRO_TELEF : " + parametrosIn.getTipoNroTelef());
        logger.info(traId + "P_CODIGO_HLR     : " + parametrosIn.getCodigoHlr());
        logger.info(traId + "P_NRO_TELEF      : " + parametrosIn.getNroTelef());
        
        boolean existeData=false;
        
      
        List<ItReturnType> listaOUT2 = (List<ItReturnType>)objResultado.get( "IT_RETURN" );
        Iterator<ItReturnType> it2=listaOUT2.iterator();
        ConsultarTelefonoDispResponse.ItReturn itReturn = new  ConsultarTelefonoDispResponse.ItReturn();
        while (it2.hasNext()) {
            itReturn.getItReturnRow().add((it2.next()));
        }
        response.setItReturn(itReturn);
                
        List<ItDataType> listaOUT = (List<ItDataType>)objResultado.get( "IT_DATA" );
        Iterator<ItDataType> it=listaOUT.iterator();
        ConsultarTelefonoDispResponse.ItData itData = new  ConsultarTelefonoDispResponse.ItData();
        
        while (it.hasNext()) {
            itData.getItDataRow().add((it.next())); 
            existeData=true;
        }
        if(existeData!=true){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }
        
        response.setItData(itData);
    }  
    catch( Exception e){
           this.logger.error( traId + "ERROR: [Exception] - [" + e.getMessage() + "] ", e );   
      
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CONSULTAR_TELEFONO_DISP);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }                    
    }
    return response;
  }

    protected class ReturnOptionalRefCursorItData implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorItData(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItDataType>() {
          public ItDataType mapRow(ResultSet rs, int rowNum) throws SQLException {
              ItDataType itDataRow = new ItDataType();
              itDataRow.setCodigoHlr(rs.getString(1));
              itDataRow.setNroTelef(rs.getString(1));
              itDataRow.setCodigoHlr(rs.getString(2));
              itDataRow.setDescClred(rs.getString(3));
              itDataRow.setDescRegio(rs.getString(4));
              itDataRow.setDescTpcli(rs.getString(5));
              itDataRow.setDescTptlf(rs.getString(6));
              return itDataRow;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }

  protected class ReturnOptionalRefCursorItReturn implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorItReturn(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItReturnType>() {
          public ItReturnType mapRow(ResultSet rset, int rowNum) throws SQLException {
              ItReturnType itReturnRow = new ItReturnType();
              itReturnRow.setTipo(rset.getString(1));
              itReturnRow.setClaseMensaje(rset.getString(2));
              itReturnRow.setNumeroMensaje(rset.getString(3));
              itReturnRow.setMensaje(rset.getString(4));
              itReturnRow.setNumeroLog(rset.getString(5));
              itReturnRow.setNumeroConsecutivoInterno(rset.getString(6));
              itReturnRow.setMensajeV1(rset.getString(7));
              itReturnRow.setMensajeV2(rset.getString(8));
              itReturnRow.setMensajeV3(rset.getString(9));
              itReturnRow.setMensajeV4(rset.getString(10));
              itReturnRow.setParametro(rset.getString(11));
              itReturnRow.setLineas(BigInteger.valueOf(rset.getInt(12)));
              itReturnRow.setCampo(rset.getString(13));
              itReturnRow.setSistema(rset.getString(14));
              itReturnRow.setCodigo(rset.getString(15));
              return itReturnRow;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }


  protected class ReturnOptionalRefCursorItReturnCorto implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorItReturnCorto(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItReturnType>() {
          public ItReturnType mapRow(ResultSet rset, int rowNum) throws SQLException {
              ItReturnType itReturnRow = new ItReturnType();
              itReturnRow.setTipo(rset.getString(1));
              itReturnRow.setMensaje(rset.getString(2));
              itReturnRow.setCodigo(rset.getString(3));
              return itReturnRow;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }
//*****************************************************************************************************
// METODO : consultarTelefonoTodos
//          Obtiene todos los datos relacionados al n�mero de telefono independientemente de 
//          su estado.
//    
//*****************************************************************************************************    
  public ConsultarTelefonoTodosResponse consultarTelefonoTodos(ConsultarTelefonoTodosRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[consultarTelefonoTodos]-";
    logger.info(traId + "======== Inicio del metodo consultarTelefonoTodos");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ConsultarTelefonoTodosResponse response = new ConsultarTelefonoTodosResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CONSULTAR_TELEFONO_TODOS);

        SqlParameter[] parametros = new SqlParameter[3];
        parametros[0] = new SqlParameter("P_NRO_TELEF", Types.VARCHAR);        
        parametros[1] = new SqlOutParameter("IT_NUM_TELEF", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItNumTelef(traId));
        parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CONSULTAR_TELEFONO_TODOS);
        logger.info(traId + "PARAMETROS ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());

        Iterator it = ((ArrayList<ItNumTelefType>)objResultado.get("IT_NUM_TELEF")).iterator();               
        ConsultarTelefonoTodosResponse.ItNumTelef itNumTelef = new  ConsultarTelefonoTodosResponse.ItNumTelef();
        
        boolean datosOK=false;
        while (it.hasNext()) {
            itNumTelef.getItNumTelefRow().add((ItNumTelefType) it.next()); 
            datosOK=true;
        }
        if(!datosOK){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }
        
        response.setItNumTelef(itNumTelef);
        
        it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ConsultarTelefonoTodosResponse.ItReturn itReturn = new  ConsultarTelefonoTodosResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);


    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CONSULTAR_TELEFONO_TODOS);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }                    
    }
    return response;
  }

  protected class ReturnOptionalRefCursorItNumTelef implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorItNumTelef(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItNumTelefType>() {
          public ItNumTelefType mapRow(ResultSet rset, int rowNum) throws SQLException {
              ItNumTelefType itNumTelefRow = new ItNumTelefType();
              itNumTelefRow.setNroTelef(rset.getString(1));
              itNumTelefRow.setMarcCorta(rset.getString(2));
              itNumTelefRow.setCodigRtpe(rset.getString(3));
              itNumTelefRow.setPniAbierto(rset.getString(4));
              itNumTelefRow.setEmpresa(rset.getString(5));
              itNumTelefRow.setBezei(rset.getString(6));
              itNumTelefRow.setDescrRed(rset.getString(7));
              itNumTelefRow.setCodigoHlr(rset.getString(8));
              itNumTelefRow.setDescrStatus(rset.getString(9));
              return itNumTelefRow;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }

  //*****************************************************************************************************
  // METODO : consultarStatusTelefono
  //          Conuslta el estado de un  n�mero de telefono ( conuslta individual).
  //    
  //*****************************************************************************************************    

  public ConsultarStatusTelefonoResponse consultarStatusTelefono(ConsultarStatusTelefonoRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[consultarStatusTelefono]-";
    logger.info(traId + "======== Inicio del metodo consultarStatusTelefono");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ConsultarStatusTelefonoResponse response = new ConsultarStatusTelefonoResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_NROSERIE", parametrosIn.getNroSerie(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CONSULTAR_STATUS_TELEFONO);

        SqlParameter[] parametros = new SqlParameter[4];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_NROSERIE", OracleTypes.VARCHAR);        
        parametros[2] = new SqlOutParameter("GT_OUTTAB", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorGtOuttab(traId));
        parametros[3] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CONSULTAR_STATUS_TELEFONO);
        logger.info(traId + "PARAMETROS ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "P_NROSERIE: "+parametrosIn.getNroSerie());

        Iterator it = ((ArrayList<GtOutTabType>)objResultado.get("GT_OUTTAB")).iterator();       
      boolean datosOK=false;
        ConsultarStatusTelefonoResponse.GtOutTab gtOutTabType = new  ConsultarStatusTelefonoResponse.GtOutTab();
        while (it.hasNext()) {
            gtOutTabType.getGtOutTabRow().add((GtOutTabType) it.next()); 
          datosOK=true;
        }
      if(!datosOK){
        response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
        response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
      }
        response.setGtOutTab(gtOutTabType);
        
        it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ConsultarStatusTelefonoResponse.ItReturn itReturn = new  ConsultarStatusTelefonoResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CONSULTAR_STATUS_TELEFONO);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
    }
    return response;
  }

  protected class ReturnOptionalRefCursorGtOuttab implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorGtOuttab(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<GtOutTabType>() {
          public GtOutTabType mapRow(ResultSet rset, int rowNum) throws SQLException {
              GtOutTabType gtOutTabType = new GtOutTabType();
              gtOutTabType.setCodRet(BigInteger.valueOf(rset.getInt(1)));
              gtOutTabType.setDesCod(rset.getString(2));
              gtOutTabType.setTexto(rset.getString(3));
              gtOutTabType.setStatusSim(rset.getString(4));
              return gtOutTabType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }
  //*****************************************************************************************************
  // METODO : cambiarStatusTelefono
  //          Cambio de Status del Nro Telef�nico por regi�n.
  //    
  //*****************************************************************************************************    
  public CambiarStatusTelefonoResponse cambiarStatusTelefono(CambiarStatusTelefonoRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[cambiarStatusTelefono]-";
    logger.info(traId + "======== Inicio del metodo cambiarStatusTelefono");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    CambiarStatusTelefonoResponse response = new CambiarStatusTelefonoResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_CANT_NRO", parametrosIn.getCantNro().intValue(), OracleTypes.NUMBER);
        objParametrosIN.addValue("P_REGION", parametrosIn.getRegion(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CLASIF_RED", parametrosIn.getClasifRed(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CODIGO_HLR", parametrosIn.getCodigoHlr(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_CLIENTE", parametrosIn.getTipoCliente(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_STATUS_INICIAL", parametrosIn.getStatusInicial(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_STATUS_FINAL", parametrosIn.getStatusFinal(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
    
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CAMBIAR_STATUS_TELEFONO);

        SqlParameter[] parametros = new SqlParameter[12];
        parametros[0]  = new SqlParameter("P_CANT_NRO", OracleTypes.NUMBER);        
        parametros[1]  = new SqlParameter("P_REGION", OracleTypes.VARCHAR);        
        parametros[2]  = new SqlParameter("P_CLASIF_RED", OracleTypes.VARCHAR);        
        parametros[3]  = new SqlParameter("P_CODIGO_HLR", OracleTypes.VARCHAR);        
        parametros[4]  = new SqlParameter("P_TIPO_CLIENTE", OracleTypes.VARCHAR);        
        parametros[5]  = new SqlParameter("P_STATUS_INICIAL", OracleTypes.VARCHAR);        
        parametros[6]  = new SqlParameter("P_STATUS_FINAL", OracleTypes.VARCHAR);        
        parametros[7]  = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);        
        parametros[8]  = new SqlOutParameter("E_SUBRC", OracleTypes.NUMBER);
        parametros[9]  = new SqlOutParameter("ES_RESUMEN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorEsResumen(traId));
        parametros[10] = new SqlOutParameter("ET_DETALLE", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorEtDetalle(traId));
        parametros[11] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CAMBIAR_STATUS_TELEFONO);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_CANT_NRO: "+parametrosIn.getCantNro());
        logger.info(traId + "P_REGION: "+parametrosIn.getRegion());
        logger.info(traId + "P_CLASIF_RED: "+parametrosIn.getClasifRed());
        logger.info(traId + "P_CODIGO_HLR: "+parametrosIn.getCodigoHlr());
        logger.info(traId + "P_TIPO_CLIENTE: "+parametrosIn.getTipoCliente());
        logger.info(traId + "P_STATUS_INICIAL: "+parametrosIn.getStatusInicial());
        logger.info(traId + "P_STATUS_FINAL: "+parametrosIn.getStatusFinal());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        BigDecimal respuesta= (BigDecimal)objResultado.get("E_SUBRC");
        
        if(!PropertiesInternos.SP_COD_EXITO.equals(respuesta.toString())){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }
        response.setSubRc(respuesta.toBigInteger());

        Iterator it = ((ArrayList<EsResumenType>)objResultado.get("ES_RESUMEN")).iterator();               
        CambiarStatusTelefonoResponse.EsResumen esResumenType = new  CambiarStatusTelefonoResponse.EsResumen();
        while (it.hasNext()) {
            esResumenType.getEsResumenRow().add((EsResumenType) it.next()); 
        }
        response.setEsResumen(esResumenType);
        
        it = ((ArrayList<EtDetalleType>)objResultado.get("ET_DETALLE")).iterator();               
        CambiarStatusTelefonoResponse.EtDetalle etDetalleType = new  CambiarStatusTelefonoResponse.EtDetalle();
        while (it.hasNext()) {
            etDetalleType.getEtDetalleRow().add((EtDetalleType) it.next()); 
        }
        response.setEtDetalle(etDetalleType);

        it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        CambiarStatusTelefonoResponse.ItReturn itReturn = new  CambiarStatusTelefonoResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CAMBIAR_STATUS_TELEFONO);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  protected class ReturnOptionalRefCursorEsResumen implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorEsResumen(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<EsResumenType>() {
          public EsResumenType mapRow(ResultSet rset, int rowNum) throws SQLException {
              EsResumenType esResumenType = new EsResumenType();
              esResumenType.setCantSol(BigInteger.valueOf(rset.getInt(1)));
              esResumenType.setCantCarg(BigInteger.valueOf(rset.getInt(2)));
              esResumenType.setCodigoHlr(rset.getString(3));
              esResumenType.setRegion(rset.getString(4));
              esResumenType.setRegionDes(rset.getString(5));
              return esResumenType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor:"+e.getMessage());
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }    

  protected class ReturnOptionalRefCursorEtDetalle implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorEtDetalle(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<EtDetalleType>() {
          public EtDetalleType mapRow(ResultSet rset, int rowNum) throws SQLException {
              EtDetalleType etDetalleType = new EtDetalleType();
              etDetalleType.setCorrel(BigInteger.valueOf(rset.getInt(1)));
              etDetalleType.setRangoInicial(rset.getString(2));
              etDetalleType.setRangoFinal(rset.getString(3));
              etDetalleType.setCantidad(BigInteger.valueOf(rset.getInt(4)));
              etDetalleType.setRegion(rset.getString(5));
              etDetalleType.setInC(rset.getString(6));
              etDetalleType.setCodigoHlr(rset.getString(5));
              etDetalleType.setRegionDes(rset.getString(6));
              return etDetalleType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor : "+e.getMessage());
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }    

  //*****************************************************************************************************
  // METODO : cambiarStatus
  //          Actualiza el status de  solo un n�mero de Telefono.
  //    
  //*****************************************************************************************************    
  public CambiarStatusResponse cambiarStatus(CambiarStatusRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[cambiarStatus]-";
    logger.info(traId + "======== Inicio del metodo cambiarStatus");
     MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    CambiarStatusResponse response = new CambiarStatusResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_STATUS", parametrosIn.getStatus(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CAMBIAR_STATUS);

        SqlParameter[] parametros = new SqlParameter[4];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_STATUS", OracleTypes.VARCHAR);        
        parametros[2] = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);        
        parametros[3] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CAMBIAR_STATUS);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "P_STATUS: "+ parametrosIn.getStatus());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        CambiarStatusResponse.ItReturn itReturn = new  CambiarStatusResponse.ItReturn();
        boolean registroCorrecto=false;
        while (it.hasNext()) {
            ItReturnType type=(ItReturnType) it.next();
            itReturn.getItReturnRow().add(type);
            if(PropertiesInternos.SP_COD_EXITO.equals(type.getTipo())){
              registroCorrecto=true;
              break;
            }
        }
        if(!registroCorrecto){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }
        
        
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CAMBIAR_STATUS);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          

    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : reversionCambioNumero
  //          Reversi�n de cambio de n�mero.
  //    
  //*****************************************************************************************************    
  public ReversionCambioNumeroResponse reversionCambioNumero(ReversionCambioNumeroRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[reversionCambioNumero]-";
    logger.info(traId + "======== Inicio del metodo reversionCambioNumero");
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ReversionCambioNumeroResponse response = new ReversionCambioNumeroResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_TELEF_AN", parametrosIn.getTelefAn(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TELEF_NV", parametrosIn.getTelefNv(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_REVERSION_CAMBIO_NUMERO);

        SqlParameter[] parametros = new SqlParameter[5];
        parametros[0] = new SqlParameter("P_TELEF_AN", OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_TELEF_NV", OracleTypes.VARCHAR);        
        parametros[2] = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);    
        parametros[3] = new SqlOutParameter("COD_ERROR", OracleTypes.VARCHAR);            
        parametros[4] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_REVERSION_CAMBIO_NUMERO);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_TELEF_AN: "+parametrosIn.getTelefAn());
        logger.info(traId + "P_TELEF_NV: "+ parametrosIn.getTelefNv());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ReversionCambioNumeroResponse.ItReturn itReturn = new  ReversionCambioNumeroResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

        if( objResultado.get("COD_ERROR")!=null){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);                
        }

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_REVERSION_CAMBIO_NUMERO);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : registroIMSIPortabilidad
  //          Registro el codigo de  IMSI para los numero provenientes de la Portabilidad.
  //    
  //*****************************************************************************************************    
  public RegistroIMSIPortabilidadResponse registroIMSIPortabilidad(RegistroIMSIPortabilidadRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[registroIMSIPortabilidad]-";
    logger.info(traId + "======== Inicio del metodo registroIMSIPortabilidad");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    RegistroIMSIPortabilidadResponse response = new RegistroIMSIPortabilidadResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CLASIF_RED", parametrosIn.getClasifRed(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_NRO_TELEF", parametrosIn.getTipoNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_NROSERIE", parametrosIn.getNroSerie(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_CLIENTE", parametrosIn.getTipoCliente(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_REGION", parametrosIn.getRegion(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CODIGO_MATERIAL", parametrosIn.getMaterial(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_REGISTRO_IMSI_PROTABILIDAD);

        SqlParameter[] parametros = new SqlParameter[9];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_CLASIF_RED", OracleTypes.VARCHAR);        
        parametros[2] = new SqlParameter("P_TIPO_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[3] = new SqlParameter("P_NROSERIE", OracleTypes.VARCHAR);        
        parametros[4] = new SqlParameter("P_TIPO_CLIENTE", OracleTypes.VARCHAR);        
        parametros[5] = new SqlParameter("P_REGION", OracleTypes.VARCHAR);        
        parametros[6] = new SqlParameter("P_CODIGO_MATERIAL", OracleTypes.VARCHAR);        
        parametros[7] = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);        
        parametros[8] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_REGISTRO_IMSI_PROTABILIDAD);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "P_CLASIF_RED: "+ parametrosIn.getClasifRed());
        logger.info(traId + "P_TIPO_NRO_TELEF: "+ parametrosIn.getTipoNroTelef());
        logger.info(traId + "P_NROSERIE: "+ parametrosIn.getNroSerie());
        logger.info(traId + "P_TIPO_CLIENTE: "+ parametrosIn.getTipoCliente());
        logger.info(traId + "P_REGION: "+ parametrosIn.getRegion());
        logger.info(traId + "P_CODIGO_MATERIAL: "+ parametrosIn.getMaterial());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        RegistroIMSIPortabilidadResponse.ItReturn itReturn = new  RegistroIMSIPortabilidadResponse.ItReturn();
        boolean exito=false;
        while (it.hasNext()) {
          ItReturnType type=(ItReturnType) it.next();
            itReturn.getItReturnRow().add(type);
            if(PropertiesInternos.SP_COD_EXITO.equals(type.getTipo())){
              exito=true;
              break;
            }
        }
        if(!exito){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }
        
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_REGISTRO_IMSI_PROTABILIDAD);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : actualizarNroTelef
  //          Actualizacion el  n�mero de telefono antiguo por un n�mero de telefono nuevo.
  //    
  //*****************************************************************************************************    
  public ActualizarNroTelefResponse actualizarNroTelef(ActualizarNroTelefRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[actualizarNroTelef]-";
    logger.info(traId + "======== Inicio del metodo actualizarNroTelef");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ActualizarNroTelefResponse response = new ActualizarNroTelefResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_TELEF_AN", parametrosIn.getTelefAn(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TELEF_NV", parametrosIn.getTelefNv(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_ACTUALIZAR_NRO_TELEF);

        SqlParameter[] parametros = new SqlParameter[5];
        parametros[0] = new SqlParameter("P_TELEF_AN", OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_TELEF_NV", OracleTypes.VARCHAR);        
        parametros[2] = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);        
        parametros[3] = new SqlOutParameter("COD_ERROR", OracleTypes.VARCHAR);
        parametros[4] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_ACTUALIZAR_NRO_TELEF);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_TELEF_AN: "+parametrosIn.getTelefAn());
        logger.info(traId + "P_TELEF_NV: "+ parametrosIn.getTelefNv());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ActualizarNroTelefResponse.ItReturn itReturn = new  ActualizarNroTelefResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);
        
        if( objResultado.get("COD_ERROR")==null){
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO );          
        }else{
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO + " : "+objResultado.get("COD_ERROR").toString());                    
        }

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_ACTUALIZAR_NRO_TELEF);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : eliminarNroTelef
  //          Actualizacion el  n�mero de telefono antiguo por un n�mero de telefono nuevo.
  //    
  //*****************************************************************************************************    
  public EliminarNroTelefResponse eliminarNroTelef(EliminarNroTelefRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[eliminarNroTelef]-";
    logger.info(traId + "======== Inicio del metodo eliminarNroTelef");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

        
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    EliminarNroTelefResponse response = new EliminarNroTelefResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_ELIMINAR_NRO_TELEF);

        SqlParameter[] parametros = new SqlParameter[3];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);        
        parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_ELIMINAR_NRO_TELEF);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        EliminarNroTelefResponse.ItReturn itReturn = new  EliminarNroTelefResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_ELIMINAR_NRO_TELEF);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    }
    return response;
  }

  //*****************************************************************************************************
  // METODO : listarMSISDNreserva
  //          Obtener MSISDN que busque y reserve N� ZSans.
  //    
  //*****************************************************************************************************    
  public ListarMSISDNreservaResponse listarMSISDNreserva(ListarMSISDNreservaRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[listarMSISDNreserva]-";
    logger.info(traId + "======== Inicio del metodo listarMSISDNreserva");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ListarMSISDNreservaResponse response = new ListarMSISDNreservaResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_REGION", parametrosIn.getRegion(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CLASIF_RED", parametrosIn.getClasifRed(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CODIGO_HLR", parametrosIn.getCodigoHlr(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_NRO_TELEF", parametrosIn.getTipoNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_TIPO_CLIENTE", parametrosIn.getTipoCliente(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CLASIF_DESTI", parametrosIn.getClasifDesti(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_CANTIDAD_BUSCAR", parametrosIn.getCantidadBuscar().intValue(), OracleTypes.NUMBER);
        objParametrosIN.addValue("P_STATUS", parametrosIn.getStatus(), OracleTypes.VARCHAR);
    
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_LISTAR_MSISDN_RESERVA);

        SqlParameter[] parametros = new SqlParameter[11];
        parametros[0]  = new SqlParameter("P_REGION", OracleTypes.VARCHAR);        
        parametros[1]  = new SqlParameter("P_CLASIF_RED", OracleTypes.VARCHAR);        
        parametros[2]  = new SqlParameter("P_CODIGO_HLR", OracleTypes.VARCHAR);        
        parametros[3]  = new SqlParameter("P_TIPO_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[4]  = new SqlParameter("P_TIPO_CLIENTE", OracleTypes.VARCHAR);        
        parametros[5]  = new SqlParameter("P_CLASIF_DESTI", OracleTypes.VARCHAR);        
        parametros[6]  = new SqlParameter("P_CANTIDAD_BUSCAR", OracleTypes.NUMBER);        
        parametros[7]  = new SqlParameter("P_STATUS", OracleTypes.VARCHAR);        
        parametros[8] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        parametros[9]  = new SqlOutParameter("IT_WORK", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItWork(traId));
        parametros[10] = new SqlOutParameter("IT_NROTEL", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItNrotel(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_LISTAR_MSISDN_RESERVA);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_REGION: "+parametrosIn.getRegion());
        logger.info(traId + "P_CLASIF_RED: "+parametrosIn.getClasifRed());
        logger.info(traId + "P_CODIGO_HLR: "+parametrosIn.getCodigoHlr());
        logger.info(traId + "P_TIPO_NRO_TELEF: "+parametrosIn.getTipoNroTelef());
        logger.info(traId + "P_TIPO_CLIENTE: "+parametrosIn.getTipoCliente());
        logger.info(traId + "P_CLASIF_DESTI: "+parametrosIn.getClasifDesti());
        logger.info(traId + "P_CANTIDAD_BUSCAR: "+parametrosIn.getCantidadBuscar());
        logger.info(traId + "P_STATUS: "+parametrosIn.getStatus());

        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ListarMSISDNreservaResponse.ItReturn itReturn = new  ListarMSISDNreservaResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

        it = ((ArrayList<ItWorkType>)objResultado.get("IT_WORK")).iterator();               
        ListarMSISDNreservaResponse.ItWork itWorkType = new  ListarMSISDNreservaResponse.ItWork();
        while (it.hasNext()) {
            itWorkType.getItWorkRow().add((ItWorkType) it.next()); 
        }
        response.setItWork(itWorkType);
        
        it = ((ArrayList<ItNroTelType>)objResultado.get("IT_NROTEL")).iterator();               
        ListarMSISDNreservaResponse.ItNroTel itNroTelType = new  ListarMSISDNreservaResponse.ItNroTel();
        while (it.hasNext()) {
            itNroTelType.getItNroTelRow().add((ItNroTelType) it.next()); 
        }
        response.setItNroTel(itNroTelType);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_LISTAR_MSISDN_RESERVA);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  protected class ReturnOptionalRefCursorItWork implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorItWork(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItWorkType>() {
          public ItWorkType mapRow(ResultSet rset, int rowNum) throws SQLException {
              ItWorkType itWorkType = new ItWorkType();
              itWorkType.setNroTelef(rset.getString(1));
              return itWorkType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }    

  protected class ReturnOptionalRefCursorItNrotel implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorItNrotel(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItNroTelType>() {
          public ItNroTelType mapRow(ResultSet rset, int rowNum) throws SQLException {
              ItNroTelType itNroTelType = new ItNroTelType();
              itNroTelType.setNroTelef(rset.getString(1));
              return itNroTelType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }    


  //*****************************************************************************************************
  // METODO : rollbackPelVendido
  //          Rollback PEL a VENDIDO.
  //    
  //*****************************************************************************************************    
  public RollbackPelVendidoResponse rollbackPelVendido(RollbackPelVendidoRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[rollbackPelVendido]-";
    logger.info(traId + "======== Inicio del metodo rollbackPelVendido");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    RollbackPelVendidoResponse response = new RollbackPelVendidoResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NROSERIE", parametrosIn.getNroSerie(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_ROLLBACK_PEL_VENDIDO);

        SqlParameter[] parametros = new SqlParameter[3];
        parametros[0] = new SqlParameter("P_NROSERIE", OracleTypes.VARCHAR);        
        parametros[1] = new SqlOutParameter("E_SUBRC", OracleTypes.VARCHAR);        
        parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_ROLLBACK_PEL_VENDIDO);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NROSERIE: "+parametrosIn.getNroSerie());
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        RollbackPelVendidoResponse.ItReturn itReturn = new  RollbackPelVendidoResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_ROLLBACK_PEL_VENDIDO);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : cambiarStatusVendido
  //          Port-out a numero telefonico - Portabilidad.
  //    
  //*****************************************************************************************************    
  public CambiarStatusVendidoResponse cambiarStatusVendido(CambiarStatusVendidoRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[cambiarStatusVendido]-";
    logger.info(traId + "======== Inicio del metodo cambiarStatusVendido");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

        
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    CambiarStatusVendidoResponse response = new CambiarStatusVendidoResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CAMBIAR_STATUS_VENDIDO);

        SqlParameter[] parametros = new SqlParameter[4];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlOutParameter("E_SUBRC", OracleTypes.NUMBER);        
        parametros[2] = new SqlParameter("P_USUARIO", OracleTypes.VARCHAR);        
        parametros[3] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CAMBIAR_STATUS_VENDIDO);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "P_USUARIO: "+parametrosIn.getUsuario());
        
        response.setSubRc(BigInteger.valueOf(Integer.valueOf(objResultado.get("E_SUBRC").toString())));
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        CambiarStatusVendidoResponse.ItReturn itReturn = new  CambiarStatusVendidoResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }        
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CAMBIAR_STATUS_VENDIDO);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : obtenerDatosNroTelef
  //          Obtener todos los datos del n�mero telef�nico, consultando por varios par�metros.
  //    
  //*****************************************************************************************************    
  public ObtenerDatosNroTelefResponse obtenerDatosNroTelef(ObtenerDatosNroTelefRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[obtenerDatosNroTelef]-";
    logger.info(traId + "======== Inicio del metodo obtenerDatosNroTelef");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ObtenerDatosNroTelefResponse response = new ObtenerDatosNroTelefResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_MATERIAL", parametrosIn.getMaterial(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_NROSERIE", parametrosIn.getNroSerie(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_OBTENER_DATOS_NRO_TELEF);

        SqlParameter[] parametros = new SqlParameter[5];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlOutParameter("P_MATERIAL", OracleTypes.VARCHAR);        
        parametros[2] = new SqlParameter("P_NROSERIE", OracleTypes.VARCHAR);        
        parametros[3] = new SqlOutParameter("NRO_SIMCARDS_DATA", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorNroSimcardsData(traId));
        parametros[4] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_OBTENER_DATOS_NRO_TELEF);
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: " + parametrosIn.getNroTelef());
        logger.info(traId + "P_MATERIAL: " + parametrosIn.getMaterial());
        logger.info(traId + "P_NROSERIE: " + parametrosIn.getNroSerie());
        
        Iterator it = ((ArrayList<NroSimcardsDataType>)objResultado.get("NRO_SIMCARDS_DATA")).iterator();        
        ObtenerDatosNroTelefResponse.NroSimcardsData nroSimcardsData = new  ObtenerDatosNroTelefResponse.NroSimcardsData();
        while (it.hasNext()) {
            nroSimcardsData.getNroSimcardsDataRow().add((NroSimcardsDataType) it.next());            
        }
        response.setNroSimcardsData(nroSimcardsData);
        
        it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ObtenerDatosNroTelefResponse.ItReturn itReturn = new  ObtenerDatosNroTelefResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }        
        response.setItReturn(itReturn);
        
    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_OBTENER_DATOS_NRO_TELEF);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    }
    return response;
  }

  protected class ReturnOptionalRefCursorNroSimcardsData implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorNroSimcardsData(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<NroSimcardsDataType>() {
          public NroSimcardsDataType mapRow(ResultSet rset, int rowNum) throws SQLException {
              NroSimcardsDataType nroSimcardsDataType = new NroSimcardsDataType();
              nroSimcardsDataType.setNroTelef(rset.getString(1));
              nroSimcardsDataType.setRegion(rset.getString(2));
              nroSimcardsDataType.setCodigoRed(rset.getString(3));
              nroSimcardsDataType.setCodigoHlr(rset.getString(4));
              nroSimcardsDataType.setStatus(rset.getString(5));
              nroSimcardsDataType.setFecCarga(asXMLGregorianCalendar(rset.getDate(6),msgId));
              nroSimcardsDataType.setUsoCarga(rset.getString(7));
              nroSimcardsDataType.setTipoNro(rset.getString(8));
              nroSimcardsDataType.setLoteBscs(rset.getString(9));
              nroSimcardsDataType.setTipoCliente(rset.getString(10));
              nroSimcardsDataType.setCodigoDest(rset.getString(11));
              nroSimcardsDataType.setLoteDest(rset.getString(12));
              nroSimcardsDataType.setLoteMkt(rset.getString(13));
              nroSimcardsDataType.setSerNr(rset.getString(14));
              nroSimcardsDataType.setMatNr(rset.getString(15));
              nroSimcardsDataType.setKunNr(rset.getString(16));
              nroSimcardsDataType.setMatNrAntig(rset.getString(17));
              nroSimcardsDataType.setFecTrafico(asXMLGregorianCalendar(rset.getDate(18),msgId));
              nroSimcardsDataType.setBm(rset.getString(19));
              nroSimcardsDataType.setTipoDocCliente(rset.getString(20));
              nroSimcardsDataType.setCliente(rset.getString(21));
              nroSimcardsDataType.setStatusChipRep(rset.getString(22));
              nroSimcardsDataType.setFecCambio(asXMLGregorianCalendar(rset.getDate(23),msgId));
              
              return nroSimcardsDataType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();

              for (int i = 0; rs.next(); i++) {
                  lista.add(rowMapper.mapRow(rs, i));
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }    
  
  //*****************************************************************************************************
  // METODO : obtenerNroTelefSerie
  //          Obtener el n�mero telef�nico y serie, enviado como par�metros de entrada el n�mero 
  //          telef�nico y/o serie..
  //    
  //*****************************************************************************************************    
  public ObtenerNroTelefSerieResponse obtenerNroTelefSerie(ObtenerNroTelefSerieRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[obtenerNroTelefSerie]-";
    logger.info(traId + "======== Inicio del metodo obtenerNroTelefSerie");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ObtenerNroTelefSerieResponse response = new ObtenerNroTelefSerieResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_NROSERIE", parametrosIn.getNroSerie(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_OBTENER_NRO_TELEF_SERIE);

        SqlParameter[] parametros = new SqlParameter[3];
        parametros[0] = new SqlInOutParameter("P_NRO_TELEF", OracleTypes.VARCHAR);
        parametros[1] = new SqlInOutParameter("P_NROSERIE",OracleTypes.VARCHAR);
        parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturnCorto(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "P_NROSERIE: "+parametrosIn.getNroSerie());
        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_OBTENER_NRO_TELEF_SERIE);
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
       
        logger.info(traId + "PARAMETROS DE SALIDA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+(String)objResultado.get("P_NRO_TELEF"));
        logger.info(traId + "P_NROSERIE: "+(String)objResultado.get("P_NROSERIE"));
        response.setNroTelef((String)objResultado.get("P_NRO_TELEF"));
        response.setNroSerie((String)objResultado.get("P_NROSERIE"));
        if(!(response.getNroSerie()!=null && response.getNroTelef()!=null)){
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }

        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ObtenerNroTelefSerieResponse.ItReturn itReturn = new  ObtenerNroTelefSerieResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if ( getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_OBTENER_NRO_TELEF_SERIE);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }

  //*****************************************************************************************************
  // METODO : obtenerNroTelef
  //          Obtener el n�mero telef�nico, desde el env�o del c�digo de material y el n�mero de serie.
  //    
  //*****************************************************************************************************    
  public ObtenerNroTelefResponse obtenerNroTelef(final ObtenerNroTelefRequest parametrosIn){
      String traId = parametrosIn.getIdTransaccion() + "[obtenerNroTelef]-";
      logger.info(traId + "======== Inicio del metodo obtenerNroTelef");
      logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

      long tiempoInicio = System.currentTimeMillis();
      MapSqlParameterSource objParametrosIN = null;
      JdbcTemplate objJdbcTemplate = null;
      SimpleJdbcCall objJdbcCall = null;

      ObtenerNroTelefResponse response = new ObtenerNroTelefResponse();
      response.setIdTransaccion(parametrosIn.getIdTransaccion());
      response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
      response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

      try {
          eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
          objParametrosIN = new MapSqlParameterSource();
             
          SqlTypeValue sqlType = new AbstractSqlTypeValue(){                 
              protected Object createTypeValue( Connection conn,int sqlType,String typeName ) throws SQLException{           
                 ArrayDescriptor  objArrayDescriptor  = new ArrayDescriptor( typeName, conn );                                                                                              
                 StructDescriptor objStructDescriptor = StructDescriptor.createDescriptor( "OBJ_INPUT_GETNUM", conn );  
                 STRUCT[]         objStructArray      = new STRUCT[ parametrosIn.getItInput().getItInputRow().size() ];
                 STRUCT           objStruct           = null;
                 ARRAY            objArray            = null;
                                                                                       
                 logger.info("SIZE:" + parametrosIn.getItInput().getItInputRow().size());                        
                 for( int i=0; i<parametrosIn.getItInput().getItInputRow().size(); i++ ){
                    ItMatSerType objLlamadasBean = parametrosIn.getItInput().getItInputRow().get( i );        
                    Object[] attributes = { objLlamadasBean.getNroSerie() + "", 
                                            objLlamadasBean.getMaterial() + "" };                                                                                       
                    objStruct = new STRUCT( objStructDescriptor, conn, attributes );  
                    objStructArray[ i ] = objStruct;  
                    logger.info("objLlamadasBean:" + objLlamadasBean.getNroSerie());                        
                    logger.info("objLlamadasBean:" + objLlamadasBean.getMaterial());                        
                 }   
                 objArray = new ARRAY( objArrayDescriptor, conn, objStructArray );                                                                                     
                 return objArray;              
              }
          }; 
                 
          objParametrosIN.addValue("TI_INPUT",sqlType);
    
          logger.info(traId + "Se procede a procesar conexion en EAI: " + PropertiesExternos.DB_EAI_JNDI);
          objJdbcCall = new SimpleJdbcCall(eaiDS);
          objJdbcCall.withoutProcedureColumnMetaDataAccess();
          objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
          objJdbcCall.withProcedureName(PropertiesInternos.SP_OBTENER_NRO_TELEF);

          SqlParameter[] parametros = new SqlParameter[3];
          parametros[0] = new SqlParameter("TI_INPUT", Types.ARRAY, "TBL_INPUT_GETNUM");
          parametros[1] = new SqlOutParameter("TI_OUTPUT", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorTiOutput(traId));
          parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
          objJdbcCall.declareParameters(parametros);

          objJdbcTemplate = objJdbcCall.getJdbcTemplate();
          objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
          logger.info(traId + "Se obtuvo conexion con exito");
          logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_OBTENER_NRO_TELEF);
          logger.info(traId + "PARAMETROS DE ENTRADA SP: " + EaiUtil.getXmlTextFromJaxB(objParametrosIN));
          Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

          Iterator it = ((ArrayList<ItTelSerType>)objResultado.get("TI_OUTPUT")).iterator();
          ObtenerNroTelefResponse.ItOutput itOutput = new  ObtenerNroTelefResponse.ItOutput();
          while (it.hasNext()) {
              itOutput.getItOutputRow().add((ItTelSerType) it.next()); 
          }
          response.setItOutput(itOutput);
         
          it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
          ObtenerNroTelefResponse.ItReturn itReturn = new  ObtenerNroTelefResponse.ItReturn();
          while (it.hasNext()) {
              itReturn.getItReturnRow().add((ItReturnType) it.next());
          }
          response.setItReturn(itReturn);
          
          logger.info(traId + "PARAMETROS DE SALIDA:" + EaiUtil.getXmlTextFromJaxB(response));            

      } catch(Exception e){
            logger.error(traId + "Error: " + e.getMessage(), e);          
            if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
                response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
                response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_OBTENER_NRO_TELEF);
            }
            else {
                response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
                response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
            }          
            logger.error(traId + EaiUtil.getXmlTextFromJaxB(response));            
            
      } finally {
          this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
          logger.info(traId + "======== Fin del metodo obtenerNroTelef");
      }
      return response;
    }

  protected class ReturnOptionalRefCursorTiOutput implements SqlReturnType {

      private String msgId;

      public ReturnOptionalRefCursorTiOutput(String msgId) {
          this.msgId = msgId;
      }

      private RowMapper rowMapper = new RowMapper<ItTelSerType>() {
          public ItTelSerType mapRow(ResultSet rs, int rowNum) throws SQLException {
              ItTelSerType itTelSerType = new ItTelSerType();
              itTelSerType.setNroTelef(rs.getString(1));
              itTelSerType.setNroSerie(rs.getString(2));
              return itTelSerType;
          }
      };

      public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
          ResultSet rs = null;
          Object retorno = new ArrayList();
          try {
              rs = (ResultSet)cs.getObject(ix);
              List lista = new ArrayList();
              int i=0;
              while(rs.next()){
                lista.add(rowMapper.mapRow(rs, i));
                i++;  
              }
              retorno = lista;
          }
          catch (Exception e) {
                  logger.error(msgId + "No se obtuvo datos del cursor",e);
                  retorno = new ArrayList();
          }
          finally {
              try {
                  if (rs != null) {
                      if (!rs.isClosed()) {
                          rs.close();
                      }
                  }
              }
              catch (SQLException e) {
                  logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
              }
          }

          return retorno;
      }
  }

  //*****************************************************************************************************
  // METODO : obtenerHLR
  //          Consultar la tabla de los n�meros telef�nicos y obtener el c�digo del HLR.
  //    
  //*****************************************************************************************************    
  public ObtenerHLRResponse obtenerHLR(ObtenerHLRRequest parametrosIn){
    String traId = parametrosIn.getIdTransaccion() + "[obtenerHLR]-";
    logger.info(traId + "======== Inicio del metodo obtenerHLR");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    
    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ObtenerHLRResponse response = new ObtenerHLRResponse();
    response.setIdTransaccion(parametrosIn.getIdTransaccion());
    
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
  
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_OBTENER_HLR);

        SqlParameter[] parametros = new SqlParameter[3];
        parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
        parametros[1] = new SqlOutParameter("P_CODIGO_HLR", OracleTypes.VARCHAR);        
        parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_OBTENER_HLR);
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
        
        logger.info(traId + "PARAMETRO DE SALIDA P_CODIGO_HLR: "+objResultado.get("P_CODIGO_HLR"));
        //Verifica la Existencia y por ende es correcto
        if (objResultado.get("P_CODIGO_HLR")!=null && !objResultado.get("P_CODIGO_HLR").equals(""))
        {
            response.setCodigoHlr((String)objResultado.get("P_CODIGO_HLR"));
        }else{
          response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
        }
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ObtenerHLRResponse.ItReturn itReturn = new  ObtenerHLRResponse.ItReturn();
        while (it.hasNext()) {
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_OBTENER_HLR);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }          
          
    } 
    return response;
  }
  
  //*****************************************************************************************************
  // METODO : actualizarSimcardPorReposicion
  //          Actualizaci�n de material y de serie seg�n el n�mero de cliente.
  //    
  //*****************************************************************************************************   
  public ActualizarSimcardPorReposicionResponse actualizarSimcardPorReposicion(ActualizarSimcardPorReposicionRequest request) {
      
    String traId = request.getIdTransaccion() + "[actualizarSimcardPorReposicion]-";
    logger.info(traId + "======== Inicio del metodo actualizarSimcardPorReposicion");
    logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

    MapSqlParameterSource objParametrosIN = null;
    JdbcTemplate objJdbcTemplate = null;
    SimpleJdbcCall objJdbcCall = null;

    ActualizarSimcardPorReposicionResponse response = new ActualizarSimcardPorReposicionResponse();
    response.setIdTransaccion(request.getIdTransaccion());
    
    response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
    response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);

    try {
        eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
        objParametrosIN = new MapSqlParameterSource();
        objParametrosIN.addValue("P_NRO_TELEF"    ,request.getNroTelef(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_SERNR"        ,request.getNroSerie(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_MATNR"        ,request.getDescripcionProducto(), OracleTypes.VARCHAR);
        objParametrosIN.addValue("P_USUARIO"      ,request.getUsuario(), OracleTypes.VARCHAR);
    
        logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
        objJdbcCall = new SimpleJdbcCall(eaiDS);
        objJdbcCall.withoutProcedureColumnMetaDataAccess();
        objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
        objJdbcCall.withProcedureName(PropertiesInternos.SP_CAMBIAR_ESTADO_SIMCARD);

        SqlParameter[] parametros = new SqlParameter[5];
        parametros[0] = new SqlParameter("P_NRO_TELEF"  , OracleTypes.VARCHAR);        
        parametros[1] = new SqlParameter("P_SERNR"      , OracleTypes.VARCHAR);        
        parametros[2] = new SqlParameter("P_MATNR"      , OracleTypes.VARCHAR);        
        parametros[3] = new SqlParameter("P_USUARIO"    , OracleTypes.VARCHAR);        
        parametros[4] = new SqlOutParameter("IT_RETURN" , oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
        objJdbcCall.declareParameters(parametros);

        objJdbcTemplate = objJdbcCall.getJdbcTemplate();
        objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
        logger.info(traId + "Se obtuvo conexion con exito");
        logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
        logger.info(traId + "P_NRO_TELEF: "+request.getNroTelef());
        logger.info(traId + "P_SERNR:     "+request.getNroSerie());
        logger.info(traId + "P_MATNR:     "+request.getDescripcionProducto());
        logger.info(traId + "P_USUARIO:   "+request.getUsuario());
        
        logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_CAMBIAR_ESTADO_SIMCARD);
        Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
               
        response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);
        response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);
        
        Iterator it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
        ActualizarSimcardPorReposicionResponse.ItReturn itReturn = new  ActualizarSimcardPorReposicionResponse.ItReturn();
        while (it.hasNext()){
            itReturn.getItReturnRow().add((ItReturnType) it.next());
        }
        response.setItReturn(itReturn);
        logger.info(traId + "PARAMETROS DE SALIDA:" + EaiUtil.getXmlTextFromJaxB(response));  

    } catch(Exception e){
          logger.error(traId + "Error: " + e.getMessage(), e);          
          if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_CAMBIAR_ESTADO_SIMCARD);
          }
          else {
              response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
              response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
          }
    }
    return response;
  }
    //*****************************************************************************************************
    // METODO : asociarNroTelefSerie
    //          Realiza la asociaci�n de un n�mero telef�nico con la serie.
    //    
    //*****************************************************************************************************    
    public AsociarNroTelefSerieResponse asociarNroTelefSerie(AsociarNroTelefSerieRequest            parametrosIn){
      String traId = parametrosIn.getIdTransaccion() + "[asociarNroTelefSerie ]-";
      logger.info(traId + "======== Inicio del metodo asociarNroTelefSerie ============");
       MapSqlParameterSource objParametrosIN = null;
      JdbcTemplate objJdbcTemplate = null;
      int timeoutExecutionDb =PropertiesExternos.DB_SANS_TIMEOUT_EXECUTION;
      SimpleJdbcCall objJdbcCall = null;

      AsociarNroTelefSerieResponse response = new AsociarNroTelefSerieResponse();

      try {
          
          response.setIdTransaccion(parametrosIn.getIdTransaccion());
          eaiDS.setLoginTimeout(PropertiesExternos.DB_SANS_TIMEOUT_EXECUTION);
          objParametrosIN = new MapSqlParameterSource();
          objParametrosIN.addValue("P_NRO_TELEF", parametrosIn.getNroTelef(), OracleTypes.VARCHAR);
          objParametrosIN.addValue("P_NRO_SERIE", parametrosIn.getNroSerie(), OracleTypes.VARCHAR);
          objParametrosIN.addValue("P_MATERIAL", parametrosIn.getMaterial(), OracleTypes.VARCHAR);
    
          logger.info(traId + "Se procede a procesar conexion en DBZSANS: " + PropertiesExternos.DB_EAI_JNDI);
          objJdbcCall = new SimpleJdbcCall(eaiDS);
          objJdbcCall.withoutProcedureColumnMetaDataAccess();
          objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
          objJdbcCall.withProcedureName(PropertiesInternos.SP_SANSSU_ASOCIA_NROSERIE);

          SqlParameter[] parametros = new SqlParameter[5];
          parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR);        
          parametros[1] = new SqlParameter("P_NRO_SERIE", OracleTypes.VARCHAR);        
          parametros[2] = new SqlParameter("P_MATERIAL", OracleTypes.VARCHAR);        
          parametros[3] = new SqlOutParameter("P_OUTPUT", oracle.jdbc.OracleTypes.CURSOR, null, new OutputTelefonoBeanReturnType());
          parametros[4] = new SqlOutParameter("P_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
          
          objJdbcCall.declareParameters(parametros);                          
          objJdbcTemplate = objJdbcCall.getJdbcTemplate();
          objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
          logger.info(traId + "Se obtuvo conexion con exito");
          Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);

          logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_SANSSU_ASOCIA_NROSERIE);
          logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
          logger.info(traId + "P_NRO_TELEF: "+parametrosIn.getNroTelef());
          logger.info(traId + "P_NRO_SERIE: "+ parametrosIn.getNroSerie());
          logger.info(traId + "P_MATERIAL: "+parametrosIn.getMaterial());
          
          Iterator it = ((ArrayList<ItWorkType>)objResultado.get("P_OUTPUT")).iterator();               
          AsociarNroTelefSerieResponse.ItOutput ouputRes = new  AsociarNroTelefSerieResponse.ItOutput();
          
          while (it.hasNext()) {
              logger.info("registro:"+it.toString());
                 ouputRes.getItOutputRow().add((ItWorkType)it.next());
          }
          
          response.setItOutput(ouputRes);
          
          it = ((ArrayList<ItReturnType>)objResultado.get("P_RETURN")).iterator();               
                    AsociarNroTelefSerieResponse.ItReturn ouputReturn = new  AsociarNroTelefSerieResponse.ItReturn();
         
         while (it.hasNext()) {
             ouputReturn.getItReturnRow().add((ItReturnType)it.next());
         }
                     
          response.setItReturn(ouputReturn);
          

      } catch(Exception e){
            logger.error(traId + "Error: " + e.getMessage(), e);           
          String error = (e+PropertiesInternos.VACIO);
          logger.info(traId + "ERROR EN SP: " + error.toUpperCase(Locale.getDefault()) ); 
          if(error.toUpperCase(Locale.getDefault()).contains(PropertiesExternos.DB_TIMEOUT_TEXT)){
              response.setCodigoResultado(PropertiesExternos.IDT_2_CODIGO);        
              response.setMensajeResultado(PropertiesExternos.IDT_2_MENSAJE);
              logger.info(traId + "ERROR EN TIEMPO DE ESPERA: " + PropertiesExternos.IDT_2_CODIGO);
          }else{
              response.setCodigoResultado(PropertiesExternos.IDT_1_CODIGO);        
              response.setMensajeResultado(PropertiesExternos.IDT_1_MENSAJE);
              logger.info(traId + "ERROR EN SP: " + error.toUpperCase(Locale.getDefault()) ); 
          }    
      } 
      return response;
    }

  //*****************************************************************************************************
  // METODO : desasociarNroTelefSerie
  //          desasocia el numero de linea de la serie.
  //    
  //*****************************************************************************************************   
 
  public ResponseBean realizarDesosiacion(DesasociarNroTelefSerieRequest   request) throws DBException{
      
     String traId = request.getIdTransaccion() + "[realizarDesosiacion]-";
     logger.info(traId + "======== Inicio del metodo realizarDesosiacion");
     logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");
     
     MapSqlParameterSource objParametrosIN = null;
     JdbcTemplate objJdbcTemplate = null;
     SimpleJdbcCall objJdbcCall = null;
     ResponseBean responseBean = new ResponseBean();
     
     try{
       eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
       objParametrosIN = new MapSqlParameterSource();
       objParametrosIN.addValue("P_NRO_TELEF", request.getNroTelef(), OracleTypes.VARCHAR);
       objParametrosIN.addValue("P_NRO_SERIE", request.getNroSerie(), OracleTypes.VARCHAR);
        
       logger.info(traId + "Se procede a procesar conexion en  BD SANDB (ZSANS): " + PropertiesExternos.DB_EAI_JNDI);
       
       objJdbcCall = new SimpleJdbcCall(eaiDS);
       objJdbcCall.withoutProcedureColumnMetaDataAccess();
       objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
       objJdbcCall.withProcedureName(PropertiesInternos.SP_DESASOCIAR_NRO_TELEF_SERIE);
         
       SqlParameter[] parametros = new SqlParameter[3];
       parametros[0] = new SqlParameter("P_NRO_TELEF", OracleTypes.VARCHAR); 
       parametros[1] = new SqlParameter("P_NRO_SERIE", OracleTypes.VARCHAR); 
       parametros[2] = new SqlOutParameter("P_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItReturn(traId));
       objJdbcCall.declareParameters(parametros);
         
       objJdbcTemplate = objJdbcCall.getJdbcTemplate();
       objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);  
         
       logger.info(traId + "Se obtuvo conexion con exito");
       logger.info(traId + "PARAMETROS DE ENTRADA SP: " ); 
       logger.info(traId + "P_NRO_TELEF: "+request.getNroTelef());
       logger.info(traId + "P_NRO_SERIE: "+request.getNroSerie());
         
       logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_DESASOCIAR_NRO_TELEF_SERIE);
       Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
      
       Iterator it = ((ArrayList<ItReturnType>)objResultado.get("P_RETURN")).iterator();    
       DesasociarNroTelefSerieResponse.ItReturn itReturn = new  DesasociarNroTelefSerieResponse.ItReturn();
       List<DesasociarBean> listaDesasociar = new ArrayList<DesasociarBean>();
       ItReturnType itReturnType = new ItReturnType();
       int i =PropertiesInternos.LINEA_0;
       while (it.hasNext()){
           
           itReturn.getItReturnRow().add((ItReturnType) it.next());
           itReturnType = itReturn.getItReturnRow().get(i);
           DesasociarBean bean = new DesasociarBean();
             bean.setTipo(itReturnType.getTipo());
             bean.setClaseMensaje(itReturnType.getClaseMensaje());
             bean.setNumeroMensaje(itReturnType.getNumeroMensaje());
             bean.setMensaje(itReturnType.getMensaje());
             bean.setNumeroLog(itReturnType.getNumeroLog());
             bean.setNumeroConsecutivoInterno(itReturnType.getNumeroConsecutivoInterno());
             bean.setMensajeV1(itReturnType.getMensajeV1());
             bean.setMensajeV2(itReturnType.getMensajeV2());
             bean.setMensajeV3(itReturnType.getMensajeV3());
             bean.setMensajeV4(itReturnType.getMensajeV4());
             bean.setParametro(itReturnType.getParametro());
             bean.setLineas(itReturnType.getLineas());
             bean.setCampo(itReturnType.getCampo());
             bean.setSistema(itReturnType.getSistema());
             bean.setCodigo(itReturnType.getCodigo());     
           
         listaDesasociar.add(bean);
         i++;
       }
       responseBean.setCursorDesasociar(listaDesasociar);
         
       logger.info(traId + "PARAMETROS DE SALIDA:" + EaiUtil.getXmlTextFromJaxB(responseBean));  
       logger.info(traId + "======== Fin del metodo realizarDesosiacion");
         
     } catch(Exception e){
       String excepcion = e + PropertiesInternos.VACIO;
       logger.error(traId , e); 
       String msjError = null;
       String codError = null;
       
       if( excepcion.contains(PropertiesExternos.DB_EAI_ERRORSQLTIMEOUT_EXCEPTION ) ) {          
             codError = PropertiesExternos.CODIGO_GENERICO_ERROR_IDT1;
             msjError = PropertiesExternos.MENSAJE_GENERICO_ERROR_IDT1;   
       } else {
             codError = PropertiesExternos.CODIGO_GENERICO_ERROR_IDT2;
             msjError = PropertiesExternos.MENSAJE_GENERICO_ERROR_IDT2;
      }
       
      throw new DBException( codError, msjError );
     }
     return responseBean;
   }
    //*****************************************************************************************************
    // METODO : obtenerNroTelefPost
    //          Obtener el n�mero telef�nico, desde el env�o del c�digo de material y el n�mero de serie.
    //    
    //*****************************************************************************************************    
    public ObtenerNroTelefPostResponse obtenerNroTelefPost(final ObtenerNroTelefPostRequest parametrosIn) {
        String traId = parametrosIn.getIdTransaccion() + "[obtenerNroTelefPost]-";
        logger.info(traId + "======== Inicio del metodo obtenerNroTelefPost");
        logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");

        long tiempoInicio = System.currentTimeMillis();
        MapSqlParameterSource objParametrosIN = null;
        JdbcTemplate objJdbcTemplate = null;
        SimpleJdbcCall objJdbcCall = null;

        ObtenerNroTelefPostResponse response = new ObtenerNroTelefPostResponse();
        response.setIdTransaccion(parametrosIn.getIdTransaccion());
        response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDF_0_CODIGO);        
        response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDF_0_MENSAJE);
       
        try {
            eaiDS.setLoginTimeout(PropertiesExternos.DB_SANS_TIMEOUT_EXECUTION);
            objParametrosIN = new MapSqlParameterSource();

            SqlTypeValue sqlType = new AbstractSqlTypeValue(){
                protected Object createTypeValue( Connection conn,int sqlType,String typeName ) throws SQLException{           
                   ArrayDescriptor  objArrayDescriptor  = new ArrayDescriptor( typeName, conn );                                                                                              
                   StructDescriptor objStructDescriptor = StructDescriptor.createDescriptor( "OBJ_INPUT_GETNUM", conn );  
                   STRUCT[]         objStructArray      = new STRUCT[ parametrosIn.getItInput().getItInputRow().size() ];
                   STRUCT           objStruct           = null;
                   ARRAY            objArray            = null;
                                                                                         
                   logger.info("SIZE:" + parametrosIn.getItInput().getItInputRow().size());                        
                   for( int i=0; i<parametrosIn.getItInput().getItInputRow().size(); i++ ){
                      ItMatSerType objLlamadasBean = parametrosIn.getItInput().getItInputRow().get( i );        
                      Object[] attributes = { objLlamadasBean.getNroSerie() + "", 
                                              objLlamadasBean.getMaterial() + "" };                                                                                       
                      objStruct = new STRUCT( objStructDescriptor, conn, attributes );  
                      objStructArray[ i ] = objStruct;  
                      logger.info("objLlamadasBean:" + objLlamadasBean.getNroSerie());                        
                      logger.info("objLlamadasBean:" + objLlamadasBean.getMaterial());                        
                   }   
                   objArray = new ARRAY( objArrayDescriptor, conn, objStructArray );                                                                                     
                   return objArray;              
                }
            }; 
        
            objParametrosIN.addValue("TI_INPUT",sqlType);        
        
            logger.info(traId + "Se procede a procesar conexion en EAI: " + PropertiesExternos.DB_EAI_JNDI);
            objJdbcCall = new SimpleJdbcCall(eaiDS);
            objJdbcCall.withoutProcedureColumnMetaDataAccess();
            objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
            objJdbcCall.withProcedureName(PropertiesInternos.SP_OBTENER_NRO_TELEFPOST);

            SqlParameter[] parametros = new SqlParameter[3];
            parametros[0] = new SqlParameter("TI_INPUT", Types.ARRAY, "TBL_INPUT_GETNUM");
            parametros[1] = new SqlOutParameter("TI_OUTPUT", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefPostCursorTiOutput(traId));
            parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefPostCursorItReturn(traId));
            objJdbcCall.declareParameters(parametros);

            objJdbcTemplate = objJdbcCall.getJdbcTemplate();
            objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
            logger.info(traId + "Se obtuvo conexion con exito");
            logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_OBTENER_NRO_TELEFPOST);
            logger.info(traId + "PARAMETROS DE ENTRADA SP: " + EaiUtil.getXmlTextFromJaxB(objParametrosIN));
            Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
            
            Iterator it = ((ArrayList<ItTelSerType>)objResultado.get("TI_OUTPUT")).iterator();               
            ObtenerNroTelefPostResponse.ItOutput itOutput = new  ObtenerNroTelefPostResponse.ItOutput();
            while (it.hasNext()) {               
                itOutput.getItOutputRow().add((ItTelSerType) it.next()); 
            }
            response.setItOutput(itOutput);

            it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
            ObtenerNroTelefPostResponse.ItReturn itReturn = new  ObtenerNroTelefPostResponse.ItReturn();
            while (it.hasNext()) {
                itReturn.getItReturnRow().add((ItReturnType) it.next());
            }
            response.setItReturn(itReturn);
            if(response.getItReturn().getItReturnRow().get(PropertiesInternos.LINEA_0).getCodigo().equals(PropertiesExternos.CODIGO_ESTANDAR_ERROR)){
                response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDF_1_CODIGO);        
                response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDF_1_MENSAJE);
            } 
            logger.info(traId + "PARAMETROS DE SALIDA:" + EaiUtil.getXmlTextFromJaxB(response)); 
          
        } catch(Exception e){
              logger.error(traId + "Error: " + e.getMessage(), e);          
              if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == (Integer.parseInt(PropertiesInternos.VALOR_MENOS_UNO))) {
                  response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDT_1_CODIGO);        
                  response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDT_1_MENSAJE + ": " + PropertiesInternos.SP_OBTENER_NRO_TELEFPOST);
              }
              else {
                  response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDT_2_CODIGO);        
                  response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDT_2_MENSAJE);
              }          
              logger.error(traId + EaiUtil.getXmlTextFromJaxB(response));            
              
        } finally {
            this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
            logger.info(traId + "======== Fin del metodo obtenerNroTelefPost");
        }
        return response;
    }
    
    
    public ObtenerNroTelefPreResponse obtenerNroTelefPre(final ObtenerNroTelefPreRequest parametrosIn) {
        String traId= parametrosIn.getIdTransaccion() + "[obtenerNroTelefPre]-";
        logger.info(traId + "======== Inicio del metodo obtenerNroTelefPre");
        logger.info(traId + "Se procede a ejecutar SP en la DBZSANS");
            
        long tiempoInicio = System.currentTimeMillis();
        MapSqlParameterSource objParametrosIN = null;
        JdbcTemplate objJdbcTemplate = null;
        SimpleJdbcCall objJdbcCall = null;
        
        ObtenerNroTelefPreResponse response = new ObtenerNroTelefPreResponse();
        response.setIdTransaccion(parametrosIn.getIdTransaccion());
        response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDF_0_CODIGO);        
        response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDF_0_MENSAJE);
        
        try {
            eaiDS.setLoginTimeout(PropertiesExternos.DB_SANS_TIMEOUT_EXECUTION);
            objParametrosIN = new MapSqlParameterSource();

            SqlTypeValue sqlType = new AbstractSqlTypeValue(){
                protected Object createTypeValue( Connection conn,int sqlType,String typeName ) throws SQLException{           
                   ArrayDescriptor  objArrayDescriptor  = new ArrayDescriptor( typeName, conn );                                                                                              
                   StructDescriptor objStructDescriptor = StructDescriptor.createDescriptor( "OBJ_INPUT_GETNUM", conn );  
                   STRUCT[]         objStructArray      = new STRUCT[ parametrosIn.getItInput().getItInputRow().size() ];
                   STRUCT           objStruct           = null;
                   ARRAY            objArray            = null;
                                                                                         
                   logger.info("SIZE:" + parametrosIn.getItInput().getItInputRow().size());                        
                   for( int i=0; i<parametrosIn.getItInput().getItInputRow().size(); i++ ){
                      ItMatSerType objLlamadasBean = parametrosIn.getItInput().getItInputRow().get( i );        
                      Object[] attributes = { objLlamadasBean.getNroSerie() + "", 
                                              objLlamadasBean.getMaterial() + "" };                                                                                       
                      objStruct = new STRUCT( objStructDescriptor, conn, attributes );  
                      objStructArray[ i ] = objStruct;  
                      logger.info("objLlamadasBean:" + objLlamadasBean.getNroSerie());                        
                      logger.info("objLlamadasBean:" + objLlamadasBean.getMaterial());                        
                   }   
                   objArray = new ARRAY( objArrayDescriptor, conn, objStructArray );                                                                                     
                   return objArray;              
                }
            }; 
        
            objParametrosIN.addValue("TI_INPUT",sqlType);        
        
            logger.info(traId + "Se procede a procesar conexion en EAI: " + PropertiesExternos.DB_EAI_JNDI);
            objJdbcCall = new SimpleJdbcCall(eaiDS);
            objJdbcCall.withoutProcedureColumnMetaDataAccess();
            objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
            objJdbcCall.withProcedureName(PropertiesInternos.SP_OBTENER_NRO_TELEFPRE);

            SqlParameter[] parametros = new SqlParameter[3];
            parametros[0] = new SqlParameter("TI_INPUT", Types.ARRAY, "TBL_INPUT_GETNUM");
            parametros[1] = new SqlOutParameter("TI_OUTPUT", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefPreCursorTiOutput(traId));
            parametros[2] = new SqlOutParameter("IT_RETURN", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefPreCursorItReturn(traId));
            objJdbcCall.declareParameters(parametros);

            objJdbcTemplate = objJdbcCall.getJdbcTemplate();
            objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
            logger.info(traId + "Se obtuvo conexion con exito");
            logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_OBTENER_NRO_TELEFPRE);
            logger.info(traId + "PARAMETROS DE ENTRADA SP: " + EaiUtil.getXmlTextFromJaxB(objParametrosIN));
            Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
            
            Iterator it = ((ArrayList<ItTelSerType>)objResultado.get("TI_OUTPUT")).iterator();               
            ObtenerNroTelefPreResponse.ItOutput itOutput = new  ObtenerNroTelefPreResponse.ItOutput();
            while (it.hasNext()) {               
                itOutput.getItOutputRow().add((ItTelSerType) it.next()); 
            }
            response.setItOutput(itOutput);

            it = ((ArrayList<ItReturnType>)objResultado.get("IT_RETURN")).iterator();        
            ObtenerNroTelefPreResponse.ItReturn itReturn = new  ObtenerNroTelefPreResponse.ItReturn();
            while (it.hasNext()) {
                itReturn.getItReturnRow().add((ItReturnType) it.next());
            }
            response.setItReturn(itReturn);
            if(response.getItReturn().getItReturnRow().get(PropertiesInternos.LINEA_0).getCodigo().equals(PropertiesExternos.CODIGO_ESTANDAR_ERROR)){
                response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDF_1_CODIGO);        
                response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDF_1_MENSAJE);
            } 
            logger.info(traId + "PARAMETROS DE SALIDA:" + EaiUtil.getXmlTextFromJaxB(response)); 
          
        } catch(Exception e){
              logger.error(traId + "Error: " + e.getMessage(), e);          
              if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == (Integer.parseInt(PropertiesInternos.VALOR_MENOS_UNO))) {
                  response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDT_1_CODIGO);        
                  response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDT_1_MENSAJE + ": " + (PropertiesInternos.SP_OBTENER_NRO_TELEFPRE));
              }
              else {
                  response.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDT_2_CODIGO);        
                  response.setMensajeResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDT_2_MENSAJE);
              }          
              logger.error(traId + EaiUtil.getXmlTextFromJaxB(response));            
              
        } finally {
            this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
            logger.info(traId + "======== Fin del metodo obtenerNroTelefPre");
        }
        return response;
    }
    
  //*****************************************************************************************************
      // METODO : listarLineasPrereservadas
      //          Libera las l�neas pre-resevadas (no seleccionadas) por la transacci�n y les actualiza el 
      //          estado a disponibles y la l�nea que seleccion� pasa de estado pre-reservada a reservada.
      //    
      //*****************************************************************************************************
    public ListarLineasPrereservadasResponse listarLineasPrereservadas(ListarLineasPrereservadasRequest parametrosIn) {
        String traId = parametrosIn.getIdTransaccion() + "[listarLineasPrereservadas]";
              logger.info(traId + "======== Inicio del metodo listarLineasPrereservadas==========");
              logger.info(traId + "Se procede a ejecutar " + PropertiesInternos.SP_LISTAR_LINEAS_PRERESERVADAS + " en la DBSANS");
           
              MapSqlParameterSource objParametrosIN = null;
              JdbcTemplate objJdbcTemplate = null;
              SimpleJdbcCall objJdbcCall = null;
              
            ListarLineasPrereservadasResponse response = new  ListarLineasPrereservadasResponse();
            response.setIdTransaccion(parametrosIn.getIdTransaccion());
            response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
            response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);
          
            
          try {
               eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
              
               objParametrosIN = new MapSqlParameterSource();
               objParametrosIN.addValue("PI_CANT_LINEA",new BigInteger(parametrosIn.getCantidadLineas()).intValue(),OracleTypes.NUMBER);
              
               logger.info(traId + "Se procede a procesar conexion en SANS: " + PropertiesExternos.DB_EAI_JNDI);
              
               objJdbcCall = new SimpleJdbcCall(eaiDS);
              
               objJdbcCall.withoutProcedureColumnMetaDataAccess();
          
               objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
            
                objJdbcCall.withProcedureName(PropertiesInternos.SP_LISTAR_LINEAS_PRERESERVADAS);
           
               SqlParameter[] parametros = new SqlParameter[4];
                 parametros[0]  = new SqlParameter("PI_CANT_LINEA", OracleTypes.NUMBER); 
                 parametros[1] = new SqlOutParameter("PO_CODIGO_RESPUESTA", OracleTypes.NUMBER);
                 parametros[2] = new SqlOutParameter("PO_MENSAJE_RESPUESTA", OracleTypes.VARCHAR);
                 parametros[3] = new SqlOutParameter("PO_CURSOR", oracle.jdbc.OracleTypes.CURSOR, null, new ReturnOptionalRefCursorItNrotel(traId));
                 objJdbcCall.declareParameters(parametros);
                 objJdbcTemplate = objJdbcCall.getJdbcTemplate(); 
                 logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
                 objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
                 logger.info(traId + "Se obtuvo conexion con exito");
                 logger.info(traId + "PI_CANT_LINEA: "+parametrosIn.getCantidadLineas());
                 Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
                   logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_LISTAR_LINEAS_PRERESERVADAS);
                 
                      String codigoRespuesta = (objResultado.get("PO_CODIGO_RESPUESTA")!=null)?objResultado.get("PO_CODIGO_RESPUESTA").toString():null;
                      String mensajeRespuesta = (objResultado.get("PO_MENSAJE_RESPUESTA").toString());




                      if(codigoRespuesta==PropertiesExternos.CODIGO_ESTANDAR_EXITO)
                      {
    
                       response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);
                      }
                      else
                      if(codigoRespuesta==PropertiesExternos.CODIGO_ESTANDAR_ERROR)
                      {
      
                         response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
                      } 
                         response.setCodigoResultado(codigoRespuesta);
                   
              
              Iterator it = ((ArrayList<ItNroTelType>)objResultado.get("PO_CURSOR")).iterator();    
              ListarLineasPrereservadasResponse.ItNroTel itNroTelType = new  ListarLineasPrereservadasResponse.ItNroTel();
              logger.info(traId +"PARAMETROS DE SALIDA");
               int i=0;  
                   while (it.hasNext()) {
                     itNroTelType.getItNroTelRow().add((ItNroTelType) it.next()); 
                     logger.info(traId + itNroTelType.getItNroTelRow().get(i).getNroTelef()); 
                     i++;
                   }
                     response.setItNroTel(itNroTelType);
                    
          } catch (Exception e)
          {
               logger.error(traId + "Error: " + e.getMessage(), e);          
                       if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) 
                          {
                           response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
                             response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_LISTAR_LINEAS_PRERESERVADAS);
                          }
                     else {
                           response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
                           response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
                          }    
          }
            return response;
        }


  //*****************************************************************************************************
      // METODO : reservaOnline
      //         Libera las l�neas pre-resevadas (no seleccionadas) por la transacci�n y 
      //         les actualiza el estado a disponibles y la l�nea que seleccion� pasa de estado 
      //         pre-reservada a reservada.
      //    
      //*****************************************************************************************************  
    public ReservaOnlineResponse reservaonline(ReservaOnlineRequest parametrosIn) {
        String traId = parametrosIn.getIdTransaccion() + "[reservaonline]-";
             logger.info(traId + "======== Inicio del metodo reservaonline");
             logger.info(traId + "Se procede a ejecutar SP en la SANS");
               
             MapSqlParameterSource objParametrosIN = null;
             JdbcTemplate objJdbcTemplate = null;
             SimpleJdbcCall objJdbcCall = null;  
              
             ReservaOnlineResponse response = new ReservaOnlineResponse();
             response.setIdTransaccion(parametrosIn.getIdTransaccion());
             response.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_EXITO);        
             response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO); 
             try {
                     eaiDS.setLoginTimeout(PropertiesExternos.DB_EAI_TIMEOUT_SECONDS);
                     objParametrosIN = new MapSqlParameterSource();
                     objParametrosIN.addValue("PI_LINEA", parametrosIn.getLinea(), OracleTypes.VARCHAR);
                     objParametrosIN.addValue("PI_ESTADO", parametrosIn.getEstado(), OracleTypes.VARCHAR);
                     objParametrosIN.addValue("PI_USUARIO", parametrosIn.getUsuario(), OracleTypes.VARCHAR);
               
                     logger.info(traId + "Se procede a procesar conexion en SANS: " + PropertiesExternos.DB_EAI_JNDI);
                     objJdbcCall = new SimpleJdbcCall(eaiDS);
                     objJdbcCall.withoutProcedureColumnMetaDataAccess();
                     objJdbcCall.withSchemaName(PropertiesExternos.DB_EAI_OWNER);
                     objJdbcCall.withProcedureName(PropertiesInternos.SP_RESERVA_ONLINE);

                     SqlParameter[] parametros = new SqlParameter[5];
                     parametros[0] = new SqlParameter("PI_LINEA", OracleTypes.VARCHAR);        
                     parametros[1] = new SqlParameter("PI_ESTADO", OracleTypes.VARCHAR);        
                     parametros[2] = new SqlParameter("PI_USUARIO", OracleTypes.VARCHAR);        
                     parametros[3] = new SqlOutParameter("PO_COD_RESPUESTA", OracleTypes.NUMBER);
                     parametros[4] = new SqlOutParameter("PO_MENSAJE_RESPUESTA", OracleTypes.VARCHAR);
                     objJdbcCall.declareParameters(parametros);

                     objJdbcTemplate = objJdbcCall.getJdbcTemplate();
                     objJdbcTemplate.setQueryTimeout(timeoutExecutionDb);
                     logger.info(traId + "Se obtuvo conexion con exito");
                     Map<String, Object> objResultado = objJdbcCall.execute(objParametrosIN);
                     logger.info(traId + "Se procede a ejecutar Store: " + PropertiesExternos.DB_EAI_OWNER + "." + PropertiesInternos.SP_RESERVA_ONLINE);
                     logger.info(traId + "PARAMETROS DE ENTRADA SP: " );
                     logger.info(traId + "PI_LINEA: "+parametrosIn.getLinea());
                     logger.info(traId + "PI_ESTADO: "+ parametrosIn.getEstado());
                     logger.info(traId + "PI_USUARIO: "+parametrosIn.getUsuario());

                   String codigoRespuesta = (objResultado.get("PO_COD_RESPUESTA")!=null)?objResultado.get("PO_COD_RESPUESTA").toString():null;
                 
                   String mensajeRespuesta = (objResultado.get("PO_MENSAJE_RESPUESTA").toString());
                  
                     
                                     if(codigoRespuesta.equals(PropertiesExternos.CODIGO_ESTANDAR_EXITO))
                                      
                                    {
                                      
                                     response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_EXITO);
                                  
                                    }
                                    else
                                    if(codigoRespuesta.equals(PropertiesExternos.CODIGO_ESTANDAR_ERROR))
                                    {
                                      
                                       response.setMensajeResultado(PropertiesExternos.MENSAJE_ESTANDAR_ERROR);
                                  
                                    } 
                                       response.setCodigoResultado(codigoRespuesta);
                   logger.info(traId +"PARAMETROS DE SALIDA");
                   logger.info(traId+response.getCodigoResultado());
                   logger.info(traId+response.getMensajeResultado());
                
                                     

                 } catch(Exception e){
                       logger.error(traId + "Error: " + e.getMessage(), e);          
                       if (getStackTraceFromException(e).indexOf("java.sql.SQLTimeoutException") == -1) {
                           response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_NODISPONIBLE);        
                           response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_NODISPONIBLE + ": " + PropertiesInternos.SP_RESERVA_ONLINE);
                       }
                       else {
                           response.setCodigoResultado(PropertiesExternos.CODIGO_ERROR_TIMEOUT);        
                           response.setMensajeResultado(PropertiesExternos.MENSAJE_ERROR_TIMEOUT);
                       }          
                       
                 } 
                 return response;
      }
    protected class ReturnOptionalRefPostCursorTiOutput implements SqlReturnType {

        private String msgId;

        public ReturnOptionalRefPostCursorTiOutput(String msgId) {
            this.msgId = msgId;
        }

        private RowMapper rowMapper = new RowMapper<ItTelSerType>() {
            public ItTelSerType mapRow(ResultSet rs, int rowNum) throws SQLException {
                ItTelSerType itTelSerType = new ItTelSerType();
                itTelSerType.setNroTelef(rs.getString(1));
                itTelSerType.setNroSerie(rs.getString(2));
                return itTelSerType;
            }
        };

        public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
            ResultSet rs = null;
            Object retorno = new ArrayList();
            try {
                rs = (ResultSet)cs.getObject(ix);
                List lista = new ArrayList();
                int i=0;
                while(rs.next()){
                  lista.add(rowMapper.mapRow(rs, i));
                  i++;  
                }
                retorno = lista;
            }
            catch (Exception e) {
                    logger.error(msgId + "No se obtuvo datos del cursor",e);
                    retorno = new ArrayList();
            }
            finally {
                try {
                    if (rs != null) {
                        if (!rs.isClosed()) {
                            rs.close();
                        }
                    }
                }
                catch (SQLException e) {
                    logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
                }
            }

            return retorno;
        }
    }
    protected class ReturnOptionalRefPostCursorItReturn implements SqlReturnType {

        private String msgId;

        public ReturnOptionalRefPostCursorItReturn(String msgId) {
            this.msgId = msgId;
        }

        private RowMapper rowMapper = new RowMapper<ItReturnType>() {
            public ItReturnType mapRow(ResultSet rset, int rowNum) throws SQLException {
                ItReturnType itReturnRow = new ItReturnType();
                itReturnRow.setTipo(rset.getString(1));
                itReturnRow.setClaseMensaje(rset.getString(2));
                itReturnRow.setNumeroMensaje(rset.getString(3));
                itReturnRow.setMensaje(rset.getString(4));
                itReturnRow.setNumeroLog(rset.getString(5));
                itReturnRow.setNumeroConsecutivoInterno(rset.getString(6));
                itReturnRow.setMensajeV1(rset.getString(7));
                itReturnRow.setMensajeV2(rset.getString(8));
                itReturnRow.setMensajeV3(rset.getString(9));
                itReturnRow.setMensajeV4(rset.getString(10));
                itReturnRow.setParametro(rset.getString(11));
                itReturnRow.setLineas(BigInteger.valueOf(rset.getInt(12)));
                itReturnRow.setCampo(rset.getString(13));
                itReturnRow.setSistema(rset.getString(14));
                itReturnRow.setCodigo(rset.getString(15));
                return itReturnRow;
            }
        };

        public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
            ResultSet rs = null;
            Object retorno = new ArrayList();
            try {
                rs = (ResultSet)cs.getObject(ix);
                List lista = new ArrayList();

                for (int i = 0; rs.next(); i++) {
                    lista.add(rowMapper.mapRow(rs, i));
                }
                retorno = lista;
            }
            catch (Exception e) {
                    logger.error(msgId + "No se obtuvo datos del cursor",e);
                    retorno = new ArrayList();
            }
            finally {
                try {
                    if (rs != null) {
                        if (!rs.isClosed()) {
                            rs.close();
                        }
                    }
                }
                catch (SQLException e) {
                    logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
                }
            }

            return retorno;
        }
    }
    
    protected class ReturnOptionalRefPreCursorTiOutput implements SqlReturnType {

        private String msgId;

        public ReturnOptionalRefPreCursorTiOutput(String msgId) {
            this.msgId = msgId;
        }

        private RowMapper rowMapper = new RowMapper<ItTelSerType>() {
            public ItTelSerType mapRow(ResultSet rs, int rowNum) throws SQLException {
                ItTelSerType itTelSerType = new ItTelSerType();
                itTelSerType.setNroTelef(rs.getString(1));
                itTelSerType.setNroSerie(rs.getString(2));
                return itTelSerType;
            }
        };

        public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
            ResultSet rs = null;
            Object retorno = new ArrayList();
            try {
                rs = (ResultSet)cs.getObject(ix);
                List lista = new ArrayList();
                int i=0;
                while(rs.next()){
                  lista.add(rowMapper.mapRow(rs, i));
                  i++;  
                }
                retorno = lista;
            }
            catch (Exception e) {
                    logger.error(msgId + "No se obtuvo datos del cursor",e);
                    retorno = new ArrayList();
            }
            finally {
                try {
                    if (rs != null) {
                        if (!rs.isClosed()) {
                            rs.close();
                        }
                    }
                }
                catch (SQLException e) {
                    logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
                }
            }

            return retorno;
        }
    }
      
    protected class ReturnOptionalRefPreCursorItReturn implements SqlReturnType {

        private String msgId;

        public ReturnOptionalRefPreCursorItReturn(String msgId) {
            this.msgId = msgId;
        }

        private RowMapper rowMapper = new RowMapper<ItReturnType>() {
            public ItReturnType mapRow(ResultSet rset, int rowNum) throws SQLException {
                ItReturnType itReturnRow = new ItReturnType();
                itReturnRow.setTipo(rset.getString(1));
                itReturnRow.setClaseMensaje(rset.getString(2));
                itReturnRow.setNumeroMensaje(rset.getString(3));
                itReturnRow.setMensaje(rset.getString(4));
                itReturnRow.setNumeroLog(rset.getString(5));
                itReturnRow.setNumeroConsecutivoInterno(rset.getString(6));
                itReturnRow.setMensajeV1(rset.getString(7));
                itReturnRow.setMensajeV2(rset.getString(8));
                itReturnRow.setMensajeV3(rset.getString(9));
                itReturnRow.setMensajeV4(rset.getString(10));
                itReturnRow.setParametro(rset.getString(11));
                itReturnRow.setLineas(BigInteger.valueOf(rset.getInt(12)));
                itReturnRow.setCampo(rset.getString(13));
                itReturnRow.setSistema(rset.getString(14));
                itReturnRow.setCodigo(rset.getString(15));
                return itReturnRow;
            }
        };

        public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
            ResultSet rs = null;
            Object retorno = new ArrayList();
            try {
                rs = (ResultSet)cs.getObject(ix);
                List lista = new ArrayList();

                for (int i = 0; rs.next(); i++) {
                    lista.add(rowMapper.mapRow(rs, i));
                }
                retorno = lista;
            }
            catch (Exception e) {
                    logger.error(msgId + "No se obtuvo datos del cursor",e);
                    retorno = new ArrayList();
            }
            finally {
                try {
                    if (rs != null) {
                        if (!rs.isClosed()) {
                            rs.close();
                        }
                    }
                }
                catch (SQLException e) {
                    logger.error(msgId+"getTypeValue. Error: " + e.getMessage(), e);
                }
            }

            return retorno;
        }
    }
    
  protected String getStackTraceFromException(Exception exception) {
      StringWriter stringWriter = new StringWriter();
      exception.printStackTrace(new PrintWriter(stringWriter, true));
      return stringWriter.toString();
  }
  private XMLGregorianCalendar asXMLGregorianCalendar(Date date,String traId) {
     GregorianCalendar gc = null;
     DatatypeFactory df = null;
       if (date == null) {
           return null;
       } else {
          try {
            df = DatatypeFactory.newInstance();
            gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
          } catch (Exception e) {
            logger.error(traId + "Se produjo un error al obtene fecha xml: " + e.getMessage());
          }
         return df.newXMLGregorianCalendar(gc);
       }
   }


}