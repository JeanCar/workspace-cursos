package pe.com.claro.eai.ws.sap.simcards.bean;

import java.io.Serializable;
import java.math.BigInteger;


public class  DesasociarBean implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String tipo;
    private String claseMensaje;
    private String numeroMensaje;
    private String mensaje;
    private String numeroLog;
    private String numeroConsecutivoInterno;
    private String mensajeV1;
    private String mensajeV2;
    private String mensajeV3;
    private String mensajeV4;
    private String parametro;
    private BigInteger lineas;
    private String campo;
    private String sistema;
    private String codigo;


    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setClaseMensaje(String claseMensaje) {
        this.claseMensaje = claseMensaje;
    }

    public String getClaseMensaje() {
        return claseMensaje;
    }

    public void setNumeroMensaje(String numeroMensaje) {
        this.numeroMensaje = numeroMensaje;
    }

    public String getNumeroMensaje() {
        return numeroMensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setNumeroLog(String numeroLog) {
        this.numeroLog = numeroLog;
    }

    public String getNumeroLog() {
        return numeroLog;
    }

    public void setNumeroConsecutivoInterno(String numeroConsecutivoInterno) {
        this.numeroConsecutivoInterno = numeroConsecutivoInterno;
    }

    public String getNumeroConsecutivoInterno() {
        return numeroConsecutivoInterno;
    }

    public void setMensajeV1(String mensajeV1) {
        this.mensajeV1 = mensajeV1;
    }

    public String getMensajeV1() {
        return mensajeV1;
    }

    public void setMensajeV2(String mensajeV2) {
        this.mensajeV2 = mensajeV2;
    }

    public String getMensajeV2() {
        return mensajeV2;
    }

    public void setMensajeV3(String mensajeV3) {
        this.mensajeV3 = mensajeV3;
    }

    public String getMensajeV3() {
        return mensajeV3;
    }

    public void setMensajeV4(String mensajeV4) {
        this.mensajeV4 = mensajeV4;
    }

    public String getMensajeV4() {
        return mensajeV4;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getParametro() {
        return parametro;
    }

    public void setLineas(BigInteger lineas) {
        this.lineas = lineas;
    }

    public BigInteger getLineas() {
        return lineas;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getCampo() {
        return campo;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getSistema() {
        return sistema;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
