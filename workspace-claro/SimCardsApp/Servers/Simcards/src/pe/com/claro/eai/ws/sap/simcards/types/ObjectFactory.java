
package pe.com.claro.eai.ws.sap.simcards.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.com.claro.eai.ws.sap.simcards.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.com.claro.eai.ws.sap.simcards.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReversionCambioNumeroResponse }
     * 
     */
    public ReversionCambioNumeroResponse createReversionCambioNumeroResponse() {
        return new ReversionCambioNumeroResponse();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPreResponse }
     * 
     */
    public ObtenerNroTelefPreResponse createObtenerNroTelefPreResponse() {
        return new ObtenerNroTelefPreResponse();
    }

    /**
     * Create an instance of {@link ConsultarStatusTelefonoResponse }
     * 
     */
    public ConsultarStatusTelefonoResponse createConsultarStatusTelefonoResponse() {
        return new ConsultarStatusTelefonoResponse();
    }

    /**
     * Create an instance of {@link EliminarNroTelefResponse }
     * 
     */
    public EliminarNroTelefResponse createEliminarNroTelefResponse() {
        return new EliminarNroTelefResponse();
    }

    /**
     * Create an instance of {@link ActualizarNroTelefResponse }
     * 
     */
    public ActualizarNroTelefResponse createActualizarNroTelefResponse() {
        return new ActualizarNroTelefResponse();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPreRequest }
     * 
     */
    public ObtenerNroTelefPreRequest createObtenerNroTelefPreRequest() {
        return new ObtenerNroTelefPreRequest();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPostResponse }
     * 
     */
    public ObtenerNroTelefPostResponse createObtenerNroTelefPostResponse() {
        return new ObtenerNroTelefPostResponse();
    }

    /**
     * Create an instance of {@link AsociarNroTelefSerieResponse }
     * 
     */
    public AsociarNroTelefSerieResponse createAsociarNroTelefSerieResponse() {
        return new AsociarNroTelefSerieResponse();
    }

    /**
     * Create an instance of {@link ActualizarSimcardPorReposicionResponse }
     * 
     */
    public ActualizarSimcardPorReposicionResponse createActualizarSimcardPorReposicionResponse() {
        return new ActualizarSimcardPorReposicionResponse();
    }

    /**
     * Create an instance of {@link CambiarStatusResponse }
     * 
     */
    public CambiarStatusResponse createCambiarStatusResponse() {
        return new CambiarStatusResponse();
    }

    /**
     * Create an instance of {@link DesasociarNroTelefSerieResponse }
     * 
     */
    public DesasociarNroTelefSerieResponse createDesasociarNroTelefSerieResponse() {
        return new DesasociarNroTelefSerieResponse();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefResponse }
     * 
     */
    public ObtenerNroTelefResponse createObtenerNroTelefResponse() {
        return new ObtenerNroTelefResponse();
    }

    /**
     * Create an instance of {@link ObtenerHLRResponse }
     * 
     */
    public ObtenerHLRResponse createObtenerHLRResponse() {
        return new ObtenerHLRResponse();
    }

    /**
     * Create an instance of {@link RollbackPelVendidoResponse }
     * 
     */
    public RollbackPelVendidoResponse createRollbackPelVendidoResponse() {
        return new RollbackPelVendidoResponse();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoTodosResponse }
     * 
     */
    public ConsultarTelefonoTodosResponse createConsultarTelefonoTodosResponse() {
        return new ConsultarTelefonoTodosResponse();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPostRequest }
     * 
     */
    public ObtenerNroTelefPostRequest createObtenerNroTelefPostRequest() {
        return new ObtenerNroTelefPostRequest();
    }

    /**
     * Create an instance of {@link CambiarStatusVendidoResponse }
     * 
     */
    public CambiarStatusVendidoResponse createCambiarStatusVendidoResponse() {
        return new CambiarStatusVendidoResponse();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefRequest }
     * 
     */
    public ObtenerNroTelefRequest createObtenerNroTelefRequest() {
        return new ObtenerNroTelefRequest();
    }

    /**
     * Create an instance of {@link RegistroIMSIPortabilidadResponse }
     * 
     */
    public RegistroIMSIPortabilidadResponse createRegistroIMSIPortabilidadResponse() {
        return new RegistroIMSIPortabilidadResponse();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefSerieResponse }
     * 
     */
    public ObtenerNroTelefSerieResponse createObtenerNroTelefSerieResponse() {
        return new ObtenerNroTelefSerieResponse();
    }

    /**
     * Create an instance of {@link ListarMSISDNreservaResponse }
     * 
     */
    public ListarMSISDNreservaResponse createListarMSISDNreservaResponse() {
        return new ListarMSISDNreservaResponse();
    }

    /**
     * Create an instance of {@link CambiarStatusTelefonoResponse }
     * 
     */
    public CambiarStatusTelefonoResponse createCambiarStatusTelefonoResponse() {
        return new CambiarStatusTelefonoResponse();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoDispResponse }
     * 
     */
    public ConsultarTelefonoDispResponse createConsultarTelefonoDispResponse() {
        return new ConsultarTelefonoDispResponse();
    }

    /**
     * Create an instance of {@link ListarLineasPrereservadasResponse }
     * 
     */
    public ListarLineasPrereservadasResponse createListarLineasPrereservadasResponse() {
        return new ListarLineasPrereservadasResponse();
    }

    /**
     * Create an instance of {@link ObtenerDatosNroTelefResponse }
     * 
     */
    public ObtenerDatosNroTelefResponse createObtenerDatosNroTelefResponse() {
        return new ObtenerDatosNroTelefResponse();
    }

    /**
     * Create an instance of {@link CambiarStatusTelefonoRequest }
     * 
     */
    public CambiarStatusTelefonoRequest createCambiarStatusTelefonoRequest() {
        return new CambiarStatusTelefonoRequest();
    }

    /**
     * Create an instance of {@link ReversionCambioNumeroResponse.ItReturn }
     * 
     */
    public ReversionCambioNumeroResponse.ItReturn createReversionCambioNumeroResponseItReturn() {
        return new ReversionCambioNumeroResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPreResponse.ItOutput }
     * 
     */
    public ObtenerNroTelefPreResponse.ItOutput createObtenerNroTelefPreResponseItOutput() {
        return new ObtenerNroTelefPreResponse.ItOutput();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPreResponse.ItReturn }
     * 
     */
    public ObtenerNroTelefPreResponse.ItReturn createObtenerNroTelefPreResponseItReturn() {
        return new ObtenerNroTelefPreResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ReservaOnlineResponse }
     * 
     */
    public ReservaOnlineResponse createReservaOnlineResponse() {
        return new ReservaOnlineResponse();
    }

    /**
     * Create an instance of {@link ConsultarStatusTelefonoResponse.GtOutTab }
     * 
     */
    public ConsultarStatusTelefonoResponse.GtOutTab createConsultarStatusTelefonoResponseGtOutTab() {
        return new ConsultarStatusTelefonoResponse.GtOutTab();
    }

    /**
     * Create an instance of {@link ConsultarStatusTelefonoResponse.ItReturn }
     * 
     */
    public ConsultarStatusTelefonoResponse.ItReturn createConsultarStatusTelefonoResponseItReturn() {
        return new ConsultarStatusTelefonoResponse.ItReturn();
    }

    /**
     * Create an instance of {@link EliminarNroTelefResponse.ItReturn }
     * 
     */
    public EliminarNroTelefResponse.ItReturn createEliminarNroTelefResponseItReturn() {
        return new EliminarNroTelefResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ActualizarNroTelefRequest }
     * 
     */
    public ActualizarNroTelefRequest createActualizarNroTelefRequest() {
        return new ActualizarNroTelefRequest();
    }

    /**
     * Create an instance of {@link ReversionCambioNumeroRequest }
     * 
     */
    public ReversionCambioNumeroRequest createReversionCambioNumeroRequest() {
        return new ReversionCambioNumeroRequest();
    }

    /**
     * Create an instance of {@link ActualizarNroTelefResponse.ItReturn }
     * 
     */
    public ActualizarNroTelefResponse.ItReturn createActualizarNroTelefResponseItReturn() {
        return new ActualizarNroTelefResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPreRequest.ItInput }
     * 
     */
    public ObtenerNroTelefPreRequest.ItInput createObtenerNroTelefPreRequestItInput() {
        return new ObtenerNroTelefPreRequest.ItInput();
    }

    /**
     * Create an instance of {@link ObtenerHLRRequest }
     * 
     */
    public ObtenerHLRRequest createObtenerHLRRequest() {
        return new ObtenerHLRRequest();
    }

    /**
     * Create an instance of {@link AsociarNroTelefSerieRequest }
     * 
     */
    public AsociarNroTelefSerieRequest createAsociarNroTelefSerieRequest() {
        return new AsociarNroTelefSerieRequest();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPostResponse.ItOutput }
     * 
     */
    public ObtenerNroTelefPostResponse.ItOutput createObtenerNroTelefPostResponseItOutput() {
        return new ObtenerNroTelefPostResponse.ItOutput();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPostResponse.ItReturn }
     * 
     */
    public ObtenerNroTelefPostResponse.ItReturn createObtenerNroTelefPostResponseItReturn() {
        return new ObtenerNroTelefPostResponse.ItReturn();
    }

    /**
     * Create an instance of {@link AsociarNroTelefSerieResponse.ItOutput }
     * 
     */
    public AsociarNroTelefSerieResponse.ItOutput createAsociarNroTelefSerieResponseItOutput() {
        return new AsociarNroTelefSerieResponse.ItOutput();
    }

    /**
     * Create an instance of {@link AsociarNroTelefSerieResponse.ItReturn }
     * 
     */
    public AsociarNroTelefSerieResponse.ItReturn createAsociarNroTelefSerieResponseItReturn() {
        return new AsociarNroTelefSerieResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ActualizarSimcardPorReposicionResponse.ItReturn }
     * 
     */
    public ActualizarSimcardPorReposicionResponse.ItReturn createActualizarSimcardPorReposicionResponseItReturn() {
        return new ActualizarSimcardPorReposicionResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ListarLineasPrereservadasRequest }
     * 
     */
    public ListarLineasPrereservadasRequest createListarLineasPrereservadasRequest() {
        return new ListarLineasPrereservadasRequest();
    }

    /**
     * Create an instance of {@link CambiarStatusResponse.ItReturn }
     * 
     */
    public CambiarStatusResponse.ItReturn createCambiarStatusResponseItReturn() {
        return new CambiarStatusResponse.ItReturn();
    }

    /**
     * Create an instance of {@link DesasociarNroTelefSerieResponse.ItReturn }
     * 
     */
    public DesasociarNroTelefSerieResponse.ItReturn createDesasociarNroTelefSerieResponseItReturn() {
        return new DesasociarNroTelefSerieResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ReservaOnlineRequest }
     * 
     */
    public ReservaOnlineRequest createReservaOnlineRequest() {
        return new ReservaOnlineRequest();
    }

    /**
     * Create an instance of {@link EliminarNroTelefRequest }
     * 
     */
    public EliminarNroTelefRequest createEliminarNroTelefRequest() {
        return new EliminarNroTelefRequest();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefResponse.ItOutput }
     * 
     */
    public ObtenerNroTelefResponse.ItOutput createObtenerNroTelefResponseItOutput() {
        return new ObtenerNroTelefResponse.ItOutput();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefResponse.ItReturn }
     * 
     */
    public ObtenerNroTelefResponse.ItReturn createObtenerNroTelefResponseItReturn() {
        return new ObtenerNroTelefResponse.ItReturn();
    }

    /**
     * Create an instance of {@link CambiarStatusRequest }
     * 
     */
    public CambiarStatusRequest createCambiarStatusRequest() {
        return new CambiarStatusRequest();
    }

    /**
     * Create an instance of {@link ObtenerDatosNroTelefRequest }
     * 
     */
    public ObtenerDatosNroTelefRequest createObtenerDatosNroTelefRequest() {
        return new ObtenerDatosNroTelefRequest();
    }

    /**
     * Create an instance of {@link ObtenerHLRResponse.ItReturn }
     * 
     */
    public ObtenerHLRResponse.ItReturn createObtenerHLRResponseItReturn() {
        return new ObtenerHLRResponse.ItReturn();
    }

    /**
     * Create an instance of {@link RollbackPelVendidoResponse.ItReturn }
     * 
     */
    public RollbackPelVendidoResponse.ItReturn createRollbackPelVendidoResponseItReturn() {
        return new RollbackPelVendidoResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoTodosResponse.ItNumTelef }
     * 
     */
    public ConsultarTelefonoTodosResponse.ItNumTelef createConsultarTelefonoTodosResponseItNumTelef() {
        return new ConsultarTelefonoTodosResponse.ItNumTelef();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoTodosResponse.ItReturn }
     * 
     */
    public ConsultarTelefonoTodosResponse.ItReturn createConsultarTelefonoTodosResponseItReturn() {
        return new ConsultarTelefonoTodosResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefPostRequest.ItInput }
     * 
     */
    public ObtenerNroTelefPostRequest.ItInput createObtenerNroTelefPostRequestItInput() {
        return new ObtenerNroTelefPostRequest.ItInput();
    }

    /**
     * Create an instance of {@link CambiarStatusVendidoResponse.ItReturn }
     * 
     */
    public CambiarStatusVendidoResponse.ItReturn createCambiarStatusVendidoResponseItReturn() {
        return new CambiarStatusVendidoResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefRequest.ItInput }
     * 
     */
    public ObtenerNroTelefRequest.ItInput createObtenerNroTelefRequestItInput() {
        return new ObtenerNroTelefRequest.ItInput();
    }

    /**
     * Create an instance of {@link ConsultarStatusTelefonoRequest }
     * 
     */
    public ConsultarStatusTelefonoRequest createConsultarStatusTelefonoRequest() {
        return new ConsultarStatusTelefonoRequest();
    }

    /**
     * Create an instance of {@link DesasociarNroTelefSerieRequest }
     * 
     */
    public DesasociarNroTelefSerieRequest createDesasociarNroTelefSerieRequest() {
        return new DesasociarNroTelefSerieRequest();
    }

    /**
     * Create an instance of {@link CambiarStatusVendidoRequest }
     * 
     */
    public CambiarStatusVendidoRequest createCambiarStatusVendidoRequest() {
        return new CambiarStatusVendidoRequest();
    }

    /**
     * Create an instance of {@link RegistroIMSIPortabilidadResponse.ItReturn }
     * 
     */
    public RegistroIMSIPortabilidadResponse.ItReturn createRegistroIMSIPortabilidadResponseItReturn() {
        return new RegistroIMSIPortabilidadResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefSerieResponse.ItReturn }
     * 
     */
    public ObtenerNroTelefSerieResponse.ItReturn createObtenerNroTelefSerieResponseItReturn() {
        return new ObtenerNroTelefSerieResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ListarMSISDNreservaResponse.ItReturn }
     * 
     */
    public ListarMSISDNreservaResponse.ItReturn createListarMSISDNreservaResponseItReturn() {
        return new ListarMSISDNreservaResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ListarMSISDNreservaResponse.ItWork }
     * 
     */
    public ListarMSISDNreservaResponse.ItWork createListarMSISDNreservaResponseItWork() {
        return new ListarMSISDNreservaResponse.ItWork();
    }

    /**
     * Create an instance of {@link ListarMSISDNreservaResponse.ItNroTel }
     * 
     */
    public ListarMSISDNreservaResponse.ItNroTel createListarMSISDNreservaResponseItNroTel() {
        return new ListarMSISDNreservaResponse.ItNroTel();
    }

    /**
     * Create an instance of {@link CambiarStatusTelefonoResponse.EsResumen }
     * 
     */
    public CambiarStatusTelefonoResponse.EsResumen createCambiarStatusTelefonoResponseEsResumen() {
        return new CambiarStatusTelefonoResponse.EsResumen();
    }

    /**
     * Create an instance of {@link CambiarStatusTelefonoResponse.EtDetalle }
     * 
     */
    public CambiarStatusTelefonoResponse.EtDetalle createCambiarStatusTelefonoResponseEtDetalle() {
        return new CambiarStatusTelefonoResponse.EtDetalle();
    }

    /**
     * Create an instance of {@link CambiarStatusTelefonoResponse.ItReturn }
     * 
     */
    public CambiarStatusTelefonoResponse.ItReturn createCambiarStatusTelefonoResponseItReturn() {
        return new CambiarStatusTelefonoResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoTodosRequest }
     * 
     */
    public ConsultarTelefonoTodosRequest createConsultarTelefonoTodosRequest() {
        return new ConsultarTelefonoTodosRequest();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoDispResponse.ItData }
     * 
     */
    public ConsultarTelefonoDispResponse.ItData createConsultarTelefonoDispResponseItData() {
        return new ConsultarTelefonoDispResponse.ItData();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoDispResponse.ItReturn }
     * 
     */
    public ConsultarTelefonoDispResponse.ItReturn createConsultarTelefonoDispResponseItReturn() {
        return new ConsultarTelefonoDispResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ObtenerNroTelefSerieRequest }
     * 
     */
    public ObtenerNroTelefSerieRequest createObtenerNroTelefSerieRequest() {
        return new ObtenerNroTelefSerieRequest();
    }

    /**
     * Create an instance of {@link RegistroIMSIPortabilidadRequest }
     * 
     */
    public RegistroIMSIPortabilidadRequest createRegistroIMSIPortabilidadRequest() {
        return new RegistroIMSIPortabilidadRequest();
    }

    /**
     * Create an instance of {@link RollbackPelVendidoRequest }
     * 
     */
    public RollbackPelVendidoRequest createRollbackPelVendidoRequest() {
        return new RollbackPelVendidoRequest();
    }

    /**
     * Create an instance of {@link ListarLineasPrereservadasResponse.ItNroTel }
     * 
     */
    public ListarLineasPrereservadasResponse.ItNroTel createListarLineasPrereservadasResponseItNroTel() {
        return new ListarLineasPrereservadasResponse.ItNroTel();
    }

    /**
     * Create an instance of {@link ListarMSISDNreservaRequest }
     * 
     */
    public ListarMSISDNreservaRequest createListarMSISDNreservaRequest() {
        return new ListarMSISDNreservaRequest();
    }

    /**
     * Create an instance of {@link ConsultarTelefonoDispRequest }
     * 
     */
    public ConsultarTelefonoDispRequest createConsultarTelefonoDispRequest() {
        return new ConsultarTelefonoDispRequest();
    }

    /**
     * Create an instance of {@link ObtenerDatosNroTelefResponse.NroSimcardsData }
     * 
     */
    public ObtenerDatosNroTelefResponse.NroSimcardsData createObtenerDatosNroTelefResponseNroSimcardsData() {
        return new ObtenerDatosNroTelefResponse.NroSimcardsData();
    }

    /**
     * Create an instance of {@link ObtenerDatosNroTelefResponse.ItReturn }
     * 
     */
    public ObtenerDatosNroTelefResponse.ItReturn createObtenerDatosNroTelefResponseItReturn() {
        return new ObtenerDatosNroTelefResponse.ItReturn();
    }

    /**
     * Create an instance of {@link ActualizarSimcardPorReposicionRequest }
     * 
     */
    public ActualizarSimcardPorReposicionRequest createActualizarSimcardPorReposicionRequest() {
        return new ActualizarSimcardPorReposicionRequest();
    }

    /**
     * Create an instance of {@link ItReturnType }
     * 
     */
    public ItReturnType createItReturnType() {
        return new ItReturnType();
    }

    /**
     * Create an instance of {@link EsResumenType }
     * 
     */
    public EsResumenType createEsResumenType() {
        return new EsResumenType();
    }

    /**
     * Create an instance of {@link GtOutTabType }
     * 
     */
    public GtOutTabType createGtOutTabType() {
        return new GtOutTabType();
    }

    /**
     * Create an instance of {@link ItDataType }
     * 
     */
    public ItDataType createItDataType() {
        return new ItDataType();
    }

    /**
     * Create an instance of {@link EtDetalleType }
     * 
     */
    public EtDetalleType createEtDetalleType() {
        return new EtDetalleType();
    }

    /**
     * Create an instance of {@link ItNumTelefType }
     * 
     */
    public ItNumTelefType createItNumTelefType() {
        return new ItNumTelefType();
    }

    /**
     * Create an instance of {@link ItTelSerType }
     * 
     */
    public ItTelSerType createItTelSerType() {
        return new ItTelSerType();
    }

    /**
     * Create an instance of {@link ItMatSerType }
     * 
     */
    public ItMatSerType createItMatSerType() {
        return new ItMatSerType();
    }

    /**
     * Create an instance of {@link NroSimcardsDataType }
     * 
     */
    public NroSimcardsDataType createNroSimcardsDataType() {
        return new NroSimcardsDataType();
    }

    /**
     * Create an instance of {@link ItNroTelType }
     * 
     */
    public ItNroTelType createItNroTelType() {
        return new ItNroTelType();
    }

    /**
     * Create an instance of {@link ItWorkType }
     * 
     */
    public ItWorkType createItWorkType() {
        return new ItWorkType();
    }

}
