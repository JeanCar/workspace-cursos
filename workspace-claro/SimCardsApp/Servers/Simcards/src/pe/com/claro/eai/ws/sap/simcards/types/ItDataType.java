
package pe.com.claro.eai.ws.sap.simcards.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for itDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="itDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroTelef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoHlr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descClred" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descRegio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descTpcli" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descTptlf" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itDataType", propOrder = {
    "nroTelef",
    "codigoHlr",
    "descClred",
    "descRegio",
    "descTpcli",
    "descTptlf"
})
public class ItDataType {

    @XmlElement(required = true)
    protected String nroTelef;
    @XmlElement(required = true)
    protected String codigoHlr;
    @XmlElement(required = true)
    protected String descClred;
    @XmlElement(required = true)
    protected String descRegio;
    @XmlElement(required = true)
    protected String descTpcli;
    @XmlElement(required = true)
    protected String descTptlf;

    /**
     * Gets the value of the nroTelef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroTelef() {
        return nroTelef;
    }

    /**
     * Sets the value of the nroTelef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroTelef(String value) {
        this.nroTelef = value;
    }

    /**
     * Gets the value of the codigoHlr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoHlr() {
        return codigoHlr;
    }

    /**
     * Sets the value of the codigoHlr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoHlr(String value) {
        this.codigoHlr = value;
    }

    /**
     * Gets the value of the descClred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescClred() {
        return descClred;
    }

    /**
     * Sets the value of the descClred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescClred(String value) {
        this.descClred = value;
    }

    /**
     * Gets the value of the descRegio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRegio() {
        return descRegio;
    }

    /**
     * Sets the value of the descRegio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRegio(String value) {
        this.descRegio = value;
    }

    /**
     * Gets the value of the descTpcli property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTpcli() {
        return descTpcli;
    }

    /**
     * Sets the value of the descTpcli property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTpcli(String value) {
        this.descTpcli = value;
    }

    /**
     * Gets the value of the descTptlf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTptlf() {
        return descTptlf;
    }

    /**
     * Sets the value of the descTptlf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTptlf(String value) {
        this.descTptlf = value;
    }

}
