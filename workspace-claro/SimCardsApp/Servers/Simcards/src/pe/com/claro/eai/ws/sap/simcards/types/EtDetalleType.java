
package pe.com.claro.eai.ws.sap.simcards.types;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for etDetalleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="etDetalleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="correl" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="rangoInicial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rangoFinal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="inC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoHlr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="regionDes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "etDetalleType", propOrder = {
    "correl",
    "rangoInicial",
    "rangoFinal",
    "cantidad",
    "region",
    "inC",
    "codigoHlr",
    "regionDes"
})
public class EtDetalleType {

    @XmlElement(required = true)
    protected BigInteger correl;
    @XmlElement(required = true)
    protected String rangoInicial;
    @XmlElement(required = true)
    protected String rangoFinal;
    @XmlElement(required = true)
    protected BigInteger cantidad;
    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected String inC;
    @XmlElement(required = true)
    protected String codigoHlr;
    @XmlElement(required = true)
    protected String regionDes;

    /**
     * Gets the value of the correl property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCorrel() {
        return correl;
    }

    /**
     * Sets the value of the correl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCorrel(BigInteger value) {
        this.correl = value;
    }

    /**
     * Gets the value of the rangoInicial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRangoInicial() {
        return rangoInicial;
    }

    /**
     * Sets the value of the rangoInicial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRangoInicial(String value) {
        this.rangoInicial = value;
    }

    /**
     * Gets the value of the rangoFinal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRangoFinal() {
        return rangoFinal;
    }

    /**
     * Sets the value of the rangoFinal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRangoFinal(String value) {
        this.rangoFinal = value;
    }

    /**
     * Gets the value of the cantidad property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCantidad() {
        return cantidad;
    }

    /**
     * Sets the value of the cantidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCantidad(BigInteger value) {
        this.cantidad = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the inC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInC() {
        return inC;
    }

    /**
     * Sets the value of the inC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInC(String value) {
        this.inC = value;
    }

    /**
     * Gets the value of the codigoHlr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoHlr() {
        return codigoHlr;
    }

    /**
     * Sets the value of the codigoHlr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoHlr(String value) {
        this.codigoHlr = value;
    }

    /**
     * Gets the value of the regionDes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionDes() {
        return regionDes;
    }

    /**
     * Sets the value of the regionDes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionDes(String value) {
        this.regionDes = value;
    }

}
