
package pe.com.claro.eai.ws.sap.simcards.types;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for esResumenType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="esResumenType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantSol" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="cantCarg" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="codigoHlr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="regionDes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "esResumenType", propOrder = {
    "cantSol",
    "cantCarg",
    "codigoHlr",
    "region",
    "regionDes"
})
public class EsResumenType {

    @XmlElement(required = true)
    protected BigInteger cantSol;
    @XmlElement(required = true)
    protected BigInteger cantCarg;
    @XmlElement(required = true)
    protected String codigoHlr;
    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected String regionDes;

    /**
     * Gets the value of the cantSol property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCantSol() {
        return cantSol;
    }

    /**
     * Sets the value of the cantSol property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCantSol(BigInteger value) {
        this.cantSol = value;
    }

    /**
     * Gets the value of the cantCarg property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCantCarg() {
        return cantCarg;
    }

    /**
     * Sets the value of the cantCarg property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCantCarg(BigInteger value) {
        this.cantCarg = value;
    }

    /**
     * Gets the value of the codigoHlr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoHlr() {
        return codigoHlr;
    }

    /**
     * Sets the value of the codigoHlr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoHlr(String value) {
        this.codigoHlr = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the regionDes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionDes() {
        return regionDes;
    }

    /**
     * Sets the value of the regionDes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionDes(String value) {
        this.regionDes = value;
    }

}
