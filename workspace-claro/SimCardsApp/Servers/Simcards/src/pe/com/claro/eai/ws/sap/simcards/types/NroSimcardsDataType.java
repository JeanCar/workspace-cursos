
package pe.com.claro.eai.ws.sap.simcards.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for nroSimcardsDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nroSimcardsDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroTelef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoRed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoHlr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecCarga" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="usoCarga" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoNro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="loteBscs" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoDest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="loteDest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="loteMkt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serNr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="matNr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="kunNr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="matNrAntig" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecTrafico" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="bm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusChipRep" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecCambio" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nroSimcardsDataType", propOrder = {
    "nroTelef",
    "region",
    "codigoRed",
    "codigoHlr",
    "status",
    "fecCarga",
    "usoCarga",
    "tipoNro",
    "loteBscs",
    "tipoCliente",
    "codigoDest",
    "loteDest",
    "loteMkt",
    "serNr",
    "matNr",
    "kunNr",
    "matNrAntig",
    "fecTrafico",
    "bm",
    "tipoDocCliente",
    "cliente",
    "statusChipRep",
    "fecCambio"
})
public class NroSimcardsDataType {

    @XmlElement(required = true)
    protected String nroTelef;
    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected String codigoRed;
    @XmlElement(required = true)
    protected String codigoHlr;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fecCarga;
    @XmlElement(required = true)
    protected String usoCarga;
    @XmlElement(required = true)
    protected String tipoNro;
    @XmlElement(required = true)
    protected String loteBscs;
    @XmlElement(required = true)
    protected String tipoCliente;
    @XmlElement(required = true)
    protected String codigoDest;
    @XmlElement(required = true)
    protected String loteDest;
    @XmlElement(required = true)
    protected String loteMkt;
    @XmlElement(required = true)
    protected String serNr;
    @XmlElement(required = true)
    protected String matNr;
    @XmlElement(required = true)
    protected String kunNr;
    @XmlElement(required = true)
    protected String matNrAntig;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fecTrafico;
    @XmlElement(required = true)
    protected String bm;
    @XmlElement(required = true)
    protected String tipoDocCliente;
    @XmlElement(required = true)
    protected String cliente;
    @XmlElement(required = true)
    protected String statusChipRep;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fecCambio;

    /**
     * Gets the value of the nroTelef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroTelef() {
        return nroTelef;
    }

    /**
     * Sets the value of the nroTelef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroTelef(String value) {
        this.nroTelef = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the codigoRed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRed() {
        return codigoRed;
    }

    /**
     * Sets the value of the codigoRed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRed(String value) {
        this.codigoRed = value;
    }

    /**
     * Gets the value of the codigoHlr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoHlr() {
        return codigoHlr;
    }

    /**
     * Sets the value of the codigoHlr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoHlr(String value) {
        this.codigoHlr = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the fecCarga property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecCarga() {
        return fecCarga;
    }

    /**
     * Sets the value of the fecCarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecCarga(XMLGregorianCalendar value) {
        this.fecCarga = value;
    }

    /**
     * Gets the value of the usoCarga property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsoCarga() {
        return usoCarga;
    }

    /**
     * Sets the value of the usoCarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsoCarga(String value) {
        this.usoCarga = value;
    }

    /**
     * Gets the value of the tipoNro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoNro() {
        return tipoNro;
    }

    /**
     * Sets the value of the tipoNro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoNro(String value) {
        this.tipoNro = value;
    }

    /**
     * Gets the value of the loteBscs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoteBscs() {
        return loteBscs;
    }

    /**
     * Sets the value of the loteBscs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoteBscs(String value) {
        this.loteBscs = value;
    }

    /**
     * Gets the value of the tipoCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
     * Sets the value of the tipoCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCliente(String value) {
        this.tipoCliente = value;
    }

    /**
     * Gets the value of the codigoDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDest() {
        return codigoDest;
    }

    /**
     * Sets the value of the codigoDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDest(String value) {
        this.codigoDest = value;
    }

    /**
     * Gets the value of the loteDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoteDest() {
        return loteDest;
    }

    /**
     * Sets the value of the loteDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoteDest(String value) {
        this.loteDest = value;
    }

    /**
     * Gets the value of the loteMkt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoteMkt() {
        return loteMkt;
    }

    /**
     * Sets the value of the loteMkt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoteMkt(String value) {
        this.loteMkt = value;
    }

    /**
     * Gets the value of the serNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerNr() {
        return serNr;
    }

    /**
     * Sets the value of the serNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerNr(String value) {
        this.serNr = value;
    }

    /**
     * Gets the value of the matNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatNr() {
        return matNr;
    }

    /**
     * Sets the value of the matNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatNr(String value) {
        this.matNr = value;
    }

    /**
     * Gets the value of the kunNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKunNr() {
        return kunNr;
    }

    /**
     * Sets the value of the kunNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKunNr(String value) {
        this.kunNr = value;
    }

    /**
     * Gets the value of the matNrAntig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatNrAntig() {
        return matNrAntig;
    }

    /**
     * Sets the value of the matNrAntig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatNrAntig(String value) {
        this.matNrAntig = value;
    }

    /**
     * Gets the value of the fecTrafico property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecTrafico() {
        return fecTrafico;
    }

    /**
     * Sets the value of the fecTrafico property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecTrafico(XMLGregorianCalendar value) {
        this.fecTrafico = value;
    }

    /**
     * Gets the value of the bm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBm() {
        return bm;
    }

    /**
     * Sets the value of the bm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBm(String value) {
        this.bm = value;
    }

    /**
     * Gets the value of the tipoDocCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocCliente() {
        return tipoDocCliente;
    }

    /**
     * Sets the value of the tipoDocCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocCliente(String value) {
        this.tipoDocCliente = value;
    }

    /**
     * Gets the value of the cliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Sets the value of the cliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente(String value) {
        this.cliente = value;
    }

    /**
     * Gets the value of the statusChipRep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusChipRep() {
        return statusChipRep;
    }

    /**
     * Sets the value of the statusChipRep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusChipRep(String value) {
        this.statusChipRep = value;
    }

    /**
     * Gets the value of the fecCambio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecCambio() {
        return fecCambio;
    }

    /**
     * Sets the value of the fecCambio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecCambio(XMLGregorianCalendar value) {
        this.fecCambio = value;
    }

}
