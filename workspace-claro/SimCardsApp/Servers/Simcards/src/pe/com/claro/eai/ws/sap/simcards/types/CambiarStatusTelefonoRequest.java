
package pe.com.claro.eai.ws.sap.simcards.types;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTransaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ipAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuarioAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cantNro" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clasifRed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoHlr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusInicial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusFinal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idTransaccion",
    "ipAplicacion",
    "nombreAplicacion",
    "usuarioAplicacion",
    "cantNro",
    "region",
    "clasifRed",
    "codigoHlr",
    "tipoCliente",
    "statusInicial",
    "statusFinal",
    "usuario"
})
@XmlRootElement(name = "cambiarStatusTelefonoRequest")
public class CambiarStatusTelefonoRequest {

    @XmlElement(required = true)
    protected String idTransaccion;
    @XmlElement(required = true)
    protected String ipAplicacion;
    @XmlElement(required = true)
    protected String nombreAplicacion;
    @XmlElement(required = true)
    protected String usuarioAplicacion;
    @XmlElement(required = true)
    protected BigInteger cantNro;
    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected String clasifRed;
    @XmlElement(required = true)
    protected String codigoHlr;
    @XmlElement(required = true)
    protected String tipoCliente;
    @XmlElement(required = true)
    protected String statusInicial;
    @XmlElement(required = true)
    protected String statusFinal;
    @XmlElement(required = true)
    protected String usuario;

    /**
     * Gets the value of the idTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * Sets the value of the idTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTransaccion(String value) {
        this.idTransaccion = value;
    }

    /**
     * Gets the value of the ipAplicacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpAplicacion() {
        return ipAplicacion;
    }

    /**
     * Sets the value of the ipAplicacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpAplicacion(String value) {
        this.ipAplicacion = value;
    }

    /**
     * Gets the value of the nombreAplicacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAplicacion() {
        return nombreAplicacion;
    }

    /**
     * Sets the value of the nombreAplicacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAplicacion(String value) {
        this.nombreAplicacion = value;
    }

    /**
     * Gets the value of the usuarioAplicacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioAplicacion() {
        return usuarioAplicacion;
    }

    /**
     * Sets the value of the usuarioAplicacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioAplicacion(String value) {
        this.usuarioAplicacion = value;
    }

    /**
     * Gets the value of the cantNro property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCantNro() {
        return cantNro;
    }

    /**
     * Sets the value of the cantNro property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCantNro(BigInteger value) {
        this.cantNro = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the clasifRed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasifRed() {
        return clasifRed;
    }

    /**
     * Sets the value of the clasifRed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasifRed(String value) {
        this.clasifRed = value;
    }

    /**
     * Gets the value of the codigoHlr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoHlr() {
        return codigoHlr;
    }

    /**
     * Sets the value of the codigoHlr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoHlr(String value) {
        this.codigoHlr = value;
    }

    /**
     * Gets the value of the tipoCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
     * Sets the value of the tipoCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCliente(String value) {
        this.tipoCliente = value;
    }

    /**
     * Gets the value of the statusInicial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusInicial() {
        return statusInicial;
    }

    /**
     * Sets the value of the statusInicial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusInicial(String value) {
        this.statusInicial = value;
    }

    /**
     * Gets the value of the statusFinal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusFinal() {
        return statusFinal;
    }

    /**
     * Sets the value of the statusFinal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusFinal(String value) {
        this.statusFinal = value;
    }

    /**
     * Gets the value of the usuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Sets the value of the usuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

}
