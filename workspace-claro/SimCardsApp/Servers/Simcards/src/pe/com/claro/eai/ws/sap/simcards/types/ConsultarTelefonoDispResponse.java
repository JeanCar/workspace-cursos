
package pe.com.claro.eai.ws.sap.simcards.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTransaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="itData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="itDataRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itDataType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="itReturn">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="itReturnRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itReturnType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idTransaccion",
    "codigoResultado",
    "mensajeResultado",
    "itData",
    "itReturn"
})
@XmlRootElement(name = "consultarTelefonoDispResponse")
public class ConsultarTelefonoDispResponse {

    @XmlElement(required = true)
    protected String idTransaccion;
    @XmlElement(required = true)
    protected String codigoResultado;
    @XmlElement(required = true)
    protected String mensajeResultado;
    @XmlElement(required = true)
    protected ConsultarTelefonoDispResponse.ItData itData;
    @XmlElement(required = true)
    protected ConsultarTelefonoDispResponse.ItReturn itReturn;

    /**
     * Gets the value of the idTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * Sets the value of the idTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTransaccion(String value) {
        this.idTransaccion = value;
    }

    /**
     * Gets the value of the codigoResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResultado() {
        return codigoResultado;
    }

    /**
     * Sets the value of the codigoResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResultado(String value) {
        this.codigoResultado = value;
    }

    /**
     * Gets the value of the mensajeResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeResultado() {
        return mensajeResultado;
    }

    /**
     * Sets the value of the mensajeResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeResultado(String value) {
        this.mensajeResultado = value;
    }

    /**
     * Gets the value of the itData property.
     * 
     * @return
     *     possible object is
     *     {@link ConsultarTelefonoDispResponse.ItData }
     *     
     */
    public ConsultarTelefonoDispResponse.ItData getItData() {
        return itData;
    }

    /**
     * Sets the value of the itData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultarTelefonoDispResponse.ItData }
     *     
     */
    public void setItData(ConsultarTelefonoDispResponse.ItData value) {
        this.itData = value;
    }

    /**
     * Gets the value of the itReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ConsultarTelefonoDispResponse.ItReturn }
     *     
     */
    public ConsultarTelefonoDispResponse.ItReturn getItReturn() {
        return itReturn;
    }

    /**
     * Sets the value of the itReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultarTelefonoDispResponse.ItReturn }
     *     
     */
    public void setItReturn(ConsultarTelefonoDispResponse.ItReturn value) {
        this.itReturn = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="itDataRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itDataType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itDataRow"
    })
    public static class ItData {

        protected List<ItDataType> itDataRow;

        /**
         * Gets the value of the itDataRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the itDataRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItDataRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItDataType }
         * 
         * 
         */
        public List<ItDataType> getItDataRow() {
            if (itDataRow == null) {
                itDataRow = new ArrayList<ItDataType>();
            }
            return this.itDataRow;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="itReturnRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itReturnType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itReturnRow"
    })
    public static class ItReturn {

        protected List<ItReturnType> itReturnRow;

        /**
         * Gets the value of the itReturnRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the itReturnRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItReturnRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItReturnType }
         * 
         * 
         */
        public List<ItReturnType> getItReturnRow() {
            if (itReturnRow == null) {
                itReturnRow = new ArrayList<ItReturnType>();
            }
            return this.itReturnRow;
        }

    }

}
