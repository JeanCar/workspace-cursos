
package pe.com.claro.eai.ws.sap.simcards.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for itTelSerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="itTelSerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroTelef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nroSerie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itTelSerType", propOrder = {
    "nroTelef",
    "nroSerie"
})
public class ItTelSerType {

    @XmlElement(required = true)
    protected String nroTelef;
    @XmlElement(required = true)
    protected String nroSerie;

    /**
     * Gets the value of the nroTelef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroTelef() {
        return nroTelef;
    }

    /**
     * Sets the value of the nroTelef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroTelef(String value) {
        this.nroTelef = value;
    }

    /**
     * Gets the value of the nroSerie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroSerie() {
        return nroSerie;
    }

    /**
     * Sets the value of the nroSerie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroSerie(String value) {
        this.nroSerie = value;
    }

}
