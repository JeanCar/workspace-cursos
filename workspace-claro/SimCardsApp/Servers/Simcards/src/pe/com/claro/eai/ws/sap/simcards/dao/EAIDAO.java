package pe.com.claro.eai.ws.sap.simcards.dao;

import pe.com.claro.eai.ws.sap.simcards.bean.ResponseBean;
import pe.com.claro.eai.ws.sap.simcards.exception.DBException;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarSimcardPorReposicionRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ActualizarSimcardPorReposicionResponse;
import pe.com.claro.eai.ws.sap.simcards.types.AsociarNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.AsociarNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusTelefonoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusTelefonoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusVendidoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.CambiarStatusVendidoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarStatusTelefonoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarStatusTelefonoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoDispRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoDispResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoTodosRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ConsultarTelefonoTodosResponse;
import pe.com.claro.eai.ws.sap.simcards.types.DesasociarNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.EliminarNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.EliminarNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ListarMSISDNreservaRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ListarMSISDNreservaResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerDatosNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerDatosNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerHLRRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerHLRResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPostRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPostResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPreRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefPreResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefSerieRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ObtenerNroTelefSerieResponse;
import pe.com.claro.eai.ws.sap.simcards.types.RegistroIMSIPortabilidadRequest;
import pe.com.claro.eai.ws.sap.simcards.types.RegistroIMSIPortabilidadResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ReversionCambioNumeroRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ReversionCambioNumeroResponse;
import pe.com.claro.eai.ws.sap.simcards.types.RollbackPelVendidoRequest;
import pe.com.claro.eai.ws.sap.simcards.types.RollbackPelVendidoResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ListarLineasPrereservadasRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ListarLineasPrereservadasResponse;
import pe.com.claro.eai.ws.sap.simcards.types.ReservaOnlineRequest;
import pe.com.claro.eai.ws.sap.simcards.types.ReservaOnlineResponse;


public interface EAIDAO {
  public ConsultarTelefonoDispResponse consultarTelefonoDisp(ConsultarTelefonoDispRequest parametros);
  public ConsultarTelefonoTodosResponse consultarTelefonoTodos(ConsultarTelefonoTodosRequest parametros);
  public ConsultarStatusTelefonoResponse consultarStatusTelefono(ConsultarStatusTelefonoRequest parametrosIn);
  public CambiarStatusTelefonoResponse cambiarStatusTelefono(CambiarStatusTelefonoRequest parametrosIn);
  public CambiarStatusResponse cambiarStatus(CambiarStatusRequest parametrosIn);
  public ReversionCambioNumeroResponse reversionCambioNumero(ReversionCambioNumeroRequest parametrosIn);
  public RegistroIMSIPortabilidadResponse registroIMSIPortabilidad(RegistroIMSIPortabilidadRequest parametrosIn);
  public ActualizarNroTelefResponse actualizarNroTelef(ActualizarNroTelefRequest parametrosIn);
  public EliminarNroTelefResponse eliminarNroTelef(EliminarNroTelefRequest parametrosIn);
  public ListarMSISDNreservaResponse listarMSISDNreserva(ListarMSISDNreservaRequest parametrosIn);
  public RollbackPelVendidoResponse rollbackPelVendido(RollbackPelVendidoRequest parametrosIn);
  public CambiarStatusVendidoResponse cambiarStatusVendido(CambiarStatusVendidoRequest parametrosIn);
  public ObtenerDatosNroTelefResponse obtenerDatosNroTelef(ObtenerDatosNroTelefRequest parametrosIn);
  public ObtenerNroTelefSerieResponse obtenerNroTelefSerie(ObtenerNroTelefSerieRequest parametrosIn);
  public ObtenerNroTelefResponse obtenerNroTelef(ObtenerNroTelefRequest parametrosIn);
  public ObtenerHLRResponse obtenerHLR(ObtenerHLRRequest parametrosIn);
  public ActualizarSimcardPorReposicionResponse actualizarSimcardPorReposicion(ActualizarSimcardPorReposicionRequest request);
  public AsociarNroTelefSerieResponse asociarNroTelefSerie(AsociarNroTelefSerieRequest request);
  public ResponseBean realizarDesosiacion(DesasociarNroTelefSerieRequest request) throws DBException;
  public ObtenerNroTelefPostResponse obtenerNroTelefPost(ObtenerNroTelefPostRequest parametrosIn);
  public ObtenerNroTelefPreResponse obtenerNroTelefPre(ObtenerNroTelefPreRequest parametrosIn);
  public ListarLineasPrereservadasResponse listarLineasPrereservadas(ListarLineasPrereservadasRequest parametrosIn);
  public ReservaOnlineResponse reservaonline(ReservaOnlineRequest parametrosIn);

}
