package pe.com.claro.eai.ws.sap.simcards.util;

import java.io.Serializable;

import java.sql.SQLException;

public class PropertiesExternos implements Serializable{
	
	private static final long serialVersionUID = 1L;
  
	public static ReadForeignPropertiesUtil foreignProp = ReadForeignPropertiesUtil.getSingleton(System.getProperty("claro.properties") + "Simcards.properties");
  
	public static final String DB_EAI_JNDI = foreignProp.getValor("db.eai.jndi");
	public static final String DB_EAI_OWNER = foreignProp.getValor("db.eai.owner");
    
  public static final int DB_EAI_TIMEOUT_SECONDS = Integer.parseInt(foreignProp.getValor("db.eai.timeout.seconds"));
  public static final String CODIGO_ESTANDAR_EXITO         = foreignProp.getValor("codigo.estandar.exito");
  public static final String CODIGO_ESTANDAR_ERROR         = foreignProp.getValor("codigo.estandar.error");
  public static final String MENSAJE_ESTANDAR_EXITO        = foreignProp.getValor("mensaje.estandar.exito");
  public static final String MENSAJE_ESTANDAR_ERROR        = foreignProp.getValor("mensaje.estandar.error");
  public static final String CODIGO_ERROR_NODISPONIBLE     = foreignProp.getValor("codigo.error.nodisponible");
  public static final String MENSAJE_ERROR_NODISPONIBLE    = foreignProp.getValor("mensaje.error.nodisponible");
  public static final String CODIGO_ERROR_TIMEOUT          = foreignProp.getValor("codigo.error.timeout");
  public static final String MENSAJE_ERROR_TIMEOUT         = foreignProp.getValor("mensaje.error.timeout");

  public static final int DB_SANS_TIMEOUT_EXECUTION         =Integer.parseInt(foreignProp.getValor("db.sans.timeout.execution"));
  public static final String DB_TIMEOUT_TEXT                = foreignProp.getValor("db.timeout.text");
  
  public static final String IDF_0_CODIGO                   = foreignProp.getValor("asociarnrotelefserie.idf0.codigo");           
  public static final String IDF_0_MENSAJE                  = foreignProp.getValor("asociarnrotelefserie.idf0.mensaje");
  public static final String IDF_1_CODIGO                   = foreignProp.getValor("asociarnrotelefserie.idf1.codigo");                 
  public static final String IDF_1_MENSAJE                  = foreignProp.getValor("asociarnrotelefserie.idf1.mensaje");
  public static final String IDF_2_CODIGO                   = foreignProp.getValor("asociarnrotelefserie.idf2.codigo");                 
  public static final String IDF_2_MENSAJE                  = foreignProp.getValor("asociarnrotelefserie.idf2.mensaje");
  public static final String IDT_1_CODIGO                   = foreignProp.getValor("asociarnrotelefserie.idt1.codigo");
  public static final String IDT_1_MENSAJE                  = foreignProp.getValor("asociarnrotelefserie.idt1.mensaje");
  public static final String IDT_2_CODIGO                   = foreignProp.getValor("asociarnrotelefserie.idt2.codigo");
  public static final String IDT_2_MENSAJE                  = foreignProp.getValor("asociarnrotelefserie.idt2.mensaje");
   
  public static final String DESASOCIAR_NRO_TELEF_SERIE_CODIGO_IDF0     = foreignProp.getValor("desasociar.nro.telef.serie.codigo.idf0");
  public static final String DESASOCIAR_NRO_TELEF_SERIE_MENSAJE_IDF0    = foreignProp.getValor("desasociar.nro.telef.serie.mensaje.idf0");
  public static final String DESASOCIAR_NRO_TELEF_SERIE_CODIGO_IDF1     = foreignProp.getValor("desasociar.nro.telef.serie.codigo.idf1");
  public static final String DESASOCIAR_NRO_TELEF_SERIE_MENSAJE_IDF1    = foreignProp.getValor("desasociar.nro.telef.serie.mensaje.idf1");
  public static final String CODIGO_GENERICO_ERROR_IDT1                 = foreignProp.getValor("codigo.generico.error.idt1");
  public static final String MENSAJE_GENERICO_ERROR_IDT1                = foreignProp.getValor("mensaje.generico.error.idt1");
  public static final String CODIGO_GENERICO_ERROR_IDT2                 = foreignProp.getValor("codigo.generico.error.idt2");
  public static final String MENSAJE_GENERICO_ERROR_IDT2                = foreignProp.getValor("mensaje.generico.error.idt2");
  public static final String DB_EAI_ERRORSQLTIMEOUT_EXCEPTION           = foreignProp.getValor("db.eai.errorSqlTimeoutException");

  public static final String OBTENER_NRO_TELEF_POST_IDF_0_CODIGO     = foreignProp.getValor("obtener_nro_telef_post_idf_0_codigo");
  public static final String OBTENER_NRO_TELEF_POST_IDF_0_MENSAJE    = foreignProp.getValor("obtener_nro_telef_post_idf_0_mensaje");
  public static final String OBTENER_NRO_TELEF_POST_IDF_1_CODIGO     = foreignProp.getValor("obtener_nro_telef_post_idf_1_codigo");
  public static final String OBTENER_NRO_TELEF_POST_IDF_1_MENSAJE    = foreignProp.getValor("obtener_nro_telef_post_idf_1_mensaje");
  public static final String OBTENER_NRO_TELEF_POST_IDT_1_CODIGO     = foreignProp.getValor("obtener_nro_telef_post_idt_1_codigo");
  public static final String OBTENER_NRO_TELEF_POST_IDT_1_MENSAJE    = foreignProp.getValor("obtener_nro_telef_post_idt_1_mensaje");
  public static final String OBTENER_NRO_TELEF_POST_IDT_2_CODIGO     = foreignProp.getValor("obtener_nro_telef_post_idt_2_codigo");
  public static final String OBTENER_NRO_TELEF_POST_IDT_2_MENSAJE    = foreignProp.getValor("obtener_nro_telef_post_idt_2_mensaje");
     
  public static final String OBTENER_NRO_TELEF_PRE_IDF_0_CODIGO     = foreignProp.getValor("obtener_nro_telef_pre_idf_0_codigo");
  public static final String OBTENER_NRO_TELEF_PRE_IDF_0_MENSAJE    = foreignProp.getValor("obtener_nro_telef_pre_idf_0_mensaje");
  public static final String OBTENER_NRO_TELEF_PRE_IDF_1_CODIGO     = foreignProp.getValor("obtener_nro_telef_pre_idf_1_codigo");
  public static final String OBTENER_NRO_TELEF_PRE_IDF_1_MENSAJE    = foreignProp.getValor("obtener_nro_telef_pre_idf_1_mensaje");
  public static final String OBTENER_NRO_TELEF_PRE_IDT_1_CODIGO     = foreignProp.getValor("obtener_nro_telef_pre_idt_1_codigo");
  public static final String OBTENER_NRO_TELEF_PRE_IDT_1_MENSAJE    = foreignProp.getValor("obtener_nro_telef_pre_idt_1_mensaje");
  public static final String OBTENER_NRO_TELEF_PRE_IDT_2_CODIGO     = foreignProp.getValor("obtener_nro_telef_pre_idt_2_codigo");
  public static final String OBTENER_NRO_TELEF_PRE_IDT_2_MENSAJE    = foreignProp.getValor("obtener_nro_telef_pre_idt_2_mensaje");
  public static final String CANTIDAD_LINEAS                        = foreignProp.getValor("cantidad_configurable"); 

}