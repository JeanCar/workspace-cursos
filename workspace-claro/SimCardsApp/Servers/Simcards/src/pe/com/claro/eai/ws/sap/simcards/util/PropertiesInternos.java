package pe.com.claro.eai.ws.sap.simcards.util;

import java.io.Serializable;
import java.util.ResourceBundle;


public class PropertiesInternos implements Serializable{

    private static final long serialVersionUID = 1L;
	
    private static ResourceBundle localProp = ResourceBundle.getBundle("application");	
  
    public static final String SP_CONSULTAR_TELEFONO_DISP         = localProp.getString("sp.consultarTelefonoDisp");
    public static final String SP_CONSULTAR_TELEFONO_TODOS        = localProp.getString("sp.consultarTelefonoTodos");
    public static final String SP_CONSULTAR_STATUS_TELEFONO       = localProp.getString("sp.consultarStatusTelefono");
    public static final String SP_CAMBIAR_STATUS_TELEFONO         = localProp.getString("sp.cambiarStatusTelefono");
    public static final String SP_CAMBIAR_STATUS                  = localProp.getString("sp.cambiarStatus");
    public static final String SP_REVERSION_CAMBIO_NUMERO         = localProp.getString("sp.reversionCambioNumero");
    public static final String SP_REGISTRO_IMSI_PROTABILIDAD      = localProp.getString("sp.registroIMSIPortabilidad");
    public static final String SP_ACTUALIZAR_NRO_TELEF            = localProp.getString("sp.actualizarNroTelef");
    public static final String SP_ELIMINAR_NRO_TELEF              = localProp.getString("sp.eliminarNroTelef");
    public static final String SP_LISTAR_MSISDN_RESERVA           = localProp.getString("sp.listarMSISDNreserva");
    public static final String SP_ROLLBACK_PEL_VENDIDO            = localProp.getString("sp.rollbackPelVendido");
    public static final String SP_CAMBIAR_STATUS_VENDIDO          = localProp.getString("sp.cambiarStatusVendido");
    public static final String SP_OBTENER_DATOS_NRO_TELEF         = localProp.getString("sp.obtenerDatosNroTelef");
    public static final String SP_OBTENER_NRO_TELEF_SERIE         = localProp.getString("sp.obtenerNroTelefSerie");
    public static final String SP_OBTENER_NRO_TELEF               = localProp.getString("sp.obtenerNroTelef");
    public static final String SP_OBTENER_HLR                     = localProp.getString("sp.obtenerHlr");
    public static final String SP_CAMBIAR_ESTADO_SIMCARD          = localProp.getString("sp.cambiarEstadoSimcard");
    public static final String SP_SANSSU_ASOCIA_NROSERIE          = localProp.getString("sp.sanssuAsociaNroserie");    
    public static final String SP_DESASOCIAR_NRO_TELEF_SERIE      = localProp.getString("sp.desasociarNroTelefSerie");
    public static final String SP_OBTENER_NRO_TELEFPOST           = localProp.getString("sp.obtenerNroTelefPost");
    public static final String SP_OBTENER_NRO_TELEFPRE            = localProp.getString("sp.obtenerNroTelefPre");    
    public static final String SP_LISTAR_LINEAS_PRERESERVADAS     = localProp.getString("sp.listarLineasPrereservadas");
    public static final String SP_RESERVA_ONLINE                  = localProp.getString("sp.reservaOnline");
    public static final String SP_COD_EXITO                       = localProp.getString("sp.cod.exito");
    public static final int  LINEA_0                              = Integer.parseInt(localProp.getString("linea.0"));
    public static final String VACIO                              = localProp.getString("variable.vacia");
    public static final String VALOR_MENOS_UNO                    = localProp.getString("valor.menos.uno");
}
