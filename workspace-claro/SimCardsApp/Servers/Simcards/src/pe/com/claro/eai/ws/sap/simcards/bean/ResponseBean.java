package pe.com.claro.eai.ws.sap.simcards.bean;

import java.io.Serializable;

import java.util.List;
import pe.com.claro.eai.ws.sap.simcards.types.ItReturnType;

public class ResponseBean implements Serializable{
    
  private static final long serialVersionUID = 1L;
  //private ItReturnType itReturnRow;
  private List<DesasociarBean> cursorDesasociar;
  
    public void setCursorDesasociar(List<DesasociarBean> cursorDesasociar) {
        this.cursorDesasociar = cursorDesasociar;
    }
  
    public List<DesasociarBean> getCursorDesasociar() {
        return cursorDesasociar;
    }
    /*
    public void setItReturnRow(ItReturnType itReturnRow) {
      this.itReturnRow = itReturnRow;
    }
    
    public ItReturnType getItReturnRow(){
        return  itReturnRow;
    } 
   */
}
