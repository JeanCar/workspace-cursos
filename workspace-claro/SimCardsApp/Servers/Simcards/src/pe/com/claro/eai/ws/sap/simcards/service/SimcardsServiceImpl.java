package pe.com.claro.eai.ws.sap.simcards.service;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.ws.sap.simcards.bean.DesasociarBean;
import pe.com.claro.eai.ws.sap.simcards.bean.ResponseBean;
import pe.com.claro.eai.ws.sap.simcards.dao.EAIDAO;
import pe.com.claro.eai.ws.sap.simcards.exception.DBException;
import pe.com.claro.eai.ws.sap.simcards.types.*;
import pe.com.claro.eai.ws.sap.simcards.util.EaiUtil;
import pe.com.claro.eai.ws.sap.simcards.util.PropertiesExternos;
import pe.com.claro.eai.ws.sap.simcards.util.PropertiesInternos;

@Service
public class SimcardsServiceImpl implements SimcardsService{
  private static Logger logger = Logger.getLogger(SimcardsServiceImpl.class);

  @Autowired
  private EAIDAO eaiDAO;    


  /**
   * 1) consultarTelefonoDisp : consulta de telefono
   * @param request
   * @return bean
   */
  public ConsultarTelefonoDispResponse consultarTelefonoDisp(ConsultarTelefonoDispRequest request) {
    String traId =  request.getIdTransaccion() + "[consultarTelefonoDisponible]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo consultarTelefonoDisponible ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ConsultarTelefonoDispResponse bean = new ConsultarTelefonoDispResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
        
    try {
        
       bean = eaiDAO.consultarTelefonoDisp(request);
       
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }   finally {
        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo consultarTelefonoDisp");
    }    

    return bean;
  }

  /**
   * 2) consultarTelefonoTodos : consulta de numero de telefono
   * @param request
   * @return bean
   */
  public ConsultarTelefonoTodosResponse consultarTelefonoTodos(ConsultarTelefonoTodosRequest request) {
    String traId =  request.getIdTransaccion() + "[consultarTelefonoTodos]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo consultarTelefonoTodos ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ConsultarTelefonoTodosResponse bean = new ConsultarTelefonoTodosResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
       bean = eaiDAO.consultarTelefonoTodos(request);
       
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }  finally {
      logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo consultarTelefonoTodos");
    }    
  

    return bean;
  }

  /**
   * 3) consultarStatusTelefono : consulta de estatus de n�meros tel.
   * @param request
   * @return
   */
  public ConsultarStatusTelefonoResponse consultarStatusTelefono(ConsultarStatusTelefonoRequest request) {
    String traId =  request.getIdTransaccion() + "[consultarStatusTelefono]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo consultarStatusTelefono ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ConsultarStatusTelefonoResponse bean = new ConsultarStatusTelefonoResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
       bean = eaiDAO.consultarStatusTelefono(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }  finally {
        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo consultarStatusTelefono");
    }    

    return bean;
  }
  /**
   * 4) cambiarStatusTelefono : Cambio de Status del Nro Telef�nico.
   * @param request
   * @return beancambiarStatusTelefono
   */
  public CambiarStatusTelefonoResponse cambiarStatusTelefono(CambiarStatusTelefonoRequest request) {
    String traId =  request.getIdTransaccion() + "[cambiarStatusTelefono]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo cambiarStatusTelefono ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    CambiarStatusTelefonoResponse bean = new CambiarStatusTelefonoResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
       bean = eaiDAO.cambiarStatusTelefono(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    } finally {
        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo cambiarStatusTelefono");
    }   

    return bean;
  }
  /**
   * 5) cambiarStatus : Cambio de STATUS.
   * @param request
   * @return
   */
  public CambiarStatusResponse cambiarStatus(CambiarStatusRequest request) {
    String traId =  request.getIdTransaccion() + "[cambiarStatus]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo cambiarStatus ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    CambiarStatusResponse bean = new CambiarStatusResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.cambiarStatus(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }finally {
        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo cambiarStatus");
    }  

    return bean;
  }
  /**
   * 6) reversionCambioNumero : Reversi�n de cambio de n�mero.
   * @param request
   * @return bean
   */
  public ReversionCambioNumeroResponse reversionCambioNumero(ReversionCambioNumeroRequest request) {
    String traId =  request.getIdTransaccion() + "[reversionCambioNumero]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo reversionCambioNumero ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ReversionCambioNumeroResponse bean = new ReversionCambioNumeroResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    try {
      bean = eaiDAO.reversionCambioNumero(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    } finally {
        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo reversionCambioNumero");
    }
    return bean;
  }
  /**
   * 7) registroIMSIPortabilidad : Registro del IMSI Portabilidad.
   * @param request
   * @return bean
   */
  public RegistroIMSIPortabilidadResponse registroIMSIPortabilidad(RegistroIMSIPortabilidadRequest request) {
    String traId =  request.getIdTransaccion() + "[registroIMSIPortabilidad]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo registroIMSIPortabilidad ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    RegistroIMSIPortabilidadResponse bean = new RegistroIMSIPortabilidadResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.registroIMSIPortabilidad(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }  finally {
      logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo registroIMSIPortabilidad");
    }

    return bean;
  }
  /**
   * 8) actualizarNroTelef : Actualizacion de n�mero de telefono.
   * @param request
   * @return bean
   */
  public ActualizarNroTelefResponse actualizarNroTelef(ActualizarNroTelefRequest request) {
    String traId =  request.getIdTransaccion() + "[actualizarNroTelef]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo actualizarNroTelef ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ActualizarNroTelefResponse bean = new ActualizarNroTelefResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.actualizarNroTelef(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }    finally {
        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo actualizarNroTelef");
    }

    return bean;
  }
  /**
   * 9) eliminarNroTelef : Transportabilidad: Deleteo de n�mero telef�nico..
   * @param request
   * @return bean
   */
  public EliminarNroTelefResponse eliminarNroTelef(EliminarNroTelefRequest request) {
    String traId =  request.getIdTransaccion() + "[eliminarNroTelef]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo eliminarNroTelef ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    EliminarNroTelefResponse bean = new EliminarNroTelefResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.eliminarNroTelef(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }  finally {
           logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
           this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
           logger.info(traId + "======== Fin del metodo eliminarNroTelef");
       }     

    return bean;
  }
  /**
   * 10) listarMSISDNreserva : Obtener MSISDN que busque y reserve N� ZSans.
   * @param request
   * @return bean
   */
  public ListarMSISDNreservaResponse listarMSISDNreserva(ListarMSISDNreservaRequest request) {
    String traId =  request.getIdTransaccion() + "[listarMSISDNreserva]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo listarMSISDNreserva ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ListarMSISDNreservaResponse bean = new ListarMSISDNreservaResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
       logger.info(traId + "Datos de Salida: " + EaiUtil.getXmlTextFromJaxB(bean));

       bean = eaiDAO.listarMSISDNreserva(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    } finally {
            logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
            this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
            logger.info(traId + "======== Fin del metodo listarMSISDNreserva");
       }  

    return bean;
  }
  /**
   * 11) rollbackPelVendido : Rollback PEL a VENDIDO.
   * @param request
   * @return bean
   */
  public RollbackPelVendidoResponse rollbackPelVendido(RollbackPelVendidoRequest request) {
    String traId =  request.getIdTransaccion() + "[rollbackPelVendido]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo rollbackPelVendido ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    RollbackPelVendidoResponse bean = new RollbackPelVendidoResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.rollbackPelVendido(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }   finally {
           logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
           this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
           logger.info(traId + "======== Fin del metodo rollbackPelVendido");
       }   

    return bean;
  }
  /**
   * 12) cambiarStatusVendido : Port-out a numero telefonico - Portabilidad.
   * @param request
   * @return bean
   */
  public CambiarStatusVendidoResponse cambiarStatusVendido(CambiarStatusVendidoRequest request) {
    String traId =  request.getIdTransaccion() + "[cambiarStatusVendido]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo cambiarStatusVendido ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    CambiarStatusVendidoResponse bean = new CambiarStatusVendidoResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.cambiarStatusVendido(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    } finally {
          logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                                
           this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
           logger.info(traId + "======== Fin del metodo cambiarStatusVendido");
       } 

    return bean;
  }
  /**
   * 13) obtenerDatosNroTelef : Obtener todos los datos del n�mero telef�nico, consultando por varios par�metros.
   * @param request
   * @return
   */
  public ObtenerDatosNroTelefResponse obtenerDatosNroTelef(ObtenerDatosNroTelefRequest request) {
    String traId =  request.getIdTransaccion() + "[obtenerDatosNroTelef]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo obtenerDatosNroTelef ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ObtenerDatosNroTelefResponse bean = new ObtenerDatosNroTelefResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.obtenerDatosNroTelef(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    } 
    finally {
          logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));            
           this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
           logger.info(traId + "======== Fin del metodo obtenerDatosNroTelef");
       }

    return bean;
  }
  /**
   * 14) obtenerNroTelefSerie : Obtener el n�mero telef�nico y serie, enviado como par�metros de entrada
   *                            el n�mero telef�nico y/o serie.
   * @param request
   * @return bean
   */
  public ObtenerNroTelefSerieResponse obtenerNroTelefSerie(ObtenerNroTelefSerieRequest request) {
    String traId =  request.getIdTransaccion() + "[obtenerNroTelefSerie]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo obtenerNroTelefSerie ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ObtenerNroTelefSerieResponse bean = new ObtenerNroTelefSerieResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.obtenerNroTelefSerie(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }  
    finally {
          logger.info(traId + "Datos de Salida:" + EaiUtil.getXmlTextFromJaxB(bean));            
           this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
           logger.info(traId + "======== Fin del metodo obtenerNroTelefSerie");
       }

    return bean;
  }
  /**
   * 15) obtenerNroTelef : Obtener el n�mero telef�nico, desde el env�o del c�digo de material y 
   *                       el n�mero de serie.
   * @param request
   * @return bean
   */
  public ObtenerNroTelefResponse obtenerNroTelef(ObtenerNroTelefRequest request) {
    String traId =  request.getIdTransaccion() + "[obtenerNroTelef]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo obtenerNroTelef ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ObtenerNroTelefResponse bean = new ObtenerNroTelefResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    
    try {
      bean = eaiDAO.obtenerNroTelef(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }   
    return bean;
  }
  /**
   * 16) obtenerHLR : Consultar la tabla de los n�meros telef�nicos y obtener el c�digo del HLR
   * @param request
   * @return bean
   */
  public ObtenerHLRResponse obtenerHLR(ObtenerHLRRequest request) {
    String traId =  request.getIdTransaccion() + "[obtenerHLR]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo obtenerHLR ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ObtenerHLRResponse bean = new ObtenerHLRResponse();
    bean.setIdTransaccion(request.getIdTransaccion());
    try {
      bean = eaiDAO.obtenerHLR(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    }  finally {
        logger.info(traId + "Datos de Salida:" + EaiUtil.getXmlTextFromJaxB(bean));            
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo obtenerHLR");
    }
    return bean;
  }
  
  
  /**
   * 17) ActualizarSimcardPorReposicion : Actualizaci�n de material y de serie seg�n el n�mero de cliente.
   * @param request
   * @return bean
   */
  public ActualizarSimcardPorReposicionResponse actualizarSimcardPorReposicion(ActualizarSimcardPorReposicionRequest request) {
    String traId =  request.getIdTransaccion() + "[actualizarSimcardPorReposicion]-";
    long tiempoInicio = System.currentTimeMillis();
    logger.info(traId + "======== Inicio del metodo actualizarSimcardPorReposicion ========");
    logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
    
    ActualizarSimcardPorReposicionResponse bean = new ActualizarSimcardPorReposicionResponse();

    try {
      bean = eaiDAO.actualizarSimcardPorReposicion(request);
    } catch (Exception e) {
      logger.error(traId + "Hubo un error en el proceso:",e);
      bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
      bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
      logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
    } finally {
        logger.info(traId + "Datos de Salida:" + EaiUtil.getXmlTextFromJaxB(bean));            
        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
        logger.info(traId + "======== Fin del metodo actualizarSimcardPorReposicion");
    }
    return bean;
  }
    /**
     * 18) asociarNroTelefSerie : Realiza la asociaci�n de un n�mero telef�nico con la serie..
     * @param request
     * @return
     */

    public AsociarNroTelefSerieResponse asociarNroTelefSerie(AsociarNroTelefSerieRequest request) {
      String traId =  request.getIdTransaccion() + "[asociarNroTelefSerie]-";
      AsociarNroTelefSerieResponse asociarNroTelefSerieResponse =new AsociarNroTelefSerieResponse();
      long tiempoInicio = System.currentTimeMillis();
      logger.info(traId + "======== Inicio del metodo asociarNroTelefSerie ========");
    
      logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
      
      AsociarNroTelefSerieRequest beanRequest = new AsociarNroTelefSerieRequest();    
      beanRequest.setIdTransaccion(request.getIdTransaccion());
      beanRequest.setNroTelef(request.getNroTelef());
      beanRequest.setNroSerie(request.getNroSerie());
      beanRequest.setMaterial(request.getMaterial());      
             
            try {
                
                asociarNroTelefSerieResponse = eaiDAO.asociarNroTelefSerie(beanRequest);
                             
               if(asociarNroTelefSerieResponse.getItOutput()!=null || asociarNroTelefSerieResponse.getItReturn()!=null ){                
                    if(asociarNroTelefSerieResponse.getItReturn().getItReturnRow().get(PropertiesInternos.LINEA_0).getCodigo().
                        equals(PropertiesExternos.IDF_0_CODIGO)){                   
                        asociarNroTelefSerieResponse.setCodigoResultado(PropertiesExternos.IDF_0_CODIGO);
                        asociarNroTelefSerieResponse.setMensajeResultado(PropertiesExternos.IDF_0_MENSAJE); 
                    
                    }else if(asociarNroTelefSerieResponse.getItReturn().getItReturnRow().get(PropertiesInternos.LINEA_0).getCodigo().
                        equals(PropertiesExternos.IDF_1_CODIGO)){                        
                        asociarNroTelefSerieResponse.setCodigoResultado(PropertiesExternos.IDF_1_CODIGO);
                        asociarNroTelefSerieResponse.setMensajeResultado(PropertiesExternos.IDF_1_MENSAJE);  
                      
                    }else{
                        asociarNroTelefSerieResponse.setCodigoResultado(PropertiesExternos.IDF_2_CODIGO);
                        asociarNroTelefSerieResponse.setMensajeResultado(PropertiesExternos.IDF_2_MENSAJE);
                    }
                               
                }
               
            } catch (Exception e) {
                   asociarNroTelefSerieResponse.setCodigoResultado(PropertiesExternos.IDT_1_CODIGO);
                   asociarNroTelefSerieResponse.setMensajeResultado(PropertiesExternos.IDT_1_MENSAJE);  

            }finally {
                logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(asociarNroTelefSerieResponse));                        
                this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
                logger.info(traId + "======== Fin del metodo asociarNroTelefSerie");
            } 

            return asociarNroTelefSerieResponse;
        }
  /**
     * 19) DesasociarNroTelefSerie : desasocia el numero de linea de la serie.
     * @param request
     * @return bean
  */
  public DesasociarNroTelefSerieResponse desasociarNroTelefSerie(DesasociarNroTelefSerieRequest request) {  
        String traId =  request.getIdTransaccion() + "[desasociarNroTelefSerie]-";
        long tiempoInicio = System.currentTimeMillis();
        logger.info(traId + "======== Inicio del metodo desasociarNroTelefSerie ========");
        logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
        
        DesasociarNroTelefSerieResponse bean = new DesasociarNroTelefSerieResponse();
        ResponseBean response= new ResponseBean();
        DesasociarBean desasociarBean = new DesasociarBean();
        DesasociarNroTelefSerieResponse.ItReturn itReturnResp= new DesasociarNroTelefSerieResponse.ItReturn(); 
        ItReturnType itReturnresponse= new ItReturnType();
        bean.setIdTransaccion(request.getIdTransaccion());
        try {

          response=eaiDAO.realizarDesosiacion(request);
          desasociarBean = response.getCursorDesasociar().get(0);
            
          itReturnresponse.setTipo(desasociarBean.getTipo());
          itReturnresponse.setClaseMensaje(desasociarBean.getClaseMensaje());
          itReturnresponse.setNumeroMensaje(desasociarBean.getNumeroMensaje());
          itReturnresponse.setMensaje(desasociarBean.getMensaje());
          itReturnresponse.setNumeroLog(desasociarBean.getNumeroLog());
          itReturnresponse.setNumeroConsecutivoInterno(desasociarBean.getNumeroConsecutivoInterno());
          itReturnresponse.setMensajeV1(desasociarBean.getMensajeV1());
          itReturnresponse.setMensajeV2(desasociarBean.getMensajeV2());
          itReturnresponse.setMensajeV3(desasociarBean.getMensajeV3());
          itReturnresponse.setMensajeV4(desasociarBean.getMensajeV4());
          itReturnresponse.setParametro(desasociarBean.getParametro());
          itReturnresponse.setLineas(desasociarBean.getLineas());
          itReturnresponse.setCampo(desasociarBean.getCampo());
          itReturnresponse.setSistema(desasociarBean.getSistema());
          itReturnresponse.setCodigo(desasociarBean.getCodigo());   
                    
          itReturnResp.getItReturnRow().add(itReturnresponse);
            
          if(desasociarBean.getCodigo().equals(PropertiesInternos.SP_COD_EXITO)){     
            bean.setItReturn(itReturnResp);
            bean.setCodigoResultado(PropertiesExternos.DESASOCIAR_NRO_TELEF_SERIE_CODIGO_IDF0);
            bean.setMensajeResultado(PropertiesExternos.DESASOCIAR_NRO_TELEF_SERIE_MENSAJE_IDF0);
            
          }else{  
            bean.setItReturn(itReturnResp);
            bean.setCodigoResultado(PropertiesExternos.DESASOCIAR_NRO_TELEF_SERIE_CODIGO_IDF1);
            bean.setMensajeResultado(PropertiesExternos.DESASOCIAR_NRO_TELEF_SERIE_MENSAJE_IDF1);    
          }
        
        } catch (DBException e) {
           logger.error(traId + " Error en SP: "+ e.getMessage() );
           bean.setCodigoResultado(e.getCode());        
           bean.setMensajeResultado(e.getMessage());
                       
        } catch (Exception e) {
          logger.error(traId + "Hubo un error en el proceso:" + e.getMessage(), e);
          bean.setCodigoResultado(PropertiesExternos.CODIGO_GENERICO_ERROR_IDT2);        
          bean.setMensajeResultado(PropertiesExternos.MENSAJE_GENERICO_ERROR_IDT2);
          
        } finally {
            logger.info(traId + "Datos de Salida:" + EaiUtil.getXmlTextFromJaxB(bean));            
            this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
            logger.info(traId + "======== Fin del metodo desasociarNroTelefSerie");
        }
    return bean;    
  }
    /**
     * 20) obtenerNroTelefPost : Obtener el n�mero telef�nico, desde el env�o del c�digo de material y 
     *                           el n�mero de serie.
     * @param request
     * @return bean
     */
    public ObtenerNroTelefPostResponse obtenerNroTelefPost(ObtenerNroTelefPostRequest request) {
      String traId =  request.getIdTransaccion() + "[obtenerNroTelefPost]-";
      long tiempoInicio = System.currentTimeMillis();
      logger.info(traId + "======== Inicio del metodo obtenerNroTelefPost ========");
      logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
      
      ObtenerNroTelefPostResponse bean = new ObtenerNroTelefPostResponse();
      bean.setIdTransaccion(request.getIdTransaccion());
      
      try {
        bean = eaiDAO.obtenerNroTelefPost(request);
      } catch (Exception e) {
        logger.error(traId + "Hubo un error en el proceso:",e);
        bean.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_POST_IDF_1_CODIGO);        
        bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
        logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
      }   
      return bean;
}
    
    public ObtenerNroTelefPreResponse obtenerNroTelefPre(ObtenerNroTelefPreRequest request){ 
        String traId =  request.getIdTransaccion() + "[obtenerNroTelefPre]-";
        long tiempoInicio = System.currentTimeMillis();
        logger.info(traId + "======== Inicio del metodo obtenerNroTelefPre ========");
        logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
            
        ObtenerNroTelefPreResponse bean = new ObtenerNroTelefPreResponse();
        bean.setIdTransaccion(request.getIdTransaccion());
            
        try {
            bean = eaiDAO.obtenerNroTelefPre(request);
        } catch (Exception e) {
            logger.error(traId + "Hubo un error en el proceso:",e);
            bean.setCodigoResultado(PropertiesExternos.OBTENER_NRO_TELEF_PRE_IDF_1_CODIGO);        
            bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
            logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
        }   
        return bean;
}
    

   public ListarLineasPrereservadasResponse listarLineasPrereservadas(ListarLineasPrereservadasRequest request) {
      String traId =  request.getIdTransaccion() + "[listarLineasPrereservadas]";
              
                long tiempoInicio = System.currentTimeMillis();
                logger.info(traId + "======== Inicio del metodo listarLineasPrereservadas ========");
                logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
                
                ListarLineasPrereservadasResponse bean = new ListarLineasPrereservadasResponse();
                ListarLineasPrereservadasRequest response = new ListarLineasPrereservadasRequest();
                bean.setIdTransaccion(request.getIdTransaccion());
                request.setCantidadLineas(PropertiesExternos.CANTIDAD_LINEAS);
              
            try {
                   logger.info(traId + "Datos de Salida: " + EaiUtil.getXmlTextFromJaxB(bean));

                   bean = eaiDAO.listarLineasPrereservadas(request);
                } 
            catch (Exception e) 
                {
                  logger.error(traId + "Hubo un error en el proceso:",e);
                  bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
                  bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
                  logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
                } 
            finally
                  {
                        logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
                        this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
                        logger.info(traId + "======== Fin del metodo listarLineasPrereservadas");
                   }  

                return bean;
}
        public ReservaOnlineResponse reservaonline(ReservaOnlineRequest request) {
        String traId =  request. getIdTransaccion() + "[reservaonline]-";
        long tiempoInicio = System.currentTimeMillis();
        logger.info(traId + "======== Inicio del metodo reservaonline ========");
        logger.info(traId + "Datos de Entrada: " + EaiUtil.getXmlTextFromJaxB(request));
        
        ReservaOnlineResponse bean = new ReservaOnlineResponse();
        bean.setIdTransaccion(request.getIdTransaccion());
        
        try {
          bean = eaiDAO.reservaonline(request);
        } catch (Exception e) {
          logger.error(traId + "Hubo un error en el proceso:",e);
          bean.setCodigoResultado(PropertiesExternos.CODIGO_ESTANDAR_ERROR);        
          bean.setMensajeResultado("Error "+ e + ": " + e.getMessage());
          logger.error(traId + EaiUtil.getXmlTextFromJaxB(bean));
        }    finally {
            logger.info(traId + "Datos de Salida :" + EaiUtil.getXmlTextFromJaxB(bean));                        
            this.logger.info( traId + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio) + " milisegundos ]" );        
            logger.info(traId + "======== Fin del metodo reservaonline");
        }

        return bean;
  }
}

