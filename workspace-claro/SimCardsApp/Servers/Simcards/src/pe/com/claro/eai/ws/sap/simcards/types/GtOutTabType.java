
package pe.com.claro.eai.ws.sap.simcards.types;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gtOutTabType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gtOutTabType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codRet" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="desCod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="texto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusSim" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gtOutTabType", propOrder = {
    "codRet",
    "desCod",
    "texto",
    "statusSim"
})
public class GtOutTabType {

    @XmlElement(required = true)
    protected BigInteger codRet;
    @XmlElement(required = true)
    protected String desCod;
    @XmlElement(required = true)
    protected String texto;
    @XmlElement(required = true)
    protected String statusSim;

    /**
     * Gets the value of the codRet property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCodRet() {
        return codRet;
    }

    /**
     * Sets the value of the codRet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCodRet(BigInteger value) {
        this.codRet = value;
    }

    /**
     * Gets the value of the desCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCod() {
        return desCod;
    }

    /**
     * Sets the value of the desCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCod(String value) {
        this.desCod = value;
    }

    /**
     * Gets the value of the texto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Sets the value of the texto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTexto(String value) {
        this.texto = value;
    }

    /**
     * Gets the value of the statusSim property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusSim() {
        return statusSim;
    }

    /**
     * Sets the value of the statusSim property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusSim(String value) {
        this.statusSim = value;
    }

}
