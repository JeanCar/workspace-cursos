
package pe.com.claro.eai.ws.sap.simcards.types;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for itReturnType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="itReturnType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="claseMensaje" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroMensaje" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensaje" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroLog" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroConsecutivoInterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeV1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeV2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeV3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeV4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parametro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lineas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="campo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sistema" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itReturnType", propOrder = {
    "tipo",
    "claseMensaje",
    "numeroMensaje",
    "mensaje",
    "numeroLog",
    "numeroConsecutivoInterno",
    "mensajeV1",
    "mensajeV2",
    "mensajeV3",
    "mensajeV4",
    "parametro",
    "lineas",
    "campo",
    "sistema",
    "codigo"
})
public class ItReturnType {

    @XmlElement(required = true)
    protected String tipo;
    @XmlElement(required = true)
    protected String claseMensaje;
    @XmlElement(required = true)
    protected String numeroMensaje;
    @XmlElement(required = true)
    protected String mensaje;
    @XmlElement(required = true)
    protected String numeroLog;
    @XmlElement(required = true)
    protected String numeroConsecutivoInterno;
    @XmlElement(required = true)
    protected String mensajeV1;
    @XmlElement(required = true)
    protected String mensajeV2;
    @XmlElement(required = true)
    protected String mensajeV3;
    @XmlElement(required = true)
    protected String mensajeV4;
    @XmlElement(required = true)
    protected String parametro;
    @XmlElement(required = true)
    protected BigInteger lineas;
    @XmlElement(required = true)
    protected String campo;
    @XmlElement(required = true)
    protected String sistema;
    @XmlElement(required = true)
    protected String codigo;

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Gets the value of the claseMensaje property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaseMensaje() {
        return claseMensaje;
    }

    /**
     * Sets the value of the claseMensaje property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaseMensaje(String value) {
        this.claseMensaje = value;
    }

    /**
     * Gets the value of the numeroMensaje property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMensaje() {
        return numeroMensaje;
    }

    /**
     * Sets the value of the numeroMensaje property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMensaje(String value) {
        this.numeroMensaje = value;
    }

    /**
     * Gets the value of the mensaje property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Sets the value of the mensaje property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Gets the value of the numeroLog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLog() {
        return numeroLog;
    }

    /**
     * Sets the value of the numeroLog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLog(String value) {
        this.numeroLog = value;
    }

    /**
     * Gets the value of the numeroConsecutivoInterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroConsecutivoInterno() {
        return numeroConsecutivoInterno;
    }

    /**
     * Sets the value of the numeroConsecutivoInterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroConsecutivoInterno(String value) {
        this.numeroConsecutivoInterno = value;
    }

    /**
     * Gets the value of the mensajeV1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeV1() {
        return mensajeV1;
    }

    /**
     * Sets the value of the mensajeV1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeV1(String value) {
        this.mensajeV1 = value;
    }

    /**
     * Gets the value of the mensajeV2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeV2() {
        return mensajeV2;
    }

    /**
     * Sets the value of the mensajeV2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeV2(String value) {
        this.mensajeV2 = value;
    }

    /**
     * Gets the value of the mensajeV3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeV3() {
        return mensajeV3;
    }

    /**
     * Sets the value of the mensajeV3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeV3(String value) {
        this.mensajeV3 = value;
    }

    /**
     * Gets the value of the mensajeV4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeV4() {
        return mensajeV4;
    }

    /**
     * Sets the value of the mensajeV4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeV4(String value) {
        this.mensajeV4 = value;
    }

    /**
     * Gets the value of the parametro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParametro() {
        return parametro;
    }

    /**
     * Sets the value of the parametro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParametro(String value) {
        this.parametro = value;
    }

    /**
     * Gets the value of the lineas property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLineas() {
        return lineas;
    }

    /**
     * Sets the value of the lineas property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLineas(BigInteger value) {
        this.lineas = value;
    }

    /**
     * Gets the value of the campo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampo() {
        return campo;
    }

    /**
     * Sets the value of the campo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampo(String value) {
        this.campo = value;
    }

    /**
     * Gets the value of the sistema property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistema() {
        return sistema;
    }

    /**
     * Sets the value of the sistema property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistema(String value) {
        this.sistema = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

}
