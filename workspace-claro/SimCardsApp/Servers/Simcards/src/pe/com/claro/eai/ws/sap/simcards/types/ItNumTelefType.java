
package pe.com.claro.eai.ws.sap.simcards.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for itNumTelefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="itNumTelefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroTelef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="marcCorta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigRtpe" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pniAbierto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bezei" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descrRed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoHlr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descrStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itNumTelefType", propOrder = {
    "nroTelef",
    "marcCorta",
    "codigRtpe",
    "pniAbierto",
    "empresa",
    "bezei",
    "descrRed",
    "codigoHlr",
    "descrStatus"
})
public class ItNumTelefType {

    @XmlElement(required = true)
    protected String nroTelef;
    @XmlElement(required = true)
    protected String marcCorta;
    @XmlElement(required = true)
    protected String codigRtpe;
    @XmlElement(required = true)
    protected String pniAbierto;
    @XmlElement(required = true)
    protected String empresa;
    @XmlElement(required = true)
    protected String bezei;
    @XmlElement(required = true)
    protected String descrRed;
    @XmlElement(required = true)
    protected String codigoHlr;
    @XmlElement(required = true)
    protected String descrStatus;

    /**
     * Gets the value of the nroTelef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroTelef() {
        return nroTelef;
    }

    /**
     * Sets the value of the nroTelef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroTelef(String value) {
        this.nroTelef = value;
    }

    /**
     * Gets the value of the marcCorta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarcCorta() {
        return marcCorta;
    }

    /**
     * Sets the value of the marcCorta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarcCorta(String value) {
        this.marcCorta = value;
    }

    /**
     * Gets the value of the codigRtpe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigRtpe() {
        return codigRtpe;
    }

    /**
     * Sets the value of the codigRtpe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigRtpe(String value) {
        this.codigRtpe = value;
    }

    /**
     * Gets the value of the pniAbierto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPniAbierto() {
        return pniAbierto;
    }

    /**
     * Sets the value of the pniAbierto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPniAbierto(String value) {
        this.pniAbierto = value;
    }

    /**
     * Gets the value of the empresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Sets the value of the empresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Gets the value of the bezei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezei() {
        return bezei;
    }

    /**
     * Sets the value of the bezei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezei(String value) {
        this.bezei = value;
    }

    /**
     * Gets the value of the descrRed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrRed() {
        return descrRed;
    }

    /**
     * Sets the value of the descrRed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrRed(String value) {
        this.descrRed = value;
    }

    /**
     * Gets the value of the codigoHlr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoHlr() {
        return codigoHlr;
    }

    /**
     * Sets the value of the codigoHlr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoHlr(String value) {
        this.codigoHlr = value;
    }

    /**
     * Gets the value of the descrStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrStatus() {
        return descrStatus;
    }

    /**
     * Sets the value of the descrStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrStatus(String value) {
        this.descrStatus = value;
    }

}
