
package pe.com.claro.eai.ws.sap.simcards.types;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTransaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensajeResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subRc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="esResumen">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="esResumenRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}esResumenType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="etDetalle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="etDetalleRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}etDetalleType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="itReturn">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="itReturnRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itReturnType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idTransaccion",
    "codigoResultado",
    "mensajeResultado",
    "subRc",
    "esResumen",
    "etDetalle",
    "itReturn"
})
@XmlRootElement(name = "cambiarStatusTelefonoResponse")
public class CambiarStatusTelefonoResponse {

    @XmlElement(required = true)
    protected String idTransaccion;
    @XmlElement(required = true)
    protected String codigoResultado;
    @XmlElement(required = true)
    protected String mensajeResultado;
    @XmlElement(required = true)
    protected BigInteger subRc;
    @XmlElement(required = true)
    protected CambiarStatusTelefonoResponse.EsResumen esResumen;
    @XmlElement(required = true)
    protected CambiarStatusTelefonoResponse.EtDetalle etDetalle;
    @XmlElement(required = true)
    protected CambiarStatusTelefonoResponse.ItReturn itReturn;

    /**
     * Gets the value of the idTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * Sets the value of the idTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTransaccion(String value) {
        this.idTransaccion = value;
    }

    /**
     * Gets the value of the codigoResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResultado() {
        return codigoResultado;
    }

    /**
     * Sets the value of the codigoResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResultado(String value) {
        this.codigoResultado = value;
    }

    /**
     * Gets the value of the mensajeResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeResultado() {
        return mensajeResultado;
    }

    /**
     * Sets the value of the mensajeResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeResultado(String value) {
        this.mensajeResultado = value;
    }

    /**
     * Gets the value of the subRc property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubRc() {
        return subRc;
    }

    /**
     * Sets the value of the subRc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubRc(BigInteger value) {
        this.subRc = value;
    }

    /**
     * Gets the value of the esResumen property.
     * 
     * @return
     *     possible object is
     *     {@link CambiarStatusTelefonoResponse.EsResumen }
     *     
     */
    public CambiarStatusTelefonoResponse.EsResumen getEsResumen() {
        return esResumen;
    }

    /**
     * Sets the value of the esResumen property.
     * 
     * @param value
     *     allowed object is
     *     {@link CambiarStatusTelefonoResponse.EsResumen }
     *     
     */
    public void setEsResumen(CambiarStatusTelefonoResponse.EsResumen value) {
        this.esResumen = value;
    }

    /**
     * Gets the value of the etDetalle property.
     * 
     * @return
     *     possible object is
     *     {@link CambiarStatusTelefonoResponse.EtDetalle }
     *     
     */
    public CambiarStatusTelefonoResponse.EtDetalle getEtDetalle() {
        return etDetalle;
    }

    /**
     * Sets the value of the etDetalle property.
     * 
     * @param value
     *     allowed object is
     *     {@link CambiarStatusTelefonoResponse.EtDetalle }
     *     
     */
    public void setEtDetalle(CambiarStatusTelefonoResponse.EtDetalle value) {
        this.etDetalle = value;
    }

    /**
     * Gets the value of the itReturn property.
     * 
     * @return
     *     possible object is
     *     {@link CambiarStatusTelefonoResponse.ItReturn }
     *     
     */
    public CambiarStatusTelefonoResponse.ItReturn getItReturn() {
        return itReturn;
    }

    /**
     * Sets the value of the itReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CambiarStatusTelefonoResponse.ItReturn }
     *     
     */
    public void setItReturn(CambiarStatusTelefonoResponse.ItReturn value) {
        this.itReturn = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="esResumenRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}esResumenType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "esResumenRow"
    })
    public static class EsResumen {

        protected List<EsResumenType> esResumenRow;

        /**
         * Gets the value of the esResumenRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the esResumenRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEsResumenRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EsResumenType }
         * 
         * 
         */
        public List<EsResumenType> getEsResumenRow() {
            if (esResumenRow == null) {
                esResumenRow = new ArrayList<EsResumenType>();
            }
            return this.esResumenRow;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="etDetalleRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}etDetalleType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "etDetalleRow"
    })
    public static class EtDetalle {

        protected List<EtDetalleType> etDetalleRow;

        /**
         * Gets the value of the etDetalleRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the etDetalleRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEtDetalleRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EtDetalleType }
         * 
         * 
         */
        public List<EtDetalleType> getEtDetalleRow() {
            if (etDetalleRow == null) {
                etDetalleRow = new ArrayList<EtDetalleType>();
            }
            return this.etDetalleRow;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="itReturnRow" type="{http://claro.com.pe/eai/ws/sap/simcards/types}itReturnType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itReturnRow"
    })
    public static class ItReturn {

        protected List<ItReturnType> itReturnRow;

        /**
         * Gets the value of the itReturnRow property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the itReturnRow property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItReturnRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItReturnType }
         * 
         * 
         */
        public List<ItReturnType> getItReturnRow() {
            if (itReturnRow == null) {
                itReturnRow = new ArrayList<ItReturnType>();
            }
            return this.itReturnRow;
        }

    }

}
