package pe.com.claro.eai.ws.sap.simcards.util;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlObject;

public class EaiUtil implements Serializable {

    @SuppressWarnings("compatibility:5202602955694279224")
    private static final long serialVersionUID = 5202602955694279224L;

    private static HashMap<Class, JAXBContext> mapContexts = new HashMap<Class, JAXBContext>();

    private static final Logger LOGGER = Logger.getLogger(EaiUtil.class);

    public static String getXmlTextFromJaxB(Object anyObject) {
        String commandoRequestEnXml = null;
        JAXBContext context;
        try {
            context = obtainJaxBContextFromClass(anyObject.getClass());
            Marshaller marshaller = context.createMarshaller();

            StringWriter xmlWriter = new StringWriter();
            marshaller.marshal(new JAXBElement(new QName("", anyObject.getClass().getSimpleName()), anyObject.getClass(), anyObject), xmlWriter);

            XmlObject xmlObj = XmlObject.Factory.parse(xmlWriter.toString());

            commandoRequestEnXml = xmlObj.toString();
        }
        catch (Exception e) {
            LOGGER.error("ERROR[XMLException]: " + e.getMessage(), e);
        }
        return commandoRequestEnXml;
    }

    private static JAXBContext obtainJaxBContextFromClass(Class clas) {
        JAXBContext context;
        context = mapContexts.get(clas);
        if (context == null) {
            try {
                context = JAXBContext.newInstance(clas);
                mapContexts.put(clas, context);
            }
            catch (Exception e) {
                LOGGER.error("ERROR[XMLException]: " + e.getMessage(), e);
            }
        }
        return context;
    }
    
    public static String getStackTraceFromException(Exception exception) {
        StringWriter stringWriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(stringWriter, true));
        return stringWriter.toString();
    }
}
