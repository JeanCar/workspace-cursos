package pe.com.claro.actualizar.estado.cupon.dao;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.com.claro.actualizar.estado.cupon.exception.DBException;
import pe.com.claro.actualizar.estado.cupon.util.PropertiesExternos;
import pe.com.claro.actualizar.estado.cupon.util.PropertiesInternos;

@Repository
public class MssapDaoImpl implements MssapDao{

	@Autowired
	@Qualifier("mssapDS")
	private DataSource mssapDS;
	
	@Autowired
	private PropertiesExternos propertiesExternos;
	
	public static final Logger	logger	= Logger.getLogger(MssapDaoImpl.class);
	
	@Override
	public void actualizarEstadoCupon(String mensajeTransaccion, String fecha) throws DBException {
		String mensaje = mensajeTransaccion.concat("[ MssapDaoImpl : actualizarEstadoCupon ]");
		long tiempoInicio = System.currentTimeMillis();
		logger.info(mensaje.concat("[INICIO] - METODO: [actualizarEstadoCupon - DAO] "));
		try{
			logger.info(mensaje.concat(("Consultado Base de Datos : ").concat(propertiesExternos.dbMssapSchema)));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(mssapDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExternos.dbMssapOwner)
					.withProcedureName(propertiesExternos.spMSSAPActualizaEstadoCupon)
					.declareParameters(new SqlParameter("PI_FECHA", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_COD_RESPUESTA", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_MSJ_RESPUESTA", OracleTypes.VARCHAR));
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExternos.dbMssapExecutionMaxTimeOut);
			logger.info(mensaje.concat(" Se invocara el SP: ").concat(propertiesExternos.dbMssapOwner+PropertiesInternos.punto).concat(propertiesExternos.spMSSAPActualizaEstadoCupon));
			logger.info(mensaje + "PARAMETROS [INPUT]: ");
			logger.info(mensaje + "PI_FECHA = " + fecha);
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
			.addValue("PI_FECHA",fecha);
			jdbcCall.execute(objParametrosIN);
		}catch (Exception e) {
				String codigoError = "";
				String mensajeError = "";
				if(e.getMessage().contains(propertiesExternos.dbErrorSqlTimeOutException)){
					codigoError = propertiesExternos.spMSSAPActualizaEstadoCuponIdt1Codigo;
					mensajeError = String.format(propertiesExternos.spMSSAPActualizaEstadoCuponIdt1Mensaje,propertiesExternos.dbMssapName.concat(PropertiesInternos.punto).concat(propertiesExternos.dbMssapSchema)).concat(e.getMessage());
				}
				else{
					codigoError = propertiesExternos.spMSSAPActualizaEstadoCuponIdt2Codigo;
					mensajeError = String.format( propertiesExternos.spMSSAPActualizaEstadoCuponIdt2Mensaje,propertiesExternos.dbMssapName.concat(PropertiesInternos.punto).concat(propertiesExternos.dbMssapSchema)).concat(e.getMessage());
				}
				logger.error(mensaje + mensajeError + "[" + e.getMessage() + "] ", e);
				logger.error(e.getCause());
				throw new DBException(codigoError,mensajeError, e);		
		}finally{
			logger.info(mensaje + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)+ " milisegundos ]");
			logger.info(mensaje + "[FIN] - METODO: [actualizarEstadoCupon - DAO] ");
		}
	}

}
